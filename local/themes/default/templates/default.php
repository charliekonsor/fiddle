<?php
print "
".$this->header()."
<body id='body'>
	<div id='container'>
		<div id='header'>
			<div class='menu menu-horizontal' id='menu-main'>
				".$this->load->view('menus.item.main')."
			</div>
		</div>
		<div id='content'>
			".($error ? $error : $this->view())."
			<div class='clear'></div>
		</div>
		<div id='footer'>
			<div class='menu menu-horizontal' id='menu-footer'>
				".$this->load->view('menus.item.footer')."
			</div>
		</div>
	</div>
</body>
".$this->footer();
?>