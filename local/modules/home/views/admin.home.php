<?php
// Heading
print $this->heading();
// Buttons
print $this->heading_buttons();

// Make sure id = 1, then reload item_class/class
$this->id = 1;
$this->class = $this->item_class = item::load($this->module,$this->id);

// Permission
if($this->permission('edit',1)) {
	// Form
	$form = $this->item_class->form();
	print $form->render();
}
?>