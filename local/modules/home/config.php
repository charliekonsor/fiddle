<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Home Page',
	'single' => 'Home Page',
	'plural' => 'Home Page',
	'icon' => 'home',
	'db' => array(
		'table' => 'home',
		'id' => 'home_id',
		'name' => 'home_name',
		'theme' => 'home_theme',
		'template' => 'home_template',
		//'meta_title' => 'home_meta_title', // We use global settings for these on the home page
		//'meta_description' => 'home_meta_description',
		//'meta_keywords' => 'home_meta_keywords',
		'created' => 'home_created',
		'updated' => 'home_updated'
	),
	'permissions' => array(
		'edit' => array(
			'label' => 'Edit',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'view' => array(
			'label' => 'View',
			'default' => array(
				'admins',
				'members',
				'guests'
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'views' => array(
			'home' => 'Home Page'
		)
	),
	'public' => array(
		'views' => array(
			'item' => array(
				'label' => 'Home Page',
				'item_id' => 1,
				'urls' => array(
					array(
						'format' => '{home}',
					),
				),
				'meta' => array(
					'title' => array(
						'format' => '{modules.settings.settings.site.meta.title}'
					)
				)
			),
		),
	),
	'plugins' => array(
		'permissions' => 1,
		'sitemap' => 1,
	),
	'settings' => array(
		'sitemap' => array(
			'views' => array(
				'item'
			)
		)
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'home_name',
					'default' => 'Home',
					'validate' => array(
						'required' => 1,
					),
				),
				/*array( // This gets added dynamically in the form_home() class, based upon how many {} are in the home page 'view.
					'label' => 'Text',
					'type' => 'ckeditor',
					'name' => 'home_text',
					'validate' => array(
						'required' => 1,
					),
				)*/
			)
		)
	)	
);
?>