<?php
print "
<h1>Pages</h1>
Adding a page is probably the most basic way to add content to your site. It will also be the tool you probably use the most. As such, knowing exactly how to add, upate, and otherwise manage a page is pretty important.  Which, of course, is why you're reading this documentation.<br /><br />

A page, at its simplest level, is just a Name and some Text (or, more specifically, some HTML which you edit through a WYSIWYG...more on this later).  Before we can start managing the content of our pages, let's take a look at how the home page of the Pages module in the admin. To do so, we'll click on 'Content' in the left hand sidebar, then click on 'Pages', and...<br /><br />

<h2>Pages home page</h2>
As promised, let's take a look at the home page of the Pages module:<br /><br />

<em>Pages module home page</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/home.png' alt='' width='650' /><br /><br />

You're probably familiar with (or can guess) what most of the parts of this page do: the 'Edit' icon lets you edit a page, the 'Search..' box lets you search pages, clicking on a page's name also lets you edit that page.  But for now we're going to want to concentrate on the button we see in the top right, the 'Add New Page' button.<br /><br />

<h2>Adding a page</h2>
Once you've clicked on the 'Add New Page' link, you should be presented with a form that looks something like this:<br /><br />

<em>New page form</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/item-new.png' alt='' width='650' /><br /><br />

The main part of this form is pretty basic. Just two inputs. The second one (Text) is a doozey though.  Let's take a look at these fields:<br /><br />

<h3>Name</h3>
This is pretty simple, it's just the name of the page.<br /><br />

However, you should be aware the name is also used to create the URL.  You can of course create a custom URL via the 'URL' section in the sidebar, but if you leave that blank, we'll use the name to construct the URL.<br /><br />

The Name is also used for the 'Meta Title' of the page.  Though, once again, you can customize the Meta Title of a page via the 'Meta' section in the sidebar.<br /><br />

<h3>Text</h3>
The text input uses a WYSIWYG to allow you to easily create the HTML of your page.  It's a powerful tool, but with all those buttons, it can be a little intimidating. But we're here to learn, right?

First off, what in the blazes does WYSIWYG mean? Well, WYSIWYG (pronounced <em>wizzy-wig</em>) stands for \"What you see is what you get\". That just means that what appears in the Editor is exactly (or very nearly) what it will look like when it's published. <br /><br />

If you get bored or curious you might take a look at the Source Code of a Page you created (by clicking the <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-source.png' alt='' /> button in the top left of the WYSIWYG editor's toolbar). When you do, a window will pop up with a bunch of tags and code and bits of your text. This is the HTML of the page, or all the technical crap which makes your Page look the way it does. The WYSIWYG hides all this code and just shows you \"What You Get\".<br /><br />

Alright, so enough about what it is. Now, how do I make it do what I want?<br /><br />

<h4>Text</h4>
Text will always be the basis of your site. Pictures are pretty, videos interesting, but text will always be the bulk of your content. To create text in the WYSIWYG editor simply click inside the editor's text box and start typing.<br /><br />

Simple enough. And if you want to alter your text (make it bigger, smaller, change the color, make it bold, italic, or do whatever else) you simply select the text you want to format and then click on the appropriate button in the WYSIWYG's toolbar just like you would with Microsoft Word or any document creation software. <br /><br />

It's probably a good time to mention that if you don't know what an icon does (\"what does that horse shoe thing <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-character.png' alt='' /> do?\") you can simply hover over the button and, after a moment, a description will appear (\"Oh, it 'Inserts Special Characters').<br /><br />

<a name='uploadbrowse'></a>
<h4>Upload / Browse Server</h4>
One of the nicest features of the WYSIWYG is the ability to Upload files (images, videos, etc.) without leaving the page.  You can also easily access file's you've already uploaded (via the <b>Browse Server</b> button). This is particularly useful when inserting links, images, and flash. In fact, we'll often reference back to this section when talking about adding media so look sharp.<br /><br />

<a name='upload'></a>
<b>Upload</b><br />
Whenever you click the Link icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-link.png' alt='' />, Image icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-image.png' alt='' />, or Flash (video) icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-flash.png' alt='' /> you'll see a window with a bunch of options for adjusting your link / image / flash.<br /><br />

<em>Image Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-image.png' alt='' /><br /><br />

But you'll also see a tab at the top of the window called Upload. And when you click it, you'll see this screen:<br /><br />

<em>Image Window - Upload Tab</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-image-upload.png' alt='' /><br /><br />

Here you have the ability to Browse your computer for different files and then upload them (by clicking the <b>Send it to the Server</b>). It's that simple. Indeed, I feel like I should explain more about the Upload feature, but it's just too simple. Click the <b>Browse</b> button, locate the file you want to upload, and click the <b>Send it to Server</b> link.<br /><br />

It may take a few moments to upload (depending on the file size), but once it's complete you'll get a message saying whether the upload succeeded or failed. If it was successful, it will also automatically put the URL of the uploaded file into the URL field on the main page of the Link / Image / Flash Window. <br /><br />

So, for example, if you were to upload an image called \"me.jpg\" you would see the new URL appear in your Image URL field as illustrated below:<br /><br />

<em>URL Auto Insert</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-image-uploaded.png' alt='' /><br /><br />

From there it's just a matter of tweaking the different properties (they're all optional) and clicking the <b>OK</b> button at the bottom of the window.<br /><br />

<a name='browse'></a>
<b>Browse Server</b><br />
So let's say you've already upload a file. You don't want to have to upload it again. Well, you don't have to. <br /><br />

On the main page of the Link / Image / Flash Windows you should see a <b>Browse Server</b> button to the right of the URL field (look at the image above, see it?). That button opens a new window which lists all the files you've already uploaded and looks something like this:<br /><br />

<em>Browse Server Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-browse.png' width='650' alt='' /><br /><br />

If you've ever used a PC or a Mac or a computer of any kind (and I'm going to assume you have), you should know how to navigate this page. You click on the folders to see inside them, you double click on a file to use it, etc. But I'll point out a few cool features as well.
 
One really useful feature is the <b>Upload</b> section at the top of the Window. This works just like the Upload tab we just talked about. Simply click the <b>Browse</b> button, locate your file, and click the <b>Upload Selected File</b> button.<br /><br />

The <b>Refresh</b> button will reload the content items if you think new items have recently been uploaded but are not showing. And last is the <b>Settings</b> button, which offer options for viewing, display and sorting the files you've already uploaded to the server.<br /><br />

Using those two tools (Upload and Browse Server) you should be able to add almost any kind of media to your Page. <br /><br />

<h4>Links</h4>
Of course you'll need a few links in your Page. So how do you add them? Well, just select the text (or image) you want to serve as the link (example \"I found this on <span style='background:#fffd71;'>Google</span>.\") and then click the link icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-link.png' alt='' />. When you do, you'll see this window:<br /><br />

<em>Link Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-link.png' alt='' /><br /><br />

<b>Link Type</b><br />
This is just the type of link (URL, Anchor, or Email). 95% of the time you'll use the URL Link Type which lets you enter a <b>URL</b> to link to. It also allows you to link to a file on your server (more about that in a minute).<br /><br />

Anchor links allow you to link to a different spot on the same page (used in conjunction with the Anchor icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-anchor.png' alt='' /> in the WYSIWYG). <br /><br />

E-mail links allow you to type in an e-mail address. When a visitor click on the link, their default e-mail software (Outlook, Thunderbird, etc.) will open up with the e-mail address in the 'To:' field.<br /><br />

<b>Protocol</b><br />
You'll almost always want to use <b>http://</b>. Unnless you need to link to a secure site, then you'd use https://.<br /><br />

<b>URL</b><br />
This is just the URL of the page you want to link to such as http://www.google.com/. If you simply enter the link url and click the <b>OK</b> button you'll see that your text is now blue and underlined (example \"I found this on <a href='http://www.google.com/'>Google</a>.\"). That means your link worked and if you were to save your Page using the Save button at the bottom of the form and visit the public Page you'd be able to click the link and be taken to http://www.google.com/.<br /><br />

<b>Browse Server</b><br />
This button allows you to browse for files which you've uploaded to your site (images, videos, pdfs, etc.). For much more info on this function, see the <a href='#browse'>Browse Server</a> section above. For now, though, we'll just do a quick run down.<br /><br />

For example, if you wanted to link to a .pdf file and you'd already uploaded it, you'd see it on the Browse Server page. And all you'd have to do to link to that .pdf file would be locate it in the Browse Server page and click on it. It should then automatically appear in your link's URL field. <br /><br />

Then, it's just a matter of clicking the <b>OK</b> button on the Link Window and you've created a link to your .pdf file.<br /><br />

<b>Upload Tab</b><br />
At the top of the Link Window you should see an Upload tab. We've already described how to use this page (see the <a href='#upload'>Upload</a> section above). Here's a quick rundown of how to use it with links.<br /><br />

If, for example, you wanted to link to an excel file (.xls), you could upload your .xls file and it would automatically appear in your link's URL field.<br /><br />

<b>Fancier Stuff</b><br /><br />

<em>Captions:</em><br />
Sometimes you want the user to be able to see a little more information about the link before they click it. To add this 'Title' variable as it's called, simply click on the <b>Advanced</b> tab at the top of the Link Window, type your desired text into the <b>Advisory Title</b> field, and click <b>OK</b>. Then, whenever a user hovers their mouse over the link (on the live site) they'll see a little bubble with that 'Title' text.<br /><br />

<em>New Window Links:</em><br />
Sometimes you want to open a link in a new window or tab. To achieve these links click on the <b>Target</b> tab on the top of the Link Window. From here simply change the selection for the <b>Target</b> variable to \"_blank\". Note: each \"Target\" value has an explanation of what it does (example: _blank opens a new window).<br /><br />

<b>Removing Links</b><br />
Finally, you may decide you want to get rid of a link. To do this simply highlight the text (or select the image) which is the link and click the Unlink icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-unlink.png' alt='' /> which appears right next to the Link icon. Your link will then be removed.<br /><br />

<h4>Tables</h4>
In laying out your Pages you may find a need for tables. The WYSIWYG editor easily handles these. Simply click the Table icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-table.png' alt='' /> and the Table Window will popup:<br /><br />

<em>Table Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-table.png' alt='' /><br /><br />

You're probably familiar with this window as it's very similar to the way software like Microsoft Word handles tables. We'll give you a quick description of some of the most important values though.<br /><br />

<b>Rows / Columns</b><br />
Pretty simple, these let you specify how many columns and rows you want.<br /><br />

<b>Border Size</b><br />
If you'd like each table cell to have a border, you can set that value here.<br /><br />

<b>Width / Height</b><br />
How big you want your entire table to be (in pixels). You'll be able to drag the table to different sizes after it's been inserted into the WYSIWYG if you like.<br /><br />

<b>Cell Padding</b><br />
This applies padding to each cell. The value is in pixels. <br /><br />

<b>Cell Spacing</b><br />
This specifies how much space you want between each table cell. The value is in pixels. <br /><br />

The only really necessary values are the <b>Columns</b> and <b>Rows</b> values. If you simple set those values and click the <b>Insert</b> button at the bottom of the window a small table will appear in your editor. <br /><br />

<b>Editing Tables</b><br />
As with everything else in the WYSIWYG, if you select your table, and click the Table icon in the WYSIWYG toolbar, you'll be able to edit all the Table's Properties.<br /><br />

<h4>Images</h4>
We've already mentioned a bit about adding images to your Page, but we'll take just a few moments to explain the WYSIWYG's image tool.<br /><br />

You probably already recognized the Image icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-image.png' alt='' />. And when you click it you get this Image Window.<br /><br />

<em>Image Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-image.png' alt='' /><br /><br />

The images are pretty straight forward here. As we mentioned before, to add an image using this method you must enter the <b>Image URL</b> (the place where you images is located such as http://www.google.com/images/logo.gif). Nothing else is required and you'll see that after you type in your Image URL and move onto the next input field that a Preview of your image shows up in the <b>Preview</b> box. If the image doesn't appear, double check to make sure that your entered the Image URL correctly.<br /><br />

Of course, it's a bit of a hassle to always locate and type in your image's URL. Luckily, the WYSIWYG provides a <b>Browse Server</b> button, as well as an <b>Upload</b> tab.<br /><br />

<b>Upload / Browse Server</b><br />
We already talked a lot about these in the <a href='#uploadbrowse'>Upload / Browse Server</a> section so we won't bore you any further. Instead, we'll concentrate on some of the other possible <b>Image Properties</b>.<br /><br />

<b>Alternative Text</b><br />
If, for some reason, you image got deleted, the Alternative Text would show up (instead of a broken image icon). Alternative text also helps search engines tell what they're looking at, which is always a good thing so it's a good idea to include a short description of the image.<br /><br />

<b>Width / Height</b><br />
If you leave these blank you image will show up just fine. The only reason you'd need these was if you wanted to shrink the size of your image for some reason. Note: you should also be able to select and drag the size of the image once it's been added to the WYSIWYG.<br /><br />

<b>HSpace / VSpace</b><br />
The Horizontal and Vertical space lets you add a 'margin' around the image. For example, if your image was in the middle of some text, but you didn't want the text to run up right next to the image (which might look a little too cluttered), you could add some Horizontal and Vertical space (in pixels). Then your text won't invade the 'margin' space you've added around the image.<br /><br />

<b>Align</b><br />
The Align property lets you change where in the text your image is located. You can leave it blank and you image will show up just fine. There are two very helpful Align values, however. The <b>Left</b> and <b>Right</b> values position your image on either the right or left side of the page. They also allow text to 'wrap' around the image which is very useful.<br /><br />

<b>Editing an Image</b><br />
As with most of the WYSIWYG Toolbar functions, to edit an image all you have to do is select the image and re-click the Image icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-image.png' alt='' /> you used to first insert it.<br /><br />

<h4>Videos</h4>
More and more videos seem to be taking over the internet. Fortunately, the WYSIWYG offers a way to include videos. Unfortunately, it only allow Flash videos, such as those found on YouTube.<br /><br />

First, the icon <img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-icon-flash.png' alt='' />. And when you click it, you get the Flash Window:<br /><br />

<em>Flash Window</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/wysiwyg-window-flash.png' alt='' /><br /><br />

<b>URL</b><br />
The URL value is the web location of your flash file / video such as http://www.myweb.com/videos/me.swf. If your video is already online here would be where you'd enter its location. If you uploaded it using the WYSIWYG (see below) you may also use the <b>Browse Server</b> button we've talked about before (<a href='#browse'>Browse Server</a>) to more easily find your flash file / video.<br /><br />

Now, if your video isn't on the web yet, don't worry, you'll still be able to add your video using the <b>Upload</b> tab at the top of the Flash window. For much more on uploading, see the <a href='#uplod'>Upload</a> section.<br /><br />

<b>Width / Height</b><br />
Unlike with images, DWS CMS doesn't automatically detect the size of your flash file / video so here would be where you'd want to specify the width and height of your video (in pixels).<br /><br />

<b>HSpace</b> sets the horizontal space between the image and surrounding text. <b>VSpace</b> sets the vertical space between the image and surrounding text.<br /><br />

<b>Preview</b><br />
The Preview box shows you what you're video looks like. If it's not showing up, double check to make sure you have the correct URL. <br /><br />

Note: when you click the OK button, you won't be able to see your video in the WYSIWYG. Instead, you should see a box with a 'Flash' icon in the middle of it.<br /><br />

<b>YouTube Videos</b><br />
YouTube has become so popular for videos that it seems prudent to include a method by which you can add YouTube videos to your website. It takes a little work, but it should be worth it.<br /><br />

First, you must get the URL of the video. This is pretty easy to do, though. All you have to do is visit the YouTube page where the video is displayed. There you will be able to get your video's \"ID\" by looking at the video URL in your browsers address bar. For example:<br /><br />

<center>http://www.youtube.com/watch?v=jNQXAC9IVRw</center><br /><br />

The \"ID\" is the string of letters and numbers at the end of the URL. In this case, the ID is GHyo33XLP24. Now, you just need to create the URL of the actual flash video. To do so, you just have to add the ID to this URL, http://www.youtube.com/v/, so our sample video's flash URL would be:<br /><br />

<center>http://www.youtube.com/v/jNQXAC9IVRw</center><br /><br />

Returning to your website, then, all you have to do is click the Video icon in the WYSIWYG and enter the YouTube video's <b>URL</b> (which you just found) into the URL field. The dimensions for all YouTube videos is the same: 425px wide, 344px high). Then, click the <b>OK</b> button at the bottom of the Video Window.<br /><br />

As with other videos, you will only see a box in the WYSIWYG, but fear not, your video will show up on the live page.<br /><br />

".$this->documentation_plugins() ."

".$this->documentation_manage();

?>