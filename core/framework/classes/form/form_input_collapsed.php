<?php
if(!class_exists('form_input_collapsed_framework',false)) {
	/**
	 * Creates a collapsed input
	 */
	class form_input_collapsed_framework extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$c = NULL) {
			return parent::load($label,NULL,NULL,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_collapsed_framework($label,$name,$value,$c);
		}
		function form_input_collapsed_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!x($this->c[open])) $this->c[open] = 0; // Whether or not you want the collapsed section 'opened' already when the form is displayed.
		}
		
		/**
		 * Renders the HTML for the input's 'label', including the container.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's label, including the container.
		 */
		function html_row($form,$c = NULL) {
			$r = random();
			
			// End last collapsed section
			if($form->collapsed) {
				$html .= "
	</div>
</div>";
			}
			
			// Start new collapsed section
			$html .= "
<div class='form-collapsible form-collapsible-".$r."'>
	<div class='form-collapsible-heading'>
		<a href='javascript:void(0);' onclick=\"toggle('#form-collapsible-".$r."-content',{animation:'',heading:'form-collapsible-".$r."-heading'});\" class='toggle-".($this->c[open] ? "open" : "closed")."' id='form-collapsible-".$r."-heading'>".$this->label[value]."</a>
	</div>
	<div class='form-collapsible-content' id='form-collapsible-".$r."-content' style='".($this->c[open] ? "display:block;" : "display:none;")."'>";
	
			// Store that we're in a collapsed section
			$form->collapsed = 1;
			
			// Return
			return $html;
		}
	}
}
?>