<?php
if(!class_exists('form_input_framework',false)) {
	/**
	 * An extension of the form_input class with functionality specific to the framework.
	 *
	 * At the moment, the only real reason for this is to declare the $types variable so,
	 * when passing it via $c, it'll get saved as $this->types and the type check in
	 * $form_framework->input_render() will work as indended.
	 */
	class form_input_framework extends form_input {
		/**
		 * Variables
		 */	
		public $types;

		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_framework($label,$name,$value,$c);
		}
		function form_input_framework($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Standardizes the given array of options.
		 *
		 * Want in array('value' => '123','label' => 'Item 123') format instead of array('123' => 'Item 123') format.
		 *
		 * In the framework, we can pass option 'classes' which load options from the database (ex: a module's items) so $array can be a string.
		 *
		 * @param array|string $options The array of options we want to standardize or an options 'class'.
		 * @return array The standardized array of options.
		 */
		function options($options) {
			// Class
			if($options and !is_array($options)) {
				// Module class
				$module_class = module::load($this->c[options_module],array('type' => (!is_array($this->c[options_type]) ? $this->c[options_type] : NULL)));
					
				// Items
				if($options == "items" or $options == "items_own") {
					$query_c = array(
						'db.user' => ($options == "items_own" ? u('id') : ""),
						'db.visible' => 1,
						//'order' => $module_class->v('db.name')." ASC", // Using PHP for sorting (faster than MySQL)
					);
					if(is_array($this->c[options_type]) and $module_class->v('db.type')) {
						$query_c[where] .= ($query_c[where] ? " AND " : "").$module_class->v('db.type')." IN ('".implode("','",$this->c[options_type])."')";
					}
					$rows = $module_class->rows($query_c);
					while($row = $module_class->db->f($rows)) {
						$options_new[$row[$module_class->v('db.id')]] = s(string_format($module_class->v('db.name'),$row));
					}
					natcasesort($options_new); // Faster than via MySQL
				}
				
				// Modules
				if($options == "modules") {
					foreach(m() as $k => $v) {
						$options_new[$k] = $v[name];
					}
				}
				
				// Types
				if($options == "types") {
					if($array = $module_class->v('types')) {
						foreach($array as $k => $v) {
							if($this->c[options_module] != "users" or $k != "supers") $options_new[$k] = $v[single];
						}
					}
				}
				
				// Categories
				if($options == "categories" and $this->c[options_category]) {
					// Code to id
					if($this->c[options_category] and !is_int_value($this->c[options_category])) {
						$this->c[options_category] = code_id('categories',$this->c[options_category]);
					}
					
					// Options
					$options_new = categories_options($this->c[options_category]);
					
					// Name - need specific input 'name' to save...shouldn't, but do...for now
					$this->name = "__plugins[categories][".$this->c[options_category]."]";
				}
				
				// Views
				if($options == "views") {
					if($array = $module_class->v('public.views')) {
						foreach($array as $k => $v) {
							if(is_array($v)) $options_new[$k] = $v[label];
							else $options_new[$k] = $v;
						}
					}
					
				}
				// Countries // !!! Use 'options' => 'items', 'options_module' => 'countries' !!!
				/*if($options == "countries") {
					$module_class = module::load('countries');
					$rows = $module_class->rows(array('order' => $module_class->v('db.name')));
					while($row = $module_class->db->f($rows)) {
						$options_new[$module_class->v('db.id')] = s($row[$module_class->v('db.name')]);
					}
				}*/
				
				// States
				if(substr($options,0,6) == "states") {
					// Database
					$db = db::load();
				
					// United Sates - keyed by code
					/*if($options == "states") {
						$rows = $db->q("SELECT * FROM states WHERE country_code = 'US' ORDER BY state_name ASC");
						while($row = $db->f($rows)) {
							$options_new[$row[state_code]] = s($row[state_name]);
						}
					}
					// Global states - keyed by id
					else {*/
						$country = $this->c[options_country];
						if(!$country) list($blank,$country) = explode('_',$options);
						if(!$country) $country = 234; // Default to United States
						if(!is_array($country)) $country = array($country);
						$country_count = count($country);
						$rows = $db->q("SELECT s.*,c.country_name FROM states s JOIN countries c ON s.country_id = c.country_id WHERE s.country_id IN (".implode(',',$country).") ORDER BY c.country_name ASC, s.state_name ASC");
						$options_grouped = NULL;
						$country_saved = NULL;
						while($row = $db->f($rows)) {
							if($country_count > 1 and $row[country_name] != $country_saved) {
								if($country_saved) {
									$options_grouped[] = array(
										'label' => $country_saved,
										'options' => $options_new
									);
									$options_new = NULL;
								}
								$country_saved = $row[country_name];
							}
							$options_new[$row[state_id]] = s($row[state_name]);
						}
						if($country_saved) {
							$options_grouped[] = array(
								'label' => $country_saved,
								'options' => $options_new
							);
							$options_new = $options_grouped;
						}
					//}
				}
				
				// Update
				$options = $options_new;
			}
			
			// Return
			return parent::options($options);
		}
		
		/**
		 * Renders the HTML for the input's 'field'.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input's field.
		 */
		function html_field($form,$c = NULL) {
			// Value - category
			if($form->id and !x($this->value) and $this->c[options_category]) {
				$plugin_item = $form->item->plugin('categories');
				$this->value = $plugin_item->categories($this->c[options_category]);
			}
			
			// International
			$this->plugin_international($form);
			
			// Add
			if($this->c[add] === 1) $this->c[add] = array('active' => 1); // Legacy, used to just be $c[add] as opposed to $c[add][active] = 1;
			if(!$this->c[add][module]) $this->c[add][module] = $this->c[options_module]; // The module you want to 'add' an item to
			if(!$this->c[add][type]) $this->c[add][type] = $this->c[options_type]; // The module typoe you want to use when adding an item
			if(!$this->c[add][title]) $this->c[add][title] = $this->label[value]; // The title to display when we show the 'add' form
			if($this->c[add][active] and $this->c[add][module] /*and $_GET['ajax_action'] != "form_add_option"*/) {
				// Element
				$this->c[add][active] = 0;
				$html_element = $this->html_element($form,$c);
				$html = "
<div id='form-add-field-".str_replace(array('[',']'),'',$this->name)."' class='form-add-field".($this->type == "select" ? " form-add-field-inline" : "")."'>";
				if($html_element) $html .= "
	".$html_element;
				else {
					$html .= "
	<em class='form-add-none'>No ".strtolower(m($this->c[add][module].'.plural',$this->c[add][type]))." found</em>";
					if($this->validate[required]) $html .= "
	<input class='required required-input' type='text' name='".$this->name."' style='width:0px;height:0px;padding:0px;margin:0px;visibility:hidden' />";
				}
				$html .= "
</div>";
				// Add
				$url = D."?ajax_action=form_add_option&amp;ajax_module=".$this->c[add][module]."&amp;type=".$this->c[add][type]."&amp;form_module=".$this->module."&amp;".http_build_query(array('input' => serialize($this)));
				if($_GET['ajax_action'] == "form_add_option") $url .= "&amp;".http_build_query(array('parent' => g('unpure.get')));
				if($this->type != "select") $html .= "
<div class='clear'></div>";
				$html .= "
<div class='form-add-button".($this->type == "select" ? " form-add-button-inline" : "")."'>";
				if($this->type == "select") $html .= "
	<a href='".$url."' class='i i-add i-inline overlay {width:430,height:320}' title='".string_encode($this->c[add][title])."'></a>";
				else $html .= "
	<a href='".$url."' class='i-button overlay {width:430,height:320}' title='".string_encode($this->c[add][title])."'><span class='i i-add i-text'>New ".m($this->c[add][module].'.single',$this->c[add][type])."</span></a>";
				$html .= "
</div>
<div class='clear'></div>";
					
				// Prepend
				if($this->prepend) $html = $this->prepend.$html;
				
				// Append
				if($this->append) $html .= $this->append;
			
				// Help
				if($this->help) $html .= help($this->help);
					
				// Error
				if($this->error) $html .= "
					<label class='error form-error' for='".$this->name."' style='display:inline;'>".$this->error."</label>";
				
				// Return
				return $html;
			}
			// Default
			else {
				return parent::html_field($form,$c);
			}
		}
		
		/**
		 * Adds 'international' button to input (adding it to its 'append' value)
		 *
		 * Add hidden input so it'll call $plugin_international->item_save() at which point we can get the $_SESSION values.
		 *
		 * Not super cool to have it here, should try and get it into the international plugin files somehow, but for now we'll do it here.
		 *
		 * Should also probalby move the call to this to the 'framework' version of each input (instead of cheching $this-> type here),
		 * but lots of redundancies and it's something we'll get rid of when we figure out how to move this to the plugin files anyway.
		 */
		function plugin_international($form) {
			// Plugin
			preg_match('/__plugins\[(.*?)\]/',$this->name,$match);
			$plugin = $match[1];
			
			if(
				$form->module
				and plugin('international')
				and m($form->module.'.plugins.international')
				and m('languages')
				and count(m('languages.settings.languages.available')) > 1
				and $this->name
				and ( // Isn't a plugin column or the plugin doesn't have a seperate table (aka, we store the value in the module's table)
					!$plugin
					or !plugin($plugin.'.db.table')
				)
				and in_array($this->type,array('text','number','url','email','textarea','textarea_ckeditor','file','file_swfupload','file_plupload'))
			) {
				// Key
				$key = microtime_float()."-".random();
				
				// Store
				cache_save("forms/inputs/".$key,$this,array('force' => 1));
				
				$r = random();
				$db = db::load();
				$append = "
<a href='".D."?ajax_plugin=international&amp;ajax_action=international_value&amp;ajax_module=".$form->module."&amp;id=".$form->id."&amp;column=".$this->name."&amp;icon=i-international-".$r."&amp;input=".$key."' class='i i-international i-inline overlay {type:\"iframe\",width:750,height:500} tooltip' title='International Values' id='i-international-".$r."'>";
				if($form->id) {
					$count = 0;
					$rows = $db->q("SELECT * FROM items_international WHERE module_code = '".$form->module."' AND item_id = '".$form->id."' AND item_column = '".$this->name."'");
					while($row = $db->f($rows)) {
						$count++;
						$append .= "<input type='hidden' name='__plugins[international][ids][]' value='".$row[id]."' />";
					}
					if($count) $append .= "<span class='i-count'>".$count."</span>";
				}
				$append .= "
</a>";
				$this->append = $append.$this->append;
				
				// Input - need an input in the form which we can 'process' 
				$form->hidden('__plugins[international][ids][]');
				
				// Session
				$_SESSION['__plugins']['international']['ids'] = NULL;
			}
		}
	}
}
?>