<?php
if(!class_exists('form_framework',false)) {
	/**
	 * Class for creating a form and adding inputs to it.
	 *
	 * Example:
	 *	$form = new form_framework('users',1); // Create a new instance of the form, passing the $module and $id (or 'new') of the item we're editing.
	 *	$form->text('Name','name'); // Adding an input via a helper method: $form->text(), $form->textarea(), $form->select(), etc.
	 * 	$form->input('Phone','text','phone'); // Adding an input via the input() method: $form->input($label,$type,$name,$value,$c);
	 *	$form->inputs(array(array('label' => 'E-mail','type' => 'email','name' => 'email','validate' => array('email' => 1))); // Adding an array of inputs
	 *	$form->submit('Submit'); // Adding a submit button via the 'submit()' helper method.
	 *	print $form->render(); // Render the form HTML
	 *
	 * To do
	 * - rotate/crop icons in preview() (different than form's preview)
	 * - Maybe deprecate ->core and just pass those variables in $this->values
	 *
	 * Dependencies
	 * - functions
	 *   - x
	 *   - g
	 *   - m
	 *   - array_first_key
	 *   - string_array - plugins()
	 *   - array_string - plugins()
	 * - classes
	 *   - item
	 *   - db
	 * - variables
	 *   - $_SESSION['urls']
	 *   - $page
	 *
	 * @package kraken\forms
	 */
	class form_framework extends form {
		public $module, $id, $type, $collapsed; // string|int
		public $plugins/*, $core*/ = array(); // array
		public $item, $db; // object
		
		/**
		 * Creates an instance of the class and stores the module and id variables
		 *
		 * @param string $module The module of the item this form will be adding/editing.
		 * @param int $id The id of the item this form will be editing (if adding an item, this will be NULL). Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			self::form_framework($module,$id,$c);
		}
		function form_framework($module,$id = NULL,$c = NULL) {
			// new form_framework($c);
			if(is_array($module) and !$id and !$c) {
				$c = $module;
				$module = NULL;
				$id = NULL;	
			}
			
			// Module
			$this->module = $module;
			// ID
			if($id > 0) $this->id = $id;
			
			// Item
			if(!$this->item) $this->item = item::load($this->module,$this->id,array('debug' => $c[debug]));
			
			// Type
			if(!$this->id and $c[type]) $this->type = $c[type];
			else $this->type = $this->item->type;
			
			// Page
			global $page;
			
			// Database
			$this->db = new db();
			
			// Redirect
			$redirect = NULL;
			if(!$c[redirect]) {
				if(!x($c[redirect])) unset($c[redirect]); // Remove completely if blank so array_merge() will use our default value
				
				if(in_array($page->area,array('admin','account'))) {
					$redirect = DOMAIN.$page->area;
					// Parent(s)
					if($page->parents) {
						foreach($page->parents as $k => $v) {
							if($k !== "last") {
								$redirect .= "/".$v[module];
								if($v[id]) $redirect .= "/".$v[id];
								else if($v[type]) $redirect .= "/".$v[type];
							}
						}
					}
					// Module
					if($page->module) {
						$redirect .= "/".$page->module;
					}
					// Type
					if($page->type and strstr($_SESSION['urls'][1],"/".$page->type)) {
						$redirect .= "/".$page->type;
					}
					// Variables - if no parents or module
					if(!$page->parents and !$page->module) {
						if($page->variables[0]) $redirect .= "/".$page->variables[0];
					}
				}
				else {
					if(in_array($page->area,array('admin','account'))) $redirect = $_SESSION['urls'][1];
					else $redirect = $_SESSION['urls'][0];
				}
			}
			else $c[redirect] = unpurify($c[redirect]); // Because $_GET['redirect'] will have been purified and & will have become &amp; Not an ideal fix, but..
			
			// Config
			$c_default = array(
				'action' => NULL, // Action of the form
				'form' => 'add', // The form you want to load inputs of
				'inputs' => 1, // Whether or not we want to automatically add in the module's defined form inputs
				'plugins' => ($page->area == "admin" ? 1 : 0), // Include plugins sidebar of related data
				'submit' => array( // Submit button ('submit' => 0 will remove the submit button)
					'value' => ($c[submit_text] ? $c[submit_text] : 'Submit'), // Legacy, used to be $c[submit_text]
				),
				'redirect' => $redirect // Redirect URL to return after we process/save
			);
			if($c) $c = array_merge($c_default,$c);
			else $c = $c_default;
			// Config - module
			$c_form = m($this->module.'.forms.'.$c[form],$this->type);
			unset($c_form[inputs]);
			if($c_form) $c = array_merge($c,$c_form);
			
			// Parent
			parent::__construct($c);
			
			// Inputs
			if($this->module and $this->c[inputs]) {
				// Inputs
				$inputs = m($this->module.'.forms.'.$this->c[form].'.inputs',$this->type);
				if($inputs) {
					$this->inputs($inputs);
				}
				// Plugins
				$this->plugins = m($this->module.'.forms.'.$this->c[form].'.plugins',$this->type);
			}
		}
		
		/**
		 * Adds an input to the form
		 *
		 * @param string $label The label of the input
		 * @param string $type The input type (ex: text, textarea, hidden, etc.)
		 * @param string $name The name of the input field (the key in the $_POST/$_GET results)
		 * @param string $value The current value of this input
		 * @param array $c An array of config values. Default = NULL
		 */
		function input($label,$type,$name = NULL,$value = NULL,$c = NULL) {
			// New method
			if(is_object($label)) {
				$name = $label->name;
				$value = $label->value;
			}
			
			// Type - make sure it's all lowercase
			$type = trim(strtolower($type));
			
			// Plugin
			if($type == "plugin") {
				if(m($this->module.'.plugins.'.$c[plugin])) {
					$plugin_instance = plugin::load($this->module,$this->id,$c[plugin]);
					$sidebar = $plugin_instance->sidebar();
					if($sidebar[inputs]) {
						foreach($sidebar[inputs] as $k => $v) {
							$sidebar[inputs][$k][plugin] = $c[plugin];	
						}
						$this->inputs($sidebar[inputs]);
					}
				}
				return;
			}
			
			// Value - need to do here, as well as in standardize, because some inputs still get generated before input_html() is called (namely, swfupload/ckeditor)
			if($this->id) {
				// Saved value
				if(!x($value) and $name) {
					$row = $this->item->row;
					$name_simple = $name;
					if(strstr($name,"]")) {
						list($name_simple,$array) = explode('[',$name,2);
						if(!is_array($row[$name_simple])) $row[$name_simple] = data_unserialize($row[$name_simple]);
					}
					if($encrypt = m($this->module.'.db_encrypt')) {
						if(in_array($name_simple,$encrypt)) {
							$row[$name_simple] = $this->item->v_decrypted($name_simple);
						}
					}
					$value = $this->value($row,$name);
				}
				// Remove default (don't want to use on existing items)
				unset($c['default']);
			}
			// Value - type
			if($name and $name == m($this->module.'.db.type') and $this->type) {
				$value = $this->type;	
			}
			
			// New method
			if(is_object($label) and x($value)) {
				$label->value = $value;
				$label->value_default = NULL;
			}
			
			// Core
			parent::input($label,$type,$name,$value,$c);
		}
		
		/**
		 * Turns any 'option classes' into their corresponding option array then sends to the core form classes input_options() method.S
		 *
		 * @param array $input The input in which you want to standardize the options.
		 * @return array The standardized array of options.
		 */
		/*function input_options($input) {
			// Option class
			$options = NULL;
			if(!is_array($input[options]) and $input[options]) {
				// Module
				$module = ($input[options_module] ? $input[options_module] : $this->module);
				// Type - Note: $input[options_type] can be an array of type codes too. Example: array('admins','supers');
				$type = ($input[options_type] ? $input[options_type] : $this->type);
				// Module class
				$module_class = module::load($module,array('type' => (!is_array($type) ? $type : NULL)));
					
				// Items
				if($input[options] == "items") {
					$query_c = array(
						//'order' => $module_class->v('db.name')." ASC", // Using PHP for sorting (faster than MySQL)
						'db.visible' => 1,
					);
					if(is_array($type) and $module_class->v('db.type')) {
						$query_c[where] .= ($query_c[where] ? " AND " : "").$module_class->v('db.type')." IN ('".implode("','",$type)."')";
					}
					$rows = $module_class->rows($query_c);
					while($row = $module_class->db->f($rows)) {
						$options[$row[$module_class->v('db.id')]] = s(string_format($module_class->v('db.name'),$row));
					}
					natcasesort($options); // Faster than via MySQL
				}
				
				// Types
				if($input[options] == "types") {
					if($array = $module_class->v('types')) {
						foreach($array as $k => $v) {
							if($module != "users" or $k != "supers") $options[$k] = $v[single];
						}
					}
				}
				
				// Categories
				if($input[options] == "categories" and $input[options_category]) {
					$options = categories_options($input[options_category]);
				}
				
				// Views
				if($input[options] == "views") {
					if($array = $module_class->v('public.views')) {
						foreach($array as $k => $v) {
							$options[$k] = $v;
						}
					}
					
				}
				// Countries
				if($input[options] == "countries") {
					// !!! Use 'options' => 'items', 'options_module' => 'countries' !!!
					
					/*$module_class = module::load('countries');
					$rows = $module_class->rows(array('order' => $module_class->v('db.name')));
					while($row = $module_class->db->f($rows)) {
						$options[$module_class->v('db.id')] = s($row[$module_class->v('db.name')]);
					}*/
				/*}
				
				// States
				if(substr($input[options],0,6) == "states") {
					// United Sates - keyed by code
					/*if($input[options] == "states") {
						$rows = $this->db->q("SELECT * FROM states WHERE country_code = 'US' ORDER BY state_name ASC");
						while($row = $this->db->f($rows)) {
							$options[$row[state_code]] = s($row[state_name]);
						}
					}
					// Global states - keyed by id
					else {*/
						/*$country = $input[options_country];
						if(!$country) list($blank,$country) = explode('_',$input[options]);
						if(!$country) $country = 234; // Default to United States
						if(!is_array($country)) $country = array($country);
						$country_count = count($country);
						$rows = $this->db->q("SELECT s.*,c.country_name FROM states s JOIN countries c ON s.country_id = c.country_id WHERE s.country_id IN (".implode(',',$country).") ORDER BY c.country_name ASC, s.state_name ASC");
						$options_grouped = NULL;
						$country_saved = NULL;
						while($row = $this->db->f($rows)) {
							if($country_count > 1 and $row[country_name] != $country_saved) {
								if($country_saved) {
									$options_grouped[] = array(
										'label' => $country_saved,
										'options' => $options
									);
									$options = NULL;
								}
								$country_saved = $row[country_name];
							}
							$options[$row[state_id]] = s($row[state_name]);
						}
						if($country_saved) {
							$options_grouped[] = array(
								'label' => $country_saved,
								'options' => $options
							);
							$options = $options_grouped;
						}
					//}
				}
				
				// Blank option - because we can't manually add one when we're using these option 'classes'. Can overwite by passing 'options_blank' => 0
				if($options and !$input[placeholder] and $input[type] == "select" and !x($input[options_blank])) {
					$input[options_blank] = 1;
				}
				
				// New options
				$input[options] = $options;
			}
			
			// Return
			return parent::input_options($input);	
		}*/
			
		/**
		 * Standardizes an input and it's configuration values.
		 *
		 * @param array $input The input array we want to standardize.
		 * @return array The standardized input array.
		 */
		function input_standardize($input) {
			// New method
			if(is_object($input)) {
				# Almost nothing, no longer use standardization except when we create the input
				
				// Brackets
				if(x($input->value)) $input->value = $this->brackets($input->value);
			}
			// Old method
			else {
				// Value
				if($this->id) {
					// Saved value
					if(!x($input[value]) and $input[name]) {
						$row = $this->item->row;
						$name_simple = $input[name];
						if(strstr($input[name],"]")) {
							list($name_simple,$array) = explode('[',$input[name],2);
							if(!is_array($row[$name_simple])) $row[$name_simple] = data_unserialize($row[$name_simple]);
						}
						if($encrypt = m($this->module.'.db_encrypt')) {
							if(in_array($name_simple,$encrypt)) {
								$row[$name_simple] = $this->item->v_decrypted($name_simple);
							}
						}
						$input[value] = $this->value($row,$input[name]);
					}
					// Remove default (don't want to use on existing items)
					unset($input['default']);
				}
				// Value - type
				if($input[name] and $input[name] == m($this->module.'.db.type') and $this->type) {
					$input[value] = $this->type;
				}
				
				// Brackets
				if(x($input[value])) $input[value] = $this->brackets($input[value]);
				if(x($input['default'])) $input['default'] = $this->brackets($input['default']);
				
				// Categories - need specific input 'name' to save...shouldn't, but do...for now
				if($input[options] == "categories" and $input[options_category]) {
					// Name
					$category = $input[options_category];
					if($category and !is_int_value($category)) $category = code_id('categories',$category);
					$input[name] = "__plugins[categories][".$category."]";
					
					// Value
					if($this->id and !x($input[value])) {
						$plugin_item = $this->item->plugin('categories');
						$input[value] = $plugin_item->categories($category);
					}
				}
				
				// File
				/*if(in_array($input[type],array("file"))) {
					// File Type
					if(!$input[file][type]) $input[file][type] = "file";
					// Path
					if(!$input[file][path]) $input[file][path] = m($this->module.'.uploads.types.'.$input[file][type].'.path');
					// Thumbs
					if(!x($input[file][thumbs])) $input[file][thumbs] = m($this->module.'.uploads.types.'.$input[file][type].'.thumbs');
					else if(is_array($input[file][thumbs])) $input[file][thumbs] = array_merge_associative(m($this->module.'.uploads.types.'.$input[file][type].'.thumbs'),$input[file][thumbs]);
					// Extensions
					if(!x($input[file][extensions])) $input[file][extensions] = m($this->module.'.uploads.types.'.$input[file][type].'.extensions');
					// Accept
					if(!x($input[validate][accept])) $input[validate][accept] = implode(",",g('config.uploads.types.'.$input[file][type].'.accept'));
					// Browse
					if(!x($input[file][browse][active])) $input[file][browse][active] = (u('admin') ? 1 : 0);
					if(!class_exists('file')) $input[file][browse][active] = 0;
					if($input[file][browse][active] == 1) {
						if(!$input[file][browse][path]) $input[file][browse][path] = $input[file][path];
						if(!$input[file][browse][type]) $input[file][browse][type] = $input[file][type];
						if(!$input[row][id]) $input[row][id] = "file-row-".random();
						if(!$input[file][browse][row]) $input[file][browse][row] = "#".$input[row][id];
					}
				}*/
				// Collapsed
				/*if($input[type] == "collapsed") {
					$r = random();
					$input[value] = NULL;
					// End last collapsed section
					if($this->collapsed) {
						$input[value] .= "
	</div>
</div>";
					}
					// Start new collapsed section
					$input[value] .= "
<div class='form-collapsible form-collapsible-".$r."'>
	<div class='form-collapsible-heading'>
		<a href='javascript:void(0);' onclick=\"toggle('#form-collapsible-".$r."-content',{animation:'',heading:'form-collapsible-".$r."-heading'});\" class='toggle-".($input[open] ? "open" : "closed")."' id='form-collapsible-".$r."-heading'>".$input[label][value]."</a>
	</div>
	<div class='form-collapsible-content' id='form-collapsible-".$r."-content' style='".($input[open] ? "display:block;" : "display:none;")."'>";
					// Store that we're in a collapsed section
					$this->collapsed = 1;
					
					$input[type] = "html";
					$input[label] = NULL;
					$input[name] = NULL;
				}
				// End Collapsed
				if($input[type] == "collapsed_end") {
					$input[type] = "html";
					$input[label] = NULL;
					$input[name] = NULL;
					if($this->collapsed) $input[value] = "
	</div>
</div>";
					$this->collapsed = 0;
				}*/
			}
			
			// Parent
			return parent::input_standardize($input);
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function input_html($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				// Value - saved
				if($this->values and $input->name and $input->process) {
					$input->value = $this->value($this->values,$input->name);
					$input->value_temp = $input->value; // Used to use 'input_update() to do this, not sure if this will update it in $this->inputs or not
				}
				// HTML
				$html = $input->html_field($this,$c);
			}
			// Old method
			else {
				// Standardize - did in render(), but just in case we call this method externally
				$input = $this->input_standardize($input);
				
				// Add
				/*if($input[add] and $input[options_module] /*and $_GET['ajax_action'] != "form_add_option"*//*) {
					// Input
					$input[add] = 0;
					$html_element = $this->input_html($input);
					$html = "
	<div id='form-add-field-".str_replace(array('[',']'),'',$input[name])."' class='form-add-field".($input[type] == "select" ? " form-add-field-inline" : "")."'>";
					if($html_element) $html .= "
		".$html_element;
					else {
						$html .= "
		<em>No ".strtolower(m($input[options_module].'.plural',$input[options_type]))." found</em>";
						if($input[required]) $html .= "
		<input class='required required-input' type='text' name='".$input[name]."' style='width:0px;height:0px;padding:0px;margin:0px;visibility:hidden' />";
					}
					$html .= "
	</div>";
					// Add
					$url = D."?ajax_action=form_add_option&amp;ajax_module=".$input[options_module]."&amp;type=".$input[options_type]."&amp;form_module=".$this->module."&amp;".http_build_query(array('input' => $input));
					if($_GET['ajax_action'] == "form_add_option") $url .= "&amp;".http_build_query(array('parent' => $_GET));
					if($input[type] != "select") $html .= "
	<div class='clear'></div>";
					$html .= "
	<div class='form-add-button".($input[type] == "select" ? " form-add-button-inline" : "")."'>
		<a href='".$url."' class='i i-add".($input[type] != "select" ? " i-text" : " i-inline")." overlay {width:430,height:320}' title='".string_encode($input[label][value])."'>".($input[type] != "select" ? "New ".$input[label][value] : "")."</a>
	</div>
	<div class='clear'></div>";
	
					$input[value] = $html;
					$input[type] = "html";
				}
				
				// File
				/*if($input[type] == "file") {
					// Browse
					$browse = $this->file_browse($input);
					if($browse) {
						// Icon
						$input[append] .= $browse;
						
						// HTML
						$html = parent::input_html($input,$c);
					
						// Preview wrapper
						if(strstr($html,'form-preview')) {
							$html = preg_replace("/<div([^>]+)class='form-preview/","<div class='form-browse-preview'>$0",$html,1);
						}
						else {
							$html .= "<div class='form-browse-preview'>";	
						}
						$html .= "</div>";
						
						// Return
						return $html;
					}
				}*/
			}
			
			// Parent
			return parent::input_html($input,$c);
		}
		
		/**
		 * Renders the HTML for an input and its label.
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input and label.
		 */
		function input_render($input,$c = NULL) {
			// New method
			if(is_object($input)) {
				// Types
				if($input->types) {
					if(!in_array(u('type'),$input->types)) {
						$input = form_input_hidden::load($input->name,$input->value,$input->c_duplicate());
					}
				}
			}
			// Old method
			else {
				// Types
				if($input[types]) {
					if(!in_array(u('type'),$input[types])) $input[type] = "hidden";
				}
			}
			
			// Parent
			$html = parent::input_render($input,$c);
			
			// Return
			return $html;
		}
		
		/**
		 * Processes bracket data in an input's configuration.
		 *
		 * @param string $string The string you want to process the bracket data of.
		 * @return string The resulting value of the bracket data.
		 */
		function brackets($string = NULL) {
			if($string) {
				if(substr($string,0,1) == "{" and substr($string,-1) == "}") {
					$string = substr($string,1);
					$string = substr($string,0,strlen($string) - 1);
					$parts = explode('.',$string);
					
					// {input.user_password}
					if($parts[0] == "input") {
						foreach($this->inputs as $k => $v) {
							// New method
							if(is_object($v)) {
								if($v->name == $parts[1]) $string = $v->value;
							}
							// Old method
							else {
								if($v[name] == $parts[1]) $string = $v[value];
							}
						}
					}
					// {GET.client_id}
					if($parts[0] == "GET") {
						$string = $_GET[$parts[1]];
					}
					// {POST.client_id}
					if($parts[0] == "POST") {
						$string = $_GET[$parts[1]];
					}
				}
			}
			
			// Return
			return $string;
		}
		
		/**
		 * Returns an array of non-attributes.
		 *
		 * Non-attriubutes are usually configuration vales (for an input, field, row, or entire form) which aren't key/value pair attributes (or one's we'll manually set).
		 *
		 * @return array An array of non-attributes.
		 */
		function nonattributes() {
			// Core
			$array_core = parent::nonattributes();
			
			// Framework
			$array_framework = array(
				'plugins',
				'plugin',
				'submit',
				'redirect',
				'options_module',
				'options_type',
				'options_category',
				'options_country',
				'types',
				'add',
				'open'
			);
			
			// Merge
			$array = array_merge($array_core,$array_framework);
			
			// Return
			return $array;
		}
		
		/**
		 * Renders a framework form, automatically setting missing values and adding in the submit button and hidden fields.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the rendered form.
		 */
		function render($c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
				
			// Type
			$type_needed = 0;
			$type_key = m($this->module.'.db.type',$this->type);
			if($type_key and $types = m($this->module.'.types') and !$this->input_exists($type_key)) {
				// Only one type
				if(count($types) == 1) $this->type = array_first_key($types);
				// Defined type
				if($this->type and $types[$this->type]) $this->hidden($type_key,$this->type);
				// Must choose type
				else {
					// Type needed
					$type_needed = 1;
					// Change method to GET
					$this->c[method] = 'GET';
					// Not going to process (don't want to pass __form)
					$this->c[process] = 0;
					// Clear old inputs
					$this->inputs = NULL;
					// No 'collapsed'
					$this->collapsed = 0;
					
					// Types
					$options = array('' => '- Select a Type -');
					foreach($types as $k => $v) $options[$k] = $v[single];
					$this->select('Type','type',$options,NULL,array('onchange' => 'this.form.submit();'));
				}
			}
			
			// Have inputs
			if($this->inputs) {
				// Collapsed?
				$collapsed = 0;
				foreach($this->inputs as $input) {
					if($input->type == "collapsed") $collapsed = 1;
					if($input->type == "collapsed_end") $collapsed = 0;
				}
				if($collapsed) $this->collapsed_end();
				
				if(!$type_needed) {
					// Plugins
					if($this->c[plugins]) {
						$plugins = $this->plugins();
						if($plugins) {
							// Start columns - append to beginning of arrays
							$input = form_input_html::load("<div class='form-framework'><div class='form-left'>");
							array_unshift($this->inputs,$input);
						
							// Column separator
							$this->html("</div><div class='form-right'>");
							
							// Plugins
							foreach($plugins as $k => $v) {
								// Visible - has non-hidden inputs
								$visible = 0;
								foreach($v[inputs] as $input) {
									if(is_object($input)) {
										if($input->type != "hidden") $visible = 1;	
									}
									else {
										if($input[type] != "hidden") $visible = 1;	
									}
									if($visible) break;
								}
								
								// Start collapsible
								if($visible) $this->collapsed($v[label],array('open' => $v[open]));
							
								// Sidebar inputs
								$this->inputs($v[inputs]);
								
								// End collapsible
								if($visible) $this->collapsed_end();
							}
						
							// End columns
							$this->html("</div></div><div class='clear'></div>");
						}
					}
					
					// Submit
					if($this->c[submit]) $this->submit($this->c[submit][value],$this->c[submit]);
					
					// Hidden fields
					if($this->c[process]) {
						if($this->module) {
							if(!$this->input_exists('process_action')) $this->hidden('process_action','save',array('process' => 0));
							if(!$this->input_exists('process_module')) $this->hidden('process_module',$this->module,array('process' => 0));
							if($this->type) {
								if(!$this->input_exists('process_type')) $this->hidden('process_type',$this->type,array('process' => 0));
							}
						}
					}
				}
				
				// HTML
				$html = parent::render();
				
				// Hash
				$html = str_replace('{hash}',$this->hash,$html);
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Process a submitted framework form.
		 *
		 * @param array $post The array of submitted form data.
		 * @param array $files The array of submitted file data. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of result data including module, id, and values.
		 */
		function process($post,$files = NULL,$c = NULL) {
			// Serialize
			$serialize = $this->c['serialize'];
			$this->c['serialize'] = $c['serialize'] = 0; // We'll do it in this method if we need to (not in parent)
			
			// Process
			$results = parent::process($post,$files,$c);
			
			// Item - want most current one, not one saved in form instance
			$this->item = item::load($this->module,$this->id);
			
			// Values
			/*foreach($results[values] as $k => $v) {
				// Core variable
				if(substr($k,0,2) == "__") {
					$this->core[$k] = $v;
					unset($results[values][$k]);
				}
			}*/
			
			// Values - plugins - files
			foreach($this->inputs as $input) {
				// New method
				if(is_object($input)) {
					# do nothing, already handled this in $form_input_file_framework->process() if it was a file input
				}
				// Old method
				else {
					// Type
					$type = ($input[type_process] ? $input[type_process] : $input[type]);
					/*if($type == "file" and $input[process] and $input[name] and $input[file][path]) {
						// Have a file..
						if($value = $this->value($results[values],$input[name])) {
							// Values - from form inputs (ex: file_thumb)
							$results[values][__plugins][files][$input[name]] = $this->post[__plugins][files][$input[name]];
							
							// Values - new file
							if($results[values_files][$input[name]][name]) {
								$results[values][__plugins][files][$input[name]][file_type] = $results[values_files][$input[name]][type];
								$results[values][__plugins][files][$input[name]][file_path] = $results[values_files][$input[name]][path];
								$results[values][__plugins][files][$input[name]][file_extension] = $results[values_files][$input[name]][extension];
								$results[values][__plugins][files][$input[name]][file_width] = $results[values_files][$input[name]][width];
								$results[values][__plugins][files][$input[name]][file_height] = $results[values_files][$input[name]][height];
								$results[values][__plugins][files][$input[name]][file_length] = $results[values_files][$input[name]][length];
								$results[values][__plugins][files][$input[name]][file_size] = $results[values_files][$input[name]][size];
								$results[values][__plugins][files][$input[name]][file_extensions] = $results[values_files][$input[name]][extensions];
								$results[values][__plugins][files][$input[name]][file_thumbs] = $results[values_files][$input[name]][thumbs];
								$results[values][__plugins][files][$input[name]][file_storage] = $results[values_files][$input[name]][storage];
							}
								
							// Values - thumb - if none selected, use first thumb
							if(!$results[values][__plugins][files][$input[name]][file_thumb]) {
								if($thumbs = $results[values_files][$input[name]][thumbs]) {
									foreach($thumbs as $k => $v) {
										$results[values][__plugins][files][$input[name]][file_thumb] = array_first_key($v);
										break;
									}
								}
							}
							
							// Values - basic - only needed if we have some values to save, if not, we simply don't save it at all
							if(x($results[values][__plugins][files][$input[name]])) {
								$results[values][__plugins][files][$input[name]][file_column] = $input[name];
								$results[values][__plugins][files][$input[name]][file_name] = $value;
							}
							// Nothing's changed, don't save
							else {
								unset($results[values][__plugins][files][$input[name]]);
							}
						}
						// No file, delete plugin row entry
						else $results[values][__plugins][files][$input[name]] = array();
					}*/
				}
			}
			
			// Values - serialize
			$this->c['serialize'] = $serialize;
			foreach($results[values] as $k => $v) {
				if($k == "__plugins") { // Always want to serialize as that's how we expect them to come through when saving data
					foreach($v as $plugin => $plugin_values) {
						foreach($plugin_values as $plugin_values_k => $plugin_values_v) {
							if(is_array($plugin_values_v)) $results[values][$k][$plugin][$plugin_values_k] = data_serialize($plugin_values_v);
						}
					}
				}
				else if($this->c[serialize] and is_array($v)) {
					$results[values][$k] = data_serialize($v);
				}
			}
			
			// Values - items
			//print "/*************************************************/<br />";
			if($results[values][__items]) {
				$items = data_unserialize($results[values][__items]);
				//print "have items<br />";
				
				// Errors?
				//print "<b>errors</b><br />";
				foreach($items as $name => $modules) {
					//print "errors - sub items<br />";
					foreach($modules as $module => $ids) {
						foreach($ids as $id => $values) {
							//print "values:";
							//print_array($values);
							
							foreach($values as $k => $v) {
								if($k == "__items") {
									foreach($v as $name0 => $modules0) {
										if($items[$name][$module][$id][$name0] != "new" or $results[values][$name] != "new") {
											foreach($modules0 as $module0 => $ids0) {
												foreach($ids0 as $id0 => $values0) {
													foreach($values0 as $k0 => $v0) {
														$key = '__items['.$name.']['.$module.']['.$id.']['.$k.']['.$name0.']['.$module0.']['.$id0.']['.$k0.']';
														//print "unsetting ".$key." errors<br />";
														unset($results[errors][header][$key],$results[errors][inline][$key]);
													}
												}
											}
										}
									}
								}
								else if($results[values][$name] != "new") {
									$key = '__items['.$name.']['.$module.']['.$id.']['.$k.']';
									//print "unsettings ".$key." errors<br />";
									unset($results[errors][header][$key],$results[errors][inline][$key]);
								}
							}
						}
					}
				}
				if(!$results[errors][header]) unset($results[errors][header]);
				if(!$results[errors][inline]) unset($results[errors][inline]);
				//print "errors:";
				//print_array($results[errors]);
				
				if(!$results[errors]) {
					//print "<b>sub-items</b><br />";
					// Sub-items - do first so we can set the values correctly
					/* Example:
					 [__items] => Array (
						[card_id] => Array (
							[credit_cards] => Array (
								[new] => Array (
									[address_id] => new
									[__items] => Array (
										[address_id] => Array (
											[addresses] => Array (
												[new] => Array (
													[address_country] => 234
													[address_first_name] => Test
													[address_last_name] => Address 3
													...
												)
											)
										)
									)
									[card_number] => 4111111111111111
									...
								)
							)
						)
					)*/
					foreach($items as $name => $modules) {
						//print "name: ".$name."<br />";
						if($results[values][$name] == "new" or !$results[values][$name]) {
							//print "dropdown set to new<br />";
							foreach($modules as $module => $ids) {
								foreach($ids as $id => $values) {
									//print "values:";
									//print_array($values);
									if($values[__items]) {
										foreach($values[__items] as $name0 => $modules0) {
											if($items[$name][$module][$id][$name0] == "new") {
												foreach($modules0 as $module0 => $ids0) {
													foreach($ids0 as $id0 => $values0) {
														//print "sub-values:";
														//print_array($values0);
														//print "module: ".$module0.", id: ".$id0."<br />";
														$item0 = item::load($module0,$id0);
														$id0 = $item0->save($values0);
														$items[$name][$module][$id][$name0] = $id0;
														//print "sub-id: ".$id0."<br />";
													}
												}
											}
										}
									}
								}
							}
						}
					}
					// Items
					//print "<b>items</b><br />";
					foreach($items as $name => $modules) {
						//print "name: ".$name."<br />";
						if($results[values][$name] == "new" or !$results[values][$name]) {
							//print "dropdown set to new<br />";
							foreach($modules as $module => $ids) {
								foreach($ids as $id => $values) {
									//print "values:";
									//print_array($values);
									$item = item::load($module,$id);
									$id = $item->save($values);
									$results[values][$name] = $id;
									//print "id: ".$id."<br />";
								}
							}
						}
					}
					unset($results[values][__items]);
				}
			}
			//print "/*************************************************/<br />";
			
			// Values - store
			$this->values = $results[values];
			$this->values_files = $results[values_files];
			
			// Results
			$results[module] = $this->module;
			$results[id] = $this->id;
			//$results[core] = $this->core; // May deprecate if plugins works out well
			
			// Debug
			debug("\$".__CLASS__."->".__FUNCTION__."() results: ".return_array($results),$this->c[debug]);
			
			// Cache
			if($results[errors]) $this->cache_save(1);
			else $this->cache_delete();
			
			// Return	
			return $results;
		}

		/**
		 * Processes a file input in the submitted form.
		 *
		 * @param array $input The inputs array of data.
		 * @param string|array $value The value submitted. Either a file name or uploaded file array. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The resulting value (file name).
		 */
		/*function input_process_file($input,$value = NULL,$c = NULL) {
			// Browsed for file?
			$browsed = $this->value($this->post,'__file_browsed['.$input[name].']');
			if($browsed) {
				$f = new file($browsed,array('overwrite' => $input[file][overwrite],'debug' => $this->c[debug]));
				$input[	
			}	
		}
		
		/**
		 * Saves a the framework form's module item's values.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of result data including module, id, and values.
		 */
		/*function save($c = NULL) {
			// Config
			if($c) $this->c = array_merge($this->c,$c);
			
			// Debug
			debug("save()",$this->c[debug]);
			
			// Module
			if($this->module) {
				// Messsage (call before so we know if it's a new/existing item as $this->id will be set momentarily)
				if($this->id > 0) $message = 'Changes saved.';
				else $message = 'This '.strtolower(m($this->module.'.single',$this->type)).' has been successfully added.';
				
				// Files
				/*foreach($this->inputs as $input) {
					// Type
					$type = ($input[type_process] ? $input[type_process] : $input[type]);
					if($type == "file" and $input[process] and $input[name] and $input[file][path]) {
						// Have a file..
						if($value = $this->values[$input[name]]) {
							// Is a new value, save data
							if($this->values_files[$input[name]][name]) {
								// Thumb - none selected, use first thumb
								if($thumbs = $this->values_files[$input[name]][thumbs]) {
									foreach($thumbs as $k => $v) {
										$thumb = array_first_key($v);
										break;
									}
								}
								// Values
								$this->values[__plugins][files][$input[name]] = array(
									'file_column' => $input[name],
									'file_type' => $this->values_files[$input[name]][type],
									'file_name' => $value,
									'file_path' => $this->values_files[$input[name]][path],
									'file_extension' => $this->values_files[$input[name]][extension],
									'file_width' => $this->values_files[$input[name]][width],
									'file_height' => $this->values_files[$input[name]][height],
									'file_length' => $this->values_files[$input[name]][length],
									'file_size' => $this->values_files[$input[name]][size],
									'file_extensions' => $this->values_files[$input[name]][extensions],
									'file_thumbs' => $this->values_files[$input[name]][thumbs],
									'file_thumb' => $thumb,
								);
							}
							// Existing file, may have made changes (namely, the file_thumb value)
							else if($this->post[__plugins][files][$input[name]]) {
								$this->values[__plugins][files][$input[name]] = $this->post[__plugins][files][$input[name]];
							}
						}
						// No file, delete plugin row entry
						else $this->values[__plugins][files][$input[name]] = array();
					}
				}*/
				
				/*// Save
				$this->id = $this->item->save($this->values,array('debug' => $this->c[debug]));
			}
			
			// Clear saved form
			$this->cache_delete();
			
			// Results
			$results = array(
				'module' => $this->module,
				'id' => $this->id,
				'redirect' => $this->c[redirect],
				'message' => $message,
				'values' => $this->values,
				'values_files' => $this->values_files,
				//'core' => $this->core,
				'post' => $this->post,
				'files' => $this->files,
				'c' => $this->c
			);
			debug("save() results: ".return_array($results),$this->c[debug]);
			return $results;
		}
		
		/**
		 * Returns array of plugin sidebar boxes where users can edit the item's related information.
		 *
		 * @return array The plugins that apply to the current item with current values stored.
		 */
		function plugins() {
			// Form specific
			$plugins = $this->plugins;
			
			// Config
			$c = $this->c;
			$c[type] = $this->type;
			
			// Global
			if($module_plugins = m($this->module.'.plugins',$this->type)) {
				foreach($module_plugins as $plugin => $active) {
					if($active) {
						// Already in form?
						$exists = 0;
						foreach($this->inputs as $input) {
							// New method
							if(is_object($input)) {
								if($input->plugin == $plugin) {
									$exists = 1;	
								}
							}
							// Old method
							else {
								if($input[plugin] == $plugin) {
									$exists = 1;	
								}
							}
						}
						// No, we can add
						if(!$exists) {
							// Inputs
							$plugin_instance = plugin::load($this->module,$this->id,$plugin,$c);
							$plugin_sidebar = $plugin_instance->sidebar($this);
								
							// Sidebar
							if($plugin_sidebar[inputs]) {
								// Input names
								if(plugin($plugin.'.db.table')) {
									foreach($plugin_sidebar[inputs] as $k => $v) {
										// Convert to new method (if we can)
										if(!is_object($v)) {
											if($_v = $this->input_upgrade($v[label],$v[type],$v[name],$v[value],$v)) {
												$v = $_v;
											}
										}
																
										// New method
										if(is_object($v)) {
											if($v->name) {
												$name = array_string($v->name);
												$name = string_array("__plugins.".$plugin.".".$name);
												$v->name = $name;
											}
										}
										// Old method
										else {
											if($v[name]) {
												$name = array_string($v[name]);
												$name = string_array("__plugins.".$plugin.".".$name);
												$v[name] = $name;	
											}
										}
										
										// Store
										$plugin_sidebar[inputs][$k] = $v;
									}
								}
								
								// Store
								$plugins[$plugin] = $plugin_sidebar;
							}
						}
					}
				}
			}
			
			// Return
			return $plugins;	
		}
		
		/**
		 * Creates and returns the HTML for previewing a previously uploaded file.
		 *
		 * @param array $input The input's array of information including file path and value.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the file preview.
		 */
		/*function file_preview($input,$c = NULL) {
			// Value
			if($input[value] and $input[file][path] or $c[browse]) {
				// Speed
				$f_r = function_speed(__CLASS__."->".__FUNCTION__);
				
				// Config
				if(!x($c[delete])) $c[delete] = 1; // Show delete icon for removing the file
				if(!x($c[crop])) $c[crop] = 1; // Show crop icon for cropping the file (images only)
				if(!x($c[rotate])) $c[rotate] = 1; // Show rotate icons for rotating the file left/right (images only)
				if(!x($c[browse])) $c[browse] = NULL; // Full path to file we selected via file browser
				
				// Random
				$r = "preview".random();
			
				// File - browse
				if($c[browse]) {
					$storage = "local";
					$file = $c[browse];
					$input[value] = file::name($file);
				}
				// File - saved
				else {
					// File - files plugin
					if($this->item->id) {
						$f = $this->item->file($input[name]);
						$file = $f->file['class']->file;
						$storage = $f->file['class']->storage;
					}
					// File - default
					if(!$file) {
						$storage = $input[file][storage];
						$file = $input[file][path].$input[value];
					}
				}
			
				// File class
				if(class_exists('file')) {
					$f = file::load($file,array('storage' => $storage));
					$type = $f->type;
				}
	
				// Container
				$text .= "
<div id='".$r."' class='form-preview".($type ? " form-preview-".$type : "")."'>";

				// Text
				$text .= "
	<div class='form-preview-keep'>Leave this field blank to keep the existing file.</div>
	<div class='clear'></div>";
	
				// Display
				$text .= "
	<div class='form-preview-media'>";
			
				// Display - advanced
				if($f) {
					// Image
					if($type == "image") {
						// Thumb
						$file_thumb = NULL;
						
						// Via file browser
						if($c[browse]) {
							if($input[file][thumbs][s][path]) {
								$file_thumb = file::load($input[file][thumbs][s][path].$input[value],array('storage' => $input[file][thumbs][s][storage]));
							}
						}
						// Saved file
						else if($input[file][thumbs]) {
							$file_thumb = $this->item->file($input[name],array('thumb' => 's'));
						}
						
						// Thumb
						if($file_thumb->exists) {
							$url = $f->url();
							$text .= "
		".$file_thumb->image_html(array('attributes' => " target='_blank'",'link' => $url,'link_attributes' => " class='overlay'"));
						}
						// No thumb, use original (shrunk)
						else {
							$text .= "
		".$f->image_html(array('max_width' => 100,'max_height' => 100,'attributes' => " target='_blank'",'link' => $f->url(),'link_attributes' => " class='overlay'"));
						}
					}
					// Video
					else if($type == "video") {
						// Via file browser
						if($c[browse]) {
							// Config
							$video_c = array(
								'max_width' => 265,
								'auto' => 0,
							);
							// Extensions
							if($input[file][extensions]) {
								foreach($input[file][extensions] as $extension => $v) {
									$f_extension = file::load($v[path].str_replace(".".$f->extension(),".".$extension,$input[value]));
									if($f_extension->exists) {
										// Same as original, don't want 2, don't add as fallback
										if($f_extension->extension == $f->extension) {
											$f = $f_extension; // Use converted video if one exists as it'll have all necessary tweaks/headers
										}
										// Different than original, add as fallback
										else {
											$video_c[fallback][] = $f_extension->file;	
										}
									}
								}
							}
							// Video
							$text .= "
		".$f->video_html($video_c);
		
							// Thumbs
							$query = "SELECT * FROM items_files WHERE file_path = '".a($input[file][path])."' AND file_name = '".a($input[value])."' AND file_thumbs != '' LIMIT 1";
							$row = $this->db->f($query);
							$thumbs = data_unserialize($row[file_thumbs]);
							$thumbs = $thumbs[t];
							if($thumbs) {
								$thumbs_count = count($thumbs);
								$thumb_selected = $row[file_thumb];
								$text .= "
		<table class='form-preview-video-thumbs'>
			<tr>";
								foreach($thumbs as $k => $v) {
									$file_video_thumb = file::load($v[path].$v[name],array('storage' => $v[storage]));
									if($file_video_thumb->exists) $text .= "
				<td width='".round(100 / $thumbs_count)."' align='center' valign='top'>
					<img src='".$file_video_thumb->url()."' />
					<input type='radio' name='__plugins[files][".string_encode($input[name])."][file_thumb]' value='".$k."'".($thumb_selected == $k ? " checked='checked'" : "")." />
				</td>";
								}
								$text .= "
			</tr>
		</table>";
							}
						}
						// Saved file
						else {
							// Video
							$file_video = $this->item->file($input[name],array('extension' => array('mp4','flv')));
							$text .= "
		".$file_video->video_html(array('max_width' => 265,'width' => $file_video->width(),'height' => $file_video->height(),'poster' => $file_video->thumb_url(),'auto' => 0));
		
							// Thumbs
							if($thumbs = $file_video->thumbs('t')) {
								$thumbs_count = count($thumbs);
								$thumb_selected = $file_video->thumb('t');
								$text .= "
		<table class='form-preview-video-thumbs'>
			<tr>";
								foreach($thumbs as $k => $v) {
									$file_video_thumb = file::load($v[path].$v[name],array('storage' => $v[storage]));
									if($file_video_thumb->exists) $text .= "
				<td width='".round(100 / $thumbs_count)."' align='center' valign='top'>
					<img src='".$file_video_thumb->url()."' />
					<input type='radio' name='__plugins[files][".string_encode($input[name])."][file_thumb]' value='".$k."'".($thumb_selected[key] == $k ? " checked='checked'" : "")." />
				</td>";
								}
								$text .= "
			</tr>
		</table>";
							}
						}
					}
					// Audio
					else if($type == "audio") $text .= "
		".$f->audio_html();
					// File
					else $text .= "
		<a href='".$f->url()."' target='_blank'>".basename($file)."</a>";
				}
				// Display - simple
				else {
					// URL
					$url = str_replace(SERVER,DOMAIN,$file);
					// File
					if($url) $text .= "
		<a href='".$url."' target='_blank'>".basename($file)."</a>";
					else $text .= basename($file);
				}
				
				$text .= "
	</div>";
	
				// Icons
				$text .= "
	<div class='form-preview-icons'>";
	
				// Delete
				if($c[delete]) {
					// Deleted input name
					$name = $input[name];
					$name = preg_replace('/\[/','][',$name,1); // Add first closing bracket bracket. Example: __plugins[menu][item_image] becomes __plugins][menu][item_image]
					$name = "__file_deleted[".$name;
					if(substr($name,-1) != "]") $name .= "]";
					
					// Required element selector
					if($input[id])  $selector = "#".$input[id].".required-input";
					else $selector = ".required-input:input[name=".$input[name]."]"; // !!! This needs to be updated to work with a multilevel input names
					
					$text .= "
		<script type='text/javascript'>
		function deleteFile".$r."() {
			if(confirm('Are you sure you want to delete this file?')) {
				$('#".$r."').html(\"<input type='hidden' name='".$name."' value='1' />\");
				$('".$selector."').addClass('required'); // Re-require
			}
		}
		</script>
		<a href='javascript:void(0);' onclick='deleteFile".$r."();' class='i i-clear i-inline tooltip core-hover' title='Delete'></a>";
				}
			
				/*if($type == "image") {
					// Image - Crop
					if($c[crop] == 1 and g('config.javascript.admin.jquery\.jcrop\.js')) $text .= "
		<a href='".D."?ajax_action=cropIt&amp;module=".$module."&amp;id=".$id."&amp;file=".urlencode($url)."&amp;div=".$r."' class='i i-crop i-inline tooltip overlay' title='Crop'></a>";
	
					// Image - Rotate
					if($c[rotate] == 1) {
					$oncomplete = "function(image){\$('#".$module."-".$id."-image-".$f."').closest('a').attr('href',str_replace('/".$c[size]."/','/".$c[size_large]."/',image));}";
					$text .= "
		<a href='javascript:void(0);' onclick=\"rotate('".$module."-".$id."-image-".$f."',270,{vars:'module=".$module."&id=".$id."&size=".$c[size]."','loading':1,oncomplete:".$oncomplete."});\" class='i i-rotate-left i-inline tip' title='Rotate Left'></a>
		<a href='javascript:void(0);' onclick=\"rotate('".$module."-".$id."-image-".$f."',90,{vars:'module=".$module."&id=".$id."&size=".$c[size]."','loading':1,oncomplete:".$oncomplete."});\" class='i i-rotate-right i-inline tip' title='Rotate Right'></a>";
					}
				}*/
			
				/*$text .= "
	</div>
	<div class='clear'></div>";

				// AJAX
				if($c[browse]) $text .= "
	<input type='hidden' name='".$input[name]."' value='".string_encode($c[browse])."' />";
	
				$text .= "
</div>";
				// Speed
				function_speed(__CLASS__."->".__FUNCTION__,$f_r);
				
				// Return
				return $text;
			}
		}
		
		/**
		 * Returns HTML for displaying icon to browse previously uploaded files via the file browser.
		 *
		 * @param array $input The array of data for the input
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML for the browse icon.
		 */
		/*function file_browse($input,$c = NULL) {// Browse
			// Active
			if($input[file][browse][active] and $input[file][browse][path] and $input[file][browse][row]) {
				// Icon
				$icon = "<a href='".D."?ajax_action=file_browse&amp;directory=".urlencode($input[file][browse][path])."&amp;type=".$input[file][browse][type]."&amp;".http_build_query(array('input' => $input))."&amp;row=".urlencode($input[file][browse][row])."' class='overlay i i-search i-inline' title='File Browser'></a>";
			}
			
			// Return
			return $icon;	
		}
		
		/**
		 * Deletes previous file after new one is uploaded.
		 *
		 * @param array $input The input's array of information including file path and old value.
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function file_delete($input,$c = NULL) {
			// Delete by plugin
			if($this->module and $this->id > 0 and m($this->module.'.plugins.files') and $input[name]) {
				// Plugin
				$plugin = plugin::load($this->module,$this->id,'files');
				// Plugin item row
				$plugin_item_row = $plugin->db->f($plugin->rows(array('where' => "file_column = '".$input[name]."'")));
				// Plugin item
				$plugin_item = $plugin->plugin_item($plugin_item_row);
				if($plugin_item->id) {
					// Delete // This already happens when we save the item: $item->save() -> $plugin->item_delete() -> $plugin_item->delete()
					//$plugin_item->delete(array('debug' => $this->c[debug]));	
					debug("Not deleting ".$plugin_item->v('file_path').$plugin_item->v('file_name').", it'll be deleted when we save the results for this item",$this->c[debug]);
					
					// Return - deleting will happen when we save the item
					return;
				}
			}
			
			// Parent
			parent::file_delete($input,$c);
		}
		
		/**
		 * Helper for adding a collapsible section to the form.
		 *
		 * Same as $this->input(form_input_collapsed_framework::load($label,$c));
		 *
		 * !!! Deprecated. Use $form->input(form_input_collapsed_framework::load($label,$c)); instead. !!!
		 *
		 * @param string $label The label of the input
		 * @param array $c An array of config values. Default = NULL
		 */
		function collapsed($label,$c = NULL) {
			$this->input(form_input_collapsed_framework::load($label,$c));
		}
		
		/**
		 * Helper for ending a collapsible section in the form.
		 *
		 * Same as $this->input(form_input_collapsed_end_framework::load($c));
		 *
		 * !!! Deprecated. Use $form->input(form_input_collapsed_end_framework::load($c)); instead. !!!
		 *
		 * @param array $c An array of config values. Default = NULL
		 */
		function collapsed_end($c = NULL) {
			$this->input(form_input_collapsed_end_framework::load($c));
		}
		
		/**
		 * Adds an 'add' button to the form for dynamically adding a form input.
		 *
		 * @param string $value The value of the 'add' button.
		 * @param array $input The array of input information for the input you want to dynamically add.
		 * @param array $c An array of config values. Default = NULL
		 */
		/*function add($value,$input,$c = NULL) {
			// Error
			if(!$input) return;
			
			// Config
			if(!$c[element]) { // The jQuery selector for the element you want to append the input to
				if(!$c[id]) $c[id] = "form-add-".random(); // If no 'element', we add id just before this input
			}
			if(!$c[url]) $c[url] = D."?ajax_action=form_add_input";
			$c['class'] .=  ($c['class'] ? " " : "")."form-add"; // Add a class just to differentiate it from other buttons
			
			// Onclick
			$onclick = "
$.ajax({
	type:'POST',
	url:'".$c[url]."',
	data:'form={hash}&".http_build_query(array('input' => $input))."',
	success: function(result) {
		debug('<xmp>'+result+'</xmp>');";
			if($c[element]) $onclick .= "
		$('".$c[element]."').append(result);";
			else $onclick .= "
		$('#".$c[id]."').closest('.form-row-button').before(result);";
			$onclick .= "
	}
});";
			$c[onclick] = string_strip_breaks($onclick);
			unset($c[element]);
			
			// Button
			$this->button($value,$c);
		}*/
	}
}
?>