<?php
if(!class_exists('plugin_item',false)) {
	/**
	 * Class for managing plugin values for a plugin item.
	 *
	 * @package kraken
	 */
	class plugin_item extends plugin {
		// Variables
		var $plugin_id; // string
		var $row, $database = array(); // array
		public $row_original = array(); // Holds the original version of any changed values (unencrypted, translated, etc.)
		
		/**
		 * Loads and returns an instance of either the core plugin class or (if it exists) the plugin specific plugin class.
		 *
		 * Example:
		 * - $plugin_item = plugin_item::load($module,$id,$plugin,$plugin_id,$c);
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core plugin_item class or (if it exists) the plugin specific plugin_item class.
		 */
		static function load($module,$id,$plugin,$plugin_id,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_plugin(__CLASS__,$params,$module,$id,$plugin,$c); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$id,$plugin,$plugin_id,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$plugin_id,$c = NULL) {
			self::plugin_item($module,$id,$plugin,$plugin_id,$c);
		}
		function plugin_item($module,$id,$plugin,$plugin_id,$c = NULL) {
			// Config
			if(m('languages') and plugin('international') and plugin($plugin.'.plugins.international') and strpos(URL,'/admin') === false) { // Language to display the data in
				if(!$c[language]) {
					$c[language] = $_SESSION['__modules']['languages']['selected']; 
					if(!$c[language]) $c[language] = m('languages.settings.languages.default');
				}
			}
			else $c[language] = NULL;
			//if(!x($c[cached])) $c[cached] = 1; // Get cached data (if available) // Defaults in plugin class (parent)
			//if(!x($c[cache])) $c[cache] = 1; // Cache data for future use // Defaults in plugin class (parent)
			
			// Parent
			parent::__construct($module,$id,$plugin,$c);
			
			// ID
			if($plugin_id) {
				// Passed an array of item's db row
				if(is_array($plugin_id)) {
					$this->row = $plugin_id;
					$plugin_id = $this->row[plugin($plugin.'.db.id')];
					$cache = 1;
				}
				
				if($plugin_id > 0) {
					// Set ID
					$this->plugin_id = $plugin_id;
					
					// Module / ID
					if(!$this->module or !$this->id) {
						$this->module = $this->db('parent_module');
						$this->id = $this->db('parent_id');
						if($this->module or $this->id) {
							// Reconstruct
							parent::__construct($this->module,$this->id,$this->plugin,$c);
						}
					}
				
					// Language - probably a more elegant way to do this (so we're not copying code we use in row()), but quick fix for now
					// Note, we do this lower than we do in item and check $cache. Basically we want to make sure we've gotten $this->module and $this-> id already before rnning htis. if($cache) essentially tells us if the row was passed to us, in which casae we need to manually get international values.
					if($cache and $c[language] and $c[language] != m('languages.settings.languages.default')) {
						$rows = $this->db->q("SELECT * FROM items_international WHERE module_code = '".$this->module."' AND item_id = '".$this->id."' AND plugin_code = '".$this->plugin."' AND plugin_id = '".$this->plugin_id."' AND language_code = '".a($c[language])."'");
						while($row = $this->db->f($rows)) {
							$this->row[$row[item_column]] = $row[international_value];
						}
					}
			
					// Cache
					if($cache) $this->cache_save();
				}
			}
		}
		
		/**
		 * Returns the item's stored value for the given database column.
		 *
		 * @param string $column The column in the item's database table you want to get the value of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The value stored in the database for the given column for this item.
		 */
		function v($column,$c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id or !$column) return;
			
			// Config
			if(!x($c[translate])) $c[translate] = 1; // Return translated version of the value (if user's selected language is different than the default language)
			
			// Row
			if(!$this->row) $this->row();
			if($this->row) {
				// Value
				if($c[translate]) $value = $this->row[$column];
				else {
					$value = $this->row_original[$column]; // Get original value (only exists if there are other versions of the value, ex: translated version)
					if(!x($value)) $value = $this->row[$column]; // Didn't have an 'original' value, meaninig it never changed, use $this->row
				}
				
				// Return
				return $value;
			}
		}
		
		/**
		 * Returns the item's stored value for the given global database variable key.
		 *
		 * For example, if the module had defined the database variable of 'name' as connecting to the 'photo_name' column, $item->db('name') would return the item's photo_name value.
		 *
		 * @param string $key The global database variable key you want to get the corresponding value of for this item.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The value stored in the database column which corresponds to the given gloabal database variable key.
		 */
		function db($key,$c = NULL) {
			// Error
			if(/*!$this->module or !$this->id or */!$this->plugin or !$this->plugin_id or !$key) return; // Sometimes just have plugin/plugin_id, get module/id from that.
			
			// Config
			if(!x($c[translate])) $c[translate] = 1; // Return translated version of the value (if user's selected language is different than the default language)
			
			// Row
			if(!$this->row) $this->row();
			if($this->row) {
				// Get saved value // Skipping caching for now, causes more headaches (refresh()) than it probably solves
				/*if(x($this->database[$key])) {
					return $this->database[$key];
				}
				// Get new value
				else {*/
					// Row
					$row = $this->row;
					if(!$c[translate] and $this->row_original) $row = array_merge($row,$this->row_original);
					
					// Key
					$_key = plugin($this->plugin.'.db.'.$key);
					if($_key) {
						// Value
						if(strstr($_key,' ')) $value = string_format($_key,$row);
						else $value = $row[$_key];
					}
					// Missing (but is a core definition)
					else if(g('framework.db.'.$key)) {
						// Defaults to true
						if(g('framework.db.'.$key.'.default') == 1) {
							$value = 1;
						}
					}
				
					// Save
					//$this->database[$key] = $value;
					
					// Return
					return $value;
				//}
			}
		}
		
		/**
		 * Gets plugin row's array of database column values.
		 *
		 * @return array The plugin row's array of columns/values.
		 */
		function row() {
			// Error
			if(/*!$this->module or !$this->id or */!$this->plugin or !$this->plugin_id) return; // Sometimes just have plugin/plugin_id, get module/id from that.
			// Data stored locally
			if(!plugin($this->plugin.'.db.table')) return;
			
			// Already got
			if($this->row) {
				return $this->row;	
			}
			
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Cached?
			$cache = $this->cache_get();
			if($cache->row) {
				// Restore
				foreach($cache as $k => $v) $this->$k = $v;
				
				// Speed
				function_speed(__CLASS__."->".__FUNCTION__,$f_r,1);
				
				// Return
				return $this->row;
			}
			
			// Default
			if(plugin($this->plugin.'.db.table') and plugin($this->plugin.'.db.id')) {
				// Select
				$query = "SELECT * FROM `".plugin($this->plugin.'.db.table')."` WHERE ".plugin($this->plugin.'.db.id')." = '".$this->plugin_id."'";
				debug($query,$this->c[debug]);
				$this->row = $this->db->f($query);
				
				// Language
				if($this->c[language] and $this->c[language] != m('languages.settings.languages.default')) {
					$rows = $this->db->q("SELECT * FROM items_international WHERE module_code = '".$this->module."' AND item_id = '".$this->id."' AND plugin_code = '".$this->plugin."' AND plugin_id = '".$this->plugin_id."' AND language_code = '".a($this->c[language])."'");
					while($row = $this->db->f($rows)) {
						$this->row[$row[item_column]] = $row[international_value];
					}
				}
			}
			
			// Success
			$this->plugin_id = $this->row[plugin($this->plugin.'.db.id')];
			if($this->plugin_id) {
				// Cache
				$this->cache_save();
			}
				
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $this->row;
		}
		
		/**
		 * Returns the form class object for adding a plugin item in this plugin.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form class object with fields pre-added in.
		 */
		function form($c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin) return;
			
			// Parent
			$form = parent::form($c);
			
			// Saved values
			if($form->inputs) {
				// Row
				if(!$this->row) $this->row();
				
				// Values we want to overwrite with saved values
				$overwrite = array(
					plugin($this->plugin.'.db.parent_module'),
					plugin($this->plugin.'.db.parent_id'),
					plugin($this->plugin.'.db.user'),
				);
				
				// Saved values
				foreach($form->inputs as $k => $v) {
					// New method
					if(is_object($v)) {
						if($v->name and (!x($v->value) or in_array($v->name,$overwrite))) {
							$form->inputs[$k]->value = $this->row[$v->name];	
						}
					}
					// Old method
					else {
						if($v[name] and (!x($v[value]) or in_array($v[name],$overwrite))) {
							$form->inputs[$k][value] = $this->row[$v[name]];	
						}
					}
				}
			}
				
			// Saved id
			$form->hidden('id',$this->plugin_id,array('process' => 0));
			
			// Return
			return $form;
		}
		
		/**
		 * Saves the given values for the current plugin item.
		 *
		 * @param array $values An array of values you want to save.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the saved plugin item.
		 */
		function save($values,$c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$values) return;
			// Data stored locally
			if(!plugin($this->plugin.'.db.table')) return;
			
			// Config
			if(!x($c[process])) $c[process] = 1; // Do you want to process the plugin item's results after we're done saving?
			if(!x($c[process_item])) $c[process_item] = 0; // Do you want to process the plugin item's parent item after we're done saving?
			if(!x($c[debug])) $c[debug] = 0; // Debug
		
			// Plugin values
			$plugins_values = $values[__plugins];
			
			// Row - needed to check if $this->plugin_id actually exists
			$this->row();
			
			// Statement
			if($this->plugin_id > 0) $statement = "UPDATE";
			else $statement = "INSERT";
			
			// Values
			$values = $this->save_values($values,$statement);
			if($values) {
				// Debug
				debug("\$".__CLASS__."->".__FUNCTION__."() values: ".return_array($values),$c[debug]);
				
				// Old array
				$c[old] = NULL;
				if($this->id > 0) $c[old] = $this->row();
				
				// Values string
				$values_string = $this->db->values($values);
				
				// Build query
				if($statement == "INSERT") $query = "INSERT INTO ".plugin($this->plugin.'.db.table')." ".$values_string;
				if($statement == "UPDATE") $query = "UPDATE `".plugin($this->plugin.'.db.table')."` ".$values_string." WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."' AND "./*plugin($this->plugin.'.db.parent_id')." = '".$this->id."' AND ".*/plugin($this->plugin.'.db.id')." = '".$this->plugin_id."'";
				
				// Debug
				debug($query,$c[debug]);
				
				// Run query
				$result = $this->db->q($query);
				
				// Id (if insert)
				if($statement == "INSERT") $this->plugin_id = $result;
				// Refresh cache (if updating)
				else $this->refresh($values);
			}
			
			// Plugins
			if($plugins = plugin($this->plugin.'.plugins')) {
				$c_plugins = $c;
				$c_plugins[item] = $this->item;
				$method = __CLASS__."_".__FUNCTION__;
				foreach($plugins as $plugin => $plugin_v) {
					$plugin_instance = $this->plugin($plugin,$c);
					if(method_exists($plugin_instance,$method)) {
						debug("Saving plugin ".$plugin." (from plugin ".$this->plugin.")",$c[debug]);
						$c_plugins['isset'] = isset($plugins_values[$plugin]);
						$c_plugins[process_item] = 0; // We're going to process on our own shortly
						$plugin_instance->$method($plugins_values[$plugin],$c_plugins);
					}
				}
			}
			
			// Process
			if($c[process]) {
				$c[save][values] = $values;
				$this->process($c);
			}
			
			// Return
			return $this->plugin_id;
		}
		
		/**
		 * Processes a plugin item and it's related data such as visiblity, etc.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the new plugin item.
		 */
		function process($c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id) return;
			
			// Id
			$id = ($this->id ? $this->id : 0);
			
			// Already processing - check this so as to avoid infinite loop since any $item->save() within here will re-process
			if(!g('processing.'.$this->module.'.'.$id.'.plugin_item.'.$this->plugin.'.'.$this->plugin_id)) {
				// Processing
				g('processing.'.$this->module.'.'.$id.'.plugin_item.'.$this->plugin.'.'.$this->plugin_id,1);
				
				// Config
				if(!x($c[process_item])) $c[process_item] = 0; // Do you want to process the plugin item's parent item? Only really need to to it 'saving' (save() method sets this to 1, see above)
				
				// Visible
				$this->process_visible($c);
				// Order
				$this->process_order($c);
				
				// Process item
				if($c[process_item]) {
					$c_item = $c;
					unset($c_item[save]);
					if(x($c[save])) $c_item[save][values] = array(); // Indicate we were 'saving', but not any values (so search indexing doesn't run again)
					$this->item->process($c_item);
				}
				
				// Done processing
				g('processing.'.$this->module.'.'.$id.'.plugin_item.'.$this->plugin.'.'.$this->plugin_id,0);
			}
		}
		
		/** 
		 * Determines if a plugin item is visible to the public or not.
		 *
		 * Really just a placeholder function so we can easily effect the 'visible' value in plugin specific plugin_item classes.
		 *
		 * @param array $c An array of configuration values. Deafult = NULL
		 * @return boolean Whether or not the plugin item is visible to the public.
		 */
		function visible_check($c) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id) return;
			
			// Visible by default
			$visible = 1;
			
			// Disabled (inactive)
			if(plugin($this->plugin.'.db.active') and !$this->db('active')) {
				$visible = 0;
			}
			
			// Return
			return $visible;
		}
		
		/** 
		 * Determines if a plugin item is visible to the public or not and saves that value to the item's 'visible' db field (m($module.'.db.visible');).
		 *
		 * @param array $c An array of configuration values. Deafult = NULL
		 */
		function process_visible($c) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Field exists
			if(plugin($this->plugin.'.db.visible')) {
				// Debug
				debug("<br />\$".__CLASS__."->".__FUNCTION__."();",$c[debug]);
				
				// Visible?
				$visible = $this->visible_check(array('debug' => $c[debug]));
				debug("visible: ".$visible,$c[debug]);
				
				// Save
				$this->save(array('db.visible' => $visible),array('debug' => $c[debug]));
			}
		}
		
		/**
		 * Gives the plugin item an 'order' # (if the db.order variable is defined in the module and they don't already have an order). 
		 */
		function process_order($c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Field exists (and doesn't yet have a value)
			if(!$this->db('order') and plugin($this->plugin.'.db.order')) {
				// Debug
				debug("<br />\$".__CLASS__."->".__FUNCTION__."();",$c[debug]);
				
				// Query
				$where = NULL;
				if(plugin($this->plugin.'.db.parent_module') and $this->db('parent_module')) $where .= ($where ? " AND " : " WHERE ").plugin($this->plugin.'.db.parent_module')." = '".$this->db('parent_module')."'";
				if(plugin($this->plugin.'.db.parent_id') and $this->db('parent_id')) $where .= ($where ? " AND " : " WHERE ").plugin($this->plugin.'.db.parent_id')." = '".$this->db('parent_id')."'";
				if(plugin($this->plugin.'.db.parent_plugin_id') and $this->db('parent_plugin_id')) $where .= ($where ? " AND " : " WHERE ").plugin($this->plugin.'.db.parent_plugin_id')." = '".$this->db('parent_plugin_id')."'";
				if(plugin($this->plugin.'.db.order_level')) $where .= ($where ? " AND " : " WHERE ").plugin($this->plugin.'.db.order_level')." = 0";
				$query = "SELECT MAX(".plugin($this->plugin.'.db.order').") o FROM ".plugin($this->plugin.'.db.table').$where;
				debug($query,$c[debug]);
				
				// Result
				$row = $this->db->f($query);
				
				// Update order
				$this->save(array('db.order' => ($row[o] + 1)),array('debug' => $c[debug]));
			}
		}
		
		/**
		 * Deletes plugin row for the current plugin item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function delete($c = NULL) {
			// Error
			if(!$this->module /*or !$this->id*/ or !$this->plugin or !$this->plugin_id) return;
			
			if(plugin($this->plugin.'.db.table') and plugin($this->plugin.'.db.id') and plugin($this->plugin.'.db.parent_module') and plugin($this->plugin.'.db.parent_id')) {
				// Plugins - do before we actually delete plugint item so we can reference the plugin item's info
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = plugin($this->plugin.'.plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						// Plugin active
						if($plugin_active) {
							// Plugin class
							$plugin_instance = $this->plugin($plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								// Delete
								debug("Deleting in plugin ".$plugin." (from plugin ".$this->plugin.")",$c[debug]);
								$plugin_instance->$method($c);
							}
						}
					}
				}
				
				// Query
				$query = "DELETE FROM ".plugin($this->plugin.'.db.table')." WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."'".($this->id ? " AND ".plugin($this->plugin.'.db.parent_id')." = '".$this->id."'" : "")." AND ".plugin($this->plugin.'.db.id')." = '".$this->plugin_id."'";
				debug($query,$c[debug]);
				
				// Delete
				$this->db->q($query);
				
				// Refresh
				$this->refresh();
			}
		}
		
		/**
		 * Disables or enables the plugin item based on its current 'active' state.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function disable($c = NULL) {
			// Error
			if(!$this->module /*or !$this->id*/ or !$this->plugin or !$this->plugin_id) return;
			
			// Column
			$column = plugin($this->plugin.'.db.active');
			
			// Have id and column
			if($this->id > 0 and $column) {
				// Config
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Values
				$values = array(
					$column => ($this->db('active') ? 0 : 1)
				);
				
				// Save
				$this->save($values,$c);
			}
		}
	
		/**
		 * 'Refreshes' plugin item by removing all old data so we'll have to re-create it.
		 *
		 * Usually called after altering a plugin item. Data will get recreated next time we call $this->v() or $this->db() (which in turn call $this->row())
		 *
		 * @param array $values An array of values that we need to refresh (means we won't have to re-query db, just update $this->row). Default = NULL
		 */
		function refresh($values = NULL) {
			// Cache
			$cache = 0;
			
			// Update old row
			if(is_array($values) and $this->row and (!$this->c[language] or $this->c[language] == m('languages.settings.languages.default'))) {
				// Update row
				$this->row = array_merge($this->row,$values);
				// Clear 'original' row - we know this is not translated item class so there will be no changed values
				$this->row_original = NULL;
				// Want to re-cache item as we have updated row (avoid new query next time we want to use this item)
				$cache = 1;
			}
			// Clear old row
			else unset($this->row,$this->row_original);
				
			// Clear old data
			foreach(plugin($this->plugin.'.db') as $k => $v) {
				if(!in_array($k,array('module','id','type','plugin','plugin_id'))) unset($this->$k); // Global database variables we set to $this-> variables for faster access.
			}
			unset($this->cache,$this->type); // Instance variables
			
			// Update cache
			if($cache) {
				$this->cache_save();
			}
		}
		
		/**
		 * Returns an instance of the file plugin and sets the file key.
		 *
		 * @param string $column The database column in the item's row that contains the file's name. Can also be db variable, example: db.image.
		 * @param array An array of configuration values. Default = NULL
		 * @return object An instance of the files plugin class.
		 */
		function file($column,$c = NULL) {
			// Config String
			$c_string = cache_config($c);
		
			// DB definition - passed db.definition, get real field name so we can use that with custom info (_source, _file, _extension, etc.)
			if(strstr($column,'db.') and plugin($this->plugin.'.'.$column)) {
				if(!$c[type]) $c[type] = substr($column,3); // Use db variable to define the 'type' of file this is
				$column = plugin($this->plugin.'.'.$column);
			}
				
			// Get cached plugin item
			if($plugin_item = $this->cache_get('files/'.$column.'/'.$c_string)) {
				return $plugin_item;
			}
			
			// Get new plugin item
			if($name = $this->v($column)) {
				$row = NULL;
				
				// International - 'original' value exists so must have changed (aka, been translated)
				if($this->row_original[$column]) {
					// Is there an interntional value stored?
					$row0 = $this->db->f("SELECT * FROM ".plugin('international.db.table')." WHERE ".plugin('international.db.parent_module')." = '".$this->module."' AND ".plugin('international.db.parent_id')." = '".$this->id."' AND plugin_code = '".$this->plugin."' AND plugin_id = '".$this->plugin_id."' AND item_column = '".$column."'");
					// Yes, get corresponding file info
					if($row0[id]) {
						$row = $this->db->f("SELECT * FROM ".plugin('files.db.table')." WHERE ".plugin('files.db.parent_module')." = '".$this->module."' AND ".plugin('files.db.parent_id')." = '".$this->id."' AND plugin_code = 'international' AND plugin_id = '".$row0[id]."' AND file_column = 'international_value'");
					}
				}
				// Default
				if(!$row[id]) {
					$row = $this->db->f("SELECT * FROM ".plugin('files.db.table')." WHERE ".plugin('files.db.parent_module')." = '".$this->module."' AND ".plugin('files.db.parent_id')." = '".$this->id."' AND plugin_code = '".$this->plugin."' AND plugin_id = '".$this->plugin_id."' AND file_column = '".$column."'");
				}
			}
			
			// Plugin item - create even if no 'row' so any methods we call on this will still work
			$plugin_item = $this->plugin_item('files',$row,$c);
			
			// Cache
			if($row) {
				$this->cache_save('files/'.$column.'/'.$c_string,$plugin_item);
			}
			
			// Return
			return $plugin_item;
		}
		
		/**
		 * Checks whether current user has permission to perform given plugin action in this module and (if present) item.
		 *
		 * @param string $action The action we want to check the permission of.
		 * @return boolean Whether or not the current user has the permission to perform this plugin action on this module/module item.
		 */
		function permission($action) {
			// Permission
			$permission = permission($this->module,$this->plugin.'.'.$action,$this->id,array('type' => $this->item->type,'plugin_id' => $this->plugin_id));
			// Return
			return $permission;
		}
		
		/**
		 * Returns HTML of this given view for this plugin item.
		 *
		 * @param string $view The 'view' you want to load. Default = item
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the view.
		 */
		function view($view,$variables = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$view) return;
			
			// Default
			if(!$view) $view = 'item';
			
			// Array
			$array = array(
				'module' => $this->module,
				'id' => $this->id,
				'plugin' => $this->plugin,
				'plugin_id' => $this->plugin_id,
				'view' => $view
			);
			
			// View
			$loader = loader::load();
			return $loader->view($array,$variables);
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action on this plugin item.
		 *
		 * This method basically lets us check permission and data within this plugin item before we send it to the $core->icon() method.
		 * See $core->icon() for more configuration values.
		 *
		 * @param string $action The action of the icon you're creating.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($action,$c = NULL) {
			// Error
			if(!$this->module or /*!$this->id or */!$this->plugin or !$this->plugin_id) return;
			
			// Config
			if(!$c[icon]) $c[icon] = $action; // The CSS 'i-' class you want to apply to generate the icon. Example: 'add' would result in 'i-add'.
			if(!$c[text] and $c[button]) $c[text] = ($c[tooltip_text] ? $c[tooltip_text] : ucwords($action)); // Text to display alongside the icon (required if a 'button')
			if(!$c[tooltip_text]) $c[tooltip_text] = ($c[text] ? $c[text] : ucwords($action)); // The tooltooltip text to show on hover (if $c[tooltip] == 1).
			if(!$c[parent_selector]) $c[parent_selector] = "#".$this->plugin."-".$this->plugin_id; // The jQuery selector of the parent element. Used when deleting an item.
			if(!x($c[permission])) $c[permission] = 1; // Do you want to check whether or not the user has permission to perform this action on this item?
			if(!x($c[permission_action])) $c[permission_action] = $action; // What permission action are we checking against?
			if(!x($c[area])) $c[area] = NULL; // What 'area' are we linking icons to (ex: edit). Defaults to current area (if admin/account), else admin (if user is admin), else account.
		
			// Cached?
			#do we want this? so specific and frequently called that it's probably not worth it to cache it
			
			// Permission?
			$permission = 0;
			if(!$c[permission]) $permission = 1;
			else if($this->permission($c[permission_action])) $permission = 1;
			if($permission) {
				if(!x($c[url])) {
					// Area
					if(!$c[area]) {
						$page = g('page');
						if(in_array($page->area,array('admin','account'))) $c[area] = $page->area;
						else if(u('admin')) $c[area] = 'admin';
						else $c[area] = 'account';
					}
				
					// Delete
					if($action == "delete") {
						$c[url] = "javascript:void(0);";
						$c[attributes][onclick] = "framework_plugin_delete('Are you sure you want to delete this ".strtolower(plugin($this->plugin.'.single'))."?','".$c[parent_selector]."','".$this->module."','".$this->id."','".$this->plugin."','".$this->plugin_id."');";
					}
					// Disable
					else if($action == "disable") {
						if(!plugin($this->plugin.'.db.active')) return;
						
						$c[url] = "javascript:void(0);";
						$c[attributes][id] = $this->module."-".$this->id."-disable-".random();
						$c[attributes][onclick] = "framework_disable('".$c[parent_selector]."','".$this->module."','".$this->id."',{plugin:'".$this->plugin."',plugin_id:'".$this->plugin_id."',icon_selector:'#".$c[attributes][id]."'});";
						$c[attributes]['class'] .= ($c[attributes]['class'] ? " " : "")."i-able";
						if(!$this->db('active')) {
							if($c[icon] == "disable") $c[icon] = "enable";
							if($c[tooltip_text] == "Disable") $c[tooltip_text] = "Enable";
						}
					}
				}
			}
			else $c[url] = 0;
				
			// Return
			if($c[url] or $c[required]) {
				return parent::icon($action,$c);
			}
		}
		
		/**
		 * Returns an array of the child plugin items connected to this parent plugin item.
		 *
		 * Note: this will return the child plugin items either as a plugin item object ($c['return'] == "item") [default] or as their database row array ($c['return'] == "row").
		 *
		 * @param string $plugin The child plugin you want to get the plugin items of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @param return array An array of the child plugin items connected to this parent plugin item.
		 */
		function children($plugin,$c = NULL) {
			// Error
			if(!$plugin or plugin($plugin.'.parent') != $this->plugin or !plugin($plugin.'.db.table') or !plugin($plugin.'.db.parent_id') or !$this->id) return;
			
			// Config
			if(!$c['return']) $c['return'] = "item"; // What format do you want the child items returned in: item [default], row.
			if(!$c[query_c]) $c[query_c] = NULL; // An array of configuation values passed to the $plugin_class->rows() method.
			$c[query_c]['db.parent_id'] = $this->id;
			$c[query_c][debug] = $c[debug];
			
			// Module
			$plugin_c = array(
				'item' => $this->item,
				'parent_plugin' => $this->plugin,
				'parent_plugin_id' => $this->plugin_id,
			);
			$plugin_class = plugin::load($this->module,$this->id,$plugin,$plugin_c);
			
			// Childern
			$rows = $plugin_class->rows($c[query_c]);
			while($row = $plugin_class->db->f($rows)) {
				if($c['return'] == "row") $array[] = $row;	
				else $array[] = $plugin_class->plugin_item($row);
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Loads an instance of the given plugin for the plugin.
		 * 
		 * @param string $plugin The plugin you want to load.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin class.
		 */
		function plugin($plugin,$c = NULL) {
			// Error
			if(!$plugin or !$this->module) return;
			
			// Config
			if(!x($c[cache])) $c[cache] = $this->c[cache];
			if(!x($c[cached])) $c[cached] = $this->c[cached];
			
			// Active
			if(plugin($this->plugin.'.plugins.'.$plugin)) {
				// Get
				$c[item] = $this->item;
				$c[parent_plugin] = $this->plugin;
				$c[parent_plugin_id] = $this->plugin_id;
				$plugin_instance = plugin::load($this->module,$this->id,$plugin,$c);
				
				// Return
				return $plugin_instance;
			}
		}
		
		/**
		 * Saves plugin_item instance or specific data (if 1st and 2nd param) to global cache.
		 *
		 * If you don't pass a name and $data we'll simply save the entire instance to the cache.
		 *
		 * @param string $name The name we want to use when storing the cached data. We'll use this when retrieving the cached data later. 
		 * @param string|array $data The data (string or array) we want to cache
		 */
		function cache_save($name = NULL,$data = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$this->plugin_id or !$this->c[cache] or !$this->row) return;
			
			// Language
			$language = ($this->c[language] ? $this->c[language] : 'eng');
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id.'/plugin_item/'.$this->plugin.'/'.$this->plugin_id;
			
			// Specific data
			if($name) $string .= '/plugin_item_custom/'.$language.'/'.$name;
			// Class instance
			else {
				$string .= '/plugin_item/'.$language;
				$data = $this;
			}
			
			// Local cache
			$string_local = 'cache.'.str_replace('/','.',$string);
			g($string_local,$data);
			// Global cache
			cache_save($string,$data);
		}
		
		/**
		 * Returns saved cache (if it exists).
		 *
		 * @param string $name The name of the cached data.  Default = NULL (which will return a cache of the entire plugin instance if it exists).
		 * @return mixed The cached data.
		 */
		function cache_get($name = NULL,$data = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$this->plugin_id or !$this->c[cached]) return;
			
			// Language
			$language = ($this->c[language] ? $this->c[language] : 'eng');
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id.'/plugin_item/'.$this->plugin.'/'.$this->plugin_id;
			
			// Specific data
			if($name) $string .= '/plugin_item_custom/'.$language.'/'.$name;
			// Class instance
			else $string .= '/plugin_item/'.$language;
			
			// Local cache
			$string_local = 'cache.'.str_replace('/','.',$string);
			$cache = g($string_local);
			// Global cache
			if(!$cache) {
				$cache = cache_get($string);
				// Store in local cache
				if($cache) g($string_local,$cache);
			}
			
			// Config - want current config. For example, if we passed $c[cache] = '0', but the cached item had it set to '1', we wouldn't want to cache the item now would we.
			if(!$name) $cache->c = $this->c;
			
			// Return
			return $cache;
		}
	}
}
?>