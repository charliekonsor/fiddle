<?php
if(!class_exists('loader_framework',false)) {
	/**
	 * Class for easily loading other classes.
	 *
	 * @package kraken
	 */
	class loader_framework extends loader {
		// Variables
		//var $cached;
		
		/**
		 * Loads and returns an instance of the form class.
		 * 
		 * @param string $module The module of the item we'll be editing in the form.
		 * @param int $id The id of the item we'll be editing in the form. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the form class.
		 */
		function form($module,$id = NULL,$c = NULL) {
			// Params
			if(is_array($module)) { // $page->form($c)
				$c = $module;
				$module = NULL;
				$id = NULL;
			}
			
			// Module form 
			if($module) $instance = form::load($module,$id,$c);
			// Core form
			else $instance = form::load($c);
			
			// Return
			return $instance;
		}
		
		/**
		 * Loads and returns an instance of the module class.
		 * 
		 * @param string $module The module you want to create the class with.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the module class.
		 */
		function module($module,$c = NULL) {
			// Error
			if(!$module) return;
		
			// Saved? // Can't use because, again,in the case of modules speciric classes, would load cached objects before the corresponding class has been included // Can use since we're no longer caching globally // ...but not worth it because this all takes longer than just re-loading it probably would
			//if($instance = $this->cached[modules][$module][md5(serialize($c))]) return $instance;
		
			// Instance
			$instance = module::load($module,$c);
			
			// Save // Can use since we're no longer caching globally // ...but not worth it because this all takes longer than just re-loading it probably would
			//$this->cached[modules][$module][md5(serialize($c))] = $instance;
			
			// Return
			return $instance;
		}
		
		/**
		 * Loads and returns an instance of the item class.
		 * 
		 * @param string $module The module the item is in.
		 * @param int $id The id of the item. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the item class.
		 */
		function item($module,$id = NULL,$c = NULL) {
			// Error
			if(!$module) return;
			
			// Saved? // Can't use because, again,in the case of modules specific classes, would load cached objects before the corresponding class has been included // Can use since we're no longer caching globally
			//if($instance = $this->cached[items][$module][$id][md5(serialize($c))]) return $instance;
			if($instance = $this->cached[items][$module][$id]) return $instance; // $c isn't important enough for separate caching
			
			// Instance
			$instance = item::load($module,$id,$c);
			
			// Save // Can use since we're no longer caching globally // ...but adds to memory size (caching both item and this reference to item, makes no sense)
			//$this->cached[items][$module][$id][md5(serialize($c))] = $instance;
			//$this->cached[items][$module][$id] = $instance; // $c isn't important enough for separate caching
			
			// Return
			return $instance;
		}
		
		/**
		 * Loads and returns an instance of the plugin class.
		 * 
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin class.
		 */
		function plugin($module,$id,$plugin,$c = NULL) {
			// Error
			if(!$module or /*!$id or */!$plugin) return;
			
			// Instance
			$instance = plugin::load($module,$id,$plugin,$c);
			
			// Return
			return $instance;
		}
		
		/**
		 * Loads and returns an instance of the plugin item class.
		 * 
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin item class.
		 */
		function plugin_item($module,$id,$plugin,$plugin_id = NULL,$c = NULL) {
			// Error
			if(!$module or /*!$id or */!$plugin) return;
			
			// Instance
			$instance = plugin_item::load($module,$id,$plugin,$plugin_id,$c);
			
			// Return
			return $instance;
		}
		
		/**
		 * Loads an instance of the view class and returns the HTML ($view->html()).
		 *
		 * @param string $view The view you want to load.
		 * @param array $variables An array of variables you want to pass to the view.
		 * @return string The HTML of the view.
		 */
		function view($view,$variables = NULL) {
			// Error
			if(!$view) return;
			
			// Instance
			$instance = view::load($view,$variables);
			
			// HTML
			$html = $instance->html();
			
			// Return
			return $html;	
		}
	}
}
?>