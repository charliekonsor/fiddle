<?php
if(!class_exists('view',false)) {
	/**
	 * Class for loading a view and retriving its HTML.
	 *
	 * @package kraken
	 */
	class view extends core_framework {
		// Variables
		var $view_string, $area, $module, $type, $view, $id, $plugin, $plugin_id, $item_module, $item_type, $item_id, $item_code, $device; // string|int
		var $variables = array(); // array
		var $page, $load, $class, $module_class, $type_class, $item_class, $plugin_class, $plugin_item_class; // object
		
		/**
		 * Loads and returns an instance of the view class.
		 *
		 * Example:
		 * - $view = view::load($view,$variables);
		 *
		 * @param string $view The view you want to load.
		 * @param array $variables An array of variables to pass to the view.
		 * @return object An instance of the view class.
		 */
		static function load($view,$variables = NULL) {
			// Class names
			$class = __CLASS__;
		
			// Framework class
			return new $class($view,$variables);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $view The view you want to load. Either as a string (ex: blog.item.123) or an array (ex: array('area' => 'public','module' => 'blog','view' => 'item','id' => 123)).
		 * @param array $variables An array of variables to pass to the view.
		 */
		function __construct($view,$variables = NULL) {
			self::view($view,$variables);
		}
		function view($view,$variables = NULL) {
			// Error
			if(!$view) return;
			
			// Parent
			parent::__construct($c);
			
			// Loader
			$this->load = loader::load();
			
			// Variables
			$this->variables = $variables;
			
			// Passed array
			if(is_array($view)) {
				// Variables
				foreach($view as $k => $v) {
					$this->$k = $v;
				}
				
				if(!$this->area) $this->area = "public";
				if(!$this->item_module) $this->item_module = $this->module;
				if(!$this->item_id) $this->item_id = $this->id;
				
				if($this->item_id and !is_int_value($this->item_id) and $this->item_id != "new") $this->item_id = $this->id = code_id($this->module,$this->item_id); // Example: menus.item.main
				
				// String
				$this->view_string = $this->module.".".($this->type ? $this->type."." : "").($this->area != "public" ? $this->area."." : "").$this->view.($this->item_id > 0 ? ".".$this->item_id : "").($this->plugin ? ".".$this->plugin.($this->plugin_id ? ".".$this->plugin_id : "") : "");
				
				// Debug
				debug_speed('view - '.$this->view_string);
			}
			// Passed string, parse
			else {
				// Stirng
				$this->view_string = $view;
				
				// Debug
				debug_speed('view - '.$this->view_string);
				debug_speed('parse','view - '.$this->view_string);
			
				// Variables
				$ex = explode('.',$view);
				// Variables - module
				$this->item_module = $this->module = $ex[0];
				unset($ex[0]);
				$ex = array_values($ex);
				// Variables - type. Example: users.members.home
				if(m($this->module.'.types.'.$ex[0])) {
					$this->type = $ex[0];
					unset($ex[0]);
				}
				$ex = array_values($ex);
				// Variables - view
				if($ex[0]) {
					$this->view = $ex[0];
					unset($ex[0]);
					if($this->view == "admin" or $this->view == "account") { // Example: blog.admin.home
						$this->area = $ex[0];
						$this->view = $ex[1];
						unset($ex[1]);
					}
					else $this->area = "public";
					$ex = array_values($ex);
				}
				// Variables - id
				if($ex[0]) {
					$this->item_id = $ex[0];
					if($this->item_id and !is_int_value($this->item_id)) $this->item_id = code_id($this->item_module,$this->item_id); // Example: menus.item.main
					$this->id = $this->item_id;
					unset($ex[0]);
					$ex = array_values($ex);
				}
				// Varibles - plugin
				if($ex[0]) {
					if(m($this->item_module.'.plugins.'.$ex[0]) and $ex[1]) {
						$this->plugin = $ex[0];	
						$this->view = $ex[1];
					}
					unset($ex[0],$ex[1]);
					$ex = array_values($ex);
				}
				// Variables - plugin_id
				if($ex[0] and $this->plugin) {
					$this->plugin_id = $ex[0];
					unset($ex[0],$ex[1]);
					$ex = array_values($ex);
				}
			}
				
			// Debug
			debug_speed('load','view - '.$this->view_string);
			
			// Page
			$this->page = g('page');
			
			// Classes
			$this->module_class = module::load($this->module);
			$this->class = $this->module_class;
			if($this->type) {
				$this->type_class = module::load($this->module,array('type' => $this->type));
				$this->class = $this->type_class;
			}
			if($this->item_module) { // Can have item_module, but not item_id (ex: if 'new' view or what not)
				$this->item_module_class = module::load($this->item_module);
				$this->item_class = item::load($this->item_module,$this->item_id,array('type' => $this->type));
				$this->class = $this->item_class;
			}
			if($this->item_module and $this->plugin) {
				$this->plugin_class = plugin::load($this->item_module,$this->item_id,$this->plugin);
				$this->class = $this->plugin_class;
			}
			if($this->item_module and $this->plugin and $this->plugin_id > 0) {
				$this->plugin_item_class = plugin_item::load($this->item_module,$this->item_id,$this->plugin,$this->plugin_id);
				$this->class = $this->plugin_item_class;
			}
			
			// Type - can also be set if we passed type in $view array param
			/*if($variables[type]) $this->type = $c[type];
			else*/ if($this->item_class) {
				if(!in_array($this->view,array('type','category')) and $this->item_class->id > 0) {
					$this->type = $this->item_class->type;
					$this->type_class = module::load($this->module,array('type' => $this->type));
				}
			}
			
			// Device
			$this->device = $_SESSION['device'];
				
			// Debug
			debug_speed('load','view - '.$this->view_string);
		}
		
		/**
		 * Determines the the path to the file for the current view.
		 * 
		 * @return string The path to the file where this view is located.
		 */
		function file() {
			// Debug
			debug_speed('file','view - '.$this->view_string);
			
			// Prefixes - device, language, area (in that order)
			$prefixes = NULL;
			$language = $_SESSION['__modules']['languages']['selected'];
			if(!$language) $language = m('languages.settings.languages.default');
			if($this->device and $this->device != "default") {
				if($language) $prefixes[] = $this->device.".".$language."."; // Device and language
				$prefixes[] = $this->device."."; // Device
			}
			if($language) $prefixes[] = $language."."; // Language
			if($this->area != "public") $prefixes[] = $this->area."."; // Area
			$prefixes[] = ""; // No prefix
				
			// Files
			$files = NULL;
			// Files - plugin
			if($this->plugin) {
				// Paths
				$path = plugin($this->plugin.'.path');
				$path_theme = plugin($this->plugin.'.path_themes');
				if(plugin($this->plugin.'.core')) $path_local = plugin($this->plugin.'.path_local');
				// Paths - module
				$path_module = $this->module_class->v('path');
				$path_module_theme = $this->module_class->v('path_themes');
				if($this->module_class->v('core')) $path_module_local = $this->module_class->v('path_local');
				
				// View specific file within module, plugin and theme
				if(!$file and $this->module and $this->page->theme) $files[] = SERVER."local/themes/".$this->page->theme."/modules/".$this->module."/".$path_theme."views/".$this->view.".php";
				// View specific file within local module and plugin (if core module)
				if(!$file and $this->module and $path_module_local) $files[] = SERVER.$path_module_local.$path_theme."views/".$this->view.".php";
				// View specific file within module and plugin
				if(!$file and $this->module) $files[] = SERVER.$path_module.$path_theme."views/".$this->view.".php";
				// View specific file within plugin and theme
				if(!$file and $this->page->theme) $files[] = SERVER."local/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".php";
				// View specific file within local plugin (if core plugin)
				if(!$file and $path_local) $files[] = SERVER.$path_local."views/".$this->view.".php";
				// View specific file within plugin
				if(!$file) $files[] = SERVER.$path."views/".$this->view.".php";
			}
			// Files - module
			else if($this->module) {		
				// Paths
				$path = $this->module_class->v('path');
				$path_theme = $this->module_class->v('path_themes');
				if($this->module_class->v('core')) $path_local = $this->module_class->v('path_local');
				
				// Item specific // Deprecated for speed
				/*if($this->item_id > 0) {
					// Item and type specific file within module and theme (defined by id)
					if(!$file and $this->type) {
						if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".".$this->item_id.".php";
						else $files[] = SERVER."local/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".".$this->item_id.".php";
					}
					// Item and type specific file within local module (defined by id) (if core module)
					if(!$file and $this->type and $path_local) $files[] = SERVER.$this->type_class->v('path_local')."/views/".$this->view.".".$this->item_id.".php";
					// Item and type specific file within module (defined by id)
					if(!$file and $this->type) $files[] = SERVER.$this->type_class->v('path')."/views/".$this->view.".".$this->item_id.".php";
					// Item and type specific file within module and theme (defined by code)
					if(!$file and $this->item_class->code and $this->type) {
						if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".".$this->item_class->code.".php";
						else $files[] = SERVER."local/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".".$this->item_class->code.".php";
					}
					// Item and type specific file within local module (defined by code) (if core module)
					if(!$file and $this->item_class->code and $this->type and $path_local) $files[] = SERVER.$this->type_class->v('path_local')."/views/".$this->view.".".$this->item_class->code.".php";
					// Item and type specific file within module (defined by code)
					if(!$file and $this->item_class->code and $this->type) $files[] = SERVER.$this->type_class->v('path')."/views/".$this->view.".".$this->item_class->code.".php";
					// Item specific file within module and theme (defined by id)
					if(!$file) {
						if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".".$this->item_id.".php";
						else $files[] = SERVER."local/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".".$this->item_id.".php";
					}
					// Item specific file within module (defined by id) (if core module)
					if(!$file and $path_local) $files[] = SERVER.$path_local."views/".$this->view.".".$this->item_id.".php";
					// Item specific file within module (defined by id)
					if(!$file) $files[] = SERVER.$path."views/".$this->view.".".$this->item_id.".php";
					// Item specific file within module and theme (defined by code)
					if(!$file and $this->item_class->code) {
						if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".".$this->item_class->code.".php";
						else $files[] = SERVER."local/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".".$this->item_class->code.".php";
					}
					// Item specific file within local module (defined by code) (if core module)
					if(!$file and $this->item_class->code and $path_local) $files[] = SERVER.$path_local."views/".$this->view.".".$this->item_class->code.".php";
					// Item specific file within module (defined by code)
					if(!$file and $this->item_class->code) $files[] = SERVER.$path."views/".$this->view.".".$this->item_class->code.".php";
				}*/
			
				// View specific file within module type and theme
				if(!$file and $this->type) {
					if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".php";
					else $files[] = SERVER."local/themes/".$this->page->theme."/".$this->type_class->v('path_themes')."/views/".$this->view.".php";
				}
				// View specific file within module type
				if(!$file and $this->type) $files[] = SERVER.$this->type_class->v('path')."/views/".$this->view.".php";
				// View specific file within local module type (if core module)
				if(!$file and $this->type and $path_local) $files[] = SERVER.$this->type_class->v('path_local')."/views/".$this->view.".php";
				// View specific file within module and theme
				if(!$file) {
					if($this->area == "admin") $files[] = SERVER."core/framework/admin/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".php";
					else $files[] = SERVER."local/themes/".$this->page->theme."/".$path_theme."views/".$this->view.".php";
				}
				// View specific file within local module (if core module)
				if(!$file and $path_local) $files[] = SERVER.$path_local."views/".$this->view.".php";
				// View specific file within module
				if(!$file) $files[] = SERVER.$path."views/".$this->view.".php";
			}
			// Files - admin / account
			else if($this->area and $this->area != "public") {
				// Theme specific view (account only)
				if(!$file and $this->area == "account") $files[] = SERVER."local/themes/".$this->page->theme."/".$this->area."/views/home.php";
				// Local view
				if(!$file) $files[] = SERVER."local/".$this->area."/views/home.php";
				// Core theme view (admin only)
				if(!$file and $this->area == "admin") $files[] = SERVER."core/framework/".$this->area."/themes/".g('config.admin.theme')."/views/home.php";
				// Core view
				if(!$file) $files[] = SERVER."core/framework/".$this->area."/views/";
			}
			//debug("files: ".return_array($files));
			
			// Exists?
			foreach($files as $file) {
				$name = basename($file);
				$path = str_replace($name,'',$file);
				foreach($prefixes as $prefix) {
					$file = $path.$prefix.$name;
					if(file_exists($file)) break;
					else $file = NULL;
				}
				if($file) break;
			}
			
			// Debug
			debug_speed('file','view - '.$this->view_string);
			
			// Return
			return $file;
		}
		
		/**
		 * Returns the HTML of the view.
		 * 
		 * @param array $variables An array of variables to pass to the view file. Default = NULL
		 * @return string The HTML of the view file.
		 */
		function html($variables = NULL) {
			// Variables - parents
			$variables[parents] = $this->page->parents;
			// Variables - variables
			$variables[variables] = $this->page->variables;
			// Variables - page // Use $this->page->... instead
			//$variables[page] = $this->page;
			// Variables - custom
			if($this->variables and $variables) $variables = array_merge($this->variables,$variables);
			else $variables = $this->variables;
			
			/* Calling via this class */
			// File
			$file = $this->file();
			//print $file."<br />";
			
			// Debug - call after we got 'file' as the file() method also debugs speed
			debug_speed('html','view - '.$this->view_string);
			
			// HTML
			$html = $this->html_get($file,$variables);
			
			// Debug
			debug_speed('html','view - '.$this->view_string);
			debug_speed('view - '.$this->view_string);
			
			// Return
			return $html;
		}
		
		/**
		 * Determines whether or not the current user has permission to perform the given action.
		 * 
		 * @param string $action The action they would be performing.
		 * @param boolean|string $message Whether or not you want to display an error message if they don't have permission. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @param array $c An array of congiguration values. Default = NULL
		 * @return boolean Whether or not they have perimssion to perform the action.
		 */
		function permission($action,$message = 0,$c = NULL) {
			// Class
			$permission = $this->class->permission($action,$message,$c);
			
			// Return
			return $permission;
			
			/* This is how I'd like to do it, where the child classes just return boolean, but the whole returning an array in the permission if a plugin error in the item check effects the message is a bit odd */
			// Config
			/*if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Message - build here so we can overwrite entire thing in class permission check
			if(!x($message) or $message === 1) $message = "I'm sorry, but you don't have permission to ".($this->area == "public" ? "view" : "do")." this.";
			if($message) {
				$message = "<div class='core-red core-none'>".$message."</div>";	
				if(!g('u.id')) $message .= $this->login_form();
			}
			
			// Permission
			$permission = $this->class->permission($action);
			if(is_array($permission)) {
				if($message) $message = $permission[message];	
				$permission = $permission[permission];
			}
			
			// Message
			if(!$permission and $message) print $message;
			
			// Return
			return $permission;*/
		}
		
		/**
		 * Determines whether or not the current page should be 'visible'.
		 * 
		 * @param boolean|string $message Whether or not you want to display an error message if it's not visible. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not the page is visible.
		 */
		function visible($message = 0) {
			// Visible
			$visible = 1;
			// Visible - plugin item
			if($this->plugin_item) {
				if($this->plugin_item->id) {
					$visible = $this->plugin_item->db('visible');
				}
			}
			// Visible - item
			if($this->item_class) {
				if($this->item_class->id) {
					$visible = $this->item_class->db('visible');
				}
			}
			
			// Message
			if(!$visible and $message) {
				// Container
				print "<div class='core-red core-none'>";
				
				// Default
				if($message === 1) print "I'm sorry, but you don't have permission to ".($this->area == "public" ? "view" : "do")." this.";
				// Custom
				else print $message;
				
				// End container
				print "</div>";
			}
			
			// Return
			return $visible;
		}

		/**
		 * Determines the heading for the given page and returns the HTML of it (or, if $c['return'] == "array", an array of url => title heading parts).
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|array The HTML of the page heading or (if $c['return'] == "array") an array of the heading parts (url => title).
		 */
		function heading($c = NULL) {
			// Config
			if(!$c['return']) $c['return'] = "html"; // What to return: html [default], array (will be in url => title format).
			
			// Array
			$array = $this->heading_array($c);
			if($array) {
				// Return
				if($c['return'] == "array") return $array;
				else return $this->heading_html($array,$c);
			}
		}

		/**
		 * Determines the heading for the given page and returns an array in url => title format.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the heading parts (url => title).
		 */
		function heading_array($c = NULL) {
			// Array
			$array = NULL;
			
			// URL root
			$url = D;
			if($this->area != "public") $url .= $this->area;
			
			// Parent(s)
			if($this->page->parents) {
				foreach($this->page->parents as $k => $v) {
					if($k !== "last") {
						// Module
						if($v[module]) {
							$module_class = module::load($v[module]);
							$url = $module_class->url('home',$this->area,array('parent_module' => $this->page->parents[($k - 1)][module],'parent_id' => $this->page->parents[($k - 1)][item_id]));
							$array[$url] = $module_class->v('name');
						}
						
						// Type
						if($v[type] and (!is_int_value($v[item_id]) or $v[item_module] == "types")) {
							$type_class = module::load($v[module],array('type' => $v[type]));
							$url = $type_class->url('type',$this->area,array('parent_module' => $this->page->parents[($k - 1)][module],'parent_id' => $this->page->parents[($k - 1)][item_id]));
							$array[$url] = $type_class->v('name');
						}
						
						// Item
						if($v[module] and $v[id] and $v[module] == $v[item_module]) {
							$url = $url = $v[item]->url($this->area);
							$name = NULL;
							if($v[item]->name) $name = string_limit($v[item]->db('name'),40);
							else if($v[id] == "new") $name = "New ".m($v[module].'.single');
							$array[$url] = $name;
						}
					}
				}
			}
			
			// Module
			if($this->module and $url = $this->module_class->url('home',$this->area,array('parent_module' => $this->page->parents[last][module],'parent_id' => $this->page->parents[last][item_id]))) {
				$array[$url] = $this->module_class->v('name');
			}
			
			// Type
			if($this->type and $url = $this->type_class->url('type',$this->area,array('parent_module' => $this->page->parents[last][module],'parent_id' => $this->page->parents[last][item_id]))) {
				$array[$url] = $this->type_class->v('name');
			}
			
			// Item
			if($this->item_module and $this->item_id and $this->module == $this->item_module) {
				if($url = $this->item_class->url($this->area)) {
					
					$name = $this->item_class->name;
					if(!$name and $this->item_class->id > 0) $name = $this->item_module_class->v('single')." #".$this->item_class->id; // If I decide to remove this, have to do within views I want it (ex: payments)
					if(!$name and $this->item_id == "new") $name = "New ".$this->item_module_class->v('single',$this->type);
					$array[$url] = string_limit($name,40);
				}
			}
			
			// Variables
			if($this->variables) {
				foreach($this->variables as $k => $v) {
					$url_type .= "/".$v;
					$array[$url_type] = ucwords($v);
				}
			}
			
			// Return
			return $array;
		}

		/**
		 * Returns the HTML for displaying the given array of page heading levels.
		 * 
		 * @param array $array The array of heading levels you want to display in array(url => title) format.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the page heading.
		 */
		function heading_html($array,$c = NULL) {
			// Config
			if(!$c[separator]) $c[separator] = ": "; // The string you want to use when separating levels of the heading.
			
			// Current URL (without query string)
			$url_noquery = url_noquery(URL);
			
			// Heading
			$return = NULL;
			foreach($array as $k => $v) {
				$return .= ($return ? $c[separator] : "").(substr($k,0,4) == "http" ? "<a href='".$k."'".($url_noquery == $k ? " class='page-heading-selected'" : "").">".$v."</a>" : $v);
			}
			$return = "<h1 class='page-heading'>".$return."</h1>";
			
			// Return
			return $return;
		}

		/**
		 * Determines the what buttons can appear to the user and returns the HTML of it (or, if $c['return'] == "array", an array of url => title buttons).
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|array The HTML of the page heading buttons or (if $c['return'] == "array") an array of the buttons (url => title).
		 */
		function heading_buttons($c = NULL) {
			// Config
			if(!$c['return']) $c['return'] = "html"; // What to return: html [default], array (will be in url => title format).
			
			// Array
			$array = $this->heading_buttons_array($c);
			if($array) {
				// Return
				if($c['return'] == "array") return $array;
				else return $this->heading_buttons_html($array,$c);
			}
		}

		/**
		 * Determines the what buttons can appear to the user and returns an array in url => title format.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the buttons (url => title).
		 */
		function heading_buttons_array($c = NULL) {
			// Config
			if(!x($c[parents])) $c[parents] = 1; // Include parent module buttons (if they exist).
			
			// URL root
			$url = D;
			if($this->area != "public") $url .= $this->area;
			// Parents
			$parents_count = count($this->page->parents);
			foreach($this->page->parents as $k => $v) {
				// Including parent buttons?
				if($c[parents]) {
					// Only want to show the previous level
					if($k === "last") {
						$previous = count($c[parents]) - 1;
						// All 
						$view = ($v[type] && !is_int_value($v[id]) ? "type" : "home");
						if(m($v[module].'.'.$this->area.'.views.'.$view) and m($v[module].'.'.$this->area.'.views.item')) {
							$module_class = module::load($v[module]);
							if($url = $module_class->url($view,$this->area,array('parent_module' => $this->page->parents[$previous][module],'parent_id' => $this->page->parents[$previous][item_id])) and $module_class->permission('manage')) {
								$array[$url] = "All ".m($v[module].'.plural',$v[type]);	
							}
						}
						// New
						if(m($v[module].'.'.$this->area.'.views.item')) {
							$item_class = item::load($v[module],'new');
							if($url = $item_class->url($this->area,array('parent_module' => $this->page->parents[$previous][module],'parent_id' => $this->page->parents[$previous][item_id])) and  $item_class->permission('add')) {
								$array[$url] = m($v[module].'.permissions.add.label',$v[type])." New ".m($v[module].'.single',$v[type]);	
							}
						}
						// Separator
						if($array) $array[] = "separator";
					}
				}
			}
			
			if($this->module) {
				// Class - module or type
				$class = ($this->type_class ? $this->type_class : $this->module_class);
				
				// All 
				$view = ($this->type ? "type" : "home");
				if($class->v($this->area.'.views.'.$view) and $class->v($this->area.'.views.item')) {
					if($url = $class->url($view,$this->area,array('parent_module' => $this->page->parents[last][module],'parent_id' => $this->page->parents[last][item_id])) and $class->permission('manage')) {
						$array[$url] = "All ".$class->v('plural');
					}
				}
				// New
				if($class->v($this->area.'.views.item')) {
					$item_class = $class->item('new');
					if($url = $item_class->url($this->area,array('parent_module' => $this->page->parents[last][module],'parent_id' => $this->page->parents[last][item_id])) and $item_class->permission('add')) {
						$array[$url] = $class->v('permissions.add.label',$this->type)." New ".$class->v('single',$this->type);	
					}
				}
				// Categories
				if($class->v('plugins.categories')) {
					$categories_class = module::load('categories');
					if($url = $categories_class->url('home',$this->area,array('parent_module' => $this->module,'parent_id' => $this->id)) and $categories_class->permission('add')) $array[$url] = "Categories";
				}
				if($this->area == "admin") {
					// Settings
					if($class->permission('settings')) { // Check permission first as it's less resource intensive
						if($class->settings(1) and $this->module != "settings") {
							$array[DOMAIN.$this->area."/settings?module=".$this->module] = "Settings";
						}
					}
					// Documentation
					if($file = $class->documentation_file()) {
						$array[D."?ajax_action=documentation&amp;ajax_module=".$this->module."' class='overlay"] = "Help";
					}
				}
			}
			
			// Separator
			$last = array_last_key($array);
			if($array[$last] == "separator") unset($array[$last]);
			
			// Return
			return $array;
		}

		/**
		 * Returns the HTML for displaying the given array of page heading buttons.
		 * 
		 * @param array $array The array of buttons you want to display in array(url => title) format.
		 * @return string The HTML of the page heading buttons.
		 */
		function heading_buttons_html($array) {
			// Current URL (without query string)
			$url_noquery = url_noquery(URL);
			
			// Buttons
			foreach($array as $k => $v) {
				if($v == "separator") $return .= "<li class='page-heading-button-separator'></li>";
				else $return .= "<li class='page-heading-button".(URL == $k || $url_noquery == $k ? " page-heading-button-selected" : "")."'><a href='".$k."'>".$v."</a></li>";
			}
			$return = "<div class='page-heading-buttons'><ul>".$return."</ul></div>";
			
			// Return
			return $return;
		}
	}
}
?>