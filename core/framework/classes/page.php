<?php
if(!class_exists('page_framework',false)) {
	/**
	 * Determines the information and HTML of a page.
	 *
	 * Dependencies
	 * - functions
	 *   - permission - in $page_framework->permission() function
	 *
	 * @package kraken
	 */
	class page_framework extends page {
		// Variables
		public $area, $module, $view, /*$view_string,*/ $type, $id, $code, $item_module, $item_type, $item_id, $item_code, $device, $theme, $template; // string|int
		public $parents, $meta, $cached = array(); // array
		public /*$m, */$item, $load; // object
		
		/**
		 * Constructs the class. Then determines what page we're on and returns an array of information about that page.			
		 * 
		 * @param string $url The URL of the page we want to get information on. Default = current page's URL
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($url = NULL,$c = NULL) {
			self::page_framework($url,$c);
		}
		function page_framework($url = NULL,$c = NULL) {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Config
			if(!x($c[cached])) $c[cached] = 1; // Use cached data (if available)
			if(!x($c[cache])) $c[cache] = 1; // Cache data for future use
			
			// Speed
			debug_speed('page - framework');
			
			// Parent
			parent::__construct($url,$c);
			
			// Loader
			$this->load = loader::load();
			
			// Variables - we'll be recreating this
			$this->variables = NULL; 
			// Theme - we'll be recreating this. Don't use in core page class anymore so it's probably not even set, but just to be sure.
			$this->theme = NULL;
			
			// SEF
			$sef = $this->sef;
			
			// Defined
			if($c[area]) {
				$this->area = $c[area];
				$this->module = $c[module];
				$this->view = $c[view];
				$this->item_module = ($c[item_module] ? $c[item_module] : $c[module]);
				$this->item_id = $this->id = $c[id];
			}
			// Home
			else if(!$sef[0]) {
				$this->area = 'public';
				$this->module = 'home';
				$this->view = 'item';
				$this->item_module = $this->module;
				$this->item_id = $this->id = 1;
			}
			// Preview URL
			/*else if(PREVIEW == 1) {
				$this->area = 'public';
				$this->module = $sef[0];
				$this->view = 'item';
				$this->item_module = $this->module;
				$this->item_id = $this->id = $sef[2];
				$this->preview = 1;
				unset($sef[0],$sef[1],$sef[2]);
				define('PREVIEW_MODULE',$this->module);
				define('PREVIEW_ID',$this->id);
				$this->variables = $sef;
			}*/
			// Saved URL
			else if(!in_array($sef[0],array('admin','account'))) {
				$row = NULL;
				$x = 0;
				$sef_lookup = $sef;
				foreach($sef_lookup as $k => $v) { // Since we're using $_SERVER['REQUEST_URI'] to create $this->url and are using that in $this->sef(), it's already encoded // Well, now it's apparently not already encoded for some reason
					$sef_lookup[$k] = string_url_encode($v);
				}
				while($x == 0) {
					$row = $this->db->f("SELECT * FROM items_urls WHERE url COLLATE utf8_bin = '".a(implode('/',$sef_lookup))."' AND url_visible = 1 ORDER BY url_primary DESC LIMIT 1");
					if($row[id]) $x = 1;
					else { // Try to match shorter part of the string
						$_variables[] = array_pop($sef_lookup); // Remove last item
						if(count($sef_lookup) <= 0) $x = 1; // No items left, couldn't match
					}
				}
				if($row[id]) {
					$this->area = $row[module_area];
					$this->module = $row[module_code];
					$this->view = $row[module_view];
					$this->item_module = $row[item_module];
					$this->item_id = $row[item_id];
					if($this->item_module == $this->module) $this->id = $row[item_id];
					if($_variables) {
						$this->variables = $_variables;
						krsort($this->variables);
					}
				}
			}
			// Parse URL
			if(!$this->area) {
				// Parse
				$array = $this->url_parse($sef);
				if($array) {
					// Set
					foreach($array as $k => $v) $this->$k = $v;
				}	
			}
			
			// Variables - reset keys
			if($this->variables) $this->variables = array_values($this->variables);
			
			// Item
			if(!$this->item_module) $this->item_module = $this->module;
			$this->item = item::load($this->item_module,$this->item_id);
			if($this->item->id) {
				$this->item_type = $this->item->type;
				$this->item_code = $this->item->code;
				if($this->module == $this->item_module) $this->type = $this->item->type;
				if($this->module == $this->item_module) $this->code = $this->item->code;
			}
			else {
				if($_GET['type']) $this->type = $_GET['type']; // 'type', not 'item_type'
			}
			// Parent item(s)
			if($this->parents) {
				foreach($this->parents as $k => $v) {
					$this->parents[$k][item] = item::load($v[item_module],$v[item_id]);
					if($this->parents[$k][item]->id) {
						$this->parents[$k][item_type] = $this->parents[$k][item]->type;
						$this->parents[$k][item_code] = $this->parents[$k][item]->code;
						if($v[module] == $v[item_module]) $this->parents[$k][type] = $this->parents[$k][item]->type;
						if($v[module] == $v[item_module]) $this->parents[$k][code] = $this->parents[$k][item]->code;
					}
				}
				$this->parents[last] = $this->parents[$k];
			}
			
			// Device
			if(!x($_SESSION['device'])){
				if(browser_tablet()) $_SESSION['device'] = "tablet";
				else if(browser_mobile()) $_SESSION['device'] = "mobile";
				else $_SESSION['device'] = "default";
			}
			$this->device = $_SESSION['device'];
			
			// Theme
			$this->theme = $this->theme();
			
			// Template
			$this->template = $this->template();
			
			// View string - no longer use this
			//$this->module.($this->type ? ".".$this->type : "")".".($this->area != "public" ? $this->area."." : "").$this->view.($this->id ? $this->id."." : "");
			
			// Global
			g('page',$this);
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r,$cached);
			
			//print_array($this);
		}
		
		/**
		 * Loops through the given SEF $array and determines the module/view/id of it as well as extra variables.
		 *
		 * On the possiblility of includeing 'plugins' in the automatic hierarchy lookup. 
		 * 1. Must decide if it's part of a module or its own level (meaning if there was a parent module in the url,
		 * that would be in the 'parent' array, not in the array of the plugin.
		 * 2. Must decide if want to prefix the view name with plugin. (ex: plugin.item, plugin.home) in which case
		 * we'd have to strip that out (or deal with it somehow) in $view->file(). Or if we just make sure we're not
		 * getting module views a plugin value exists (in $view->file()).		
		 * 
		 * @param array $array The array you want to determine the information of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the URL information including module, view, id, id_module, parents, and extra variables.
		 */
		function url_parse($array,$c = NULL) {
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Reset keys
			$array = array_values($array);
			
			// Lowercase - for matching purposes
			foreach($array as $k => $v) $array[$k] = strtolower($v);	
			
			// Area
			if(in_array($array[0],array('admin','account'))) {
				$area = $array[0];
				unset($array[0]);
		
				// Reset keys
				$array = array_values($array);
			}
			// Public area (default)
			else $area = 'public';
			
			// Count - do once, after 'area' removed, but before we look for sub-modules
			$array_count = count($array);
			
			// Modules
			$modules = g('modules');
			for($x = 0;$x < count($array);$x++) { // Put any modules in the URL first
				if(m($array[$x].'.code')) {
					$modules = array($array[$x] => $modules[$array[$x]]) + $modules;
				}
			}
			
			// Matches
			$matches = NULL;
			$matches_data = NULL;
			$key_exact = NULL;
			foreach($modules as $module => $module_v) {
				// Views
				if($views = $module_v[$area][views]) {
					foreach($views as $view => $v) {
						if($v[urls]) {
							foreach($v[urls] as $x => $url_v) {
								if($format = $url_v[format] and strpos($format,'{name}') === false) {
									// Debug
									debug("module: ".$module.", view: ".$view.", x: ".$x.", format: ".$format,$c[debug]);
									
									// Variables
									$key = $module."::".$view;
									$format = strtolower(str_replace($area."/","",$format));
									$parts = explode('/',$format);
									
									// Format is at least as long as the URL (part wise)
									if($array_count >= count($parts)) {
										// Exact match
										if($format == implode('/',$array)) {
											$key_exact = $key;
											break;
										}
										
										// Partial match
										$x = 0;
										$match = 0;
										$parent = 0;
										foreach($parts as $part) {
											// Debug
											debug("module: ".$module.", x: ".$x.", part: ".$part.", url: ".$array[$x],$c[debug]);
											
											// Part matches
											if($part == $array[$x]) {
												$match++;
												// Parent module - this needs some work. We are matching against a defined view, but don't know what parent information might be in the URL too. We're trying to guess that here, but guessing causes problems. Right now, I'm just saying if we didn't convert the parent module tot he actual 'parents' part of the maches_data array, don't use it (meaning, if the 'module' we're setting in the matches_data array must be moved to the mathces_data 'parents' array at some point, then when creating the final $heirarchy array, we don't update the 'module' based upon the matches_data array).
												if(m($array[$x])) {
													if($matches_data[$key][module]) $matches_data[$key] = $this->url_data_parent($matches_data[$key]);
													$matches_data[$key][module] = $array[$x];
												}
											}
											// Part is id and value could be id
											else if($part == "{id}" and (is_int_value($array[$x]) or $array[$x] == "new") and $v[item] == 1) {
												$match++;
												$matches_data[$key][item_module] = $v[item_module];
												$matches_data[$key][item_id] = $array[$x];
												if($module == $v[item_module]) $matches_data[$key][id] = $array[$x];
											}
											// Part is code and value could be code (only works for 'types' right now)
											else if($part == "{code}" and $v[item] == 1 and $v[item_module] == "types" and $id = m($module.'.types.'.$array[$x].'.id')) {
												$match++;
												$matches_data[$key][item_module] = $v[item_module];
												$matches_data[$key][item_id] = $id;
												if($module == $v[item_module]) $matches_data[$key][id] = $id;
												$matches_data[$key][type] = $array[$x];
											}
											// Part is code and value is code (doesn't work for 'types', those must match both the code and the module they're a part of
											else if($part == "{code}" and $v[item] == 1 and m($v[item_module].'.db.code') and $id = code_id($v[item_module],$array[$x]) and $v[item_module] != "types") {
												$match++;
												$matches_data[$key][item_module] = $v[item_module];
												$matches_data[$key][item_id] = $id;
												if($module == $v[item_module]) $matches_data[$key][id] = $id;
											}
											// Part is module (variable) and value matches that
											else if($part == "{module}" and m($array[$x]) and $module == $array[$x]) {
												if($matches_data[$key][module]) $matches_data[$key] = $this->url_data_parent($matches_data[$key]);
												$matches_data[$key][module] = $array[$x];
												$match++;
											}
											// Part is parent that we've actually discovered already
											else if($part == "{parent_module}" and ($module_v['parent'] or $module == "categories") and m($array[$x])) {
												$match++;
												if($matches_data[$key][module]) $matches_data[$key] = $this->url_data_parent($matches_data[$key]);
												$matches_data[$key][module] = $array[$x];
											}
											// Part is parent id and this module is a child module
											else if($part == "{parent_id}" and $module_v['parent'] and is_int_value($array[$x])) {
												$match++;
												$matches_data[$key][item_module] = $module_v['parent'];
												$matches_data[$key][item_id] = $matches_data[$key][id] = $array[$x];
											}
											// Part is parent of a parent that we've actually discovered already
											else if($part == "{parent_parent_module}" and (m($module_v['parent'].'.parent') or $module == "categories_items") and m($array[$x])) {
												$match++;
												if($matches_data[$key][module]) $matches_data[$key] = $this->url_data_parent($matches_data[$key]);
												$matches_data[$key][module] = $array[$x];
											}
											// Part is the special {module_code} variable (used in categories) and this value is a module // Should maybe use parent_module for this?
											else if($part == "{module_code}" and m($array[$x])) {
												$match++;
												if($matches_data[$key][module]) $matches_data[$key] = $this->url_data_parent($matches_data[$key]);
												$matches_data[$key][module] = $array[$x];
											}
											// Part is 'module_code'. categories_items specific thing as I don't know how to catch this otherwise
											/*else if($module == "categories_items" and ($part == "{module_code}" or $part == "categories") and m($array[$x])) {
												$match++;
											}*/
											// Part doesn't match, end loop
											else {
												$match = 0;
												break;
											}
											$x++;
											
											// Matched all parts, basically an 'exact' key
											if($x == $array_count) {
												$key_exact = $key;
												break;	
											}
										}
										if($key_exact) break;
										if($match) $matches[$key] = $match;
									}
								}
							}
						}
					}
					if($key_exact) break;
				}
			}
			
			
			// Have matches
			$key = NULL;
			if($key_exact) $key = $key_exact;
			else if($matches) {
				arsort($matches);
				debug("matches: ".return_array($matches),$c[debug]);
				$key = array_first_key($matches);
			}
			if($key) {
				list($module,$view) = explode('::',$key);
				
				// Store
				$hierarchy[module] = $module;
				$hierarchy[view] = $view;
				foreach($matches_data[$key] as $k => $v) {
					if(!$hierarchy[$k]) $hierarchy[$k] = $v;
				}
				
				// Remove
				if($key_exact) $array = NULL;
				else {
					for($x = 0;$x < $matches[$key];$x++) {
						unset($array[$x]);	
					}
				}
				
				// Reset keys
				if($array) $array = array_values($array);
			}
			
			// No module, use home module. Example: /custom-home-variable
			if(!$hierarchy[module] and $area == "public") {
				$hierarchy[module] = "home";
				$hierarchy[view] = "item";
				$hiearchry[item_module] = $hierarchy[module];
				$hierarchy[item_id] = $hierarchy[id] = 1;
			}
				
			// Variables
			if($array) $hierarchy[variables] = array_values($array);
			
			// Area
			$hierarchy[area] = $area;
			
			// Debug
			debug("hierarchy: ".return_array($hierarchy),$c[debug]);
			
			// Return
			return $hierarchy;
		}
		
		/**
		 * Moves current data array to a 'parent' within the data array.
		 *
		 * Used within $this->url_parse() when storing data.
		 *
		 * @param array $array The array of data we want to move to a parent within that array.
		 * @return array The new array, with the data moved to a parent within it.
		 */
		function url_data_parent($array) {
			// New array
			$array_new = NULL;
			
			// Add old parents to new array
			if($array[parents]) {
				$array_new[parents] = $array[parents];
				unset($array[parents]);
			}
			
			// Add old data to new array as a parent
			$array_new[parents][] = $array;	
			
			// Return
			return $array_new;
		}
		
		/**
		 * Loops through the given $array and determines the module/view/id hierarchy as well as extra variables.
		 *
		 * On the possiblility of includeing 'plugins' in the automatic hierarchy lookup. 
		 * 1. Must decide if it's part of a module or its own level (meaning if there was a parent
		 * module in the url, that would be in the 'parent' array, not in the array of the plugin.
		 * 2. Must decide if want to prefix the view name with plugin. (ex: plugin.item, plugin.home)
		 * in which case we'd have to strip that out (or deal with it somehow) in $view->file(). Or if
		 * we just make sure we're not getting module views a plugin value exists (in $view->file()).		
		 * 
		 * @param array $array The array you want to determine the hierarchy of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the hierachy information including module, view, id, parents, and extra variables.
		 */
		/*function hierarchy($array,$c = NULL) {
			// Config
			if(!$c[area]) $c[area] = NULL; // Area we're in (get on first level, use throughout).
			if(!x($c[parents])) $c[parents]; // Array of parent modules/views/etc. Mostly just used for determining if we're on the root level (and, if so, we can default to 'home' module)
			
			// Reset keys
			$array = array_values($array);
			
			// Area
			if(!$c[area]) {
				// Specific area
				if(in_array($array[0],array('admin','account'))) {
					$c[area] = $array[0];
					unset($array[0]);
			
					// Reset keys again
					$array = array_values($array);
				}
				// Public area (default
				else $c[area] = 'public';
			}
			
			// Have module // 2nd part checks to make sure we're not redirecting to a module when we just want it to be a variable (ex: /products/cart/shipping, don't want it to be a part of the 'shipping' module).  Pretty inelegant solution. Try to come up with something better. Maybe require views to be defined in config.php and check against thoughs.
			if(m($array[0].'.code') and (!$c[parents] or m($array[0].'.parent') == $c[parents][module] or $c[area] != "public")) {
				// Module
				$hierarchy[module] = $array[0];
				unset($array[0]);
				
				// View - item
				if(is_int_value($array[1]) or $array[1] == "new" and in_array($c[area],array('admin','account'))) { // Example: blog/123(/my-blog-name), admin/blog/new
					$hierarchy[view] = 'item';
					$hierarchy[id] = $array[1];
					unset($array[1]);
				}
				// View - type
				else if(m($hierarchy[module].'.types.'.$array[1])) { // Example: admin/users/members
					$hierarchy[type] = $array[1];
					if(is_int_value($array[2]) or $array[2] == "new" and in_array($c[area],array('admin','account'))) { // Example: admin/users/members/new
						$hierarchy[view] = 'item';
						$hierarchy[id] = $array[2];
						unset($array[2]);
					}
					else { // Example: events/meetings, admin/users/members
						$hierarchy[view] = "type";
						$hierarchy[id] = m($hierarchy[module].'.types.'.$array[1].'.id');
					}
					unset($array[1]);
				}
				// View - plugin // I don't think we would ever use this, leaving off for now (also, nothing built to use the 'plugin.' bit of the view, would have to add that into view module...or remove it and make sure we're not getting a module's home/item view if we have a plugin/plugin_id). Also, would have to add this to all other lookups as well (item/type/custom)...or maybe just add this at the end of the lookup, outside the if/else.
				/*else if(plugin($array[1]) and m($hierarchy[module].'.plugins.'.$array[1])) { // Example: admin/photos/comments
					$hierarchy[plugin] = $array[1];
					if(is_int_value($array[2]) or $array[2] == "new" and in_array($c[area],array('admin','account'))) { // Example: admin/photos/comments/123
						$hierarchy[view] = 'plugin.item';
						$hierarchy[plugin_id] = $array[2];
						unset($array[2]);
					}
					else { // Example: blog/comments, admin/photos/permissions
						$hierarchy[view] = "plugin.home";
						$hierarchy[id] = m($hierarchy[module].'.types.'.$array[1].'.id');
					}
					unset($array[1]);
				}*/
				// View - custom
				/*else if(m($hierarchy[module].'.'.$c[area].'.views.'.$array[1])) { // Example: blog/archive, users/type/admin
					$hierarchy[view] = $array[1];
					unset($array[1]);
					
					// View - custom - id (only applies to 'category' and 'type' views as we want their items for meta info, might deprecate)
					if(in_array($hierarchy[view],array("category","type")) and is_int_value($array[2])) { // Example: news/category/35
						$hierarchy[id] = $array[2];
						unset($array[2]);
					}
				}
				// View - home (default)
				else $hierarchy[view] = 'home'; // Example: blog, blog/non-view-variable
				
				// Remainder
				if($array) {
					// Variables
					$hierarchy[variables] = array_values($array);
				
					// Sub-module
					$_c = $c;
					$_c[parents] = $hierarchy;
					$_hierarchy = $this->hierarchy($array,$_c);
					if($_hierarchy[module]) {
						unset($hierarchy[variables]);
						$_hierarchy[parents][] = $hierarchy;
						$hierarchy = $_hierarchy;
					}
				}
			}
			// First part is a plugin // Haven't built yet
			/*else if(plugin($array[0].'.code')) {
				// Plugin
				$hierarchy[plugin] = $array[0];
				unset($array[0]);
				
				// View - plugin item
				if(is_int_value($array[1]) or $array[1] == "new" and in_array($c[area],array('admin','account'))) { // Example: comments/123, admin/blog/123/comments/456
					$hierarchy[view] = 'plugin.item';
					$hierarchy[plugin_id] = $array[1];
					unset($array[1]);
				}
				// View - home (default)
				else $hierarchy[view] = 'plugin.home'; // Example: comments, admin/comments/non-view-variable
			}*/
			// First variable is not a module or plugin
			/*else {
				// Module specific view without the module in the name? Example: /login
				foreach(m() as $module => $v) {
					if($v[$c[area]][urls]) {
						foreach($v[$c[area]][urls] as $view => $view_v) {
							if($c[area] != "public") $view_v[format] = str_replace($c[area]."/","",$view_v[format]);
							if($view_v[format] == $array[0]) {
								$hierarchy[module] = $module;
								$hierarchy[view] = $view;
								unset($array[0]);
								break;
							}
						}
					}
					if($hierarchy[module]) break;
				}
				// Type specific view without the module or type in the name? Example: /clients
				if(!$hierarchy[module]) {
					foreach(m() as $module => $module_v) {
						if($module_v[types]) {
							foreach($module_v[types] as $type => $v) {
								if($v[$c[area]][urls]) {
									foreach($v[$c[area]][urls] as $view => $view_v) {
										if($view_v[format] == $array[0]) {
											$hierarchy[module] = $module;
											$hierarchy[type] = $type;
											$hierarchy[id] = $v[id];
											$hierarchy[view] = $view;
											unset($array[0]);
											break;
										}
									}
								}
								if($hierarchy[module]) break;
							}
						}
					}
				}
				
				// No module, use home module. Example: /custom-home-variable
				if(!$hierarchy[module] and !$c[parents] and $array and $c[area] == "public") {
					$hierarchy[module] = "home";
					$hierarchy[view] = "item";
					$hierarchy[id] = 1;
				}
				
				// Just variables from here on out
				if($array) $hierarchy[variables] = $array;	
			}
				
			// Reverse parents (from last to first right now, want from first to last)
			if($hierarchy[parents]) $hierarchy[parents] = array_reverse($hierarchy[parents]);
			
			// Area
			$hierarchy[area] = $c[area];
			
			// Return
			return $hierarchy;
		}*/
		
		/**
		 * Compiles and returns the HTML code for the current page.							
		 * 
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the page.
		 */
		function html($variables = NULL) {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Template
			$template = $this->template();
			
			// HTML
			if($template) {
				$html = $this->html_get($template,$variables);
			}
			else {
				$html = $this->view($variables);
			}
			
			// Dependencies
			$html = $this->html_dependent_css($html);
			$html = $this->html_dependent_javascript($html);
			
			// HTTPS
			$html = $this->html_https($html);
			
			// Organize
			$html = $this->html_organize($html);
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;
		}
	
		/**
		 * Replaces all bracket variables (ex: {page_text}) with the item's actual value and returns the resulting HTML.
		 *
		 * Deprecated, except for use with meta title format.					
		 * 
		 * @param string $html The HTML you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The processed HTML of the page.
		 */
		function brackets($html,$c = NULL) {
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// System variables
			/*$system = array(
				'DOMAIN' => DOMAIN,
				'SDOMAIN' => SDOMAIN,
				'D' => D,
				'SERVER' => SERVER,
				'IP' => IP,
				'URL' => URL,
			);*/
			
			// Matches
			preg_match_all('/\{([a-z0-9\._\-]+)\}/i',$html,$matches);
			if(count($matches[1]) > 0) {
				// Changes
				$changes = NULL;
				foreach($matches[1] as $match) {
					$change = NULL;
					//debug("bracket: ".$match);
					
					// Levels
					$levels = explode('.',$match);
					$levels_count = count($levels);
						
					// Header
					/*if($match == "header") $change = $this->header();
					// Footer
					else if($match == "footer") $change = $this->footer();
					// System variables
					else if($_change = $system[$match]) $change = $_change;
					// Page
					else*/ if($levels[0] == "page") {
						// Default
						if(!$levels[2]) $change = $this->$levels[1]; // page.area
						
						// Module
						if($levels[1] == "module" and $levels[2]) { // page.module.settings.image.m.w
							unset($levels[0],$levels[1]);
							$change = m($this->module.'.'.implode('.',$levels));
						}
						// Type
						if($levels[1] == "type" and $levels[2] and $this->type) { // page.type.name
							unset($levels[0],$levels[1]);
							$change = m($this->module.'.types.'.$this->type.'.'.implode('.',$levels));
						}
						// Item
						if($levels[1] == "item" and $this->item) { // page.item.name
							// Empty default
							$change = NULL;
							
							// Value - plugin method
							if($levels[3] and m($this->module.'.plugins.'.$levels[2])) {
								$plugin_instance = $this->item->plugin($levels[2]);
								if(method_exists($plugin_instance,$levels[3])) $change = $plugin_instance->$levels[3]();
							}
							// Value - method
							if(!x($change) and method_exists($this->item,$levels[2])) $change = $this->item->$levels[2]();
							// Value - db variable
							if(!x($change)) $change = $this->item->db($levels[2]);
							// Value - column
							if(!x($change)) $change = $this->item->v($levels[2]);
						}
					}
					// Modules
					else if($levels[0] == "modules") { // modules.settings.settings.site.meta.title
						unset($levels[0]);
						$change = m(implode('.',$levels));
					}
					// Module view
					/*else if(m($levels[0])) { // {module}.{view}.{id|code}, Example: menus.item.main or blog.archive
						$change = $this->view($match);
					}*/
					
					// Change
					$changes['{'.$match.'}'] = $change;
				}
				
				// Replace
				//debug("changes: ".return_array($changes));
				if($changes) {
					$html = str_replace(array_keys($changes),array_values($changes),$html);	
				}
			}
			
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $html;
		}

		/**
		 * Returns (or sets, if pass a 2nd param) the meta value of the specified type			
		 * 
		 * @param string $type The type of meta information you want to set: title, description, keywords, image
		 * @param string $value The meta value you want to save for the given meta information. Default = NULL
		 * @return string The meta value of the given $type.
		 */
		function meta($type,$value = NULL) {
			// Get
			if(!$value) {
				$value = $this->meta[$type];
			}
			
			// Create
			if(!$value) {
				// Defined format (which may or may not take defined value into account)
				$format = m($this->module.'.'.$this->area.'.views.'.$this->view.'.meta.'.$type.'.format');
				if($format) {
					$value = $this->brackets($format);
					$value = str_replace(array(' | | ',' | | ',' | | '),' | ',$value);
					$value = str_replace(array(' >  > ',' >  > ',' >  > '),' > ',$value);
					$value = str_replace(array(' >  | ',' >  | ',' >  | '),' | ',$value);
					if(substr($value,0,3) == " > ") $value = substr($value,3);
					if(substr($value,0,3) == " | ") $value = substr($value,3);
				}
				// Defined value
				else if($this->area == "public" and $this->item) {
					if($plugin_instance = $this->item->plugin('meta')) {
						$method = "meta_".$type;
						$value = $plugin_instance->$method();
					}
				}
				
				// Default
				if(!$value) {
					// Title
					if($type == "title") {
						if($this->area == "admin") $value = ($this->variables[0] ? ucwords($this->variables[0])." | " : "")."Admin | ".m('settings.settings.site.name');
						else $value = m('settings.settings.site.meta.title');
					}
					// Other
					else if($this->area == "public") {
						$value = m('settings.settings.site.meta.'.$type);
					}
				}
			}
			
			// Clean
			$value = $this->meta_clean($value);
			
			// Save
			$this->meta[$type] = $value;
			
			// Return
			return $value;
		}
	
		/**
		 * Gets (and sets globally) the current page's theme.			
		 * 
		 * @return string The code of the current page's theme.
		 */
		function theme() {
			// Saved
			if($this->theme) $theme = $this->theme;
			// Admin
			else if($this->area == "admin") $theme = g('config.admin.theme');
			// Public / Account
			else {
				$theme = $this->c[theme]; // Defined a theme when we created this instance of this class.
				if(!$theme) $theme = $this->item->theme; // Item specific theme
				if(!$theme) $theme = m($this->module.'.settings.theme.'.$this->area.'.views.'.$this->view.'.theme'); // View specific theme
				if(!$theme) $theme = m('settings.settings.theme'); // Global theme
				
				// Exists?
				if(!$theme or !file_exists(SERVER."local/themes/".$theme."/")) {
					// No, use default
					$theme = "default";
				}
			}
			
			// Save
			$this->theme = $theme;
			
			// Return
			return $theme;
		}
	
		/**
		 * Determines the 'public' theme.		
		 * 
		 * Used when in admin, but declaring public CSS for CKEditor.
		 *
		 * @return string The code of the public theme.
		 */
		function theme_public() {
			// Theme
			$theme = m('settings.settings.theme'); // Global theme
				
			// Exists?
			if(!$theme or !file_exists(SERVER."local/themes/".$theme."/")) {
				// No, use default
				$theme = "default";
			}
			
			// Return
			return $theme;
		}
		
		/**
		 * Gets (and sets globally) the current page's template.			
		 * 
		 * @return string The full path to the current page's template file.
		 */
		function template() {
			// Saved
			if($this->template) $file = $this->template;
			// Get
			else {
				// Theme
				if($theme = $this->theme()) {
					// Prefixes
					$prefixes = NULL;
					$language = $_SESSION['__modules']['languages']['selected'];
					if(!$language) $language = m('languages.settings.languages.default');
					if($this->device and $this->device != "default") {
						if($language) $prefixes[] = $this->device.".".$language."."; // Device and language
						$prefixes[] = $this->device."."; // Device
					}
					if($language) $prefixes[] = $language."."; // Language
					$prefixes[] = ""; // No prefix
						
					// Files
					$files = NULL;
					if($this->area != "public") {
						// Local theme (account only)
						if($this->area == "account") $files[] = SERVER."local/themes/".$theme."/".$this->area."/templates/default.php";
						// Local
						$files[] = SERVER."local/".$this->area."/templates/default.php";
						// Local theme - public (account only)
						if($this->area == "account") $files[] = SERVER."local/themes/".$theme."/templates/default.php";
						// Core theme (admin only)
						if($this->area == "admin") $files[] = SERVER."core/framework/".$this->area."/themes/".$theme."/templates/default.php";
						// Core // None currently use this as admin uses 'Core theme' and account will use 'Local theme - public'
						$files[] = SERVER."core/framework/".$this->area."/templates/default.php";
					}
					else {
						// Defined a template when we created this instance of this class.
						if($this->c[template]) $files[] = SERVER."local/themes/".$theme."/templates/".$this->c[template]; 
						// Item specific template
						if($this->item->template) $files[] = SERVER."local/themes/".$theme."/templates/".$this->item->template;
						// View specific template
						if(m($this->module.'.settings.theme.'.$this->area.'.views.'.$this->view.'.template')) $files[] = SERVER."local/themes/".$theme."/templates/".m($this->module.'.settings.theme.'.$this->area.'.views.'.$this->view.'.template'); 
						// Default
						$files[] = SERVER."local/themes/".$theme."/templates/default.php";
					}
					//debug("files: ".return_array($files));
					
					// Exists?
					foreach($files as $file) {
						$name = basename($file);
						$path = str_replace($name,'',$file);
						foreach($prefixes as $prefix) {
							$file = $path.$prefix.$name;
							if(file_exists($file)) break;
							else $file = NULL;
						}
						if($file) break;
					}
				}
			}
			
			// Save
			$this->template = $file;
			
			// Return
			return $file;
		}
	
		/**
		 * Compiles and returns the standard HTML header for a page within the framework including meta data, CSS, and Javascript.				
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the framework's header.
		 */
		function header($c = NULL) {
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// CSS
			if(!$c[css]) $c[css] = $this->css();
			// CSS - public
			if(!$c[css_public] and $this->area == "admin") $c[css_public] = $this->css(array('area' => 'public'));
			
			// Javascript
			if(!$c[javascript]) $c[javascript] = $this->javascript();
			
			// Sitemap
			if(!$c[sitemap] and m('sitemap')) $c[sitemap] = DOMAIN."sitemap.xml";
			
			// RSS
			if(!$c[rss] and plugin('rss')) $c[rss] = DOMAIN."rss.xml";
			
			// Extra
			if(m('settings.settings.site.meta.verification')) $c[extra] .= m('settings.settings.site.meta.verification');
			if(m('settings.settings.site.meta.analytics')) $c[extra] .= m('settings.settings.site.meta.analytics');
			
			// HTML
			$html = parent::header($c);
			
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			return $html;
		}
	
		/**
		 * Compiles and returns the standard HTML footer for a page within the framework.				
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return $string The HTML of the framework's footer.
		 */
		function footer($c = NULL) {
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// HTML
			$html = parent::footer($c);
			
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			return $html;
		}

		/**
		 * Discovers what javascript are needed within framework.									
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the full file paths to the needed javascript files.
		 */
		function javascript($c = NULL) {
			// Theme
			$theme_public = $this->theme_public();
			if($c[area] == "public" and $this->area != "public") $theme = $theme_public;
			else $theme = $this->theme;
			
			// Core
			$files[] = SERVER."core/core/js/jquery.js";
			$files[] = SERVER."core/core/js/js.js";
			if($this->area == "admin") $files[] = SERVER."core/core/admin/js/js.js";
			
			// Core - other
			if(g('config.include.javascript')) {
				foreach(g('config.include.javascript') as $k => $v) {
					if($v[core] and $v[files]) {
						foreach($v[files] as $file) {
							$files[] = SERVER.$file;
						}
					}
				}
			}
			
			// Framework
			$files[] = SERVER."core/framework/js/js.js";
			if($this->area == "admin") {
				$files[] = SERVER."core/framework/admin/js/js.js";
				$files[] = SERVER."core/framework/admin/themes/".g('config.admin.theme')."/js/js.js";
			}
			
			// Local
			$files[] = SERVER."local/js/js.js";
			
			// Modules
			foreach(m() as $module => $v) {
				$files[] = SERVER.$v[path]."js/js.js";
				if($v[core]) $files[] = SERVER.$v[path_local]."js/js.js";
				if($c[area] == "admin") {
					$files[] = SERVER.$v[path]."js/admin.js"; // Should maybe be /modules/{module}/admin/js/js.js
					if($v[core]) $files[] = SERVER.$v[path_local]."js/admin.js";
				}
			}
			// Page module
			if($this->module) {
				$files[] = SERVER.m($this->module.'.path')."js/page.js";
				if(m($this->module.'.core')) $files[] = SERVER.m($this->module.'.path_local')."js/page.js";
			}
			
			// Plugins
			foreach(plugin() as $plugin => $v) {
				$files[] = SERVER.$v[path]."js/js.js";
				if($v[core]) $files[] = SERVER.$v[path_local]."js/js.js";
				if($c[area] == "admin") {
					$files[] = SERVER.$v[path]."js/admin.js"; // Should maybe be /modules/{module}/admin/js/js.js
					if($v[core]) $files[] = SERVER.$v[path_local]."js/admin.js";
				}
			}
			// Page plugin
			if($this->plugin) {
				$files[] = SERVER.plugin($this->plugin.'.path')."js/page.js";
				if(plugin($this->plugin.'.core')) $files[] = SERVER.plugin($this->plugin.'.path_local')."js/page.js";
			}
			
			// Theme
			if($c[area] != "admin") $files[] = SERVER."local/themes/".$theme."/js/js.js";
			if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/js/admin.js"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
				
			// Theme - modules
			foreach(m() as $module => $v) {
				$files[] = SERVER."local/themes/".$theme."/".$v[path_themes]."js/js.js";
				if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."js/admin.js"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
			}
			// Theme - page module
			if($this->module) {
				$files[] = SERVER."local/themes/".$theme."/".m($this->module.'.path_themes')."js/page.js";
			}
			
			// Theme - plugins
			foreach(plugin() as $plugin => $v) {
				$files[] = SERVER."local/themes/".$theme."/".$v[path_themes]."js/js.js";
				if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."js/admin.js"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
			}
			// Theme - page plugin
			if($this->plugin) {
				$files[] = SERVER."local/themes/".$theme."/".plugin($this->plugin.'.path_themes')."js/page.js";
			}
			
			// Device
			if($this->device != "deafult") {
				foreach($files as $file) {
					$files[] = preg_replace('/\/([^\/]+)\.js$/','/'.$this->device.'.$1.js',$file);
				}
			}
			
			// Return
			return files_exists($files);
		}
		
		/**
		 * Discovers what CSS are needed within framework.										
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the full file paths to the needed CSS files.
		 */
		function css($c = NULL) {
			// Config
			if(!$c[area]) $c[area] = $this->area; // The area of CSS you want: public, admin
			
			// Theme
			$theme_public = $this->theme_public();
			if($c[area] == "public" and $this->area != "public") $theme = $theme_public;
			else $theme = $this->theme;
			
			// Core
			$files[] = SERVER."core/core/css/css.css";
			if($c[area] == "admin") $files[] = SERVER."core/core/admin/css/css.css";
			
			// Core - other
			if(g('config.include.css')) {
				foreach(g('config.include.css') as $k => $v) {
					if($v[core] and $v[files]) {
						foreach($v[files] as $file) {
							$files[] = SERVER.$file;
						}
					}
				}
			}
			
			// Framework
			$files[] = SERVER."core/framework/css/css.css";
			if($c[area] == "admin") {
				$files[] = SERVER."core/framework/admin/css/css.css";
				$files[] = SERVER."core/framework/admin/themes/".g('config.admin.theme')."/css/css.css";
			}
			
			// Local - should usually go in the theme's css file, but just in case
			$files[] = SERVER."local/css/css.css";
				
			// Modules
			foreach(m() as $module => $v) {
				$files[] = SERVER.$v[path]."css/css.css";
				if($v[core]) $files[] = SERVER.$v[path_local]."css/css.css";
				if($c[area] == "admin") {
					$files[] = SERVER.$v[path]."css/admin.css"; // Should maybe be /modules/{module}/admin/css/css.css
					if($v[core]) $files[] = SERVER.$v[path_local]."css/admin.css";
				}
			}
			// Page module
			if($this->module) {
				$files[] = SERVER.m($this->module.'.path')."css/page.css";
				if(m($this->module.'.core')) $files[] = SERVER.m($this->module.'.path_local')."css/page.css";
			}
			
			// Plugins
			foreach(plugin() as $plugin => $v) {
				$files[] = SERVER.$v[path]."css/css.css";
				if($v[core]) $files[] = SERVER.$v[path_local]."css/css.css";
				if($c[area] == "admin") {
					$files[] = SERVER.$v[path]."css/admin.css"; // Should maybe be /modules/{module}/admin/css/css.css
					if($v[core]) $files[] = SERVER.$v[path_local]."css/admin.css";
				}
			}
			// Page plugin
			if($this->plugin) {
				$files[] = SERVER.plugin($this->plugin.'.path')."css/page.css";
				if(plugin($this->plugin.'.core')) $files[] = SERVER.plugin($this->plugin.'.path_local')."css/page.css";
			}
			
			// Theme
			if($c[area] != "admin") $files[] = SERVER."local/themes/".$theme."/css/css.css";
			if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/css/admin.css"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
				
			// Theme - modules
			foreach(m() as $module => $v) {
				$files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."css/css.css";
				if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."css/admin.css"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
			}
			// Theme - page module
			if($this->module) {
				$files[] = SERVER."local/themes/".$theme_public."/".m($this->module.'.path_themes')."css/page.css";
			}
			
			// Theme - plugins
			foreach(plugin() as $plugin => $v) {
				$files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."css/css.css";
				if($c[area] == "admin") $files[] = SERVER."local/themes/".$theme_public."/".$v[path_themes]."css/admin.css"; // Bit odd since we're effecting admin theme from public theme...but I'll allow it for now
			}
			// Theme - page plugin
			if($this->plugin) {
				$files[] = SERVER."local/themes/".$theme_public."/".plugin($this->plugin.'.path_themes')."css/page.css";
			}
			
			// Internet Explorer // Think I'd want them to manually add any IE specific styles to the header
			/*$browser = browser(array('array' => 1));
			list($v,$_temp) = explode('.',$browser[version],2);
			if($browser[browser] == "msie") {
				// Generel IE CSS
				$files[] = SERVER."local/themes/".$theme."/css/ie.css";
				// Version specific IE CSS
				$files[] = SERVER."local/themes/".$theme."/css/ie".$v.".css";
			}*/
			
			// Device
			if($this->device != "deafult") {
				foreach($files as $file) {
					$files[] = preg_replace('/\/([^\/]+)\.css$/','/'.$this->device.'.$1.css',$file);
				}
			}
			
			// Return
			return files_exists($files);
		}
		
		/**
		 * Returns HTML for the current page's view.
		 *
		 * @param array $variables An array of variables to pass to the view.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the view (if it existed).
		 */
		function view($variables,$c = NULL) {
			// Config
			if(!x($c[error])) $c[error] = "<div class='core-none'>I'm sorry but I couldn't find the page you were looking for.</div>"; // Error message to display if page not found.
			
			// Array
			$array = array(
				'area' => $this->area,
				'module' => $this->module,
				'type' => $this->type,
				'view' => $this->view,
				'id' => $this->id,
				'code' => $this->code,
				'item_module' => $this->item_module,
				'item_type' => $this->item_code,
				'item_id' => $this->item_id,
				'item_code' => $this->item_code
			);
			
			// View
			$view = view::load($array,$variables);
			// File - want to check if file exists first before getting html so we can show an error if the view/file doesn't exist.
			$file = $view->file();
			
			// Success
			if($file) {
				$html = $view->html();
			}
			// Error
			else {
				$html = $c[error];	
			}
		
			// Return 
			return $html;
		}
	}
}
?>