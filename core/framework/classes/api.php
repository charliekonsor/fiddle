<?php
if(!class_exists('api_framework',false)) {
	/**
	 * An extenion of the api class with functionality specific to the framework.
	 *
	 * Dependencies
	 * - is_int_value()
	 * - debug()
	 *
	 * @package kraken\api
	 */
	class api_framework extends api {
		public $call;
		
		/**
		 * Constructs the class.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::api_framework($c);
		}
		function api_framework($c = NULL) {
			// Config
			if(!$this->x($c[permission])) $c[permission] = g('config.api.permission'); // Check permissions
			
			// Parent
			parent::__construct($c);
		}
	
		/**
		 * Parses the given API URL path to determine what action the user is attempting to do.
		 *
		 * Examples: 
		 * 
		 * URL string /users/123/photos would return:
		 * array(
		 *		'action' => 'get',
		 *		'module' => 'photos',
		 *		'user' => 123
		 * )
		 * 
		 * URL string /photos/45/comments would return:
		 * array(
		 *		'action' => 'get',
		 *		'module' => 'photos',
		 *		'id' => 45,
		 *		'plugin' => 'comments'
		 * )
		 * 
		 * URL string /pages/urls would return:
		 * array(
		 *		'action' => 'get',
		 *		'module' => 'pages',
		 *		'plugin' => 'urls'
		 * )
		 * 
		 * URL string /login would return:
		 * array(
		 *		'action' => 'login',
		 * )
		 * 
		 * URL string /blogs/812/save would return:
		 * array(
		 *		'action' => 'save',
		 *		'module' => 'blogs',
		 *		'id' => 812,
		 * )
		 * 
		 * URL string /galleries/10/galleries_media/53 would return:
		 * array(
		 *		'action' => 'get',
		 *		'module' => 'galleries_media',
		 *		'id' => 53,
		 *		'parents' => array(
		 *			array(
		 *				'module' => 'galleries',
		 *				'id' => 10
		 *			)
		 *		)
		 * )
		 * 
		 * URL string /save?module=photos&id=123 would return:
		 * array(
		 *		'action' => 'save',
		 *		'module' => 'photos',
		 *		'id' => 123
		 * )
		 *
		 * Notes:
		 * It doesn't matter if you include a / at the beginning or the end.
		 * It's also fine if you were to include /api/ at the beginning as that will be stripped.
		 *
		 * Configuration:
		 *
		 * You can also pass some configuration values in the URL. A common example might be passing the 'user' variable (ex: &user=123)
		 * which would limit the items returned to those created by the given user. Example:
		 *
		 * URL string /photos?user=123 would return:
		 * array(
		 *		'action' => 'get',
		 *		'module' => 'photos',
		 *		'user' => 123
		 * )
		 *
		 * You may also pass configuration values for the query via query[c]. For example:
		 *
		 * URL string /news?query[c][order]=news_name ASC
		 * array(
		 *		'actin' => 'get',
		 *		'module' => 'photos',
		 *		'query' => array(
		 *			'c' => array(
		 *				'order' => 'news_name ASC'
		 *			)
		 *		)
		 * )
		 *
		 * This would make the query that retrieves items order the results by news_name.
		 * See the $module->rows() method for more info on what you can configure in the query.
		 *
		 * @param string $url The URL path we want to parse.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of data concerning the call made by the user (see method notes for examples).
		 */
		function parse($url,$c = NULL) {
			// Error
			if(!$url) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Parse
			list($url,$get) = explode('?',$url,2);
			$parts = explode('/',$url);
			$parts = array_values(array_filter($parts));
			if($parts[0] == "api") {
				unset($parts[0]);
				$parts = array_values($parts);	
			}
			$parts_count = count($parts);
			
			// Debug
			debug("url: ".$url.", get: ".$get.", parts: ".return_array($parts),$c[debug]);
			
			// Array
			$call = array();
			
			// Get
			if($get) {
				parse_str($get,$get);
				$call[get] = $get;
			}
			// Post
			if(g('unpure.post')) $call[post] = g('unpure.post');
			
			// Loop through parts
			for($x = 0;$x < $parts_count;$x++) {
				$part = $parts[$x];
				
				// Debug
				if($call) debug("array: ".return_array($call),$c[debug]);
				debug("part: ".$part,$c[debug]);
				
				// Module
				if(m($part)) {
					// Parent
					if($call[module]) {
						if($call[module] == "users" and $call[id]) {
							$call[user] = $call[id];
						}
						else {
							$call[parents][] = array(
								'module' => $call[module],
								'id' => $call[id]
							);
						}
						unset($call[module],$call[id]);
					}
					
					// Module
					$call[module] = $part;
					
					// Id
					$part_next = $parts[$x + 1];
					if(is_int_value($part_next) or $part_next == "new") {
						$call[id] = $part_next;
						$x++;
					}
					
					continue;
				}
				
				// Plugin
				if(plugin($part)) {
					// Parent
					if($call[plugin]) {
						$call[plugin_parents][] = array(
							'plugin' => $call[plugin],
							'plugin_id' => $call[plugin_id]
						);
						unset($call[plugin],$call[plugin_id]);
					}
					
					// Plugin
					$call[plugin] = $part;
					
					// Plugin id
					$part_next = $parts[$x + 1];
					if(is_int_value($part_next) or $part_next == "new") {
						$call[plugin_id] = $part_next;
						$x++;
					}
					
					continue;
				}
				
				// Action - last item only
				if($x == ($parts_count - 1)) {
					// Module actions
					if($call[module]) {
						$module_class = module_api::load($call[module]);
						if(method_exists($module_class,$part)) {
							$call[action] = $part;
							continue;	
						}
					}
					// Custom actions
					else if(in_array($part,array('login'))) {
						$call[action] = $part;	
						continue;	
					}
				}
				
				// Variables
				$call[variables][] = $part;
			}
			
			// Query
			if($get) {
				foreach($get as $k => $v) {
					if($k == "user" and !$call[user]) $call[user] = $v;
					else if($k == "action" and !$call[action]) $call[action] = $v;
					else if($k == "module" and !$call[module]) $call[module] = $v;
					else if($k == "id" and !$call[id]) $call[id] = $v;
					else if($k == "plugin" and !$call[plugin]) $call[plugin] = $v;
					else if($k == "plugin_id" and !$call[plugin_id]) $call[plugin_id] = $v;
					else if($k == "query" and !$call[query]) $call[query] = $v;
				}
			}
			
			// Defaults
			if(!$call[action]) $call[action] = "get";
			if($call[action] == "save" and $call[module] and !$call[id]) $call[id] = "new";
			
			// Debug
			debug("final array: ".return_array($call),$c[debug]);
			
			// Save
			$this->call = $call;
			
			// Return
			return $call;
		}
	
		/**
		 * Makes a call to the API.
		 *
		 * The $call parameter can either be a URL string of the API call or an array of action/module/etc. contained in the call (what's returned when we parse a URL string).
		 * String example: /photos/123/get
		 * Array example: array('action' => 'get','module' => 'photos','id' => 123)
		 *
		 * @param string|array $call Either the URL string that was called or an already parsed array of the call (see method notes for examples).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of data concerning the action the user is attempting (see method notes for examples).
		 */
		function call($call,$c = NULL) {
			// Error
			if(!$call) return;
			
			// Config
			if(!$this->x($c[query_c])) $this->c[query_c] = NULL; // An array of query config to pass to $module->rows() when getting multiple items.
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Parse
			if(is_array($call)) $this->call = $call;
			else $this->parse($call,array('debug' => $c[debug]));
			if(!$this->call) return;
			
			// Debug
			debug("call: ".return_array($this),$c[debug]);
			
			// Key
			if($this->keys) {
				// No / incorrect key
				if(!$this->key or !in_array($this->key,$this->keys)) {
					$results = array(
						'status' => 0,
						'message' => (!$this->key ? "You must pass your 'key' in the API call." : "The key you're using isn't valid.")
					);
				}
			}
			
			// Call
			if(!$results) {
				// Login
				if($this->call[action] == "login") {
					#build this	
				}
				// Plugin
				else if($this->call[plugin]) {
					$class = plugin_api::load($this->call[module],$this->call[id],$this->call[plugin]);
					$method = $this->call[action];
					if(method_exists($class,$method)) {
						$results = $class->$method($this,$c);
					}
				}
				// Module
				else if($this->call[module]) {
					$class = module_api::load($this->call[module]);
					$method = $this->call[action];
					if(method_exists($class,$method)) {
						$results = $class->$method($this,$c);
					}
				}
			}
		
			// Error
			if(!$results) {
				$results[status] = 0;
				$results[message] = "We're not sure what you were trying to do.";
			}
			
			// Debug
			debug("results: ".return_array($results),$c[debug]);
			
			// Save
			$this->results = $results;
			
			// Return
			return $results;
		}
	}
}
?>