<?php
/*if(!class_exists('item_factory',false)) {
	/**
	 * A factory class for accessing either the core item class or a module specific item class.
	 *
	 * !!! Deprecated !!!
	 * Currently use a 'function factory' (item() function) as it's slightly quicker and not as messy (no __call used).
	 * If we did decide to use it again we'd change to 'item' and change the current 'item' class to 'item_core'.
	 *
	 * @package kraken
	 */	
	/*class item_factory {
		/** Stores the instance of whichever item class we use, the core or a module specific one */
		/*var $instance;
		
		/**
		 * Constructs the class
		 *
		 * @param string $module The module this item is in.
		 * @param int The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*function __construct($module = NULL,$id = NULL,$c = NULL) {
			self::item_factory($module,$id,$c);
		}
		function item_factory($module = NULL,$id = NULL,$c = NULL) {
			// Class names
			$class = 'item';
			$class_module = $class.'_'.$module;
			
			// Module class
			if(class_exists($class_module)) $this->instance = new $class_module($module,$id,$c);
			// Core class
			else $this->instance = new $class($module,$id,$c);
		}
		
		/**
		 * Catches all calls to the generic item class and redirects to the correct instance (core class or module specific class).
		 *
		 * @param string $method The method that was called.
		 * @param array $parameters An array of parameters passed to the method.
		 * @return mixed The value returned by the called $method.
		 */
		/*function __call($method,$parameters) {
			if($this->instance) {
				if(method_exists($this->instance,$method)) {
					return call_user_func_array(array($this->instance,$method),$parameters);
				}
			}
		}
		
		/**
		 * Helper function so you can easily get an item's global database variable value. Example: $this->name will return $this->db('name').
		 *
		 * @param string $key The global database variable key you want to get the value of.
		 * @return mixed The corresponding global database value.
		 */
		/*function __get($key) {
			return $this->instance->db($key);	
		}
	}
}*/

if(!class_exists('item',false)) {
	/**
	 * A class for easily accessing an item's data.
	 *
	 * Dependencies
	 * - functions
	 *   - x
	 *   - m
	 *   - cache_get
	 *   - cache_save
	 *   - cache_delete
	 *   - load_class_module
	 * - classes
	 *   - file
	 *   - db
	 * - constants
	 *   - SERVER
	 *
	 * @package kraken
	 */
	class item extends module {
		// Variables
		public $id,/*$type,*/$preview; // string|int // Can't declare $type because then $item->type won't trigger db() which triggers row() so it won't return type unless row has been grabbed previously.
		//public $row = array(); // array // Can't declare $row because then $item->row won't trigger $this->row() when we want it to (namely, when $this->row hasn't been created yet).
		public $row_original = array(); // Holds the original version of any changed values (unencrypted, translated, etc.)
		public /*$database,*/$cache = array(); // array
		
		/**
		 * Loads and returns an instance of either the core item class or (if it exists) the module specific item class.
		 *
		 * Example:
		 * - $item = item::load($module,$id,$c);
		 *
		 * @param string $module The module this item is in.
		 * @param int $id The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core item class or (if it exists) the module specific item class.
		 */
		static function load($module = NULL,$id = NULL,$c = NULL) {
			// Params
			$params = func_get_args(); // Must be outside function call
		
			// Class to load - this also acts to pre-load all type classes which will be needed for looking up a item's type in the cache (if $c[type] isn't defined)
			$class_load = load_class_module(__CLASS__,$params,$module,$c[type]);
			
			// Type - unless we passed type, we need to look that up now as it may alter what class we want to load
			if(($id > 0 or is_array($id)) and $module and !$c[type]) {
				if(m($module.'.types') and m($module.'.db.type')) {
					// Passed array, get type from there
					if(is_array($id)) {
						$c[type] = $id[m(module.'.db.type')];
					}
					// Cached item, get type from there
					else {
						// Languge - needed when building cache string
						if(m('languages') and plugin('international') and !$c[language] and strpos(URL,'/admin') === false) { // Language to display the data in
							$c[language] = $_SESSION['__modules']['languages']['selected']; 
							if(!$c[language]) $c[language] = m('languages.settings.languages.default');
						}
			
						// String
						$language = ($c[language] ? $c[language] : 'eng');
						$string = 'item/'.$module.'/'.$id.'/item';
						$string_local = 'cache.'.str_replace('/','.',$string);
						// Local
						$cache = g($string_local);
						// Global
						if(!$cache) {
							$cache = cache_get($string);
							if($cache) g($string_local,$cache);
						}
						if($cache) {
							$c[type] = $cache->type;
						}
						// Get row, get type from there
						else {
							$db = db::load();
							$row = $db->f("SELECT * FROM `".m($module.'.db.table')."` WHERE ".m($module.'.db.id')." = '".$id."'");
							$c[type] = $row[m($module.'.db.type')];
							$id = $row;
						}
					}
					
					// Class name - re-run this with $c[type]
					if($c[type]) {
						$class_load = load_class_module(__CLASS__,$params,$module,$c[type]); 
					}
				}
			}
			
			// Load class // No way to instanitiate class with array of params in load_class_module() so we'll just return the class name and...
			return new $class_load($module,$id,$c);  // ...manually pass the params here
		}
			
		/**
		 * Constructs the class
		 *
		 * @param string $module The module this item is in.
		 * @param int $id The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module = NULL,$id = NULL,$c = NULL) {
			return self::item($module,$id,$c);
		}
		function item($module = NULL,$id = NULL,$c = NULL) {
			// Error
			if(!$module) return;
			
			// Config
			if(m('languages') and plugin('international') and m($module.'.plugins.international') and strpos(URL,'/admin') === false) { // Language to display the data in
				if(!$c[language]) {
					$c[language] = $_SESSION['__modules']['languages']['selected']; 
					if(!$c[language]) $c[language] = m('languages.settings.languages.default');
				}
			}
			else $c[language] = NULL;
			//if(!x($c[cached])) $c[cached] = 1; // Get cached data (if available) // Defaults in module class (parent)
			//if(!x($c[cache])) $c[cache] = 1; // Cache data for future use // Defaults in module class (parent)
			
			// Parent
			parent::__construct($module,$c);
			
			// ID
			if($id) {
				// Cache
				$cache = 0;
				
				// Passed an array of item's db row
				if(is_array($id)) {
					// All data in array?
					$all = 1;
					$columns = $this->db->table_columns(m($module.'.db.table'));
					foreach($columns as $k => $v) {
						if(!isset($id[$k])) {
							$all = 0;
							break;	
						}
					}
					// Yes, use
					if($all) {
						$this->row = $id;
						$id = $this->row[m($module.'.db.id')];
						$cache = 1;
				
						// Language - probably a more elegant way to do this (so we're not copying code we use in row()), but quick fix for now
						if($c[language] and $c[language] != m('languages.settings.languages.default')) {
							$rows = $this->db->q("SELECT * FROM items_international WHERE module_code = '".$module."' AND item_id = '".$id."' AND plugin_code = '' AND language_code = '".a($c[language])."'");
							while($row = $this->db->f($rows)) {
								$this->row_original[$row[item_column]] = $this->row[$row[item_column]];
								$this->row[$row[item_column]] = $row[international_value];
							}
						}
					}
					// No, get id from array
					else {
						$id = $id[m($module.'.db.id')];
					}
				}
				
				if($id > 0) {
					// Set ID
					$this->id = $id;
					
					// Preview?
					if($_GET['approval_id']) {
						$this->preview = $_GET['approval_id']; // We'll check if this is the correct approval id in row().
						$this->row = $this->row_original = NULL; // Must be empty otherwise we won't ever retrieve preview values (as we think we already have $this->row).
					}
					
					// Cache
					if($cache) $this->cache_save();
				}
			}
		}
		
		/**
		 * Helper function so you can easily get an item's global database variable value. Example: $this->name will return $this->db('name').
		 *
		 * @param string $key The global database variable key you want to get the value of.
		 * @return mixed The corresponding global database value.
		 */
		function __get($key) {
			if($key == "row") {
				return $this->row();
			}
			else {
				$variables = array( // Variables which shouldn't give a response in __get (as they're declared variables).
					'module',
					'id',
					//'type', // Don't know this until we get row()
					'row', // Special case in __get()
					'row_original',
					//'database', // No longer use
					'cache', // Array that stores special cached data (files, urls, etc.) which takes awhile to recreate otherwise
					'cache_local', // Array that stores special cached data, but only locally (on one page), not globally
					'preview',
					'preview_merged'
				);
				if(!in_array($key,$variables)) {
					// Get value
					$value = $this->db($key);
					// Set globally (3x quicker 2nd time around)
					$this->$key = $value;
					
					// Return value
					return $value;
				}
			}
		}
		
		/**
		 * Gets the item's row from the database (if not already retrieved).
		 *
		 * @return array The item's database row array .
		 */
		function row() {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Already got
			if($this->row) {
				return $this->row;	
			}
			
			// Speed
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Cached?
			$cache = $this->cache_get();
			if($cache->row) {
				// Debug
				if($this->c[debug]) debug("Returning cached item: ".return_array($cache),$this->c[debug]); // return_array() consumes lots of memory, check debug first
				
				// Restore
				foreach($cache as $k => $v) {
					//debug("setting ".$k." to ".$v,$this->c[debug]);
					$this->$k = $v;
				}
				
				// Speed
				function_speed(__CLASS__."->".__FUNCTION__,$f_r,1);
				
				// Return
				return $this->row;
			}
			
			// Default
			if(!$this->row and m($this->module.'.db.table') and m($this->module.'.db.id')) {
				// Select
				$select = "*";
				if($encrypt = m($this->module.'.db_encrypt')) {
					foreach($encrypt as $column) {
						$column_alias = "_decrypted_".$column;
						$select .= ", AES_DECRYPT(".$column.",'".g('config.db.mysql.encryption')."') ".$column_alias;	
					}
				}
				
				// Row
				$query = "SELECT ".$select." FROM `".m($this->module.'.db.table')."` WHERE ".m($this->module.'.db.id')." = '".$this->id."'";
				debug($query,$this->c[debug]);
				$this->row = $this->db->f($query);
				
				// Decrypt
				if($encrypt) {
					foreach($encrypt as $column) {
						$column_alias = "_decrypted_".$column;
						$this->cache[decrypted][$column] = $this->row[$column_alias];
						unset($this->row[$column_alias]);
					}
				}
				
				// Language
				if($this->c[language] and $this->c[language] != m('languages.settings.languages.default')) {
					$rows = $this->db->q("SELECT * FROM items_international WHERE module_code = '".$this->module."' AND item_id = '".$this->id."' AND plugin_code = '' AND language_code = '".a($this->c[language])."'");
					while($row = $this->db->f($rows)) {
						$this->row[$row[item_column]] = $row[international_value];
					}
				}
				
				//print "got row: ".$query."<br />";
				//print_array($this->row);
			}
			
			// Preview
			if($this->preview and !$this->preview_merged) {
				// Get approval item
				$row = approval_item($this->module,$this->id,array('return' => 'approval_row'));
				if($row) {
					// Values
					$values = data_unserialize($row[approval_values]);
					
					// Merge
					if(is_array($this->row)) $this->row = array_merge($this->row,$values);
					else $this->row = $values;
					$this->preview_merged = 1;
					
					// Visible (user has perimssion to view this preview)
					if(m($this->module.'.db.visible')) {
						$this->row[m($this->module.'.db.visible')] = 1;
					}
				}
				else {
					$this->preview = 0;
				}
			}
			
			// Success
			$this->id = $this->row[m($this->module.'.db.id')];
			if($this->id) {
				// Type
				if(!$this->type) $this->type = $this->db('type');
				
				// Cache
				$this->cache_save();
			}
				
			// Speed
			function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $this->row;
		}
		
		/**
		 * Returns the item's stored value for the given database column.
		 *
		 * @param string $column The column in the item's database table you want to get the value of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The value stored in the database for the given column for this item.
		 */
		function v($column,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$column) return;
			
			// Config
			if(!x($c[translate])) $c[translate] = 1; // Return translated version of the value (if user's selected language is different than the default language)
			
			// Row
			if(!$this->row) $this->row();
			if($this->row) {
				// Encrypted?
				if($encrypt = m($this->module.'.db_encrypt')) {
					// Yes, return decrypted value
					if(in_array($column,$encrypt)) {
						return $this->v_decrypted($column);
					}
				}
				
				// Value
				if($c[translate]) $value = $this->row[$column];
				else {
					$value = $this->row_original[$column]; // Get original value (only exists if there are other versions of the value, ex: translated version)
					if(!x($value)) $value = $this->row[$column]; // Didn't have an 'original' value, meaninig it never changed, use $this->row
				}
				
				// Return
				return $value;
			}
		}
		
		/**
		 * Returns the item's decrypted value for the given database column (or columns if an array is passed).
		 *
		 * @param string|array $column A single column or an array of columns in the item's database table you want to get the decrypted value of.
		 * @return mixed The decrypted value stored in the database for the given column for this item or an array of decrypted values if an array was inputed.
		 */
		function v_decrypted($column) {
			// Error
			if(!$this->module or !$this->id or !$column) return;
			
			// Input
			if(is_array($column)) {
				$columns = $column;
				$array = 1;
			}
			else $columns = array($column);
			$columns = array_combine($columns,$columns);
			$columns_values = NULL;
			$columns_get = $columns;
			
			// Cached
			foreach($columns as $column) {
				if(x($this->cache[decrypted][$column])) {
					$columns_values[$column] = $this->cache[decrypted][$column];
					unset($columns_get[$column]);
				}
			}
			
			// Get
			if($columns_get) {
				$query = NULL;
				foreach($columns_get as $column) {
					$query .= (!$query ? "SELECT " : ", ")."AES_DECRYPT(".$column.",'".g('config.db.mysql.encryption')."') ".$column;	
				}
				$query .= " FROM `".m($this->module.'.db.table')."` WHERE ".m($this->module.'.db.id')." = '".$this->id."'";
				$row = $this->db->f($query);
				
				foreach($columns_get as $column) {
					$columns_values[$column] = $row[$column];
					$this->cache[decrypted][$column] = $row[$column];
				}
				
				// Cache
				$this->cache_save();
			}
			
			// Return
			if($array == 1) return $columns_values;
			else return array_first($columns_values);
		}
		
		/**
		 * Returns the item's stored value for the given global database variable key.
		 *
		 * For example, if the module had defined the database variable of 'name' as connecting to the 'photo_name' column, $item->db('name') would return the item's photo_name value.
		 *
		 * @param string $key The global database variable key you want to get the corresponding value of for this item.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The value stored in the database column which corresponds to the given gloabal database variable key.
		 */
		function db($key,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$key) return;
			
			// Config
			if(!x($c[translate])) $c[translate] = 1; // Return translated version of the value (if user's selected language is different than the default language)
			
			// Name Variations (common misuses of global db keys)
			//if($key == "categories") $key = "category"; // Not a db variable yet
			
			// Row
			if(!$this->row) $this->row();
			if($this->row) {
				//debug("db(".$key.")");
			
				// Get saved value // Skipping caching for now, causes more headaches (refresh()) than it probably solves
				/*if(x($this->database[$key])) {
					//debug("got cached db value for `".$key."`: ".$this->database[$key]);
					return $this->database[$key];
				}
				// Get new value
				else {*/
					//debug("getting new value for `".$key."`");
					
					// Row
					$row = $this->row;
					if(!$c[translate] and $this->row_original) $row = array_merge($row,$this->row_original);
					
					// Key
					$_key = m($this->module.'.db.'.$key,$this->type);
					if($_key) {
						//debug("_key `".$_key."`");
						// Value
						if(strstr($_key,' ')) $value = string_format($_key,$row);
						else $value = $row[$_key];
					}
					// Missing (but is a core definition)
					else if(g('framework.db.'.$key)) {
						//debug("no global database definition, cheching it it defaults to true");
						// Defaults to true
						if(g('framework.db.'.$key.'.default') == 1) {
							//debug("it does");
							$value = 1;
						}
					}
					//debug("new value for `".$key."`: ".$value);
				
					// Save
					//$this->database[$key] = $value;
					
					// Return
					return $value;
				//}
			}
		}
		
		/**
		 * Returns an array of the item data and global values.
		 *
		 * @return array An array of the item's data and global values.
		 */
		function a() {
			$array = array();
			foreach(m($this->module.'.db') as $k => $v) {
				if($k != "table") {
					$database[$k] = $this->db($k);	
				}
			}
			if($database) $array = array_merge($array,$database);
			if($this->row) $array = array_merge($array,$this->row); // $this->row() gets called in $this->db()
			return $array;
		}
		
		/**
		 * Returns an instance of the file plugin and sets the file key.
		 *
		 * @param string $column The database column in the item's row that contains the file's name. Can also be db variable, example: db.image.
		 * @param array An array of configuration values. Default = NULL
		 * @return object An instance of the files plugin class.
		 */
		function file($column,$c = NULL) {
			// Config String
			$c_string = cache_config($c);
		
			// DB definition - passed db.definition, get real field name so we can use that with custom info (_source, _file, _extension, etc.)
			if(strstr($column,'db.') and m($this->module.'.'.$column)) {
				if(!$c[type]) $c[type] = substr($column,3); // Use db variable to define the 'type' of file this is
				$column = m($this->module.'.'.$column);
			}
				
			// Get cached plugin item
			if($plugin_item = $this->cache_get('files/'.$column.'/'.$c_string)) {
				return $plugin_item;
			}
			
			// Row - just to make sure we've got all values
			$this->row();
			
			// International - 'original' value exists so must have changed (aka, been translated)
			if($this->row_original[$column]) {
				// Is there an interntional value stored?
				$row0 = $this->db->f("SELECT * FROM ".plugin('international.db.table')." WHERE ".plugin('international.db.parent_module')." = '".$this->module."' AND ".plugin('international.db.parent_id')." = '".$this->id."' AND plugin_code = '' AND item_column = '".$column."' AND language_code = '".$this->c[language]."'");
				// Yes, get corresponding file info
				if($row0[id]) {
					$row = $this->db->f("SELECT * FROM ".plugin('files.db.table')." WHERE ".plugin('files.db.parent_module')." = '".$this->module."' AND ".plugin('files.db.parent_id')." = '".$this->id."' AND plugin_code = 'international' AND plugin_id = '".$row0[id]."' AND file_column = 'international_value'");
				}
			}
			// Default
			if(!$row[id]) {
				$row = $this->db->f("SELECT * FROM ".plugin('files.db.table')." WHERE ".plugin('files.db.parent_module')." = '".$this->module."' AND ".plugin('files.db.parent_id')." = '".$this->id."' AND plugin_code = '' AND file_column = '".$column."'");
			}
			
			// Preview
			if($this->preview) {
				if($this->row and $this->preview) { // $this->row will make sure we've gotten our row (and preview data), $this->preview will check to make sure 
					if($this->row[__plugins][files][$column]) {
						// Merge proposed change with old data
						if($row) $row = array_merge($row,$this->row[__plugins][files][$column]);
						else $row = $this->row[__plugins][files][$column];
						
						// Turn off caching for plugin_item class
						$c[cached] = 0;
						$c[cache] = 0;
					}
				}
			}
			
			// Plugin item - create even if no 'row' so any methods we call on this will still work
			$plugin_item = $this->plugin_item('files',$row,$c);
			
			// Cache
			if($row) {
				$this->cache_save('files/'.$column.'/'.$c_string,$plugin_item);
			}
			
			// Return
			return $plugin_item;
			
			// Old method
			// Plugin
			/*$plugin_instance = $this->plugin('files');
			// File
			$plugin_instance->file = NULL; // Already using 'unique' instances (not caching) in $item->plugin(), but this could be an alternative
			$plugin_instance->file($column,$c);
			// Return
			return $plugin_instance;*/
		}
		
		/**
		 * Processes the value for the given file column, creating thumbs, converting, etc.
		 *
		 * You can configure how it thumbs, converts, etc. via the $c parameter. 
		 *
		 * If you don't, we'll try to use the configuration saved in the form / module for this.
		 *
		 * You can also pass a $c[type] (we'll try to guess this from the form) which helps determine
		 * the config values to get from the module.
		 *
		 * Note, you can also pass the full path to the file we want to process (as opposed to having this
		 * method retrieve it using $item->file()). This may make it easier to import old files.
		 *
		 * @param string $column The database column in the item's row that contains the file you want to process. Can also be db variable, example: db.image.
		 * @param array An array of configuration values. Default = NULL
		 * @return object The pluging item object.
		 */
		function file_process($column,$c = NULL) {
			// Config - same as file_process() function, see /core/core/functions/core.php for more
			if(!$c[file]) $c[file] = NULL; // Full path to the file you want to process (instead of having this grab it from the db).
			if(!x($c[form])) $c[form] = 1; // Get configurations from the module's form. Often more accurate, but takes longer.
			if(!$c[type]) $c[type] = NULL; // Can pass a 'type' of file, helps to determine what configurationt o get from the module/global config.php. We'll try to get this from the form first too.
			if(!x($c[extensions_action])) $c[extensions_action] = "recreate"; // How to handle extensions: get (just get them if they exist), create (create them if they don't exist), recreate (create them, even if they do exist)
			if(!x($c[thumbs_action])) $c[thumbs_action] = "recreate"; // How to handle thumbs: get (just get them if they exist), create (create them if they don't exist), recreate (create them, even if they do exist)
			if(!x($c[process])) $c[process] = 1; // Process item after saving.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Config keys - used when merging form config with what we passed
			$config_keys = array(
				'thumbs',
				'extensions',
				'storage',
				'type', // Technically not in the file_process() function $c array, but we pass it as the 2nd param
			);
		
			// DB definition - passed db.definition, get real field name
			if(strstr($column,'db.') and m($this->module.'.'.$column)) {
				if(!$c[type]) $c[type] = substr($column,3); // Use db variable to define the 'type' of file this is
				$column = m($this->module.'.'.$column);
			}
			
			// Plugin
			$plugin = $this->file($column);
			
			// File
			if($c[file]) $file = file::load($c[file]);
			else $file = $plugin->file['class'];
			if(!$file->exists) return;
			
			// Form
			$form = 0;
			if($c[form]) {
				$form = $this->form();
				$input = $form->input_exists($column);
				if($input) {
					$input->html_row($form); // Need to force framework config to be calculated...hacky way to get this, but will work until I figure out how to set configuration when input's created (aka, when we don't know the $form->module)
					//debug("input: ".return_array($input),$c[debug]);
					foreach($input->c as $k => $v) {
						if(in_array($k,$config_keys)) {
							if(!x($c[$k])) $c[$k] = $v;
						}
					}
					$form = 1;
				}
			}
			// Module
			if(!$form) {
				if(!$c[type]) $c[type] = "file";
				if(!$c[storage]) $c[storage] = m($this->module.'.uploads.types.'.$c[type].'.storage');
				if(!x($c[thumbs])) $c[thumbs] = m($this->module.'.uploads.types.'.$c[type].'.thumbs');
				else if(is_array($c[thumbs])) $c[thumbs] = array_merge_associative(m($this->module.'.uploads.types.'.$c[type].'.thumbs'),$c[thumbs]);
				if(!x($c[extensions])) $c[extensions] = m($this->module.'.uploads.types.'.$c[type].'.extensions');	
			}
			
			// Process
			$results = file_process($file->file,$c[type],$c);
			
			// Values
			$values[file_column] = $column;
			$values[file_name] = $results[name];
			$values[file_type] = $results[type];
			$values[file_path] = $results[path];
			$values[file_extension] = $results[extension];
			$values[file_width] = $results[width];
			$values[file_height] = $results[height];
			$values[file_length] = $results[length];
			$values[file_size] = $results[size];
			$values[file_extensions] = data_serialize($results[extensions]);
			$values[file_thumbs] = data_serialize($results[thumbs]);
			$values[file_storage] = $results[storage];
			if((!$plugin->v('file_thumb') or $c[file]) and $results[thumbs]) {
				foreach($results[thumbs] as $k => $v) {
					$values[file_thumb] = array_first_key($v);
					break;
				}
			}
			debug("file values: ".return_array($values),$c[debug]);
			
			// Save
			$plugin->save($values,array('process_item' => $c[process],'debug' => $c[debug]));
			
			// Reload
			$plugin = $this->file($column);
			
			// Return
			return $plugin;
		}
		
		// image() - Returns given key's image in defined format
		function image($key = NULL,$c = NULL) {
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Default key
			if(!$key) $key = "db.image";
			// Key is global database definition
			if(in_array($key,array('file','image')) and !$this->v($key)) $key = "db.".$key;
			
			// Config
			if(!$c[thumb]) $c[thumb] = NULL; // Thumb of image we want to return: l, m, s, t
			if(!x($c[debug])) $c[debug] = 0; // Debug
			// Config - don't use until image() call, but can be passed through this function
			/*if(!x($c[box])) $c[box] = 1; // Add class='box' to image
			if(!$c[alt]) $c[alt] = NULL; // <img> tag's alt attribute (will default to base filename)
			if(!$c[attributes]) $c[attributes] = NULL; // Additional attributes to add to the <img> tag
			if(!$c[link]) $c[link] = NULL; // Where we want the image to link to
			if(!$c[link_title]) $c[link_title] = NULL; // Title attribute of link's <a> tag
			if(!x($c[thickbox])) $c[thickbox] = 0; // Open link in thickbox (or active equivalent like colorbox or fancybox)
			if(!$c[width]) $c[width] = NULL; // Width of image, not necessary, but speeds up page load time if passed
			if(!$c[height]) $c[height] = NULL; // Height of image, not necessary, but speeds up page load time if passed
			if(!$c[min_width]) $c[min_width] = NULL; // Minimum width to display image
			if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display image
			if(!x($c[timestamp])) $c[timestamp] = 0; // Append url with timestamp so browser won't use cached file*/
			// Config String
			$c_string = cache_config($c);
			
			// Debug
			debug("<b>\$".__CLASS__."->image($key);</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Get cached value
			if($value = $this->cache[images][$key][$c_string]) {
				function_speed(__CLASS__."->".__FUNCTION__,$f_r,1);
				return $value;	
			}
			// Get new value
			else {
				// URL
				// Get's size, which is faster for page load, but slower on backend because we have to look up every time. Use this if we start storing w/h in db.
				/*$file = $this->file($key,array('thumb' => $c[thumb],'debug' => $c[debug]));
				$url = $file->url();
				if(!$c[width]) $c[width] = $file->width();
				if(!$c[height]) $c[height] = $file->height();*/
				
				// Doesn't get size, but is faster
				$file = $this->file($key,array('thumb' => $c[thumb],'debug' => $c[debug]));
				$url = $file->url();
				
				// Image
				$value = image($url,$c);
				
				// Save Value
				$this->cache[images][$key][$c_string] = $value;
			
				// Cache
				$this->cache_save();
			
				function_speed(__CLASS__."->".__FUNCTION__,$f_r);
				return $value;
			}
		}
		
		// video() - Returns given key's video in defined format
		function video($key,$c = NULL) {
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Default key
			if(!$key) $key = (m($this->module.'.db.video') ? 'db.video' : 'db.file');;
			// Key is global database definition
			if(in_array($key,array('file','video')) and !$this->v($key)) $key = "db.".$key;
			// Key is URL
			if(substr($key,0,7) == "http://" or substr($key,0,8) == "https://") {
				$c[file] = $key;
				$key = NULL;
			}
			
			// Config
			if(!$c[format]) $c[format] = NULL; // Video format (will use file extension of default file if none)
			if(!$c[width]) $c[width] = NULL; // Width of video
			if(!$c[height]) $c[height] = NULL; // height of video
			if(!$c['return']) $c['return'] = 'html'; // What to return: html (HTML/JS for display), url (video url), swf (.swf file with video vars), embed (html w/o js)
			if(!x($c[debug])) $c[debug] = 0; // Debug
			// Config - don't use until video() call, but can be passed through this function
			/*if(!x($c[auto])) $c[auto] = 1; // Autoplay the video
			if(!$c[min_width]) $c[min_width] = 300; // Minimum width to display video
			if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display video
			if(!x($c[download])) $c[download] = 1; // Show download link if other versions don't work
			if(!$c[id]) $c[id] = "player_".random(); // ID of player element*/
			// Config String
			$c_string = cache_config($c);
			
			// Debug
			debug("<b>\$".__CLASS__."->video($key);</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Get cached value
			if($value = $this->cache[videos][$key][$c_string]) {
				function_speed(__CLASS__."->".__FUNCTION__,$f_r,1);
				return $value;	
			}
			// Get new value
			else {
				// URL
				$file = $this->file($key,array('format' => $c[format],'debug' => $c[debug]));
					
				// Embed Code
				/*if($embed = $file->embed()) { // Not yet built
					if($c['return'] == "url" or $c['return'] == "swf") $value = $file->url(); // url or swf
					else $value = $embed; // html or embed
				}
				// Default
				else {*/
					$url = $file->url();
					if(!$c[width]) {
						if($c[height]) $c[width] = ($c[height] * $file->width()) / $file->height(); // Keep aspect ratio using defined height
						else $c[width] = $file->width();
					}
					if(!$c[height]) {
						if($c[width]) $c[height] = ($c[width] * $file->height()) / $file->width(); // Keep aspect ratio using defined width
						else $c[height] = $file->height();
					}
					if(!$c[format]) $c[format] = $file->extension();
				//}
				
				// Video
				if(!$value) $value = video($url,$c);
				
				// Save Value
				$this->cache[videos][$key][$c_string] = $value;
			
				// Cache
				$this->cache_save();
			
				function_speed(__CLASS__."->".__FUNCTION__,$f_r);
				return $value;
			}
		}
		
		// audio() - Returns given key's audio in defined format
		function audio($key,$c = NULL) {
			$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Default key
			if(!$key) $key = "db.audio";
			// Key is global database definition
			if(in_array($key,array('file','audio')) and !$this->v($key)) $key = "db.".$key;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			// Config - don't use until audio() call, but can be passed through this function
			/*if(!x($c[timestamp])) $c[timestamp] = 0; // Append url with timestamp so browser won't use cached file*/
			// Config String
			$c_string = cache_config($c);
			
			// Debug
			debug("<b>\$".__CLASS__."->audio($key);</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Get cached value
			if($value = $this->cache[audio][$key][$c_string]) {
				function_speed(__CLASS__."->".__FUNCTION__,$f_r,1);
				return $value;	
			}
			// Get new value
			else {
				// URL
				$file = $this->file($key,array('debug' => $c[debug]));
				$url = $file->url();
				
				// Audio
				$value = audio($url,$c);
				
				// Save Value
				$this->cache[audio][$key][$c_string] = $value;
			
				// Cache
				$this->cache_save();
			
				function_speed(__CLASS__."->".__FUNCTION__,$f_r);
				return $value;
			}
		}
		
		/**
		 * Determines the URL for viewing an item in the given area.
		 *
		 * @param string $area The area of the site where this URL will link to: public [default], admin, account
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL of the item in the given area (if it exists).
		 */
		function url($area = NULL,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!x($c[https])) $c[https] = 0; // Make domain HTTPS
			if(!$c[module]) $c[module] = $this->module; // The module of the view you're using to define the URL format
			if(!$c[type]) $c[type] = ($this->module == $c[module] ? $this->type : NULL); // The module type of the view you're using to define the URL format
			if(!$c[view]) $c[view] = "item"; // The view you're using to define the URL format
			if(!$c[parent_module]) { // Parent module
				$c[parent_module] = $this->db('parent_module');
				if(!$c[parent_module]) $c[parent_module] = m($this->module.'.parent');
			}
			if(!$c[parent_id]) $c[parent_id] = $this->db('parent_id'); // Parent id
			if(!$c[name]) $c[name] = NULL; // If passed it will overwrite the default 'name' value
			if(!x($c[saved])) $c[saved] = 1; // Try and get saved value (don't want if we're recreating item's url in save_urls())
			if(!x($c[cache])) $c[cache] = $this->c[cache]; // Cache the URL after retrieving it.
			if(!x($c[cached])) $c[cached] = $this->c[cached]; // Get cached value (if available)
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Debug
			debug(__CLASS__."->".__FUNCTION__."($area)",$c[debug]);
			
			// Variables
			$url = NULL;
			if(!$area) $area = "public";
			
			// Cache string
			$_c = $c;
			unset($_c[https]);
			$c_string = cache_config($_c);
			
			// Get saved value
			if($value = $this->cache[urls][$area.".".$c_string] and $c[cached]){
				$url = $value;	
			}
			// Get new value
			else {
				// Domain
				$domain = domain_page();
				
				// Format
				$format = m($c[module].'.'.$area.'.views.'.$c[view].'.urls.0.format',$c[type]);
				debug("format: ".$format,$c[debug]);
				if($format) {
					// Saved (only save for 'public' URL's at the moment)
					if($c[saved] and $area == "public") {
						$row = $this->db->f("SELECT * FROM items_urls WHERE module_area = '".$area."' AND module_code = '".$c[module]."' AND module_view = '".$c[view]."' AND item_module = '".$this->module."' AND item_id = '".$this->id."' AND url_primary = 1");
						if($row[id]) $url = $domain.($row[url] ? $row[url] : "");
					}
					// Create
					if(!$url and $format) {
						// Name
						if(!$c[name]) {
							//$c[name] = string_url_encode($this->db('name')); // Not sure why I was using string_url_encode() here when we use it below too already. At any rate, if removing this causes an issue, turn it back on here and in the $plugin_url->item_save() class.
							$c[name] = $this->db('name',array('translate' => 0));
						}
						debug("name: ".$c[name],$c[debug]);
						
						// New
						$new = 0;
						if(!$this->id) $new = 1;
						
						// Brackets
						$url = $format;
						preg_match_all('/\{([a-z_\-0-9]+)\}/i',$format,$matches);
						if(count($matches[0]) > 0) {
							foreach($matches[1] as $x => $match) {
								$value = NULL;
								if($match == "id" and $new) $value = "new";
								else if($match == "home") $value = " "; // Will get removed when we explode/implode, needed now for x($value) though
								else if($match == "module") $value = $c[module];
								else if($match == "name") $value = $c[name];
								else if($match == "parent_module") $value = $c[parent_module];
								else if($match == "parent_id") $value = $c[parent_id];
								else if($match == "parent_parent_module" and $c[parent_module]) {
									if($parent = m($c[parent_module].'.parent')) $value = $parent;
									else if($c[parent_id] > 0) {
										$parent_item = item::load($c[parent_module],$c[parent_id]);
										$value = $parent_item->db('parent_module');
									}
								}
								else if(m($this->module.'.db.'.$match)) $value = $this->db($match);
								else $value = $this->v($match);
								if(!x($value)) {
									$url = 0;
									break;	
								}
								$url = str_replace('{'.$match.'}',$value,$url);
							}
						}
						
						if($url) {
							// Encode - might want to do something so we're not re-enconding already encoded things, really just want to encode entities like (™)
							$ex = explode('/',$url);
							foreach($ex as $k => $v) {
								$ex[$k] = string_url_encode($v);
							}
							$url = implode('/',$ex);
							
							// Type
							if($new and $this->type) $url .= "?type=".$this->type;
						
							// Domain
							$url = $domain.$url;
						}
					}
			
					// Save
					if($c[cache]) $this->cache[urls][$area.".".$c_string] = $url;
				}
			}
					
			// HTTPS
			if($c[https] == 1) $url = str_replace(DOMAIN,SDOMAIN,$url);
				
			// Debug
			debug("url: ".$url,$c[debug]);
				
			// Return
			if($url) return $url;
		}
		
		/**
		 * Saves an item, either inserting it or (if $this->id) updating the existing row, then processes its data.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the new item.
		 */
		function save($values = NULL,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!x($c[process])) $c[process] = 1; // Do you want to process the item's results after we're done saving?
			if(!x($c[debug])) $c[debug] = 0; // Debug
					
			// Debug
			debug("<b>\$".__CLASS__."->".__FUNCTION__."();</b>",$c[debug]);
			
			// Plugins
			$plugins_values = $values[__plugins];
			if(is_string($plugins_values)) $plugins_values = data_unserialize($plugins_values); // Make sure unserialized (just the top level). If we called $item->save() from within a plugin which received values that contained a serialzied __plugins key (aka, there was plugin data in the plugin) we need to make sure it's unserialized here
			
			// Row - needed to check if $this->id actually exists
			$this->row();
			
			// Statement
			if($this->id > 0) $statement = "UPDATE";
			else $statement = "INSERT";
			
			// Values
			$values = $this->save_values($values,$statement);
			if($values) {
				// Debug
				if($this->c[debug]) debug("\$".__CLASS__."->".__FUNCTION__."() values: ".return_array($values),$c[debug]); // return_array() can consume a lot of memory, check debug first
				
				// Old array
				$c[old] = NULL;
				if($this->id > 0) $c[old] = $this->row();
				
				// Values - encrypt
				if($encrypt = m($this->module.'.db_encrypt')) {
					foreach($values as $k => $v) {
						if(in_array($k,$encrypt)) {
							if(substr($v,0,4) != "AES_") $values[$k] = "AES_ENCRYPT('".$v."','".g('config.db.mysql.encryption')."')";	
						}
					}
				}
				
				// Values - string
				$values_string = $this->db->values($values);
				
				// Build query
				if($statement == "INSERT") $query = "INSERT INTO ".m($this->module.'.db.table')." ".$values_string;
				if($statement == "UPDATE") $query = "UPDATE `".m($this->module.'.db.table')."` ".$values_string." WHERE ".m($this->module.'.db.id')." = '".$this->id."'";
				
				// Debug
				debug($query,$c[debug]);
				
				// Run query
				$result = $this->db->q($query);
				
				// Id (if insert)
				if($statement == "INSERT") $this->id = $result;
				// Refresh cache (if updating)
				else $this->refresh($values);
			}
			
			// Plugins
			$plugins = m($this->module.'.plugins');
			debug("plugins: ".return_array($plugins),$c[debug]);
			debug("plugins_values: ".return_array($plugins_values),$c[debug]);
			if($plugins) {
				$c_plugins = $c;
				$c_plugins[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				foreach($plugins as $plugin => $plugin_v) {
					$plugin_instance = $this->plugin($plugin,$c);
					if(method_exists($plugin_instance,$method)) {
						debug("Saving plugin ".$plugin,$c[debug]);
						$c_plugins['isset'] = isset($plugins_values[$plugin]);
						$c_plugins[process_item] = 0; // We're going to process on our own shortly
						$plugin_instance->$method($plugins_values[$plugin],$c_plugins);
					}
				}
			}
			/*if($plugins_values) {
				$c[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				foreach($plugins_values as $plugin => $plugin_values) {
					$plugin_instance = $this->plugin($plugin,$c);
					if(method_exists($plugin_instance,$method)) {
						debug("Saving plugin ".$plugin,$c[debug]);
						$plugin_instance->$method($plugin_values,$c);
					}
				}
			}*/
			
			// Process
			if($c[process]) {
				$c[save][values] = $values;
				$this->process($c);
			}
			
			// Return ID
			return $this->id;
		}
		
		/**
		 * Processes an array of values we're about to save.
		 * 
		 * Converts all keys in the values array that are prefixed with 'db.' to the corresponding global database key. Example: db.name becomes photo_name.
		 * Also checks to make sure each column/key actually exists in the table.
		 *
		 * @param array $values The array of values.
		 * @param string $statement The statement we'll use to save the values: INSERT or UPDATE. Used for determining automatic values. Default = NULL
		 * @return array The array of values with the 'db' keys replaced with their actual keys.
		 */
		function save_values($values,$statement = NULL) {
			// Error
			if(!$this->module or !is_array($values)) return;
			
			foreach($values as $k => $v) {
				// Global database variable
				if(substr($k,0,3) == "db.") {
					$key = m($this->module.'.'.$k);
					if($key) $values[$key] = $v;
					unset($values[$k]);
					$k = $key;
				}
				
				// Column exists?
				if(!$this->db->table_column(m($this->module.'.db.table'),$k)) {
					unset($values[$k]);	
				}
			}
				
			// Automatic values
			if($values) {
				// Automatic values
				if($key = m($this->module.'.db.updated')) { // Updated
					if(!x($values[$key])) $values[$key] = time();
				}
				// Automatic values - insert
				if($statement == "INSERT") {
					if($key = m($this->module.'.db.created')) { // Created
						if(!x($values[$key])) $values[$key] = time();
					}
				}
				// Automatic values - update
				if($statement == "UPDATE") {
					# none yet
				}
			}
			
			// Return
			return $values;
		}
		
		/**
		 * Processes an item and it's related data such as URL's, search indexing, visiblity, etc.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the new item.
		 */
		function process($c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Already processing - check this so as to avoid infinite loop since any $item->save() within here will re-process
			if(!g('processing.'.$this->module.'.'.$this->id.'.item')) {
				// Processing
				g('processing.'.$this->module.'.'.$this->id.'.item',1);
				
				// Visible
				$this->process_visible($c);
				// Order
				$this->process_order($c);
			
				// Plugins
				$c[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = m($this->module.'.plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						if($plugin_active) {
							$plugin_instance = $this->plugin($plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								debug("Processing plugin ".$plugin,$c[debug]);
								$plugin_instance->$method($c);
							}
						}
					}
				}
				
				// Done processing
				g('processing.'.$this->module.'.'.$this->id.'.item',0);
			}
		}
		
		/** 
		 * Determines if an item is visible to the public or not.
		 *
		 * Really just a placeholder function so we can easily effect the 'visible' value in module specific item classes.
		 *
		 * @param array $c An array of configuration values. Deafult = NULL
		 * @return boolean Whether or not the item is visible to the public.
		 */
		function visible_check($c) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Visible by default
			$visible = 1;
			
			// Disabled (inactive)
			if(m($this->module.'.db.active') and !$this->db('active')) {
				$visible = 0;
			}
			
			// Plugins
			if($visible) {
				$c[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = m($this->module.'.plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						if($plugin_active) {
							$plugin_instance = $this->plugin($plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								// Check if visible
								debug("Visible check in plugin ".$plugin."..",$c[debug]);
								$visible = $plugin_instance->$method($c);
								debug("..visible result: ".$visible,$c[debug]);
								// No, return false
								if(!$visible) return $visible;
							}
						}
					}
				}
			}
			
			// Return
			return $visible;
		}
		
		/** 
		 * Determines if an item is visible to the public or not and saves that value to the item's 'visible' db field (m($module.'.db.visible');).
		 *
		 * @param array $c An array of configuration values. Deafult = NULL
		 */
		function process_visible($c) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Field exists
			if(m($this->module.'.db.visible',$this->type)) {
				// Debug
				debug("<br />\$".__CLASS__."->".__FUNCTION__."();",$c[debug]);
				
				// Visible?
				$visible = $this->visible_check(array('debug' => $c[debug]));
				debug("visible: ".$visible.", previously: ".$this->db('visible'),$c[debug]);
				if($visible != $this->db('visible')) {
					// Item
					$this->save(array('db.visible' => $visible),array('debug' => $c[debug]));
					// Menus
					if(m($this->module.'.public.views.item') and m($this->module.'.public.active')) {
						$query = "UPDATE menus_items SET item_visible = '".$visible."' WHERE module_code = '".$this->module."' AND item_id = '".$this->id."'";
						debug($query,$c[debug]);
						$this->db->q($query);
					}
					// Index
					if(m($this->module.'.settings.search.index',$this->type) == 1 or m($this->module.'.settings.search.public',$this->type) == 1) {
						$query = "UPDATE items_index SET item_visible = '".$visible."' WHERE module_code = '".$this->module."' AND item_id = '".$this->id."'";
						debug($query,$c[debug]);
						$this->db->q($query);
					}
				}
			}
		}
		
		/**
		 * Gives the item an 'order' # (if the db.order variable is defined in the module and they don't already have an order). 
		 */
		function process_order($c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Field exists (and doesn't yet have a value)
			if(!$this->order and m($this->module.'.db.order')) {
				// Debug
				debug("<br />\$".__CLASS__."->".__FUNCTION__."();",$c[debug]);
				
				// Query
				$where = NULL;
				if(m($this->module.'.db.parent_id')) $where .= ($where ? " AND " : " WHERE ").m($this->module.'.db.parent_id')." = '".$this->parent_id."'";
				if(m($this->module.'.db.order_level')) $where .= ($where ? " AND " : " WHERE ").m($this->module.'.db.order_level')." = 0";
				$query = "SELECT MAX(".m($this->module.'.db.order').") o FROM ".m($this->module.'.db.table').$where;
				debug($query,$c[debug]);
				
				// Result
				$row = $this->db->f($query);
				
				// Update order
				$this->save(array('db.order' => ($row[o] + 1)),array('debug' => $c[debug]));
			}
		}
		
		/**
		 * Creates copy of the item.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function copy($c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Speed
			//$f_r = function_speed(__CLASS__."->".__FUNCTION__);
			
			// Config	
			if(!x($c[change])) $c[change] = NULL; // Array of columns to change (column => new value)
			if(!x($c[skip])) $c[skip] = NULL; // Array of columns to skip
			if(!x($c[append])) $c[append] = 1; // Append text to name of copied item?
			if(!x($c[append_text])) $c[append_text] = " Copy"; // Text to append to name of copied item
			if(!x($c[permission])) $c[permission] = 1; // Check permission
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Permission and have table/id db variables
			if((permission($this->module,'copy',$this->id) or $c[permission] == 0) and m($this->module.'.db.table') and m($this->module.'.db.id')) {
				// Skip defaults
				$c[skip][] = m($this->module.'.db.id');
				$c[skip][] = m($this->module.'.db.code');
				$c[skip][] = m($this->module.'.db.created');
				$c[skip][] = m($this->module.'.db.updated');
				
				// Loop through original vlaues
				$name = string_word_last(m($this->module.'.db.name',$this->type));
				foreach($this->row() as $k => $v) {
					// Skip value?
					if(!in_array($k,$c[skip])) {
						// Change Value
						if($c[change][$k]) $v = $c[change][$k];
						// Rename
						else if($c[append] and $k == $name) {
							for($x = 1;$x <= 100;$x++) {
								// New name
								$_v = $v.$c[append_text].($x > 1 ? " (".$x.")" : "");
								// Already exists?
								$row = parent::row(array('where' => $name." = '".a($_v)."'"));
								// No, can use
								if(!$row[m($this->module.'.db.id')]) {
									$v = $_v;
									$x = 101;
								}
							}
						}
						// Values
						$values[$k] = $v;
					}
				}
				
				// Copy
				$item = item::load($this->module,'new');
				$id = $item->save($values,array('debug' => $c[debug]));
				
				// Child modules
				if($child_modules = m($this->module.'.children')) {
					foreach($child_modules as $child_module) {
						if($child_items = $this->children($child_module)) {
							foreach($child_items as $child_item) {
								$child_id = $child_item->copy(array('change' => array(m($child_module.'.db.parent_id') => $id),'debug' => $c[debug]));
							}
						}
					}
				}
				
				// Plugins
				$c[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = m($this->module.'.plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						// Plugin active
						if($plugin_active) {
							// Plugin class
							$plugin_instance = $this->plugin($plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								// Copy
								debug("Copying in plugin ".$plugin,$c[debug]);
								$plugin_instance->$method($id,$c);
							}
						}
					}
				}
				
				// Process - just in case something happened in plugin copying that effected visibility, etc.
				$this->process();
			}
	
			// Speed
			//function_speed(__CLASS__."->".__FUNCTION__,$f_r);
			
			// Return
			return $id;
		}
		
		/**
		 * Deletes the item and all related data.
		 *
		 * Processing of all related data (child module items, plugin items attached to this item) is also handled by this function.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function delete($c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			if($this->id > 0) {
				// Config
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Plugins - do before we actually delete so we can reference the item's info (especially in the trash plugin)
				$_c = $c;
				$_c[item] = $this;
				$method = __CLASS__."_".__FUNCTION__;
				if($plugins = m($this->module.'.plugins')) {
					foreach($plugins as $plugin => $plugin_active) {
						// Plugin active
						if($plugin_active) {
							// Plugin class
							$plugin_instance = $this->plugin($plugin,$_c);
							if(method_exists($plugin_instance,$method)) {
								// Delete
								debug("Deleting in plugin ".$plugin,$c[debug]);
								$plugin_instance->$method($c);
							}
						}
				
						// Comments
						#when we build the comments plugin:
						#here we'd pass $trash and store that in the items_trash parent_id field so, if we restore the parent, we'd restore the comments as well.
						#since it's a plugin, not sure how we handle calling framework_delete() up.
					}
				}
				
				// Build query
				$query = "DELETE FROM `".m($this->module.'.db.table')."` WHERE ".m($this->module.'.db.id')." = '".$this->id."'";
				
				// Debug
				debug($query,$c[debug]);
				
				// Run query
				$this->db->q($query);
				
				// Child modules
				if($child_modules = m($this->module.'.children')) {
					foreach($child_modules as $child_module) {
						if($child_items = $this->children($child_module)) {
							foreach($child_items as $child_item) {
								$child_item->delete($c,array('cache' => 0)); // Turning off caching in case their's a lot, want to keep memory use down
							}
						}
					}
				}
				
				// Refresh cache
				$this->refresh();
			}
		}
		
		/**
		 * Disables or enables the item based on its current 'active' state.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function disable($c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Column
			$column = m($this->module.'.db.active');
			
			// Have id and column
			if($this->id > 0 and $column) {
				// Config
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Values
				$values = array(
					$column => ($this->db('active') ? 0 : 1)
				);
				
				// Save
				$this->save($values,$c);
			}
		}
	
		/**
		 * 'Refreshes' item by removing all old data so we'll have to re-create it.
		 *
		 * Usually called after altering an item. Data will get recreated next time we call $this->v() or $this->db() (which in turn call $this->row())
		 *
		 * @param array $values An array of values that we need to refresh (means we won't have to re-query db, just update $this->row). Default = NULL
		 */
		function refresh($values = NULL) {
			// Cache
			$cache = 0;
			
			// Update old row // Won't work for 'default' values. Blank value submitted, but in db it'd default. Example: if INT it'd be 0, for DATETIME it'd be a 0000-00-00 00:00:00.
			/*if(is_array($values) and $this->row and (!$this->c[language] or $this->c[language] == m('languages.settings.languages.default'))) {
				// Strip automatically added slashes // No longer automatically added
				//$values = s($values);
				// Update row
				$this->row = array_merge($this->row,$values);
				// Clear 'original' row - we know this is not translated item class so there will be no changed values
				$this->row_original = NULL;
				// Want to re-cache item as we have updated row (avoid new query next time we want to use this item)
				$cache = 1;
			}
			// Clear old row
			else*/ unset($this->row,$this->row_original);
				
			// Clear old data
			foreach(m($this->module.'.db') as $k => $v) {
				if(!in_array($k,array('module','id','type'))) unset($this->$k); // Global database variables we stored for faster access.
			}
			unset($this->cache,$this->type); // Instance variables
			
			// Update cache
			if($cache) {
				$this->cache_save();
			}
		}
		
		/**
		 * Loads an instance of the given plugin for the item.
		 * 
		 * @param string $plugin The plugin you want to load.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin class.
		 */
		function plugin($plugin,$c = NULL) {
			// Error
			if(!$plugin or !$this->module) return;
			
			// Config
			if(!x($c[cache])) $c[cache] = $this->c[cache];
			if(!x($c[cached])) $c[cached] = $this->c[cached];
			
			// Saved? // Won't we want unique instances always, especially with the file plugin? // Reset the $this->file in $plugin_files->file() so may be able to turn caching back on. // Nope, can't, because the plugin specific class may not have been included yet
			//if($instance = $this->cache[plugins][$plugin]) return $instance;
			
			// Active
			if(m($this->module.'.plugins.'.$plugin)) {
				// Get
				$c[item] = $this;
				$plugin_instance = plugin::load($this->module,$this->id,$plugin,$c);
				
				// Save // Won't we want unique instances always, especially with the file plugin? // Reset the $this->file in $plugin_files->file() so may be able to turne caching back on. // Nope, can't, because the plugin specific class may not have been included yet
				//$this->cache[plugins][$plugin] = $plugin_instance; // Nope, can't, because the plugin specific class may not have been included yet
				
				// Return
				return $plugin_instance;
			}
		}
		
		/**
		 * Loads an instance of the given plugin for the item.
		 * 
		 * @param string $plugin The plugin you want to load.
		 * @param int|array $plugin_id The id of the plugin item you want to create the item instance of. You can also pass the item's full db row array.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the plugin class.
		 */
		function plugin_item($plugin,$plugin_id = NULL,$c = NULL) {
			// Error
			if(!$this->module or !$plugin) return;
			
			// Config
			if(!x($c[cache])) $c[cache] = $this->c[cache];
			if(!x($c[cached])) $c[cached] = $this->c[cached];
			
			// Active
			if(m($this->module.'.plugins.'.$plugin)) {
				// Get
				$c[item] = $this;
				$plugin_item_instance = plugin_item::load($this->module,$this->id,$plugin,$plugin_id,$c);
				
				// Return
				return $plugin_item_instance;
			}
		}
	
		/**
		 * Method for eaily creating a form instance within the module class. Note, preloads $this->module and $this->id.
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The instance of the form class.
		 */
		function form($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!x($c[type])) $c[type] = $this->type;
			
			// Form
			return form_framework::load($this->module,$this->id,$c); // form_framework extends form
			//return item_form::load($this->module,$this->id,$c); // item_form extends item
		}
		
		/**
		 * Returns HTML of the given view for this item.
		 *
		 * Example:
		 * $item->view(); // Defaults to 'item' view
		 * $item->view('simple');
		 *
		 * @param string $view The 'view' you want to load. Default = item
		 * @param array $variables An array of variables to pass when get the HTML of the view. Example: array('perpage' => 5); would be accessible via $perpage in the view file. Default = NULL
		 * @return string The HTML of the view.
		 */
		function view($view = NULL,$variables = NULL) {
			// Error
			if(!$this->module or !$view) return;
			
			// Default
			if(!$view) $view = 'item';
			
			// Array
			$array = array(
				'module' => $this->module,
				'id' => $this->id,
				'view' => $view
			);
			
			// View
			$loader = loader::load();
			return $loader->view($array,$variables);
		}
		
		/**
		 * Determines whether or not the current item is 'visible' to the current user.
		 * 
		 * @param boolean|string $message Whether or not you want to display an error message if it's not visible. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not the item is visible.
		 */
		function visible($message = 0) {
			// Visible
			$visible = $this->visible;
			
			// Message
			if(!$visible and $message) {
				// Container
				print "<div class='core-red core-none'>";
				
				// Default
				if($message === 1) print "I'm sorry, but you don't have permission to ".($this->area == "public" ? "view" : "do")." this.";
				// Custom
				else print $message;
				
				// End container
				print "</div>";
			}
			
			// Return
			return $visible;
		}
		
		/**
		 * Creates and returns the HTML of an icon for performing some action on this item.
		 *
		 * This method basically lets us check permission and data within this item before we send it to the $core->icon() method.
		 * See $core->icon() for more configuration values.
		 *
		 * @param string $action The action of the icon you're creating.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML of the icon.
		 */
		function icon($action,$c = NULL) {
			// Error
			if(!$this->module or !$this->id) return;
			
			// Config
			if(!$c[icon]) $c[icon] = $action; // The CSS 'i-' class you want to apply to generate the icon. Example: 'add' would result in 'i-add'.
			if(!$c[text] and $c[button]) $c[text] = ($c[tooltip_text] ? $c[tooltip_text] : ucwords($action)); // Text to display alongside the icon (required if a 'button')
			if(!$c[tooltip_text]) $c[tooltip_text] = ($c[text] ? $c[text] : ucwords($action)); // The tooltooltip text to show on hover (if $c[tooltip] == 1).
			if(!$c[parent_selector]) $c[parent_selector] = "#".$this->module."-".$this->id; // The jQuery selector of the parent element. Used when deleting an item.
			if(!$c[delete_text]) $c[delete_text] = "Are you sure you want to delete this ".strtolower(m($this->module.'.single'))."?"; // Text to 'confirm' when deleting an item.
			if(!x($c[permission])) $c[permission] = 1; // Do you want to check whether or not the user has permission to perform this action on this item?
			if(!x($c[permission_action])) $c[permission_action] = $action; // What permission action are we checking against?
			if(!x($c[area])) $c[area] = NULL; // What 'area' are we linking icons to (ex: edit). Defaults to current area (if admin/account), else admin (if user is admin), else account.
			
			// Permission
			$permission = 0;
			if(!$c[permission]) $permission = 1;
			else if($this->permission($c[permission_action])) $permission = 1;
			if($permission) {
				if(!x($c[url])) {
					// Delete
					if($action == "delete") {
						$c[url] = "javascript:void(0);";
						$c[attributes][onclick] = "framework_delete('".string_encode($c[delete_text],1)."','".$c[parent_selector]."','".$this->module."','".$this->id."');";
					}
					// Disable
					else if($action == "disable") {
						if(!m($this->module.'.db.active')) return;
						
						$c[url] = "javascript:void(0);";
						$c[attributes][id] = $this->module."-".$this->id."-disable-".random();
						$c[attributes][onclick] = "framework_disable('".$c[parent_selector]."','".$this->module."','".$this->id."',{icon_selector:'#".$c[attributes][id]."'});";
						$c[attributes]['class'] .= ($c[attributes]['class'] ? " " : "")."i-able";
						if(!$this->db('active')) {
							if($c[icon] == "disable") $c[icon] = "enable";
							if($c[tooltip_text] == "Disable") $c[tooltip_text] = "Enable";
						}
					}
					// Copy
					else if($action == "copy") {
						$c[url] = "javascript:void(0);";
						$c[attributes][onclick] = "fw_confirm_redirect('Are you sure you want to copy this ".strtolower(m($this->module.'.single'))."?','".DOMAIN."?process_module=".$this->module."&process_action=copy&module=".$this->module."&id=".$this->id."');";
					}
					// Default
					else {
						// Area
						if(!$c[area]) {
							$page = g('page');
							if(in_array($page->area,array('admin','account'))) $c[area] = $page->area;
							else if(u('admin')) $c[area] = 'admin';
							else $c[area] = 'account';
						}
					
						// Edit
						if($action == "edit") $c[url] = $this->url($c[area])."?redirect=".urlencode(URL);
						// Default
						else $c[url] = $this->url($c[area])."/".$action;
					}
				}
			}
				
			// Return
			if($c[url] or $c[required]) {
				return parent::icon($action,$c);
			}
		}
		
		/**
		 * Checks whether current user has permission to perform given action in this module and (if present) item.
		 *
		 * @param string $action The action we want to check the permission of.
		 * @param boolean|string $message Whether or not you want to display an error message if they don't have permission. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not the current user has the permission to perform this action on this module/module item.
		 */
		function permission($action,$message = 0) {
			// Page - need for area
			$page = g('page');
			
			// Message - build here so we can overwrite entire thing in plugin check
			if(!x($message) or $message === 1) $message = "I'm sorry, but you don't have permission to ".($page->area == "public" ? "view" : "do")." this.";
			if($message) {
				$message = "<div class='core-red core-none'>".$message."</div>";	
				if(!g('u.id')) $message .= "{login_form}";
			}
			
			// Permission - global
			if($action) $permission = permission($this->module,$action,$this->id,array('type' => $this->type));
			// Permission - plugins
			if($permission and $this->id and $action == "view") {
				// Plugins
				$c[item] = $this;
				if($plugins = m($this->module.'.plugins')) {
					$method = "item_".__FUNCTION__;
					foreach($plugins as $plugin => $plugin_active) {
						if($plugin_active) {
							$plugin_instance = plugin::load($this->module,$this->id,$plugin,$c);
							if(method_exists($plugin_instance,$method)) {
								// Permission
								debug("Checking permissions in plugin ".$plugin,$c[debug]);
								$permission = $plugin_instance->$method($action,$message,$c);
								
								// Returned array instead of just boolean true/false. Example: array('permission' => 0, 'message' => 'custom error message');
								if(is_array($permission)) {
									$message = $permission[message];	
									$permission = $permission[permission];
								}
							}
						}
					}
				}
			}
			else $c[url] = 0;
			
			// Message
			if(!$permission and $message) {	
				// Login form
				if(strstr($message,"{login_form}")) {
					$login_form = $this->login_form();	
					$message = str_replace("{login_form}",$login_form,$message);
				}		
				
				// Message
				print "
<div class='permission-error permission-error-item'>
	".$message."
</div>";
			}
			
			// Return
			return $permission;
		}
		
		/**
		 * Returns an array of the child items connected to this parent item.
		 *
		 * Note: this will return the child items either as an item object ($c['return'] == "item") [default] or as their database row array ($c['return'] == "row").
		 *
		 * @param string $module The child module you want to get the items of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @param return array An array of the child items connected to this parent item.
		 */
		function children($module,$c = NULL) {
			// Error
			if(!$module or m($module.'.parent') != $this->module or !m($module.'.db.table') or !m($module.'.db.parent_id') or !$this->id) return;
			
			// Config
			if(!$c['return']) $c['return'] = "item"; // What format do you want the child items returned in: item [default], row.
			if(!$c[query_c]) $c[query_c] = NULL; // An array of configuation values passed to the $module_class->rows() method.
			$c[query_c]['db.parent_id'] = $this->id;
			$c[query_c][debug] = $c[debug];
			
			// Module
			$module_class = module::load($module);
			
			// Childern
			$rows = $module_class->rows($c[query_c]);
			while($row = $module_class->db->f($rows)) {
				if($c['return'] == "row") $array[] = $row;	
				else $array[] = $module_class->item($row);
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Saves item instance or specific data (if 1st and 2nd param) to global cache.
		 *
		 * If you don't pass a name and $data we'll simply save the entire instance to the cache.
		 *
		 * @param string $name The name we want to use when storing the cached data. We'll use this when retrieving the cached data later. 
		 * @param string|array $data The data (string or array) we want to cache
		 */
		function cache_save($name = NULL,$data = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->c[cache] or !$this->row or $this->preview) return; // 'preview' must be after $this->row as $this->row may call the row() method which defines 'preview'
			
			// Language
			$language = ($this->c[language] ? $this->c[language] : 'eng');
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id;
			
			// Specific data
			if($name) $string .= '/item_custom/'.$language.'/'.$name;
			// Class instance
			else {
				$string .= '/item/'.$language;
				$data = $this;
			}
			
			// Local cache
			$string_local = 'cache.'.str_replace('/','.',$string);
			g($string_local,$data);
			// Global cache
			unset($data->cache_local);
			cache_save($string,$data);
		}
		
		/**
		 * Returns saved cache (if it exists).
		 *
		 * @param string $name The name of the cached data.  Default = NULL (which will return a cache of the entire plugin instance if it exists).
		 * @return mixed The cached data.
		 */
		function cache_get($name = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->c[cached] or $this->preview) return;
			
			// Language
			$language = ($this->c[language] ? $this->c[language] : 'eng');
			
			// Name
			$string = 'item/'.$this->module.'/'.$this->id;
			
			// Specific data
			if($name) $string .= '/item_custom/'.$language.'/'.$name;
			// Class instance
			else $string .= '/item/'.$language;
			
			// Local cache
			$string_local = 'cache.'.str_replace('/','.',$string);
			$cache = g($string_local);
			// Global cache
			if(!$cache) {
				$cache = cache_get($string);
				// Store in local cache
				if($cache) g($string_local,$cache);
			}
			
			// Config - want current config. For example, if we passed $c[cache] = '0', but the cached item had it set to '1', we wouldn't want to cache the item now would we.
			if(!$name) $cache->c = $this->c;
			
			// Return
			return $cache;
		}
	}
}
?>