<?php
if(!class_exists('plugin_item_cart',false)) {
	/**
	 * A class used for tweaking item arrays within the cart based upon the item's selected plugin values.
	 *
	 * @package kraken\cart
	 */
	class plugin_item_cart extends plugin_item {
		/**
		 * Loads and returns an instance of either the core plugin_cart class or (if it exists) the plugin specific plugin_cart class.
		 *
		 * Example:
		 * - $plugin_item_cart = plugin_cart::load($module,$id,$plugin,$plugin_id,$c);
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core plugin_cart class or (if it exists) the plugin specific plugin_item class.
		 */
		static function load($module,$id,$plugin,$plugin_id,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_plugin(__CLASS__,$params,$module,$id,$plugin,$c); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$id,$plugin,$plugin_id,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon.
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$plugin_id,$c = NULL) {
			self::plugin_item_cart($module,$id,$plugin,$plugin_id,$c);
		}
		function plugin_item_cart($module,$id,$plugin,$plugin_id,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$plugin,$plugin_id,$c);
		}
		
		/**
		 * Processes the given item array through this specific plugin item and returns the updated array.
		 *
		 * @param object $cart The cart object we'll be adding this item to.
		 * @param array $item The item array you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array after it's been processed through this plugin item.
		 */
		function item_add($cart,$item,$c = NULL) {
			// Error
			if(!$this->module or !$this->plugin or !$this->plugin_id) return $item;
			
			// Price
			if($this->db('cart_price') and !$item[price_custom]) $item[price] = $this->db('cart_price');
			
			// Quantity - unit
			if($this->db('cart_quantity_unit_single') or $this->db('cart_quantity_unit_plural')) {
				$item[quantity][unit][single] = ($this->db('cart_quantity_unit_single') ? $this->db('cart_quantity_unit_single') : $this->db('cart_quantity_unit_plural'));
				$item[quantity][unit][plural] = ($this->db('cart_quantity_unit_plural') ? $this->db('cart_quantity_unit_plural') : $this->db('cart_quantity_unit_single'));
			}
			
			// Quantity - min / max
			if($this->db('cart_quantity_minimum')) $item[quantity][minimum] = $this->db('cart_quantity_minimum');
			if($this->db('cart_quantity_maximum')) $item[quantity][maximum] = $this->db('cart_quantity_maximum');
			
			// Shipping - weight
			if($this->db('cart_shipping_weight_weight') and $this->db('cart_shipping_weight_unit')) {
				$item[shipping][weight] = array(
					'weight' => $this->db('cart_shipping_weight_weight'),
					'unit' => $this->db('cart_shipping_weight_unit'),
				);
			}
			
			// Shipping - dimensions
			if(
				$this->db('cart_shipping_dimensions_width')
				and $this->db('cart_shipping_dimensions_length')
				and $this->db('cart_shipping_dimensions_height')
				and $this->db('cart_shipping_dimensions_unit')
			) {
				$item[shipping][dimensions] = array(
					'width' => $this->db('cart_shipping_dimensions_width'),
					'length' => $this->db('cart_shipping_dimensions_length'),
					'height' => $this->db('cart_shipping_dimensions_height'),
					'unit' => $this->db('cart_shipping_dimensions_unit'),
				);
			}
			
			// Shipping - price
			if($this->db('cart_shipping_price')) $item[shipping][price] = $this->db('cart_shipping_price');
			if($this->db('cart_shipping_charge')) $item[shipping][charge] = $this->db('cart_shipping_charge');
			
			// Return
			return $item;
		}
		
		/**
		 * Makes sure an item meets all requirments assigned to it specifically or globally.
		 *
		 * If an error(s) occured, it'll appear in the item's 'errors' array. Example: 'errors' => array("Please select a quantity of 10 or less.")
		 *
		 * @param array $item The array of the item you want to validate.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the item, including any errors that occured during validation.
		 */
		function item_validate($item,$c = NULL) {
			// Return
			return $item;	
		}
		
		/**
		 * Called when an item is saved in a new order.
		 *
		 * @param array $item The item array of the item as it appears in the cart.
		 * @param int $id The 'id' (in the orders_items module) of the item that was just saved.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function order($item,$id,$c = NULL) {
			
		}
		
		/**
		 * Called when an item is cancelled in an order.
		 *
		 * @param int $id The 'id' (in the orders_items module) of the item that we just cancelled.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function cancel($id,$c = NULL) {
			
		}
	}
}
?>