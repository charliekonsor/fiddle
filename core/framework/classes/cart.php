<?php
if(!class_exists('cart_framework',false)) {
	/**
	 * An extenion of the cart class with functionality specific to the framework.
	 *
	 * Dependencies
	 * - random()
	 *
	 * @package kraken\cart
	 */
	class cart_framework extends cart {
		public $module; // string
		public $user; // int
		public $payment; // int
		public $order; // int
		public $text; // string // Should maybe make $order $order[id] and this $order[text]?
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module this cart is working with.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$c = NULL) {
			self::cart_framework($module,$c);
		}
		function cart_framework($module,$c = NULL) {
			// Module
			$this->module = $module;
			
			// Cache key
			$this->cache_key = "cart_".$module;
			
			// Parent
			parent::__construct($c);
			
			// User
			if(!$this->x($c[user])) $c[user] = u('id');
			$this->user = $c[user];
			unset($c[user]);
			
			// Shipping
			if(m('shipping')) {
				// Methods
				if(!$c[shipping][methods]) {
					$methods = NULL;
					$module_class = module::load('shipping');
					$rows = $module_class->rows(array('db.visible' => 1));
					while($row = $module_class->db->f($rows)) {
						$item = $module_class->item($row);
						
						$states = NULL;
						$states_ids = data_unserialize($row[shipping_include_states]);
						if($states_ids) {
							foreach($states_ids as $k => $v) {
								$state_item = item::load('states',$v);
								$states[$state_item->v('country_code')][] = state_code($state_item);	
							}
						}
						$countries = NULL;
						$countries_ids = data_unserialize($row[shipping_include_countries]);
						if($countries_ids) {
							foreach($countries_ids as $k => $v) {
								$code = country_code($v);
								if($states[$code]) $countries[$code] = $states[$code];
								else $countries[$code] = "all";
							}
						}
						
						$states_exclude = NULL;
						$states_exclude_ids = data_unserialize($row[shipping_exclude_states]);
						if($states_exclude_ids) {
							foreach($states_exclude_ids as $k => $v) {
								$state_item = item::load('states',$v);
								$states_exclude[$state_item->v('country_code')][] = state_code($state_item);	
							}
						}
						$countries_exclude = NULL;
						$countries_exclude_id = data_unserialize($row[shipping_exclude_countries]);
						if($countries_exclude_id) {
							foreach($countries_exclude_id as $k => $v) {
								$code = country_code($v);
								if($states_exclude[$code]) $countries_exclude[$code] = $states_exclude[$code];
								else $countries_exclude[$code] = "all";
							}
						}
						
						$methods[] = array(
							'company' => $module_class->v('settings.companies.'.s($row[shipping_company]).'.name'),
							'code' => $row[shipping_code],
							'name' => $row[shipping_name],
							'rate' => $row[shipping_rate],
							'countries' => $countries,
							'countries_exclude' => $countries_exclude,
							'active' => $row[shipping_visible],
							'item' => $item,
						);
					}
					$this->shipping(array('methods' => $methods));
				}
				// Default
				if(!$c[shipping]['default']) {
					$default = array(
						'weight' => array(
							'weight' => m('shipping.settings.default.weight.weight'),
							'unit' => m('shipping.settings.default.weight.unit'),
						),
						'dimensions' => array(
							'width' => m('shipping.settings.default.dimensions.width'),
							'length' => m('shipping.settings.default.dimensions.length'),
							'height' => m('shipping.settings.default.dimensions.height'),
							'unit' => m('shipping.settings.default.dimensions.unit'),
						),
					);
					$this->shipping(array('default' => $default));
				}
			}
			
			// Tax
			if(g('config.cart.tax.active') and g('config.cart.tax.rates')) {
				$this->tax(g('config.cart.tax.rates'));
			}
			
			// Handling
			if(g('config.cart.handling.active') and g('config.cart.handling.price')) {
				$this->handling(g('config.cart.handling'));
			}
		}
		
		/**
		 * Returns key for the given item array.
		 *
		 * @param array $item The item array you want to get (or set) the key for.
		 * @return string The item's key.
		 */
		function item_key($item) {
			// Key
			if(!$item[key]) {
				// Module/id
				$item[key] = $item[module]."|".$item[id];
				
				// Plugins
				if($item[plugins]) {
					foreach($item[plugins] as $plugin => $plugin_ids) {
						if(m($item[module].'.plugins.'.$plugin) == 1 and $plugin_ids) {
							if(!is_array($plugin_ids)) $plugin_ids = array($plugin_ids);
							foreach($plugin_ids as $plugin_id) {
								$item[key] .= "|".$plugin.":".$plugin_id;
								
								/*$class = load_class_plugin('plugin_item_cart',array($item[module],$item[id],$plugin,$plugin_id),$item[module],$item[id],$plugin,$c);
								if($class) {
									$instance = new $class($item[module],$item[id],$plugin,$plugin_id);
									$method = __FUNCTION__;
									if(method_exists($instance,$method)) {
										$item[key] = $instance->$method($item);
									}
								}*/
							}
						}
					}
				}
				
				// Custom
				if($item[custom]) $item[key] .= "|".data_serialize($item[custom]);
				
				// Encode
				$item[key] = md5($item[key]);
			}
			
			// Return
			return $item[key];
		}
		
		/**
		 * Adds an item to the cart.
		 *
		 * @param int|array $id The id of the item you want to add to the cart or the actual pre-built item array (useful if upating)
		 * @param int $quantity The quantity of this item we're adding to the cart (not the whole quantity, but the quantity we're adding).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array as it appears stored in the cart.
		 */
		function item_add($id,$quantity,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Item
			$item = NULL;
			
			// Passed item array
			if(is_array($id)) { // $cart->item_add($item,NULL,$c);
				$item = $id;
				$id = NULL;
				if(is_array($quantity)) { // $cart->item_add($item,$c); // Shouldn't use this, but just to be safe
					$c = $quantity;
					$quantity = NULL;	
				}
			}
			
			// Config
			//if(!$c[description]) $c[description] = NULL; // An example of how you can overwrite the default/defined value for this item in the cart.  Applies to all variables in the cart item array.
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			$c_skip = array('debug'); // An array of config keys we don't want to add to the cart item array for this item.
			
			// Create item array
			if(!$item) {
				// Array
				$item = array();
				
				// Item
				$item[item] = item::load($this->module,$id);
				if(!$item[item]->id) return;
				
				// Values - framework
				$item[module] = $this->module;
				$item[id] = $item[item]->id;
				$item[plugins] = $c[plugins];
				// Values - custom - need to add before 'key' is created
				$item[custom] = $c[custom];
				
				// Values - key
				$item[key] = $this->item_key($item);
				
				// Values - default
				$item[name] = ($item[item]->db('cart_name') ? $item[item]->db('cart_name') : $item[item]->db('name'));
				$item[description] = string_breaks($item[item]->db('cart_description')); // Only want to add description if we defined a cart_description field
				if($item[item]->db('cart_image')) $file = $item[item]->file('db.cart_image',array('thumb' => 's'));
				else $file = $item[item]->file('db.image',array('thumb' => 's'));
				$item[image] = $file->url();
				$item[url] = $item[item]->url();
				
				// Values - default - quantity
				$item[quantity][unit] = array(
					'single' => $item[item]->db('cart_quantity_unit_single'),
					'plural' => $item[item]->db('cart_quantity_unit_plural'),
				);
				
				if($item[item]->db('cart_quantity_minimum')) $item[quantity][minimum] = $item[item]->db('cart_quantity_minimum');
				if($item[item]->db('cart_quantity_maximum')) $item[quantity][maximum] = $item[item]->db('cart_quantity_maximum');
				
				// Values - default - shipping
				$item[shipping] = array(
					'price' => $item[item]->db('cart_shipping_price'),
					'charge' => $item[item]->db('cart_shipping_charge'),
					'weight' => array(
						'weight' => $item[item]->db('cart_shipping_weight_weight'),
						'unit' => $item[item]->db('cart_shipping_weight_unit'),
					),
					'dimensions' => array(
						'width' => $item[item]->db('cart_shipping_dimensions_width'),
						'length' => $item[item]->db('cart_shipping_dimensions_length'),
						'height' => $item[item]->db('cart_shipping_dimensions_height'),
						'unit' => $item[item]->db('cart_shipping_dimensions_unit'),
					),
				);
				
				// Values - default - handling
				$item[handling][price] = $item[item]->db('cart_handling_price');
				$item[handling][charge] = $item[item]->db('cart_handling_charge');
			}
			debug("item (after creating item array): ".return_array($item),$c[debug]);
				
			// Values - config
			foreach($c as $k => $v) {
				if(!in_array($k,$c_skip)) {
					if(is_array($v)) $item[$k] = $this->array_merge_associative($item[$k],$v);
					else $item[$k] = $v;
				}
			}
			debug("item (after merging with passed config, before adding via core class): ".return_array($item),$c[debug]);
			
			// Add to cart
			$_c = $c;
			$_c[standardize] = 0;
			$item = parent::item_add($item,$quantity,$_c);
			debug("item (after adding via core class): ".return_array($item),$c[debug]);
			
			// Error?
			if($item[errors]) {
				// Yes, return item (with errors) now to save time
				return $item;
			}
		
			// Price - do each time in case quantity has effected price (unless, of course, we have set a 'custom' price at any point)
			if($c[price]) $item[price_custom] = 1;
			if(!$item[price_custom] and $item[item]) $item[price] = $item[item]->db('cart_price');
			
			// Values - plugins // Do after we've added so we know the total quantity, etc. 
			//if(!is_array($id)) { // Not sure why I had this, but I think my price_custom check will fix any issues this dealt with
				//$item = $this->item_plugins($item,__FUNCTION__,array($item,$c));
				if($item[plugins]) { // Plugin items
					foreach($item[plugins] as $plugin => $plugin_ids) {
						if($plugin_ids) {
							if(!is_array($plugin_ids)) $plugin_ids = array($plugin_ids);
							foreach($plugin_ids as $plugin_id) {
								debug("processing plugin ".$plugin." id ".$plugin_id,$c[debug]);
								$item_plugin = $this->plugin_item($item[id],$plugin,$plugin_id,__FUNCTION__,array($this,$item,$c));
								if($item_plugin) $item = $item_plugin;
							}
						}
					}
				}
				if($plugins = m($item[module].'.plugins')) { // Plugins
					foreach($plugins as $plugin => $active) {
						debug("processing plugin ".$plugin,$c[debug]);
						$item_plugin = $this->plugin($item[id],$plugin,__FUNCTION__,array($this,$item,$c));
						if($item_plugin) $item = $item_plugin;
					}
				}
				debug("item (after plugins): ".return_array($item),$c[debug]);
			
				// Values - config // Run again after everythign else has run, just to be sure we didn't overwrite them
				foreach($c as $k => $v) {
					if(!in_array($k,$c_skip)) {
						if(is_array($v)) $this->array_merge_associative($item[$k],$v);
						else $item[$k] = $v;
					}
				};
				debug("item (after re-adding passed config): ".return_array($item),$c[debug]);
			//}
			
			// Discounts - item specific discount applies to this, add its 'key' to the array of keys the disocunt applies to
			if($this->discounts) {
				foreach($this->discoutns as $k => $v) {
					if($v[ids] and in_array($v[ids],$item[id]) and !in_array($v[keys],$item[key])) {
						$this->discounts[$k][keys][] = $item[key];
					}
				}
			}
			
			// Update
			$item = parent::item_update($item[key],$item,$c);
			
			// Return
			return $item;
		}
		
		/**
		 * Updates an item in the cart.
		 *
		 * Need this (even though it's identical to the parent method) because we point to $this instead of self:: when calling item_add().
		 *
		 * @param string $key The key of the item you want to update.
		 * @param array $item An array of item info we want to update.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array.
		 */
		function item_update($key,$item,$c = NULL) {
			$item[key] = $key;
			return $this->item_add($item,NULL,$c);
		}
		
		/**
		 * Makes sure an item meets all requirments assigned to it specifically or globally.
		 *
		 * If an error(s) occured, it'll appear in the item's 'errors' array. Example: 'errors' => array("Please select a quantity of 10 or less.")
		 *
		 * @param array $item The array of the item you want to validate.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the item, including any errors that occured during validation.
		 */
		function item_validate($item,$c = NULL) {
			// Error
			if(!$item) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Parent
			$item = parent::item_validate($item,$c);
			
			// Plugins
			//$item = $this->item_plugins($item,__FUNCTION__,array($item));
			if($item[plugins]) { // Plugin items
				foreach($item[plugins] as $plugin => $plugin_ids) {
					if($plugin_ids) {
						if(!is_array($plugin_ids)) $plugin_ids = array($plugin_ids);
						foreach($plugin_ids as $plugin_id) {
							$item_plugin = $this->plugin_item($item[id],$plugin,$plugin_id,__FUNCTION__,array($item,$c));
							if($item_plugin) $item = $item_plugin;
						}
					}
				}
			}
			if($plugins = m($item[module].'.plugins')) { // Plugins
				foreach($plugins as $plugin => $active) {
					$item_plugin = $this->plugin($item[id],$plugin,__FUNCTION__,array($item,$c));
					if($item_plugin) $item = $item_plugin;
				}
			}
			
			// Return
			return $item;
		}
		
		/**
		 * Creates the discount array from the given discount code and adds it to the cart, checking any restrictions contained in the discount.
		 *
		 * This returns the discount array. If an error occured (a restriction wasn't met), the array will contain an error message with the key 'error'.
		 * Example:
		 *
		 * $result = $cart->discount($discount);
		 * if($result[error]) print "Error: ".$result[error]."<br />";
		 *
		 * @param string $discount The discount code.
		 * @return array The resulting discount array, including any 'error' that occured.
		 */
		function discount_add($discount) {
			// Array
			$array = NULL;
		
			// Get
			if($this->x($discount)) {
				// Get
				$module_class = module::load('discounts');
				$row = $module_class->row(array('where' => "discount_code = '".a($discount)."'"));
				$item = $module_class->item($row);
				
				// Array
				$array = array(
					'key' => $item->v('discount_code'),
					'name' => $item->db('name'),
					'type' => $item->v('discount_type'),
					'rate' => $item->v('discount_rate'),
					'date' => array(
						'start' => $item->v('discount_date_start'),
						'end' => $item->v('discount_date_end'),
					),
					'item' => $item,
				);
				$this->debug('discount array: '.return_array($array));
				
				// Database
				$db = db::load();
				
				// Used?
				$query = "SELECT o.order_id FROM orders_discounts d JOIN orders o ON o.order_id = d.order_id WHERE o.user_id = '".u('id')."' AND o.order_cancelled = 0 AND d.discount_id = '".$item->id."'";
				$this->debug($query);
				$used = $db->f($query);
				
				// Used by customer?
				$query = "SELECT o.order_id FROM orders_discounts d JOIN orders o ON o.order_id = d.order_id WHERE o.user_id = '".u('id')."' AND o.order_cancelled = 0 AND d.discount_id = '".$item->id."' AND o.user_id = '".u('id')."'";
				$this->debug($query,$c[debug]);
				$used_customer = $db->f($query);
				
				// Doesn't exist
				if(!$item->id) {
					$array[error] = "We couldn't find this discount.";
				}
				// Isn't active
				else if(!$item->db('visible')) {
					$array[error] = "This discount isn't active.";
				}
				// Already used
				else if($item->v('discount_onetime') and $used[order_id]) {
					$array[error] = "This ".strtolower(m('discounts.single'))." has already been used.";
				}
				// Already used by customer
				else if($item->v('discount_one_per') and (!u('id') or $used_customer[order_id])) {
					if(!u('id')) $array[error] = "You must be logged in to use this ".strtolower(m('discounts.single')).".";
					else $array[error] = "You've already used this ".strtolower(m('discounts.single')).".";
				}
				// Not in this module
				else if($item->v('module_code') and $item->v('module_code') != $this->module) {
					$array[error] = "This discount isn't valid for your purchase.";
				}
				// Doesn't match items - note, we add this to the 'global' discounts ($this->discounts as opposed to $this->items[$key][discounts]), but only apply it to items that match the 'keys' this discount applies to (which we determine from the ids it applies to)
				else if($item->v('module_code') and $item->v('item_id')) {
					$items = data_unserialize($item->v('item_id'));
					if($items) {
						$match = 0;
						foreach($this->items as $k => $v) {
							if(in_array($v[id],$items)) {
								$match = 1;
								$array[keys][] = $k;
							}
						}
						$array[ids] = $items;
						if(!$match) {
							$array[error] = "This discount doesn't apply to any items in your cart.";
						}
					}
				}
				
				// Debug
				if($array[error]) $this->debug('error: '.$array[error]);
			}
			
			// Parent
			return parent::discount_add($array);
		}
		
		/**
		 * Returns HTML for displaying all discount, including an icon to delete each.
		 *
		 * @return string The HTML for displaying discounts.
		 */
		function discounts_display() {
			// Error
			if(!$this->discounts) return;
			
			// HTML
			$html = "
			<script type='text/javascript'>
			function cart_discount_delete(selector,discount) {
				// Remove
				$(selector).remove();
				
				// Ajax
				$.ajax({
					type: 'POST',
					url: DOMAIN,
					data: 'ajax_action=cart_discount_delete&ajax_module=".$this->module."&discount='+discount,
					dataType: 'json',
					success: function(result) {
						debug('discounts: '+result.discounts+', tax: '+result.tax+', subtotal: '+result.subtotal);
						 
						if(result.discounts < 0) $('.cart-totals-discounts').show();
						else $('.cart-totals-discounts').hide();
						$('.cart-totals-discounts-price').html('-$'+number_format(result.discounts * -1,2));
						$('.cart-totals-tax-price').html('$'+number_format(result.tax));
						$('#totals_subtotal').val(result.subtotal);
						cart_total();
					}
				});
			}
			</script>
			<table class='cart-discounts core-table core-rows'>";
			foreach($this->discounts as $key => $discount) {
				$html .= "
				".$this->discount_display($key);
			}
			$html .= "
			</table>";
			
			// Return
			return $html;
		}
		
		/**
		 * Returns HTML for displaying a discount, including an icon to delete it.
		 *
		 * @param string $key The 'key' of the discount you want to display.
		 * @return string The HTML for displaying the discount.
		 */
		function discount_display($key) {
			// Error
			if(!$this->x($key)) return;
			
			// Discount
			$discount = $this->discounts[$key];
			if($discount) {
				// Id
				$id = "cart-discount-".str_replace(' ','_',$discount[key]);
			
				// Icons
				$icons = NULL;
				$icons[delete] = "<a href='javascript:void(0);' onclick=\"cart_discount_delete('#".$id."','".$key."');\" class='i i-delete i-inline'></a>";
				$icons_width = (count($icons) * 18);
			
				// HTML
				$html = "
				<tr class='cart-discount' id='".$id."'>
					<td class='cart-discount-name'>".$discount[name]."</td>
					<td class='cart-discount-icons' width='".$icons_width."'>
						".implode($icons)."
					</td>
				</tr>";
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Sets the shipping address for all items or (if $item_key is passed) for a specific item.
		 *
		 * @param int|array $address Either the address id (from the addresses module) or the array of address information.
		 * @param string $item_key The key of the item this address is assigned to. If none passed, we apply this address to all items. Default = NULL
		 */
		function shipping_address($address,$item_key = NULL) {
			// Error
			if(!$address or !$this->items) return;
			
			// Address id passed, get array
			if(!is_array($address) and $address > 0) {
				$address_item = item::load('addresses',$address);
				$address = $address_item->payment_values();
				$address[item] = $address_item;	
			}
			
			// Parent
			parent::shipping_address($address,$item_key);
		}
		
		/**
		 * Places items into packages based upon the address they're getting sent to.
		 * 
		 * We need to group pre-defined items by 'id'. For example, if we added 2 different options of the
		 * same item and they can both fit in a pre-defined box for that item, we want them in that same box.
		 * 
		 * The core method only goes by 'key' which is a combination of item id and option info.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of packages for this cart's items. This is also stored in $this->shipping[packages].
		 */
		function shipping_package($c) {
			// Error
			if(!$this->items) return;
			
			// Update everything - so 'group packaging' in item defined boxes happens // Should probably find a more elegant way to do this
			foreach($this->items as $k => $item) {
				$this->item_update($k,$item);	
			}
			
			// Items
			$items = $this->items;
			// Items - remove any where package was 'grouped' with another entry
			foreach($items as $k => $v) {
				if($v[shipping][packages] == "grouped") unset($items[$k]);
			}
			
			// Parent
			$c[items] = $items;
			$packages = parent::shipping_package($c);
			
			// Return
			return $packages;
		}
		
		/**
		 * Calculates shipping rates for all 'packages' in this cart.
		 *
		 * Need this just so we can check shipping module defined variables for shipping APIs.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of shipping rates in array('company|code' => array('code' => code,'company' => company,'rate' => rate)) foramt.
		 */
		function shipping_rates($c) {
			// Companies
			if(m('shipping')) {
				$this->shipping[companies] = array_merge_associative($this->shipping[companies],m('shipping.settings.companies'),1);
			}
			
			// Parent
			return parent::shipping_rates($c);
		}
		
		/**
		 * Stores the shipping method that has been selected by the customer.
		 *
		 * @param string $string The shipping method string in {key}|{total} format.
		 * @param return array The selected shipping method array including company, name, code, and totals.
		 */
		function shipping_selected($string) {
			// Parse string
			list($key,$total) = explode('|',$string);
			
			// Parent
			return parent::shipping_selected($key);
		}
		
		/**
		 * Returns object of the form you can use to add an item to your cart.
		 *
		 * @param int $id The id of the item we're creating the add to cart HTML for.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object of the add to cart form.
		 */
		function form_add($id,$c = NULL) {
			// Error
			if(!$this->module or !$id) return;
			
			// Config
			if(!$c[price_text_selector]) $c[price_text_selector] = NULL; // A jQuery selector of the element that holds the text for displaying the price. We'll update it automatically based upon quantity/options/etc. if this is passed.
			
			// Item
			$item = item::load($this->module,$id);
			
			// Form
			$form_c = array(
				'redirect' => DOMAIN.$this->module."/cart",
				'submit' => array(
					'value' => 'Add to Cart',
					'class' => 'cart-add-input cart-add-submit',
					'row' => array(
						'class' => 'cart-add-row',
					),
					'label' => array(
						'class' => 'cart-add-label',
					),
					'field' => array(
						'class' => 'cart-add-field',
					),
				),
				'serialize' => 0,
			);
			if($this->x($c)) $form_c = array_merge_associative($form_c,$c);
			$form = form::load($form_c);
			
			// Plugins
			$quantity_unit = NULL;
			$plugins_inputs = array();
			if($plugins = m($this->module.'.plugins')) {
				foreach($plugins as $plugin => $active) {
					$plugin_inputs = $this->plugin($id,$plugin,__FUNCTION__);
					if($plugin_inputs) {
						// Tweaks
						foreach($plugin_inputs as $k => $v) {
							// Classes
							$v->row[attributes]['class'] .= ($v->row[attributes]['class'] ? " " : "")."cart-add-row";
							if($v->label and !is_array($v->label)) $v->label = array('value' => $v->label);
							$v->label[attributes]['class'] .= ($v->label[attributes]['class'] ? " " : "")."cart-add-label";
							$v->field[attributes]['class'] .= ($v->field[attributes]['class'] ? " " : "")."cart-add-field";
							$v->attributes['class'] .= ($v->attributes['class'] ? " " : "")."cart-add-input";
							
							// Quantity
							if($v->name == "plugins[quantities_units]") {
								$quantity_unit = $v;
								unset($plugin_inputs[$k]);
							}
						}
						
						// Add
						$plugins_inputs = array_merge($plugins_inputs,$plugin_inputs);
					}
				}
			}
			
			// Quantity
			$input_c = array(
				'default' => 1,
				'validate' => array(
					//'min' => $item->db('cart_quantity_minimum'),
					//'max' => $item->db('cart_quantity_maximum'),
					'required' => 1,
					'number' => 1,
				),
				'class' => 'cart-add-input',
				'row' => array(
					'class' => 'cart-add-row',
				),
				'label' => array(
					'class' => 'cart-add-label',
				),
				'field' => array(
					'class' => 'cart-add-field',
				),
			);
			$form->number('Quantity','quantity[count]',NULL,$input_c);
			// Quantity - unit
			if($quantity_unit) {
				if(!x($quantity_unit[inline])) $quantity_unit[inline] = 1;
				if(!x($quantity_unit[label][value])) $quantity_unit[label][value] = "&nbsp;";
				$form->inputs(array($quantity_unit));
			}
			
			// Plugins
			if($plugins_inputs) $form->inputs($plugins_inputs);
			
			// Hidden
			$form->hidden('module',$this->module,array('process' => 0));
			$form->hidden('id',$id,array('process' => 0));
			$form->hidden('process_action','cart_add',array('process' => 0));
			$form->hidden('process_module',$this->module,array('process' => 0));
			$form->hidden('url',URL,array('process' => 0));
			
			// Javascript
			if($c[price_text_selector]) {
				$javascript = "
<script type='text/javascript'>
$(document).ready(function() {
	$('.item-cart :input').change(function() {
		calculatePrice();
	});
});
function calculatePrice() {
	// Data
	var data = '';
	$('.item-cart :input').each(function() {
		var name = $(this).attr('name');
		var value = $(this).val();
		if(name == 'process_action') {
			name = 'ajax_action';
			value = 'cart_price';
		}
		if(name == 'process_module') name = 'ajax_module';
		if(value) data += (data ? '&' : '')+name+'='+value;
	});
	
	// Debug
	debug('data: '+data);
	
	// AJAX
	$.ajax({
		type: 'POST',
		url: DOMAIN,
		data: data,
		success: function(price) {
			// Debug
			debug('result: '+price);
			
			// Display
			if(price) $('".$c[price_text_selector]."').html(price);
		}
	});
}
</script>";
				$form->html($javascript);
			}
			
			// Return
			return $form;
		}
		
		/**
		 * Builds and returns the form object for displaying the cart form.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object for the cart form.
		 */
		function form_cart($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$this->x($c[thumb])) $c[thumb] = "s"; // Thumb size to display
			if(!$this->x($c[url_checkout])) $c[url_checkout] = DOMAIN.$this->module."/checkout";
			
			// Form
			$form_c = array(
				'submit' => 0
			);
			if($this->x($c)) $form_c = array_merge_associative($form_c,$c);
			$form = form::load($form_c);
			
			$form->html("
			<div class='cart'>");
			if($this->count()) {
				$form->html("
					<table class='core-table core-rows cart-table'>
						<thead>
							<tr class='cart-table-header'>
								<th".(m($this->module.'.db.image') ? " colspan='2'" : "")." class='cart-column-item'>Item</th>
								<th class='cart-column-price'>Price</th>
								<th class='cart-column-quantity'>Quantity</th>
								<th class='cart-column-total'>Subtotal</th>
								<th class='cart-column-icons'></th>
							</tr>
						</thead>
						<tbody>");
				
				// Items
				foreach($this->items as $k => $item) {
					$image = NULL;
					if(m($this->module.'.db.image')) $image = $item[item]->file('db.image',array('thumb' => $c[thumb]));
					$url = $item[item]->url();
					$form->html("
							<tr class='cart-item'>");
					// Image
					if(m($this->module.'.db.image')) {
						if($image->exists) {
							$image_string = "<img src='".$image->url()."' width='".$image->width()."' height='".$image->height()."' />";
							$form->html("
								<td class='cart-column-image' width='".$image->width()."'>".($url ? "<a href='".$url."'>".$image_string."</a>" : $image_string)."</td>");
						}
						else $form->html("<td class='cart-column-image'></td>");
					}
					
					// Name / description
					
					$form->html("
								<td class='cart-column-name'>
									<div class='cart-item-name'>".($url ? "<a href='".$url."'>".$item[name]."</a>" : $item[name])."</div>");
					if($item[description]) $form->html("
									<div class='cart-item-description'>".$item[description]."</div>");
					$form->html("
								</td>");
					
					// Price
					$form->html("
								<td class='cart-column-price'>
									<div class='cart-item-price'>".string_price($item[price])."</div>
								</td>");
					
					// Quantity
					$form->html("
								<td class='cart-column-quantity'>
									<div class='cart-item-quantity'>");
					$append = "</span>";
					if($item[quantity][unit][single] and $item[quantity][unit][plural]) $append .= "<span class='cart-item-quantity-unit'>".($item[quantity][count] == 1 ? $item[quantity][unit][single] : $item[quantity][unit][plural])."</span>";
					$input_c = array(
						'prepend' => "<span class='cart-item-quantity-input'>",
						'append' => $append,
						'validate' => array(
							'min' => $item[quantity][minimum],
							'max' => $item[quantity][maximum]
						)
					);
					$form->number("","items[".$item[key]."][quantity][count]",$item[quantity][count],$input_c);
					$form->html("
									</div>
								</td>");
					
					// Total
					$form->html("
								<td class='cart-column-total'>
									<div class='cart-item-total'>".string_price($item[totals][subtotal])."</div>
								</td>");
							
					// Delete
					$form->html("
								<td width='18' class='cart-column-icons'>
									<div id='cart-item-delete'><a href='".DOMAIN."?process_action=cart_delete&amp;module=".$this->module."&amp;key=".$item[key]."&amp;redirect=".urlencode(URL)."' class='i i-delete tooltip' title='Remove From Cart'></a></div>
								</td>");
					
					$form->html("
							</tr>");
				}
				
				// Total
				$form->html("
							<tr class='cart-total'>
								<td colspan='".(m($this->module.'.db.image') ? 4 : 3)."' class='cart-column-label'>
									<div class='cart-total-label'>Subtotal</div>
								</td>
								<td class='cart-column-total'>
									<div class='cart-total-price'>".string_price($this->totals[subtotal])."</div>
								</td>
								<td class='cart-column-icons'></td>
							</tr>");
				
				$form->html("
						</tbody>
					</table>
					<div class='cart-buttons'>
						<span class='cart-buttons-continue'><input type='button' value='Continue Shopping' onclick=\"".($_GET['url'] ? "location.assign('".urldecode($_GET['url'])."');" : "history.go(-1);")."\" /></span>
						<span class='cart-buttons-update'><input type='submit' value='Update Cart' /></span>
						<span class='cart-buttons-checkout'><input type='button' value='Checkout' onclick=\"location.assign('".$c[url_checkout]."');\" /></span>
						<div class='clear'></div>
					</div>
					<input type='hidden' name='process_action' value='cart_update' />
					<input type='hidden' name='process_module' value='".$this->module."' />
					<input type='hidden' name='redirect' value='".URL."' />
				</form>
				<div class='clear'></div>");
			}
			else $form->html("
				<div class='core-none'>Your cart is empty.</div>");
			$form->html("
			</div>");
			
			// Return
			return $form;
		}
		
		/**
		 * Builds and returns the form object for displaying the shipping address form.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object for the shipping address form.
		 */
		function form_shipping_address($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$this->x($c[saved][label])) $c[saved][label] = '&nbsp;'; // Label for saved dropdown.
			if(!$this->x($c[user])) $c[user] = u('id'); // User who this address belongs to
			if(!$c[same]) $c[same] = NULL; // An array of 'Same as' values we can auto-fill (see form_framework_addresses class for more info) 
			if(!$c['use']) $c['use'] = 'shipping'; // What are we using this address for (used to sort addresses by their 'used' date): shipping [default], billing
			if(!$c[redirect]) $c[redirect] = urldecode($_GET['redirect']); // Where to redirect the user to after address is saved. This is also an exmaple of how you can pass a form class config value via this method.
		
			// Selected
			$selected = $this->shipping[addresses];
			$selected_count = count($selected);
			
			// Saved config
			$saved_c = $c;
			if($selected) {
				foreach($selected as $selected_item) {
					$saved_c[selected][] = $selected_item[item]->db('id');
				}
			}
			
			// Multiple
			$multiple_active = m($this->module.'.settings.cart.shipping.multiple');
			if($this->count() == 1) $multiple_active = 0;
			if($multiple_active) {
				$multiple = $_GET['multiple'];
				if(!$this->x($multiple) and $selected_count > 1) $multiple = 1;
			}
			else $multiple = 0;
			
			// Single address
			if(!$multiple) {
				// Saved value
				$saved_c[saved][value] = $saved_c[selected][0];
		
				// Form
				$form_c = $c;
				unset($form_c[saved],$form_c[user],$form_c[same]);
				$form = form::load($form_c);
		
				// Multiple  address buttons
				if($multiple_active) {
					$html = "
		<div class='page-heading-buttons cart-checkout-shipping-buttons'>
			<ul>
				<li><a href='".url_query_append(URL,"multiple=1")."'>Ship to Multiple Addresses</a></li>
			</ul>
		</div>";
					$form->html($html);
				}
				
				// Saved / new addresses
				$module = 'addresses';
				$module_class = module::load($module);
				$inputs = $module_class->saved_inputs($saved_c);
				$form->inputs($inputs);
			
				// Hidden
				$form->hidden('process_action','cart_shipping_address');
				$form->hidden('process_module',$this->module);
			}
			// Multiple Addresses
			else {
				$form = $this->form_shipping_address_multiple($c);
			}
			
			// Return
			return $form;
		}
		
		/**
		 * Builds and returns the form object for displaying the multiple shipping address form.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object for the multiple shipping address form.
		 */
		function form_shipping_address_multiple($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$this->x($c[saved][label])) $c[saved][label] = '&nbsp;'; // Label for saved dropdown.
			if(!$this->x($c[user])) $c[user] = u('id'); // User who this address belongs to
			if(!$c[same]) $c[same] = NULL; // An array of 'Same as' values we can auto-fill (see form_framework_addresses class for more info) 
			if(!$c['use']) $c['use'] = 'shipping'; // What are we using this address for (used to sort addresses by their 'used' date): shipping [default], billing
			if(!$c[redirect]) $c[redirect] = urldecode($_GET['redirect']); // Where to redirect the user to after address is saved. This is also an exmaple of how you can pass a form class config value via this method.
		
			// Selected
			$selected = $this->shipping[addresses];
			$selected_count = count($selected);
			
			// Saved config
			$saved_c = $c;
			if($selected) {
				foreach($selected as $selected_item) {
					$saved_c[selected][] = $selected_item[item]->db('id');
				}
			}
			
			// Multiple
			$multiple_active = m($this->module.'.settings.cart.shipping.multiple');
			if($this->count() == 1) $multiple_active = 0;
			if($multiple_active) {
				$multiple = $_GET['multiple'];
				if(!$this->x($multiple) and $selected_count > 1) $multiple = 1;
			}
			else $multiple = 0;
			
			// Not active, return
			if(!$multiple_active) return;
		
			// Module
			$module = 'addresses';
			$module_class = module::load($module);
		
			// Form
			$form_c = $c;
			unset($form_c[saved],$form_c[user],$form_c[same]);
			$form = form::load($form_c);
		
			// Single address buttons
			$html = "
		<div class='page-heading-buttons cart-checkout-shipping-buttons'>
			<ul>
				<li><a href='".url_query_append(URL,"multiple=0")."'>Ship to Single Address</a></li>
			</ul>
		</div>";
			$form->html($html);
			
			$form->html("
		<div class='cart-checkout-shipping-multiple'>");
				
			if(!u('id')) {
				$form->html("
			<div class='core-red'>You must be logged in to ship to multiple addresses</div><br />
			".$this->login_form());
			}
			else {
				if(!$_GET['id']) {
					// Saved addresses?
					$saved_array = $module_class->saved_array($saved_c);
					// No, must add one
					if(!$saved_array) $_GET['id'] = "new";
				}
				
				// New Address
				if($_GET['id'] == "new") {
					$new_form = $module_class->form();
					$inputs = $new_form->inputs();
					foreach($inputs as $k => $v) {
						if($v->name) $inputs[$k]->name = "__items[address_id][addresses][new][".$v->name."]";
					}
					$form->inputs($inputs);
					$form->c[redirect] = url_query_remove(URL,'id');
				}
				// Current Addresses
				else {
					$form->html("
			<div class='cart-checkout-shipping-button-new'>
				<input type='button' value='Add A New Address' onclick=\"location.assign('".url_query_append(URL,'id=new')."');\" />
			</div>
			<table class='core-table core-rows cart-checkout-shipping-multiple-table'>
				<thead>
					<tr>
						<th".(m($this->module.'.db.image') ? " colspan='2'" : "")." align='left'>Item</th>
						<th align='left'>Address</th>
					</tr>
				</thead>
				<tbody>");
					// Items
					foreach($this->items as $k => $item) {
						$image = NULL;
						if(m($this->module.'.db.image')) $image = $item[item]->file('db.image',array('thumb' => 's'));
						$url = $item[item]->url();
						$form->html("
					<tr>");
						// Image
						if(m($this->module.'.db.image')) {
							if($image->exists) {
								$image_string = "<img src='".$image->url()."' width='".$image->width()."' height='".$image->height()."' />";
								$form->html("
						<td width='".$image->width()."'>".($url ? "<a href='".$url."'>".$image_string."</a>" : $image_string)."</td>");
							}
							else $form->html("<td></td>");
						}
						// Name
						$form->html("
						<td>
							<div class='cart-checkout-shipping-multiple-item-name'>".($url ? "<a href='".$url."'>".$item[name]."</a>" : $item[name])."</div>");
							// Description
							if($item[description]) $form->html("
							<div class='cart-checkout-shipping-multiple-item-description'>".$item[description]."</div>");
							$form->html("
						</td>");
					
						// Address
						$form->html("
						<td>
							<div class='cart-checkout-shipping-multiple-item-address'>");
						$saved_c['new'] = 0; // Don't want to include 'New Address' option because it's not user friendly have to type in the same address for multiple items if that's what they want.  Instead we're showing a 'New Address' button above the table of items.
						$saved_c[saved][name] = "addresses[".$item[key]."]";
						$saved_c[saved][value] = NULL;
						if($item[shipping][address][item]) $saved_c[saved][value] = $item[shipping][address][item]->db('id');
						$inputs = $module_class->saved_inputs($saved_c);
						$form->inputs($inputs);
						$form->html("
							</div>
						</td>");
							
						$form->html("
					</tr>");
					}
					$form->html("
				</tbody>
			</table>");
				}
			}
			
			$form->html("
		</div>");
			
			// Hidden
			$form->hidden('multiple',1);
			$form->hidden('process_action','cart_shipping_address');
			$form->hidden('process_module',$this->module);
			
			// Return
			return $form;
		}
		
		/**
		 * Builds and returns the form object for displaying the payment form.
		 *
		 * Note, $c gets passed to the payment form so all configuration of that form can be effected via this method's $c array.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object for the payment form.
		 */
		function form_payment($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$c[redirect]) $c[redirect] = urldecode($_GET['redirect']); // Where to redirect the user to after address payment is complete. This is also an exmaple of how you can pass a form class config value via this method.
		
			// Total
			$this->total(); // Just to be sure
			$total = 0; // We'll use this to get the total without shipping
		
			// Form
			$form_c = $c;
			$form_c[submit] = 0;
			$form = form::load($form_c);
			
			// Start container
			$form->html("
<div class='cart-checkout-payment'>
	<div class='cart-checkout-payment-left'>");
			
			// Payment
			$form->html("
		<div class='cart-checkout-payment-payment'>
			<div class='cart-checkout-payment-box-outer'>
				<h3 class='cart-checkout-heading'>Payment</h3>
				<div class='cart-checkout-payment-box-inner'>");
			$form_payments_c = $c;
			$form_payments = form::load('payments','new',$form_payments_c);
			$form->inputs($form_payments->inputs);
			$submit = $form_payments->c[submit];
			$submit[type] = "submit";
			$submit[value] = "Submit Order";
			$form->inputs(array($submit));
			
			$form->html("
				</div>
			</div>
		</div>");
		
			// Submit
			/*$form->html("
		<div class='cart-checkout-payment-buttons'>");
			$form->submit('Submit Order');
			$form->html("
		</div>");*/
			
			// Column break
			$form->html("
	</div>
	<div class='cart-checkout-payment-right'>");
			
			// Cart
			$form->html("
		<div class='cart-checkout-payment-cart'>
			<div class='cart-checkout-payment-box-outer'>
				<h3 class='cart-checkout-heading'>Cart</h3>
				<div class='cart-checkout-payment-box-inner'>");
			
			// Cart - items
			$form->html("
					<div class='cart-checkout-payment-cart-items'>
						<table class='core-table'>
							<thead>
								<tr>
									<th class='cart-column-name'>Item</th>");
									/*<th class='cart-column-price'>Price</th>
									<th class='cart-column-quantity'>Quantity</th>*/
			$form->html("
									<th class='cart-column-total'>Total</th>
								</tr>
							</thead>
							<tbody>");
			foreach($this->items as $k => $item) {
				$url = $item[item]->url();
				$form->html("
								<tr class='cart-item'>");
				// Name
				$form->html("
									<td class='cart-column-name'>
										<div class='cart-item-name'>".($url ? "<a href='".$url."'>".$item[name]."</a>" : $item[name])."</div>");
				// Description
				if($item[description]) $form->html("
										<div class='cart-item-description'>".$item[description]."</div>");
				$form->html("
									</td>");
					
				// Price
				/*$form->html("
									<td class='cart-column-price'>
										<div class='cart-item-price'>".string_price($item[price])."</div>
									</td>");
					
				// Quantity
				$form->html("
									<td class='cart-column-quantity'>
										<div class='cart-item-quantity'>
											<span class='cart-item-quantity-input'>".$item[quantity][count]."</span>");
				if($item[quantity][unit][single] and $item[quantity][unit][plural]) $form->html("
											<span class='cart-item-quantity-unit'>".($item[quantity][count] == 1 ? $item[quantity][unit][single] : $item[quantity][unit][plural])."</span>");
				$form->html("
										</div>
									</td>");*/
					
				// Total
				$form->html("
									<td class='cart-column-total'>
										<div class='cart-item-total'>".string_price($item[totals][subtotal])."</div>
									</td>");
					
				$form->html("
								</tr>");
			}
			
			// Cart - subtotal
			$total += $this->totals[subtotal];
			$form->html("
								<tr class='cart-totals-subtotal'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-subtotal-label'>Subtotal</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-subtotal-price'>".string_price($this->totals[subtotal])."</div>
									</td>
								</tr>");
			
			// Cart - tax
			if($this->totals[tax]) {
				$total += $this->totals[tax];
				$form->html("
								<tr class='cart-totals-tax'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-tax-label'>Tax</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-tax-price'>".string_price($this->totals[tax])."</div>
									</td>
								</tr>");
			}
			
			// Cart - discounts
			if($this->totals[discounts]) {
				$total += $this->totals[discounts];
				$form->html("
								<tr class='cart-totals-discounts'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-discounts-label'>Discounts</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-discounts-price'>".string_price($this->totals[discounts])."</div>
									</td>
								</tr>");
			}
			
			// Cart - handling
			if($this->totals[handling]) {
				$total += $this->totals[handling];
				$form->html("
								<tr class='cart-totals-handling'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-handling-label'>Handling</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-handling-price'>".string_price($this->totals[handling])."</div>
									</td>
								</tr>");
			}
			
			// Cart - insurance
			if($this->totals[insurance]) {
				$total += $this->totals[insurance];
				$form->html("
								<tr class='cart-totals-insurance'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-insurance-label'>Insurance</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-insurance-price'>".string_price($this->totals[insurance])."</div>
									</td>
								</tr>");
			}
			
			// Cart - shipping
			if($this->shipping[addresses]) {
				$rates = $this->shipping_rates(array('debug' => 0));
				if($rates) {
					$form->html("
								<tr class='cart-totals-shipping'>
									<td colspan='2'>
										<div class='cart-totals-shipping-label'>Shipping</div>
										<div class='cart-totals-shipping-price'>");
					if(count($rates) == 1) {
						$rate = array_first($rates);
						$form->hidden('totals[shipping]',$rate[key]."|".$rate[totals][total],array('id' => 'totals_shipping'));
						$form->html("
											<div class='cart-totals-shipping-price-single'>
												".string_price($rate[totals][total])."
											</div>");
					}
					else {
						$rates_options = $this->shipping_rates_options();
						$form->html("
											<div class='cart-totals-shipping-price-select'>");
						$form->select(NULL,'totals[shipping]',$rates_options,NULL,array('id' => 'totals_shipping','onchange' => 'cart_total();','validate' => array('required' => 1)));
						$form->html("
											</div>");
					}
				}
				$form->html("
										</div>
									</td>
								</tr>");
			}
			
			// Cart - total
			if($total) {
				// Display
				$form->html("
								<tr class='cart-totals-total'>
									<td colspan='1'>
										<div class='cart-total-label'>Total</div>
									</td>
									<td class='cart-totals-column-total'>
										<div class='cart-total-price'>".string_price($total)."</div>
									</td>
								</tr>");
								
				// Calculate
				$javascript = "
				<script type='text/javascript'>
				$(document).ready(function() {
					cart_total();
				});
				function cart_total() {
					// Subtotal
					var subtotal = $('#totals_subtotal').val() * 1;
					var total = subtotal;
					
					// Shipping
					var shipping = $('#totals_shipping').val();
					if(shipping) {
						var shipping_array = shipping.split('|');
						var shipping_total = shipping_array[1]  * 1;
						if(shipping_total) total += shipping_total;
					}
					
					// Total
					$('.cart-totals-total-price').html('$'+number_format(total,2));
					$('#totals_total').val(total);
				}
				</script>";
				$form->html($javascript);
				$form->hidden('totals[subtotal]',$total,array('id' => 'totals_subtotal'));
				$form->hidden('totals[total]',$total,array('id' => 'totals_total'));
			}
				
			$form->html("
							</tbody>
						</table>
					</div>");
			
			$form->html("
				</div>
			</div>
		</div>");
			
			// Shipping
			if($this->shipping[addresses]) {
				$steps = $this->checkout_steps();
				$form->html("
		<div class='cart-checkout-payment-shipping'>
			<div class='cart-checkout-payment-box-outer'>
				<h3 class='cart-checkout-heading'>Shipping</h3>
				<div class='cart-checkout-payment-box-inner'>
					<div class='cart-checkout-payment-shipping-edit'>
						<a href='".$steps[shipping]."?redirect=".URL."' class='i-button'><span class='i i-edit i-text'>Edit</span></a>
					</div>");
				foreach($this->shipping[addresses] as $key => $address) {
					$form->html("
					<div class='cart-checkout-payment-shipping-address'>
						".$address[item]->address()."
					</div>");
				}
				$form->html("
				</div>
			</div>
		</div>");
			}
			
			// End container
			$form->html("
	</div>
	<div class='clear'></div>
</div>");

			// Process
			$form->hidden('process_action','cart_purchase');
			$form->hidden('process_module',$this->module);
			
			// Return
			return $form;
		}
		
		/**
		 * Builds and returns the form object for displaying the checkout form.
		 *
		 * Note, $c[payment] gets passed to the payment form and $c[shipping] gets passed to the shipping form, so all configuration in those forms can be effected via this method $c array.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form object for the checkout form.
		 */
		function form_checkout($c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Config
			if(!$this->x($c[user])) $c[user] = u('id'); // User who's checkout out. Used for saved/new addresses and saved/new credit cards.
			if(!$this->x($c[shipping][saved][label])) $c[shipping][saved][label] = 'Address'; // Label for saved shipping address dropdown.
			if(!$c[shipping]['use']) $c[shipping]['use'] = 'shipping'; // What are we using this address for (used to sort addresses by their 'used' date): shipping [default], billing
			if(!$c[shipping][same]) $c[shipping][same] = NULL; // An array of 'Same as' values we can auto-fill (see form_framework_addresses class for more info) 
			if(!$this->x($c[payment][credit_cards][addresses][saved][label])) $c[payment][credit_cards][addresses][saved][label] = '&nbsp;'; // Label for saved billing address dropdown.
			if(!$c[payment][credit_cards][addresses][same]) $c[payment][credit_cards][addresses][same] = NULL; // An array of 'Same as' values we can auto-fill (see form_framework_addresses class for more info) 
			if(!$c[redirect]) $c[redirect] = urldecode($_GET['redirect']); // Where to redirect the user to after address checkout is complete. This is also an exmaple of how you can pass a form class config value via this method.
		
			// Total
			$this->total(); // Just to be sure
			$total = 0; // We'll use this to get the total without shipping
			
			// Shipping
			if(m('shipping') and m($this->module.'.settings.cart.shipping.active')) {
				// Shipping - selected
				$selected = $this->shipping[addresses];
				$selected_count = count($selected);
				
				// Shipping - multiple
				$multiple_active = m($this->module.'.settings.cart.shipping.multiple');
				if($this->count() == 1) $multiple_active = 0;
				if($multiple_active) {
					$multiple = $_GET['multiple'];
					if(!$this->x($multiple) and $selected_count > 1) $multiple = 1;
				}
				else $multiple = 0;
			}
		
			// Form
			$form_c = $c;
			$form_c[submit] = 0;
			$form = form::load($form_c);
			
			// Start container
			$form->html("
<div class='cart-checkout'>");
			
			// Buttons
			$buttons = NULL;
			if(!$c[user]) {
				$buttons[DOMAIN."login?redirect=".URL] = "Already a member? Login";
			}
			if($multiple_active) {
				if($multiple) $buttons[url_query_append(URL,"multiple=0")] = "Ship to Single Address";
				else $buttons[url_query_append(URL,"multiple=1")] = "Ship to Multiple Addresses";
			}
			if($buttons) {
				$html = "
		<div class='page-heading-buttons cart-checkout-buttons'>
			<ul>";
				foreach($buttons as $button_url => $button_label) $html .= "
				<li><a href='".$button_url."'>".$button_label."</a></li>";
				$html .= "
			</ul>
		</div>";
				$form->html($html);
			}
			
			$form->html("
	<div class='cart-checkout-left'>");
			
			// Cart
			$form->html("
		<div class='cart-checkout-cart'>
			<div class='cart-checkout-box-outer'>
				<h3 class='cart-checkout-heading'>Cart</h3>
				<div class='cart-checkout-box-inner'>");
			
			// Cart - items
			$form->html("
					<div class='cart-checkout-cart-items'>
						<table class='core-table'>
							<thead>
								<tr>
									<th class='cart-column-name'>Item</th>");
									/*<th class='cart-column-price'>Price</th>
									<th class='cart-column-quantity'>Quantity</th>*/
			$form->html("
									<th class='cart-column-total'>Total</th>
								</tr>
							</thead>
							<tbody>");
			foreach($this->items as $k => $item) {
				$url = $item[item]->url();
				$form->html("
								<tr class='cart-item'>");
				// Name
				$form->html("
									<td class='cart-column-name'>
										<div class='cart-item-name'>".($url ? "<a href='".$url."'>".$item[name]."</a>" : $item[name])."</div>");
				// Description
				if($item[description]) $form->html("
										<div class='cart-item-description'>".$item[description]."</div>");
				$form->html("
									</td>");
					
				// Price
				/*$form->html("
									<td class='cart-column-price'>
										<div class='cart-item-price'>".string_price($item[price])."</div>
									</td>");
					
				// Quantity
				$form->html("
									<td class='cart-column-quantity'>
										<div class='cart-item-quantity'>
											<span class='cart-item-quantity-input'>".$item[quantity][count]."</span>");
				if($item[quantity][unit][single] and $item[quantity][unit][plural]) $form->html("
											<span class='cart-item-quantity-unit'>".($item[quantity][count] == 1 ? $item[quantity][unit][single] : $item[quantity][unit][plural])."</span>");
				$form->html("
										</div>
									</td>");*/
					
				// Total
				$form->html("
									<td class='cart-column-total'>
										<div class='cart-item-total'>".string_price($item[totals][subtotal])."</div>
									</td>");
					
				$form->html("
								</tr>");
			}
			
			// Cart - subtotal
			$total += $this->totals[subtotal];
			$form->html("
								<tr class='cart-totals-subtotal'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-subtotal-label'>Subtotal</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-subtotal-price'>".string_price($this->totals[subtotal])."</div>
									</td>
								</tr>");
			
			// Cart - discounts - only if we already have one or the module is active (in which case we may be able to add via AJAX)
			if(m('discounts') or $this->totals[discounts]) {
				$total += $this->totals[discounts];
				$form->html("
								<tr class='cart-totals-discounts'".($this->totals[discounts] ? "" : " style='display:none;'").">
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-discounts-label'>Discounts</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-discounts-price'>".string_price($this->totals[discounts])."</div>
									</td>
								</tr>");
			}
			
			// Cart - tax - always present (if sometimes hidden) if tax rates are defined as we may add tax via AJAX (if set shipping to state we charge tax form)
			if($this->tax) {
				$total += $this->totals[tax];
				$form->html("
								<tr class='cart-totals-tax'".($this->totals[tax] ? "" : " style='display:none;'").">
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-tax-label'>Tax</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-tax-price'>".string_price($this->totals[tax])."</div>
									</td>
								</tr>");
			}
			
			// Cart - handling
			if($this->totals[handling]) {
				$total += $this->totals[handling];
				$form->html("
								<tr class='cart-totals-handling'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-handling-label'>Handling</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-handling-price'>".string_price($this->totals[handling])."</div>
									</td>
								</tr>");
			}
			
			// Cart - insurance
			if($this->totals[insurance]) {
				$total += $this->totals[insurance];
				$form->html("
								<tr class='cart-totals-insurance'>
									<td colspan='1' class='cart-column-label'>
										<div class='cart-totals-insurance-label'>Insurance</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-insurance-price'>".string_price($this->totals[insurance])."</div>
									</td>
								</tr>");
			}
			
			// Cart - shipping
			if(m('shipping') and m($this->module.'.settings.cart.shipping.active') and !m($this->module.'.settings.cart.shipping.included')) {
				$form->html("
								<tr class='cart-totals-shipping'>
									<td colspan='2'>
										<div class='cart-totals-shipping-label'>Shipping</div>
										<div class='cart-totals-shipping-price'>");
				if($multiple) {
					$inputs = $this->form_checkout_cart_shipping(array('return' => 'array'));
					$form->inputs($inputs);
				}
				else {
					$form->hidden("totals[shipping]"); // This will get replaced via AJAX, but need to 'register' it in the form instance so it'll get processed
				}
				$form->html("	
										</div>
									</td>
								</tr>");
			}
			
			// Cart - total
			if($total) {
				// Display
				$form->html("
								<tr class='cart-totals-total'>
									<td colspan='1'>
										<div class='cart-totals-total-label'>Total</div>
									</td>
									<td class='cart-column-total'>
										<div class='cart-totals-total-price'>".string_price($total)."</div>
									</td>
								</tr>");
								
				// Calculate
				if(m('shipping') and m($this->module.'.settings.cart.shipping.active') and !m($this->module.'.settings.cart.shipping.included')) {
					$javascript = "
					<script type='text/javascript'>
					$(document).ready(function() {
						cart_total();
					});
					function cart_total() {
						// Subtotal
						var subtotal = $('#totals_subtotal').val() * 1;
						var total = subtotal;
						
						// Shipping
						var shipping = $('#totals_shipping').val();
						if(shipping) {
							var shipping_array = shipping.split('|');
							var shipping_total = shipping_array[1]  * 1;
							if(shipping_total) total += shipping_total;
						}
						
						// Total
						$('.cart-totals-total-price').html('$'+number_format(total,2));
						$('#totals_total').val(total);
					}
					</script>";
					$form->html($javascript);
					$form->hidden('totals[subtotal]',$total,array('id' => 'totals_subtotal'));
					$form->hidden('totals[total]',$total,array('id' => 'totals_total'));
				}
			}
				
			$form->html("
							</tbody>
						</table>
					</div>");
			
			$form->html("
				</div>
			</div>
		</div>");
			
			// Column break
			$form->html("
	</div>
	<div class='cart-checkout-right'>");
		
			// Shipping
			if(m('shipping') and m($this->module.'.settings.cart.shipping.active')) {
				$module = 'addresses';
				$module_class = module::load($module);
				$form->html("
		<div class='cart-checkout-shipping'>
			<div class='cart-checkout-box-outer'>
				<h3 class='cart-checkout-heading'>Shipping Address</h3>
				<div class='cart-checkout-box-inner'>");
			
				// Shipping - config
				$shipping_c = $c[shipping];
				if($selected) {
					foreach($selected as $selected_item) {
						if($selected_item[item]) $shipping_c[selected][] = $selected_item[item]->db('id');
					}
				}
				
				// Shipping - single address
				if(!$multiple) {
					// Same as..
					if(u('id') and !$shipping_c[same]) {
						$shipping_c[same] = array(
							'Same as my account' => array(
								'address_first_name' => u('user_first_name'),
								'address_last_name' => u('user_last_name'),
								'address_company' => u('user_company'),
								'address_address' => u('user_address'),
								'address_address_2' => u('user_address_2'),
								'address_city' => u('user_city'),
								'address_state' => u('user_state'),
								'address_zip' => u('user_zip'),
								'address_country' => u('user_country'),
								'address_phone' => u('user_phone'),
								'address_email' => u('user_email'),
							)
						);
					}
					
					// Saved value
					$saved_c[saved][value] = $saved_c[selected][0];
					
					// Saved / new addresses
					$inputs = $module_class->saved_inputs($shipping_c);
					$form->inputs($inputs);
					
					// Javascript
					if(!m($this->module.'.settings.cart.shipping.included')) {
						$javascript = "
					<script type='text/javascript'>
					$(document).ready(function() {
						cart_shipping();
						
						$('#address_id').change(function() {
							cart_shipping();
						});
						
						$(':input[name=\"__items[address_id][addresses][new][address_country]\"], :input[name=\"__items[address_id][addresses][new][address_state]\"], :input[name=\"__items[address_id][addresses][new][address_zip]\"]').change(function() {
							cart_shipping();
						});
					});
					
					function cart_shipping() {
						// Address
						var address = $('.cart-checkout-shipping #address_id').val();
						// Data
						var data = 'ajax_action=cart_shipping&ajax_module=".$this->module."&address='+address;
					
						// New
						if(address == 'new') {
							var country = $(':input[name=\"__items[address_id][addresses][new][address_country]\"]').val();
							var state = $(':input[name=\"__items[address_id][addresses][new][address_state]\"]').val();
							var zip = $(':input[name=\"__items[address_id][addresses][new][address_zip]\"]').val();
							data += '&country='+country+'&state='+state+'&zip='+zip;
							
							if((!x(country) || !x(state) || !x(zip)) && !$('.cart-totals-shipping-price :input[name=\"totals[shipping]\"]').length && !$('.cart-totals-shipping-price div.loader').length) {
								return;
							}
						}
						
						// Selected
						var selected = $('#totals_shipping').val();
						data += '&selected='+selected;
						
						// Loader
						loader('.cart-totals-shipping-price',{position:'right',image_url:'".DOMAIN."core/core/images/ajax-loader-sm.gif'});
						$('.cart-totals-shipping-price').append(\"<input type='text' name='shipping_loading' class='required' style='visibility:hidden;width:0px;height:0px;padding:0px;margin:0px;' /><label for='shipping_loading' class='error'>Please wait..</label>\");
						
						// Ajax
						debug('data: '+data);
						$.ajax({
							type: 'POST',
							url: DOMAIN,
							data: data,
							dataType: 'json',
							success: function(result) {
								// Debug
								debug('tax: '+result.tax+', subtotal: '+result.subtotal+', shipping: <xmp>'+result.shipping+'</xmp>'+', debug: '+result.debug);
								
								// Tax
								if(result.tax > 0) $('.cart-totals-tax').show();
								else $('.cart-totals-tax').hide();
								$('.cart-totals-tax-price').html('$'+number_format(result.tax,2));
								
								// Shipping
								$('.cart-totals-shipping-price').html(result.shipping);
								
								// Total
								$('#totals_subtotal').val(result.subtotal);
								cart_total();
							}
						});
					}
					</script>";
						$form->html($javascript);
					}
				}
				// Shipping - multiple addresses
				else {
					$form->html("
					<div class='cart-checkout-shipping-multiple'>
						<div class='cart-checkout-shipping-multiple-edit'>
							<a href='".url_query_append(URL,'multiple=1')."' class='i-button'><span class='i i-edit i-text'>Edit</span></a>
						</div>");
					foreach($this->shipping[addresses] as $key => $address) {
						$form->html("
						<div class='cart-checkout-shipping-multiple-address'>
							".$address[item]->address()."
						</div>");
					}
					$form->html("
					</div>");
				}
					
				$form->html("
				</div>
			</div>
		</div>");
			}
			
			// Discounts
			if(m('discounts')) {
				$form->html("
		<div class='cart-checkout-discounts'>
			<div class='cart-checkout-box-outer'>
				<h3 class='cart-checkout-heading'>Discounts</h3>
				<div class='cart-checkout-box-inner'>
					<div class='cart-checkout-discounts-discounts'>
						".$this->discounts_display()."
					</div>
					<div class='cart-checkout-discounts-add'>
						<a href='".D."?ajax_action=cart_discount_add&amp;ajax_module=".$this->module."' class='i-button overlay {type:\"iframe\",width:340,height:120}' title='Discount'>
							<span class='i i-add i-text'>Add a Discount</span>
						</a>
						<div class='clear'></div>
					</div>
				</div>
			</div>
		</div>");
				
				// Javascript
				/*$javascript = "
		<script type='text/javascript'>
		$(document).ready(function() {
			cart_discounts()
		});
		
		function cart_discounts() {
		
		}
		</script>";
				$form->html($javascript);*/
			}
			
			// Text
			if(m($this->module.'.settings.cart.text.active')) {
				$label = m($this->module.'.settings.cart.text.label');
				if(!$label) $label = "Special Instructions";
				$form->html("
		<div class='cart-checkout-text'>
			<div class='cart-checkout-box-outer'>
				<h3 class='cart-checkout-heading'>".$label."</h3>
				<div class='cart-checkout-box-inner'>
					<div class='cart-checkout-text-input'>");
				$form->textarea(NULL,'text');
				$form->html("
					</div>
				</div>
			</div>
		</div>");
			}
			
			// Payment
			$form->html("
		<div class='cart-checkout-payment'>
			<div class='cart-checkout-box-outer'>
				<h3 class='cart-checkout-heading'>Payment</h3>
				<div class='cart-checkout-box-inner'>");
			$form_payments_c = $c[payments];
			if(!$form_payments_c[credit_cards][addresses][same]) {
				$form_payments_c[credit_cards][addresses][same] = array(
					'Same as shipping' => 'cart_shipping_address'
				);
				$form->html("
				<script type='text/javascript'>
				function cart_shipping_address(\$form,columns) {
					// Values
					var values = {};
					
					// Address
					var address = $('.cart-checkout-shipping #address_id').val();
					
					// Saved
					if(address > 0) {
						$.ajax({
							type: 'POST',
							url: DOMAIN,
							data: 'ajax_action=addresses_item_values&ajax_module=addresses&id='+address,
							dataType: 'json',
							success: function(values) {
								$.each(columns,function(k,v) {
									if(v != 'address_country' || values[v]) {
										if(values[v]) value = values[v];
										else value = null;
										
										$('div.addresses-form-'+v+'-row :input',\$form).val(value).change();
									}
								});
							}
						});
					}
					// New
					if(address == 'new') {
						$('.cart-checkout-shipping .addresses-saved-new :input').each(function() {
							var name = $(this).attr('name');
							if(name) {
								name = str_replace('__items[address_id][addresses][new][','',name);
								name = str_replace(']','',name)
								values[name] = $(this).val();
							}
						});
					}
					
					// Return
					return values;
				}
				</script>");
			}
			$form_payments = form::load('payments','new',$form_payments_c);
			$form->inputs($form_payments->inputs);
			
			$form->html("
				</div>
			</div>
		</div>");
		
			// Submit
			$form->html("
		<div class='cart-checkout-submit'>");
			$form->submit('Submit Order');
			$form->html("
		</div>");
			
			// End container
			$form->html("
	</div>
	<div class='clear'></div>
</div>");

			// Process
			$form->hidden('process_action','cart_purchase');
			$form->hidden('process_module',$this->module);
			
			// Return
			return $form;
		}
		
		/**
		 * Returns inputs used for selecting shipping methdod during checkout.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string|array Either the HTML string for the inputs or an array of the inputs that need to be added to the checkout form.
		 */
		function form_checkout_cart_shipping($c = NULL) {
			// Config
			if(!$c['return']) $c['return'] = 'string'; // What to return: string (a string of hte HTML for displaing the inputs), array (an array of inputs)
			if(!$this->x($c[selected])) $c[selected] = $this->shipping[selected][key]; // The previously selected shipping method
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// Rates
			if($this->shipping[addresses]) $rates = $this->shipping_rates(array('debug' => $c[debug]));
			
			// Form
			$form = form::load();
			
			if($rates or !$this->shipping[address]) {
				// Single method, auto-select
				if(count($rates) == 1) {
					$rate = array_first($rates);
					$form->hidden('totals[shipping]',$rate[key]."|".$rate[totals][total],array('id' => 'totals_shipping'));
					$form->html("
											<div class='cart-totals-shipping-price-single'>
												".string_price($rate[totals][total])."
											</div>");
				}
				// Multiple methods, must choose
				else if(count($rates) > 0) {
					// Options
					$rates_options = $this->shipping_rates_options();
					
					// Selected was just the key, need to match key|rate
					if($c[selected] and is_int_value($c[selected])) {
						$selected_length = strlen($c[selected]) + 1;
						foreach($rates_options as $k => $v) {
							if(substr($k,0,$selected_length) == $c[selected]."|") {
								$c[selected] = $k;
								break;
							}
						}
					}
					
					$form->html("
											<div class='cart-totals-shipping-price-select'>");
					$form->select(NULL,'totals[shipping]',$rates_options,$c[selected],array('id' => 'totals_shipping','onchange' => 'cart_total();','validate' => array('required' => 1)));
					$form->html("
											</div>");
				}
				// No address yet
				else if(!$this->shipping[addresses]) {
						$form->html("
											<div class='cart-totals-shipping-price-address'>
												<input type='text' name='shipping_address' class='required' style='visibility:hidden;width:0px;height:0px;padding:0px;margin:0px;' />
												<label for='shipping_address'>Please enter your address</label>
											</div>");
				}
				// Error
				else {
						$form->html("
											<div class='cart-totals-shipping-price-error'>
												<input type='text' name='shipping_error' class='required' style='visibility:hidden;width:0px;height:0px;padding:0px;margin:0px;' />
												<label for='shipping_error' class='error' style='display:inline;'>Error calculating shipping</label>
											</div>");
				}
				
				// HTML
				if($c['return'] != "array") {
					$html = NULL;
					foreach($form->inputs as $input) {
						$html .= $form->input_render($input);	
					}
				}
			}
				
			// Cache
			$this->cache_save();
			
			// Return
			if($c['return'] == "array") return $form->inputs;
			else return $html;
		}
		
		/**
		 * Calls up the given method on the given plugin_cart class in the given plugin so we can process the param(s) through that plugin.
		 *
		 * @param int $id The id of the item we're working with.
		 * @param string $plugin The plugin we're working with.
		 * @param string $method The method you're calling on the class.
		 * @param array $params An array of params to pass to the method. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed Whatever the plugin class method returned.
		 */
		function plugin($id,$plugin,$method,$params = NULL,$c = NULL) {
			// Error
			if(!$plugin or !$method or !$this->module) return;
			
			// Params
			if(!$params) $params = array();
			
			// Active?
			if(m($this->module.'.plugins.'.$plugin) == 1) {
				// Class
				/*$class = load_class_plugin('plugin_cart',array($this->module,$id,$plugin),$this->module,$id,$plugin,c);
				if($class) {
					// Instance
					$instance = new $class($this->module,$id,$plugin);*/
					$instance = plugin_cart::load($this->module,$id,$plugin);
					// Method
					if(method_exists($instance,$method)) {
						// Call
						//print "calling ".$method." on plugin ".$plugin.", class: ".$class."<br />";
						$return = call_user_func_array(array($instance,$method),$params);
					}
				//}
			}
			
			// Return
			return $return;
		}
		
		/**
		 * Calls up the given method on the given plugin_item_cart class in the given plugin item so we can process the param(s) through that plugin item.
		 *
		 * @param int $id The id of the item we're working with.
		 * @param string $plugin The plugin we're working with.
		 * @param int $plugin_id The id of the plugin item we're working with.
		 * @param string $method The method you're calling on the class.
		 * @param array $params An array of params to pass to the method. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed Whatever the plugin class method returned.
		 */
		function plugin_item($id,$plugin,$plugin_id,$method,$params = NULL,$c = NULL) {
			// Error
			if(!$plugin or !$method or !$this->module) return;
			
			// Active?
			if(m($this->module.'.plugins.'.$plugin) == 1) {
				// Class
				/*$class = load_class_plugin('plugin_item_cart',array($this->module,$id,$plugin,$plugin_id),$this->module,$id,$plugin,$c);
				if($class) {
					// Instance
					$instance = new $class($this->module,$id,$plugin,$plugin_id);*/
					$instance = plugin_item_cart::load($this->module,$id,$plugin,$plugin_id);
					
					// Method
					if(method_exists($instance,$method)) {
						// Call
						//print "calling ".$method." on plugin ".$plugin.", class: ".$class." (plugin id: ".$plugin_id.")<br />";
						$return = call_user_func_array(array($instance,$method),$params);
					}
				//}
			}
			
			// Return
			return $return;
		}
		
		/**
		 * Returns array of steps in the moduele's checkout process.
		 *
		 * @return array An array of steps in array('name' => 'url') format.
		 */
		function checkout_steps() {
			// Error
			if(!$this->module) return;
			
			// Module
			$module_class = module::load($this->module);
			
			// Base url
			$url_base = $module_class->url('checkout');
			
			// User
			if(!u('id')) $steps[user] = $url_base;
			// Shipping
			if($module_class->v('settings.cart.shipping.active')) $steps[shipping] = $url_base."/shipping";
			// Payment
			$steps[payment] = $url_base.($steps ? "/payment" : "");
			
			// Return
			return $steps;
		}
		
		/**
		 * Determines what step we're on in the checkout process.
		 *
		 * @param array $steps An array of steps in the checkout process. Default = steps determined by $this->checkout_steps()
		 * @return array An array of steps in array('name' => 'url') format.
		 */
		function checkout_step($steps = NULL) {
			// Error
			if(!$this->module) return;
			
			// Steps
			if(!$steps) $steps = $this->checkout_steps();
			
			// Page
			$page = g('page');
			
			// Variable 0 - example: /products/checkout/shipping (shipping is variable[0]
			$step = $page->variables[0];
			// Thanks - no futher checking neeeded
			if($step == "thanks") {}
			// User - force user if not logged in (step only exists if not logged in)
			else if($steps[user]) $step = "user";
			// Shipping - force shipping if it's active and we haven't selected a shipping method
			else if($steps[shipping] and !$this->shipping[addresses]) $step = "shipping";
			
			// No step, default
			if(!$step) {
				if($steps[user]) $step = "user";
				else if($steps[shipping]) $step = "shipping";
				else if($steps[payment]) $step = "payment";
			}
			
			// Return
			return $step;
		}
		
		/**
		 * Saves the purchased cart as an order.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the order.
		 */
		function order($c = NULL) {
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			// Config - notifications - $c[notifications] = 0 means we won't send any. Otherwise, you can pass an array of configuration options that will be passed on to the item_orders classes notifications() method.
			if(!$this->x($c[notifications][debug])) $c[notifications][debug] = $c[debug];
			
			// Module
			$module = 'orders';
			$module_class = module::load($module);
			
			// Values
			$order_values = array(
				'module_code' => $this->module,
				'user_id' => $this->user,
				'user_ip' => IP,
				'payment_id' => $this->payment,
				'shipping_id' => ($this->shipping[selected][item] ? $this->shipping[selected][item]->db('id') : ""),
				'order_text' => $this->text,
				'order_total' => $this->totals[total],
				'order_total_subtotal' => $this->totals[subtotal],
				'order_total_discounts' => $this->totals[discounts],
				'order_total_tax' => $this->totals[tax],
				'order_total_handling' => $this->totals[handling],
				'order_total_insurance' => $this->totals[insurance],
				'order_total_shipping' => $this->totals[shipping],
				'order_currency' => $this->currency,
				'order_handling_price' => $this->handling[price],
				'order_handling_charge' => $this->handling[charge],
				'order_shipping_price' => $this->shipping[price],
				'order_shipping_charge' => $this->shipping[charge],
			);
			
			// Item
			$order_item = $module_class->item('new');
			
			// Save
			$order_item->save($order_values,$c);
			$this->order = $order_item->db('id');
				
			// Items
			if($this->items) {
				$child_module = "orders_items";
				$child_module_class = module::load($child_module);
				foreach($this->items as $key => $item) {
					// Values
					$child_values = array(
						'order_id' => $order_item->db('id'),
						'module_code' => $this->module,
						'item_id' => $item[item]->db('id'),
						'address_id' => ($item[shipping][address][item] ? $item[shipping][address][item]->db('id') : NULL),
						'item_key' => $item[key],
						'item_name' => $item[name],
						'item_description' => $item[description],
						'item_price' => $item[price],
						'item_quantity' => $item[quantity][count],
						'item_quantity_unit_single' => $item[quantity][unit][single],
						'item_quantity_unit_plural' => $item[quantity][unit][plural],
						'item_total' => $item[totals][total],
						'item_total_subtotal' => $item[totals][subtotal],
						'item_total_discounts' => $item[totals][discounts],
						'item_total_tax' => $item[totals][tax],
						'item_total_handling' => $item[totals][handling],
						'item_total_insurance' => $item[totals][insurance],
						'item_total_shipping' => $item[totals][shipping],
						'item_handling_price' => $item[handling][price],
						'item_handling_charge' => $item[handling][charge],
						'item_shipping_price' => $item[shipping][price],
						'item_shipping_charge' => $item[shipping][charge],
						'item_shipping_weight_weight' => $item[shipping][weight][weight],
						'item_shipping_weight_unit' => $item[shipping][weight][unit],
						'item_shipping_dimensions_width' => $item[shipping][dimensions][width],
						'item_shipping_dimensions_length' => $item[shipping][dimensions][length],
						'item_shipping_dimensions_height' => $item[shipping][dimensions][height],
						'item_shipping_dimensions_unit' => $item[shipping][dimensions][unit],
						'item_custom' => data_serialize($item[custom]),
					);
					
					// Item
					$child_item = $child_module_class->item('new');
					
					// Save
					$child_item->save($child_values,$c);
					
					// Address
					if($item[shipping][address][item]) {
						// Update 'used' date so it's first in the list from now on
						$item[shipping][address][item]->save(array('address_used_shipping' => time()));
					}
					
					// Plugin params
					$plugin_params = array(
						$item,
						$child_item->db('id'),
						$c,
					);
					
					// Plugin Ids
					if($item[plugins]) {
						$child_plugin_module = "orders_items_plugins";
						$child_plugin_module_class = module::load($child_plugin_module);
						foreach($item[plugins] as $plugin => $plugin_ids) {
							if(!is_array($plugin_ids)) $plugin_ids = array($plugin_ids);
							foreach($plugin_ids as $plugin_id) {
								// Values
								$child_plugin_values = array(
									'item_id' => $child_item->db('id'),
									'plugin_code' => $plugin,
									'plugin_id' => $plugin_id,
								);
								
								// Item
								$child_plugin_item = $child_plugin_module_class->item('new');
								
								// Save
								$child_plugin_item->save($child_plugin_values,$c);
								
								// Process
								$this->plugin_item($item[item]->db('id'),$plugin,$plugin_id,__FUNCTION__,$plugin_params);
							}
						}
					}
					
					// Module plugins
					if($plugins = m($this->module.'.plugins')) {
						foreach($plugins as $plugin => $active) {
							// Process
							$this->plugin($item[item]->db('id'),$plugin,__FUNCTION__,$plugin_params);
						}
					}
				}
			}
			
			// Discounts
			if($this->discounts) {
				$child_module = "orders_discounts";
				$child_module_class = module::load($child_module);
				foreach($this->discounts as $key => $discount) {
					// Values
					$child_values = array(
						'order_id' => $order_item->db('id'),
						'discount_id' => ($discount[item] ? $discount[item]->db('id') : ""),
						'discount_name' => $discount[name],
						'discount_type' => $discount[type],
						'discount_rate' => $discount[rate],
						'discount_total' => $discount[total],
					);
					
					// Item
					$child_item = $child_module_class->item('new');
					
					// Save
					$child_item->save($child_values,$c);
				}
			}
			
			// Notifications // This can't be called until $order->paid() is called, which happens outside of this
			/*if($this->x($c[notifications])) {
				$order_item->notifications('charge',$c[notifications]);
			}*/
			
			// Return
			return $order_item->db('id');
		}
		
		/**
		 * Clears contents of the cart.
		 */
		function clear() {
			// Clear
			unset(
				$this->order,
				$this->payment
			);
			
			// Parent
			parent::clear();
		}
		
		/**
		 * Retrieves and restores the cached cart object (if one exists).
		 *
		 * Same as the core cart classes method, but includes modules which may not have been included yet.
		 */
		function cache_get() {
			// Error
			if(!$this->cache_key) return;
			
			// Modules - need to load these as they may not have been included yet
			load_class_module('module',NULL,'addresses');
			load_class_module('item',NULL,'addresses');
			load_class_module('module',NULL,$this->module);
			load_class_module('item',NULL,$this->module);
			load_class_module('cart',NULL,$this->module);
			
			// Cache
			//$cache = $_SESSION[$this->cache_key]; // Session
			$cache = cache_get('cart/'.$this->cookie().'/'.$this->cache_key,array('life' => $this->c[cache_life],'force' => 1)); // Cache
			
			// Exists?
			if($cache) {
				// Yes, restore
				foreach($cache as $k => $v) {
					$this->$k = $v;
				}
			}	
		}
	}
}
?>