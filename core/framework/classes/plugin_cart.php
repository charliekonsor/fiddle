<?php
if(!class_exists('plugin_cart',false)) {
	/**
	 * A class used for tweaking item arrays within the cart based upon a plugin used by the item's module.
	 *
	 * @package kraken\cart
	 */
	class plugin_cart extends plugin {
		/**
		 * Loads and returns an instance of either the core plugin_cart class or (if it exists) the plugin specific plugin_cart class.
		 *
		 * Example:
		 * - $plugin_cart = plugin_cart::load($module,$id,$plugin,$c);
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon.
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core plugin_cart class or (if it exists) the plugin specific plugin_cart class.
		 */
		static function load($module,$id,$plugin,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_plugin(__CLASS__,$params,$module,$id,$plugin,$c); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$id,$plugin,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$c = NULL) {
			self::plugin_cart($module,$id,$plugin,$c);
		}
		function plugin_cart($module,$id,$plugin,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Processes the given item array through this plugin and returns the updated array.
		 *
		 * @param object $cart The cart object we'll be adding this item to.
		 * @param array $item The item array you want to process.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The item array after it's been processed by this plugin.
		 */
		function item_add($cart,$item,$c = NULL) {
			// Return
			return $item;
		}
		
		/**
		 * Makes sure an item meets all requirments assigned to it specifically or globally.
		 *
		 * If an error(s) occured, it'll appear in the item's 'errors' array. Example: 'errors' => array("Please select a quantity of 10 or less.")
		 *
		 * @param array $item The array of the item you want to validate.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The array of the item, including any errors that occured during validation.
		 */
		function item_validate($item,$c = NULL) {
			// Return
			return $item;	
		}
		
		/**
		 * Returns array of plugin specific form inputs for the add to cart form.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of inputs to add to the add to cart form for this plugin.
		 */
		function form_add($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Inputs
			$inputs = NULL;
			
			// Return
			return $inputs;
		}
		
		/**
		 * Called when an item is saved in a new order.
		 *
		 * @param array $item The item array of the item as it appears in the cart.
		 * @param int $id The 'id' (in the orders_items module) of the item that was just saved.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function order($item,$id,$c = NULL) {
			
		}
		
		/**
		 * Called when an item is cancelled in an order.
		 *
		 * @param int $id The 'id' (in the orders_items module) of the item that we just cancelled.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function cancel($id,$c = NULL) {
			
		}
	}
}
?>