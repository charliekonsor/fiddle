<?php
if(!class_exists('module_api',false)) {
	/**
	 * An extenion of the module class with functionality specific to the api.
	 *
	 * @package kraken\api
	 */
	class module_api extends module {
		/**
		 * Loads and returns an instance of either the core module_api class or (if it exists) the module specific module_api class.
		 *
		 * Example:
		 * - $module_api = module_api::load($module,$c);
		 *
		 * @param string $module The module of the item.
		 * @return object An instance of either the core module_api class or (if it exists) the module specific module_api class.
		 */
		static function load($module,$c = NULL) {
			$params = func_get_args(); // Must be outside function call
			$class_load = load_class_module(__CLASS__,$params,$module); // No way to instanitiate class with array of params so we'll just return the class name and...
			return new $class_load($module,$c); // ...manually pass the params here
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$c = NULL) {
			self::module_api($module,$c);
		}
		function module_api($module,$c = NULL) {
			parent::__construct($module,$c);
		}
		
		/**
		 * Handles 'get' calls for this module, returning either several items or (if $api->call[id] exists) a single item in the results.
		 *
		 * @param object $api The api object this was called from, including the $api->call array of module, id, etc.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, including the resulting item(s), as it's used in the API.
		 */
		function get($api,$c = NULL) {
			// Error
			if(!$api) return;	
			
			// Item
			if($api->call[id] > 0) {
				// Item
				$item = item_api::load($this->module,$api->call[id]);
				
				// Success
				if($item->db('id')) {
					$results[item] = $item->item($api);
				}
				// Error
				else {
					$results[status] = 0;
					$results[message] = "We couldn't find this ".strtolower($this->v('single')).".";
				}
			}
			// Items
			else $results = $this->items($api,$c);
			
			// Return
			return $results;
		}
		
		/**
		 * Handles 'save' calls for this module. Saves an item and returns the new/updated item's array.
		 *
		 * @param object $api The api object this was called from, including the $api->call array of module, id, etc.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, including the updated item, as it's used in the API.
		 */
		function save($api,$c = NULL) {
			// Error
			if(!$api or !$api->call[id]) return;
			
			// Item
			$item = $this->item($api->call[id]);
			
			// Values
			$post = $api->call[post][values];
			if($post) {
				$this->db->q("INSERT INTO test SET test = '".a("save. some values. post: ".return_array($api->call[post]).". values raw: ".return_array($post),1)."'");
				// Passed an array
				if(is_array($post)) $values = s($post);
				else {
					$post = trim(s($post));
					// Passed JSON
					$values = json_decode($post,1); 
					// Passed XML
					if(!is_array($values)) { 
						if(substr($post,0,8) != "<values>") $post = "<values>".$post."</values>";
						include_function('xml');
						$values = xml2array($post,array('debug' => $c[debug]));
					}
				}
				
				if($values) {
					// Wrapped values in a 'values' element (values => array(), <values>{data}</values>), but just want the actual values
					if(is_array($values[values])) $values = $values[values];
					
					$this->db->q("INSERT INTO test SET test = '".a("save. values: ".return_array($values).", values string: ".http_build_query($values),1)."'");
				}
			}
			
			// No values
			if(!$post) {
				$error = "You didn't pass any values to save.";
			}
			// Couldn't interpret values
			else if(!$values) {
				$error = "There was an error interpreting the values you passed.";	
			}
			// Doesn't exist
			else if($api->call[id] > 0 and !$item->db('id')) {
				$error = "We couldn't find this ".strtolower($this->v('single')).".";
			}
			// No permission
			else if(!$item->permission('edit') and $api->c[permission]) {
				$error = "You don't have permission to do this.";
			}
			
			// Error
			if($error) {
				$results = array(
					'status' => 0,
					'message' => $error
				);
			}
			// Save
			else {
				// Format
				debug("values: ".return_array($values),$c[debug]);
				$values = $item->format($values,$this->v('api.actions.'.__FUNCTION__.'.values'),$api,"in");
				debug("values (formatted): ".return_array($values),$c[debug]);
				
				// Save
				$item->save($values,$c);
				
				// Results
				$results = array(
					'status' => 1,
					'message' => "This ".strtolower($this->v('single'))." has been successfully saved.",
					'item' => $item->item($api)
				);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Handles 'delete' calls for this module. Deletes the given item as long as user has permission to.
		 *
		 * @param object $api The api object this was called from, including the $api->call array of module, id, etc.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, whether or not the delete was successful, as it's used in the API.
		 */
		function delete($api,$c = NULL) {
			// Item
			$item = $this->item($api->call[id]);
			
			// Doesn't exist
			if(!$item->db('id')) {
				$error = "We couldn't find this ".strtolower($this->v('single')).".";
			}
			// No permission
			else if(!$item->permission('delete') and $api->c[permission]) {
				$error = "You don't have permission to do this.";
			}
			
			// Error
			if($error) {
				$results = array(
					'status' => 0,
					'message' => $error
				);
			}
			// Delete
			else {
				$item->delete($c);
				$results = array(
					'status' => 1,
					'message' => "This ".strtolower($this->v('single'))." has been successfully deleted."
				);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Handles 'disable' calls for this module. Disables the given item as long as user has permission to.
		 *
		 * @param object $api The api object this was called from, including the $api->call array of module, id, etc.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, including the updated item array, as it's used in the API.
		 */
		function disable($api,$c = NULL) {
			// Item
			$item = $this->item($api->call[id]);
			
			// No 'active' column
			if(!$this->v('db.active')) {
				$error = "Disabling isn't allowed for ".strtolower($this->v('plural')).".";
			}
			// Doesn't exist
			else if(!$item->db('id')) {
				$error = "We couldn't find this ".strtolower($this->v('single')).".";
			}
			// No permission
			else if(!$item->permission('disable') and $api->c[permission]) {
				$error = "You don't have permission to do this.";
			}
			// Already disabled
			else if(x($item->db('active')) and $item->db('active') == 0) {
				$error = "This ".strtolower($this->v('single'))." is already disabled.";
			}
			
			// Error
			if($error) {
				$results = array(
					'status' => 0,
					'message' => $error
				);
			}
			// Disable
			else {
				$item->save(array('db.active' => 0),$c);
				$results = array(
					'status' => 1,
					'message' => "This ".strtolower($this->v('single'))." has been successfully disabled.",
					'item' => $item->item($api)
				);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Handles 'enable' calls for this module. Enables the given item as long as user has permission to.
		 *
		 * @param object $api The api object this was called from, including the $api->call array of module, id, etc.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, including the updated item array, as it's used in the API.
		 */
		function enable($api,$c = NULL) {
			// Item
			$item = $this->item($api->call[id]);
			
			// No 'active' column
			if(!$this->v('db.active')) {
				$error = "Enabling isn't allowed for ".strtolower($this->v('plural')).".";
			}
			// Doesn't exist
			else if(!$item->db('id')) {
				$error = "We couldn't find this ".strtolower($this->v('single')).".";
			}
			// No permission
			else if(!$item->permission('disable') and $api->c[permission]) {
				$error = "You don't have permission to do this.";
			}
			// Already enabled
			else if($item->db('active') == 1) {
				$error = "This ".strtolower($this->v('single'))." is already enabled.";
			}
			
			// Error
			if($error) {
				$results = array(
					'status' => 0,
					'message' => $error
				);
			}
			// Enable
			else {
				$item->save(array('db.active' => 1),$c);
				$results = array(
					'status' => 1,
					'message' => "This ".strtolower($this->v('single'))." has been successfully enabled.",
					'item' => $item->item($api)
				);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Method for eaily creating an item_api instance within the module_api class. Note, preloads $this->module so you only have to pass the item $id.
		 * 
		 * @param int|array $id Either the id or the database array of the item you want to create an item_api instance of.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The item_api class instance for the given item.
		 */
		function item($id,$c = NULL) {
			// Error
			if(!$this->module) return;
			
			// Return
			return item_api::load($this->module,$id);
		}
		
		/**
		 * Returns the API results array of given items in this module.
		 *
		 * @param object $api The api object this was called from, including $api->call[user] and any other filters we defined in the call.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array, including all items, as it's used in the API.
		 */
		function items($api,$c = NULL) {
			// Config
			if($api->call[query][c]) {
				foreach($api->call[query][c] as $k => $v) {
					if(!x($c[query_c][$k])) $c[query_c][$k] = s($v);
				}
			}
			if(!x($c[query_c]['db.user']) and $api->call[user]) $c[query_c]['db.user'] = $api->call[user];
			//if(!x($c[query_c]['db.visible'])) $c[query_c]['db.visible'] = 1; // May want some check like this in future, but for now not needed
			$c[query_c][debug] = $c[debug];
			
			// Items
			$rows = $this->rows($c[query_c]);
			
			// Success
			if($this->db->n($rows)) {
				$results[status] = 1;
				while($row = $this->db->f($rows)) {
					$item = item_api::load($this->module,$row);
					$results[items][] = $item->item($api);
				}
			}
			// Error
			else {
				$results[status] = 0;
				$results[message] = "We couldn't find any ".strtolower($this->v('plural')).".";
			}
			
			// Return
			return $results;
		}
	}
}
?>