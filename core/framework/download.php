<?php
/************************************************************************************/
/*************************************** Dawn ***************************************/
/************************************************************************************/
/** Dawn - loads core functionality */
require_once "../core/includes/dawn.php";
 
/************************************************************************************/
/************************************* Backend **************************************/
/************************************************************************************/
if($_GET['file']) {
	// Core DB
	if($_GET['file'] == "db") {
		// Variables
		$queries_tables = NULL;
		$queries_rows = NULL;
		$path = SERVER."local/";
		$modules = array( // These modules are 'local' but included by default
			'home',
			'pages',
		);
		
		// Not wrteable
		if(!file::directory_writeable($path)) page_error("The /core/modules/ folder is not writeable. Please make sure it has 777 permissions.",1);
		// Download
		else {
			// Tables - modules - core
			foreach(m() as $module => $v) {
				if($v[db][table] and $v[core] and $v[install][db][table]) {
					if(in_array($module,array("users","modules","types")) or $v[install][db][rows]) $tables[$v[db][table]] = 1;
					else $tables[$v[db][table]] = 0;
				}
			}
			
			// Tables - modules - included
			foreach($modules as $module) {
				if(m($module.'.db.table')) $tables[m($module.'.db.table')] = 0;
			}
			
			// Tables - plugins
			foreach(plugin() as $plugin => $v) {
				if($v[db][table] and $v[core] and $v[install][db][table]) {
					$tables[$v[db][table]] = $v[install][db][rows];
				}
			}
			
			// Tables - other
			$tables[cache] = 0;
			$tables[crons] = 0;
			$tables[emails] = 0;
			$tables[items_index_stops] = 1;
			$tables[test] = 0;
			$tables[users_external] = 0;
			$tables[users_resets] = 0;
			
			foreach($tables as $table => $table_rows) {
				// Rows
				$max = 0;
				if($table_rows) {
					$rows = $core->db->q("SELECT * FROM `".$table."`".($table == "users" ? " WHERE user_id IN (1,2)" : ""));
					while($row = $core->db->f($rows)) {
						if($table == "modules" or $table == "types") {
							if(m($row[module_code].'.core') or in_array($row[module_code],$modules)) {
								$queries_rows[] = "INSERT INTO `".$table."`".$core->db->values($row).";";	
								if($table == "modules" and $row[module_id] > $max) $max = $row[module_id];
								if($table == "types" and $row[type_id] > $max) $max = $row[type_id];
							}
						}
						else {
							$queries_rows[] = "INSERT INTO `".$table."`".$core->db->values($row).";";	
						}
					}
				}
				
				// Create table
				$row = $core->db->f("SHOW CREATE TABLE `".$table."`",array('association' => MYSQL_BOTH));
				$query = preg_replace('/CREATE TABLE/','CREATE TABLE IF NOT EXISTS',$row[1]);	
				if(!$table_rows) $query = preg_replace('/AUTO_INCREMENT=([0-9]+) /','AUTO_INCREMENT='.($table == 'modules' || $table == 'types' ? ($max + 1) : 0).' ',$query);
				$queries_tables[] = $query.";";
			}
			//print_array($tables);
			//print_array($queries_tables);
			//print_array($queries_rows);
				
			// Content
			$content = implode("\r\n\r\n",$queries_tables);
			$content .= "\r\n".implode("\r\n",$queries_rows);
			
			// Delete old file
			file::delete($path."db.sql");
			
			// Save
			file_put_contents($path."db.sql",$content);
			
			// Download
			file::download($path."db.sql");
		}
	}
}

/************************************************************************************/
/************************************* Frontend *************************************/
/************************************************************************************/
// Header
if(debug_active() == false) exit;
print $page->header()."
<body>";

// Content
print "
<h1>Download</h1>
<ul>
	<li><a href='".DOMAIN."core/framework/download.php?file=db'>Core DB</a></li>
</ul><br /><br />";

// Footer
print "
</body>
".$page->footer();

/************************************************************************************/
/*************************************** Dusk ***************************************/
/************************************************************************************/
/** Dusk - unloads core functionality. */
require_once "../core/includes/dusk.php";
?>