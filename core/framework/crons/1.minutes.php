<?php
/** Dawn - loads core functionality */
require_once dirname(__FILE__)."/../../../core/core/includes/dawn.php";
// Header
print $page->header()."
<body>";

	
// Run
$time = basename(__FILE__);
//$core->db->q("INSERT INTO test SET test = '".a(time_format(time())." - ".$time." cron")."'");
cron_run($time);

// Footer
print "
</body>
".$page->footer();
/** Dusk - unloads core functionality. */
require_once dirname(__FILE__)."/../../../core/core/includes/dusk.php";
?>