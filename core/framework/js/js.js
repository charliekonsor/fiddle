/**
 * Refreshes jquery	in the framework														
 */
function js_refresh_framework() {
	
}

/**
 * Sends ajax request do delete given item if given text is confirmed.
 *
 * @param string text The text that will be displayed to the user for them to 'confirm'.
 * @param string selector The selector of the element you want to remove if the user confirms the delete.
 * @param string module The module of the item we want to delete.
 * @param int id The id of the item we want to delete.
 * @param array c An array of configuration values.
 */
function framework_delete(text,selector,module,id,c) {
	if(!c) c = {};
	
	// Confirm
	if(confirm(text)){
		// Hide
		if(x(selector)) {
			$(selector).fadeOut(500,function() {
				// Remove element
				$(selector).remove();
					
				// Rows
				rows();
			});
		}
		
		// AJAX
		$.ajax({
			type: 'POST',
			url: DOMAIN,
			data: 'ajax_action=delete&ajax_module='+module+'&module='+module+'&id='+id+(c.vars ? '&'+c.vars : ''),
			success: function(html){
				// Debug
				debug('framework_delete() reponse: '+html);
				
				// Redirect
				if(x(c.redirect)) location.assign(c.redirect);
			}
		});
	}
}

/**
 * Sends ajax request do delete given plugin item if given text is confirmed.
 *
 * @param string text The text that will be displayed to the user for them to 'confirm'.
 * @param string selector The selector of the element you want to remove if the user confirms the delete.
 * @param string module The module of the plugin item we want to delete.
 * @param int id The item id of the plugin item we want to delete.
 * @param string plugin The plugin of the plugin item we want to delete.
 * @param int plugin_id The plugin item id of the plugin item we want to delete.
 * @param array c An array of configuration values.
 */
function framework_plugin_delete(text,selector,module,id,plugin,plugin_id,c) {
	if(!c) c = {};
	
	// Confirm
	if(confirm(text)){
		// Hide
		if(x(selector)) {
			$(selector).fadeOut(500,function() {
				// Remove element
				$(selector).remove();
					
				// Rows
				rows();
			});
		}
		
		// AJAX
		$.ajax({
			type: 'POST',
			url: DOMAIN,
			data: 'ajax_action=plugin_delete&ajax_plugin='+plugin+'&module='+module+'&id='+id+'&plugin='+plugin+'&plugin_id='+plugin_id+(c.vars ? '&'+c.vars : ''),
			success: function(html){
				// Debug
				debug('framework_plugin_delete() reponse: '+html);
				
				// Redirect
				if(x(c.redirect)) location.assign(c.redirect);
			}
		});
	}
}

/**
 * Sends ajax request do disable/enable given item.
 *
 * You can also diable/enable a plugin by passin 'plugin' and 'plugin_id' in the c array.
 *
 * @param string selector The selector of the row you want to convert to disabled/enabled once it's enabled/disabled.
 * @param string module The module of the item we want to delete.
 * @param int id The id of the item we want to delete.
 * @param array c An array of configuration values.
 */
function framework_disable(selector,module,id,c) {
	// Defaults
	var d = {
		plugin: '', // If disabling a plugin item, the plugin of that plugin item
		plugin_id: '', // If disabling a plugin item, the plugin_id of that plugin item
		icon_selector: '' // jQuery selector of the icon (we'll gues if none passed)
	};
	
	// Settings
	if(!c) c = {};
	c = $.extend(d,c);
	
	// Row
	var $row = $(selector);
	
	// Icon
	var $icon = null;
	if(c.icon_selector) $icon = $(c.icon_selector);
	else {
		$icon = $('.i-enable',$row);
		if(!$icon) $icon = $('.i-disable',$row);
	}
	
	// Enable
	var opacity = Math.round($row.css('opacity') * 100) / 100;
	if($row.hasClass('core-fade') || opacity == 0.3) {
		$row.fadeTo("normal", 1).removeClass('core-fade');
		$icon
			.removeClass('i-enable')
			.addClass('i-disable')
			.mouseout(function() {
				$(this)
					.attr('title','Disable')
					.unbind('mouseout');
				tooltips();
			});
	}
	// Disable
	else {
		$row.fadeTo("normal", 0.3);
		$icon
			.removeClass('i-disable')
			.addClass('i-enable')
			.mouseout(function() {
				$(this)
					.attr('title','Enable')
					.unbind('mouseout');
				tooltips();
			});
	}
	
	// AJAX
	$.ajax({
		type: 'POST',
		url: DOMAIN,
		data: 'ajax_action=disable&ajax_module='+module+'&module='+module+'&id='+id+'&plugin='+c.plugin+'&plugin_id='+c.plugin_id+(c.vars ? '&'+c.vars : ''),
		success: function(html){
			// Debug
			debug('framework_disable() reponse: '+html);
		}
	});
}