<?php
/**
 * @package kraken
 */

if(!function_exists('cron_run')) {
	/**
	 * Determines if we should be running the cron or not and, if we should be running it, runs it.		
	 * 
	 * @param string $time The time period of the cron file(s) we want to run.
	 * @param boolean $manual Whether or not the cron was called manually.
	 */
	function cron_run($time,$manual = 0) {
		// Core
		$core = core::load();
		
		// Name
		$name = str_replace('.php','',$time);
		
		// Get cron
		$row = $core->db->f("SELECT * FROM crons WHERE cron_name = '".$name."'");
		
		// Time
		list($number,$length) = explode('.',$name);
		$time_unix = adodb_strtotime("-".$number." ".$length);
		// Time - manual - if last cron ran wasn't manual and we're calling this 'manually' we add a bit of padding to the time in case non-manual cron is about to run.
		if($manual and $row[cron_manual] != 1 and $row[cron_name]) {
			if($length == "seconds") $time_unix -= ($number * .2);
			if($length == "minutes") $time_unix -= ($number * 60 * .2);
			if($length == "hours") $time_unix -= ($number * 3600 * .2);
			if($length == "days") $time_unix -= ($number * 86400 * .1);
			if($length == "weeks") $time_unix -= ($number * 86400 * 7 * .1);
			if($length == "months") $time_unix -= ($number * 86400 * 30 * .05);
			$time_unix = round($time_unix);
		}
		
		// Already ran/running
		if($row[cron_updated] >= $time_unix) {
			// Message
			if(!$manual) print "Already ran ".$name." cron at ".time_format($row[cron_updated])." (cutoff: ".time_format($time_unix).", current time: ".time_format(time()).", seconsds ".(time() - $time_unix).")<br />";
		}
		// Need to run
		else {
			// Values
			$values = array(
				'cron_name' => $name,
				'cron_manual' => $manual,
				'cron_output' => '',
				'cron_updated' => time()
			);
			$values_string = $core->db->values($values);
			
			// Save
			if($row[cron_name]) $query = "UPDATE crons ".$values_string." WHERE cron_name = '".$row[cron_name]."'";
			else $query = "INSERT INTO crons ".$values_string.", cron_created = '".time()."'";
			$core->db->q($query);
			$__name = $name; // In case we use the $name variable in one of the cron files
			
			// Array
			$array = NULL;
		 
			// Modules
			foreach(m() as $module => $v) {
				if(file_exists(SERVER.$v[path]."crons/".$name.".php")) {
					$priority = m($module.'.crons.'.$name);
					if(!$priority) $priority = 1;
					$array[$v[path]."crons/".$name.".php"] = $priority;
				}
				if($v[core]) {
					if(file_exists(SERVER.$v[path_local]."crons/".$name.".php")) {
						$priority = m($module.'.crons.'.$name);
						if(!$priority) $priority = 1;
						$array[$v[path_local]."crons/".$name.".php"] = $priority;
					}
				}
			}
		 
			// Plugins
			foreach(plugin() as $plugin => $v) {
				if(file_exists(SERVER.$v[path]."crons/".$name.".php")) {
					$priority = plugin($plugin.'.crons.'.$name);
					if(!$priority) $priority = 1;
					$array[$v[path]."crons/".$name.".php"] = $priority;
				}
				if($v[core]) {
					if(file_exists(SERVER.$v[path_local]."crons/".$name.".php")) {
						$priority = plugin($plugin.'.crons.'.$name);
						if(!$priority) $priority = 1;
						$array[$v[path_local]."crons/".$name.".php"] = $priority;
					}
				}
			}
			
			// Local
			if(file_exists(SERVER."local/crons/".$name.".php")) {
				$array["local/crons/".$name.".php"] = 1;
			}
			
			// Sort
			arsort($array);
			
			// Start output buffer
			ob_start();
			
			// Run
			foreach($array as $file => $priority) {
				print "Including ".$file." cron<br />";
				include_once SERVER.$file;
			}
			
			// Get captured contents
			$output = ob_get_contents();
			// End capture
			ob_end_clean();
			
			// Save
			$core->db->q("UPDATE crons SET cron_output = '".a($output,1)."' WHERE cron_name = '".$__name."'");
			
			// Print
			if(!$manual) print $output;
			
			// Expired - should really set up a cron for this, but important enough to do this way (and need for CS as can't add crons just now) // Already in 1.hours.php file so only need to do if 'manual'
			if($manual and $name == "1.hours") cache_delete_expired();
		}
	}
}
?>