<?php
/**
 * @package kraken
 */

/************************************************************************************/
/************************************ Modules ***************************************/
/************************************************************************************/
if(!function_exists('modules')) {
	/**
	 * Builds, stores, and returns the modules array containing each module and it's configuration values.
	 *
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The array of modules and their configuration values.
	 */
	function modules($c = NULL) {
		// Global config
		$config_global = array(
			//'name' => '', // Always module specific
			//'single' => '', // Always module specific
			//'plural' => '', // Always module specific
			'icon' => 'page',
			//'children' => array(), // Gets created by this function.
			//'db' => array(), // Always module specific. See section above for available definitions.
			'permissions' => array(
				'add' => array(
					'label' => 'Add',
					'manage' => 1,
					'default' => array(
						'admins'
					)
				),
				'view' => array(
					'label' => 'View',
					'default' => array(
						'admins',
						'members',
						'guests'
					)
				),
				'edit' => array(
					'label' => 'Edit',
					'manage' => 1,
					'default' => array(
						'admins'
					)
				),
				'delete' => array(
					'label' => 'Delete',
					'manage' => 1,
					'default' => array(
						'admins'
					)
				),
				/*'copy' => array(
					'label' => 'Copy',
					'manage' => 1,
					'defaults' => array(
						'admins'
					)
				),*/
				'settings' => array(
					'label' => 'Settings',
					'manage' => 1,
					'defaults' => array(
						'admins'
					)
				),
			),
			'admin' => array(
				'active' => 1,
				'header' => 1,
				'sidebar' => 1,
				'section' => 'content',
				'types' => 1,
				'views' => array( // Each view array must exist in the local config.php file. If it does, here are the defaults for each array
					'home' => array(
						'label' => '',
						'urls' => array(
							array(
								'format' => 'admin/{module}', // Note: defaults to admin/{parent_module}/{parent_id}/{module} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => '{page.module.name} > {page.type.name} | Admin | {modules.settings.settings.site.name}'
							)
						),
					),
					'type' => array(
						'item' => 1,
						'item_module' => 'types',
						'urls' => array(
							array(
								'format' => 'admin/{module}/{code}', // Note: defaults to admin/{parent_module}/{parent_id}/{module}/{code} for child modules
							),
						),
					),
					'item' => array(
						'item' => 1,
						'label' => '',
						'urls' => array(
							array(
								'format' => 'admin/{module}/{id}', // Defaults to admin/{parent_module}/{parent_id}/{module}/{id} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => '{page.module.name} > {page.item.name} | Admin | {modules.settings.settings.site.name}'
							)
						),
					),
				),
				'filters' => array(
					'search' => 1
				),
				'arrange' => array(
					'items' => 1
				),
			),
			'account' => array(
				'active' => 0,
				'views' => array( // Each view array must exist in the local config.php file. If it does, here are the defaults for each array
					'home' => array(
						'label' => '',
						'urls' => array(
							array(
								'format' => 'account/{module}', // Note: defaults to account/{parent_module}/{parent_id}/{module} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => 'My {page.module.plural} > {page.type.name} | {modules.settings.settings.site.name}'
							)
						),
					),
					'type' => array(
						'item' => 1,
						'item_module' => 'types',
						'urls' => array(
							array(
								'format' => 'account/{module}/{code}', // Note: defaults to account/{parent_module}/{parent_id}/{module}/{code} for child modules
							),
						),
					),
					'item' => array(
						'item' => 1,
						'label' => '',
						'urls' => array(
							array(
								'format' => 'account/{module}/{id}', // Defaults to account/{parent_module}/{parent_id}/{module}/{id} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => 'My {page.module.plural} > {page.item.name} | {modules.settings.settings.site.name}'
							)
						),
					),
				),
				'filters' => array(
					'search' => 1
				),
				'arrange' => array(
					'items' => 1
				),
			),
			'public' => array(
				'active' => 1,
				'views' => array( // Each view array must exist in the local config.php file. If it does, here are the defaults for each array
					'home' => array(
						'label' => '',
						'urls' => array(
							array(
								'format' => '{module}', // Note: defaults to {parent_module}/{parent_id}/{module} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => '{page.module.name} | {modules.settings.settings.site.meta.title}'
							)
						),
					),
					'type' => array(
						'item' => 1,
						'item_module' => 'types',
						'urls' => array(
							array(
								'format' => '{module}/{code}', // Note: defaults to {parent_module}/{parent_id}/{module}/{code} for child modules
							),
						),
					),
					'category' => array(
						'item' => 1,
						'item_module' => 'categories_items',
						'urls' => array(
							array(
								'format' => '{module}/category/{id}', // Note: defaults to {parent_module}/{parent_id}/{module}/category/{id} for child modules
							),
						),
					),
					'item' => array(
						'item' => 1,
						'label' => '',
						'urls' => array(
							array(
								'format' => '{module}/{id}/{name}', // Defaults to {parent_module}/{parent_id}/{module}/{id}/{name} for child modules
							),
						),
						'meta' => array(
							'title' => array(
								'format' => '{page.item.meta.meta_title} | {page.module.name} | {modules.settings.settings.site.meta.title}'
							)
						),
					),
				),
				'filters' => array(
					'search' => 1
				),
				'arrange' => array(
					'items' => 1
				),
			),
			'uploads' => g('config.uploads'),
			'plugins' => array(
				'files' => 1,
				//'categories' => 0, // Turned off by default, but want here so it's displayed on top in the sidebar
				'urls' => 1,
				'search' => 1, // before meta so we can use search index for automatically generated meta keywords
				'meta' => 1,
				'theme' => 1,
				'menu' => 1,
				'breadcrumbs' => 1,
				'code' => 1,
				'changes' => 1,
			),
			'settings' => array(
				'inputs' => array(
					# modules specific settings would go here
				),
				// Plugin setting defaults
				# these are placed in each plugin's config 'settings' array as defaults (not inputs)
				/*'sitemap' => array(
					'views' => array(
						'home'
					)
				),
				'search' => array(
					'public' => 0,
					'index' => 0,
					'columns' => array(
						'db.name' => 10,
						'db.code' => 9,
						'db.id' => 8,
						'db.description' => 7,
						'db.meta_title' => 8,
						'db.meta_description' => 7,
						'db.meta_keywords' => 6
					)
				),*/
			),
			//'forms' => array(), // Always module specific.
			'crons' => array( // You can use the 'crons' array to define the 'priority' (meaning order in which crons are run) of a specific cron in this module. 1 (lowest priority) is the default. In our example, we set the 1.hours cron for this module to 2 so it'll be run before all priority 1 crons.
				//'1.hours' => 2,
			),
			'install' => array(
				'db' => array(
					'table' => 1,
					'rows' => 0,
				),
			),
		);
	
		// Areas
		$areas = array('admin','account','public');
		
		// Plugin settings - use for defaults values within a module
		$plugins_settings = NULL;
		foreach(plugin() as $plugin => $v) {
			if($v[settings]) {
				foreach($v[settings] as $k => $v) {
					// Inputs - get any default values
					if($k == "inputs") {
						foreach($v as $input) {
							if($input['default'] and $input[name]) {
								$name = rtrim(array_string($input[name]),".");
								$value = array_eval($plugins_settings,$name);
								if(!x($value)) $plugins_settings = array_eval($plugins_settings,$name,$input['default']);
							}
						}
					}
					// Default settings
					else {
						$plugins_settings[$plugin][$k] = $v;	
					}
				}
			}
		}
		
		// Module config
		$db = new db();
		$rows = modules_rows();
		foreach($rows as $row) {
			// Code
			$code = s($row[module_code]);
			// Parent
			$parent = s($row[parent_module_code]);
			$parent_path = ($parent ? $modules[$parent][path_themes] : "");
			// Path
			$path = $parent_path."modules/".$code."/";
			
			// Exists / core?
			$exists = 0;
			if(file_exists(SERVER."core/".$path."config.php")) {
				$exists = 1;
				$core = 1;
			}
			else if(file_exists(SERVER."local/".$path."config.php")) {
				$exists = 1;
				$core = 0;
			}
			if($exists) {
				// Folder
				$folder = ($core ? "core" : "local");
				
				// Paths
				$path_local = "local/".$path;
				$path_themes = $path;
				$path = $folder."/".$path;
					
				// Config
				$config = NULL;
				// Config - module
				include SERVER.$path."config.php";
				// Config - global
				$_config_global = $config_global;
				// Must by defined in module config now
				/*$_config_global[admin][views][home][label] = 'All '.$config[plural];
				$_config_global[admin][views][item][label] = 'Edit '.$config[single];
				$_config_global[account][views][home][label] = 'My '.$config[plural];
				$_config_global[account][views][item][label] = 'Edit '.$config[single];
				$_config_global['public'][views][home][label] = $config[name].' Home Page';
				$_config_global['public'][views][item][label] = $config[single];*/
				// Config - global - defaults - child
				if($parent) {
					$_config_global[admin][views][home][urls][0][format] = "admin/".$parent."/{parent_id}/{module}";
					$_config_global[admin][views][type][urls][0][format] = "admin/".$parent."/{parent_id}/{module}/{code}";
					$_config_global[admin][views][item][urls][0][format] = "admin/".$parent."/{parent_id}/{module}/{id}";
					$_config_global[account][views][home][urls][0][format] = "account/".$parent."/{parent_id}/{module}";
					$_config_global[account][views][type][urls][0][format] = "account/".$parent."/{parent_id}/{module}/{code}";
					$_config_global[account][views][item][urls][0][format] = "account/".$parent."/{parent_id}/{module}/{id}";
					$_config_global['public'][views][home][urls][0][format] = $parent."/{parent_id}/{module}";
					$_config_global['public'][views][type][urls][0][format] = $parent."/{parent_id}/{module}/{code}";
					$_config_global['public'][views][category][urls][0][format] = $parent."/{parent_id}/{module}/category/{id}";
					$_config_global['public'][views][item][urls][0][format] = $parent."/{parent_id}/{module}/{id}/{name}";
				}
				
				// Config - module - upgrade
				$config = modules_config_upgrade($config,$code);
				
				// Config - module (local)
				if($core and file_exists(SERVER.$path_local."config.php")) {
					$config_core = $config;
					$config = NULL;
					include SERVER.$path_local."config.php";
					if($config) {
						// Config - module - upgrade
						$config = modules_config_upgrade($config,$code);
						// Config - merge
						$config = modules_config_merge($config_core,$config,$code,array('local' => 1));
					}
					else $config = $config_core;
				}
				
				// Config - merge
				$modules[$code] = modules_config_merge($_config_global,$config,$code);
				
				// Config - variables
				$modules[$code][id] = $row[module_id];
				$modules[$code][code] = $code;
				$modules[$code][core] = $core;
				$modules[$code]['parent'] = $parent;
				$modules[$code][path] = $path;
				$modules[$code][path_local] = $path_local;
				$modules[$code][path_themes] = $path_themes;
				if($parent) $children[$parent][] = $code;
				// Config - areas
				foreach($areas as $area) {
					// This area turned off, remove all varaibles so we're not constantly checking both the variable and m('{module}.{area}.active');
					if(!$modules[$code][$area][active]) unset($modules[$code][$area]);
				}
				
				// Plugins - order - want to use order they're defined in in the config.php file (also include 'children')
				$plugins = NULL;
				// Plugins - order - config
				foreach($config[plugins] as $plugin => $active) {
					$plugins[$plugin] = $active;
					// Children
					if($active and $plugin_children = plugin($plugin.'.children')) {
						foreach($plugin_children as $plugin_child) {
							$plugins[$plugin_child] = $active;
						}
					}
				}
				// Plugins - order - global
				foreach($config_global[plugins] as $plugin => $active) {
					if(!x($plugins[$plugin])) {
						$plugins[$plugin] = $active;
						// Children
						if($active and $plugin_children = plugin($plugin.'.children')) {
							foreach($plugin_children as $plugin_child) {
								$plugins[$plugin_child] = $active;
							}
						}
					}
				}
				// Plugins - required
				$plugins[urls] = 1;
				$plugins[meta] = 1;
				$plugins[theme] = 1;
				$plugins[menu] = 1;
				$plugins[files] = 1;
				$plugins[search] = 1;
				// Plugins - required - want these to be last plugins in the sidebar
				$changes = $plugins[changes]; // This one's optional (aka, we can turn it off in the module)
				unset($plugins[code],$plugins[changes]);
				$plugins[code] = 1; 
				$plugins[changes] = $changes; // NEEDS TO BE last item in sidebar so it's the last one processed
				// Plugins - restore
				$modules[$code][plugins] = $plugins;
				
				// Settings - default
				if(!$modules[$code][settings]) $modules[$code][settings] = array();
				// Settings - saved
				$settings = data_unserialize($row[module_settings]);
				// Settings - plugins
				$settings_plugins = NULL;
				if($modules[$code][plugins]) {
					foreach($modules[$code][plugins] as $plugin => $active) {
						if($active == 1 and $plugins_settings[$plugin]) {
							$settings_plugins[$plugin] = $plugins_settings[$plugin];
						}
					}
				}
				
				// Settings - default + plugins
				if($settings_plugins) $modules[$code][settings] = array_merge_associative($settings_plugins,$modules[$code][settings]);
				// Settings - default + saved
				if($settings) $modules[$code][settings] = array_merge_associative($modules[$code][settings],$settings);
				
				// Settings - overwrite default with saved (if there's an input and saved value for a setting, we don't use the default)
				$settings_core = $modules[$code][settings];
				if($settings_core[inputs]) {
					foreach($settings_core[inputs] as $input) {
						if($input[name]) {
							$name = array_string($input[name]);
							$value = array_eval($settings,$name);
							if(x($value)) array_eval($settings_core,$name,$value);
						}
					}
				}
				// Settings - overwrite default with saved (plugins)
				if($modules[$code][plugins]) {
					foreach($modules[$code][plugins] as $plugin => $active) {
						if($active == 1 and plugin($plugin.'.settings.inputs')) {
							foreach(plugin($plugin.'.settings.inputs') as $input) {
								if($input[name]) {
									$name = rtrim(array_string($input[name]),".");
									$value = array_eval($settings,$name);
									if(x($value)) $settings_core = array_eval($settings_core,$name,$value);
								}	
							}
						}
					}
				}
				$modules[$code][settings] = $settings_core;
				
				// Name - Right now going with name (and international values) must be defined in config.php file. Could switch to doing name/single/plural in settings by commenting this out, uncommenting the 3 lines below it, and uncommeting respective fields in /core/modules/settings/views/admin.home.php.  Woulud also have to set up for types too though.
				if(s($row[module_name]) != $modules[$code][name]) {
					$db->q("UPDATE modules SET module_name = '".a($modules[$code][name])."' WHERE module_code = '".a($code)."'");
				}
				/*$modules[$code][name] = $row[module_name];
				if($modules[$code][settings][single]) $modules[$code][single] = $modules[$code][settings][single];
				if($modules[$code][settings][plural]) $modules[$code][plural] = $modules[$code][settings][plural];*/
				
				// International
				if(file_exists(SERVER."local/plugins/international/config.php")) {
					$international_rows = $db->q("SELECT * FROM items_international WHERE module_code = 'modules' AND item_id = '".$row[module_id]."'");
					while($international_row = $db->f($international_rows)) {
						// Currently this gets defined in config.php so won't get saved in db at all
						/*if($international_row[item_column] == "module_name") $modules[$code][international][$international_row[language_code]][name] = $international_row[international_value];	
						else if($international_row[item_column] == "module_settings[single]") $modules[$code][international][$international_row[language_code]][single] = $international_row[international_value];	
						else if($international_row[item_column] == "module_settings[plural]") $modules[$code][international][$international_row[language_code]][plural] = $international_row[international_value];
						else*/ if(strstr($international_row[item_column],"module_settings[")) {
							$name = preg_replace('/module_settings\[(.*?)\]/','$1',$international_row[item_column]);	
							$modules[$code][international][$international_row[language_code]][settings] = array_eval($modules[$code][international][$international_row[language_code]][settings],$name,$international_row[international_value]);
						}
					}
				}
				
				// Types
				if($modules[$code][db][type]) {
					if($code == "users") {
						$modules[$code][types][supers] = array(
							'name' => 'Super Administrators',
							'single' => 'Super Administrator',
							'plural' => 'Super Administrators',
							'admin' => array(
								'active' => 0
							),
							'settings' => array(
								'registered' => 1,
								'admin' => 1
							),
							'id' => 1,
							'code' => 'supers',
							'core' => 1
						);
					}
					$type_rows = $db->q("SELECT * FROM types WHERE module_code = '".a($code)."' AND type_visible = 1 ORDER BY type_order ASC");
					while($type_row = $db->f($type_rows)) {
						// Code
						$type_code = s($type_row[type_code]);
						// Paths
						$type_path = $path."types/".$type_code."/";
						$type_path_local = $path_local."types/".$type_code."/";
						$type_path_themes = $path_themes."types/".$type_code."/";
						// Core
						$type_core = $core;
						
						// Exists?
						$exists = 0;
						if(file_exists(SERVER.$type_path."config.php")) {
							// Config
							$config = NULL;
							include SERVER.$type_path."config.php";
							$config = modules_config_upgrade($config,$code);
							$modules[$code][types][$type_code] = $config;
							
							// Exists
							$exists = 1;
						}
						// Exists? - local (if it was core)
						if($type_core == 1 and file_exists(SERVER.$type_path_local."config.php")) {
							// Only local
							if(!$exists) {
								// Path
								$type_path = $type_path_local;
								// Core
								$type_core = 0;
							}
							
							// Config
							if(!$modules[$code][types][$type_code]) $modules[$code][types][$type_code] = array();
							$config = NULL;
							include SERVER.$type_path_local."config.php";
							$config = modules_config_upgrade($config,$code);
							$modules[$code][types][$type_code] = array_merge_associative($modules[$code][types][$type_code],$config);
							
							// Exists
							$exists = 1;
						}
						
						// Exists
						if($exists) {
							// Variables
							$modules[$code][types][$type_code][id] = $type_row[type_id];
							$modules[$code][types][$type_code][code] = $type_code;
							$modules[$code][types][$type_code][core] = $type_core;
							$modules[$code][types][$type_code][path] = $type_path;
							$modules[$code][types][$type_code][path_local] = $type_path_local;
							$modules[$code][types][$type_code][path_themes] = $type_path_themes;
							
							// Settings
							if(!$modules[$code][types][$type_code][settings]) $modules[$code][types][$type_code][settings] = array();
							$settings = data_unserialize($type_row[type_settings]);
							if(!$settings) $settings = array();
							$modules[$code][types][$type_code][settings] = array_merge_associative($modules[$code][types][$type_code][settings],$settings);
				
							// Tweaks
							if(x($modules[$code][types][$type_code][plugins][files])) $modules[$code][types][$type_code][plugins][files] = 1; // Required
							if(x($modules[$code][types][$type_code][plugins][urls])) $modules[$code][types][$type_code][plugins][urls] = 1; // Required
							if(x($modules[$code][types][$type_code][plugins][search])) $modules[$code][types][$type_code][plugins][search] = 1; // Required
							if(x($modules[$code][types][$type_code][plugins][meta])) $modules[$code][types][$type_code][plugins][meta] = 1; // Required
							if(x($modules[$code][types][$type_code][plugins][theme])) $modules[$code][types][$type_code][plugins][theme] = 1; // Required
							if(x($modules[$code][types][$type_code][plugins][menu])) $modules[$code][types][$type_code][plugins][menu] = 1; // Required
							// Want these to be last plugins in the sidebar
							if(x($modules[$code][types][$type_code][plugins][code])) {
								unset($modules[$code][types][$type_code][plugins][code]);
								$modules[$code][types][$type_code][plugins][code] = 1;
							}
							if(x($modules[$code][types][$type_code][plugins][changes])) {
								unset($modules[$code][types][$type_code][plugins][changes]);
								$modules[$code][types][$type_code][plugins][changes] = 1; // NEEDS TO BE last item in the sidebar so it's the last one processed
							}
							
							// Name
							if(s($type_row[type_name]) != $modules[$code][types][$type_code][name]) {
								$db->q("UPDATE types SET type_name = '".a($modules[$code][types][$type_code][name])."' WHERE type_code = '".a($type_code)."'");
							}
							/*$modules[$code][types][$type_code][name] = $row[module_name];
							if($modules[$code][types][$type_code][settings][single]) $modules[$code][types][$type_code][single] = $modules[$code][types][$type_code][settings][single];
							if($modules[$code][types][$type_code][settings][plural]) $modules[$code][types][$type_code][plural] = $modules[$code][types][$type_code][settings][plural];*/
							
							// International
							if(file_exists(SERVER."local/plugins/international/config.php")) {
								$international_rows = $db->q("SELECT * FROM items_international WHERE module_code = 'modules' AND item_id = '".$row[module_id]."'");
								while($international_row = $db->f($international_rows)) {
									/*if($international_row[item_column] == "type_name") $modules[$code][types][$type_code][international][$international_row[language_code]][name] = $international_row[international_value];	
									else if($international_row[item_column] == "module_settings[single]") $modules[$code][types][$type_code][international][$international_row[language_code]][single] = $international_row[international_value];	
									else if($international_row[item_column] == "module_settings[plural]") $modules[$code][types][$type_code][international][$international_row[language_code]][plural] = $international_row[international_value];
									else*/ if(strstr($international_row[item_column],"module_settings[")) {
										$name = preg_replace('/module_settings\[(.*?)\]/','$1',$international_row[item_column]);	
										$modules[$code][types][$type_code][international][$international_row[language_code]][settings] = array_eval($modules[$code][types][$type_code][international][$international_row[language_code]][settings],$name,$international_row[international_value]);
									}
								}
							}
						}
					}
					// Guest - in case they removed or altered it
					if($code == "users" and !$modules[$code][types][guests]) {
						$modules[$code][types][guests] = array(
							'name' => 'Guests',
							'single' => 'Guest',
							'plural' => 'Guests',
							'admin' => array(
								'active' => 1,
								'sidebar' => 1
							),
							'settings' => array(
								'registered' => 0,
								'admin' => 0
							),
							'id' => NULL,
							'code' => 'guests',
							'core' => 1
						);
					}
				}
			}
		}
		// Children - do at the end in case we added child before parent existed in array // Moot now since we order modules by hierarchy (parents first), but need to move this wherever it's needed
		if($children) {
			foreach($children as $module => $child_modules) {
				$modules[$module][children] = $child_modules;	
			}
		}
		
		// Cache
		cache_save('framework/modules',$modules);
			
		// Global
		$__GLOBAL['modules'] = $modules;
	
		// Return
		return $modules;
	}
}

if(!function_exists('modules_config_merge')) {
	/**
	 * Merges two config arrays, giving precedence to the second one, and taking into account what keys need to be 'overwritten'.
	 *
	 * @param array $config1 The first array you want to merge.
	 * @param array $config2 The second array you want to merge.
	 * @param string $module The module this is a part of.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The merged array.
	 */
	function modules_config_merge($config1,$config2,$module,$c = NULL) {
		// Config
		if(!x($c[local])) $c[local] = 0; // Ar we merging the core with the local config?
	
		// Areas
		$areas = array('admin','account','public');
		
		// Merge
		$config = array_merge_associative($config1,$config2);

		// Views - make sure a view is actually defined in config file and, if it is and it's an 'item' view, make sure an item_module value is defined.
		if(!$c[local]) {
			foreach($areas as $area) {
				foreach($config[$area][views] as $k => $v) {
					if(!$config2[$area][views][$k]) unset($config[$area][views][$k]);
				}
			}
		}
			
		// Overwrite - values that are wholly the modules or wholly global...also, wholly the core module or wholly the local tweaking of the module
		$config_overwrite = array(
			'permissions',
			// Views are now merged, but not included unless a view is included in the local config.php file (see above)
			/*'admin' => array(
				'views'
			),
			'account' => array(
				'views'
			),
			'public' => array(
				'views'
			),*/
			'forms'
		);
		foreach($config_overwrite as $k => $v) {
			if(is_array($v)) {
				foreach($v as $k0 => $v0) {
					if(x($config[$k][$v0])) $config[$k][$v0] = $config[$k][$v0];
				}
			}
			else if(x($config2[$v])) $config[$v] = $config2[$v];
		}
		
		// Return
		return $config;
	}
}

if(!function_exists('modules_config_upgrade')) {
	/**
	 * Upgrades aspects of the config array from legacy format to the newest format.
	 *
	 * @param array $config The config array you want to upgrade.
	 * @param string $module The module this is a part of.
	 * @return array The upgraded config array.
	 */
	function modules_config_upgrade($config,$module) {
		// Areas
		$areas = array('admin','account','public');
		
		// View format
		foreach($areas as $area) {
			foreach($config[$area][views] as $k => $v) {
				// Old format, clear out (we'll put in new format momentarily)
				if(!is_array($v)) $config[$area][views][$k] = NULL;
			
				// Label
				if(!$config[$area][views][$k][label]) {
					if(!is_array($v)) $config[$area][views][$k][label] = $v;
					else {
						if($k == "home") $config[$area][views][$k][label] = "All ".$config[plural];
						else if($k == "item") $config[$area][views][$k][label] = ($area == "public" ? "" : "Edit ").$config[single];
						else $config[$area][views][$k][label] = $config[plural]." ".ucwords($k);
					}
				}
					
				// URL - want in views => urls => array => array( {format} )
				if(x($config[$area][urls][$k])) { // urls => {view} => array( {format} )
					$config[$area][views][$k][urls][] = $config[$area][urls][$k];
				}
				if($config[$area][views][$k][url]) { // views => {view} => url => array( {format} )
					$config[$area][views][$k][urls][] = $config[$area][views][$k][url];
					unset($config[$area][views][$k][url]);
				}
				if(!$config[$area][views][$k][urls] and !in_array($k,array('home','type','item')) and ($k != "category" or $area != "public")) { // views => {view} (no URL info, will default for home/type/item/category in modules() function)
					$config[$area][views][$k][urls][][format] = "{module}/".$k;
				}
				
				// Meta
				if($config[$area][meta][$k]) $config[$area][views][$k][meta] = $config[$area][meta][$k];
				
				// Item
				if($k == "item" or $config[$area][views][$k][item]) {
					if(!x($config[$area][views][$k][item])) $config[$area][views][$k][item] = 1;
					if($config[$area][views][$k][item] and !$config[$area][views][$k][item_module]) $config[$area][views][$k][item_module] = $module;
				}
			}
			unset($config[$area][urls],$config[$area][meta]);
		}	
		
		// Return
		return $config;
	}
}
	

if(!function_exists('m')) {
	/**
	 * Returns the value for the given module setting key.
	 *
	 * @param string $key The key you want to get the value for. Levels of the module's settings array are separated by periods (.) for example "users.db.name".
	 * @param string $type A type within the module in the key you want to get the value for. Example: m('users.name','members') = $__GLOBAL[modules][users][types][members][name]. Default = NULL
	 * @return mixed The setting value, usually a string, boolean, or int, but sometimes an array.
	 */
	function m($key,$type = NULL) {
		// Error
		if(substr($key,0,1) == ".") return;
		
		// Global
		global $__GLOBAL;
		
		// Key parts
		$keys = explode('.',$key);
		
		// International
		$language = NULL;
		if(
			$__GLOBAL['modules']['languages']
			and $__GLOBAL['plugins']['international']
			and $keys[0]
			and $keys[0] != "languages"
			and in_array($keys[1],array('name','single','plural','settings'))
			and strpos(URL,'/admin') === false
			and $_SESSION['__modules']['languages']['selected']
			and $_SESSION['__modules']['languages']['selected'] != $__GLOBAL['modules']['langauges']['settings']['languages']['default']
		) $language = $_SESSION['__modules']['languages']['selected'];
		
		// Type
		if($key and $type) {
			$keys_type[] = $keys[0]; // Module
			$keys_type[] = "types"; // Types
			$keys_type[] = $type; // Type
			unset($keys[0]); // Remove module (don't want to add it in again)
			foreach($keys as $v) $keys_type[] = $v; // Remaining keys
			
			// New key
			$key_type = implode('.',$keys_type);
			
			// Value - type, international
			if($language) {
				$key_type_language = str_replace('types.'.$type,'types.'.$type.'.international.'.$language,$key);
				$value = array_eval("\$__GLOBAL['modules']",$key_type_language);
				if(x($value)) return $value;
			}
				
			// Value - type
			$value = array_eval("\$__GLOBAL['modules']",$key_type);
			if(x($value)) return $value;
		}
		
		// Value - module, international
		if($language) {
			$key_language = str_replace($keys[0].'.',$keys[0].'.international.'.$language.'.',$key);
			$value = array_eval("\$__GLOBAL['modules']",$key_language);
			if(x($value)) return $value;
			
		}
		
		// Value - module (or all modules if no $key)
		return array_eval("\$__GLOBAL['modules']",$key);
	}
}

if(!function_exists('code_id')) {
	/**
	 * Returns the id for the item with the given code in the given module.
	 *
	 * @param string $module The module this code is associated with.
	 * @param string $code The code we want to get the corresponding id of.
	 * @return int The id for the item in the given module which has the given code.
	 */
	function code_id($module,$code) {
		// Error
		if(!$module or !$code) return;
		
		// Module
		$m = module::load($module);
		
		// Column
		$column = $m->v('db.code');
		if($column) {
			// Get item row
			$row = $m->row(array('where' => $column." = '".a($code)."'"));
			// Get id
			$id = $row[$m->v('db.id')];	
			
			// Return
			return $id;
		}
	}
}

if(!function_exists('modules_rows')) {
	/**
	 * Returns array of module rows while mainting hierarchy (parent inclued before chilren).
	 *
	 * @param string $parent The parent module you want to get child modules of.
	 * @return array An array of module database rows.
	 */
	function modules_rows($parent = NULL) {
		// Database
		$db = db::load();
		
		// Rows
		$rows = $db->q("SELECT * FROM modules WHERE module_visible = 1 AND parent_module_code = '".$parent."' ORDER BY module_name ASC");
		while($row = $db->f($rows)) {
			// Add
			$array[] = $row;
			
			// Children
			$array_children = modules_rows($row[module_code]);
			if($array_children) $array = array_merge($array,$array_children);
		}
		
		// Return
		return $array;
	}
}

/************************************************************************************/
/************************************ Plugins ***************************************/
/************************************************************************************/
if(!function_exists('plugins')) {
	/**
	 * Builds, stores, and returns the plugins array containing each plugin and it's configuration values.
	 *
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The array of plugins and their configuration values.
	 */
	function plugins($c = NULL) {
		// Core
		$plugins_core = file::directory_folders(SERVER."core/plugins/");
		if(!$plugins_core) $plugins_core = array();
		// Local
		$plugins_local = file::directory_folders(SERVER."local/plugins/");
		if(!$plugins_local) $plugins_local = array();
		
		// Combine
		$array = array_merge($plugins_core,$plugins_local);
		$array = array_unique($array);
		sort($array);
		
		// Config
		if($array) {
			foreach($array as $code) {
				$plugin = plugin_config($code);
				if($plugins) $plugins = array_merge($plugins,$plugin);
				else $plugins = $plugin;
			}
		}
		
		// Cache
		cache_save('framework/plugins',$plugins);
			
		// Global
		$__GLOBAL['plugins'] = $plugins;
		
		// Return
		return $plugins;
	}
}

if(!function_exists('plugin')) {
	/**
	 * Returns the value for the given plugin setting key.
	 *
	 * @param string $key The key you want to get the value for. Levels of the plugin's settings array are separated by periods (.) for example "users.db.name".
	 * @return mixed The setting value, usually a string, boolean, or int, but sometimes an array.
	 */
	function plugin($key) {
		// Value
		$value = array_eval("\$__GLOBAL['plugins']",$key);
		
		// Return
		return $value;
	}
}

if(!function_exists('plugin_config')) {
	/**
	 * Returns array of the config data for the given plugin.
	 *
	 * @param string $code The code of the plugin you want to get the config data of.
	 * @param array  $c An array of configuration values. Default = NULL
	 * @return array An arry of the plugin's config data.
	 */
	function plugin_config($code,$c = NULL) {
		// Config
		if(!$c['parent']) $c['parent'] = NULL; // The parent plugin this plugin is a part of.
		if(!$c[path]) $c[path] = NULL; // Needed to get the location of child plugins.
		
		// Areas
		$areas = array('admin','account','public');
		// Global config
		$config_global = array(
			//'name' => '', // Always plugin specific
			//'single' => '', // Always plugin specific
			//'plural' => '', // Always plugin specific
			'icon' => 'page',
			//'db' => array(), // Always plugin specific. See section above for available definitions.
			//'permissions' => array(), // Always plugin specific.
			'settings' => array(
				'inputs' => array(
				
				)
			),
			//'forms' => array(), // Always plugin specific.
			'install' => array(
				'db' => array(
					'table' => 1,
					'rows' => 0,
				),
			),
		);
		// Plugin setting defaults
		$plugins_settings = array();
				
		// Global config - overwrite (array of config values that are wholly the plugins or wholly global).
		$config_overwrite = array(
			# none yet
		);
		
		// Path
		$path = $c[path]."plugins/".$code."/";
		
		// Core?
		$core = 0;
		if(file_exists(SERVER."core/".$path."config.php")) $core = 1;
		
		// Paths
		$path_local = "local/".$path;
		$path_themes = $path;
		$path = ($core ? "core" : "local")."/".$path;
			
		// Config
		$config = NULL;
		if(file_exists(SERVER.$path."config.php")) {
			// Config - plugin
			include SERVER.$path."config.php";
			$array[$code] = array_merge_associative($config_global,$config);
			$array[$code][code] = $code;
			$array[$code][core] = $core;
			$array[$code]['parent'] = $c['parent'];
			$array[$code][path] = $path;
			$array[$code][path_local] = $path_local;
			$array[$code][path_themes] = $path_themes;
			// Config - overwrite
			foreach($config_overwrite as $k => $v) {
				if(is_array($v)) {
					foreach($v as $k0 => $v0) {
						if(x($config[$k][$v0])) $array[$code][$k][$v0] = $config[$k][$v0];
					}
				}
				else if(x($config[$v])) $array[$code][$v] = $config[$v];
			}
			
			// Exists? - local
			if($core and file_exists(SERVER.$path_local."config.php")) {
				// Config
				$config = NULL;
				// Config - plugin (local)
				include SERVER.$path_local."config.php";
				// Config - plugin (local) + plugin (and global)
				$array[$code] = array_merge_associative($array[$code],$config);
				// Config - overwrite
				foreach($config_overwrite as $k => $v) {
					if(is_array($v)) {
						foreach($v as $k0 => $v0) {
							if(x($config[$k][$v0])) $array[$code][$k][$v0] = $config[$k][$v0];
						}
					}
					else if(x($config[$v])) $array[$code][$v] = $config[$v];
				}
			}
			
			// Database variables
			if($array[$code][db_variables]) {
				$__GLOBAL['framework']['db'] = array_merge($__GLOBAL['framework']['db'],$array[$code][db_variables]);
			}
			
			// Plugins - required
			$array[$code][plugins][files] = 1;
			
			// Plugin settings
			if($array[$code][settings]) {
				foreach($array[$code][settings] as $k => $v) {
					// Inputs - get any default values
					if($k == "inputs") {
						foreach($array[$code][settings][inputs] as $input) {
							if($input['default'] and $input[name]) {
								$name = rtrim(array_string($input[name]),".");
								$value = array_eval($plugins_settings,$name);
								if(!x($value)) $plugins_settings = array_eval($plugins_settings,$name,$input['default']);
							}
						}
					}
					// Default settings
					else {
						$plugins_settings[$code][$k] = $v;	
					}
				}
			}
			
			// Children
			$children = file::directory_folders(SERVER.$path."plugins/");
			if($children) {
				foreach($children as $child_code) {
					$child_c = array(
						'parent' => $code,
						'path' => $path_themes
					);
					$child_array = plugin_config($child_code,$child_c);
					if($child_array) {
						$array = array_merge($array,$child_array);
						$array[$code][children][] = $child_code;
					}
				}
			}
		}
		
		// Return
		return $array;
	}
}

/************************************************************************************/
/************************************ Packages **************************************/
/************************************************************************************/
if(!function_exists('packages')) {
	/**
	 * Returns array of packages available for download.
	 *
	 * Example:
	 * array(
	 *		'geography' => array(
	 *			'name' => 'Geography',
	 *			'description' => 'Modules containing various geographic information.
	 *			'modules' => array(
	 *				'countries',
	 *				'states',
	 *			)
	 *		)
	 * )
	 *
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of packages available for download.
	 */
	function packages($c = NULL) {
		// Config
		if(!x($c[complete])) $c[complete] = 0; // Only get 'complete' packages (where all modules and plugins exist)
		
		// Array
		$array = NULL;
		
		// Core
		$packages = file::directory_folders(SERVER."core/packages/");
		if($packages) {
			foreach($packages as $package) {
				if(!$array[$package]) {
					$config = NULL;
					include SERVER."core/packages/".$package."/config.php";	
					if($config[name]) {
						
						$array[$package] = $config;
						$array[$package][code] = $package;
						$array[$package][path] = "core/packages/".$package."/";
					}
				}
			}
		}
		
		// Local
		$packages = file::directory_folders(SERVER."local/packages/");
		if($packages) {
			foreach($packages as $package) {
				if(!$array[$package]) {
					$config = NULL;
					include SERVER."local/packages/".$package."/config.php";	
					if($config[name]) {
						$array[$package] = $config;
						$array[$package][code] = $package;
						$array[$package][path] = "local/packages/".$package."/";
					}
				}
			}
		}
		
		// Complete?
		if($c[complete] and $array) {
			foreach($array as $k => $v) {
				$missing = 0;
				
				// Modules
				if($v[modules]) {
					foreach($v[modules] as $module) {
						if(!m($module)) {
							$missing = 1;
							break;
						}
					}
				}
				// Plugins
				if($v[plugins] and !$missing) {
					foreach($v[plugins] as $plugin) {
						if(!plugin($plugin)) {
							$missing = 1;
							break;
						}
					}
				}
				
				// Missing
				if($missing) {
					unset($array[$k]);	
				}
			}
		}
		
		// Return
		return $array;
	}
}

if(!function_exists('package')) {
	/**
	 * Returns array of config data for the givne package.
	 *
	 * @param string $package The package you want to get the config data array of.
	 * @return array An array of the package's config data.
	 */
	function package($package) {
		// Error
		if(!$package) return;
		
		// Packages
		$packages = packages();
		foreach($packages as $k => $v) {
			// Package
			if($k == $package) return $v;	
		}
	}
}

/************************************************************************************/
/************************************* Include **************************************/
/************************************************************************************/
if(!function_exists('load_class_module')) {
	/**
	 * Returns the name of the class we want to instanitiate.
	 *
	 * Uses $module and $type to determine if we should be using a module or type specific class.
	 *
	 * @param string $class The core class you want to check to see if there is a child plugin class of.
	 * @param array $params An array of parameters to pass to the class.
	 * @param string $module The module we're working within in this class.
	 * @param string $type The module 'type' we're working within in this class. Default = NULL
	 * @return string The name of the 
	 */
	function load_class_module($class,$params,$module,$type = NULL) {
		// Default
		if(class_exists($class)) $class_load = $class;
		
		// Module class
		$class_module = $class.'_'.str_replace('-','_',$module);
		$class_module_local = $class.'_'.str_replace('-','_',$module)."_local";
		if($type) {
			$class_module_type = $class.'_'.str_replace('-','_',$module).'_'.str_replace('-','_',$type);
			$class_module_type_local = $class.'_'.str_replace('-','_',$module).'_'.str_replace('-','_',$type)."_local";
		}
		
		// Already loaded?
		if($type and class_exists($class_module_type_local)) {
			$class_load = $class_module_type_local;
		}
		else if($type and class_exists($class_module_type)) {
			$class_load = $class_module_type;
		}
		else if(class_exists($class_module_local)) $class_load = $class_module_local;
		else if(class_exists($class_module)) $class_load = $class_module;
		
		// I think this will already get handled by __autoload() too 
		// Type - If we don't know the type, we must load all type classes in case we end up loading a cached object of one of these (ex: if we call process($_POST['__form']) where the $_POST['__form'] was a type specific form, but the form object we just initialized ($form = form::load('users');) wasn't a type form).
		/*if(!$type and $module) { 
			if($types = m($module.'.types')) {
				foreach($types as $t) {
					// Module type class - must make sure we're including this as any local module type classes will probably extend the core module type class. Example: item_users_members
					$file = SERVER.m($module.'.path',$t[code])."classes/".str_replace('_framework','',$class).".php";
					if(file_exists($file)) {
						include_once $file;	
					}
					// Local module type class (if it's a core module). Example: item_users_admins_local (also loads non-core types without _local, so if  the class item_users_customtype were located at /local/users/types/customtype/classes/item.php, it would load).
					if(m($module.'.core')) {
						$file = SERVER.m($module.'.path_local',$t[code])."classes/".str_replace('_framework','',$class).".php";
						if(file_exists($file)) {
							include_once $file;	
						}
					}
				}
			}
		}*/
		
		
		// This all gets handled by __autoload() which gets called when class_exists() is called above
		/*else if($module) {
			// Module class - must make sure we're including this as any local module classes will probably extend the core module class. Example: item_users
			$file = SERVER.m($module.'.path')."classes/".str_replace('_framework','',$class).".php";
			if(file_exists($file)) {
				include_once $file;	
			}
			// Local module class (if it's a core module). Example: item_users_local
			if(m($module.'.core')) {
				$file = SERVER.m($module.'.path_local')."classes/".str_replace('_framework','',$class).".php";
				if(file_exists($file)) {
					include_once $file;	
				}
			}
			
			// Type(s)
			if($type) $types = array(array('code' => $type));
			else $types = m($module.'.types'); // If we don't know the type, we must load all type classes in case we end up loading a cached object of one of these (ex: if we call process($_POST['__form']) where the $_POST['__form'] was a type specific form, but the form object we just initialized ($form = form::load('users');) wasn't a type form).
			if($types) {
				foreach($types as $t) {
					// Module type class - must make sure we're including this as any local module type classes will probably extend the core module type class. Example: item_users_members
					$file = SERVER.m($module.'.path',$t[code])."classes/".str_replace('_framework','',$class).".php";
					if(file_exists($file)) {
						include_once $file;	
					}
					// Local module type class (if it's a core module). Example: item_users_admins_local (also loads non-core types without _local, so if  the class item_users_customtype were located at /local/users/types/customtype/classes/item.php, it would load).
					if(m($module.'.core')) {
						$file = SERVER.m($module.'.path_local',$t[code])."classes/".str_replace('_framework','',$class).".php";
						if(file_exists($file)) {
							include_once $file;	
						}
					}
				}
			}
			
			// Load class
			if(class_exists($class_module)) $class_load = $class_module;
			if(class_exists($class_module_local)) $class_load = $class_module_local;
			if(class_exists($class_module_type)) $class_load = $class_module_type;
			if(class_exists($class_module_type_local)) $class_load = $class_module_type_local;
		}*/
		
		// Load  - can't instanitiate a class with array of params so must do that in our load()  methods.
		//if($class_load) return call_user_func_array(array($class_load,'__construct'),$params);
		
		// Return
		return $class_load;
	}
}

if(!function_exists('load_class_plugin')) {
	/**
	 * Loads instance of plugin class, using a plugin specific (or even plugin and module module specific) class if it exists.
	 *
	 * @param string $class The core class you want to check to see if there is a child plugin class of.
	 * @param array $params An array of parameters to pass to the class.
	 * @param string $module The module we're working within in this class.
	 * @param int $id The item id we're working within in this class.
	 * @param string $plugin The plugin we're working within in this class.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function load_class_plugin($class,$params,$module,$id,$plugin,$c = NULL) {
		// Default
		if(class_exists($class)) $class_load = $class;
			
		// Type
		if(($id > 0 or is_array($id)) and $module) {
			if(m($module.'.types') and m($module.'.db.type')) {
				// Passed item object, get type from there
				if($c[item]) {
					$type = $c[item]->type;	
				}
				// Passed array, get type from there
				else if(is_array($id)) {
					$type = $id[m(module.'.db.type')];
				}
				// Get type from item class
				else {
					$item = item::load($module,$id);
					$type = $item->type;
				}
			}
		}
		
		// Plugin class. Exampe: plugin_comments
		$class_plugin = $class.'_'.str_replace('-','_',$plugin);
		$class_plugin_local = $class.'_'.str_replace('-','_',$plugin)."_local";
		// Plugin module class. Example: plugin_comments_photos
		$class_plugin_module = $class.'_'.str_replace('-','_',$plugin).'_'.str_replace('-','_',$module);
		$class_plugin_module_local = $class.'_'.str_replace('-','_',$plugin).'_'.str_replace('-','_',$module)."_local";
		// Plugin module type class. Example: plugin_comments_users_members
		$class_plugin_module_type = $class.'_'.str_replace('-','_',$plugin).'_'.str_replace('-','_',$module).'_'.str_replace('-','_',$type);
		$class_plugin_module_type_local = $class.'_'.str_replace('-','_',$plugin).'_'.str_replace('-','_',$module).'_'.str_replace('-','_',$type)."_local";
		
		// Already loaded?
		if(class_exists($class_plugin_module_type_local)) $class_load = $class_plugin_module_type_local;
		else if(class_exists($class_plugin_module_type)) $class_load = $class_plugin_module_type;
		else if(class_exists($class_plugin_module_local)) $class_load = $class_plugin_module_local;
		else if(class_exists($class_plugin_module)) $class_load = $class_plugin_module;
		else if(class_exists($class_plugin_local)) $class_load = $class_plugin_local;
		else if(class_exists($class_plugin)) $class_load = $class_plugin;
		
		// This all gets handled by __autoload() which gets called when class_exists() is called above
		/*else {
			// Plugin class - must make sure we've already include the plugin class (as the plugin module class will try to extend it)
			if(!class_exists($class_plugin)) {
				// Plugin class. Example: plugin_comments
				$file = SERVER.plugin($plugin.'.path')."classes/".str_replace('_framework','',$class).".php";
				if(file_exists($file)) {
					include_once $file;
				}
				
				// Local plugin class (if it's a core plugin). Example: plugin_comments_local
				if(plugin($plugin.'.core')) {
					$file = SERVER.plugin($plugin.'.path_local')."classes/".str_replace('_framework','',$class).".php";
					if(file_exists($file)) {
						include_once $file;	
					}
				}
			}
			
			// Load class
			if(class_exists($class_plugin)) $class_load = $class_plugin;
			if(class_exists($class_plugin_local)) $class_load = $class_plugin_local;
					
			// Plugin module class - now we know we have a parent plugin class for a module specific plugin class to extend, we can look for that module specific plugin class
			if($class_load) {
				// Plugin module class. Example: plugin_comments_photos
				$file = SERVER.m($module.'.path')."plugins/".$plugin."/classes/".str_replace('_framework','',$class).".php";
				if(file_exists($file)) {
					include_once $file;
				}
				
				// Local plugin module class (if it's a core plugin). Example: plugin_comments_photos_local // Don't think I'd ever have both a module specific, and then local versoin of a modules specific plugin
				/*if(plugin($plugin.'.core')) {
					$file = SERVER.m($module.'.path_local')."plugins/".$plugin."/classes/".str_replace('_framework','',$class).".php";
					if(file_exists($file)) {
						include_once $file;
					}
				}*/
				
				/*// Load class
				if(class_exists($class_plugin_module)) $class_load = $class_plugin_module;
				if(class_exists($class_plugin_module_local)) $class_load = $class_plugin_module_local;
			}
		}*/
		
		// Load - can't instanitiate a class with array of params so must do that manually in our load()  methods.
		//if($class_load) return call_user_func_array(array($class_load,'__construct'),$params);
		
		// Return
		return $class_load;
	}
}

/************************************************************************************/
/*********************************** Permission *************************************/
/************************************************************************************/
if(!function_exists('permission')) {
	/**
	 * Determines whether or not a user has permission to perform the given action.
	 *
	 * @param string $module The module we're performing the action within.
	 * @param string $action The action we want to perform.
	 * @param int $id The id of the item we're performing this action upon. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return boolean Whether or not the user has permission to perform this action.
	 */
	function permission($module,$action,$id = NULL,$c = NULL) {
		// No module or action
		if(!$module or !$action) return false;
		
		// Manage
		if($action == "manage") {
			// Super admin, always true
			if(u('type') == "supers") return true;
			// Loop through module permission
			if($permissions = m($module.'.permissions')) {
				foreach($permissions as $k => $v) {
					// Is a 'manage' permission
					if($v[manage]) {
						$permission = u('permissions.'.$module.'.'.$k);
						// Has permission
						if($permission) return true; 
						// Permission not yet set for user, but has it in permission arrays default, allow it
						else if(!x($permission) and $v['default'] and in_array(u('type'),$v['default'])) return true;
					}
				}
			}
			return false;
		}
		
		// Id
		if(!is_int_value($id)) $id = NULL;
		
		// Plugin - example: comments.add (full example: permission('blogs','comments.add',$blog_id)
		if(strstr($action,'.')) {
			list($_plugin,$_action) = explode('.',$action,2);
			if(!in_array($_action,array('all','own')) and plugin($plugin)) {
				$plugin = $_plugin;
				$action = $_action;	
			}
		}
		
		// Add
		if($action == "edit" and !$id) $action = "add";
		
		// Debug
		//debug("<br />permission(). module: ".$module.", action: ".$action.", id: ".$id.", plugin: ".$plugin.", plugin_id: ".$c[plugin_id]);
		
		// Array
		if($plugin) $array = plugin($plugin.'.permissions.'.$action);
		else $array = m($module.'.permissions.'.$action);
		// Array - with ownership
		if(!$array and ($id > 0 or $c[plugin_id])) {
			if($plugin and $c[plugin_id]) {
				$plugin_item = plugin_item::load($module,$id,$plugin,$c[plugin_id]);
				if($plugin_item->db('user') and $plugin_item->db('user') == u('id')) $action .= "_own";
				else $action .= "_all";
			}
			else if($id > 0) {
				$item = item::load($module,$id);
				if($item->user and $item->user == u('id')) $action .= "_own";
				else $action .= "_all";
			}
			if($plugin) $array = plugin($plugin.'.permissions.'.$action);
			else $array = m($module.'.permissions.'.$action);
		}
		
		// Debug
		////debug("permission info: ".return_array($array));
		
		// Permission undefined
		if(!$array) {
			//debug("permission isn't defined, returning false");
			return false;
		}
		
		// Super admin, always true
		if(u('type') == "supers") {
			//debug("super admin, returning true");
			return true;
		}
		
		// Permission
		$permission = u('permissions.'.$module.'.'.($plugin ? $plugin."\." : "").$action);
		
		// Has permission
		if($permission == 1) {
			//debug("permission defined and given, returning true");
			return true;
		}
		
		// Doesn't have permission, but is 'true' by default (and no permission set)
		if(!x($permission) and $array['default'] and in_array(u('type'),$array['default'])) {
			//debug("permission not set for user type but given by default, returning true");
			return true;
		}
		
		// Default
		//debug("no permission, returning false");
		return false;
	}
}

/************************************************************************************/
/************************************* Cache ****************************************/
/************************************************************************************/
if(!function_exists('cache_delete_framework')) {
	/**
	 * Determines the module and id of the item(s) that were altered so we can clear their cache (via cache_delete_framework_item).
	 * 
	 * @param string $statement The statement (SELECT, UPDATE, INSERT, DELETE) of the query run.
	 * @param string $table The table the query was run on.
	 * @param string $query The query that was run.
	 */
	function cache_delete_framework($statement,$table,$query) {
		// Debug
		//debug("<b>cache_delete_framework($statement,$table,$query);</b>");
		
		// Get module
		foreach(m() as $k => $v) {
			if($v[db][table] == $table) {
				$module = $k;
				break;
			}
		}
		if($module) {
			// Get item id
			$id = 0;
			if($statement == "INSERT") $id = "new";
			if($statement == "UPDATE" or $statement == "DELETE") {
				$key = m($module.'.db.id');
				if($key) {
					preg_match('/WHERE([a-z0-9\._`\'= ]*?) `?'.$key.'`? = [\'|"]?([0-9]{1,10})[\'|"]?$/si',$query,$ids);
					$id = $ids[2];
				}
			}
			
			// Delete
			cache_delete_framework_item($module,$id,$query);
		}
		else {
			// Get plugin
			foreach(plugin() as $k => $v) {
				if($v[db][table] == $table) {
					$plugin = $k;
					break;
				}
			}
			if($plugin) {
				// Get plugin item id
				$id = 0;
				if($statement == "INSERT") $id = "new";
				if($statement == "UPDATE" or $statement == "DELETE") {
					$key = plugin($plugin.'.db.id');
					if($key) {
						preg_match('/WHERE([a-z0-9\._`\'= ]*?) `?'.$key.'`? = [\'|"]?([0-9]{1,10})[\'|"]?$/si',$query,$ids);
						$plugin_id = $ids[2];
					}
				}
				
				// Delete
				cache_delete_framework_plugin_item($plugin,$plugin_id,$query);
			}
		}
	}
}

if(!function_exists('cache_delete_framework_item')) {
	/**
	 * Delete's all cached data for given item.
	 * 
	 * @param string $module The module of the item that was altered.
	 * @param int $id The id of the item that was altered.
	 * @param string $query The query that was run.
	 */
	function cache_delete_framework_item($module,$id,$query) {
		// Error
		if(!$module) return;
		
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Debug
		//debug("cache_delete_framework_item($module,$id);");
		
		// Item
		if($id != "new") {
			// Item
			cache_delete('item/'.$module.'/'.($id ? $id : ""));
			g('cache.item.'.$module.($id ? ".".$id : ""),"NULL");
			
			// Pages - no longer cached
			/*if(in_array($module,array("types","categories_items"))) cache_delete('pages/');
			else cache_delete('pages/'.$module.'/'.($id ? 'item/'.$id.'/' : ""));*/
			
			// Menus
			if($id > 0) {
				$m = module::load('menus-items');
				if(m($module.'.public.views.item') and m($module.'.public.active')) $where = "module_area = 'public' AND module_code = '".$module."' AND module_view = 'item' AND item_id = '".$id."'";
				if($module == "types") $where = "module_area = 'public' AND module_view = 'type' AND item_id = '".$id."'";
				if($module == "categories_items") $where = "module_area = 'public' AND module_view = 'category' AND item_id = '".$id."'";
				$rows = $m->rows(array('where' => $where,'limit' => 1));
				if($m->db->n($rows)) cache_delete('menus/');
			}
			
			// User
			if($module == "users" and $id == u('id')) unset($_SESSION['u']['item']);
		}
		
		// Pages - no longer cached
		/*if(in_array($module,array("settings","menus","types"))) {
			cache_delete('pages/');
		}*/
		
		// Menus
		if($module == "menus" or $module == "menus_items") {
			cache_delete('menus/');
		}
		
		// Themes
		if($module == "themes") {
			cache_delete('themes/');
		}
		
		// Module
		if($module == "modules" or $module == "types") {
			// Modules
			cache_delete('framework/modules');
			
			// Code
			$module_code = NULL;
			if($id > 0) {
				foreach(m() as $k => $v) {
					if($module == "modules" and $v[id] == $id) {
						$module_code = $k;
						break;
					}
					if($module == "types" and $v[types]) {
						foreach($v[types] as $type => $type_v) {
							if($type_v[id] == $id) {
								$module_code = $k;
								$type_code = $type;
								break;
							}
						}		
					}
				}
			}
			
			// Item
			cache_delete('item'.($module_code ? "/".$module_code : ""));
			// Pages - no longer cached
			//cache_delete('pages'.($module_code ? "/".$module_code : ""));
			
			// Type
			if($module == "types") {
				// Permissions
				if(!$query or strstr($query,"type_permissions")) {
					cache_delete('permissions'.($type_code ? "/".$type_code : ""));
					cache_delete('admin/sections'.($type_code ? "/".$type_code : ""));
				}
			}
		}
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
	}
}

if(!function_exists('cache_delete_framework_plugin_item')) {
	/**
	 * Delete's all cached data for given plugin item.
	 * 
	 * @param string $plugin The plugin of the plugin item that was altered.
	 * @param int $plugin_id The id of the plugin item that was altered.
	 * @param string $query The query that was run.
	 */
	function cache_delete_framework_plugin_item($plugin,$plugin_id,$query) {
		// Error
		if(!$plugin or !$plugin_id) return;
		
		// Database
		$db = db::load();
		
		// Row
		$row = $db->f("SELECT * FROM `".plugin($plugin.'.db.table')."` WHERE ".plugin($plugin.'.db.id')." = '".$plugin_id."'");
		
		// Error
		if(!$row[module_code] or !$row[item_id]) return;
		
		// Plugin item
		$string = 'item/'.$row[module_code].'/'.$row[item_id].'/plugin_item/'.$plugin.'/'.$plugin_id;
		$string_local = 'cache.'.str_replace('/','.',$string);
		g($string_local,"NULL");
		cache_delete($string);
		
		// Plugin - need to do this too since we cache query reqults
		$string = 'item/'.$row[module_code].'/'.$row[item_id].'/plugin/'.$plugin;
		$string_local = 'cache.'.str_replace('/','.',$string);
		g($string_local,"NULL");
		cache_delete($string);
	}
}

/************************************************************************************/
/************************************* Nesting **************************************/
/************************************************************************************/
if(!function_exists('nest_save')) {
	/**
	 * Saves the order/parent for an array of nested items for the given module.
	 *
	 * @param string $module The module these items belong to.
	 * @param array $children The array of children below the given parent.
	 * @param int $parent The id of the parent item. Gets set internally by the function when processing child items. Default = 0
	 * @param int $level The level we're at in the nesting. Gets set internally by the function when processing child items. Default = 0
	 * @param string $string The string of the current nesting level we're at. Gets set internally by the function when processing child items. Default = NULL
	 */
	function nest_save($module,$children,$parent = 0,$level = 0,$string = NULL) {
		foreach($children as $x => $v) {
			// String
			$s = ($string ? $string.":" : '').$v[id];
			// Order
			$order = $x + 1;
			
			// Values
			$values = NULL;
			if(m($module.'.db.order')) $values[m($module.'.db.order')] = $order;
			if(m($module.'.db.order_parent')) $values[m($module.'.db.order_parent')] = $parent;
			if(m($module.'.db.order_level')) $values[m($module.'.db.order_level')] = $level;
			//if(m($module.'.db.order_string')) $values[m($module.'.db.order_string')] = $s; // Can't think of a need for this
			
			// Save
			$item = item::load($module,$v[id]);
			$item->save($values,array('debug' => 1));
			
			// Debug
			debug("module: ".$module.", id: ".$v[id].", level: ".$level.", order: ".$x);
			
			// Children
			if($v[children]) nest_save($module,$v[children],$v[id],($level + 1),$s);
		}
	}
}

/************************************************************************************/
/************************************* Strings **************************************/
/************************************************************************************/
if(!function_exists('string_word_last')) {
	/**
	 * Returns last word of a string. Used to get last variable for db array value which has a space. Example: "user_first_name user_last_name" would return "user_last_name".										
	 * 
	 * @param string $string The string you want to get the last word of.
	 */
	function string_word_last($string) {
		$string = trim($string);
		$ex = explode(' ',$string);
		$last = $ex[count($ex) - 1];
		return $last;
	}
}
if(!function_exists('string_convert_accents')) {
	/**
	 * Convert accented characters to standard 256-ASCII charset.
	 *
	 * http://code.google.com/p/sehl/
	 * 
	 * Warning: file must be saved in UTF-8 format.									
	 * 
	 * @param string $string The string you want to convert accents in.
	 * @param string The string with accents converted to standard 56-ASCII charset.
	 */
	function string_convert_accents($string) {
		//$f_r = function_speed(__FUNCTION__);
		
		// http://weblogtoolscollection.com/archives/2004/05/17/convert-accented-characters-to-their-non-accented-equivalents-in-php/
		//return strtr($string,"ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ","SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");

		/* This function moves special UTF-8 characters down to standard 256-ASCII charset */
		/* You may have to change this to your specific language, for example in german 'ü' may downcast to 'ue' instead of 'u' */
		 return strtr($string, array('À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A','Æ' => 'AE','Ç' => 'C','È' => 'E', 'É' => 'E','Ê' => 'E','Ë' => 'E','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I','Ð' => 'D','Ñ' => 'N','Ò' => 'O','Ó' => 'O','Ô' => 'O','Õ' => 'O','Ö' => 'O','Ø' => 'O','Ù' => 'U','Ú' => 'U','Ü' => 'U','Û' => 'U','Ý' => 'Y','Þ' => 'Th','ß' => 'ss','à' => 'a','á' => 'a','â' => 'a','ã' => 'a','ä' => 'a','å' => 'a','æ' => 'ae','ç' => 'c','è' => 'e','é' => 'e','ê' => 'ë','ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ð' => 'd','ñ' => 'n','ò' => 'o','ó' => 'o','ô' => 'o','õ' => 'o','ö' => 'o','ø' => 'o','ù' => 'u','ú' => 'u','û' => 'u','ü' => 'u','ý' => 'y','þ' => 'th','ÿ' => 'y','Œ' => 'OE','œ' => 'oe'));
		//function_speed(__FUNCTION__,$f_r);
	}
}

if(!function_exists('string_convert_hex')) {
	/**
	 * Get the HEX representation of some characters.
	 *
	 * http://code.google.com/p/sehl/	
	 * 
	 * @param string $string The string you want to get the hex representation of.
	 * @return string The string with hex representations in place of original strings.
	 */
	function string_convert_hex($string) {
		//$f_r = function_speed(__FUNCTION__);
		$result = '';
		for($i = 0; $i < strlen($string); $i++) {
			if(ord(substr($string, $i, 1)) > 127) $result .= '&#x'.bin2hex(substr($string, $i, 1)).';';
			else $result .= substr($string, $i, 1);
		}
		//function_speed(__FUNCTION__,$f_r);
		return $result;
	}
}

if(!function_exists('string_convert_dec')) {
	/**
	 * Get the decimal representation of some characters.
	 *
	 * http://code.google.com/p/sehl/
	 * 
	 * @param string $string The string you want to get the decimal representation of.
	 * @return string The string with decimal representations in place of original strings.
	 */
	function string_convert_dec($string) {
		//$f_r = function_speed(__FUNCTION__);
		$result = '';
		for($i = 0; $i < strlen($string); $i++) {
			if(ord(substr($string, $i, 1)) > 127) $result .= '&#'.ord(substr($string, $i, 1)).';';
			else $result .= substr($string, $i, 1);
		}
		//function_speed(__FUNCTION__,$f_r);
		return $result;
	}
}

if(!function_exists('string_format')) {
	/**
	 * Converts the given string to a dynamic string based upon the values in the given array.
	 *
	 * Note: $string may be 2 or more keys (sepearted by spaces). Example: "user_first_name user_last_name",
	 * Or even contain punctuation. Example: "location_name, location_city".
	 * 
	 * @param string $string The string we want to convert to a dynamic string based upon values in the given array.
	 * @param array $array The array of data we'll use when converting the string to a dynamic string.
	 * @return string The compiled string with keys replaced with dynamic values.
	 */
	function string_format($string,$array) {
		if($array) {
			// Do longer keys first (so we don't do an a string like 'id' that might appear in something longer like 'width')
			array_multisort(array_map('strlen', array_keys($array)),SORT_DESC,$array);
			
			// Loop through keys
			foreach($array as $k => $v) {
				// Replace key with value in $string
				$string = str_replace($k,$v,$string);
			}
		}
		// Return
		return $string;
	}
}
?>