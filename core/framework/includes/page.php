<?php
/************************************************************************************/
/********************************* Modules / Plugins ********************************/
/************************************************************************************/
// Modules
foreach(g('modules') as $module => $v) {
	if($v[core]) include_once SERVER.$v[path_local]."includes/page.php";
	include_once SERVER.$v[path]."includes/page.php";
}
// Plugins
foreach(g('plugins') as $plugin => $v) {
	if($v[core]) include_once SERVER.$v[path_local]."includes/page.php";
	include_once SERVER.$v[path]."includes/page.php";
}
?>