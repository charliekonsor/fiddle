<?php
/************************************************************************************/
/*************************************** Config *************************************/
/************************************************************************************/
// Speed
debug_speed('admin - framework - dawn');
debug_speed('config','admin - framework - dawn');

// Config
$config = NULL;
include_once SERVER."core/framework/admin/config.php";
if($config) $__GLOBAL['config'] = array_merge($__GLOBAL['config'],$config);

/************************************************************************************/
/************************************* Functions ************************************/
/************************************************************************************/
// Speed
debug_speed('functions','admin - framework - dawn');

// Core
require_once SERVER."core/framework/admin/functions/core.php";
?>