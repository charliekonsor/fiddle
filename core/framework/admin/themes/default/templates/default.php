<?php
// Functions
require_once SERVER."core/framework/admin/themes/".g('config.admin.theme')."/functions/core.php";

// Error
if(u('id') and !u('admin')) {
	page_error('You must be an administrator to view this.',1);	
}

// Meta
print "
".$this->header()."
<body>
	<div id='admin-container'>
		<div id='admin-header'>";

// Logo
print "
			<div id='admin-logo'>
				<a href='".DOMAIN."admin'>admin</a>
			</div>";

if(u('id')) {
	// Tabs
	print "
			<div id='admin-tabs'>
				".default_admin_tabs()."
			</div>";

	print "
			<div id='admin-tabs-right' class='admin-tabs'>
				<ul>";
	// Documentation
	$file = SERVER."core/framework/admin/themes/".$this->theme."/documentation/".u('type').".php";
	if(file_exists($file)) print "
					<li><a href='".D."?ajax_action=documentation&file=".urlencode($file)."' class='overlay'>Help</a></li>";
	// Logout
	print "
					<li><a href='".DOMAIN."logout'>Logout</a></li>";
	print "
			</div>";
}
else {
	// Documentation
	$file = SERVER."core/framework/admin/themes/".$this->theme."/documentation/".u('type').".php";
	if(file_exists($file)) print "
			<div id='admin-tabs-right' class='admin-tabs'>
				<ul>
					<li><a href='".D."?ajax_action=documentation&file=".urlencode($file)."' class='overlay'>Help</a></li>
				</ul>
			</div>";
}

print "
		</div>";
	
if(u('id') and u('admin')) {
	// Columns
	print "
		<table id='admin-columns'>
			<tr>";
	// Left - sidebar
	print "
				<td valign='top' id='admin-columns-left'>
					<div id='admin-sidebar'>
						".default_admin_sidebar()."
					</div>
				</td>";
	// Right - content
	print "
				<td valign='top' id='admin-columns-right'>
					<div id='admin-content'>
						".$this->view()."
					</div>
				</td>
			</tr>
		</table>";
}

// Login
else {
	print "
		<div id='admin-login'>
			<h2>Login</h2>
			".$this->login_form(array('register' => 0))."
		</div>";
}
print "
	</div>
</body>
".$this->footer();
?>