<?php
print "
<h1>Help</h1>

You've now suscessfully logged in to the admin section of your CMS.  Well done, you!<br /><br />

Now, let's start managing some content.<br /><br />

<h1>Navigation</h1>
In the left hand side bar you should see several sections (Menus, Content, Users, etc.). Each of these sections groups together similar modules.  What is a module you ask?  Well, a 'module' is simply a specific area within your CMS.  For example, 'Menus', 'Users', and 'Pages' are all modules.<br /><br />

Within each of the sections you see in the left sidebar, you'll find a list of modules that belong to that section.  Sometimes it's just one module (for example, the 'Menus' section).  Sometimes it's many (for example, the 'Content' section).<br /><br />

Most of the time you'll be working either in the 'Content' section or the 'Menus' section (which also lets you manage content).<br /><br />

<h1>Next Step</h1>
Now that you know how the admin is laid out, it's time to start diving in and editing content.<br /><br />

You should be able to figure out how to manage most content on your own.  However, if you need help, you can always click on the 'Help' button that appears to the right of a module's page heading.  For example, if you're in the Menus module, you should see a 'Menus' heading near the top of the page. To the right of that heading you will find the 'Help' button which will tell you much more about exactly how menus work.<br /><br />

If you're not sure where to start, we suggest you head over to the afore mentioned <a href='".DOMAIN."admin/menus'>Menus</a> module.<br /><br />";
?>