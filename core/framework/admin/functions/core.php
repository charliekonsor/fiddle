<?php
/**
 * @package kraken\admin
 */

if(!function_exists('admin_sections_framework')) {
	/**
	 * Returns an array of sections/modules for use in the admin.
	 * 
	 * @param array $c An array of congiguration values. Default = NULL
	 * @return array An array of sections and modules within that section.
	 */
	function admin_sections_framework($c = NULL) {
		$f_r = function_speed(__FUNCTION__);
		// Config
		if(!x($c[cached])) $c[cached] = 1; // Use cached data (if available)
		if(!x($c[cache])) $c[cache] = 1; // Cache data for future use
		
		// Cached?
		if($c[cached]) {
			if($array = cache_get('admin/sections/'.u('type'))) {
				function_speed(__FUNCTION__,$f_r,1);
				return $array;
			}
		}
		
		// Modules
		foreach(m() as $k => $v) {
			// Appears in the admin
			if($v[admin][active] == 1 and $v[admin][section] and ($v[admin][sidebar] or $v[admin][header])) {
				// Have permission to 'manage' module
				if(permission($k,'manage')) {
					// Types
					if($v[admin][types] and $v[types]) {
						$types = NULL;
						foreach($v[types] as $type => $type_v) {
							if($type_v[admin][active] == 1) {
								if(permission($k,'manage',NULL,array('type' => $type))) $types[$type] = $type_v[admin];
							}
						}
						$v[admin][types] = $types;
					}
					// Add to array
					$array[$v[admin][section]][$k] = $v[admin];
				}
			}
		}
		
		// Arrange (menus, content, other modules, users, system)
		$array_arranged = array();
		if($array[menus]) $array_arranged[menus] = $array[menus];
		if($array[content]) $array_arranged[content] = $array[content];
		unset($array[menus]);
		unset($array[content]);
		$array_arranged = array_merge($array_arranged,$array);
		unset($array_arranged[users],$array_arranged['system']);
		if($array[users]) $array_arranged[users] = $array[users];
		if($array['system']) $array_arranged['system'] = $array['system'];
		
		// Cache
		if($c[cache]) cache_save('admin/sections'.u('type'),$array_arranged);
		
		// Return
		function_speed(__FUNCTION__,$f_r);
		return $array_arranged;
	}
}
?>