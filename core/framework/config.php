<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
/**
 * An array of framework global congiguraiton values
 *
 * These values will be stored in the $__GLOBAL array under the 'config' parent so you could get our database name by calling $__GLOBAL['config']['db']['mysql']['database'].
 * We also provide a simple g() function to access the global so you could also get the database name by calling g('config.db.mysql.database');
 *
 * Note, that we separate different levels of the config array with periods. If one of the level's keys contains a period, you can access it by escaping it with a preceding forward slash (\) so, if we had the configuraiton $config = array('colors' => array('admin.h1' => 'fff','admin.h2' => '000')) we could access it like this: g('config.colors.admin\.h1');
 */
$config = array(
	'admin' => array(
		'theme' => 'default'
	)
);

?>