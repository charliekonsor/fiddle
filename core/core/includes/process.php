<?php
/************************************************************************************/
/********************************* Local / Framework ********************************/
/************************************************************************************/
// Local
include_once SERVER."local/includes/process.php";

// Framework
if(FRAMEWORK) include_once SERVER."core/framework/includes/process.php";

/************************************************************************************/
/************************************** Scripts *************************************/
/************************************************************************************/

/**
 * Logout
 **/
if($_POST['process_action'] == "logout") {
	// Redirect URL
	if($_GET['redirect']) $redirect = urldecode($_GET['redirect']);
	else if($_POST['redirect']) $redirect = $_POST['redirect'];
	else if(strstr($_SESSION['urls'][0],'login') and $_SESSION['urls'][1]) $redirect = $_SESSION['urls'][1];
	else if($_SESSION['urls'][0]) $redirect = $_SESSION['urls'][0];
	else $redirect = D;
	if(/*strstr($redirect,'login') or */strstr($redirect,'logout') or strstr($redirect,'register')) $redirect = D;
	
	// Logout
	$core->logout();
	
	// Redriect
	redirect($redirect);
}

/**
 * Login
 **/
if($_POST['process_action'] == "login") {
	// Field
	$field = "user_name";
	// Label
	$label = "Username";
	
	// Form
	$form = form::load();
	$results = $form->process(g('unpure.post'),g('unpure.files'));
	
	// Login
	if($_POST['user'] and $_POST['password']) {
		// Query
		$query = "SELECT user_id FROM users WHERE user_name = '".a($_POST['user'])."' AND user_password = '".$_POST['password']."'";
		$rows = $core->db->q($query." AND user_visible = 1");
		if(!$core->db->n($rows)) $rows = $core->db->q($query." AND user_visible = 0");
		if($core->db->n($rows)) {
			$row = $core->db->f($rows);
			// Login
			$core->login($row[user_id],array('redirect' => $results[redirect],'source' => 'login','remember' => $_POST['remember']));
		}
		else $error = true;
	}
	else $error = true;
	
	// Error
	if($error == true) {
		page_error("Incorrect username or password. Please try again.",1);
		redirect($_SESSION['urls'][0]);
	}
	
	// Exit
	exit;
}

/**
 * Adds an item to the session cart.
 **/
if($_POST['process_action'] == "cart_add") {
	// Debug
	$debug = 0;	
	// Form
	$form = form::load();
	
	// Process
	$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
	
	// Errors
	if($results[errors]) {
		if($results[url] and (!$results[c][debug] or !debug_active())) redirect($results[url]);
		else if(debug_active()) print "
<div class='core-red'>
<b>Errors:</b> 
".return_array($results[errors])."
</div>";
	}
	// No errors
	else {
		// Cart
		$cart = cart::load($module);
		
		// Add
		$item = $cart->item_add($results[values]);
		
		// Message
		if($item[errors]) {
			page_error(implode(" ",$item[errors]));
			debug("errors: ".return_array($item[errors]),$debug);
		}
		else page_message("This item has been added to your cart.");
		
		// Debug
		debug("cart: ".return_array($cart),$debug);
		
		// Redirect
		redirect($results[redirect]);
	}
	
	// Exit
	exit;
}

/**
 * Updates the session cart.
 **/
if($_POST['process_action'] == "cart_update") {
	// Debug
	$debug = 0;
	// Redirect
	$redirect = urldecode($_POST['redirect']);
	
	// Errors
	$errors = array();
	
	// Cart
	$cart = cart::load();
	
	// Update
	foreach($_POST['items'] as $key => $item) {
		$item = $cart->item_update($key,$item);
		if($item[errors]) $errors = array_merge($errors,$item[errors]);
	}
	
	// Message
	if($errors) {
		page_error(implode(" ",$errors));
		debug("errors: ".return_array($errors),$debug);
	}
	else page_message("Your cart has been updated.");
			
	// Debug
	debug("cart: ".return_array($cart),$debug);
	
	// Redirect
	if((!$debug or !debug_active()) and $redirect) redirect($redirect);
	
	// Exit
	exit;
}

/**
 * Removes an item from the session cart.
 **/
if($_GET['process_action'] == "cart_delete") {
	if($_GET['key']) {
		// Key
		$key = $_GET['key'];
		// Redirect
		$redirect = urldecode($_GET['redirect']);
		
		// Cart
		$cart = cart::load($module);
		
		// Item
		$item = $cart->items[$key];
		
		// Delete
		$cart->item_delete($key);
		
		// Message
		page_message(($item[name] ? $item[name] : "This item")." has been removed from your cart.");
		
		// Redirect
		if($redirect) redirect($redirect);
	}
	
	// Exit
	exit;
}
?>