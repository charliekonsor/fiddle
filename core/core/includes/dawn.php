<?php
/**
 * @package kraken
 */
 
/************************************************************************************/
/************************************** Global **************************************/
/************************************************************************************/
global $__GLOBAL; // Needed in case we're calling this up from within a function/class (which we seem to do in KCFinder)
$__GLOBAL = array();

/************************************************************************************/
/*************************************** Time ***************************************/
/************************************************************************************/
if(!function_exists('microtime_float')) {
	/**
	 * Returns the UNIX time, in microsecodns, without the space of microtime()
	 * 
	 * @return float The current UNIX timestamp with microseconds
	 */
	function microtime_float() {
		list($milliseconds,$seconds) = explode(' ',microtime()); 
		$microtime = (float)$milliseconds + (float)$seconds;
		return $microtime;
	}
}

// Start debugging speed timer
$__GLOBAL['debug']['speed']['total']['start'] = microtime_float();

/************************************************************************************/
/************************************** Server **************************************/
/************************************************************************************/
if(!function_exists('server')) {
	/**
	 * Determines the full server path to either the current file or (if $relative is passed) to the root of the website.
	 * 
	 * @param string $file The current file we're in
	 * @param string $relative The relative path to the file where we're calling this from (so we don't include it in the returned path)
	 * @return string The full server path to the root of the site (or, if no $relative given, to the current file)
	 */
	function server($file = __FILE__,$relative = NULL) {
		$server = realpath($file);
		$server = str_replace("\\","/",$server); // Change forward slashes (windows servers) to back slashes
		if($relative) $server = str_replace($relative,'',$server);
		
		return $server;
	}
}
// Define
$server = server(__FILE__,'core/core/includes/dawn.php');
define('SERVER',$server);
set_include_path(get_include_path().PATH_SEPARATOR.$server);

/************************************************************************************/
/************************************** Errors **************************************/
/************************************************************************************/
if(!function_exists('php_error')) {
	/**
	 * Function to handle PHP errors
	 * 
	 * @param string $error_number The PHP error number
	 * @param string $error_message The PHP error message
	 * @param string $error_file The file where the error occured
	 * @param string $error_line The line number of where the error occured
	 */
	function php_error($error_number, $error_message, $error_file, $error_line) {
		global $__GLOBAL;
		//if(isset($__GLOBAL['config']['debug']['active']) and isset($__GLOBAL['config']['debug']['admin'])) {
			if(debug_active() and isset($error_type) and isset($error_message)) {
				$debug = NULL;
				if(isset($_GET['debug'])) $debug = $_GET['debug'];
				
				print "if($error_number == ".E_WARNING." or ($error_number == ".E_NOTICE." and (".strstr($error_message,'function.unserialize').")) or $debug == \"full\") {<br />";
				if($error_number == E_WARNING or ($error_number == E_NOTICE and (strstr($error_message,'function.unserialize'))) or $debug == "full") {
					$__GLOBAL['debug']['php']['errors-count'] += 1;
					$__GLOBAL['debug']['php']['errors'] .= "Error ".$error_number.": ".$error_message.", File: ".$error_file.", Line: ".$error_line.($debug == "advanced" ? ", Backtrace: ".return_array(debug_backtrace()) : "")."<br />";
				
					print "Error ".$error_number.": ".$error_message.", File: ".$error_file.", Line: ".$error_line.($debug == "advanced" ? ", Backtrace: ".print_array(debug_backtrace(),1) : "")."<br />";
				}
			}
		//}
		return true;
	}
}

// Error handler
set_error_handler("php_error");

// Turn on error reporting
ini_set('display_errors',1); 
error_reporting(E_ALL);

/************************************************************************************/
/*************************************** Core ***************************************/
/************************************************************************************/
// Debug
include_once SERVER."core/core/functions/debug.php";
// Core functions
require_once SERVER."core/core/functions/core.php";
// Core class
require_once SERVER."core/core/classes/core.php";

/************************************************************************************/
/************************************* Framework ************************************/
/************************************************************************************/
// !!! Must be before session_start() as the cart get stored in the session and so all classes used in the saved cart must already be included. !!!
debug_speed('framework','core - dawn');
if(file_exists(SERVER."core/framework/includes/dawn.php")) {
	/** Stores whether or not we're using the framework files or just the basic files. */
	define('FRAMEWORK',true);
	
	// Core
	require_once SERVER."core/framework/classes/core.php";
}
else define('FRAMEWORK',false);

/************************************************************************************/
/********************************* Config / Constants *******************************/
/************************************************************************************/
if(!function_exists('constants_define')) {
	/**
	 * Defines constants based upon given array keys (and parent keys) that both point to where it is in global config and how we want to name it.
	 * 
	 * @param array $keys An array of keys to to define.
	 * @param array $parents An array of parent keys to use when getting value and defining constant name. 
	 */
	function constants_define($keys,$parents = NULL) {
		if(is_array($keys)) {
			foreach($keys as $k => $v) {
				$key_parents = $parents;
				if(is_array($v)) $key_parents[] = $k;
				constants_define($v,$key_parents);
			}
		}
		else {
			if($parents) {
				$key_config = implode('.',$parents).".";
				$key_constant = implode('_',$parents)."_";
			}
			$key_config .= $keys;
			$key_constant .= $keys;
			$value = g('config.'.$key_config);
			if(x($value)) {
				//debug('defining '.strtoupper($key_constant).' as '.$value);
				define(strtoupper($key_constant),$value);
			}
		}
	}
}

// IP
/** The current visitor's IP address */
define('IP',ip());

// Config
$__GLOBAL['config'] = array();

// Config - core
$config = NULL;
include_once SERVER."core/core/config.php";
if($config) $__GLOBAL['config'] = array_merge_associative($__GLOBAL['config'],$config);

// Config - local
$config = NULL;
include_once SERVER."local/config.php";
if($config) $__GLOBAL['config'] = array_merge_associative($__GLOBAL['config'],$config);
//print "config 2 (g):".return_array(g('config'));
//print "config 2 ($__GLOBAL):".return_array($__GLOBAL['config']);

// Config - debugging
if(g('config.debug.active') and in_array(IP,g('config.debug.ips'))) g('config.debug.admin',1);
else g('config.debug.admin',0);

// Speed
debug_speed("core - dawn");
debug_speed("variables","core - dawn");

// Constants
$constants = array(
	'domain',
	'db' => array(
		'mysql' => array(
			'database',
			'username',
			'password',
			'server',
			'encryption',
		)
	),
	'cache' => array(
		'active',
		'path',
		'method',
	),
);
if(debug_active()) $constants[debug][cache][] = "active"; // If a 'debugger', use settings
else define('DEBUG_CACHE_ACTIVE',1); // If not, enable
constants_define($constants);

/************************************************************************************/
/*************************************** Time ***************************************/
/************************************************************************************/
// Timezone
if(g('config.localization.time.timezone')) date_default_timezone_set(g('config.localization.time.timezone'));

/************************************************************************************/
/*************************************** URL ****************************************/
/************************************************************************************/
// Speed
debug_speed('url','core - dawn');

/** The current page's URL */
define('URL',url());

// DOMAIN
if(!defined('DOMAIN')) {
	// Domain
	$domain = url_domain(URL);
	
	// Folder
	$domain_folder = NULL;
	$domain_folder_found = 0;
	$server_parts = array_reverse(explode('/',SERVER));
	foreach($server_parts as $part) {
		if($part) {
			if(strstr(URL,$domain.$part."/".$domain_folder)) $domain_folder_found = 1;
			else if($domain_folder_found) break;
			
			$domain_folder = $part."/".$domain_folder;
		}
	}
	if($domain_folder_found) $domain .= $domain_folder;
		
	/** The domain of the website (ex: http://www.mywebsite.com/) */
	define('DOMAIN',$domain);
}

// SDOMAIN
/** The domain of the website on https (ex: https://www.mywebsite.com/) */
define('SDOMAIN',str_replace('http://','https://',DOMAIN));

// D
if($_SERVER["HTTPS"] == "on") $d = SDOMAIN;
else $d = DOMAIN;
/** The domain (http or https) media should be hosted on. If we're on an https URL, images, js, etc. needs to be hosted on https as well. */
define('D',$d);

/************************************************************************************/
/********************************* Functions - Local ********************************/
/************************************************************************************/
// Speed
debug_speed('functions - local','core - dawn');
// Functions
include_once SERVER."local/functions/core.php";

/************************************************************************************/
/********************************* Functions - Core *********************************/
/************************************************************************************/
// Speed
debug_speed('functions - core','core - dawn');
// ADOdb - Time
require_once SERVER."core/core/libraries/adodb-time.php";
// Encoding
require_once SERVER."core/core/libraries/Encoding.php";
// User
require_once SERVER."core/core/functions/user.php";

/************************************************************************************/
/********************************** Classes - Core **********************************/
/************************************************************************************/
// Classes - if something else is using __autoload() (ex: kcfinder)
/*if(function_exists('__autoload')) {
	// Loader
	require_once SERVER."core/core/classes/loader.php";
	// Database
	require_once SERVER."core/core/classes/db.php";
	// Forms
	require_once SERVER."core/core/classes/form/form.php";
	require_once SERVER."core/core/classes/form/form_input.php";
	// File
	require_once SERVER."core/core/classes/file.php";
	// Page
	require_once SERVER."core/core/classes/page.php";
	// Cart
	if(g('config.cart.active')) {
		require_once SERVER."core/core/classes/cart.php";
	}
	// API
	if(g('config.api.active')) {
		require_once SERVER."core/core/classes/api.php";
	}
}

/************************************************************************************/
/**************************************** Spam **************************************/
/************************************************************************************/
/*// Spam
if(!$_SESSION['__spam']['checked'] and !$_SESSION['__spam']['spam']) {
	include_class('spam');
	$spam = new spam();
	$_SESSION['__spam']['spam'] = $spam->ip(IP);
	$_SESSION['__spam']['checked'] = time();
}
if($_SESSION['__spam']['spam'] and !$_COOKIE['spam_confirm']) {
	if(function_exists('spam_confirm')) print spam_confirm();
	else {
		print "
Woops!<br />
Looks like this IP address may have been the source of spam at some point (we know, it wasn't by you).<br /><br />

But have no fear! You can still access the site by simply clicking the button below.<br /><br />

<button onclick='spam_confirm();'>Continue</button><br /><br />

Sorry for the inconvencience, but we thank you for helping us combat spam!";
	}
	
	print "
<script type='text/javascript'>
function setcookie(name,value,expires,path,domain,secure) {
	// Set time, it's in milliseconds
	var today = new Date();
	today.setTime(today.getTime());

	if(expires) expires = expires * 1000 * 60 * 60 * 24;
	var expires_date = new Date(today.getTime() + (expires));

	document.cookie = name + '=' +escape(value) +
	((expires) ? ';expires=' + expires_date.toGMTString() : '') +
	((path) ? ';path=' + path : '') +
	((domain) ? ';domain=' + domain : '') +
	((secure) ? ';secure' : '');
}  
function spam_confirm() {
	setcookie('spam_confirm','1',30,'/','','');
	location.reload(true);
}
</script>";
	
	// Exit
	exit;
}

/************************************************************************************/
/**************************************** SEF ***************************************/
/************************************************************************************/
// Speed
debug_speed('sef','core - dawn');

// GET
$get = url_query_array();
$get[sef] = $_GET['sef'];
$_GET = $get;
foreach($get as $k => $v) $_REQUEST[$k] = $v;
$_SERVER['QUERY_STRING'] = http_build_query($get);

// Stripslashes to GET and POST - remove what magic quotes did
if(get_magic_quotes_gpc()) {
	$_GET = s($_GET);
	$_POST = s($_POST);
}

// Store unpurified
$__GLOBAL['unpure'] = array(
	'get' => $_GET,
	'post' => $_POST,
	'files' => $_FILES,
	'request' => $_REQUEST
);

// Addslashes to GET and POST
$_GET = a($_GET);
$_POST = a($_POST);

// Purify // Do I perhaps want to move this above addslashes? Or maybe just not purify quotes as they're already being handled
$_GET = purify($_GET);
$_POST = purify($_POST);
$_FILES = purify($_FILES);
$_REQUEST = purify($_REQUEST);

/************************************************************************************/
/*********************************** Refresh Cache **********************************/
/************************************************************************************/
if($_GET['refresh'] and !$_POST) {
	if($_GET['refresh'] === "1") cache_delete();	
	else cache_delete($_GET['refresh']);
}

/************************************************************************************/
/********************************* Framework - Dawn *********************************/
/************************************************************************************/
if(FRAMEWORK) {
	// Dawn
	require_once SERVER."core/framework/includes/dawn.php";
}

/************************************************************************************/
/********************************** Classes - Core  *********************************/
/************************************************************************************/
// Speed
debug_speed("classes","core - dawn");

/**
 * Autoloads non-essential classes.
 *
 * Note, we can use just 'include' because it will only ever be called once.
 *
 * @param string $class The name of the class we're trying to load.
 */
if(!function_exists('autoload')) {
	function autoload($class) {
		//print "autoload(".$class.")<br />";
		
		// Already attempted to autoload
		if(g('autoload.'.$class)) {
			//print "already tried to load ".$class."<br />";
			return;
		}
		// Save autoload attempt
		g('autoload.'.$class,1);
		
		// Hybrid Auth
		if($class == "Hybrid_Auth") {
			require_once SERVER."core/core/libraries/hybridauth/Hybrid/Auth.php";
			return;
		}
		
		// Local
		$class = str_replace('_local','',$class);
		
		// Explode
		$ex = explode('_',$class);
		
		// Variables
		$folder = NULL;
		
		// Form
		if($ex[0] == "form") {
			$folder = "form/";
			$class = $ex[0];
			
			if($ex[1] == "input") { // form_input_..
				unset($ex[0]);
				$class .= "_".implode('_',$ex);
			}
		}
			
		//print "autoloading ".$class." (folder: ".$folder.")<br />";
			
		
		// Core
		$files[] = SERVER."core/core/classes/".$folder.$class.".php";
		
		// Include
		if($files) {
			foreach($files as $file) {
				if(file_exists($file)) {
					//print "including ".$file."<br />";
					include_once $file;	
				}
			}
		}
	}
}
spl_autoload_register('autoload');
if(function_exists('__autoload')) spl_autoload_register('__autoload');

/************************************************************************************/
/************************************* Settings *************************************/
/************************************************************************************/
// Speed
debug_speed("settings","core - dawn");

// Compress Output // Can't get any of these to work (need to add to ini.php), but worth trying: http://tuxradar.com/practicalphp/18/1/15
//ini_set('output_buffering',1);
//ini_set('output_handler','ob_gzhandler');
//ob_start("ob_gzhandler");

// Turn off Register Globsl
//ini_set('register_globals',0); // Can't call here, must use .htaccess

// Session Life
ini_set('session.gc_maxlifetime',7200); // 3600 seconds = 60 minutes
// Timeout
ini_set('max_execution_time',120); // 2 Minutes
if(ini_get('safe_mode') == 0) set_time_limit(120);
// Memory Limit
//ini_set('memory_limit', 1024 * 1024 * 64);

// Character Set
header('Content-Type:text/html; charset=UTF-8');
ini_set('default_charset', 'UTF-8');

// Caching - don't want browser to cache dynamic pages
header("Cache-Control: no-cache, max-age=0, must-revalidate, no-store");

// Uploads
if(!in_array(ini_get('file_uploads'),array("On","1","true"))) ini_set('file_uploads','1');
if(!ini_get('upload_tmp_dir')) ini_set('upload_tmp_dir','/tmp');
// Connection Close (fix for upload bug in Safari)
header("connection: close");

/************************************************************************************/
/************************************* Core Class ***********************************/
/************************************************************************************/
// Speed
debug_speed('core class','core - dawn');

// Core
$core = core::load();

// Session
$core->session_start();

// Global
$__GLOBAL['core'] = $core;

/************************************************************************************/
/*************************************** Crons **************************************/
/************************************************************************************/
/*if(FRAMEWORK and !strstr(URL,'core/framework/crons/')) {
	// Speed
	debug_speed('crons','core - dawn');

	// Crons
	$crons = array(
		'1.minutes',
		'5.minutes',
		'15.minutes',
		'1.hours',
		'1.days',
	);
	foreach($crons as $cron) {
		cron_run($cron,1);
	}
}

/************************************************************************************/
/*************************************** User ***************************************/
/************************************************************************************/
// Speed
debug_speed('user','core - dawn');
	
// Login Inactive
if(x(g('config.login.active')) and g('config.login.active') != 1) $_SESSION['u'] = NULL;
else {
	// Login with Cookie
	if($cookie = $core->cookie_get("u_id") and !$_SESSION['u']['id']) {
		$core->login($cookie,array('source' => 'cookie'));
		# for some reason it'll do this on first page, then on the next page too, should only have to do on first page, ignore for now.
	}
	
	// Login - Facebook
	/*if($_GET['state']) {
		if(m('settings','share.facebook.key') and m('settings','share.facebook.secret') and in_array('facebook',m('settings','login.external'))) {
			// User
			$user = facebook_user();
			if($user[id]) {
				// Token
				$user[token] = facebook_token();
				
				// Redirect
				$redirect = URL;
				$redirect = preg_replace('/state=(.*?)(&|$)/','',$redirect);
				$redirect = preg_replace('/code=(.*?)(&|$)/','',$redirect);
			
				// Login
				$core->login_external("facebook",$user[id],array('data' => $user,'redirect' => $redirect));
			}
		}
	}
	// Login - Twitter
	if($_GET['process_action'] == "twitter_authorize") {
		if(m('settings','share.twitter.key') and m('settings','share.twitter.secret') and in_array('twitter',m('settings','login.external'))) {
			// Tokens
			$tokens = twitter_authorize();
			if($tokens) {
				// User
				$user = twitter_user(array('tokens' => $tokens));
				if($user[id]) {
					// Tokens
					$user[tokens_oauth_token] = $tokens[oauth_token];
					$user[tokens_oauth_token_secret] = $tokens[oauth_token_secret];
					// Redirect
					if($_GET['redirect']) $redirect = urldecode($_GET['redirect']);
					else {
						$redirect = URL;
						$redirect = preg_replace('/a=twitter_authorize(&|$)/','',$redirect);
						$redirect = preg_replace('/oauth_token=(.*?)(&|$)/','',$redirect);
						$redirect = preg_replace('/oauth_verifier=(.*?)(&|$)/','',$redirect);
					}
					
					// Admin
					if($_GET['admin']) {
						$module_class = module::load('settings');
						$module_class->setting('share_twitter_oauth_token',$tokens[oauth_token]);
						$module_class->setting('share_twitter_oauth_secret',$tokens[oauth_token_secret]);
					}
					
					// Login
					if($_GET['login']) {
						login_external("twitter",$user[id],array('data' => $user,'redirect' => $redirect));
					}
				}
			}
		}
	}*/
}

// User on a different site // I think checking it before cookie should be enough, if not, uncomment this
//if($_SESSION['u']['domain'] != DOMAIN) $_SESSION['u'] = NULL;

// Guest
if(!x($_SESSION['u']['user_id'])) {
	$_SESSION['u'] = array(
		'id' => 0,
		'type' => 'guests',
		'registered' => 0,
		'admin' => 0
	);
}

// Global // using u() function instead
//$__GLOBAL['u'] = $_SESSION['u'];

// Framework
if(FRAMEWORK) {
	// Permissions
	if(function_exists('users_type_permissions')) {
		$permissions = users_type_permissions(u('type'));
		if($permissions) $_SESSION['u']['permissions'] = $permissions;
	}
}

/************************************************************************************/
/*************************************** Local **************************************/
/************************************************************************************/
// Speed
debug_speed('local - dawn','core - dawn');
// Include
include_once SERVER."local/includes/dawn.php";
include_once SERVER."local/includes/load.php"; // Legacy

/************************************************************************************/
/*************************************** AJAX ***************************************/
/************************************************************************************/
if($_REQUEST['ajax_action']) {
	// Speed
	debug_speed('ajax','core - dawn');

	// Local - admin
	/*if(strstr($_SESSION['urls'][0],"admin"))*/ include_once SERVER."local/admin/includes/ajax.php"; // Find a way to limit this
	// Local
	include_once SERVER."local/includes/ajax.php";
	
	// Core - admin
	/*if(strstr($_SESSION['urls'][0],"admin"))*/ include_once SERVER."core/core/admin/includes/ajax.php"; // Find a way to limit this
	// Core
	include_once SERVER."core/core/includes/ajax.php";
	
	// Exit
	exit;
}

/************************************************************************************/
/************************************** Process *************************************/
/************************************************************************************/
// Logout
if($_GET['sef'] == "logout") {
	$_POST['process_action'] = "logout";
	$_REQUEST['process_action'] = "logout";
}
// Google Checkout - no way to add query string to URL it notifies, so just have to catch 'serial-number'
if($_POST['serial-number'] or $_GET['serial-number']) {
	$_POST['process_module'] = "payments";
	$_POST['process_action'] = "payments_googlecheckout_notify";
	$_REQUEST['process_module'] = "payments";
	$_REQUEST['process_action'] = "payments_googlecheckout_notify";
}

// Process
if($_REQUEST['process_action']) {
	// Speed
	debug_speed('process','core - dawn');
	
	// Local
	include_once SERVER."local/includes/process.php";
	// Core
	include_once SERVER."core/core/includes/process.php";
}

/************************************************************************************/
/*************************************** Page ***************************************/
/************************************************************************************/
include_once SERVER."core/core/includes/page.php";

/************************************************************************************/
/*************************************** Admin **************************************/
/************************************************************************************/
if($page->area == "admin") {
	// Dawn
	require_once SERVER."core/core/admin/includes/dawn.php";
}
?>