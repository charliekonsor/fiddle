<?php
/************************************************************************************/
/*************************************** Dawn ***************************************/
/************************************************************************************/
/** Dawn - loads core functionality */
require_once "includes/dawn.php";

/**
 * Example: http://www.mywebsite.com/core/core/download.php?file=http://www.mywebsite.com/uploads/photos/o/test.jpg
 * You should run urlencode() on the 'file' value.
 */
$file = file::load(urldecode($_GET['file']));
$file->download();
?>