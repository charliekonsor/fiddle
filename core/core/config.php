<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
/**
 * An array of core global congiguraiton values
 *
 * These values will be stored in the $__GLOBAL array under the 'config' parent so you could get our database name by calling $__GLOBAL['config']['db']['mysql']['database'].
 * We also provide a simple g() function to access the global so you could also get the database name by calling g('config.db.mysql.database');
 *
 * Note, that we separate different levels of the config array with periods. If one of the level's keys contains a period, you can access it by escaping it with a preceding forward slash (\) so, if we had the configuraiton $config = array('colors' => array('admin.h1' => 'fff','admin.h2' => '000')) we could access it like this: g('config.colors.admin\.h1');
 */
$config = array(
	'urls' => array(
		'query' => 0, // User 'query string' URLs instead of SEF URL's. May want to set this to 1 (true) in /local/config.php if your server doesn't allow URL rewriting
	),
	'register' => array(
		'email' => array(
			'unique' => 1
		),
		'username' => array(
			'regex' => '[a-zA-Z0-9_-]'
		)
	),
	'login' => array(
		'active' => true
	),
	'cookie' => array(
		'path' => '/'
	),
	'meta' => array(
		'maxlength' => 200,
		'keywords' => array(
			'auto' => array(
				'active' => 1,
				'count' => 15
			)
		)
	),
	'links' => array(
		'external' => array(
			'new' => 1, // Open external links in a new window
		)
	),
	'mail' => array(
		'class' => 'phpmailer',
		'smtp' => array(
			'active' => 0,
			'port' => '', // Port to send via, defaults to 25 in phpmailer class
			'secure' => NULL, // Security to use when sending e-mails via SMTP (optional): tls, ssl
			'host' => '', // Ex: mail.mydomain.com
			'username' => '',
			'password' => '',
		),
		'dkim' => array(
			'active' => 0,
			'domain' => '', // Will automatically get set to website's domain in email()
			'private' => SERVER.".htkeyprivate", // Path to DKIM private key
			'selector' => 'mail', // DKIM selector
			'password' => '', // DKIM password
		),
		'mailgun' => array( // Requires that composer is installed: https://github.com/mailgun/mailgun-php. I have a /composer/ folder in /kraken-extras/libraries/ which contains all this which you can upload to /core/core/libraries/ (if you upload it some place else, you must update the composer => path value in the config array.
			'domain' => '',
			'api' => array(
				'key' => '',
				'public_key' => '',
			),
		),
		'mandrill' => array( // Ifr you want to use SMTP with Mandrill, simply use the 'phpmailer' class (defaualt) and setup the SMTP configuration (see above)
			'api' => array(
				'key' => '',
			),
		),
	),
	'uploads' => array(
		'types' => array(
			'file' => array(
				'path' => 'local/uploads/files/',
				'hash' => 0, // Create random string 'hash' for file name instead of using original name
			),
			'image' => array(
				'path' => 'local/uploads/images/o/',
				'hash' => 0, // Create random string 'hash' for file name instead of using original name
				'accept' => array(
					'jpg',
					'jpeg',
					'gif',
					'png',
					'bmp'
				),
				'thumbs' => array( // Set this to 0 or array() in a module's config.php file (or an input's 'file' => 'thumbs' value) to disable thumbs for that module/input.
					't' => array(
						'path' => 'local/uploads/images/t/',
						'width' => 50,
						'height' => 38,
						'crop' => 1,
						'enlarge' => 1,
						'quality' => 80,
						//'storage' => 'local', // It'll inherit the original elements 'storage' service, but if you wanted to store this thumb (or others) somewhere else, you can define where here
					),
					's' => array(
						'path' => 'local/uploads/images/s/',
						'width' => 100,
						'height' => 75,
						'crop' => 1,
						'enlarge' => 1,
						'quality' => 80
					),
					'm' => array(
						'path' => 'local/uploads/images/m/',
						'width' => 250,
						'height' => 250,
						'quality' => 80
					),
					'l' => array(
						'path' => 'local/uploads/images/l/',
						'width' => 600,
						'height' => 600,
						'quality' => 80
					)
				),
			),
			'video' => array(
				'path' => 'local/uploads/videos/original/',
				'hash' => 0, // Create random string 'hash' for file name instead of using original name
				'accept' => array(
					'mov',
					'avi',
					'mp4',
					'flv',
					'ogg',
					'webm',
					'wmv'
				),
				'thumbs' => array(
					't' => array(
						'path' => 'local/uploads/videos/thumbs/t/',
						'width' => 50,
						'height' => 38,
						'crop' => 1,
						'enlarge' => 1,
						'quality' => 80,
						'count' => 4,
						'extension' => 'jpg',
						//'storage' => 'local', // It'll inherit the original elements 'storage' service, but if you wanted to store this thumb (or others) somewhere else, you can define where here
					),
					's' => array(
						'path' => 'local/uploads/videos/thumbs/s/',
						'width' => 100,
						'height' => 75,
						'crop' => 1,
						'enlarge' => 1,
						'quality' => 80,
						'count' => 4,
						'extension' => 'jpg',
					),
					'm' => array(
						'path' => 'local/uploads/videos/thumbs/m/',
						'width' => 250,
						'height' => 250,
						'quality' => 80,
						'count' => 4,
						'extension' => 'jpg',
					),
					'l' => array(
						'path' => 'local/uploads/videos/thumbs/l/',
						'width' => 600,
						'height' => 600,
						'quality' => 80,
						'count' => 4,
						'extension' => 'jpg',
					),
					'o' => array(
						'path' => 'local/uploads/videos/thumbs/o/',
						'count' => 4,
						'extension' => 'jpg',
					)
				),
				'extensions' => array(
					/*'flv' => array( // SWF's that play flv's can also play mp4s usually so we only really need that extension
						'extension' => 'flv',
						'path' => 'local/uploads/videos/flv/'
					),*/
					'mp4' => array( // This array of configuration values gets passed to $file->video_convert() so, for example, we could set 'resize' => 1,'width' => 100,'height' => 75 to resize the converted video
						'extension' => 'mp4',
						'path' => 'local/uploads/videos/mp4/',
						//'storage' => 'local', // It'll inherit the original elements 'storage' service, but if you wanted to store this extension (or others) somewhere else, you can define where here
					),
					/*'mobile' => array( // Example of using a non-file extension key for the extension and passing 'mobile' => 1 (creates mobile friendly file format)
						'extension' => 'mp4',
						'path' => 'local/uploads/videos/mobile/',
						'mobile' => 1,
					),*/
				)
			),
			'audio' => array(
				'path' => 'local/uploads/audio/',
				'hash' => 0, // Create random string 'hash' for file name instead of using original name
				'accept' => array(
					'mp3'
				)
			),
		),
		'storage' => array(
			'amazons3' => array(
				'login' => array(
					'key' => '', // Amazon Access Key ID
					'secret' => '', // Amazon Secret Access Key
				),
				'bucket' => '', // Bucket in which you want to store files. MUST BE UNIQUE ON YOUR SHARED STORAGE SERVER. SETUP THROUGH AMAZON AWS CONSOLE: https://console.aws.amazon.com/s3/home
			),
			'brightcove' => array(
				'login' => array(
					'token_read' => '', // Bright Cove Read Token
					'token_write' => '', // Bright Cove Write Token
				),
				'player' => array(
					'id' => '', // Bright Cove playerID value to use. Defaults to 876414349001 in file_brightcove.php
					'key' => '', // Bright Cove playerKey value to use. Defaults to AQ~~,AAAAkVHRxYE~,tk4-uogNPTJb1uDgVy4MlZVIDHkN_j7Z in file_brightcove.php
				),
			),
			'rackspace' => array(
				'login' => array(
					'username' => '', // Rackspace Cloud Username
					'api_key' => '', // Rackspace Cloud API Key: http://www.rackspace.com/knowledge_center/article/rackspace-cloud-essentials-1-viewing-and-regenerating-your-api-key
				),
				'container' => '', // Rackspace Cloud Files container you created
				'container_region' => '', // Rackspace region code for the container you created: ORD, DFW, SYD
				'container_url' => '', // Rackspace CDN URL for your container (must be publishing container to CDN, see container's setting). You can get the URL by visiting the Cloud Files dashboard, adding a file, and viewing it (just remove file name from URL).
			),
			'vimeo' => array( // Note, using OAuth 2 API: https://developer.vimeo.com/api/endpoints, https://github.com/vimeo/vimeo.php
				'login' => array(
					'client_id' => '', // Vimeo Client / Application ID
					'client_secret' => '', // Vimeo Client / Application Secret
					'access_token' => '', // Vimeo Access Token
				),
			),
		),
	),
	'ckeditor' => array(
		'path' => array(
			'base' => 'local/uploads/content/',
			'thumbs' => 'thumbs',
			'files' => 'files',
			'images' => 'images',
			'flash' => 'flash'
		)
	),
	'cache' => array(
		'active' => 1,
		'path' => SERVER.'local/cache/',
		'method' => 'file', // file or db (about 15% slower overall, but might be better for larger sites...might have some issue with db encoding at some point too)
	),
	'minify' => array( // Really, groups things together and (optionally) minifies them
		'javascript' => array(
			'active' => 1,
			'domain' => array(), // An array of domains (including http) to grab and minify. The website's domain will automatically be included.
		),
		'css' => array(
			'active' => 1,
			'minify' => 1, // Do we actually want to 'minify' code or just collect it (if you're using SASS and compile with minification, it'll be a lot faster to not minify here (as it's already minified)
			'domain' => array(),
		),
	),
	'cart' => array(
		'active' => 0,
		'shipping' => array(
			'companies' => array(
				'usps' => array(
					'username' => '229DARKW7858',
				),
				'ups' => array(
					'access' => '0C2D05F55AF310D0',
					'username' => 'dwstudios',
					'password' => 'dwstudios',
					'account' => '81476R',
				),
				'fedex' => array(
					'key' => 'SbVSL1BvDdAiVWD7',
					'password' => 'WGrAvRgBMkTeNM7v9JBY4dnsJ',
					'account' => '332304372',
					'meter' => '104139463',
					'wsdl' => SERVER.'core/core/classes/shipping/RateService_v10.wsdl',
				)
			),
			'boxes' => array(
				/*array( // Example of defining box size(s)
					'dimensions' => array( // Inches
						40,
						40,
						20,
					),
					'weight' => 1120, // Ounces (1120ozs = 70lbs)
				),*/
			),
		),
		'tax' => array(
			'active' => 0,
			'rates' => array(
				// An arrray of statcode => rate would go here, ex: OR => .0725
			)
		),
		'handling' => array(
			'active' => 0,
			'price' => 0.00, // Handling charge
			'charge' => 'cart', // When to charge: cart (once per cart), item (once per item), quantity (once per quantity)
		),
	),
	'api' => array(
		'active' => 0,
		'format' => 'json', // json or xml
		'permission' => 0, // Whether or not we check if user has permission to perform an action (requires they login via /api/login and pass their 'token' with all subsequent calls).
		'keys' => array(
			// An array of keys that can be used to access this API (if none set, anyone can access it)
		)
	),
	'debug' => array(
		'active' => true,
		'ips' => array(
			'70.99.206.122', // Work
			'24.22.122.203', // Home - Comcast
			'75.175.103.192', // Homne - CenturyLink
			'::1', // localhost
			'127.0.0.1', // localhost
			'192.168.10.178', // My work computers apache folder
			'192.168.10.134', // Work?
			'10.0.2.2', // Vagrant/homestead site
		),
		'email' => 'charlie@dwstudios.net',
		'minify' => array(
			'active' => 1,
		),
		'cache' => array(
			'active' => 1,
		),
	),
	'include' => array(
		'function' => array(
			'email' => 'core/core/functions/email.php',
			'xml' => 'core/core/functions/xml.php',
			'xls' => 'core/core/functions/xls.php',
			'csv' => 'core/core/functions/csv.php',
			'table' => 'core/core/functions/table.php',
			'zip' => 'core/core/functions/zip.php',
			'pdf' => 'core/core/functions/pdf.php',
			'curl' => 'core/core/functions/curl.php',
		),
		'class' => array(
			'shipping' => array(
				'core/core/classes/shipping/shipping.php',
				'core/core/classes/shipping/shipping_usps.php',
				'core/core/classes/shipping/shipping_ups.php',
				'core/core/classes/shipping/shipping_fedex.php',
			),
			'packer' => 'core/core/classes/shipping/packer.php',
			'payment' => 'core/core/classes/payment/payment.php',
			'payment_authorize' => 'core/core/classes/payment/payment_authorize.php',
			'payment_chargebackguardian' => 'core/core/classes/payment/payment_chargebackguardian.php',
			'payment_firstdata' => 'core/core/classes/payment/payment_firstdata.php',
			'payment_metricsglobal' => 'core/core/classes/payment/payment_metricsglobal.php',
			'payment_payflowpro' => 'core/core/classes/payment/payment_payflowpro.php',
			'payment_paypal_pro' => 'core/core/classes/payment/payment_paypal_pro.php',
			'payment_virtualmerchant' => 'core/core/classes/payment/payment_virtualmerchant.php',
			'payment_paypal' => 'core/core/classes/payment/payment_paypal.php',
			'payment_paypal_advanced' => 'core/core/classes/payment/payment_paypal_advanced.php',
			'payment_googlecheckout' => 'core/core/classes/payment/payment_googlecheckout.php',
			'payment_qbms' => 'core/core/classes/payment/payment_qbms.php',
			'social_facebook' => array(
				'core/core/libraries/facebook/facebook.php',
				'core/core/classes/social/social_facebook.php',
				'core/framework/classes/social/social_facebook.php',
			),
			'geo' => 'core/core/classes/geo.php',
			'aws' => 'core/core/libraries/aws/aws.phar',
		),
		'javascript' => array(
			'jquery.metadata' => array(
				'files' => array(
					'core/core/js/jquery.metadata.js',
				),
				'core' => 1,
			),
			'json' => array(
				'files' => array(
					'core/core/js/json.js',
				),
				'core' => 1,
			),
			'jquery.validation' => array(
				'files' => array(
					'core/core/js/jquery.validation.js',
				),
				'regex' => '/class=\'([^\']*?)require|class="([^"]*?)require/', // Any class='require'
			),
			/*'jquery.fancybox' => array( // We use 'FancyApps' instead (see below)
				'files' => array(
					'core/core/js/jquery.fancybox.js',
				),
			),*/
			'jquery.fancyapps.fancybox' => array(
				'files' => array(
					'core/core/js/jquery.fancyapps.fancybox.js',
				),
				'regex' => '/class=\'([^\']*?)overlay|class="([^"]*?)overlay|\.fancybox\(/', // Any class='overlay' or $().fancybox()
			),
			'jquery.fancyapps.fancybox.thumbs' => array(
				'files' => array(
					'core/core/js/jquery.fancyapps.fancybox.thumbs.js',
				),
			),
			'jquery.fancyapps.fancybox.buttons' => array(
				'files' => array(
					'core/core/js/jquery.fancyapps.fancybox.buttons.js',
				),
			),
			'jquery.fancyapps.fancybox.media' => array(
				'files' => array(
					'core/core/js/jquery.fancyapps.fancybox.media.js',
				),
			),
			'jquery.tooltip' => array(
				'files' => array(
					'core/core/js/jquery.tooltip.js',
				),
				'regex' => '/class=\'([^\']*?)tooltip|class="([^"]*?)tooltip|\.tooltip\(/', // Any class='tooltip' or $().tooltip()
			),
			'jquery.datepicker' => array(
				'files' => array(
					'core/core/js/jquery.datepicker.js',
				),
			),
			'jquery.tools.dateinput' => array(
				'files' => array(
					'core/core/js/jquery.tools.dateinput.js',
				),
			),
			'jquery.tools.overlay' => array(
				'files' => array(
					'core/core/js/jquery.tools.overlay.js',
				),
			),
			'jquery.tools.tooltip' => array(
				'files' => array(
					'core/core/js/jquery.tools.tooltip.js',
				),
			),
			'jquery.tools.validator' => array(
				'files' => array(
					'core/core/js/jquery.tools.validator.js',
				),
			),
			'ckeditor' => array(
				'files' => array(
					'core/core/libraries/ckeditor/ckeditor.js',
				),
			),
			'swfupload' => array(
				'files' => array(
					'core/core/libraries/flash/swfupload/swfupload.js',
					'core/core/js/jquery.swfupload.js',
					'core/core/js/jquery.swfupload.framework.js',
				),
			),
			'jquery.raty' => array(
				'files' => array(
					'core/core/js/jquery.raty.js'
				),
			),
			'jquery.ui' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.draggable' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.draggable.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.droppable' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.draggable.min.js',
					'core/core/js/jquery.ui.droppable.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.resizable' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.resizable.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.selectable' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.selectable.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.sortable' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.tablednd.js',
					'core/core/js/jquery.ui.sortable.min.js',
					'core/core/js/jquery.ui.sortable.framework.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.nested' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.tablednd.js',
					'core/core/js/jquery.ui.sortable.min.js',
					'core/core/js/jquery.ui.sortable.framework.js',
					'core/core/js/jquery.ui.nested.js',
					'core/core/js/jquery.ui.nested.framework.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.autocomplete' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.position.min.js',
					'core/core/js/jquery.ui.autocomplete.min.js',
					'core/core/js/jquery.ui.menu.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.accordion' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.accordion.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.button' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.button.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.datepicker' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.datepicker.min.js',
					'core/core/js/jquery.ui.widget.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.dialog' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.position.min.js',
					'core/core/js/jquery.ui.draggable.min.js',
					'core/core/js/jquery.ui.resizable.min.js',
					'core/core/js/jquery.ui.button.min.js',
					'core/core/js/jquery.ui.dialog.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.menu' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.position.min.js',
					'core/core/js/jquery.ui.menu.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.progressbar' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.progressbar.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.slider' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.slider.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.spinner' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.button.min.js',
					'core/core/js/jquery.ui.spinner.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.tabs' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.tabs.min.js',
					'core/core/js/jquery.ui.mouse.min.js', // Required for 'jquery.ui.touch' to work, throws error otherwise
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.tooltip' => array(
				'files' => array(
					'core/core/js/jquery.ui.core.min.js',
					'core/core/js/jquery.ui.widget.min.js',
					'core/core/js/jquery.ui.mouse.min.js',
					'core/core/js/jquery.ui.position.min.js',
					'core/core/js/jquery.ui.touch.min.js',
				),
			),
			'jquery.ui.effects' => array(
				'files' => array(
					'core/core/js/jquery.ui.effects.min.js',
				),
			),
			'jquery.flash' => array(
				'core/core/js/jquery.flash.js'
			),
			'videojs' => array(
				'files' => array(
					'core/core/libraries/flash/videojs/video.min.js',
				),
			),
			'jwplayer' => array(
				'files' => array(
					'core/core/libraries/flash/jwplayer/jwplayer.js',
				),
			),
			'jplayer' => array(
				'files' => array(
					'core/core/libraries/flash/jplayer/jquery.jplayer.min.js',
				),
			),
			'mediaelements' => array(
				'files' => array(
					'core/core/libraries/flash/mediaelements/mediaelement-and-player.min.js',
				),
			),
			'projekktor' => array(
				'files' => array(
					'core/core/libraries/flash/projekktor/projekktor.min.js',
				),
			),
			'colorpicker' => array(
				'files' => array(
					'core/core/js/jquery.minicolors.js',
					'core/core/js/jquery.minicolors.framework.js',
				),
			),
			'quicksearch' => array(
				'files' => array(
					'core/core/js/jquery.quicksearch.js',
					'core/core/js/quicksilver.js',
					'core/core/js/jquery.quicksearch.framework.js',
				),
			),
			'highcharts' => array(
				'files' => array(
					'core/core/libraries/highcharts/highcharts.js',
				),
			),
			'plupload' => array(
				'files' => array(
					'core/core/libraries/plupload/browserplus-min.js',
					'core/core/libraries/plupload/plupload.full.js',
					'core/core/libraries/plupload/jquery.plupload.queue/jquery.plupload.queue.js',
				),
			),
		),
		'css' => array(
			'jquery.validation' => array(
				'files' => array(
					'core/core/css/jquery.validation.css',
				),
				'regex' => '/class=\'([^\']*?)require|class="([^"]*?)require/', // Any class='require'
			),
			/*'jquery.fancybox' => array( // We use 'FancyApps' instead (see below)
				'files' => array(
					'core/core/css/jquery.fancybox.css',
				),
			),*/
			'jquery.fancyapps.fancybox' => array(
				'files' => array(
					'core/core/css/jquery.fancyapps.fancybox.css',
				),
				'regex' => '/class=\'([^\']*?)overlay|class="([^"]*?)overlay|\.fancybox\(/', // Any class='overlay' or $().fancybox()
			),
			'jquery.fancyapps.fancybox.thumbs' => array(
				'files' => array(
					'core/core/css/jquery.fancyapps.fancybox.thumbs.css',
				),
			),
			'jquery.fancyapps.fancybox.buttons' => array(
				'files' => array(
					'core/core/css/jquery.fancyapps.fancybox.buttons.css',
				),
			),
			'jquery.fancyapps.fancybox.media' => array(
				'files' => array(
					'core/core/css/jquery.fancyapps.fancybox.media.css',
				),
			),
			'jquery.tooltip' => array(
				'files' => array(
					'core/core/css/jquery.tooltip.css',
				),
				'regex' => '/class=\'([^\']*?)tooltip|class="([^"]*?)tooltip|\.tooltip\(/', // Any class='tooltip' or $().tooltip()
			),
			'jquery.datepicker' => array(
				'files' => array(
					'core/core/css/jquery.datepicker.css',
				),
			),
			'jquery.tools.dateinput' => array(
				'files' => array(
					'core/core/css/jquery.tools.dateinput.css',
				),
			),
			'jquery.tools.overlay' => array(
				'files' => array(
					'core/core/css/jquery.tools.overlay.css',
				),
			),
			'jquery.tools.tooltip' => array(
				'files' => array(
					'core/core/css/jquery.tools.tooltip.css',
				),
			),
			'jquery.tools.validator' => array(
				'files' => array(
					'core/core/css/jquery.tools.validator.css',
				),
			),
			'swfupload' => array(
				'files' => array(
					'core/core/css/jquery.swfupload.css',
				),
			),
			'jquery.ui' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
				),
			),
			'jquery.ui.draggable' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
				),
			),
			'jquery.ui.droppable' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
				),
			),
			'jquery.ui.resizable' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.resizable.min.css',
				),
			),
			'jquery.ui.selectable' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.selectable.min.css',
				),
			),
			'jquery.ui.sortable' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
				),
			),
			'jquery.ui.nested' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.nested.css',
				),
			),
			'jquery.ui.autocomplete' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.autocomplete.min.css',
					'core/core/css/jquery.ui.menu.min.css',
				),
			),
			'jquery.ui.accordion' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.accordion.min.css',
				),
			),
			'jquery.ui.button' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.button.min.css',
				),
			),
			'jquery.ui.datepicker' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.datepicker.min.css',
					'core/core/css/jquery.ui.datepicker.framework.css',
				),
			),
			'jquery.ui.dialog' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.resizable.min.css',
					'core/core/css/jquery.ui.button.min.css',
					'core/core/css/jquery.ui.dialog.min.css',
				),
			),
			'jquery.ui.menu' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.menu.min.css',
				),
			),
			'jquery.ui.progressbar' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.progressbar.min.css',
				),
			),
			'jquery.ui.slider' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.slider.min.css',
				),
			),
			'jquery.ui.spinner' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.button.min.css',
					'core/core/css/jquery.ui.spinner.min.css',
				),
			),
			'jquery.ui.tabs' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
					'core/core/css/jquery.ui.tabs.min.css',
				),
			),
			'jquery.ui.tooltip' => array(
				'files' => array(
					'core/core/css/jquery.ui.core.min.css',
					'core/core/css/jquery.ui.theme.min.css',
				),
			),
			'videojs' => array(
				'files' => array(
					'core/core/libraries/flash/videojs/video-js.min.css',
				),
			),
			'mediaelements' => array(
				'files' => array(
					'core/core/libraries/flash/mediaelements/mediaelementplayer.css',
				),
			),
			'projekktor' => array(
				'files' => array(
					'core/core/libraries/flash/projekktor/themes/maccaco/projekktor.style.css',
				),
			),
			'colorpicker' => array(
				'files' => array(
					'core/core/css/jquery.minicolors.css',
				),
			),
			'quicksearch' => array(
				'files' => array(
					'core/core/css/jquery.quicksearch.css',
				),
			),
			'plupload' => array(
				'files' => array(
					'core/core/libraries/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css',
					'core/core/css/plupload.framework.css',
				),
			),
		),
	),
	'localization' => array(
		'time' => array(
			'timezone' => 'America/Los_Angeles'
		),
	),
	'db' => array(
		'cached' => 1,
		'cache' => 1,
		'debug' => array(
			'count' => 1,
			'queries' => 1,
		),
	),
	'social' => array(
		'facebook' => array(
			'active' => 0,
			'app' => array(
				'id' => '',
				'secret' => '',
			),
			//'scope' => '', // Could update the default scope we want to get: http://hybridauth.sourceforge.net/userguide/IDProvider_info_Facebook.html
		),
		'twitter' => array( // Note, if you're having trouble with twitter, make sure the timestamp is correct on your server. If it's wrong, Twitter returns 401 Unauthorized (Failed to validate oauth signature and token). This helped: http://www.cyberciti.biz/tips/synchronize-the-system-clock-to-network-time-protocol-ntp-under-fedora-or-red-hat-linux.html
			'active' => 0,
			'app' => array(
				'key' => '',
				'secret' => '',
			)
		),
	),
	'composer' => array(
		'path' => "core/core/libraries/composer/",
	),
);

?>