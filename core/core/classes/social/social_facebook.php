<?php
if(!class_exists('facebook_core',false)) {
	/**
	 * Loads social_facebook class (which extends the Official Facebook PHP class).
	 *
	 * @package\social\facebook
	 */
	class social_facebook extends Facebook {
		/** Stores the instance of the Facebook PHP class */
		//var $facebook;
		/** Holds the OAuth token for the current session. */
		//var $token;
		/** Holds the signed request for the current session. */
		//var $request;
		
		/**
		 * Loads and returns an instance of the facebook_core class, using module or framework level classes if they exist.
		 *
		 * Example:
		 * - $facebook = social_facebook::load($module,$id,$c);
		 *
		 * @param string $module The module this item is in. Only applies to framework and module level classes.
		 * @param int The id of the item. Only applies to framework and module level classes.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the core facebook class or (if it exists) the framework facebook class or (if it exists) the module specific facebook class.
		 */
		static function load($module = NULL,$id = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Params
			if(!$module or is_array($module)) { // facebook_core::load($c)
				$c = $module;
				$module = NULL;
				$id = NULL;	
			}
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = "social_facebook_framework";
				$params = func_get_args(); // Must be outside function call
				$class_load = load_class_module($class_framework,$params,$module,$c[type]); // No way to instanitiate class with array of params so we'll just return the class name and...
				if($class_load) return new $class_load($module,$id,$c); // ...manually pass the params here
			}
			
			// Core class
			return new $class($c);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param array $c An array of configuration values.
		 */
		function __construct($c = NULL) {
			self::social_facebook($c);
		}
		function social_facebook($c = NULL) {
			// Config
			if(!x($c[app_id])) $c[app_id] =  g('config.social.facebook.app.id'); // App ID
			if(!x($c[app_secret])) $c[app_secret] = g('config.social.facebook.app.secret'); // App secret
			if(!x($c[upload])) $c[upload] = 0; // Are we going to use this facebook instance to upload files?
			if(!x($c[token])) $c[token] = NULL; // OAuth token of previous session we want to load
			
			// Parent
			$params = array(
				'appId' => $c[app_id],
				'secret' => $c[app_secret],
				'fileUpload' => ($c[upload] ? true : false)
			);
			parent::__construct($params);
			
			// Token
			if($c[token]) {
				$this->token($c[token]);
			}
			
			// Request - save for later as the Facebook PHP classes doesn't save it to session
			Facebook::$kSupportedKeys[] = 'signed_request';
			if($_REQUEST['signed_request']) {
				$this->setPersistentData('signed_request',$this->request());
			}
		}
		
		/**
		 * Returns or (if $token passed) sets token for current session.
		 *
		 * @param string $token The OAuth token of a previous session we want to load. Default = NULL
		 * @return string The OAuth token of the current session.
		 */
		function token($token = NULL) {
			// Set token
			if($token) $this->setAccessToken($token);	
			
			// Get token
			return $this->getAccessToken();
		}
		
		
		/**
		 * Returns original signed request array that was passed to us when user first accessed our app.
		 *
		 * Note, if you pass a $key, it'll return just that value, not the full array.
		 * Key can be a period separated multi-level key (ex: page.id would return $request[page][id]).
		 *
		 * @param string $key The key of the value in the signed request array you want to return. Default = NULL
		 * @return array|mixed Either the full signed request array or (if $key passed) the specific value corresponding to the $key.
		 */
		function request($key = NULL) {
			// Array
			$request = $this->getSignedRequest();
			if(!$request) $request = $this->getPersistentData('signed_request');
			
			// Return
			if($key) return array_eval($request,$key);
			else return $request;
		}
		
		/**
		 * Returns the URL to send the user to so they can login.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL where the user can login to their facebook account.
		 */
		function login_url($c = NULL) {
			// Config
			if(!x($c[permissions])) $c[permissions] = NULL; // A comma separated list of extra permissions to get: https://developers.facebook.com/docs/reference/login/#permissions
			if(!x($c[redirect])) $c[redirect] = NULL; // URL to redirect to after logging in. Defaults to current URL
			
			// Params
			$params = NULL;
			if($c[permissions]) $params[scope] = $c[permissions];
			if($c[redirect]) $params[redirect_uri] = $c[redirect];
			
			// Get URL
			return $this->getLoginUrl($params);
		}
		
		/**
		 * Returns the URL to send the user to so they can logout.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL where the user can log out of their facebook account.
		 */
		function logout_url($c = NULL) {
			// Config
			if(!x($c[redirect])) $c[redirect] = NULL; // URL to redirect to after logging out.
			
			// Params
			$params = NULL;
			if($c[redirect]) $params[next] = $c[redirect];
			
			// Get URL
			return $this->getLogoutUrl($params);
		}
		
		/**
		 * Makes a call to the Facebook Graph API.
		 *
		 * Simplifies the Facebook PHP classes api() method by putting method and values into an array.										
		 * 
		 * @param string $call The call you want to make to the api.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The results returned by the API call.
		 */
		function api($call,$c = NULL) {
			// Config
			if(!x($c[values])) $c[values] = NULL; // An array of values, used when posting data
			if(!x($c[method])) $c[method] = ($c[values] ? "POST" : "GET"); // Method, used when posting data (ex: POST)
			
			// Add / to beginning
			if(substr($call,0,1) != "/") $call = "/".$call;
			
			// Call
			$results = parent::api($call,$c[method],$c[values]);
			
			// Error
			if($results[error]) {
				// Need to login
				if($results[error][code] == 190) {
					$results[error][redirect] = $this->login_url();
				}
				// Need to authorize this action
				if($results[error][code] == 200) {
					$results[error][redirect] = $this->login_url(array('permissions' => 'publish_stream')); // May want to check what the 'call' was to determine the needed scope
				}
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Returns the id of the facebook user this visitor is currently logged in as.
		 * 
		 * @return int The id of the facebook user this visitor is currently logged in as.
		 */
		function user() {
			// Return
			return $this->getUser();
		}
	}
}
?>