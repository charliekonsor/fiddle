<?php
if(!class_exists('form_input_date',false)) {
	/**
	 * Creates a date input, using a javascript calendar for selecting the date.
	 */
	class form_input_date extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_date($label,$name,$value,$c);
		}
		function form_input_date($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Legacy - used to be $c[date]
			if($c[date]) {
				foreach($c[date] as $k => $v) $c[$k] = $v;
			}		
			
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[format]) $this->c[format] = "date";
			if(!$this->c[year_min]) $this->c[year_min] = 1900;
			if(!$this->c[year_max]) $this->c[year_max] = date('Y') + 100;
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function html_element($form,$c = NULL) {
			// Time
			$time = 0;
			if($this->value) $time = time_format($this->value,'unix');
			// Value (in mm/dd/yyyy format)
			if($time) $this->value = adodb_date('m/d/Y',$time);
			else $this->value = NULL;
			// Type - maybe in future we can use type=date, but not well enough supported yet (only Opera 9.5+ and Chrome 20+). We'll use class='date' to apply datepicker for now.
			$this->type = "text";
			
			// Class
			$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."date";
			// Years
			$years = "yearRange:\"".$this->c[year_min].":".$this->c[year_max]."\"";
			if(strstr($this->attributes['class'],' {')) $this->attributes['class'] = str_replace(' {',' {'.$years.',',$this->attributes['class']);
			else $this->attributes['class'] .= " {".$years."}";
			
			// Input
			$html = "
		".include_css('jquery.ui.datepicker')."
		".include_javascript('jquery.ui.datepicker')."
		<input ".$this->attributes($form)." />";	
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Value
			$value = parent::process($form,$c);
			
			// Format
			$value = time_format($value,$this->c[format]);
			
			// Return
			return $value;
		}
	}
}
?>