<?php
if(!class_exists('form_input_rating',false)) {
	/**
	 * Creates a rating input, using javascript/images to allow for rating via stars.
	 *
	 * Uses the 'Raty' jquery plugin: http://wbotelhos.com/raty
	 */
	class form_input_rating extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_rating($label,$name,$value,$c);
		}
		function form_input_rating($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[stars]) $this->c[stars] = 5; // Number of stars we're rating against
			if(!x($this->c[half])) $this->c[half] = 0; // Ability to choose 'half' stars
			if(!x($this->c[cancel])) $this->c[cancel] = 0; // Ability to canel/clear rating
			if(!$this->c[images][star_on]) $this->c[images][star_on] = DOMAIN."core/core/images/raty/star-on.png";
			if(!$this->c[images][star_off]) $this->c[images][star_off] = DOMAIN."core/core/images/raty/star-off.png";
			if(!$this->c[images][star_half]) $this->c[images][star_half] = DOMAIN."core/core/images/raty/star-half.png";
			if(!$this->c[images][cancel_on]) $this->c[images][cancel_on] = DOMAIN."core/core/images/raty/cancel-on.png";
			if(!$this->c[images][cancel_off]) $this->c[images][cancel_off] = DOMAIN."core/core/images/raty/cancel-off.png";
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function html_element($form,$c = NULL) {
			// Random #
			$r = random();
			
			// HTML
			$html = "
".include_javascript('jquery.raty')."
<div id='rating-".$r."'></div>
<script type='text/javascript'>
$(document).ready(function() {
	$('#rating-".$r."').raty({
		number:".$this->c[stars].",
		score:".($this->value ? $this->value : "null").",
		half:".$this->c[half].",
		scoreName:'".$this->name."',
		starHalf:'".$this->c[images][star_half]."',
		starOff:'".$this->c[images][star_off]."',
		starOn:'".$this->c[images][star_on]."',
		cancel:".$this->c[cancel].",
		cancelOff:'".$this->c[images][cancel_off]."',
		cancelOn:'".$this->c[images][cancel_on].".png'
	});
});
</script>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Value
			$value = parent::process($form,$c);
			
			// Format
			$value = time_format($value,$this->c[format]);
			
			// Return
			return $value;
		}
	}
}
?>