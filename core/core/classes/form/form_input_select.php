<?php
if(!class_exists('form_input_select',false)) {
	/**
	 * Creates a select input
	 */
	class form_input_select extends form_input_framework {
		/** Holds the array of options for this select box. */
		public $options = NULL; // Set to NULL (not array()) so we don't try to merge an empty array with an options class (string).
		/** Holds the standardized array of options. */
		public $options_standardized = NULL;
		
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$options = NULL,$value = NULL,$c = NULL) {
			$c[options] = $options;
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_select($label,$name,$value,$c);
		}
		function form_input_select($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Options
			$this->options_standardized = $this->options($this->options);
			
			// Attributes - multiple
			if($this->attributes[multiple]) {
				$this->attributes[multiple] = "multiple";
				if($this->name and substr($this->name,-2) == "[]") $this->name = substr($this->name,0,(strlen($this->name) - 2)); // Need so we get proper 'value' before creating HTML where we'll add it back?
				if(!strstr($this->attributes['class'],'form-input-select-multiple')) {
					$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."form-input-select-multiple";
				}
			}
			else unset($this->attributes[multiple]); // If we passed this, even if NULL, it'll get added as a blank attribute which browsers interpret as a multiple input
			
			// Config
			if(!x($this->c[options_blank])) $this->c[options_blank] = 0; // Include a 'blank' option at the beginnin (could also use $this->attributes[placeholder] = ' ';)
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// Multiple
			if($this->attributes[multiple]) {
				if($this->name and substr($this->name,-2) != "[]") $this->name .= "[]";
			}
			// Value
			if($this->value and $this->attributes[multiple]) {
				$value = data_unserialize($this->value);
				if(is_array($value)) $this->value = $value;
			}
			
			// Options
			$options = $this->options_standardized;
			
			// Placeholder
			if($this->attributes[placeholder] and $options) {
				$options_placeholder = array(array('label' => $this->attributes[placeholder]));
				$options = array_merge($options_placeholder,$options);
				unset($this->attributes[placeholder]);
			}
			
			// Options HTML
			$options_string = $this->options_html($form,$options,$this->value);
			if($options_string) {
				// Blank - auto // Not doing automatically, don't want to assume, must manually add in blank option via array('value' => '','label' => '') or new $this->c[options_blank] = 1 if we want a blank option at top of dropdown
				/*if(!strstr($options_string,"<option>") and !strstr($options_string,"value=''")) {
					$options_string = "<option value=''></option>".$options_string;
				}*/
				// Blank - manual
				if($this->c[options_blank]) {
					$options_string = "<option value=''></option>".$options_string;
				}
				// Select
				$html = "<select ".$this->attributes($form).">".$options_string."</select>";
			}
			// No options, remove 'required'
			else if($this->name and $this->validate[required]) {
				$this->validate[required] = 0;
				$form->input_replace($this->name,$this);
			}
			
			// Return
			return $html;
		}
		
		/**
		 * Returns a string of select option elements for the given array of option(s).
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $options The array of options to create inputs out of.
		 * @param string|array $value The current selected value. Default = NULL
		 * @param array $c An array of config values. Default = NULL
		 * @return string A string of HTML <input> elements.
		 */
		function options_html($form,$options,$value = NULL,$c = NULL) {
			// Config
			if(!$c[indent]) $c[indent] = 0; // Number of levels to indent this option.
			
			// Indent
			for($x = 0;$x < $c[indent];$x++) $indent .= "&nbsp;&nbsp;&nbsp;";
			
			// Child config
			$_c = $c;
			$_c[indent] += 1;
			
			// Value
			if(!is_array($value)) $value = (string) $value;
			
			// Options
			foreach($options as $option) {
				// Optgroup
				if($option[options] and !x($option[value])) {
					// String + children
					$options_html .= "<optgroup label='".string_encode($indent.$option[label],1)."'>".$this->options_html($form,$option[options],$value,$_c)."</optgroup>";
				}
				// Option
				else {
					// Value
					$option[value] = (string) $option[value];
					
					// Selected?
					if(x($value) and ($value === $option[value] or is_array($value) and in_array($option[value],$value))) {
						$option[selected] = 'selected';
					}
					// String
					$attributes = $form->attributes($option);
					$options_html .= "<option value='".string_encode($option[value])."'".($attributes ? " ".$attributes : "").">".$indent.$option[label]."</option>";
					
					// Children
					if($option[options]) {
						$options_html .= $this->options_html($form,$option[options],$value,$_c);
					}
				}
			}
			
			// Return
			return $options_html;
		}
	}
}
?>