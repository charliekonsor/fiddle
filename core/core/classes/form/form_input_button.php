<?php
if(!class_exists('form_input_button',false)) {
	/**
	 * Creates a button input
	 */
	class form_input_button extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($value = NULL,$c = NULL) {
			return parent::load(NULL,NULL,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_button($label,$name,$value,$c);
		}
		function form_input_button($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Attributes
			if(!$this->attributes[autocomplete]) $this->attributes[autocomplete] = "off"; // Prevent Firefox from 'autocompleting' button state to disabled when we press 'Back' in browser
		}
	}
}
?>