<?php
if(!class_exists('form_input_color',false)) {
	/**
	 * An extension of the form_input_framework class with functionality specific to color inputs.
	 */
	class form_input_color extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_color($label,$name,$value,$c);
		}
		function form_input_color($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);

			// Value - must prefix with '#'
			if($this->value and substr($this->value,0,1) != "#") $this->value = "#".$this->value;
			
			// Attributes - class
			$this->attributes['class'] .= ($this->attributes['class'] ? " " : "")."colorpicker";
			// Attributes - type - want to use the 'text' type on the input
			$this->attributes[type] = 'text';
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// Javascript / CSS - Include more than once (2nd param = 0) in case input type got tested in $module->settings() but not included in HTML
			$html .= "
".include_javascript('colorpicker',0)."
".include_css('colorpicker',0);
			
			// HTML
			$html .= "
".parent::html_element($form,$c);
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Parent
			$value = parent::process($form,$c);
				
			// Remove hash from beginning
			$value = str_replace('#','',$value);
			
			// Return
			return $value;
		}
	}
}
?>