<?php
if(!class_exists('form_input_textarea',false)) {
	/**
	 * Creates a text input
	 */
	class form_input_textarea extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_textarea($label,$name,$value,$c);
		}
		function form_input_textarea($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// HTML
			$html = "<textarea ".$this->attributes($form).">".$this->value."</textarea>";
			
			// Return
			return $html;
		}
	}
}
?>