<?php
if(!class_exists('form_input_password',false)) {
	/**
	 * Creates a password input
	 */
	class form_input_password extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_password($label,$name,$value,$c);
		}
		function form_input_password($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!x($this->attributes[autocomplete])) $this->attributes[autocomplete] = 'off'; // Prevent Firefox from 'autocompleting' password
		}
	}
}
?>