<?php
if(!class_exists('form_input_datetime',false)) {
	/**
	 * Creates a date & time input, including date selector functionality.
	 */
	class form_input_datetime extends form_input_framework {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_datetime($label,$name,$value,$c);
		}
		function form_input_datetime($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Legacy - used to be $c[date]
			if($c[date]) {
				foreach($c[date] as $k => $v) $c[$k] = $v;
			}		
		
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[format]) $this->c[format] = "datetime";
			if(!$this->c[year_min]) $this->c[year_min] = 1900;
			if(!$this->c[year_max]) $this->c[year_max] = date('Y') + 100;
			if(!$this->c[hours]) $this->c[hours] = 12; // What hour format do you want to use: 12 [default, includes AM/PM], 24
			if(!$this->c[minutes]) $this->c[minutes] = 1; // Increment between minutes (ex: $this->c[minutes] = 15; options = 00, 15, 30, 45)
		}
		
		/**
		 * Creates the HTML for an input
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML element for the input.
		 */
		function html_element($form,$c = NULL) {
			// Name
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Time
			$time = 0;
			if($this->value) $time = time_format($this->value,'unix');
			
			// Date
			$array = $this->attributes;
			$array[type] = "text";
			$array[name] = $this->name;
			// Date - class
			$array['class'] .= ($array['class'] ? " " : "")."date";
			// Date - years
			$years = "yearRange:\"".$this->c[year_min].":".$this->c[year_max]."\"";
			if(strstr($array['class'],' {')) $array['class'] = str_replace(' {',' {'.$years.',',$array['class']);
			else $array['class'] .= " {".$years."}";
			// Date - value
			if($time) $array[value] = adodb_date('m/d/Y',$time);
			else $array[value] = NULL;
			// Date - input
			$html = "
		".include_css('jquery.ui.datepicker')."
		".include_javascript('jquery.ui.datepicker')."
		<input ".$form->attributes($array)." />";
			
			// Hour
			$array = $this->attributes;
			$array[name] = $name."_hour";
			unset($array[id]);
			$value = NULL;
			if($this->c[hours] == 24) {
				$start = 0;
				$end = 23;
				if($time) $value = adodb_date('h',$time);
			}
			else {
				if($hour >= 12) {
					$meridiem = "pm";	
					if($hour > 12) $hour -= 12;
				}
				$start = 1;
				$end = 12;
				if($time) $value = adodb_date('g',$time);
			}
			// Hour
			$html .= "
		<select ".$form->attributes($array).">
			<option value=''>Hr</option>";
			for($x = $start;$x <= $end;$x++) $html .= "
			<option value='".$x."'".($time && $value == $x ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
			$html .= "
		</select> :";
		
			// Minute
			$array = $this->attributes;
			$array[name] = $name."_minute";
			unset($array[id]);
			$value = NULL;
			if($time) $value = adodb_date('i',$time);
			$html .= "
		<select ".$form->attributes($array).">
			<option value=''>Min</option>";
			for($x = 0;$x < 60;$x += $this->c[minutes]) $html .= "
			<option value='".$x."'".($time && $value == str_pad($x,2,0,STR_PAD_LEFT) ? " selected='selected'" : '').">".str_pad($x,2,0,STR_PAD_LEFT)."</option>";
			$html .= "
		</select>";
		
			// Meridiem
			if($this->c[hours] != 24) {
				$array = $this->attributes;
				$array[name] = $name."_meridiem";
				$value = NULL;
				if($time) $value = adodb_date('a',$time);
				$html .= "
		<select ".$form->attributes($array).">
			<option value='am'".($value == "am" ? " selected='selected'" : '').">AM</option>
			<option value='pm'".($value == "pm" ? " selected='selected'" : '').">PM</option>
		</select>";
			}
		
			$html .= "
		<label class='error form-error' for='".$this->name."'".($this->c[errors_inline] && $this->name && $this->errors[inline][$this->name] ? " style='display:inline;'>".$this->errors[inline][$this->name] : ">Please select a date and time.")."</label>";
			
			// Return
			return $html;
		}
			
		/**
		 * Processes the value for this input.
		 *
		 * @param object $form The form object which contains the submitted values ($form->post and $form->files).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return mixed The resulting value.
		 */
		function process($form,$c = NULL) {
			// Value
			$value = parent::process($form,$c);
			
			// Name (used for hour, minute ,and meridiem values)
			$name = str_replace(array('[',']'),'',$this->name);
			
			// Values
			$date = $value;
			$hour = $form->post[$name.'_hour'];
			$minute = str_pad($form->post[$name.'_minute'],2,0,STR_PAD_LEFT);
			$meridiem = $form->post[$name.'_meridiem'];
			
			// Value (unix)
			$value = adodb_strtotime($date.(x($hour) ? " ".$hour.":".$minute.($meridiem ? " ".$meridiem : "") : ""));
			
			// Value (formatted)
			if($value) $value = time_format($value,$this->c[format]);
			else $value = NULL;
			
			// Return
			return $value;
		}
	}
}
?>