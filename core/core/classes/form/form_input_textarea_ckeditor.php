<?php
if(!class_exists('form_input_textarea_ckeditor',false)) {
	/**
	 * An extension of the form_input_textarea class with functionality specific to ckeditor inputs.
	 */
	class form_input_textarea_ckeditor extends form_input_textarea {
		/**
		 * Loads class. Needed so we can use debug_backtrace() to get this class name in PHP < 5.3 in $form_input->load().
		 */
		static function load($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			return parent::load($label,$name,$value,$c);
		}
		
		/**
		 * Construct
		 */
		function __construct($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			$this->form_input_textarea_ckeditor($label,$name,$value,$c);
		}
		function form_input_textarea_ckeditor($label = NULL,$name = NULL,$value = NULL,$c = NULL) {
			// Config
			if(!x($c[purify])) $c[purify] = 0; // Want purification off by default.
			
			// Construct
			parent::__construct($label,$name,$value,$c);
			
			// Config
			if(!$this->c[toolbar]) $this->c[toolbar] = "Medium"; // What predefined toolbar do you want to use: Full, Medium [default], Basic
			if(!$this->c[height]) $this->c[height] = ($this->attributes[height] ? $this->attributes[height] : 250); // Height (in pixels) of texteditor.
		}
		
		/**
		 * Renders the HTML for the actual input.
		 *
		 * @param object $form The form object this input is a part of.
		 * @param array $c An array of config values. Default = NULL
		 * @return string The HTML of the input.
		 */
		function html_element($form,$c = NULL) {
			// Random
			$r = random();
			
			// Config
			if(!$this->attributes[id]) $this->attributes[id] = "ckeditor_".$r; // ID of the textarea
			
			// Javascript
			$html .= "
".include_css('jquery.fancyapps.fancybox')."
".include_javascript('jquery.fancyapps.fancybox'); // Need these for 'overwrite check' when uploading files in CKEditor
			$html .= "
".include_javascript('ckeditor',0,array('minify' =>  0)); // Include more than once (2nd param == 0) as input might get tested in $module->settings() method without the HTML being displayed // Note, can't be 'minified', causes an error (I think it expects to be on the same 'level' as the config.js file).
			$html .= "
<script type='text/javascript'>
$(document).ready(function() {
	var settings = {";
			$x = 0;
			foreach($this->c as $k => $v) {
				if(!is_array($v)) {
					$html .= ($x > 0 ? "," : "")."
		".$k.": ".($v == "true" || $v == "false" || substr($v,0,9) == "CKEDITOR." ? $v : "'".$v."'");
					$x++;
				}
			}
			$html .= "
	};
	framework_ckeditor_load_".$r."('".$this->attributes[id]."',settings);
});
function framework_ckeditor_load_".$r."(id,settings,tries) {
	if(CKEDITOR) {
		// Base
		CKEDITOR.basePath = '".D."core/core/libraries/ckeditor/';
		
		// Visible
		var visible = $('#'+id).is(':visible');
		var fancybox_visible = 0;
		var fancybox = $('#'+id).parents('.fancybox-wrap');
		if(!fancybox.length) fancybox_visible = 1;
		else if($(fancybox).hasClass('fancybox-opened')) fancybox_visible = 1;
		
		debug('trying. id: '+id+', visible: '+visible+', fancybox visible: '+fancybox_visible+', tries: '+tries);
		if(visible && fancybox_visible) {
			CKEDITOR.replace(id,settings);
		}
		else {
			if(tries) tries += 1;
			else tries = 1;
			if(tries < 20) {
				debug('setting up timeout. tries: '+tries);
				
				setTimeout(function() {
					debug('trying again');
					framework_ckeditor_load_".$r."(id,settings,tries);
				},250);
			}
		}
	}
}
</script>";
			
			// HTML
			$html .= "
".parent::html_element($form,$c);
			
			// Return
			return $html;
		}
	}
}
?>