<?php
if(!class_exists('payment_payflowpro',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Payflow Pro.
	 *
	 * Notes:
	 * - You must turn on 'Non-Referenced Credits' in your Payflow Pro settings to be able to credit an account (as opposed to refunding all/part of a previous transaction).
	 *
	 * Custom variables: 2
	 *
	 * To do
	 * - Return 'custom' variables in returned results (if possible)
	 * - clean up the parsing of the results when I test
	 * - check for required fields before sending?
	 *
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_payflowpro {
		/** Stores the user id for your Payflow Pro account (often the same as the vendor value). */
		public $user;
		/** Stores the vendor id (login id) for your Payflow Pro account. */
		public $vendor;
		/** Stores the name of the partner who registered your Payflow Pro account. If you registered it through PayPal use "PayPal". */
		public $partner;
		/** Stores the password for your Payflow Pro account. */
		public $password;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via Payflow Pro testing 'sandbox'.
		 *
		 * @param string $user The user id for your Payflow Pro account (often the same as the vendor value).
		 * @param string $vendor The vendor id (login id) for your Payflow Pro account.
		 * @param string $partner The name of the partner who registered your Payflow Pro account. If you registered it through PayPal use "PayPal".
		 * @param string $password The password for your Payflow Pro account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($user,$vendor,$partner,$password,$c = NULL) {
			self::payment_payflowpro($user,$vendor,$partner,$password,$c);
		}
		function payment_payflowpro($user,$vendor,$partner,$password,$c = NULL) {
			// User ID
			$this->user = $user;
			// Vendor ID
			$this->vendor = $vendor;
			// Partner
			$this->partner = $partner;
			// Password
			$this->password = $password;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * I'm not sure if Payflow Pro allow for 'partial' refunds (meaning only part of a transaction).
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			// No credit card - yes, Payflow Pro does require at least the last 4 digits of a customer's credit card to refund all or part of a transaction
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to refund this amount to.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to Payflow Pro API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
			
			// URL
			if($this->c[test]) $url = 'https://pilot-payflowpro.paypal.com';
			else $url = 'https://payflowpro.paypal.com';
			
			// Values - authorize / charge / credit
			if($action == "authorize" or $action == "charge" or $action == "credit") {
				if($action == "charge") $trxtype = "S";
				if($action == "authorize") $trxtype = "A";
				if($action == "credit") $trxtype = "C";
				$values = array(
					'TRXTYPE' => $trxtype,
					'ACCT' => $payment->card[number],
					'CVV2' => $payment->card[code],
					'EXPDATE' => $payment->card[expiration],
					//'ACCTTYPE' => $payment->card[type],
					'AMT' => $payment->amount,
					'CURRENCY' => $payment->currency,
					'FIRSTNAME' => $payment->address[first_name],
					'LASTNAME' => $payment->address[last_name],
					'STREET' => $payment->address[address_full],
					'CITY' => $payment->address[city],
					'STATE' => $payment->address[state],
					'ZIP' => $payment->address[zip],
					'COUNTRY' => $payment->address[country],
					'EMAIL' => $payment->address[email],
					'INVNUM' => $payment->invoice,
					'ORDERDESC' => $payment->description,
				);
			}
			// Values - capture / cancel
			if($action == "capture" or $action == "cancel") {
				if($action == "capture") $trxtype = "D";
				if($action == "cancel") $trxtype = "V";
				$values = array(
					'TRXTYPE' => $trxtype,
					'ORIGID' => $payment->transaction,
				);
			}
			// Values - refund
			if($action == "refund") {
				$values = array(
					'TRXTYPE' => 'C',
					'ORIGID' => $payment->transaction,
					'AMT' => $payment->amount,
				);
			}
			
			// Values - common
			$values[USER] = $this->user;
			$values[VENDOR] = $this->vendor;
			$values[PARTNER] = $this->partner;
			$values[PWD] = $this->password;
			$values[TENDER] = 'C';	// C - Direct Payment using credit card
			// Values - common - optional
			$values[CUSTIP] = $payment->ip();
			$values[VERBOSITY] = 'MEDIUM';
			$values[CURRENCY] = $payment->currency;
			$values[COMMENT1] = $payment->custom[0];
			$values[COMMENT2] = $payment->custom[1];
						
			// Data
			/*$data = NULL; // Mike Challis (www.carmosaic.com) added feature: bracketed numbers. Bracketed numbers are length tags which allow you to use the special characters of "&" and "=" in the value sent.
			foreach($values as $k => $v) {
				$data[] = $k.'['.strlen($v).']='.$v;
			}
			$data = implode('&', $data);*/
			$data = http_build_query($values);
			
			// Send
			$response = $payment->curl($url,$data);
	
			// Debug
			$payment->debug("values: ".return_array($values),$c[debug]);
			$payment->debug("data: ".$url."?".$data,$c[debug]);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Parse
			$response_parsed = strstr($response,"RESULT");
			$response_array = array();
			while(strlen($response_parsed)) {
				// Name
				$keypos = strpos($response_parsed,'=');
				$keyval = substr($response_parsed,0,$keypos);
				// Value
				$valuepos = strpos($response_parsed,'&') ? strpos($response_parsed,'&'): strlen($response_parsed);
				$valval = substr($response_parsed,$keypos+1,$valuepos-$keypos-1);
				
				// Store
				$response_array[$keyval] = $valval;
				$response_parsed = substr($response_parsed,$valuepos+1,strlen($response_parsed));
			}
			
			// Results
			$results = NULL;
			$results[result] = ($response_array[RESULT] == 0 && $response_array[PNREF] ? 1 : 0);
			$results[message] = $response_array[RESPMSG];
			$results[transaction] = $response_array[PNREF];
			$results[action] = $action;
			$results[test] = $this->c[test];
			$results[source] = "Payflow Pro";
			$results[response] = $response;
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used in Payflow Pro.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;	
			
			// Card number - no spaces or dashes
			if($payment->card[number]) {
				$payment->card[number] = preg_replace('/[^0-9]/','',$payment->card[number]);
			}
			
			// Card expiration - MMYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).substr($payment->card[expiration_year],2,2);
			}
			
			// Address
			if($payment->address[address]) {
				$payment->address[address_full] = $payment->address[address].($payment->address[address_2] ? " ".$payment->address[address_2] : "");
			}
			if($payment->shipping[address]) {
				$payment->shipping[address_full] = $payment->shipping[address].($payment->shipping[address_2] ? " ".$payment->shipping[address_2] : "");
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if(!$this->user or !$this->vendor or !$this->partner or !$this->password) {
				$error = "The login credentials for Payflow Pro are missing.";
			}
			
			// Return
			return $error;
		}
	}
}
?>