<?php
if(!class_exists('payment_paypal',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through PayPal.
	 * 
	 * Custom variables: 1
	 *
	 * Test account (http://developer.paypal.com/):
	 * - Business account - usually this is who we're making payments to (aka, we plug this in for the login credentials) then use the 'Personal Account' as the customer.
	 *   - email: charli_1226956766_biz@dwstudios.net
	 *   - Other:
	 *    - Password: 292442219
	 *    - API Username: charli_1226956766_biz_api1.dwstudios.net
	 *    - API Password: 1226956771
	 *    - API Signature: AXWTUOVrFKyQ9U9PlwfKZ0KYhmsRA4sqyLI0OxJVcd93sMZFGea0YXiF
	 *    - Security Question 1: What was the name of your first school?
	 *    - Security Answer 1: Ripley
	 *    - Security Question 2: What was the name of the hospital in which you were born?
	 *    - Security Answer 2: Meeker
	 * - Personal account - usually we use this as the customer when making a payment to the 'Business Account'
	 *   - email: charli_1226960732_per@dwstudios.net
	 *   - Other:
	 *    - Password: 292442263
	 *    - Security Question 1: What's the name of your first school?
	 *    - Security Answer 1: darkwater
	 *    - Security Question 2: What's the name of your first pet?
	 *    - Security Answer 2: oso
	 *    - Credit Card Number: 4115423551578071
	 *    - Credit Card Expiration: 09/2016
	 *    - Credit Card Security: 123
	 *
	 * To do
	 * - check for required fields before sending?
	 * - Parse out shipping (all the address info) in results()
	 * 
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_paypal {
		/** Stores the e-mail address for your PayPal account. */
		public $email;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - url_notify, string, NULL - The URL to send a confirmation response to on your website to 'notify' you the payment went through.
		 * - url_return, string, NULL - The URL to send the customer to after they've completed payment.
		 * - url_cancel, string, NULL - The URL to send the customer to if they cancel the payment process.
		 * - test, boolean, 0 - Whether or not we want to process transactions via PayPal testing 'sandbox'.
		 *
		 * @param string $email The e-mail address of your PayPal account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($email,$c = NULL) {
			self::payment_paypal($email,$c);
		}
		function payment_paypal($email,$c = NULL) {
			// E-mail address
			$this->email = $email;
			
			// Config
			if(!$c[url_notify]) $c[url_notify] = NULL;
			if(!$c[url_return]) $c[url_return] = NULL;
			if(!$c[url_cancel]) $c[url_cancel] = NULL;
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to PayPal API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment,$c);
			
			// Results
			$results = array(
				'url' => $this->url($payment),
				'test' => $this->c[test],
				'source' => "PayPal",
			);
			
			// Return
			return $results;
		}
		
		/**
		 * Builds and returns the URL for seding the user to PayPal to make a 'basic' payment.
		 *
		 * Configuration values (key, type, default - description):
		 * - url_notify, string, NULL - The URL to send a confirmation response to on your website to 'notify' you the payment went through.
		 * - url_return, string, NULL - The URL to send the customer to after they've completed payment.
		 * - url_cancel, string, NULL - The URL to send the customer to if they cancel the payment process.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL where a user can make their PayPal payment.
		 */
		function url($payment,$c = NULL) {
			// Config
			if(!$c[url_notify]) $c[url_notify] = $this->c[url_notify];
			if(!$c[url_return]) $c[url_return] = $this->c[url_return];
			if(!$c[url_cancel]) $c[url_cancel] = $this->c[url_cancel];
			
			// URL
			if($this->c[test]) $url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			else $url = "https://www.paypal.com/cgi-bin/webscr";
			
			// Values
			$values[business] = $this->email;
			$values[amount] = "$".$payment->amount;
			$values[e] = "0";
			$values[cmd] = "_xclick";
			$values[item_name] = $payment->description;
			$values[custom] = $payment->custom[0];
			$values[currency_code] = $payment->currency;
			$values[no_shipping] = "1";
			$values[no_note] = "1";
			$values[notify_url] = $c[url_notify];
			$values['return'] = $c[url_return];
			$values[cancel_return] = $c[url_cancel];
			$values[rm] = "1";
			$values[charset] = "utf-8";
			if($payment->shipping) {
				$values[address_override] = "1";
				$values[address1] = $payment->shipping[address];
				$values[address2] = $payment->shipping[address_2];
				$values[first_name] = $payment->shipping[first_name];
				$values[last_name] = $payment->shipping[last_name];
				$values[city] = $payment->shipping[city];
				if($payment->shipping[state] and ($payment->shipping[country] == "US" or !$payment->shipping[country])) $values[state] = $payment->shipping[state];
				$values[zip] = $payment->shipping[zip];
				$values[country] = $payment->shipping[country];
				$values[email] = $payment->shipping[email];
				if($payment->shipping[phone]) {
					$array = $this->phone_parse($payment->shipping[phone]);
					$values[night_phone_a] = $array[0];
					$values[night_phone_b] = $array[1];
					$values[night_phone_c] = $array[2];
				}
			}
			$url .= "?".http_build_query($values);
			
			// Return		
			return $url;
		}
		
		/**
		 * Process an array of data returned by PayPal after a transaction attempt and returns an array of results.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $response An array of data returned by PayPal after a transaction attempt.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function results($payment,$response,$c = NULL) {
			// Error
			if(!$payment or !$response) return;
			
			// May want to send back to PayPal with the IPN stuff so they can send back VERIFIED (prevent tampering here).
			// https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_admin_IPNIntro
			
			// Results
			$results = array(
				'result' => ($response[payment_status] == "Completed" ? 1 : 0),
				'message' => '', // Not sure what in the $response would hold this
				'transaction' => $response[txn_id],
				'action' => 'charge', // Only thing supported right now, not sure anything in the response would tell us this
				'test' => 0, // Not sure anything in the $response would hold this, you'll just have to store whether or not it's a test on your end
				'custom' => array(
					$response[custom]
				),
				'source' => 'PayPal',
				'response' => $response
			);
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used in PayPal.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Amount - must include decimals, can optionally include commas in thousands, but I'm leaving it off for now
			if($payment->amount) {
				$payment->amount = number_format($payment->amount,2,'.','');
			}
			
			// Shipping - set to address if nothing set
			$skip = NULL;
			if($payment->shipping[first_name]) $skip[] = "last_name";
			if($payment->shipping[address]) {
				$skip[] = "address_2";
				$skip[] = "company";
				$skip[] = "city";
				$skip[] = "state";
				$skip[] = "zip";
				$skip[] = "country";
			}
			if($payment->address) {
				foreach($payment->address as $k => $v) {
					if(!$payment->x($payment->shipping[$k]) and !in_array($k,$skip)) $payment->shipping[$k] = $v;
				}
			}
			
			// Card expiration - MMYYYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).$payment->card[expiration_year];
			}
			
			// Custom - 256 max, alpha-numeric
			if($payment->custom[0]) {
				$payment->custom[0] = preg_replace("/[^a-zA-Z0-9\s]/","",$payment->custom[0]);
				$payment->custom[0] = substr($payment->custom[0],0,256);
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @param string $function The function from which we called this method.
		 * @return string The error message (if an error was detected).
		 */
		function errors($function) {
			// No login credentials
			if(!$this->email) {
				$error = "The login credentials for PayPal are missing.";
			}
			
			// Return
			return $error;
		}
		
		/**
		 * Parses phone number into 3 separate parts (assumes 10 or 7 digit US number).
		 * 
		 * @param string $phone
		 * @return array A 0 indexed array of the 3 phone parts. Example: 123-456-7891 => array('123','456','7891'). Note: if just 7 digits (ex: 123-4567) it'd return array(1 => '123',2 => '4567').
		 */
		function phone_parse($phone) {
			// Remove non-numbers
			$phone = preg_replace("/[^0-9]/", "", $phone);
			$phone_length = strlen($phone);
		
			// 123-4567
			if($phone_length == 7) {
				$array[1] = substr($phone,0,3);
				$array[2] = substr($phone,3,4);	
			}
			// 123-456-7891 (or +1 123-456-7891)
			else if($phone_length == 10 or $phone_length == 11) {
				if($phone_length == 1) $phone = substr($phone,1); // Remove country code (or 1)
				
				$array[0] = substr($phone,0,3);
				$array[1] = substr($phone,3,3);
				$array[2] = substr($phone,6,4);
			}
			
			// Return
			return $array;
		}
	}
}
?>