<?php
if(!class_exists('payment_qbms',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Quickbooks Merchant Services.
	 *
	 * Connection Ticket
	 * They're supposed to display this to you once you connect your 2 accounts (by visiting the following address: 
	 * https://merchantaccount.ptc.quickbooks.com/j/sdkconnection?appid={app_id}&appdata=mydata, with {app_id}
	 * replaced by the id of your app). But I've yet to see it displayed.
	 *
	 * Instead, they POST the ticket to the 'Subscription URL' you setup for your app. So you must capture that,
	 * store it somewhere, and look in that location after connecting to see it.
	 *
	 * If you're using the Kraken CMS, it stores this in the 'test' database table with the prefix 'QBMS:'.
	 * It's stored as the POST => conntkt value.
	 * 
	 * Custom variables: 0
	 *
	 * Test account:
	 * - app login: kraken.angelvisiontech.com
	 * - app id: 1011721126
	 * - connection ticket: SDK-TGT-64-kEq1YEpv8VvlAm8iLZyOhw
	 *
	 * To do
	 * - 
	 * 
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_qbms {
		/** Stores the application login for your Intuit Partner Platform App. */
		public $app_login;
		/** Stores the connection ticket you were given that connects your Intuit Partner Platform App with your Quickbooks Merchant Services account. */
		public $connection_ticket;
		/** Stores the path on your server to the .pem certificate Quickbooks Merchant Services provided. */
		public $pem;
		/** Stores the qbms class object we'll be using for prcessing things. */
		public $qbms_class;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via Quickbooks Merchant Services test method.
		 *
		 * @param string $app_login The the application login for your Intuit Partner Platform App.
		 * @param string $connection_ticket The connection ticket you were given that connects your Intuit Partner Platform App with your Quickbooks Merchant Services account (see class comments for more info on this).
		 * @param string $pem The full path on your server to the .pem certificate Quickbooks Merchant Services provided.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($app_login,$connection_ticket,$pem,$c = NULL) {
			self::payment_qbms($app_login,$connection_ticket,$pem,$c);
		}
		function payment_qbms($app_login,$connection_ticket,$pem,$c = NULL) {
			// App login
			$this->app_login = $app_login;
			// Connection ticket
			$this->connection_ticket = $connection_ticket;
			// PEM
			$this->pem = $pem;
			// DSN - if you want to log requests/responses to a database, you can provide a database DSN-style connection string here. Example: mysql://root:@localhost/quickbooks_merchantservice
			$dsn = NULL;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
			
			// Class
			//define('QUICKBOOKS_CRLF','<br />');
			require_once SERVER."core/core/classes/payment/qbms/QuickBooks.php";
			$this->qbms_class = new QuickBooks_MerchantService($dsn,$this->pem,$this->app_login,$this->connection_ticket);
			
			// Test
			if($this->c[test]) $this->qbms_class->useTestEnvironment(true);
			// Debug
			if($this->c[debug]) $this->qbms_class->useDebugMode(true);
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Card
				$card = $this->qbms_card($payment);
				
				// Send
				$transaction = $this->qbms_class->authorize($card,$payment->amount);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Transaction
				$transaction = QuickBooks_MerchantService_Transaction::unserialize($payment->transaction);
				
				// Send
				$transaction = $this->qbms_class->capture($transaction,$payment->amount);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Card
				$card = $this->qbms_card($payment);
				
				// Send
				$transaction = $this->qbms_class->charge($card,$payment->amount);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			// No credit card - yes, Quickbooks Merchant Services does require at least the last 4 digits of a customer's credit card to refund all or part of a transaction
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to refund this amount to.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Transaction
				$transaction = QuickBooks_MerchantService_Transaction::unserialize($payment->transaction);
				
				// Send
				$transaction = $this->qbms_class->voidOrRefund($transaction,$payment->amount);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Transaction
				$transaction = QuickBooks_MerchantService_Transaction::unserialize($payment->transaction);
				
				// Send
				$transaction = $this->qbms_class->void($transaction);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Card
				$card = $this->qbms_card($payment);
				
				// Send
				$transaction = $this->qbms_class->refund($card,$payment->amount);
				
				// Results
				$results = $this->qbms_results($payment,$transaction,__FUNCTION__,$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used in Quickbooks Merchant Services.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if(!$this->app_login or !$this->connection_ticket) {
				$error = "The login credentials for Quickbooks Merchant Services are missing.";
			}
			
			// Return
			return $error;
		}

		/**
		 * Creates and returns QBMS credit card object.
		 * 
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The QBMS credit card object.
		 */
		function qbms_card($payment,$c = NULL) {
			// Error
			if(!$payment) return;
			
			// Card
			$card = new QuickBooks_MerchantService_CreditCard($payment->address[first_name]." ".$payment->address[last_name],$payment->card[number],$payment->card[expiration_year],$payment->card[expiration_month],$payment->address[address].($payment->address[address_2] ? " ".$payment->address[address_2] : ""),$payment->address[zip],$payment->card[code]);
			
			// Return
			return $card;
		}

		/**
		 * Creates a results array for the given transaction.
		 * 
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param object $transaction The QBMS transaction object.
		 * @param string $action The transaction action we performed.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array The results array for the given transaction.
		 */
		function qbms_results($payment,$transaction,$action,$c = NULL) {
			// Error
			if(!$payment) return;
			
			// Debug
			$payment->debug("transaction: ".return_array($transaction),$c[debug]);
			
			// Success
			if($transaction) {
				$results = array(
					'result' => 1,
					'message' => "Success",
					'transaction' => $transaction->serialize(),
					'action' => $action,
					'test' => $this->c[test],
					'source' => 'Quickbooks Merchant Services',
					'response' => $transaction,
				);
			}
			// Error
			else {
				$results = array(
					'result' => 0,
					'message' => 'Error '.$this->qbms_class->errorNumber().': '.$this->qbms_class->errorMessage()
				);	
			}
				
			// Return
			return $results;
		}
	}
}
?>