<?php
if(!class_exists('payment_virtualmerchant',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Virtual Merchant.
	 *
	 * Notes:
	 * - When authorizing a transaction (via the authorize() method) the 'transaction' returned isn't the transaction id but is, instead, the 'approval code' you need to send when you 'capture' the authorized transaction.
	 * - To set up a test account, e-mail internetproductsupport@merchantconnect.com with your company name, primary contact name, primary contact phone, and primary e-mail address.  Note, though that this test account will only be usable for 30 days.
	 *
	 * Custom variables: 0
	 *
	 * Test account (only good for 30 days, or until 11/23/2012):
	 * - merchant_id: 001348
	 * - user_id: 001348
	 * - pin: MTKRSW
	 * - Other:
	 *   - Admin URL: https://demo.myvirtualmerchant.com/VirtualMerchantDemo/login.do
	 *   - Password: fall2012
	 *
	 * To do
	 * - Support for $payment->custom array
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 *
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_virtualmerchant {
		/** Stores the merchant id for your Virtual Merchant account. */
		public $merchant_id;
		/** Stores the user id for your Virtual Merchant account. */
		public $user_id;
		/** Stores the pin for your Virtual Merchant account. */
		public $pin;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via the testing 'demo' version (see class comments for info on how to setup a test account).
		 *
		 * @param string $merchant_id The merchant id of your Virtual Merchant account.
		 * @param string $user_id The user id of your Virtual Merchant account.
		 * @param string $pin The pin of your Virtual Merchant account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($merchant_id,$user_id,$pin,$c = NULL) {
			self::payment_virtualmerchant($merchant_id,$user_id,$pin,$c);
		}
		function payment_virtualmerchant($merchant_id,$user_id,$pin,$c = NULL) {
			// Merchant ID
			$this->merchant_id = $merchant_id;
			// User ID
			$this->user_id = $user_id;
			// Pin
			$this->pin = $pin;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'authorize',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'capture',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'charge',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'refund',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'cancel',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer's credit card the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,'credit',$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends charge, authorize, and credit requests to the Virtual Merchant API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
			
			// URL
			if($this->c[test]) $url = "https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do";
			else $url = "https://www.myvirtualmerchant.com/VirtualMerchant/process.do";
			
			// Values
			$values = NULL;
			
			// Values - authorize
			if($action == "authorize") {
				$action_code = "ccauthonly";
				$values = array(
					'ssl_card_number' => $payment->card[number],
					'ssl_exp_date' => $payment->card[expiration],
					'ssl_amount' => $payment->amount,
					'ssl_card_present' => 'N',
					// These aren't listed in the documentation, but I was getting an error on my test account if they weren't pased
					'ssl_cvv2cvc2' => $payment->card[code],
					'ssl_cvv2cvc2_indicator' => '1',
				);
			}
			// Values - capture
			if($action == "capture") {
				$action_code = "ccforce";
				$values = array(
					'ssl_card_number' => $payment->card[number],
					'ssl_exp_date' => $payment->card[expiration],
					'ssl_amount' => $payment->amount,
					'ssl_approval_code' => $payment->transaction,
					'ssl_card_present' => 'N',
				);
			}
			// Values - charge
			if($action == "charge") {
				$action_code = "ccsale";
				$values = array(
					'ssl_card_number' => $payment->card[number],
					'ssl_exp_date' => $payment->card[expiration],
					'ssl_amount' => $payment->amount,
					'ssl_salestax' => '0',
					'ssl_cvv2cvc2' => $payment->card[code],
					'ssl_cvv2cvc2_indicator' => '1',
					'ssl_description' => $payment->description,
					'ssl_invoice_number' => $payment->invoice,
					'ssl_customer_code' => '1111',
					'ssl_company' => $payment->address[company],
					'ssl_first_name' => $payment->address[first_name],
					'ssl_last_name' => $payment->address[last_name],
					'ssl_avs_address' => $payment->address[address],
					'ssl_city' => $payment->address[city],
					'ssl_state' => $payment->address[state],
					'ssl_avs_zip' => $payment->address[zip],
					'ssl_phone' => $payment->address[phone],
					'ssl_email' => $payment->address[email]
				);
			}
			// Values - refund
			if($action == "refund") {
				$action_code = "ccreturn";
				$values = array(
					'ssl_amount' => $payment->amount,
					'ssl_txn_id' => $payment->transaction
				);
			}
			// Values - cancel
			if($action == "cancel") {
				$action_code = "ccvoid"; // There's also a 'ccdelete' action
				$values = array(
					'ssl_txn_id' => $payment->transaction
				);
			}
			// Values - credit
			if($action == "credit") {
				$action_code = "cccredit";
				$values = array(
					'ssl_card_number' => $payment->card[number],
					'ssl_exp_date' => $payment->card[expiration],
					'ssl_amount' => $payment->amount,
					'ssl_card_present' => 'N',
				);
			}
			
			// Values - common
			$values[ssl_transaction_type] = $action_code;
			$values[ssl_merchant_id] = $this->merchant_id;
			$values[ssl_user_id] = $this->user_id;
			$values[ssl_pin] = $this->pin;
			$values[ssl_show_form] = 'false';
			$values[ssl_track_data] = '';
			$values[ssl_receipt_link_method] = 'GET';
			$values[ssl_result_format] = 'ASCII';
			$values[ssl_receipt_link_url] = $_SERVER['PHP_SELF'];
			$values[ssl_test_mode] = ($this->c[test] == 1 ? "true" : "false");
			
			// Data
			$data = NULL;
			foreach($values as $k => $v) $data .= ($data ? "&" : "").$k.'='.urlencode(ereg_replace(',', '', $v));
			
			// Send
			$response = $payment->curl($url,$data);
		
			// Debug
			$payment->debug("values: ".return_array($values),$c[debug]);
			$payment->debug("data: ".$url."?".$data,$c[debug]);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Parse
			$response = trim($response);
			$response_parsed = str_replace("\r", "", $response);
			$response_parsed = str_replace("\n", "&", $response_parsed);
			parse_str($response_parsed,$response_array);
			
			// Results
			if(!$payment->x($response_array[ssl_result]) or $response_array[ssl_result] > 0) $results[result] = 0;
			else $results[result] = 1;
			$results[message] = ($results[result] == 1 ? $response_array[ssl_result_message] : $response_array[errorMessage]);
			$results[transaction] = ($action == "authorize" ? $response_array[ssl_approval_code] : $response_array[ssl_txn_id]);
			$results[test] = $this->c[test];
			$results[source] = "VirtualMerchant";
			$results[response] = $response;
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used by Virtual Merchant.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Card expiration - MMYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).substr($payment->card[expiration_year],2,2);
			}
			
			// Company - I think this is required
			if(!$payment->address[company]) $payment->address[company] = "Unknown";
			
			// Clean
			$array = array(
				'invoice',
				'description',
				'billing' => array(
					'first_name',
					'last_name',
					'company',
					'address',
					'email',
				)
			);
			foreach($array as $parent => $children) {
				if(is_array($children)) {
					foreach($children as $v) {
						if($payment->$parent[$v]) $payment->$parent[$v] = $this->clean($payment->$parent[$v]);	
					}
				}
				else {
					if($payment->$children) $payment->$children = $this->clean($payment->$children);	
				}
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Cleans the given string, removing any unwanted characters that Virtual Merchant can't handle.
		 *
		 * @param string $string The string we want to clean.
		 * @return string The cleaned string.
		 */
		function clean($string) {
			// Clean
			$string = str_replace("&amp;", "and", $string);
			$string = str_replace(" & ", " and ", $string);
			$string = str_replace("&", "", $string);
			$string = str_replace("/", "-", $string);
			
			// Return
			return $string;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if(!$this->merchant_id or !$this->user_id or !$this->pin) {
				$error = "The login credentials for Virtual Merchant are missing.";
			}
			
			// Return
			return $error;
		}
	}
}