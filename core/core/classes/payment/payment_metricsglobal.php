<?php
if(!class_exists('payment_metricsglobal',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through Metrics Global.
	 *
	 * Notes
	 * - You can't cancel a transaction that has been refunded (even if it's been fully refunded).
	 *
	 * Custom variables: 0
	 *
	 * Test account (this gets auto-set in the class when $c[test] = 1):
	 * - username: demo
	 * - password: password
	 *
	 * To do
	 * - Support for $payment->custom array
	 * - Return 'custom' variables in returned results (if possible)
	 * - check for required fields before sending?
	 * 
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_metricsglobal {
		/** Stores the username for your Metrics Global account. */
		public $username;
		/** Stores the password for your Metrics Global account. */
		public $password;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - test, boolean, 0 - Whether or not we want to process transactions via Metrics Global testing 'sandbox'.
		 *
		 * @param string $username The username of your Metrics Global account.
		 * @param string $password The password of your Metrics Global account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($username,$password,$c = NULL) {
			self::payment_metricsglobal($username,$password,$c);
		}
		function payment_metricsglobal($username,$password,$c = NULL) {
			// Username
			$this->username = $username;
			// Password
			$this->password = $password;
			
			// Config
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to authorize.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to charge.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			// No credit card - yes, Metrics Global does require at least the last 4 digits of a customer's credit card to refund all or part of a transaction
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to refund this amount to.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors()) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			// No credit card
			else if(!$payment->card[number]) {
				$error = "No credit card was passed for us to credit.";	
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to Metrics Global API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment);
			
			// Login credentials
			if($this->c[test]) {
				$username = "demo";
				$password = "password";
				$url = "https://secure.metricsglobalgateway.com/gateway/transact.dll?testing=true";
			}
			else {
				$username = $this->username;
				$password = $this->password;
				$url = "https://secure.metricsglobalgateway.com/gateway/transact.dll";
			}
			
			// Convert currency - Metrics Global only allows payments in USD at the moment (well, Authorize.net only allows them, not sure if Metrics Global does or not)
			if($payment->currency and $payment->currency != "USD") {
				$payment->amount = $payment->convert_currency($payment->amount,$payment->currency,"USD");
			}
			
			// Transaction type
			if($action == "authorize") $action_code = "AUTH_ONLY";
			if($action == "capture") $action_code = "PRIOR_AUTH_CAPTURE";
			if($action == "charge") $action_code = "AUTH_CAPTURE";
			if($action == "refund") $action_code = "CREDIT";
			if($action == "cancel") $action_code = "VOID";
			if($action == "credit") $action_code = "CREDIT";
			
			// Values
			$values	= array(
				//"x_test_request" => "TRUE", // Pass this to use test mode on live (non-Test Mode) Metrics Global accounts
				"x_login" => $username,
				"x_tran_key" => $password,
				"x_version" => "3.1",
				"x_delim_char" => "|",
				"x_delim_data" => "TRUE",
				"x_url" => "FALSE",
				"x_type" => $action_code,
				"x_method" => "CC",
				'x_trans_id' => $payment->transaction,
				"x_relay_response" => "FALSE",
				"x_card_num" => $payment->card[number],
				"x_exp_date" => $payment->card[expiration],
				"x_amount" => $payment->amount,
				//"x_currency_code" => $payment[currency], // Not yet supported by Metrics Global
				"x_first_name" => $payment->address[first_name],
				"x_last_name" => $payment->address[last_name],
				"x_company" => $payment->address[company],
				"x_address" => $payment->address[address_full],
				"x_city" => $payment->address[city],
				"x_state" => $payment->address[state],
				"x_zip"	=> $payment->address[zip],
				"x_country" => $payment->address[country],
				"x_email" => $payment->address[email],
				"x_phone" => $payment->address[phone],
				"x_invoice_num" => $payment->invoice,
				"x_description" => $payment->description,
				"x_card_code" => $payment->card[code],
				"x_ship_to_first_name" => $payment->shipping[first_name],
				"x_ship_to_last_name" => $payment->shipping[last_name],
				"x_ship_to_company" => $payment->shipping[company],
				"x_ship_to_address" => $payment->shipping[address_full],
				"x_ship_to_city" => $payment->shipping[city],
				"x_ship_to_state" => $payment->shipping[state],
				"x_ship_to_zip" => $payment->shipping[zip],
				"x_ship_to_country" => $payment->shipping[country],
			);
	
			// Data
			$data = http_build_query($values);
		
			// Send
			$response = $payment->curl($url,$data);
		
			// Debug
			$payment->debug("values: ".return_array($values),$c[debug]);
			$payment->debug("data: ".$url."?".$data,$c[debug]);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Results
			$ex = explode('|',$response);
			$results = NULL;
			$results[result] = ($ex[0] === "1" ? 1 : 0);
			$results[message] = $ex[3];
			$results[transaction] = $ex[6];
			$results[action] = $action;
			$results[test] = $this->c[test];
			$results[source] = "Metrics Global";
			$results[response] = $response;
			
			// Return
			return $results;
		}
		
		/**
		 * Standardizes some of the values used in Metrics Global.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;	
			
			// Amount - x.xx
			$payment->amount = number_format($payment->amount,2,'.','');
			
			// Address
			if($payment->address[address]) {
				$payment->address[address_full] = $payment->address[address].($payment->address[address_2] ? " ".$payment->address[address_2] : "");
			}
			if($payment->shipping[address]) {
				$payment->shipping[address_full] = $payment->shipping[address].($payment->shipping[address_2] ? " ".$payment->shipping[address_2] : "");
			}
			
			// Card expiration - MMYY
			if($payment->card[expiration_month] and $payment->card[expiration_year]) {
				$payment->card[expiration] = str_pad($payment->card[expiration_month],2,"0",STR_PAD_LEFT).substr($payment->card[expiration_year],2,2);
			}
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @return string The error message (if an error was detected).
		 */
		function errors() {
			// No login credentials
			if((!$this->username or !$this->password) and !$this->c[test]) {
				$error = "The login credentials for Metrics Global are missing.";
			}
			
			// Return
			return $error;
		}
	}
}
?>