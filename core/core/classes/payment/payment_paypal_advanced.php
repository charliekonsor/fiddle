<?php
if(!class_exists('payment_paypal_advanced',false)) {
	/**
	 * An 'extension' of the payment class used for processing payments through PayPal Advanced.
	 * 
	 * Custom variables: 1
	 *
	 * Test account (http://developer.paypal.com/):
	 * - Business account - usually this is who we're making payments to (aka, we plug this in for the login credentials) then use the 'Personal Account' as the customer.
	 *   - email: charli_1226956766_biz@dwstudios.net
	 *   - Other:
	 *    - Password: 292442219
	 *    - API Username: charli_1226956766_biz_api1.dwstudios.net
	 *    - API Password: 1226956771
	 *    - API Signature: AXWTUOVrFKyQ9U9PlwfKZ0KYhmsRA4sqyLI0OxJVcd93sMZFGea0YXiF
	 *    - Security Question 1: What was the name of your first school?
	 *    - Security Answer 1: Ripley
	 *    - Security Question 2: What was the name of the hospital in which you were born?
	 *    - Security Answer 2: Meeker
	 * - Personal account - usually we use this as the customer when making a payment to the 'Business Account'
	 *   - email: charli_1226960732_per@dwstudios.net
	 *   - Other:
	 *    - Password: 292442263
	 *    - Security Question 1: What's the name of your first school?
	 *    - Security Answer 1: darkwater
	 *    - Security Question 2: What's the name of your first pet?
	 *    - Security Answer 2: oso
	 *    - Credit Card Number: 4115423551578071
	 *    - Credit Card Expiration: 09/2016
	 *    - Credit Card Security: 123
	 *
	 * To do
	 * - check for required fields before sending?
	 * - Parse out shipping (all the address info) in results()
	 * 
	 * Dependencies
	 * - Functions
	 *   - x() - __construct()
	 *
	 * @package kraken\payments
	 */
	class payment_paypal_advanced {
		/** Stores the user id for your Payflow Advanced account (often the same as the vendor value). */
		public $user;
		/** Stores the vendor id (login id) for your Payflow Advanced account. */
		public $vendor;
		/** Stores the name of the partner who registered your Payflow Advanced account. If you registered it through PayPal use "PayPal". */
		public $partner;
		/** Stores the password for your Payflow Advanced account. */
		public $password;
		/** Stores an array of configuration values passed to the class. */
		public $c;
		
		/**
		 * Constructs the class.
		 *
		 * Configuration values (key, type, default - description):
		 * - url_notify, string, NULL - The URL to send a confirmation response to on your website to 'notify' you the payment went through. // Not a featire pf PayPal Advanced
		 * - url_return, string, NULL - The URL to send the customer to after they've completed payment.
		 * - url_cancel, string, NULL - The URL to send the customer to if they cancel the payment process.
		 * - embed, boolean, 0 - Are we going to embed the PayPal checkout page (as opposed to using Express Checkout)? If so, the URL we return in results will be the URL you embed in an iframe. // Untested
		 * - test, boolean, 0 - Whether or not we want to process transactions via PayPal testing 'sandbox'.
		 *
		 * @param string $user The user id for your Payflow Pro account (often the same as the vendor value).
		 * @param string $vendor The vendor id (login id) for your Payflow Pro account.
		 * @param string $partner The name of the partner who registered your Payflow Pro account. If you registered it through PayPal use "PayPal".
		 * @param string $password The password for your Payflow Pro account.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($user,$vendor,$partner,$password,$c = NULL) {
			self::payment_paypal_advanced($user,$vendor,$partner,$password,$c);
		}
		function payment_paypal_advanced($user,$vendor,$partner,$password,$c = NULL) {
			// User ID
			$this->user = $user;
			// Vendor ID
			$this->vendor = $vendor;
			// Partner
			$this->partner = $partner;
			// Password
			$this->password = $password;
			
			// Config
			//if(!$c[url_notify]) $c[url_notify] = NULL; // Not a featire pf PayPal Advanced
			if(!$c[url_return]) $c[url_return] = NULL;
			if(!$c[url_cancel]) $c[url_cancel] = NULL;
			if(!x($c[embed])) $c[embed] = 0;
			if(!x($c[test])) $c[test] = 0;
			$this->c = $c;
		}
		
		/**
		 * Authorizes the customer's account for given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function authorize($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No authorization amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"authorize",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Captures a transaction that was previously authorized via the authorize() method.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function capture($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No capture amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"capture",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Charges the customer a given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function charge($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No charge amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"charge",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Refunds a customer/account a defined amount on a previous transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function refund($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed for the transaction we want to refund.";
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No refund amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"refund",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Cancels/voids the given transaction.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function cancel($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No transaction ID
			else if(!$payment->transaction) {
				$error = "No transaction ID was passed that we could cancel.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Send
				$results = $this->send($payment,"cancel",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Credits a customer the given amount.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message'.
		 */
		/*function credit($payment,$c = NULL) {
			// Common errors
			if($error_common = $this->errors(__FUNCTION__)) {
				$error = $error_common;
			}
			// No amount passed
			else if(!$payment->amount) {
				$error = "No credit amount was passed.";
			}
			
			// Error
			if($error) {
				$results = array(
					'result' => 0,
					'message' => $error
				);	
			}
			// Transaction
			else {
				// Remove transaction - must NOT be submitted for this to work (if we want to refund on a specific transaction we use the refund() method).
				$payment->transaction = NULL;
				// Send
				$results = $this->send($payment,"credit",$c);
			}
			
			// Return
			return $results;
		}
		
		/**
		 * Sends various 'transactions' to PayPal Advanced API.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param string $action The transaction action we're performing.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function send($payment,$action,$c = NULL) {
			// Standardize
			$payment = $this->standardize($payment,$c);
			
			// Results
			$results = array(
				'url' => $this->url($payment),
				'test' => $this->c[test],
				'source' => "PayPal Advanced",
			);
			
			// Return
			return $results;
		}
		
		/**
		 * Builds and returns the URL for seding the user to PayPal Advanced to make a 'basic' payment.
		 *
		 * Configuration values (key, type, default - description):
		 * - url_notify, string, NULL - The URL to send a confirmation response to on your website to 'notify' you the payment went through.
		 * - url_return, string, NULL - The URL to send the customer to after they've completed payment.
		 * - url_cancel, string, NULL - The URL to send the customer to if they cancel the payment process.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL where a user can make their PayPal Advanced payment.
		 */
		function url($payment,$c = NULL) {
			// Config
			//if(!$c[url_notify]) $c[url_notify] = $this->c[url_notify]; // Not a featire pf PayPal Advanced
			if(!$c[url_return]) $c[url_return] = $this->c[url_return];
			if(!$c[url_cancel]) $c[url_cancel] = $this->c[url_cancel];
			if(substr($c[url_return],0,6) == "https:") $c[url_return] = str_replace('https://','http://',$c[url_return]); // Doesn't like https for some reason
			
			// URL
			if($this->c[test]) $url = "https://pilot-payflowpro.paypal.com";
			else $url = "https://payflowpro.paypal.com";
			
			// Data
			$values = array(
				'USER' => $this->user,
				'VENDOR' => $this->vendor,
				'PARTNER' => $this->partner,
				'PWD' => $this->password,
				'BUTTONSOURCE' => 'Kraken', // PayPal's example used "PF-ECWizard", not sure if they validate this at all
				'TENDER' => 'P',
				'ACTION' => 'S',
				'TRXTYPE' => 'S', // S = sale, A = authorize
				'AMT' => $payment->amount,
				'CURRENCY' => 'USD',
				'CANCELURL' => $c[url_cancel],
				'RETURNURL' => $c[url_return],
				//'NOTIFYURL' => $c[url_notify], // Not a featire pf PayPal Advanced
				// Assumed these exist as we use them in PayFlow Pro
				'EMAIL' => $payment->address[email],
				'INVNUM' => $payment->invoice,
				'ORDERDESC' => $payment->description,
			);
			if($payment->address) {
				$values[BILLTOFIRSTNAME] = $payment->address[first_name];
				$values[BILLTOLASTNAME] = $payment->address[last_name];
				$values[BILLTOSTREET] = $payment->address[address];
				$values[BILLTOSTREET2] = $payment->address[address_2];
				$values[BILLTOCITY] = $payment->address[city];
				if($payment->address[state] and ($payment->address[country] == "US" or !$payment->address[country])) $values[BILLTOSTATE] = $payment->address[state];
				$values[BILLTOZIP] = $payment->address[zip];
				$values[BILLTOCOUNTRY] = $payment->address[country];
			}
			if($payment->shipping) {
				$values[ADDROVERRIDE] = 1;
				$values[SHIPTOSTREET] = $payment->shipping[address];
				$values[SHIPTOSTREET2] = $payment->shipping[address_2];
				$values[SHIPTOCITY] = $payment->shipping[city];
				if($payment->shipping[state] and ($payment->shipping[country] == "US" or !$payment->shipping[country])) $values[SHIPTOSTATE] = $payment->shipping[state];
				$values[SHIPTOZIP] = $payment->shipping[zip];
				$values[SHIPTOCOUNTRY] = $payment->shipping[country];
			}
			if($this->c[embed]) {
				$values[CREATESECURETOKEN] = "Y";
				$values[SECURETOKENID] = $this->unique_id();
			}
			//$data = http_build_query($values); // Doesn't like properly urlencoded stuff apparently
			foreach($values as $k => $v) $data .= ($data ? "&" : "").$k."=".$v;
			$payment->debug("data: ".$data,$c[debug]);
			
			// Curl
			$response = $this->curl($payment,$url,$data);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Parse
			$array = $this->parse($response);
			$payment->debug("response (parsed): ".return_array($array),$c[debug]);
			
			// Success
			if($array[RESULT] == "0") {
				// Token
				$token = urldecode($array[TOKEN]);
				$_SESSION['payments']['paypal_advanced'] = array(
					'token' => $token,
					'token_id' => $values[SECURETOKENID],
					'amount' => $payment->amount,
				);
				
				// URL
				if($this->c[embed]) {
					$url = "https://payflowlink.paypal.com?SECURETOKEN=".$token."&SECURETOKENID=".$values[SECURETOKENID]."&MODE=".($this->c[test] ? "TEST" : "LIVE");
				}
				else {
					if($this->c[test]) $url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=".$token;
					else $url = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=".$token;
				}
				
				// Return
				return $url;
			}
		}
		
		/**
		 * Process an array of data returned by PayPal Advanced after a transaction attempt and returns an array of results.
		 *
		 * @param object $payment The instance of the payment class which holds the payment/customer info.
		 * @param array $response An array of data returned by PayPal Advanced after a transaction attempt.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of information about the result of the transaction including 'result' (boolean, 1 = succes, 0 = error) and 'message' as well as the transaction id ('transaction').
		 */
		function results($payment,$response,$c = NULL) {
			// Error
			if(!$payment) return;
			
			// URL
			if($this->c[test]) $url = "https://pilot-payflowpro.paypal.com";
			else $url = "https://payflowpro.paypal.com";
			
			// Data
			$values = array(
				'USER' => $this->user,
				'VENDOR' => $this->vendor,
				'PARTNER' => $this->partner,
				'PWD' => $this->password,
				'BUTTONSOURCE' => 'Kraken', // PayPal's example used "PF-ECWizard", not sure if they validate this at all
				'TENDER' => 'P',
				'ACTION' => 'D',
				'TRXTYPE' => 'S', // S = sale, A = authorize
				'AMT' => $_SESSION['payments']['paypal_advanced']['amount'],
				'CURRENCY' => 'USD',
				'TOKEN' => $_SESSION['payments']['paypal_advanced']['token'],
				'PAYERID' => $response[PayerID],
				'IPADDRESS' => $_SERVER['SERVER_NAME'],
			);
			//$data = http_build_query($values); // Doesn't like properly urlencoded stuff apparently
			foreach($values as $k => $v) $data .= ($data ? "&" : "").$k."=".$v;
			
			// cURL
			$response = $this->curl($payment,$url,$data);
			$payment->debug("response: ".$response,$c[debug]);
			
			// Parse
			$array = $this->parse($response);
			$payment->debug("response (parsed): ".return_array($array),$c[debug]);
			$db = db::load();
			$db->q("INSERT INTO test SET test = '".a('paypal_advanced. results. response: '.$response.', response (parsed): '.return_array($array).', data: '.$data,1)."'");
			
			// Results
			$results = array(
				'result' => ($array[RESULT] == 0 && $array[PNREF] ? 1 : 0),
				'message' => $array[RESPMSG], // Not sure what in the $response would hold this
				'transaction' => $array[PNREF],
				'action' => 'charge', // Only thing supported right now, not sure anything in the response would tell us this
				'test' => 0, // Not sure anything in the $response would hold this, you'll just have to store whether or not it's a test on your end
				'source' => 'PayPal Advanced',
				'response' => $response
			);
			
			// Session
			$_SESSION['payments']['paypal_advanced'] = NULL;
			
			// Return
			return $results;
		}
		
		/**
		 * Parses the returned response and converts it to an array.
		 *
		 * @param string $response The response we want to parse.
		 * @return array The response parsed into an array.
		 */
		function parse($response) {
			list($head,$result) = explode('RESULT=',$response);
			parse_str("RESULT=".$result,$array);
			return $array;	
		}
		
		/**
		 * Standardizes some of the values used in PayPal Advanced.
		 *
		 * @param object $payment The payment object we want to standize the values of.
		 * @return object The standardized payment object.
		 */
		function standardize($payment) {
			// Error
			if(!$payment) return;
			
			// Return
			return $payment;
		}
		
		/**
		 * Detects some common errors for this gateway and returns the error message.
		 *
		 * @param string $function The function from which we called this method.
		 * @return string The error message (if an error was detected).
		 */
		function errors($function) {
			// No login credentials
			if(!$this->user or !$this->vendor or !$this->partner or !$this->password) {
				$error = "The login credentials for PayPal Advanced are missing.";
			}
			
			// Return
			return $error;
		}
		
		/**
		 * Handles cURL calls for paypal advanced.
		 * 
		 * @param object $payment THe paymnet
		 * @param string $url The URL we're accessing via cURL.
		 * @param string $post The POST string we want to send.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The raw response.
		 */
		function curl($payment,$url,$post = NULL,$c = NULL) {
			// Headers
			$header[] = "Content-Type: text/namevalue";
			$header[] = "Content-Length: ".strlen($post);
			$header[] = "X-VPS-CLIENT-TIMEOUT: 20"; // This is server timeout, make sure less than curl timeout (30)
			$header[] = "X-VPS-REQUEST-ID:KRAKEN-".$this->unique_id();
			if($this->c[test]) $header[] = "Host: pilot-payflowpro.paypal.com";
			else $header[] = "Host: payflowpro.paypal.com";
			
			// Curl
			return $payment->curl($url,$post,array('header' => $header));	
		}
		
		/**
		 * Returns a unique alphanumeric id.
		 *
		 * @return string A unique alphanumeric id.
		 */
		function unique_id() {
			return mt_rand(1,99999).str_replace(' ','',microtime());
		}
	}
}
?>