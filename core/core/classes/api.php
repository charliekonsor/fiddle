<?php
if(!class_exists('api',false)) {
	/**
	 * A class for handling various API actions such as retrieving data, updating records, etc.
	 *
	 * @package kraken\api
	 */
	class api extends core_framework {
		/** An array of keys that are useable in this API. Might want to leave this blank and update the call()  method to check the key if they are stored in a database or something. */
		public $keys = array();
		/** The key the user passed when trying to access the API. */
		public $key = NULL;
		/** An array that stores the results after a call is made. */
		public $results = NULL;
		/** The format you want data to come in and go out as: json (default), xml. */
		public $format = "json";
		/** An array of configuration values passed to the class */
		public $c;
		
		/**
		 * Loads and returns an instance of the api class, using module or framework level classes if they exist.
		 *
		 * Example:
		 * - $api = api::load($c);
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of either the api class, the api_framework class (if it exists), the module specific api_framework class (if it exists).
		 */
		static function load($c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Framework
			if(FRAMEWORK) {
				$class_framework = $class."_framework";
				return new $class_framework($c);
			}
			
			// Core class
			return new $class($c);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($c = NULL) {
			self::api($c);
		}
		function api($c = NULL) {
			// Config
			$this->c = $c;
			
			// Format
			if(g('config.api.format')) $this->format = g('config.api.format');
			// Keys
			if(g('config.api.keys')) $this->keys = g('config.api.keys');
		}
		
		/**
		 * Returns (and sets if $key passed) the key for this API instance.
		 *
		 * @param string $key The key we want to store for this API instance. Default = NULL
		 * @return string The stored key for this API instance.
		 */
		function key($key = NULL) {
			// Save
			if($key) $this->key = $key;
			
			// Return
			return $this->key;
		}
		
		/**
		 * Returns (and sets if $format passed) the format for this API instance.
		 *
		 * @param string $format The format we want to use when dealing with data: xml, json. Default = xml
		 * @return string The stored format for this API instance.
		 */
		function format($format = NULL) {
			// Save
			if($format) $this->format = $format;
			
			// Return
			return $this->format;
		}
	
		/**
		 * Makes a call to the API.
		 *
		 * @param string|array $call Either the URL string that was called or an already parsed array of the call.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of data concerning the action the user is attempting.
		 */
		function call($call,$c = NULL) {
			// Error
			if(!$call) return;
			
			// Results
			$results = NULL;
			
			// Key
			if($this->keys) {
				// No / incorrect key
				if(!$this->key or !in_array($this->key,$this->keys)) {
					$results = array(
						'status' => 0,
						'message' => (!$this->key ? "You must pass your 'key' in the API call." : "The key you're using isn't valid.")
					);
				}
			}
			
			// Call
			if(!$results) {
				
			}
			
			// Save
			$this->results = $results;
			
			// Return
			return $results;
		}
		
		/**
		 * Processes the given array of values through the given format array, retuning the formatted array of values.
		 *
		 * Doesn't really do much here. Format is more robust and makes more since at the item level as that's what we're formatting.
		 *
		 * @param array $values The array of values you want to format.
		 * @param array $format The array which defines the format you want to return the values in.
		 * @return array The formatted array of values.
		 */
		/*function format($values,$format) {
			// Error
			if(!$values) return;	
			
			// No format
			if(!$format) return $values;
			
			// Format
			foreach($format as $x => $v) {
				// Value
				$value = $values[$v[name]];
				// Label
				if($v[label]) $label = $v[label];
				else $label = $v[name];
				
				// Array
				$array[$label] = $value;
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Prints XML of JSON (depending on $this->format) of the results attained when we ran $this->call();
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function results($c = NULL) {
			// Error
			if(!$this->results) return;
			
			// Config
			if(!$this->x($c[debug])) $c[debug] = 0; // Debug
			
			// XML
			if($this->format == "xml") {
				if(!$c[debug]) header ("Content-Type:text/xml");
				include_function('xml');
				print '<?xml version="1.0" encoding="UTF-8"?>'.array2xml(array('results' => $this->results));
			}
			// JSON
			if($this->format == "json") {
				print json_encode($this->results);
			}
		}
		
		/**
		 * Determines if the passed variable has a value associated with it.
		 * 
		 * Returns true if value exists or false if it's empty:
		 * - 0 = true
		 * - "0" = true
		 * - "false" = true
		 * - false = false
		 * - "" = false
		 * - NULL = false
		 * - "NULL" = true
		 * - true = true
		 * - "true" = true
		 * 
		 * Also works for arrays.
		 * 
		 * @param mixed $value The value we want to check against
		 * @return boolean Whether or not the variable has a value associated with it
		 */
		function x($value) {
			if(is_array($value)) $return = true;
			else if(is_object($value)) $return = true;
			else if(strlen($value) > 0) $return = true;
			else $return = false;
			return $return;
		}
	}
}