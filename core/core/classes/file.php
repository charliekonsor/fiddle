<?php
/**
 * Need this page level docbloc for the imagecreatefrombmp() function at the bottom.
 *
 * @package kraken\files
 */
if(!class_exists('file',false)) {
	/**
	 * Class for uploading, managing, and analyzing files.
	 *
	 * Static - currently we have some 'static' functions, but aren't declaring them a such so we can use both $this and self:: depending on whether or not they were called from a static or non-static environment.
	 * If we wanted to be valid, each static function would have to only ever be static, only call static functions, and only use static variables.
	 *
	 * Dependencies
	 * - functions
	 *   - x
	 *   - g
	 *   - include_javascript - in video_html()
	 * - constants
	 *   - SERVER - in $c and server()
	 *   - D - in $c and domain()
	 * - javascript
	 *   - videojs - in video_html()
	 *
	 * ToDo
	 * - find a way to not have to declare types in both __construct() and type()
	 * - file_image is separate class that extends file, file_video extends file, etc.?
	 *
	 * @package kraken\files
	 */
	class file {
		// Variables
		public $file, $exists, $file_changed, $name, $path, $extension, $extension_standardized, $type; // string
		public $cache; // array
		private $temp; // string
		private $image_resource; // resource
		public $storage; // string
		private static $c_static = array(	
			'storage' => 'local', // The service we're using to store files.
			'server' => SERVER, // The server path to the root of the site. Used when passing relative paths. Ex: images/ instead of /home/test/domains/test.com/public_html/
			'domain' => D, // Domain of the site, including trailing slash. Ex: http://www.test.com/
			'ffmpeg' => "/usr/bin/ffmpeg", // The path to FFMPEG on the server. Usually /usr/local/bin/ffmpeg or /usr/bin/ffmpeg
			'flvtools' => "/usr/local/bin/flvtool2", // The path to FLV Tools o the server. Usually /usr/local/bin/flvtool2 or /usr/bin/flvtool2
			'permission' => "0777", // Permission to give file when saving
			'overwrite' => 0, // Do you want to overwrite a file if it already exists (checked when save() method is called).
			'upload' => array(
				'clean' => 1, // Do you want to clean up the name of an uploaded file, removing any problematic characters?
			)
		);
		private $c = array(); // We'll set this to $c_static when we initialize a new class object
		/** An array of the exentensions for each file type. */
		private static $types = array(
			'image' => array(),
			'video' => array(),
			'audio' => array()
		);
		
		/**
		 * Loads and returns an instance of the file class.
		 *
		 * Example:
		 * - $file = file::load($file,$c);
		 *
		 * @param string $file A file you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object An instance of the class.
		 */
		static function load($file = NULL,$c = NULL) {
			// Class name
			$class = __CLASS__;
			
			// Storage - via $c - ex: $c[storage] = "amazons3"; Note, you could use this along with the file name method to load a local file in an external storage class. Example: file::load('local::path/to/file.jpg',array('storage' => 'amazons3'));
			if($c[storage]) {
				$class_storage = $class."_".$c[storage];
				include_once SERVER."core/core/classes/".$class_storage.".php";
				if(class_exists($class_storage)) {
					return new $class_storage($file,$c);
				}
				else unset($c[storage]);
			}
			// Storage - via file name - ex: amazons3::local/uploads/videos/original/video.mp4
			if($file and !is_array($file) and strstr($file,"::")) {
				list($storage,$storage_file) = explode('::',$file);
				$class_storage = $class."_".$storage;
				include_once SERVER."core/core/classes/".$class_storage.".php";
				if(class_exists($class_storage)) {
					$c[storage] = $storage;
					return new $class_storage($storage_file,$c);
				}
			}
			
			// Class
			return new $class($file,$c);
		}
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $file A file (either the path to it or the $_FILES array of it) you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($file = NULL,$c = NULL) {
			self::file($file,$c);
		}
		function file($file = NULL,$c = NULL) {
			// Config
			file::value('c',self::$c_static);
			if($c) file::call('c',$c);
			if(strlen(file::value('c.permission')) == 3) file::value('c.permission',"0".file::value('c.permission')); // Make sure we have 4 digits (staring with a 0)
			
			// Types
			file::value('types',array(
				'image' => g('config.uploads.types.image.accept'),
				'video' => g('config.uploads.types.video.accept'),
				'audio' => g('config.uploads.types.audio.accept')
			));
			
			// Upload
			if(is_array($file) and $file[name]) {
				$file = file::call('upload',$file,file::call('temp'));
			}
			
			// Storage
			$storage = file::call('storage',$file); // Passed via file name. Example:  $file = file::load('amazons3::/path/to/file.jpg');
			if(!$storage) $storage = file::value('c.storage'); // Passed via config array. Example: $file = file::load('/path/to/file.jpg',array('storage' => 'amazons3'));
			if(!$storage) $storage = "local"; // Default to local
			file::value('storage',$storage);
			$file = str_replace($storage."::","",$file);
			
			// Clean
			$file = file::call('clean',$file);
			
			// Localize
			$file = file::call('localize',$file);
			
			// File
			file::value('file',$file);
			debug("file: ".file::value('file'),file::value('c.debug'));
			
			// Analyze
			file::call('analyze');
		}
		
		/**
		 * Handle's all 'calls' within the class, calling them on $this if it exists or self if we're using a static version of the class.
		 *
		 * @param string $method The method you want to call.
		 * @param mixed $param1 The first param to pass to the method.
		 * @return mixed The returned result of the method call.
		 */
		function call($method,$param1 = NULL) {
			// Error
			if(!$method) return;
			
			// Params
			$params = func_get_args();
			unset($params[0]); // The method
			$params = array_values($params); // Reset keys
			
			// Static
			$static = 1;
			if($this) {
				if(substr(get_class($this),0,4) == "file") $static = 0;
			}
			
			// Call
			$return = call_user_func_array(array(($static ? __CLASS__ : $this),$method),$params);
			
			// Return
			return $return;
		}
		
		/**
		 * Gets (or sets if 2nd param) the global value for the given key.
		 *
		 * This is basically a way to make sure we're not in a 'static' instance of this class.
		 *
		 * @param string $key The key you want to get (or set) the value for.
		 * @param mixed $value The value you want to set the key to. Default = NULL
		 * @return mixed The global value for the given key.
		 */
		function value($key,$value = NULL) {
			// Static
			$static = 1;
			if($this) {
				if(substr(get_class($this),0,4) == "file") $static = 0;
			}
			
			// Error - return if static call as we don't declare any variables as 'static'
			if(!$key or $static) return;
			
			// Shortcuts - used a lot, speed things up a bit
			if(!$static and !x($value)) {
				if($key == "file") return $this->file;
				if($key == "c.debug") return $this->c[debug];
			}
			
			// Key
			$keys = NULL;
			if(strstr($key,'.')) {
				$keys = explode('.',$key);
			}
			
			// Set
			if(x($value)) {
				if(!$static) {
					if(x($keys[3])) $this->{$keys[0]}[$keys[1]][$keys[2]][$keys[3]] = $value;
					else if(x($keys[2])) $this->{$keys[0]}[$keys[1]][$keys[2]] = $value;
					else if(x($keys[1])) $this->{$keys[0]}[$keys[1]] = $value;
					else $this->$key = $value;
				}
				/*else { // Would have to declare values as 'statice', also the 'array' (where $keys) didn't work for me
					if(x($keys[3])) self::${$key}[$keys[1]][$keys[2]][$keys[3]] = $value;
					else if(x($keys[2])) self::${$key}[$keys[1]][$keys[2]] = $value;
					else if(x($keys[1])) self::${$key}[$keys[1]] = $value;
					else self::${$key} = $value;
				}*/
			}
			// Get
			else {
				if(!$static) {
					if(x($keys[3])) $value = $this->{$keys[0]}[$keys[1]][$keys[2]][$keys[3]];
					else if(x($keys[2])) $value = $this->{$keys[0]}[$keys[1]][$keys[2]];
					else if(x($keys[1])) $value = $this->{$keys[0]}[$keys[1]];
					else $value = $this->$key;
				}
				/*else { // Would have to declare values as 'statice', also the 'array' (where $keys) didn't work for me
					if(x($keys[3])) $value = self::${$key}[$keys[1]][$keys[2]][$keys[3]];
					else if(x($keys[2])) $value = self::${$key}[$keys[1]][$keys[2]];
					else if(x($keys[1])) $value = self::${$key}[$keys[1]];
					else $value = self::${$key};
				}*/
			}
			
			// Return
			return $value;
		}
		
		/**
		 * Anaylzes current file for basic information.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function analyze($c = NULL) {
			// Error
			if(!$this or !file::value('file')) return;
			
			// Basic file info
			file::value('name',file::call('name'));
			file::value('path',str_replace(file::value('name'),'',file::value('file')));
			file::value('extension',file::call('extension'));
			file::value('extension_standardized',file::call('extension_standardized'));
			file::value('type',file::call('type'));
			file::value('file_changed',0);
			
			// Exists?
			file::value('exists',file::call('exists',file::value('file')));
			
			// Image
			if(file::value('type') == "image") {
				// Resource // Butt load of memory (7MB/image) which we dont' waant to use. We'll just call up $this->image_resource() when we need it.
				//file::value('image_resource',file::call('image_resource'));
			}
			
			// Debug
			debug("name: ".file::value('name'),file::value('c.debug'));
			debug("path: ".file::value('path'),file::value('c.debug'));
			debug("extension: ".file::value('extension'),file::value('c.debug'));
			debug("extension_standardized: ".file::value('extension_standardized'),file::value('c.debug'));
			debug("type: ".file::value('type'),file::value('c.debug'));
			debug("exists: ".file::value('exists'),file::value('c.debug'));
		}
	
		/**
		 * Returns path to root of the website on the server.
		 *
		 * @return string The path to the root of the website on the server.
		 */
		/*static*/ function server() {
			// Instance
			$server = file::value('c.server');
			
			// Global - sometimes, even if we're in an instance, $this->c['server'] doesn't get set...not sure why, but happens on GoDaddy servers
			if(!$server) $server = SERVER;
			
			// Return
			return $server;
		}
	
		/**
		 * Returns root domain of the website.
		 *
		 * @return string The root domain of the website.
		 */
		/*static*/ function domain() {
			// Instance
			$domain = file::value('c.domain');
			
			// Global - sometimes, even if we're in an instance, $this->c['domain'] doesn't get set...not sure why, but happens on GoDaddy servers
			if(!$domain) $domain = D;
			
			// Return
			return $domain;
		}
	
		/**
		 * Determines the temp file directory on the server.
		 *
		 * @return string The path to the temp directory.
		 */
		/*static*/ function temp() {
			// Saved
			if($temp = file::value('temp')) {
				return $temp;
			}
			
			// Env variables
			if($temp = getenv('TMP')) $return = $temp;
			if($temp = getenv('TEMP')) $return = $temp;
			if($temp = getenv('TMPDIR')) $return = $temp;
			
			// Detect by creating a temp file.
			$temp = tempnam(__FILE__,'');
			if(file_exists($temp)) {
				unlink($temp);
				$return = dirname($temp);
			}
			
			// Real path
			if($return) $return = realpath($return);
			
			// Add trailing slash
			if(substr($return,-1) != "/") $return .= "/";
			
			// Save
			file::value('temp',$return);
			
			// Return
			return $return;
		}
		
		/**
		 * Cleans up the given file, removing query string, etc.
		 *
		 * @param string $file The path the file you want to clean up. Defaults to current instance's file.
		 * @return string The cleaned up file.
		 */
		/*static*/ function clean($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Strip query string - keeping query string for now, needed often (private facebook images for example). Striping it away in the name() method though.
			//list($file,$query) = explode('?',$file,2);
			
			// Return
			return $file;
		}
		
		/**
		 * Determines if the path of the given file contains a 'storage' path. Example: amazons3::/path/to/file.jpg. Returns: amazons3.
		 *
		 * @param string $file The path to the file you want to get the 'storage' of. Defaults to the global $this->file.
		 * @return string The name of the storage used by this file. If none in the file, it uses the globally defined storage value.
		 */
		/*static*/ function storage($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			
			// In file string
			if(strstr($file,'::')) {
				list($storage,$file) = explode('::',$file);
			}
			// Use default
			if(!$storage) $storage = file::value('c.storage');
			
			// Return
			return $storage;
		}
		
		/**
		 * Localizes a file, making sure it has a full path and doesn't contain the domain.
		 *
		 * @param string $file The path to the file you want to localize. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function localize($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Remove domain from local URLs
			$search = array(
				str_replace('https://','http://',file::call('domain')), // http
				str_replace('http://','https://',file::call('domain')) // https
			);
			$file = str_replace($search,'',$file);
			
			// Localize path
			if(!strstr($file,file::call('server')) and substr($file,0,4) != "http") {
				// Check it's not a 'temp' file
				if(!strstr($file,file::call('temp'))) $file = file::call('server').$file;
			}
			
			// Return
			return $file;
		}
		
		/**
		 * Gets the full URL of a file (opposite of 'localize').
		 *
		 * @param string $file The path to the file you want to get the URL of. Defaults to the global $this->file.
		 * @return string The URL of the file.
		 */
		/*static*/ function url($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Remove server from file path
			$url = str_replace(file::call('server'),'',$file);
			
			// Escape
			$url = url_escape($url);
			
			// Add domain to file
			if(substr($file,0,4) != "http") {
				$url = file::call('domain').$url;
			}
			
			// Return
			return $url;
		}
		
		/**
		 * Checks if a file (local or external) exists.					
		 * 
		 * @param string $file The file you want to check exists. Defaults to the global $this->file.
		 * @param boolean $external Do you want to check it even if it's an external link (takes longer). If no, we'll automatically return true for external URLs. Default = 0
		 * @return boolean Whether or not the file exists.
		 */
		/*static*/ function exists($file = NULL,$external = 0) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return 0;
			
			// Localize
			$file = file::call('localize',$file);
			
			// Local file
			if(substr($file,0,4) != "http") {
				if(is_file($file)) return 1;
				//else if(is_dir($file)) return 1; // Must call $file->directory_exists() for this
			}
			
			// External file
			else if($external == 0) return 1; // Takes too long, just return true if not local (unless we REALLY need to know)
			else {
				$file = str_replace(' ','%20',$file);
				$file_parts = @parse_url($file);
			
				if(empty($file_parts["host"])) return 0;
				if(!empty($file_parts["path"])) $documentpath = $file_parts["path"];
				else $documentpath = "/";
			
				if(!empty($file_parts["query"])) $documentpath .= "?" . $file_parts["query"];
			
				$host = $file_parts["host"];
				$port = $file_parts["port"];
				
				// Now (HTTP-)GET $documentpath at $host";
				if(empty($port)) $port = "80";
				$socket = @fsockopen($host, $port, $errno, $errstr, 30);
				if(!$socket) return 0;
				else {
					fwrite ($socket, "HEAD ".$documentpath." HTTP/1.0\r\nHost: $host\r\n\r\n");
					$http_response = fgets($socket, 22);
				   
					if(ereg("200 OK", $http_response, $regs)) {
						return 1;
						fclose($socket);
					}
					else {
						// echo "HTTP-Response: $http_response<br>";
						return 0;
					}
				}
			}
			
			// Default
			return 0;
		}
		
		/**
		 * Checks to see whether a file has content or not.
		 *
		 * Note, it doesn't consider white space or comments as content (though you can consider comments by passing 'comments' => 1 in the $c array).								
		 * 
		 * @param string $file The path to the file we want to chedk for content.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return boolean Whether of not the file has content.
		 */
		/*static*/ function exists_with_content($file = NULL,$c = NULL) {
			if(is_array($file)) { // exists_with_content($c)
				$c = $file;
				$file = NULL;	
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if(!x($c[comments])) $c[comments] = 0; // Count comments as content
			
			// Localize
			$file = file::call('localize',$file);
			
			// Content
			$content = file::call('get',$file);
			
			// No Comments
			if(!$c[comments]) {
				$content = preg_replace('/\s/','',$content);
				$content = preg_replace('!/\*.*?\*/!s','',$content);
				$content = preg_replace('/\n\s*\n/',"\n",$content);
			}
			
			// Return
			if(x($content)) return 1;
			else return 0;
		}
		
		/**
		 * Rename's file if it already exists (appending with _1 or _2 etc.).
		 *
		 * @param string $file The path the file you want to rename if it already exists. Defaults to the global $this->file.
		 * @return string The unique file name.
		 */
		/*static*/ function unique($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Exists?
			if(file::call('exists',$file)) {
				// Parts
				$extension = file::call('extension',$file);
				$name = file::call('name',$file,0);
				$path = str_replace($name.".".$extension,'',$file);
				
				// Append with _x until it's unique
				for($x = 1;$x < 99999999;$x++) {
					$file = $path.$name."_".$x.".".$extension;
					if(!file::call('exists',$file)) $x = 99999999;
				}
			}
			
			// Return
			return $file;
		}
		
		/**
		 * Determines and localizes the full path of the file, including the name of the file.
		 *
		 * If the path doesn't contain a name, we'll use the given $file's (or global $this->file's) name.
		 *
		 * @param string $path The path we want to save the file to.
		 * @param string $file The original file, we use this for the 'name' of the file if the path doesn't contain a name
		 * @return string The path we want to save the file, loacalized and with name.
		 */
		/*static*/ function path($path = NULL,$file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
				
			// No path
			if(!$path) $path = file::value('file'); // Use original file
			else {
				$name = file::call('name',$path); // Name is in path. Example: /path/to/image.jpg
				if(!$name and $file) { // Name isn't in path. Example: /path/to/. Use original $file's name.
					$name = file::call('name',$file);
					if(substr($path,-1) != "/") $path .= "/";
					$path .= $name;
				}
			}
			
			// Clean
			$path = file::call('clean',$path);
			// Localize
			$path = file::call('localize',$path);
			
			// Return
			return $path;
		}
		
		/**
		 * Determines the 'type' of file this is based upon its extension: image, video, audio, or file [default].
		 *
		 * @param string The file you want to get the standaradized extension of. Defaults to the global $this->file.
		 * @return string The 'type' of file this is: image, video, audio, or file [default].
		 */
		/*static*/ function type($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Extension
			$extension = file::call('extension_standardized',$file);
			
			// Types - Need to do here as we're calling it statically (file::type()) and so we don't __construct which is where these were being se.
			$types = file::value('types');
			if(!$types) $types = array(
				'image' => g('config.uploads.types.image.accept'),
				'video' => g('config.uploads.types.video.accept'),
				'audio' => g('config.uploads.types.audio.accept')
			);
			
			// Match extension
			foreach($types as $t => $e) {
				if(in_array($extension,$e)) {
					$type = $t;	
					break;
				}
			}
			// Unknown extension, assume 'file' type
			if(!$type) $type = 'file';
			
			return $type;
		}
		
		/**
		 * Returns the base name of the given file. Example: "/path/to/file.jpg" would return "file.jpg".
		 *
		 * @param string|boolean $file The file we want to get the base name of. If we want to use the file stored in this instance ($this->file) we can pass $extension here. Defaults to the global $this->file.
		 * @param boolean $extension Do you want to return the extension? Default = 1
		 * @return string The base name of the file.
		 */
		/*static*/ function name($file = NULL,$extension = 1) {
			if(is_bool($file) or $file === 0) { // name($extension)
				$extension = $file;
				$file = NULL;	
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Extension
			$e = file::call('extension',$file);
			if($e) {
				// Remove query string
				list($file,$query) = explode('?',$file,2);
				// Include Extension (if exists)
				if($extension == 1 or !$e) $name = basename($file);
				// Don't include extension
				else $name = basename($file,".".$e);
			}
		
			// Return
			return $name;
		}
		
		/**
		 * Cleans up the given file name, removing any problematic characters.
		 *
		 * @param string $name The file name you want to clean up.
		 * @return string The cleaned up file.
		 */
		/*static*/ function name_clean($name) {
			// Save extension
			$extension = file::call('extension',$name);
			// Remove extension from name
			$name = file::call('name',$name,0);
			
			// Replace periods and spaces with underscores
			$search = array('.',' ');
			$replace = array('_','_');
			$name = str_replace($search,$replace,$name);
			
			// Remove all other non alpha-numeric characters (except _ and -)
			$name = preg_replace('/[^a-z0-9_\-]/i','',$name);
			
			// Restore extension
			$name .= ".".$extension;
			
			// Return
			return $name;
		}
		
		/**
		 * Returns the extension of the file. Example: "/path/to/file.jpg" would return "jpg".
		 *
		 * @param string $file The path to the file you want to get the extensions of. Defaults to the global $this->file.
		 * @return string The extension of the file.
		 */
		/*static*/ function extension($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			$file_base = basename($file);
			if(strstr($file_base,'.')) $extension = trim(substr(strrchr($file_base, '.'),1));
			
			return $extension;
		}
		
		/**
		 * Returns standardized extension (e.g. jpeg instead of jpg).
		 *
		 * @param string The file you want to get the standaradized extension of. Defaults to the global $this->file.
		 * @return string The standardizedextension of the file.
		 */
		/*static*/ function extension_standardized($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			/* Using mime - By Roshan Bhattara: http://roshanbh.com.np */
			/*// Get the detail of the image
			$imageinfo = getimagesize($file);
			switch($imageinfo['mime']) {
				// Create the image according to the content type
				case "image/jpg":
				case "image/jpeg":
				case "image/pjpeg": //for IE
					$extension_standardized = "jpeg";
					break;
				case "image/gif":
					$extension_standardized = "gif";
					break;
				case "image/png":
				case "image/x-png": //for IE
					$extension_standardized = "png";
					break;
				default:
					$extension_standardized = file::call('extension',$file);
			}
			
			/* Using extension */
			$extension = file::call('extension',$file);
			switch($extension) {
				// Create the image according to the content type
				case "jpg":
					$extension_standardized = "jpeg";
					break;
				default:
					$extension_standardized = $extension;
			}
			
			// Lowercase
			$extension_standardized = strtolower($extension_standardized);
			
			// Return
			return $extension_standardized;
		}
		
		/**
		 * Determines and returns the content type for the given file.											
		 * 
		 * @param string $file The file you want to get the content type of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function contenttype($file,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->contenttype($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if(!x($c['default'])) $c['default'] = "binary/octet-stream";
			
			if(file::value('storage') == 'local' and file::call('exists',$file)) {
				// finfo_file (PHP 5)
				if(function_exists('finfo_file')) {
					//$mimepath = '/usr/share/magic';
					$mime = finfo_open(FILEINFO_MIME/*,$mimepath*/);
					$type = finfo_file($mime,$file);
					finfo_close($mime);
				}
				// mime_content_type (depracated function)
				if(!$type and function_exists('mime_content_type')) {
					$type = mime_content_type($file);
				}
			}
			// Manual
			if(!$type) {
				$extension = file::call('extension',$file);
				switch($extension) {
					case "pdf": $type = "application/pdf"; break;
					case "exe": $type = "application/octet-stream"; break;
					case "zip": $type = "application/zip"; break;
					case "docx": 
					case "doc": $type = "application/msword"; break;
					case "xlsx": 
					case "xls": $type = "application/vnd.ms-excel"; break;
					case "pptx": 
					case "ppt": $type = "application/vnd.ms-powerpoint"; break;
					case "txt": $type = "text/plain"; break;
					case "gif": $type = "image/gif"; break;
					case "png": $type = "image/png"; break;
					case "jpeg": 
					case "jpg": $type = "image/jpg"; break;
					case "bmp": $type = "image/bmp"; break;
					case "mp3": $type = "audio/mpeg"; break;
					case "wav": $type = "audio/x-wav"; break;
					case "flv": $type = "video/x-flv"; break;
					case "avi": $type = "video/x-msvideo"; break;
					case "mpeg": 
					case "mpg": $type = "video/mpeg"; break;
					case "mov": $type = "video/quicktime"; break;
					case "mp4": $type = "video/mp4"; break;
					case "ogg": 
					case "ogv": $type = "video/ogg"; break;
					case "wmv": $type = "video/x-ms-wmv"; break;
					case "wemb": $type = "video/webm"; break;
					default : $type = "binary/octet-stream";
				}
			}
			
			// Clean - only want first bit, e.g. "video/mp4" in "video/mp4; charset=binary"
			list($type,$extra) = explode(';',$type);
			$type = trim($type);
			
			// Return
			return $type;
		}
		
		/**
		 * Returns the permission of the given file/directory (example: 0777) or, if the $permission param is passed, it sets the permission of the file/directory.	
		 *
		 * A bit slow (.15 sec) when using chmod_ftp. // Not currently implemented			
		 * 
		 * @param string $file The file/directory we want to get (and set if 2nd param) the permission of. Defaults to the global $this->file.
		 * @param int|string $permission The permission you wan to apply to the file/directory. Default = NULL
		 * @return int The resulting permission for the file/directory.
		 */
		/*statice*/ function permission($file,$permission = NULL) {
			// Params
			if(is_int_value($file)) { // $file->permission($permission);
				$permission = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Set permission
			if($permission) {
				$permission_current = substr(sprintf('%o', fileperms($file)), -4);
				if(substr($permission,0,1) != 0) $permission = "0".$permission; // Make sure it's 4 digits (0777 instead of 777)
				
				// Try chmod()
				if($permission_current != $permission) {
					chmod($file,octdec($permission));
					$permission_current = file::call('permission',$file);
				}
				// Try shell_exec("chmod")
				if($permission_current != $permission) {
					shell_exec("chmod ".substr($permission,1,3)." '".$file."'");
					$permission_current = file::call('permission',$file);
				}
				// Change owner, chmod // Don't know if 'apache' is right name, rarely works
				/*if($permission_current != $permission) {
					shell_exec("chown apache ".$file);
					shell_exec("chmod ".substr($permission,1,3)." ".$file);
					//$writeable = is_writable($file);
					$permission_current = file::call('permission',$file);
				}*/
				// Try chmod_ftp() // Not currently implemented
				/*if($permission_current != $permission) {
					chmod_ftp($file,$permission);
					$permission_current = file::call('permission',$file);
				}*/
			}
			
			// Get permission
			$permission = substr(sprintf('%o', fileperms($file)), -4);
			
			// Return
			return $permission;
		}
		
		/**
		 * Determines if (and tries to make) given file/directory writeable.
		 * 
		 * @param string $file Path the the file/directry you to determine if (and try to make) writeable. Defaults to the global $this->file.
		 * @return boolean Whether or not the file/directory is writeable.
		 */
		/*statice*/ function writeable($file) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Get/set permission
			$permission = file::call('permission',$file,"0777");
			
			// Writeable // $permission gets cached and doesn't always show correct permission (even if we did actually make it 0777) so we'll check is_writable too
			$writeable = is_writable($file);
			
			// Return
			if($writeable or $permission == "0777") return true; 
		}
		
		/**
		 * Returns filesize of given file (even if it's remote).							
		 * 
		 * @param string $file The file you want to get the filesize of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The size (in bytes) of the file.
		 */
		/*static*/ function size($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->size($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if(!$c[username]) $c[username] = NULL; // Username required for authorizing access to a remote file.
			if(!$c[password]) $c[password] = NULL; // Password required for authorizing access to a remote file.
			
			// Localize
			$file = file::call('localize',$file);
			
			// Exists?
			if(file::call('exists',$file)) {
				// Local
				if(substr($file,0,4) != "http") {
					$size = filesize($file);
				}
				// Remote
				else {
					ob_start();
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_HEADER, 1);
					curl_setopt($ch, CURLOPT_NOBODY, 1);
				
					// Authorize
					if($c[username] and $c[password]) {
						$headers = array('Authorization: Basic '.base64_encode($c[username].":".$c[password]));
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					}
				
					$ok = curl_exec($ch);
					curl_close($ch);
					$head = ob_get_contents();
					ob_end_clean();
				
					$regex = '/Content-Length:\s([0-9].+?)\s/';
					$count = preg_match($regex, $head, $matches);
					
					// Size
					$size = ($matches[1] ? $matches[1] : 0);
				}
			}
			
			// Return
			return $size;
		}
		
		/**
		 * Returns orientation number for the given file (or the global file if no $file is passed).
		 *
		 * 1: nothing
		 * 2: horizontally flipped, need to horizontally flip
		 * 3: rotated 180 rotate clockwise (or counter-clockwise, they're the same at this point), need to rotate 180 degrees counter-clockwise
		 * 4: vertically flipped, need to vertically flip
		 * 5: vertically flipped and rotated 90 degrees counter-clockwise, need to vertically flip and rotate 90 degrees clockwise
		 * 6: rotated 90 degrees counter-clockwise, need to rotate 90 degrees clockwise
		 * 7: horizontally flipped and rotated 90 degrees counter-clockwise, need to horizontally flip and rotate 90 degrees clockwise
		 * 8: rotated 90 degrees clockwise, need to rotate 90 degrees counter-clockwise
		 *
		 * @param string|array The file you want to get the orientation of or, if you're using the global $this->file, you can pass the $c param here. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The orientation number for the given file.
		 */
		/*static*/ function orientation($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->orientation($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Exists?
			if(file::call('exists',$file)) {
				// Get ID3
				$array = file::call('getid3',$file);
				if($array) {
					$orientation = $array[$array[fileformat]][exif][IFD0][Orientation];
				}
				// Exif Read Data
				else if(function_exists('exif_read_data')) {
					$array = exif_read_data($file);
					if(x($array[Orientation])) $orientation = $array[Orientation];
					else if(x($array[IFD0][Orientation])) $orientation = $array[IFD0][Orientation];
				}
				
				// Debug
				debug("<b>\$file->orientation($file);</b>",file::value('c.debug'));
				debug("orientation: ".$orientation,file::value('c.debug'));
				debug("array:".return_array($array),file::value('c.debug'));
			}
			
			// Return
			return $orientation;
		}

		/**
		 * Returns width/height of given file												
		 * 
		 * @param string $file The file you want to get the dimensions of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the width and height of the given file.
		 */
		/*static*/ function dimensions($file = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Cached?
			if($array = file::value('cache.dimensions')) {
				// Return
				return $array;	
			}
			
			// Config
			if(!x($c[type])) $c[type] = NULL; // Can specify the file type, otherwise we'll figure it out ourself: photo, video, audio, file
			
			// Localize
			$file = file::call('localize',$file);
			// Exists?
			if(file::call('exists',$file)) {
				// Type
				if(!$c[type]) $c[type] = file::call('type',$file);
				
				// Image
				if($c[type] == "image") {
					// Remote - http://www.php.net/manual/en/function.getimagesize.php#37531
					if(substr($file,0,4) == "http") {
						$contents = file::call('get',$file);
						$im = imagecreatefromstring($contents);
						if($im) {
							$array[width] = imagesx($im);
							$array[height] = imagesy($im);
							imagedestroy($im);
						}
					}
					// Local
					else {
						$results = getimagesize($file);
						$array[width] = $results[0];
						$array[height] = $results[1];
					}
				}
				// Video
				if($c[type] == "video") {
					$results = file::call('video_info',$file);
					$array[width] = $results[width];
					$array[height] = $results[height];
				}
			
				// Cache
				file::value('cache.dimensions',$array);
			}
				
			// Return
			return $array;
		}
		
		/**
		 * Returns width of given file												
		 * 
		 * @param string $file The file you want to get the width of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The width of the given file.
		 */
		/*static*/ function width($file = NULL,$c = NULL) {
			// Dimensions
			$dimensions = file::call('dimensions',$file,$c);
			// Return
			return $dimensions[width];
		}
		
		/**
		 * Returns height of given file												
		 * 
		 * @param string $file The file you want to get the height of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The height of the given file.
		 */
		/*static*/ function height($file = NULL,$c = NULL) {
			// Dimensions
			$dimensions = file::call('dimensions',$file,$c);
			// Return
			return $dimensions[height];
		}
		
		/**
		 * Returns length (in seconds) of given file (must be audio or video file).								
		 * 
		 * @param string $file The file you want to get the length of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The length of the given file.
		 */
		/*static*/ function length($file = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Config
			if(!$c[type]) $c[type] = file::call('type',$file);
			
			// Video
			if($c[type] == "video") {
				$results = file::call('video_info',$file);
				$length = $results[length];
			}
			
			// Return
			return $length;
		}
		
		/**
		 * Returns array of getid3 file data.
		 *
		 * @param string The file you want to get the getid3 file data of. Defaults to the global $this->file.
		 * @return array An array of data about the file.
		 */
		/*static*/ function getid3($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Exists?
			if(file::call('exists',$file)) {
				$class_path = file::call('server').'core/core/libraries/getid3/getid3/getid3.php';
				if(is_file($class_path)) {
					// Class
					require_once $class_path;
					$getID3 = new getID3;
					
					// Analyze
					$array = $getID3->analyze($file);
					
					// Return
					return $array;
				}
			}
		}
		
		/**
		 * Gets and returns the content of the given file.			
		 * 
		 * @param string $file The file you want to get the content of. Can be a path to a file on the server or an external URL.
		 * @return string The raw content of the file.
		 */
		/*static*/ function get($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Localize
			$file = file::call('localize',$file);
			
			// External URL
			if(substr($file,0,4) == "http") {
				// Encode spaces
				$file = str_replace(' ','%20',$file);
				
				// Get
				if(!strstr($file,"?")) { // http://test.com/image.jpg
					debug("using file_get_contents(".$file.")",file::value('c.debug'));
					$content = file_get_contents($file); 
				}
				else { // http://test.com/download.php?id=123
					debug("using curl()",file::value('c.debug'));
					$ch = curl_init($file);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					$content = curl_exec($ch);
					if(!$content) debug("curl error (".curl_errno($ch)."): ".curl_error($ch),file::value('c.debug'));
					curl_close($ch);
				}
				
				// Debug
				debug("content (1000 characters): <xmp>".substr($content,0,1000)."</xmp>",file::value('c.debug'));
			}
			// Local file
			else if(file::call('exists',$file)) {
				$content = file_get_contents($file);
			}
			
			// Return 
			return $content;
		}
		
		/**
		 * Saves given file content to the given path.	
		 * 
		 * @param string $path The path where you want to save the file content.
		 * @param string $content The content you want to save.
		 * @return string The full path to the saved content.
		 */
		/*static*/ function put($path,$content) {
			// Error
			if(!$path or !x($content)) return;
			
			// Localize
			$path = file::call('localize',$path);
			
			// Local path
			if(substr($path,0,4) != "http") {
				// Debug
				debug("file_put_contents(".$path.",\$content);",file::value('c.debug'));
				
				// Save
				file_put_contents($path,$content);
			}
			
			// Return
			return $path;
		}

		/**
		 * Uploads the given $_FILES['key'] array file to the given destination.						
		 * 
		 * @param array $file The array of file information as passed via PHP's $_FILES array.
		 * @param string $path The path where you want to save the file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The new full path of the uploaded file.
		 */
		/*static*/ function upload($file,$path,$c = NULL) {
			// Error
			if(!$file or !$path) return;
		
			if(is_array($file) and $file[name]) {
				// Clean
				if(file::value('upload.clean')) $file[name] = file::call('name_clean',$file[name]);
				
				// Debug
				debug("Passed uploaded file:".return_array($file),file::value('c.debug'));
				debug("Uploading file to path: ".$path.$file[name],file::value('c.debug'));
				debug("Moving file (".$file[tmp_name]." -> ".$path.$file[name].")..",file::value('c.debug'));
				
				// Move file to temp path
				if(move_uploaded_file($file[tmp_name],$path.$file[name])) {
					// File
					$file_string = $path.$file[name];
					
					// Debug
					if(file::call('exists',$path.$file[name])) debug("..and file now exists",file::value('c.debug'));
					else debug("..but file does NOT now exist",file::value('c.debug'));
				}
				// Move failed, try our own move() method
				else {
					// Debug
					debug("..but file move failed. Trying \$this->move() instead..",file::value('c.debug'));
					
					// Move
					$f = file::load($file[tmp_name]);
					$f->move($path.$file[name]);
					if(file::call('exists',$path.$file[name])) {
						// File
						$file_string = $path.$file[name];
						
						// Debug
						debug("..and file now exists",file::value('c.debug'));
					}
					else debug("..but that failed too.",file::value('c.debug'));
				}
			}
			
			// Return
			return $file_string;
		}

		/**
		 * Prepares the the source file ($this->file) and destination path ($destination) for saving.			
		 * 
		 * @param string $source The source file we'll be saving to the destination. Most of the time this will use the $this->file and you'll pass the $path here.
		 * @param string|array $destination The path to the destination you want to save the file to or, if you passed $destination in the first param or you're overwritting the original file, you can pass the $c param here. Default = NULL
		 * @param array $c An array of configuration values. Mostly this is used with image_svae(), see that method for more information. Default = NULL
		 * @return array An array containing both the 'source' file object and 'destination' file object.
		 */
		/*static*/ function prepare($source = NULL,$destination = NULL,$c = NULL) {
			// Params
			if(!is_string($destination)) { // $file->prepare($destination,$c);
				$c = $destination;
				$destination = $source;
				$source = file::value('file');
			}
			if(is_array($destination)) { // $file->prepare($c);
				$c = $destination;
				$destination = NULL;
			}
			
			// Debug
			debug("<b>\$file->prepare(".$source.",".$destination.")</b>",file::value('c.debug'));
			debug("config:".return_array($c),file::value('c.debug'));
			
			// Error
			if(!$source) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Source
			$array[source] = file::call('prepare_source',$source,$destination);
			// Destination
			$array[destination] = file::call('prepare_destination',$source,$destination);
			
			// Return
			return $array;
		}

		/**
		 * Prepares the the source file ($this->file) for saving.			
		 * 
		 * @param string $source The source file we'll be saving to the destination. Most of the time this will use the $this->file and you'll pass the $path here.
		 * @param string|array $destination The path to the destination you want to save the file to or, if you passed $destination in the first param or you're overwritting the original file, you can pass the $c param here. Default = NULL
		 * @param array $c An array of configuration values. Mostly this is used with image_svae(), see that method for more information. Default = NULL
		 * @return object The source object.
		 */
		/*static*/ function prepare_source($source = NULL,$destination = NULL,$c = NULL) {
			// Params
			if(!$destination and !$c or is_array($destination)) { // $file->prepare_source($destination) or $file->prepare_source($destination,$c); // Source will be current file
				$c = $destination;
				$destination = $source;
				$source = file::value('file');
			}
			if(is_array($destination)) { // $file->prepare_source($c); // Overwriting current file so $source and $destination will be the same
				$c = $destination;
				$destination = NULL;
			}
			
			// Error
			if(!$source) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Source
			$source_object = file::load($source,array('storage' => file::value('c.storage')));
			
			// Return
			return $source_object;
		}

		/**
		 * Prepares the the $destination for saving, making sure directory exists, we're not overwriting, etc.					
		 * 
		 * @param string $source The source file we'll be saving to the destination. Most of the time this will use the $this->file and you'll pass the $destination here.
		 * @param string|array $destination The path to the destination you want to save the file to or, if you passed $destination in the first param or you're overwritting the original file, you can pass the $c param here. Default = NULL
		 * @param array $c An array of configuration values. Mostly this is used with image_svae(), see that method for more information. Default = NULL
		 * @return object The destination object.
		 */
		/*static*/ function prepare_destination($source = NULL,$destination = NULL,$c = NULL) {
			// Params
			if(!$destination and !$c or is_array($destination)) { // $file->prepare_destination($destination) or $file->prepare_destination($destination,$c); // Overwriting current file so source and destination will be the same
				$c = $destination;
				$destination = $source;
				$source = $source;
			}
			if(is_array($destination)) { // $file->prepare_destination($c);
				$c = $destination;
				$destination = NULL;
			}
			
			// Error
			if(!$source) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Destination
			$destination_object = file::load(NULL,array('storage' => file::value('c.storage')));
			$destination = $destination_object->path($destination,$source);
			$destination_object->file = $destination;
			$destination_object->analyze();
			
			// Path root exists?
			$destination_exists = $destination_object->directory_exists($destination_object->path);
			if(!$destination_exists) {
				// No, try to create it
				debug("Creating ".$destination_object->path." since it didn't exist",file::value('c.debug'));
				$destination_object->directory_create($destination_object->path);
				$destination_exists = $destination_object->directory_exists($destination_object->path);
			}
			// Yes
			if($destination_exists) {
				// Don't overwrite
				if(!file::value('c.overwrite')) {
					$destination = $destination_object->unique();
					if($destination != $destination_object->file) {
						$destination_object = file::load($destination,array('storage' => file::value('c.storage')));
					}
				}
			
				// Debug
				//debug("Destination object: ".return_array($destination_object),file::value('c.debug'));
				
				// Return
				return $destination_object;
			}
			else {
				// Debug
				debug("Couldn't create ".$destination_object->path." directory.",file::value('c.debug'));
			}
		}

		/**
		 * Saves a file to a new destination or, if no $destination, it overwrites the original file.							
		 * 
		 * @param string|array $destination The path you want to save the file to or, if you're overwritting the original file, you can pass the $c param here. Default = NULL
		 * @param array $c An array of configuration values. See image_save() for more information. Default = NULL
		 * @return object The object for the saved file, if we successfully save it (nothing is returned if there was an error).
		 */
		function save($destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Config
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->save(".$destination.")</b>",file::value('c.debug'));
			debug("config:".return_array($c),file::value('c.debug'));
			
			// Prepare
			$array = file::call('prepare',$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Saving ".$array[source]->file." to ".$array[destination]->file." (type: ".$array[source]->type.")",file::value('c.debug'));
				
				// Needs saving?
				if(file::value('file_changed') or $array[source]->file != $array[destination]->file) {
					// Save - local image - must use this as we may have altered the image ($this->image_resource) and need to save that, not just copy the original
					if($array[source]->type == "image") {
						// Image save
						file::call('image_save',$array[destination]->file,$c);
					}
					// Save - default
					else {
						// Local source
						if(substr($array[source]->file,0,4) != "http") {
							// Copy
							copy($array[source]->file,$array[destination]->file);
							
							// Debug
							debug("copy(".$array[source]->file.",".$array[destination]->file.");",file::value('c.debug'));
						}
						// Remote source
						else {
							// Get file contents
							$content = file::call('get',$array[source]->file);
							
							// Save file contents
							file::call('put',$array[destination]->file,$content);
						}
						
						// Permissions - only do if we specifically defined permission to set
						if($c[permission]) file::call('permission',$array[destination]->file,$c[permission]);
					}
				}
					
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Saved file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Saves a file to the temp folder.
		 * 
		 * @param string $file The file you want to save to the temporary folder. Can be a URL, a path to a file on the server, or the raw content of a file. Defaults to the global $this->file.
		 * @param string $name The name you want to give to the file. Defaults to temp_{microtime}.txt
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the temporary file.
		 */
		/*static*/ function save_temp($file = NULL,$name = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Localize
			$file = file::call('localize',$file);
			
			// Contents
			$content = file::call('get',$file); // Passed a URL or path to a file on the server
			if($content) {
				if(!$name) $name = file::call('name',$file); // Use file's name
			}
			else { // Passed the raw contents of a file
				$content = $file;
			}
			
			// Name
			if(!$name) $name = "temp_".str_replace(' ','_',microtime()).".txt";
			
			// Temporary path
			$path = file::call('temp').$name;
			
			// Save contents
			file::call('put',$path,$content);
	
			// Return
			return $path;
		}

		/**
		 * Copies a file to another new destination (works with remote files as well).							
		 * 
		 * @param string $destination The path where you want to save the copied file, including the file's name, though we'll default to the original file's name if the path contains none.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the copied file, if we successfully copied it (nothing is returned if there was an error).
		 */
		function copy($destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Config
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->copy(".$destination.")</b>",file::value('c.debug'));
			debug("config:".return_array($c),file::value('c.debug'));
			
			// Prepare
			$array = file::call('prepare',$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Copying ".$array[source]->file." to ".$array[destination]->file."",file::value('c.debug'));
				
				// Needs copying?
				if(file::value('file_changed') or $array[source]->file != $array[destination]->file) {
					// Local source
					if(substr($array[source]->file,0,4) != "http") {
						// Copy
						copy($array[source]->file,$array[destination]->file);
						
						// Debug
						debug("copy(".$array[source]->file.",".$array[destination]->file.");",file::value('c.debug'));
					}
					// Remote source
					else {
						// Get file contents
						$content = file::call('get',$array[source]->file);
						
						// Save file contents
						file::call('put',$array[destination]->file,$content);
					}
					
					// Permissions - only do if we specifically defined permission to set
					if($c[permission]) file::call('permission',$array[destination]->file,$c[permission]);
				}
				
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Copied file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Moves file from one location to another, checking name is unique, etc. along the way.						
		 * 
		 * @param string $destination The path where you want to move the file, including the file's name, though we'll default to the original file's name if the path contains none.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the moved file, if we successfully moved it (nothing is returned if there was an error).
		 */
		function move($destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Config
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->move(".$destination.")</b>",file::value('c.debug'));
			debug("config:".return_array($c),file::value('c.debug'));
			
			// Prepare
			$array = file::call('prepare',$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Moving ".$array[source]->file." to ".$array[destination]->file,file::value('c.debug'));
				
				// Needs moving?
				if($array[source]->file != $array[destination]->file) {
					// Local source
					if(substr($array[source]->file,0,4) != "http") {
						// Move
						rename($array[source]->file,$array[destination]->file);
						
						// Debug
						debug("rename(".$array[source]->file.",".$array[destination]->file.");",file::value('c.debug'));
					}
					// Remote source
					else {
						#build this
					}
					
					// Permissions - only do if we specifically defined permission to set
					if($c[permission]) file::call('permission',$array[destination]->file,$c[permission]);
				}
				
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Moved file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}
		
		/**
		 * Deletes a file.
		 *
		 * @param string $file The path to the file you want to delete. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function delete($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->delete($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return; 
			
			// Localize
			$file = file::call('localize',$file);
			
			// Exists?
			if(file::call('exists',$file)) {
				// Yes, delete
				unlink($file);
			}
		}
		
		/**
		 * Initializes download of a file.
		 *
		 * Note, must be called before anything has been outputted.
		 *
		 * @param string $file The path to the file you want to force the downloading of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function download($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->download($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return; 
			
			// Localize
			$file = file::call('localize',$file);
			
			// Config
			if(!x($c[name])) $c[name] = file::call('name',$file);
			if(!x($c[size])) $c[size] = file::call('size',$file);
			if(!x($c[contenttype])) $c[contenttype] = file::call('contenttype',$file,array('default' => "application/force-download"));
			$name = file::call('name',$file);
			
			// Required for IE, otherwise Content-disposition is ignored
			if(ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
	
			// Header
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers 
			header("Content-Type: ".$c[contenttype]);
			header("Content-Disposition: attachment; filename=\"".$c[name]."\";" );
			header("Content-Transfer-Encoding: binary");
			if($c[size]) header("Content-Length: ".$c[size]);
			
			// File
			readfile($file);
			//print file_get_contents($file);
			//print curl($file);
			
			// Exit
			exit;
		}

		/**
		 * Pushes the given $source to the given $destination on the external storage service.
		 *
		 * This is just a placeholder method. Not used in this class, but will be used in external storage extensions of this class.					
		 * 
		 * @param string $source The full path to the local source file.
		 * @param string $destination The path on the external storage where you want to push this file. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the external file if we successfully pushed (nothing is returned if there was an error).
		 */
		function push($source,$destination = NULL,$c = NULL) {
			// Return
			return;
		}

		/**
		 * Pulls the current file from external storage servicc to the local $destination.
		 *
		 * This is just a placeholder method. Not used in this class, but will be used in external storage extensions of this class.					
		 * 
		 * @param string $destination The local path where you want to save the pulled file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the local file if we successfully pulled it (nothing is returned if there was an error).
		 */
		function pull($destination,$c = NULL) {
			// Return
			return;
		}
		
		/**
		 * Determines if the given directory exists.
		 *
		 * @param string $directory The path to the directory you want to check for.
		 * @return boolean Whether or not the directory exists.
		 */
		/*static*/ function directory_exists($directory) {	
			// Error
			if(!$directory) return;
			
			// Return
			return is_dir($directory);
		}
		
		/**
		 * Returns an array of items (files and directories) within the given directory.
		 *
		 * @param string $directory The directory you want to look for items in.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_items($directory,$c = NULL) {	
			// Error
			if(!$directory) return;
			
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($directory and substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
			if($c[paths] == "full") $c[prefix] = $directory.$c[prefix];
				
			// Open directory
			if($handle = opendir($directory)) {
				// Get entries
				while(false !== ($entry = readdir($handle))) {
					// Skip navigation entries (up one level, etc.)
					if(!in_array($entry,array(".",".."))) {
						// File
						if($c[files]) {
							// Entry is a file (and not a system file), add to array
							if(is_file($directory.$entry) and substr($entry,0,1) != ".") {
								$array[] = $c[prefix].$entry;
							}
						}
						// Directory
						if($c[directories] or $c[recursive]) {
							// Entry is a directory, add to array
							if(is_dir($directory.$entry)) {
								// Look for items inside (recursive)
								$sub_array = NULL;
								if($c[recursive] == 1) {
									$_c = $c;
									if(!$c[multilevel]) $_c[prefix] .= $entry."/";
									$sub_array = file::call('directory_items',$directory.$entry."/",$_c);
								}
								
								// Add to array
								if($c[recursive] and $c[multilevel]) $array[$c[prefix].$entry] = ($sub_array ? $sub_array : array());
								else {
									if($c[directories]) $array[] = $c[prefix].$entry;
									if($sub_array) $array = array_merge($array,$sub_array);
								}
							}
						}
					}
				}
			
				// Close directory
				closedir($handle);
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Returns an array of files within the given directory.
		 *
		 * @param string $directory The directory you want to look for files in.
		 * @param array $c An array of configuration values (see directory_items() for configuration options). Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_files($directory,$c = NULL) {
			$c[files] = 1;
			$c[directories] = 0;
			return file::call('directory_items',$directory,$c);
		}
		
		/**
		 * Returns an array of folders within the given directory.
		 *
		 * @param string $directory The directory you want to look for folders in.
		 * @param array $c An array of configuration values (see directory_items() for configuration options). Default = NULL
		 * @return array An array of folders in the directory.
		 */
		/*static*/ function directory_folders($directory,$c = NULL) {
			$c[files] = 0;
			$c[directories] = 1;
			return file::call('directory_items',$directory,$c);
		}
		
		/**
		 * Checks to see if a directory is writeable (and attempts to make it so if it isn't already).			
		 * 
		 * @param string $directory Path to the diretory you want to check the writeability of.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function directory_writeable($directory,$c = NULL) {
			// Error
			if(!$directory) return;
			
			// Config
			if(!x($c[recursive])) $c[recursive] = 0; // Check (and apply) writeable permission recursively on all sub-directories
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Variables
			if(substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
			
			// Writeable
			$return = file::call('writeable',$directory);
			
			// Sub-directories
			if($c[recursive] == 1 and $return) {
				if($folders = file::call('directory_folders',$directory)) {
					foreach($folders as $folder) {
						$writeable = file::call('directory_writeable',$directory.$folder."/",$c);
						if(!$writeable) $return = 0;
					}
				}	
			}
			
			// Return
			return $return;
		}
		
		/**
		 * Creates a directory and all missing parent directories, applying 0777 permissions
		 *
		 * @param string $directory The directory you want to create
		 * @param boolean $recursive Do you want to 'recursively' create this directory (meaning we'll create parent directories if they don't exist). Default = 1
		 */
		/*static*/ function directory_create($directory,$recursive = 1) {	
			// Recursive
			if($recursive) {
				if(PHP_MAJOR_VERSION >= 5) mkdir($directory,0777,true);
				else { // No 3rd param (recursive), need to do ourself
					$folder_string = NULL;
					$folders = explode('/',$directory);
					foreach($folders as $folder) {
						$folder_string .= $folder."/";
						if(!file_exists($folder_string)) {
							mkdir($folder_string,0777);
						}
					}
				}
			}
			// Non-recursive
			else {
				mkdir($directory,0777);
			}
		}
		
		/**
		 * Deletes a directory's content and (optionally) the directory itself
		 * 
		 * http://www.roscripts.com/snippets/show/170
		 * 
		 * @param string $directory The directory whose contents (and possibly self) we want to delete
		 * @param boolean $delete_directory Whether or not we want to delete the directory too (if not, we'll just delete the contents within it)
		 */
		/*static*/ function directory_delete($directory,$delete_directory = 1) {
			// Error
			if(!$directory) return;
			
			// Make sure ends with /
			if(substr($directory,-1) != "/") $directory .= "/";
			
			// Open directory
			if(!$dh = @opendir($directory)) return;
			
			// Read items
			while(false !== ($obj = readdir($dh))) {
				// Non-file/directory item
				if($obj == '.' || $obj == '..') continue;
				
				// Delete item
				if(is_dir($directory.$obj)) file::call('directory_delete',$directory.$obj,1);
				else unlink($directory.$obj);		
				/*if(!@unlink($directory.$obj)) { // simple, quicker, but invalid and throws lots of PHP errors
					file::call('directory_delete',$directory.$obj,1);
				}*/
			}
			
			// Close directory
			closedir($dh);
			
			// Remove directory
			if($delete_directory) {
				@rmdir($directory);
			}
		}
		
		/**
		 * Copies an entire directory.
		 * 
		 * http://codestips.com/php-copy-directory-from-source-to-destination/
		 * 
		 * @param string $source The directory you want to copy.
		 * @param string $destination The path you want to copy the directory to.
		 */
		/*static*/ function directory_copy($source,$destination) {
			if(is_dir($source)) {
				file::call('directory_create',$destination);
				$directory = dir($source);
				while(FALSE !== ($readdirectory = $directory->read())) {
					if($readdirectory == '.' || $readdirectory == '..' ) {
						continue;
					}
					$PathDir = $source.'/'.$readdirectory; 
					if(is_dir($PathDir)) {
						file::call('directory_copy',$PathDir,$destination.'/'.$readdirectory);
						continue;
					}
					copy($PathDir,$destination.'/'.$readdirectory);
				}
				$directory->close();
			}
			else {
				copy($source,$destination);
			}
		}
		
		/**
		 * Gets the resource of the given image. 
		 *
		 * @param string $file The path to the file you want to get the resource of. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function image_resource($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Memory
			ini_set("memory_limit","1000M");
			
			// Function
			$function = "imagecreatefrom".file::value('extension_standardized');
			// Resource
			if(function_exists($function)) $resource = $function($file);
			// No resource, check if it's another image type (and was perhaps saved incorrectly)
			if(!$resource) {
				$extensions = array('jpeg','png','gif','bmp');
				foreach($extensions as $e) {
					$function = "imagecreatefrom".$e;
					if(function_exists($function)) $resource = $function($file);
					// Yes, it's another image type. Update the extension
					if($resource) {
						file::value('extension',$e);
						file::value('extension_standardized',$e);
						break;
					}
				}
			}
			
			// Return
			return $resource;
		}
		
		/**
		 * Creates a thumbnail image of the file.
		 * 
		 * @param int $width The maximum width of the resulting thumb.
		 * @param int $height The maximum height of the resulting thumb.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_thumb($width,$height,$c = NULL) {
			// Resource
			if(!file::value('image_resource')) file::value('image_resource',file::call('image_resource'));
			
			// Error
			if(!file::value('image_resource') or !$width or !$height) return;
			
			// Memory
			ini_set("memory_limit","1000M");
			
			// Config
			if(!x($c[crop])) $c[crop] = 0; // Crop the image to fit the specific size. If false, it'll keep the proportions, but shrink the image so that neither the width nor height is greater than the specified width/height.
			if(!x($c[enlarge])) $c[enlarge] = 0; // Enlarge image if smaller than desired thumbnail size
			if(!x($c[quality])) $c[quality] = 85; // Percent quality of resulting thumbnail
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->image_thumb(".$width.",".$height.",".file::value('c.crop').")</b>",file::value('c.debug'));
			debug("c:".return_array($c),file::value('c.debug'));
			
			// Resource
			$image_resource = file::value('image_resource');
			
			// Get Image Size
			$orig_w = imagesx($image_resource);
			$orig_h = imagesy($image_resource);
			
			// Crop
			if(file::value('c.crop') == 1) {
				// Bigger than thumb (or enlarging thumb)
				if($orig_w > $width or $orig_h > $height or file::value('c.enlarge') == 1) {
					if(file::value('c.enlarge') == 1 or $orig_w > $width) $new_w = $width;
					else $new_w = $orig_w;
					$diff = $orig_w / $new_w;
					$new_h = $orig_h / $diff;
					if($new_h > $height) {
						if(file::value('c.enlarge') == 1 or $orig_h > $height) $new_h = $height;
						else $new_h = $orig_h;
						$orig_h = $height * $diff;
						$y_diff = (imagesy($image_resource) - $orig_h) / 2;
					}
					if($new_h < $height) {
						if(file::value('c.enlarge') == 1 or $orig_h > $height) $new_h = $height;
						else $new_h = $orig_h;
						$diff = $orig_h / $new_h;
						$orig_w = $new_w * $diff;
						$x_diff = (imagesx($image_resource) - $orig_w) / 2;
					}
				}
				// Smaller than thumb (don't enlarge)
				else{
					$new_w = $orig_w;
					$new_h = $orig_h;
				}
			}
			// No crop
			else {
				$new_w = $width;
				if(file::value('c.enlarge') != 1 and $orig_w < $new_w) $new_w = $orig_w; // Image smaller than thumb size, don't enlarge
				$wdiff  = $orig_w / $new_w;
				$new_h = round($orig_h / $wdiff);
				if($new_h > $height) {
					$new_h = $height;
					$hdiff  = $orig_h / $new_h;
					$new_w = round($orig_w / $hdiff);
				}
			}
			
			// Debug
			debug("original width: ".$orig_w,file::value('c.debug'));
			debug("original height: ".$orig_h,file::value('c.debug'));
			debug("new width: ".$new_w,file::value('c.debug'));
			debug("new height: ".$new_h,file::value('c.debug'));
			debug("enlarge: ".file::value('c.enlarge'),file::value('c.debug'));
			debug("crop: ".file::value('c.crop'),file::value('c.debug'));
			debug("x diff: ".$x_diff,file::value('c.debug'));
			debug("y diff: ".$y_diff,file::value('c.debug'));
			
			// Create Blank Image
			$image_resource_new = imagecreatetruecolor($new_w,$new_h);
			
			// Transparency (.png images)
			if(file::value('extension_standardized') == "png") {
				debug('applying transparency to image',file::value('c.debug'));
				imagealphablending($image_resource_new,false);
				imagesavealpha($image_resource_new,true);
				imagealphablending($image_resource,true); // not 100% sure this necessary, but doesn't hurt
			}
			
			// Copy Image to Blank Image
			debug("imagecopyresampled($image_resource_new,$image_resource,0,0,$x_diff,$y_diff,$new_w,$new_h,$orig_w,$orig_h);",file::value('c.debug'));
			imagecopyresampled($image_resource_new,$image_resource,0,0,$x_diff,$y_diff,$new_w,$new_h,$orig_w,$orig_h);
			
			// Save
			file::value('image_resource',$image_resource_new);
			file::value('file_changed',1);
		}

		/**
		 * Reorientates and re-saves thie file if it's been rotated/flipped previously.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_reorientate($c = NULL) {
			if(!file::value('file')) return;
			
			// Orientation
			$orientation = file::call('orientation',$c);
			
			// Already orientated (or we don't know what it's orientation is)
			if($orientation == 1 or !$orientation) {
				file::call('copy');
			}
			// Flip horizontally
			if($orientation == 2) {
				file::call('image_flip',1);
			}
			// Rotate 180 degrees counter-clockwise (or clockwise, they're the same at this point)
			if($orientation == 3) {
				file::call('image_rotate',180);
			}
			// Flip vertically
			if($orientation == 4) {
				file::call('image_flip',2);
			}
			// Flip vertically and rotate 90 degrees clockwise
			if($orientation == 5) {
				file::call('image_flip',2);
				file::call('image_rotate',90);
			}
			// Rotate 90 degrees clockwise
			if($orientation == 6) {
				file::call('image_rotate',90);
			}
			// Flip horizontally and rotate 90 degrees clockwise
			if($orientation == 7) {
				file::call('image_flip',1);
				file::call('image_rotate',90);
			}
			// Rotate 90 degrees counter-clockwise
			if($orientation == 8) {
				file::call('image_rotate',-90);
			}
		}
	
		/**
		 * Rotates image given degress.
		 *
		 * @param int $degress The degress you want to rotate the image (clockwise).
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_rotate($degrees = 90,$c = NULL){
			// Resource
			if(!file::value('image_resource')) file::value('image_resource',file::call('image_resource'));
			
			// Error
			if(!file::value('image_resource') or !$degrees) return;
			
			// Rotate the image according to the spcified degree
			file::value('image_resource',imagerotate(file::value('image_resource'), (360 - $degrees), 0));
			file::value('file_changed',1);
		}
		
		/**
		 * Reorientates and re-saves a file if it's been rotated/flipped previously.
		 *
		 * @param int $type The type of flip you want to do. 1 = horizontal, 2 = veritical, 3 = both
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_flip($type,$c = NULL) {
			// Resource
			if(!file::value('image_resource')) file::value('image_resource',file::call('image_resource'));
			
			// Error
			if(!file::value('image_resource')) return;
			
			// Width
			$width = imagesx(file::value('image_resource'));
			// Height
			$height = imagesy(file::value('image_resource'));
			
			// Destination image
			$image_resource_new = imagecreatetruecolor($width,$height);
			 
			// Loop through horizontal pixels
			for($x = 0;$x < $width;$x++) {
				// Loop through vertical pixels
				for($y = 0;$y < $height;$y++) {
				  // Horizontal
				  if($type == 1) imagecopy($image_resource_new,file::value('image_resource'),$width - $x - 1,$y,$x,$y,1,1);
				  // Vertical
				  if($type == 2) imagecopy($image_resource_new,file::value('image_resource'),$x,$height - $y - 1,$x,$y,1,1);
				  // Both
				  if($type == 3) imagecopy($image_resource_new,file::value('image_resource'),$width - $x - 1,$height - $y - 1,$x,$y,1,1);
				}
			}
			 
			// Save
			file::value('image_resource',$image_resource_new);
			file::value('file_changed',1);
		}
		
		/**
		 * Saves the current $this->image_resource to the given $destination or, if no $destination given, it overwrites the original file.
		 *
		 * @param string $destination The path where you want to save the image, including the file's name, though we'll default to the original file's name if the path contains none.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_save($destination = NULL,$c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// File
			$file = file::value('file');
			
			// Params
			if(is_array($destination)) { // $file->image_save($c);
				$c = $destination;
				$destination = NULL;
			}
			
			// Config
			if(!x($c[quality])) $c[quality] = 100; // The percent quality you want to save the resulting image as. Note: only works with jpg images. 0 - 100
			if(!x($c[smush])) $c[smush] = 0; // 'Smush' image using Yahoo's SmushIt to reduce filesize without losing resolution (a bit slow, but recommended).
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->image_save(".$destination.")</b>",file::value('c.debug'));
			debug("c:".return_array($c),file::value('c.debug'));
			
			// Path
			$destination = file::call('path',$destination);
			
			// Function
			$function = "image".file::value('extension_standardized');
			if(!function_exists($function)) $function = "imagejpeg";
			
			// Changed?
			if(file::value('file_changed') or (file::value('c.quality') != 100 and $function == "imagejpeg")) {
				// Resource
				$resource = file::value('image_resource');
				if(!$resource) $resource = file::call('image_resource',$file);
				
				// Debug
				debug("Creating new image with ".$function." function",file::value('c.debug'));
				
				// Save - with quality
				if($function == "imagejpg") $function($resource,$destination,file::value('c.quality'));
				// Save - default
				else $function($resource,$destination);
					
				// Permissions - only do if we specifically defined permission to set
				if($c[permission]) file::call('permission',$destination,$c[permission]);
			}
			// Hasn't changed, but need to move
			else if($destination != $file) {
				// Debug
				debug("Image hasn't changed, going to use copy() instead.",file::value('c.debug'));
				
				// Copy
				file::call('copy',$destination,$c);
			}
			
			// Smush
			if(file::value('c.smush') == 1) {
				file::call('image_smush',$destination,$c);
			}
		}

		/**
		 * Uses Yahoo's smushit class to 'smush' the size of the image, stripping out uneeded headers, etc. without losing any quality.
		 *
		 * Note: it's a bit slow (2.5 seconds per call) need to speed up or run delayed.
		 *
		 * @param string $path The path to the file you want to smush.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_smush($path,$c = NULL) {
			// Default
			if(!$path) $path = file::value('file');
			// Error
			if(!$path) return;
			
			// Config
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->image_smush(".$file.")</b>",file::value('c.debug'));
			debug("c:".return_array($c),file::value('c.debug'));
			
			// Smush
			require_once file::call('server')."core/classes/smushit.php"; // Needs to be in quotes as we use SERVER as a constant
			$smush = new SmushIt;
			$smush->base = file::call('domain'); // Needs to be in quotes as we use DOMAIN as a constant
			$smush->smush($path,$path);
			
			// Results
			$results = array(
				'url' => $smush->url,
				'savings' => $smush->savings,
				'savings_perc' => $smush->savings_perc,
				'base' => $smush->base,
				'msg' => $smush->msg,			
				'src_image' => $smush->src_image,
				'res_image' => $smush->res_image
			);
			
			// Debug
			debug("\$file->image_smush() results: ".return_array($results),file::value('c.debug'));
		}
		
		/**
		 * Returns HTML for displaying an image.		 								
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function image_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!$c[alt]) $c[alt] = file::call('name'); // <img> tag's alt attribute (will default to base filename)
			if(!$c[attributes]) $c[attributes] = NULL; // Additional attributes to add to the <img> tag
			if(!$c[width]) $c[width] = NULL; // Width of image, not necessary, but speeds up page load time if passed
			if(!$c[height]) $c[height] = NULL; // Height of image, not necessary, but speeds up page load time if passed
			if(!$c[min_width]) $c[min_width] = NULL; // Minimum width to display image
			if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display image
			if(!$c[min_height]) $c[min_height] = NULL; // Minimum height to display image
			if(!$c[max_height]) $c[max_height] = NULL; // Maximum height to display image
			if(!x($c[timestamp])) $c[timestamp] = 0; // Append url with timestamp so browser won't use cached image
			if(!$c[link]) $c[link] = NULL; // Where we want the image to link to
			if(!$c[link_attributes]) $c[link_attributes] = NULL; // String of attributes to add to the link
			if(!x($c[debug])) $c[debug] = 0; // Debug
			if($c) file::call('c',$c);
			
			// Debug
			debug("<b>\$file->image_html();</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Dimensions
			if(!$c[width] and !$c[height]) {
				$dimensions = file::call('dimensions');
				$c[width] = $dimensions[width];
				$c[height] = $dimensions[height];
			}
			
			// Min width
			if($c[min_width] and $c[width] and $c[width] < $c[min_width]) {
				$c[height] = round(($c[min_width] * $c[height]) / $c[width]);
				$c[width] = $c[min_width];
			}
			// Min height
			if($c[min_height] and $c[height] and $c[height] < $c[min_height]) {
				$c[width] = round(($c[min_height] * $c[width]) / $c[height]);
				$c[height] = $c[min_height];
			}
			// Max width
			if($c[max_width] and $c[height] and $c[width] > $c[max_width]) {
				$c[height] = round(($c[max_width] * $c[height]) / $c[width]);
				$c[width] = $c[max_width];
			}
			// Max height
			if($c[max_height] and $c[width] and $c[height] > $c[max_height]) {
				$c[width] = round(($c[max_height] * $c[width]) / $c[height]);
				$c[height] = $c[max_height];
			}
				
			// URL
			$url = file::call('url');
			
			// Start link
			if($c[link]) $text .= "<a href='".$c[link].($c[timestamp] ? "?".time() : "")."'".($c[link_attributes] ? " ".$c[link_attributes] : "").">";
			
			// Image
			$text .= "<img src='".$url.($c[timestamp] ? "?".time() : "")."'".($c[width] && $c[height] ? " width='".$c[width]."' height='".$c[height]."'" : "").($c[alt] ? " alt='".string_encode($c[alt])."'" : "").($c[attributes] ? " ".$c[attributes] : "")." />";
			
			// End link
			if($c[link]) $text .= "</a>";
			
			// Debug
			debug("$file->image_html() result: <xmp>".$text."</xmp>",$c[debug]);
			
			// Return
			return $text;
		}
	
		/**
		 * Dynamically creates an image using given paramaters. 											
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the of the dynamically created image file.
		 */
		/*static*/ function image_dynamic($c = NULL) {
			/* To Do
			- Shadow needs to be fully integrated
			- Don't think we're getting exactly vertical (y) center
			*/
			// Config
			if(!$c[extension]) {
				if(!$c[extension] = file::call('extension',$c[save_path])) $c[extension] = "png"; // png, jpg, or gif
			}
			if(!$c[text]) $c[text] = NULL;
			if(!$c[text_size]) $c[text_size] = 20;
			if(!$c[text_color]) $c[text_color] = "000000"; // Defaults to black
			if(!$c[font]) $c[font] = file::call('server')."core/core/fonts/TimesNewRoman.ttf";
			if(!x($c[shadow])) $c[shadow] = 0; // Defaults to gray
			if(!$c[shadow_color]) $c[shadow_color] = "808080"; // Defaults to gray
			if(!$c[background_color]) $c[background_color] = "ffffff"; // Defaults to white
			if(!$c[quality]) $c[quality] = 100; // Image quality, 1-100 (only works with jpeg/bmp)
			if(!$c[width] and $c[text] and $c[text_size]) {
				$text_w = strlen($c[text]) * ($c[text_size] * .65);
				$c[width] = $text_w + 45; // Rough estimate of text with and a little extra padding, probably use imagettftext() instead
			}
			if(!$c[width]) $c[width] = 300;
			if(!$c[height]) $c[height] = 25;
			if(!x($c[transparent])) $c[transparent] = 0; // Make background transparent (currently only works with gif extension, need to make for png)
			if(!x($c[display])) $c[display] = 0; // Display image (as opposed to 'saving' it)
			if(!x($c[save])) $c[save] = 1; // Save image (as opposed to 'displaying' it)
			if(!$c[save_path]) $c[save_path] = file::call('server')."local/uploads/images/"; // Save path
			if(!$c[text_position]) $c[text_position] = "center"; // top-left, top-center, top-right, bottom-left, bottom-center, bottom-right, center-left, center-right, center (default). You can also pass specific text_position_top, text_position_right, text_position_bottom, and text_position_left coordinates.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Content Type (if displaying)
			if($c[display] == 1) header('Content-type: image/'.$c[extension]);
			
			// Create the image
			$im = imagecreatetruecolor($c[width], $c[height]);
			if($im) {
			
				// Create some colors
				$color_text_rgb = file::call('html2rgb',$c[text_color]);
				$color_shadow_rgb = file::call('html2rgb',$c[shadow_color]);
				$color_background_rgb = file::call('html2rgb',$c[background_color]);
				$color_text = imagecolorallocate($im, $color_text_rgb[0], $color_text_rgb[1], $color_text_rgb[2]);
				$color_shadow = imagecolorallocate($im, $color_shadow_rgb[0], $color_shadow_rgb[1], $color_shadow_rgb[2]);
				$color_background = imagecolorallocate($im, $color_background_rgb[0], $color_background_rgb[1], $color_background_rgb[2]);
				// Background Bug
				if($c[extension] == "gif" and $c[transparent] == 1) imagecolortransparent($im, $color_background); // Color not spot on for .gif images unless you do this
				// Fill Background
				imagefilledrectangle($im, 0, 0, ($c[width] - 1), ($c[height] - 1), $color_background);
				
				// Text
				if($c[text]) {
					// Font
					$font = file::call('localize',$c[font]);
				
					// Text Bounds
					$tb = imagettfbbox($c[text_size], 0, $font, $c[text]);
					$text_w = abs($tb[4]); 
					$text_h = abs($tb[5]); // Technically the height from the top of the highest character to the 'base-line' ($tb[1] is space below 'base-line')
					
					// Debug
					debug("text bounds: ".return_array($tb),$c[debug]);
					debug("text w = ".$text_w.", text h = ".$text_h,$c[debug]);
					
					// Position - defined x/y
					if($c[text_position_top] or $c[text_position_right] or $c[text_position_bottom] or $c[text_position_left]) {
						// X
						if(x($c[text_position_left])) $x = $c[text_position_left];
						else if(x($c[text_position_right])) $x = $c[width] - $text_w - $c[text_position_right];
						// Y
						if(x($c[text_position_top])) $y = $c[text_position_top] + $text_h;
						else if(x($c[text_position_bottom])) $y = $c[height] - abs($tb[1]) - $c[text_position_bottom]; // Include space 'below line' ($tb[1]), ex: the bottom curl of "g"
					}
					// Position - predefined location
					else {
						// X
						if(in_array($c[text_position],array("top-left","center-left","bottom-left"))) $x = 0;
						if(in_array($c[text_position],array("top-right","center-right","bottom-right"))) $x = $c[width] - $text_w;
						if(in_array($c[text_position],array("top-center","center","bottom-center"))) $x = ceil(($c[width] - $text_w) / 2);
						// Y
						if(in_array($c[text_position],array("top-left","top-center","top-right"))) $y = $text_h;
						if(in_array($c[text_position],array("bottom-left","bottom-center","bottom-right"))) $y = $c[height] - abs($tb[1]); // Include space 'below line' ($tb[1]), ex: the bottom curl of "g"
						if(in_array($c[text_position],array("center-left","center","center-right"))) $y = floor($c[height] / 2) + floor($text_h / 2) - ceil($c[height] * .03); // Move it up a tad as it 'looks' center if slightly higher
					}
					debug("w = ".$c[width].", h = ".$c[height].", x = ".$x.", y = ".$y."",$c[debug]);
					
					// Shadow
					if($c[shadow] == 1) imagettftext($im, 20, 0, 11, 21, $color_shadow, $font, $c[text]);
				
					// Add the text
					imagettftext($im, $c[text_size], 0, $x, $y, $color_text, $font, $c[text]); // Image, font size, rotation, left, top, font color, font ttf file, text
				}
				
				// Display
				if($c[display] == 1) {
					// Create Image
					if($c[extension] == "png") imagepng($im);
					if($c[extension] == "jpg" or $c[extension] == "jpeg" or $c[extension] == "bmp") imagejpeg($im);
					if($c[extension] == "gif") imagegif($im);
				}
				
	
				// Save
				if($c[save] == 1 and $c[save_path]) {
					// Path
					$path = file::call('path',$c[save_path]);
					$name = file::call('name',$path); // Get file name
					if(!$name) { // No name, create one
						$name = time().".".$c[extension];
						$path .= $name;
					}
					
					// Create Image
					if($c[extension] == "png") imagepng($im,$path);
					if($c[extension] == "jpg" or $c[extension] == "jpeg" or $c[extension] == "bmp") imagejpeg($im,$path,$c[quality]);
					if($c[extension] == "gif") imagegif($im,$path);
				}
				
				// Destory 
				/*if($c[display] == 1)*/ imagedestroy($im);
			}
			
			// Return
			return $path;
		}
	
		/**
		 * Creates a 'No Image' image (or returns if already exists).	 				
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the dynamically created image file.
		 */
		/*static*/ function image_none($c = NULL) {
			// Config
			if(!$c[width]) $c[width] = 100; // Width of image
			if(!$c[height]) $c[height] = 75; // Height of image
			if(!$c[extension]) $c[extension] = "gif"; // File extension: png, jpg, gif
			if(!$c[text]) $c[text] = "No Image"; // Text do display
			if(!$c[text_color]) $c[text_color] = "888888"; // Text color
			if(!$c[text_size]) { // Text size
				$square = ($c[width] * $c[height]);
				if($square <= 1500) $c[text_size] = 7;
				else if($square <= 4000) $c[text_size] = 8;
				else if($square <= 8000) $c[text_size] = 10;
				else if($square <= 20000) $c[text_size] = 12;
				else if($square <= 75000) $c[text_size] = 14;
				else if($square <= 200000) $c[text_size] = 16;
			}
			
			if($c[width] and $c[height]) {
				$name = strtolower(preg_replace('/[^0-9a-z]/i','',str_replace(' ','_',$c[text])))."_".$c[width]."x".$c[height].".".$c[extension];
				$file = file::call('server')."local/uploads/images/".$name;
				if(!file::call('exists',$file)) {
					//chmod(SERVER."local/uploads/images/",0777);
					$c[save] = 1;
					$c[save_path] = $file;
					$file = file::call('image_dynamic',$c);
				}
			}
			
			// Return
			return $file;
		}

		/**
		 * Returns array of information about given video file.									
		 * 
		 * @param string $file The video file you want to get info of. Defaults to the global $this->file.
		 * @return array An array of information about the video.
		 */
		/*static*/ function video_info($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Localize
			$file = file::call('localize',$file);
			
			// Create FFMPEG-PHP class
			if(class_exists('ffmpeg_movie')) {
				$v = new ffmpeg_movie($file);
			
				// Video Paramaters
				if($v) {
					$array[width] = $v->getFrameWidth();
					$array[height] = $v->getFrameHeight();
					$array[length] = $v->getDuration();
					$array[bitrate] = $v->getBitRate();
					$array[framerate] = $v->getFrameRate();
				}
			}
			
			// Use getID3
			if(!$array[width]) {
				$results = file::call('getid3',$file);
				if($results[video][resolution_x] > 10 and $results[video][resolution_y] > 10) {
					$array[width] = $results[video][resolution_x];
					$array[height] = $results[video][resolution_y];
				}
				$array[length] = $results[playtime_seconds];
				$array[bitrate] = round($results[bitrate]);
				$array[framerate] = $results[video][frame_rate];
			}
			
			return $array;
		}
	
		/**
		 * Trims video to given start/end points and saves to given destination			
		 * 
		 * @param int $start The point (in seconds) at which you want to start the trimmed video. Default = 0 (the start of the video)
		 * @param int $end The point (in seconds) at which you want to end the trimmed video. Default = end of the video
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the trimmed video (if successfully converted). If there was an error, it returns false.
		 */
		function video_trim($start = 0,$end = NULL,$c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[path])) $c[path] = file::value('file'); // Path to save trimmed video to, overwrites given file by default
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Save path
			$path = file::call('path',$c[path]);
			// Save path exists?
			$path_name = file::call('name',$path);
			$path_root = str_replace($path_name,'',$path);
			if(!file::call('exists',$path_root)) directory_create($path_root,0777);
			// Temporary path - in case we're saving to the same spot, in which case we can't overwrite a file and save it at the same time
			$path_temp = str_replace(".".file::value('extension')."_t.".file::value('extension'),$path);
		
			// Video info
			$v = file::call('video_info');
			
			// Start / End
			if(!$start or $start < 0 or !is_numeric($start)) $start = 0;
			if(!$end or $end > $v[length] or !is_numeric($end)) $end = $v[length];
			$length = $end - $start;
			//$start = seconds2hours($start,array('pad' => 1)); // Can convert to hh:mm:ss format, but ffmpeg also handles seconds
			//$length = seconds2hours($length,array('pad' => 1)); // Can convert to hh:mm:ss format, but ffmpeg also handles seconds
			
			// Debug
			debug("Trimming file ".file::value('file')." from ".$start." to ".$end." seconds and saving as ".$path,$c[debug]);
			debug("Video info: ".return_array($v),$c[debug]);
			debug("Trim start = ".$start,$c[debug]);
			debug("Trim length = ".$length,$c[debug]);
			
			// Command
			$command = file::value('c.ffmpeg')." -i \"".file::value('file')."\" -vcodec copy -acodec copy -ss ".$start." -t ".$length." -y \"".$path_temp."\"";
			
			// Trim
			debug("trim command: ".$command,$c[debug]);
			shell_exec(escapeshellcmd($command));
			
			// Remove original file (if we wanted to save the trimmed file to the same location)
			if(file::value('file') == $path) file::call('delete');
			// Rename temporary file to desired file
			rename($path_temp,$path);
			
			// File
			file::value('file',$path);
			file::analyze();
			
			// Return
			if(file::call('exists',$path)) return $path;
			else return false;
		}
	
		/**
		 * Converts video to a given extension and posts to given destination.		
		 *
		 * A super helpful 'command' creator: http://rodrigopolo.com/ffmpeg/. And another: http://rodrigopolo.com/ffmpeg/cheats.php
		 *
		 * Notes
		 * - The shell_exec() function must be enabled (disabled if safe mode on, sometimes manually disabled), a PHP error should tell you if it's turned off.
		 *
		 * To Do
		 * - ffmpeg command for different extensions
		 * - ffmpeg command for size
		 * 
		 * @param string $extension The extension you want to convert the video to.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The path to the converted video (if successfully converted). If there was an error, it returns false.
		 */
		function video_convert($extension,$c = NULL) {
			// Error
			if(!file::value('file') or !$extension) return;
			
			// Config
			if(!x($c[path])) $c[path] = NULL; // Path you want to save the resulting converted file. Defaluts to the same folder as the original the extension changed.
			if(!x($c[resize])) $c[resize] = m('videos','video.resize'); // Do you want to resize the video ($c[width] and $c[height] must also be set)
			if(!x($c[width])) $c[width] = m('videos','video.width'); // The width you want to resize the video to ($c[resize] and $c[height] must also be set)
			if(!x($c[height])) $c[height] = m('videos','video.height'); // The height you want to resize the video to ($c[resize] and $c[width] must also be set)
			if(!x($c[mobile])) $c[mobile] = NULL; // Optimize for mobile (only for MP4 conversion right now).
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Save path
			$path = file::call('path',$c[path]);
			$path_extension = file::call('extension',$path);
			if($path_extension != $extension) $path = str_replace('.'.$path_extension,'.'.$extension,$path);
			// Save path exists?
			$path_name = file::call('name',$path);
			$path_root = str_replace($path_name,'',$path);
			if(!file::call('exists',$path_root)) file::call('directory_create',$path_root,0777);
		
			// Video info
			$v = file::call('video_info');
			
			// Size
			$size = NULL;
			if($c[resize] == 1 and $c[width] and $c[height]) $size = $c[width]."x".$c[height];
		
			// Debug
			debug("Converting file ".file::value('file')." to ".$path,$c[debug]);
			debug("Video info:".return_array($v),$c[debug]);
			debug("Size: ".$size,$c[debug]);
			debug("Extension: ".$extension,$c[debug]);
			
			/*
			Params - http://ffmpeg.org/ffmpeg.html#Main-options, https://www.virag.si/2012/01/web-video-encoding-tutorial-with-ffmpeg-0-9/
				path to ffmpeg (first param)
				-i: source file
				-y: if present, will overwrite destination file (if it already exists)
				-s: size
				-r: fps
				-b: bitrate
				-bt: ???
				-deinterlace: deprecated in favor of -filter:v yadif
				-vcodec: video codec
				-ar: audio frequency
				-ab: audio bitrate
				-ac: audio channels (defaults to source file's audio channels)
				-acodec: audio codec
				-f: output format
				-vpre: preset video froamt to use
				path to destination file (last param)
			*/
			
			// Command - FLV
			if($extension == "flv") {
				// Patrick's Original
				//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -r 25 -b $v[bitrate] -vcodec ".$extension." -ar 44100 -ab 96k -acodec libmp3lame -f ".$extension." ".$path;
				// Works Good and Scrubs (failed on ParentingToday.com for most videos)
				//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -g 45 -vcodec ".$extension." -ar 44100 -ab 96k -acodec libmp3lame -f ".$extension." ".$path;
				// Most basic
				//$command = file::value('c.ffmpeg')." -i ".file::value('file')." ".$path;
				
				// Simple - http://www.johnrockefeller.net/using-ffmpeg-to-convert-mpg-or-mov-to-flv/
				$command = file::value('c.ffmpeg')." -i \"".file::value('file')."\" -y".($size ? " -s ".$size : "")." -deinterlace -ar 44100 -r 25 -qmin 3 -qmax 6 \"".$path."\"";
				//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -qmin 3 -qmax 6 ".$path; // Simplified version of above // Has some issues
			}
			
			// Command - MP4 - doesn't work yet
			if($extension == "mp4") {
				// Mobile - https://www.virag.si/2012/01/web-video-encoding-tutorial-with-ffmpeg-0-9/
				if($c[mobile]) {
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -codec:v libx264 -profile:v baseline -preset slow -b:v 250k -maxrate 250k -bufsize 500k -vf scale=-1:360 -threads 0 -codec:a libfdk_aac -b:a 96k -movflags faststart ".$path; // Doesn't like lots of stuff (too old)
					/*if(!$size and $v[height] > 360) { // -vf scale=-1:360 would probably handle this on newer versions of ffmpeg
						$height = 360;
						$width = round(($v[width] * $height) / $v[height]);
						if($width % 2 != 0) $width += 1; // Must be even (http://stackoverflow.com/a/7959256/502311)
						$size = $width."x".$height;
					}*/
					$command = file::value('c.ffmpeg')." -i \"".file::value('file')."\" -y".($size ? " -s ".$size : "")." -vcodec libx264 -b 250k -vpre slow -maxrate 250k -bufsize 500k -threads 0 -acodec libfaac -ab 96k -movflags faststart \"".$path."\"";
				}
				else {
					// http://www.catswhocode.com/blog/19-ffmpeg-commands-for-all-needs // Doesn't work
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." input -acodec aac -ab 128kb -vcodec mpeg4 -b 1200kb -mbd 2 -flags +4mv+trell -aic 2 -cmp 2 -subcmp 2".($size ? " -s ".$size : "")." -title X ".$path;
					// Not sure whare I got this // Doesn't work
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -acodec libfaac -ab 128kb -vcodec mpeg4 -b 1200kb -mbd 2 -flags +4mv -trellis 2 -aic 2 -cmp 2 -subcmp 2".($size ? " -s ".$size : "")." -metadata title=X ".$path;
					// Very basic // Doesn't work
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -ar 22050 ".$path;
					
					// http://stackoverflow.com/questions/3119797/ffmpeg-settings-for-converting-to-mp4-and-ogg-for-html5-video // Works!!!
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -vcodec libx264 -vpre hq -vpre ipod640 -b 250k -bt 50k -acodec libfaac -ab 56k -ac 2 ".$path;
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -vcodec libx264 -vpre ipod640 -acodec libfaac -ac 2 ".$path; // Simplified version of above
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -y".($size ? " -s ".$size : "")." -vcodec libx264 -vpre hq -acodec libfaac ".$path; // An even more simplifed version, no change in quality
					$command = file::value('c.ffmpeg')." -i \"".file::value('file')."\" -y".($size ? " -s ".$size : "")." -vcodec libx264 -vpre hq -acodec libfaac -movflags faststart \"".$path."\""; // Adds 'faststart' // A recent addition, think we have older ffmpeg installs, try but when it fails, redo with old command (no faststart)
					
					// http://rodrigopolo.com/ffmpeg/ // Doesn't work
					//if(!$size) $size = $v[width]."x".$v[height];	
					//$command = file::value('c.ffmpeg')." -i ".file::value('file')." -s ".$size." -vcodec libx264 -pass 1 -vpre fastfirstpass -an ".$path." && ffmpeg -y -i ".file::value('file')." -s ".$size." -vcodec libx264 -pass 2 -vpre hq -acodec libfaac ".$path;
				}
			}
			
			// Convert
			debug("Conversion command: ".$command,$c[debug]);
			shell_exec(escapeshellcmd($command));
			
			// MP4 error, try without faststart
			if($extension == "mp4" and !file::call('size',$path)) {
				$command = str_replace(' -movflags faststart','',$command);
				debug("Conversion command (2, because -movflags faststart didn't work): ".$command,$c[debug]);
				shell_exec(escapeshellcmd($command));
			}
			
			// Success
			if(file::call('size',$path)) {
				// FLV - add headers to video
				if($extension == "flv") {
					$command = file::value('c.flvtools')." -U \"".$path."\"";
					debug("Adding flv headers using flvtool2. command: ".$command,$c[debug]);
					shell_exec(escapeshellcmd($command));
				}	
			
				// MP4 - add faststart
				if($extension == "mp4") {
					file::call('video_faststart',$path,array('debug' => $c[debug]));
				}
			
				// Debug
				debug("Successfully converted file ".file::value('file')." to ".$path,$c[debug]);
				
				// Return
				$return = $path;
			}
			// Error
			else {
				// Debug
				debug("There was an error converting ".file::value('file')." to ".$path,$c[debug]);
				
				// Return
				$return = false;
			}
			
			// Update file
			file::value('file',$path);
			
			// Analyze
			file::analyze();
			
			// Return 
			return $return;
		}
		
		/**
		 * Moves the 'moov atom' in an MP4 from the end of the file to the beginning for faster playing.	
		 *
		 * @param string $file The file you want to add 'faststart' to. Default = $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The result returned by the command.
		 */
		function video_faststart($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->video_faststart($c)
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!file::value('file') or file::value('extension_standardized') != "mp4") return;
			
			// Config
			if(!x($c[path])) $c[path] = NULL; // Where to save the new file. If left NULL, it'll overwrite the original file.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Command
			$command = "qt-faststart \"".$file."\"".($c[path] ? " \"".$c[path]."\"" : "");
			debug($command,$c[debug]);
			
			// Run
			$result = shell_exec(escapeshellcmd($command));
			
			// Return
			return $result;
		}
	
		/**
		 * Creates thumb(s) from video and saves them to given destination.				
		 *
		 * @param string $path The path where you want to save the video thumb(s) to. Default = video's directory
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the names of the thumb(s) created.
		 */
		function video_thumb($path = NULL,$c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!$c[extension]) $c[extension] = "jpg"; // Extension of thumb(s)
			if(!$c[number]) $c[number] = 4; // Number of thumb(s) to create and return
			if(!x($c[delay])) $c[delay] = NULL; // Number of seconds to skip in the beginning
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// File name (w/o extension)
			$name = file::call('name',0);
			
			// Save path
			$path = file::call('path',$path);
			$path = str_replace(file::value('name'),'',$path); // Don't want video's name
			$path .= $name.'_%d.'.$c[extension]; // Add image name to save path 
			
			// Video info
			$v = file::call('video_info');
			
			// Delay
			if(!$c[delay]) $c[delay] = floor($v[length] / $c[number]);
			if($c[delay] <= 0) $c[delay] = 1;
			$c[delay] = 1 / $c[delay];
			if($c[delay] < .06) $c[delay] = .06;
			// Start
			if($v[length] <= 10) $ss = "00:00:00";
			else if($v[length] <= 30) $ss = "00:00:04";
			else $ss = "00:00:10";
			// Number
			$c[number]++; // Get 1 extra and skip first as first two are duplicating for some reason
			
			// Test
			debug("File: ".file::value('file'),$c[debug]);
			debug("Save path: ".$path,$c[debug]);
			debug("Delay: ".$c[delay],$c[debug]);
			debug("Number: ".$c[number],$c[debug]);
			
			// Command
			$command = file::value('c.ffmpeg')." -i \"".file::value('file')."\" -an -ss ".$ss." -r ".$c[delay]." -vframes ".$c[number]." -y \"".$path."\"";
			debug("Thumb command: ".$command,$c[debug]);
			
			// Create thumbs
			shell_exec(escapeshellcmd($command));
			
			// Build Array of Thumb Names
			//for($x = 1;$x <= $c[number];$x++) $thumbs[$x] = image_thumb($path.$name.'_'.$x.'.'.$c[extension],$path,$w,$h);
			// Validate and build array of thumbs
			for($x = 2;$x <= $c[number];$x++) {
				$thumb = str_replace($name.'_%d.'.$c[extension],$name.'_'.$x.'.'.$c[extension],$path);
				if(file::call('exists',$thumb)) {
					// Rename (starts with _2, want to start with _1)
					$thumb_new = str_replace($name.'_%d.'.$c[extension],$name.'_'.($x - 1).'.'.$c[extension],$path); // New name
					if($x > 2) file::call('delete',$thumb_new); // Delete if file already exists (would only happen on first thumb as all the rest are taking place of prior thumb which has been renamed 
					rename($thumb,$thumb_new); // Rename
					
					// Store
					$thumbs[($x - 1)] = file::call('name',$thumb_new);
				}
			}
			
			// Return
			return $thumbs;
		}
	
		/**
		 * Returns HTML for displaying a video.
		 *
		 * Players
		 * - Video.js - doesn't work in Firefox 20 or IE <= 8
		 * - MediaElements.js - doesn't scrub when using Flash, doesn't play in older IE's
		 * - JW Player - works well (migh have issue in Firefox 20), but has logo in top right always
		 * - Projekktor - a few quirks, and not best design, but works most consistently				
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML foor displaying the video.
		 */
		function video_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[auto])) $c[auto] = 1; // Autoplay the video
			if(!$c[preload]) $c[preload] = ($c[auto] ? "auto" : NULL); // What to preload the video when the page loads: auto (the whole video), metadata (just the metadata of the video), none (nothing)
			if(!$c[width]) $c[width] = file::call('width'); // Width of video. Defaults to actual video width or 265 if we can't get video width.
			if(!$c[height]) $c[height] = file::call('height'); // Height of video. Defaults to actual video width or 200 if we can't get video width.
			if(!$c[min_width]) $c[min_width] = 265; // Minimum width to display video
			if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display video
			if(!$c[poster]) $c[poster] = NULL; // URL of the image to use as a 'poster' preview image
			if(!$c[subtitles]) $c[subtitles] = NULL; // Array of subtitle files in array('lang' => 'http://path.to/file.srt') format.
			if(!x($c[download])) $c[download] = 1; // Show download link if other versions don't work
			if(!$c[fallback]) $c[fallback] = array(); // An array of URLs for videos in other extensions we can fallback to.
			if(!x($c[mobile_redirect])) $c[mobile_redirect] = 1; // Automatically redirect mobile browsers to video file (as opposed to HTML5/Flash player)
			if(!x($c[events])) $c[events] = array(); // An array of event => function to apply to the video. See the $events array for available events.
			if(!$c[id]) $c[id] = "player_".random(); // ID of player element
			if(!$c[player]) $c[player] = "projekktor"; // What player to use: projekktor (default), videojs, jwplayer, mediaelements
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Config options we handle, all other c values will be added to the video. This way you can pass config stuff specific to the player you're using.
			$c_skip  = array(
				'auto',
				'preload',
				'width',
				'height',
				'min_width',
				'max_width',
				'poster',
				'subtitles',
				'download',
				'fallback',
				'mobile_redirect',
				'events',
				'id',
				'player',
				'debug',
				'playlist',
			);
			
			// Events - these can all be defined in $c array as well. Example $c[ended] = function() {alert('the video is over');}
			$events = array(
				"loadstart", //Fired when the user agent begins looking for media data.
				"loadedmetadata", // Fired when the player has initial duration and dimension information.
				"loadeddata", // Fired when the player has downloaded data at the current playback position.
				"loadedalldata", // Fired when the player has finished downloading the source data.
				"play", // Fired whenever the media begins or resumes playback.
				"pause", // Fired whenever the media has been paused.
				"timeupdate", // Fired when the current playback position has changed. During playback this is fired every 15-250 milliseconds, depnding on the playback technology in use.
				"ended", // Fired when the end of the media resource is reached. currentTime == duration
				"durationchange", // Fired when the duration of the media resource is changed, or known for the first time.
				"progress", // Fired while the user agent is downloading media data.
				"resize", // Fired when the width and/or height of the video window changes.
				"volumechange", // Fired when the volume changes.
				"error", // Fired when there is an error in playback.
				"fullscreenchange", // Fired when the player switches in or out of fullscreen mode.
			);
			
			// Debug
			debug("<b>\$file->video_html();</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
	
			// Extension
			$extension = file::call('extension');
			// Extensions we support
			$extensions = array('flv','mp4','ogv','webm','swf');
			if($c[player] == "projekktor") $extensions[] = "mp3"; // Can also handle MP3s
			// Not a extension we currently support
			if(!in_array($extension,$extensions)) {
				// Download
				$extension = "download";
				
				// Check fallback extensions
				if($c[fallback]) {
					foreach($c[fallback] as $k => $file) {
						if(file::call('exists',$file)) {
							$_extension = file::call('extension',$file);
							if(in_array($_extension,$extensions)) {
								file::value('file',$file);
								file::analyze();
								$extension = $_extension;
								unset($c[fallback][$k]);
								break;
							}
						}
					}
				}
			}
			
			// URL
			$url = file::call('url');
			
			// Debug
			debug("file = ".file::value('file'),$c[debug]);
			debug("url = ".$url,$c[debug]);
			
			// Have url
			if($url) {			
				// Width
				if(!$c[width]) $c[width] = 265;
				// Height
				if(!$c[height]) $c[height] = 198;
			
				// Min Width
				if($c[min_width] and $c[width] < $c[min_width]) {
					$c[height] = round(($c[min_width] * $c[height]) / $c[width]);
					$c[width] = $c[min_width];
				}
				// Max Width
				if($c[max_width] and $c[width] > $c[max_width]) {
					$c[height] = round(($c[max_width] * $c[height]) / $c[width]);
					$c[width] = $c[max_width];
				}
				
				// Files
				$files = NULL;
				$files[$url] = array(
					'formats' => array(
						$url => array(
							'url' => $url,
							'poster' => $c[poster],
							'subtitles' => $c[subtitles],
						)
					)
				);
				if($c[fallback]) {
					foreach($c[fallback] as $v) {
						if(is_array($v)) {
							if(!in_array($v[url],$files[$url][files])) $files[$url][files][$v[url]] = $v;
						}
						else if(!in_array($v,$files[$url][files])) {
							$files[$url][files][$v] = array('url' => $v);	
						}
					}
				}
				
				// Files - playlist
				if($c[playlist]) {
					foreach($c[playlist] as $k => $v) {
						if(is_array($v)) {
							if(is_array($v[files]) and !in_array($k,$files)) { // $c[playlist] = array($url => array('formats' => array($url => array('url' => array('url' => $url))),'poster' => $poster));
								$files[$v[url]] = $v;
							}
							else { // $c[playlist] = array($url => array('url' => $url,'poster' => $poster));
								if(!in_array($v[url],$files)) {
									$files[$v[url]] = array(
										'formats' => array(
											$v[url] => $v
										)
									);
								}
								// In case this is main file and these got passed in playlist, but not in $c
								if($v[poster]) $files[$v[url]][poster] = $v[poster];
								if($v[subtitles]) $files[$v[url]][subtitles] = $v[subtitles];
							}
						}
						else if(!in_array($v,$files)) { // $c[playlist] = array($url);
							$files[$v] = array(
								'formats' => array(
									$v => array(
										'url' => $v
									)
								)
							);
						}
					}
				}
				
				// Files - standardize / playable
				if($files) {
					foreach($files as $file_k => $file) {
						foreach($file[formats] as $k => $v) {
							$f = file::load($v[url]);
							if($f->exists) {
								// URL (including localization)
								$v[url] = $f->url();
								// Extension
								if(!$v[extension]) $v[extension] = $f->extension_standardized;
								// Type
								if($v[extension] == "flv") $v[contenttype] = "video/mp4";
								else $v[contenttype] = $f->contenttype();
								
								// Playable
								if(in_array($v[extension],$extensions)) {
									$files[$file_k][formats][$k] = $v;
									continue;
								}
							}
							// Remove - if 'continue' not called
							unset($files[$file_k][formats][$k]);
						}
						if(!count($files[$file_k][formats])) unset($files[$file_k]);
					}
				}
				if(!$files) return;
				
				// Download
				if($extension == "download" and $c[download] == 1) {
					$text = "<a href='".$url."' target='_blank'>Download ".file::call('name')."</a>";
				}
				// View
				else {
					// JW Player
					if($c[player] == "jwplayer") {
						$text .= "
<script type='text/javascript' src='http://jwpsrv.com/library/E+bkPqCBEeK+9CIACpYGxA.js'></script>";
						/*$text .= "
".include_javascript('jwplayer');*/
						$text .= "
<div id='".$c[id]."'></div>
<script type='text/javascript'>
    jwplayer('".$c[id]."').setup({
        file: '".file::call('url')."',
        image: '".$c[poster]."',
        width: '".$c[width]."',
        height: '".$c[height]."',
		autostart: ".$c[auto];
						foreach($c as $k => $v) {
							if(!in_array($k,$c_skip)) $text .= ",
		".$k.": '".$v."'";
						}
						$text .= "
    });";
						if($c[events]) {
							if($c[events][ended]) $text .= "
	jwplayer('".$c[id]."').onComplete(".$c[events][ended].");";
						}
						$text .= "
</script>";
					}
					// Kaltura
					/*if($c[player] == "kaltura") {
						$text .= "
<script src='http://html5.kaltura.org/js'></script>
<div id='".$c[id]."'></div>
<script type='text/javascript'></script>";
					}*/
					
					// Javascript - Projekktor
					if($c[player] == "projekktor") {
						// Config
						if(!x($c[volume])) $c[volume] = 1; // 0-1
						
						// CSS / javascript
						$text .= "
".include_css('projekktor')."
".include_javascript('projekktor');
						
						// Element
						$text .= "
<div id='".$c[id]."' style='width:".$c[width]."px;height:".$c[height]."px;'></div>";
						
						// Javascript
						$text .= "
<script type='text/javascript'>
$(document).ready(function() {
    projekktor('#".$c[id]."',{
		playerFlashMP4: '".DOMAIN."core/core/libraries/flash/projekktor/swf/Jarisplayer/jarisplayer.swf',
		playerFlashMP3: '".DOMAIN."core/core/libraries/flash/projekktor/swf/Jarisplayer/jarisplayer.swf',
		//playerFlashMP4: '".DOMAIN."core/core/libraries/flash/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
		//playerFlashMP3: '".DOMAIN."core/core/libraries/flash/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
		autoplay: ".($c[auto] ? "true" : "false").",
		poster: '".$c[poster]."',";
						// Projekktor thinks certain versions of Firefox can play MP4, but they can't really, force it to use flash before native (can remove this if we get a newer version of projekktor that works well)
						if(!$c[platforms] and browser() == "firefox") $c[platforms] = "['flash', 'browser', 'ios', 'native']";
						foreach($c as $k => $v) {
							if(!in_array($k,$c_skip) or in_array($k,array('width','height'))) {
								$quotes = 1;
								if(in_array(substr(trim($v),0,1),array('[','{'))) $quotes = 0;
								$text .= "
		".$k.": ".($qoutes ? "'".$v."'" : $v).",";
							}
						}
						$text .= "
		playlist: [";
						$file_count = 0;
						foreach($files as $file_k => $file) {
							$text .= ($file_count > 0 ? "," : "")."
			{";
							$format_count = 0;
							$rtmp = 0;
							if(substr($file_k,0,7) == "rtmp://") $rtmp = 1; // This rtmp stuff doesn't work
							foreach($file[formats] as $k => $v) {
								if($rtmp) {
									$rtmp_name = basename($v[url]);
									$rtmp_server = str_replace("/".$rtmp_name,'',$v[url]);
									$v[url] = $rtmp_name;
								}
								$text .= ($format_count > 0 ? "," : "")."
				".$format_count.": {
					src:'".$v[url]."',
					type:'".$v[contenttype]."'
				}";
								$format_count++;
							}
							$config = NULL;
							if($file[poster]) $config .= ($config ? "," : "")."
					poster:'".$file[poster]."'";
							if(x($file[subtitles])) {
								$config .= ($config ? "," : "")."
					tracks: [";
								$subtitles_count = 0;
								foreach($file[subtitles] as $language => $file) {
									$config .= ($subtitles_count > 0 ? "," : "")."
						{
							src: '".$file."',
							srclang:'".$language."'
						}";
									$subtitles_count++;
								}
								$config .= "
					]";
							}
							if($rtmp) $config .= ($config ? "," : "")."
					flashStreamType:'rtmp',
					flashRTMPServer:'".$rtmp_server."'";
							if($config) $text .= ",
				config: {
					".$config."
				}";
							$text .= "
			}";
							$file_count++;
						}
						$text .= "
		],
		messages: {
			0:'An error occurred.',
			1:'You aborted the media playback. ',
			2:'A network error caused the media download to fail part-way. ',
			3:'The media playback was aborted due to a corruption problem. ',
			4:'The media (%{title}) could not be loaded because the server or network failed.',
			//5:'Sorry, your browser does not support the media format of the requested file.',
			5: \"Adobe Flash Player is required for video playback.<br /><a href='http://get.adobe.com/flashplayer/' target='_blank'>Get the Latest Flash Player</a><br /><span style='font-size:12px;'>Note: You must restart your browser after installing Adobe Flash Player.</span>\",
			6:'Your client is in lack of the Flash Plugin V%{flashver} or higher.',
			7:'No media scheduled.',
			8: '! Invalid media model configured !',
			9: 'File (%{file}) not found.',
			10: 'Invalid or missing quality settings for %{title}.',
			11: 'Invalid streamType and/or streamServer settings for %{title}.',
			12: 'Invalid or inconsistent quality setup for %{title}.',
			80: 'The requested file does not exist or delivered with an invalid content-type.',
			97:'No media scheduled.',
			98:'Invalid or malformed playlist data!',
			99:'Click display to proceed. ',
			100: 'PLACEHOLDER',
			// Youtube errors
			500: 'This Youtube video has been removed or set to private',
			501: 'The Youtube user owning this video disabled embedding.',
			502: 'Invalid Youtube Video-Id specified.'
		},
		loop: false // IE 7 was looping without this, even though it's supposed to be 'false' by default
	}".($c[events][ready] ? ", ".$c[events][ready] : "").");";
						if($c[events]) {
							$events_player = array(
								'ended' => 'done',
								#need to add other events converting {my key} => {their key}. Note, skip my key 'ready' as we handle that above.
							);
							foreach($c[events] as $k => $v) {
								if($event_player = $events_player[$k]) $text .= "
	projekktor('#".$c[id]."').addListener('".$event_player."',".$v.");";
								else $text .= "
	projekktor('#".$c[id]."').addListener('state',".$v.");";
							}
						}
						$text .= "	
});
</script>";
					}
					// Video.js / MediaElements.js - also use the <video> element similarirly
					if(in_array($c[player],array('videojs','mediaelements'))){
						// Class
						$class = NULL;
						
						// Javascript/CSS
						if($c[player] == "mediaelements") {
							$text .= "
".include_css('mediaelements')."
".include_javascript('mediaelements');
						}
						if($c[player] == "videojs") {
							/*$text .= "
".include_css('videojs')."
".include_javascript('videojs');*/
							$text .= "
<link href='http://vjs.zencdn.net/c/video-js.css' rel='stylesheet'>
<script src='http://vjs.zencdn.net/c/video.js'></script>";
							
							$class .= "video-js vjs-default-skin' data-setup='{\"techOrder\": [\"flash\"]";
							foreach($c as $k => $v) {
								if(!in_array($k,$c_skip)) $class .= ",\"".$k."\": \"".$v."\"";
							}
							$class .= "}";
						}
					
						// Video - if not a playlists, meaning only 1 file (though we might have multiple formats)
						if(count($files) == 1) {
							$text .= "
<video id='".$c[id]."' width='".$c[width]."' height='".$c[height]."' controls".($c[auto] ? " autoplay='autoplay'" : "").($c[preload] ? " preload='".$c[preload]."'" : "").($c[poster] ? " poster='".$c[poster]."'" : "").($class ? " class='".$class."'" : "").">";
							foreach($files[$url][formats] as $v) {
								$text .= "
	<source src='".$v[url]."' type='".$v[contenttype]."'>";
							}
							$text .= "
</video>";
						}
						// Javascript - MediaElements.js
						if($c[player] == "mediaelements") {
							$text .= "

<script type='text/javascript'>
$(document).ready(function() {
	$('#".$c[id]."').mediaelementplayer({
		success: function(media, node, player) {";
							// Events
							if($c[events]) {
								foreach($c[events] as $event => $function) {
									if(in_array($event,$events)) $text .= "
			media.addEventListener('".$event."', function(e) {
				debug('".$event." called');
				".$function."();
			});";
								}
							}
							// Auto-play - doesn't work when using flash player 
							if($c[auto]) {
								$text .= "
			media.play();";
							}
							$text .= "
		}
	});
});
</script>";
						}
						// Javascript - Video.js
						if($c[player] == "videojs") {
							// Events
							$text .= "
<script type='text/javascript'>
	var video_player = _V_('".$c[id]."');";
							foreach($c[events] as $event => $function) {
								if(in_array($event,$events)) $text .= "
	video_player.addEvent('".$event."', ".$function.");";
							}
							$text .= "
</script>";
						}
					}
				}
			}
			
			// Debug
			debug('video() result: <xmp>'.$result.'</xmp>',$c[debug]);
			
			// Return
			return $text;
		 }
		 
		/**
		 * Returns HTML for displaying an audio file.		 								
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function audio_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!$c[player]) $c[player] = "wpaudioplayer"; // What player to use: wpaudioplayer (default), jwplayer
			if(!$c[id]) $c[id] = "player_".random(); // ID of player element
			if(!$c[poster]) $c[poster] = NULL; // URL of the image to use as a 'poster' preview image
			if(!x($c[timestamp])) $c[timestamp] = 0; // Append url with timestamp so browser won't use cached file
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Config options we handle, all other c values will be added to the video. This way you can pass config stuff specific to the player you're using.
			$c_skip  = array(
				'player',
				'id',
				'poster',
				'timestamp',
				'debug'
			);
			
			// Random numbr
			$r = random();
			
			// Debug
			debug("<b>\$file->audio_html();</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// URL
			$url = file::call('url');
			
			// Have url
			if($url) {
				// Timestamp
				if($c[timestamp]) $url .= "?".time();
				
				// JW Player
				if($c[player] == "jwplayer") {
					// Config
					if(!x($c[width])) $c[width] = 200;
					if(!x($c[height])) $c[height] = 25;
					$text .= "
<script type='text/javascript' src='http://jwpsrv.com/library/E+bkPqCBEeK+9CIACpYGxA.js'></script>";
					/*$text .= "
".include_javascript('jwplayer');*/
					$text .= "
<div id='".$c[id]."'></div>
<script type='text/javascript'>
	jwplayer('".$c[id]."').setup({
		file: '".file::call('url')."',
		image: '".$c[poster]."'";
					foreach($c as $k => $v) {
						if(!in_array($k,$c_skip)) $text .= ",
		".$k.": '".$v."'";
					}
					$text .= "
	});
</script>";
				}
				// JPlayer
				else if($c[player] == "jplayer") {
					$text .= "
".include_javascript('jplayer')."
<div id='".$c[id]."' class='jplayer'></div>
<script type='text/javascript'>
$('#".$c[id]."').jPlayer({
	ready: function () {
		$(this).jPlayer('setMedia', {
			".file::call('extension_standardized').": '".file::call('url')."'
		});
	},
	swfPath: '".DOMAIN."core/core/libraries/flash/jplayer',
	supplied: '".file::call('extension_standardized')."'
});
</script>";
				}
				// Media Elements
				else if($c[player] == "mediaelements") {
					$text .= "
".include_css('mediaelements')."
".include_javascript('mediaelements')."
<audio id='".$c[id]."' controls='control' preload='none' src='".file::call('url')."' type='audio/mp3'></audio>
<script type='text/javascript'>
$(document).ready(function() {
	$('#".$c[id]."').mediaelementplayer({
		success: function(media, node, player) {";
					// Events
					if($c[events]) {
						foreach($c[events] as $event => $function) {
							if(in_array($event,$events)) $text .= "
			media.addEventListener('".$event."', function(e) {
				debug('".$event." called');
				".$function."();
			});";
						}
					}
					// Auto-play - doesn't work when using flash player 
					if($c[auto]) {
						$text .= "
			media.play();";
					}
					$text .= "
		}
	});
});
</script>";
				}
				// WP Audio Player
				else {
					/* Version 1.0 - http://www.macloo.com/examples/audio_player/*/
					// Flash HTML
					/*$c = array(
						'width' => 290,
						'height' => 24,
						'flashvars' => array(
							'playerID' => $r,
							'bg' => '0xf8f8f8',
							'leftbg' => '0xeeeeee',
							'lefticon' => '0x666666',
							'rightbg' => '0xcccccc',
							'rightbghover' => '0x999999',
							'righticon' => '0x666666',
							'righticonhover' => '0xffffff',
							'text' => '0x666666',
							'slider' => '0x666666',
							'track' => '0xFFFFFF',
							'border' => '0x666666',
							'loader' => '0xcfcfcf',
							'soundFile' => $url
						)
					);
					$f = new file(SERVER."core/core/libraries/flash/wpaudioplayer/player1.swf");
					$text = $f->flash_html($c);*/
					
					// Embeded Flash
					/*$text = "
					<object type='application/x-shockwave-flash' data='".D."core/libraries/flash/audio/player1.swf' id='audioplayer".$r."' height='24' width='290'>
						<param name='movie' value='".D."core/libraries/flash/audio/player1.swf'>
						<param name='FlashVars' value='playerID=".$r."&amp;bg=0xf8f8f8&amp;leftbg=0xeeeeee&amp;lefticon=0x666666&amp;rightbg=0xcccccc&amp;rightbghover=0x999999&amp;righticon=0x666666&amp;righticonhover=0xffffff&amp;text=0x666666&amp;slider=0x666666&amp;track=0xFFFFFF&amp;border=0x666666&amp;loader=0xcfcfcf&amp;soundFile=".urlencode($url)."'>
						<param name='quality' value='high'>
						<param name='menu' value='false'>
						<param name='bgcolor' value='#FFFFFF'>
					</object>";*/
					
					/* Version 2.0 - http://wpaudioplayer.com/standalone*/
					
					// Config
					if(!x($c[width])) $c[width] = 290;
					if(!x($c[height])) $c[height] = 24;
					if(!x($c[flashvars][bg])) $c[flashvars][bg] = 'f8f8f8';
					if(!x($c[flashvars][leftbg])) $c[flashvars][leftbg] = 'e0e0e0';
					if(!x($c[flashvars][lefticon])) $c[flashvars][lefticon] = '666666';
					if(!x($c[flashvars][rightbg])) $c[flashvars][rightbg] = 'cacaca';
					if(!x($c[flashvars][rightbghover])) $c[flashvars][rightbghover] = '999999';
					if(!x($c[flashvars][righticon])) $c[flashvars][righticon] = '666666';
					if(!x($c[flashvars][righticonhover])) $c[flashvars][righticonhover] = 'ffffff';
					if(!x($c[flashvars][text])) $c[flashvars][text] = '666666';
					if(!x($c[flashvars][slider])) $c[flashvars][slider] = '666666';
					if(!x($c[flashvars][track])) $c[flashvars][track] = 'FFFFFF';
					if(!x($c[flashvars][border])) $c[flashvars][border] = '666666';
					if(!x($c[flashvars][voltrack])) $c[flashvars][voltrack] = 'ffffff';
					if(!x($c[flashvars][loader])) $c[flashvars][loader] = 'cfcfcf';
					$c[flashvars][soundFile] = $url;
					
					$f = new file(SERVER."core/core/libraries/flash/wpaudioplayer/player2.swf");
					$text = $f->flash_html($c);
				}
			}
			
			// Return
			return $text;
		}

		/**
		 * Returns array of information about given audio file.									
		 * 
		 * @param string $file The audio file you want to get info of. Defaults to the global $this->file.
		 * @return array An array of information about the video.
		 */
		/*static*/ function audio_info($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Localize
			$file = file::call('localize',$file);
			
			// Info // untested
			$results = file::call('getid3',$file);
			$array[length] = $results[playtime_seconds];
			
			// Return
			return $array;
		}
	
		/**
		 * Returns script for displaying flash file											
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function flash_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!$c[allowscriptaccess]) $c[allowscriptaccess] = "always";
			if(!$c[allowfullscreen]) $c[allowfullscreen] = "true";
			if(!$c[quality]) $c[quality] = "high";
			if(!$c[pluginspage]) $c[pluginspage] = "http://www.macromedia.com/go/getflashplayer";
			if(!$c[codebase]) $c[codebase] = "http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0";
			if(!$c[play]) $c[play] = "true";
			if(!$c[loop]) $c[loop] = "true";
			if(!$c[wmode]) $c[wmode] = "transparent";
			if(!$c[bgcolor]) $c[bgcolor] = "#ffffff";
			if(!$c[width]) $c[width] = 250;
			if(!$c[height]) $c[height] = 200;
			if(!$c[id]) $c[id] = "flash_".random(); // Element id
			if(!$c[container_id]) $c[container_id] = "flash_".random()."_container"; // Container id
			if(!x($c[embed])) $c[embed] = 0; // Embedding (if = 1 we include fewer variables and don't use js)
			if(!x($c[js])) $c[js] = ($c[embed] == 1 ? 0 : 1); // Use JS to display flash
			$skip = array('container_id','auto','js','embed'); // Config values we don't want to pass as variables
			
			// URL
			$url = file::call('url');
			
			// jQuery Flash
			if($c[js] == 1) {
				$text .= "
	".include_javascript('jquery.flash')."
	<div id='".$c[container_id]."' style='width:".$c[width]."px;'></div>
	<script type='text/javascript'>
	$(document).ready(function() {
		$('#".$c[container_id]."').flash({src: '".$url."'";
				foreach($c as $k => $v) {
					if(!in_array($k,$skip)) {
						if($k == "flashvars") {
							$text .= ",".$k.": {";
							$x = 0;
							foreach($v as $_k => $_v) {
								$text .= ($x > 0 ? ", " : "")."'".$_k."' : ".(substr($_v,0,6) == "encode" ? $_v : "'".s($_v)."'");
								$x++;
							}
							$text .= "}";
						}
						else $text .= ",".$k.": '".s($v)."'";
					}
				}
				$text .= "});
	});
	</script>";
			}
		
			// Embed
			$embed .= "
		<object width='".$c[width]."' height='".$c[height]."'>
			<param name='movie' value='".$url."' />";
			array_push($skip,'width','height'); // Manually adding width/height
			if($c[embed] == 1) { // Embedding, only include required variables
				$skip = array_keys($c);
				$skip = array_diff($skip,array('flashvars','movie','allowfullscreen','allowscriptaccess'));
			}
			foreach($c as $k => $v) {
				if(!in_array($k,$skip)) {
					$embed .= "
			<param name='".$k."' value='".s((is_array($v) ? http_build_query($v) : $v))."' />";
					$embed_vars .= " ".$k."='".s((is_array($v) ? http_build_query($v) : $v))."'";
				}
			}
			$embed .= "
			<embed src='".$url."' type='application/x-shockwave-flash' width='".$c[width]."' height='".$c[height]."'".$embed_vars."></embed>
		</object>";
			
			// Using javascript - add no script
			if($c[js] == 1) $text .= "
	<noscript>
		".$embed."
	</noscript>";
			// Using embed/object
			else $text = $embed;
		
			// Return
			return $text;
		}
	
		/**
		 * Converts HTML Color Hex (000000) to RGB 											
		 * 
		 * @param string $color The 3 or 6 character color hex code (ex: ffffff = white).
		 * @return string The corresponding RGB code.
		 */
		/*static*/ function html2rgb($color) {
			if($color[0] == '#') $color = substr($color, 1);
		
			if(strlen($color) == 6) list($r, $g, $b) = array($color[0].$color[1],  $color[2].$color[3], $color[4].$color[5]);
			else if(strlen($color) == 3) list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
			else return false;
		
			$r = hexdec($r); 
			$g = hexdec($g); 
			$b = hexdec($b);
		
			return array($r, $g, $b);
		}
		
		/**
		 * Updates the global configuration with the given array of configuration values (usually passed via a method).
		 *
		 * @param array $c An array of configuration values.
		 * @return array The updated array of global configuration values.
		 */
		function c($c = NULL) {
			if($c) {
				if($this) $this->c = array_merge($this->c,$c);
				else self::$c_static = array_merge(self::$c_static,$c);
			}
			
			// Return
			if($this) $return = $this->c;
			else $return = self::$c_static;
			return $return;
		}
	}
}

if(!function_exists('imagecreatefrombmp')) {
	/**
	 * Creates function imagecreatefrombmp, since PHP doesn't have one http://www.php.net/manual/en/function.imagecreate.php#53879						
	 * 
	 * @param string $filename The path to the bmp file you want to create an image resource out of.
	 */
	function imagecreatefrombmp($filename) {
		// Ouverture du fichier en mode binaire
		if (! $f1 = fopen($filename,"rb")) return FALSE;
		
		// 1 : Chargement des enttes FICHIER
		$FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
		if ($FILE['file_type'] != 19778) return FALSE;
		
		// 2 : Chargement des enttes BMP
		$BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
		'/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
		'/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
		$BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
		if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
		$BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
		$BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
		$BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
		$BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
		$BMP['decal'] = 4-(4*$BMP['decal']);
		if ($BMP['decal'] == 4) $BMP['decal'] = 0;
		
		// 3 : Chargement des couleurs de la palette
		$PALETTE = array();
		if ($BMP['colors'] < 16777216) {
			$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
		}
		
		// 4 : Cration de l'image
		$IMG = fread($f1,$BMP['size_bitmap']);
		$VIDE = chr(0);
		
		$res = imagecreatetruecolor($BMP['width'],$BMP['height']);
		$P = 0;
		$Y = $BMP['height']-1;
		while ($Y >= 0) {
			$X=0;
			while ($X < $BMP['width']) {
				if ($BMP['bits_per_pixel'] == 24)
					$COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
				elseif ($BMP['bits_per_pixel'] == 16) { 
					$COLOR = unpack("n",substr($IMG,$P,2));
					$COLOR[1] = $PALETTE[$COLOR[1]+1];
				}
				elseif ($BMP['bits_per_pixel'] == 8) { 
					$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
					$COLOR[1] = $PALETTE[$COLOR[1]+1];
				}
				elseif ($BMP['bits_per_pixel'] == 4) {
					$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
					if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
					$COLOR[1] = $PALETTE[$COLOR[1]+1];
				}
				elseif ($BMP['bits_per_pixel'] == 1) {
					$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
					if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
					elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
					elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
					elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
					elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
					elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
					elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
					elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
					$COLOR[1] = $PALETTE[$COLOR[1]+1];
				}
				else return FALSE;
				
				imagesetpixel($res,$X,$Y,$COLOR[1]);
				$X++;
				$P += $BMP['bytes_per_pixel'];
			}
			$Y--;
			$P+=$BMP['decal'];
		}
		
		//Fermeture du fichier
		fclose($f1);
		
		return $res;
	}
}
?>