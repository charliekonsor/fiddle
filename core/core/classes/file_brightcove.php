<?php
// Load
require_once SERVER."core/core/libraries/brightcove/bc-mapi.php";
//require_once SERVER."core/core/libraries/brightcove/bc-mapi-cache.php"; // Using own cache

if(!class_exists('file_brightcove',false)) {
	/**
	 * An extension of the file class with functionality specific to storing files via Bright Cove.
	 */
	class file_brightcove extends file {
		/** Your Bright Cove Read Token. */
		public $brightcove_token_read = NULL;
		/** Your Bright Cove Write Token. */
		public $brightcove_token_write = NULL;
		/** Holds the instance of the MAPI class we're using currently. */
		public $brightcove_client = NULL;
		/** Holds the instance of the MAPI cache class we're using currently. */
		public $brightcove_cache = NULL;
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $file A file (either the path to it or the $_FILES array of it) you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($file = NULL,$c = NULL) {
			self::file_brightcove($file,$c);
		}
		function file_brightcove($file = NULL,$c = NULL) {
			// Credentials
			if($c[token_read]) file::value('brightcove_token_read',$c[token_read]);
			if(!file::value('brightcove_token_read')) file::value('brightcove_token_read',g('config.uploads.storage.brightcove.login.token_read'));
			if($c[token_write]) file::value('brightcove_token_write',$c[token_write]);
			if(!file::value('brightcove_token_write')) file::value('brightcove_token_write',g('config.uploads.storage.brightcove.login.token_write'));
			
			// Client
			if(file::value('brightcove_token_read') and file::value('brightcove_token_write')) {
				$this->brightcove_client = new BCMAPI(file::value('brightcove_token_read'),file::value('brightcove_token_write'));
			}
			
			// Cache // Using own cache
			//$this->brightcove_cache = new BCMAPICache('file',600,g('config.cache.path').'brightcove/','.txt');
			
			// Parent
			parent::__construct($file,$c);
		}
		
		/**
		 * Anaylzes current file for basic information.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function analyze($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[defaults])) $c[defaults] = 1; // If doesn't exist, use 'defaults'. Used after uploading since files isn't immediately available via API.
			
			// Exists
			if(file::call('exists')) {
				file::value('name',file::value('file'));
				file::value('path','');
				file::value('extension',file::call('extension'));
				file::value('extension_standardized',file::call('extension_standardized'));
				file::value('type','video');
				file::value('file_changed',0);
				file::value('exists',1);
			}
			// Defaults - file isn't available via API yet, assume defauult values (flv extension, etc.)
			else if($c[defaults] and is_int_value(file::value('file'))) {
				file::value('name',file::value('file'));
				file::value('path','');
				file::value('extension','flv');
				file::value('extension_standardized','flv');
				file::value('type','video');
				file::value('file_changed',0);
				file::value('exists',1);
			}
			// Doesn't exist
			else {
				file::value('exists',0);	
			}
			
			// Debug
			debug("name: ".file::value('name'),file::value('c.debug'));
			debug("extension: ".file::value('extension'),file::value('c.debug'));
			debug("extension_standardized: ".file::value('extension_standardized'),file::value('c.debug'));
			debug("type: ".file::value('type'),file::value('c.debug'));
			debug("exists: ".file::value('exists'),file::value('c.debug'));
		}
		
		/**
		 * Localizes a file, making sure it has a full path and doesn't contain the domain.
		 *
		 * @param string $file The path to the file you want to localize. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function localize($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!file::value('brightcove_client')) return;
			
			// Remove path - don't use directoies on brightcove
			$file = file::call('name',$file);
			
			// Return
			return $file;
		}
		
		/**
		 * Gets the full URL of a file (opposite of 'localize').
		 *
		 * @param string $file The path to the file you want to get the URL of. Defaults to the global $this->file.
		 * @return string The URL of the file.
		 */
		/*static*/ function url($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Object
			$object = file::call('brightcove_file',$file);
			
			// URL
			$url = $object->FLVURL;
			
			// Return
			return $url;
		}
		
		/**
		 * Checks if a file exists.		
		 * 
		 * @param string $file The file you want to check exists. Defaults to the global $this->file.
		 * @param boolean $external Not used in this child class
		 * @return boolean Whether or not the file exists.
		 */
		/*static*/ function exists($file = NULL,$external = 0) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Object
			$object = file::call('brightcove_file',$file);
			
			// Exists
			$exists = ($object->id ? true : false);
			
			// Return
			return $exists;
		}
		
		/**
		 * Returns the base name of the given file. Example: "/path/to/file.jpg" would return "file.jpg".
		 *
		 * @param string|boolean $file The file we want to get the base name of. If we want to use the file stored in this instance ($this->file) we can pass $extension here. Defaults to the global $this->file.
		 * @param boolean $extension Do you want to return the extension? Default = 1
		 * @return string The base name of the file.
		 */
		/*static*/ function name($file = NULL,$extension = 1) {
			// Name doesn't include path or extension on brightcove (just id) so could just use $file, but use basename in case path got in there somehow
			$name = basename($file);
		
			// Return
			return $name;
		}
		
		/**
		 * Returns the extension of the file. Example: "/path/to/file.jpg" would return "jpg".
		 *
		 * @param string $file The path to the file you want to get the extensions of. Defaults to the global $this->file.
		 * @return string The extension of the file.
		 */
		/*static*/ function extension($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Bright Cove ID
			if(is_int_value($file)) {
				// Object
				$object = file::call('brightcove_file',$file);
				// Extension
				$extension = strtolower($object->videoFullLength->videoContainer);
			}
			// Regular file
			else {
				$extension = parent::extension($file);	
			}
			
			// Return
			return $extension;
		}

		/**
		 * Returns width/height of given file												
		 * 
		 * @param string $file The file you want to get the dimensions of. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the width and height of the given file.
		 */
		/*static*/ function dimensions($file = NULL,$c = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// Cached?
			if($array = file::value('cache.dimensions')) {
				// Return
				return $array;	
			}
			
			// Object
			$object = file::call('brightcove_file',$file);
			
			// Dimensions
			$array = array(
				'width' => $object->videoFullLength->frameWidth,
				'height' => $object->videoFullLength->frameHeight,
			);
			
			// Cache
			file::value('cache.dimensions',$array);
			
			// Return
			return $array;
		}
		
		/**
		 * Deletes a file.
		 *
		 * @param string $file The path to the file you want to delete. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function delete($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->delete($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file or !file::value('brightcove_client')) return;
			
			// Delete
			$this->brightcove_client->delete('video',$file);
			
			// Cache
			cache_delete('brightcove/files/'.$file);
			
			// Return
			return true;
		}

		/**
		 * Pushes the given $source to the given $destination on the external storage service.	
		 *
		 * Notes:
		 * - If push is called via $form->process(), it'll pass an array of $c[values] which contain
		 *	 db defined values such as 'name', 'description', etc.
		 * - Uses Bright Cove's createMedia() method which takes 'name' and 'shortDescription'.
		 *	 You can pass those via the $c parameter.			
		 * 
		 * @param string $source The full path to the local source file.
		 * @param string $destination The path on the external storage where you want to push this file. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the external file if we successfully pushed (nothing is returned if there was an error).
		 */
		function push($source,$destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Source
			$array[source] = file::load($source);
			// Destination
			$array[destination] = file::call('prepare_destination',$source,$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[source]->type == "video") {
				// Debug
				debug("Pushing ".$array[source]->file." (local) to ".$array[destination]->storage,file::value('c.debug'));
				
				// Config
				if(!$c[name]) $c[name] = ($c[values][name] ? $c[values][name] : $array[source]->name); // Not sure if this is required, pass anyway.
				if(!$c[shortDescription]) $c[shortDescription] = $c[values][description];
				
				// Timeout
				ini_set('max_execution_time',1500); // 30 Minutes
				if(ini_get('safe_mode') == 0) set_time_limit(1500);
				ini_set('memory_limit','512M'); // 512 MB
				debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
				$start = microtime_float();
				
				// Save
				$array[destination]->file = $this->brightcove_client->createMedia('video',$array[source]->file,$c);
					
				// Debug
				debug("File ".$array[source]->file." was uploaded to ".$result,file::value('c.debug'));
				debug("Took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
					
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Pushed file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Pulls the current file from external storage servicc to the local $destination.				
		 * 
		 * @param string $destination The local path where you want to save the pulled file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the local file if we successfully pulled it (nothing is returned if there was an error).
		 */
		function pull($destination,$c = NULL) {
			// Error
			if(!$destination) return;
			
			#build this
		}
		
		/**
		 * Returns an array of items (files and directories) within the given directory.
		 *
		 * @param string $directory The directory you want to look for items in.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_items($directory,$c = NULL) {	
			// Error
			if(!file::value('brightcove_client')) return;
		
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($c[paths] == "full") $c[prefix] = $directory.$c[prefix];
			
			// Object
			$items = $this->brightcove_client->findAll('video',array('video_fields' => 'id'));
			foreach($items as $item) {
				$array[] = $item->id;	
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Determines if the given directory exists.
		 *
		 * Bright Cove doesn't use directories, neither will we, must exist.
		 *
		 * @param string $directory The path to the directory you want to check for.
		 * @return boolean Whether or not the directory exists.
		 */
		/*static*/ function directory_exists($directory) {
			// Return
			return true;
		}
	
		/**
		 * Returns HTML for displaying a video.
		 *
		 * To effect this player via javascript, setup a BCLS object.
		 * For example:
		 * 	var BCLS = (function() {
		 * 		var player,
		 * 			APIModules,
		 * 			videoPlayer;
		 *
		 * 		return {
		 * 			// Templates loaded
		 * 			onTemplateLoad : function (experienceID) {
		 * 				// Player
		 * 				player = brightcove.api.getExperience(experienceID);
		 *				// Modules
		 * 				APIModules = brightcove.api.modules.APIModules;
		 * 			},
		 *			// Custom video() method for loading a video
		 * 			video: function(id) {
		 * 				// Player
		 * 				videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
		 * 				// Load video
		 * 				videoPlayer.loadVideoByID(id);
		 * 			}
		 * 		}
		 * 	}());	
		 *
		 * Documentation: http://docs.brightcove.com/en/smart-player-api/
		 * 
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML foor displaying the video.
		 */
		function video_html($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Config
			if(!x($c[auto])) $c[auto] = 1; // Autoplay the video
			if(!$c[width]) $c[width] = file::call('width'); // Width of video. Defaults to actual video width or 265 if we can't get video width.
			if(!$c[height]) $c[height] = file::call('height'); // Height of video. Defaults to actual video width or 200 if we can't get video width.
			//if(!$c[min_width]) $c[min_width] = 265; // Minimum width to display video
			//if(!$c[max_width]) $c[max_width] = NULL; // Maximum width to display video
			//if(!$c[poster]) $c[poster] = NULL; // URL of the image to use as a 'poster' preview image
			if(!$c[playerID]) $c[playerID] = (g('config.uploads.brigtcove.player.id') ? g('config.uploads.brigtcove.player.id') : "876414349001"); // ID of BrightCove Player to use.
			if(!$c[playerKey]) $c[playerKey] = (g('config.uploads.brigtcove.player.key') ? g('config.uploads.brigtcove.player.key') : "AQ~~,AAAAkVHRxYE~,tk4-uogNPTJb1uDgVy4MlZVIDHkN_j7Z");// playerKey of BrightCove Player to use.
			if(!$c[share]) $c[share] = NULL; // The URL we want them to use when they click the 'share' button.
			if(!$c[id]) $c[id] = "player_".random(); // ID of player element
			if(!x($c[https])) $c[https] = ($_SERVER['HTTPS'] == "on" ? 1 : 0); // User HTTPS when displaying this video
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Config options we handle, all other c values will be added to the video. This way you can pass config stuff specific to the player you're using.
			$c_skip  = array(
				'auto',
				'preload',
				'width',
				'height',
				'min_width',
				'max_width',
				'poster',
				'downloade',
				'fallback',
				'mobile_redirect',
				'events',
				'id',
				'player',
				'debug'
			);
			
			// Debug
			debug("<b>\$file->video_html();</b>",$c[debug]);
			debug("c:".return_array($c),$c[debug]);
			
			// Object
			$object = file::call('brightcove_file');
			
			// HTML
			if($object->id) {
				$html .= '
<!-- Start of Brightcove Player -->
<script type="text/javascript" src="'.($c[https] ? "https://sadmin" : "http://admin").'.brightcove.com/js/BrightcoveExperiences.js"></script>
<object id="'.$c[id].'" class="BrightcoveExperience">
	<param name="bgcolor" value="#FFFFFF" />
	<param name="width" value="'.$c[width].'" />
	<param name="height" value="'.$c[height].'" />
	<param name="playerID" value="'.$c[playerID].'" />
	<param name="playerKey" value="'.$c[playerKey].'" />
	<param name="isVid" value="true" />
	<param name="dynamicStreaming" value="true" />
	<param name="@videoPlayer" value="'.$object->id.'" />
	
	<param name="includeAPI" value="true" />
	<param name="templateLoadHandler" value="BCLS.onTemplateLoad" />
	<param name="templateReadyHandler" value="BCLS.onTemplateReady" />';
				if($c[share]) $html .= '
	<param name="linkBaseURL" value="'.$c[share].'" />';
				if($c[https]) $html .= '
	<param name="secureConnections" value="true" />';
				$html .= '
</object>
<script type="text/javascript">brightcove.createExperiences();</script>
<!-- End of Brightcove Player -->';
			
				// Debug
				debug('video() result: <xmp>'.$html.'</xmp>',$c[debug]);
			}
			else debug("No object returned for video.",$c[debug]);
			
			// Return
			return $html;
		 }
		
		/**
		 * Returns file object as retrieved by $this->brightcove_client->find('find_video_by_id', 123456789);
		 *
		 * @param string $file The Bright Cove id of the media you want to get. Defaults to the global $this->file.
		 * @return object The Bright Cove file object.
		 */
		/*static*/ function brightcove_file($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file or !file::value('brightcove_client')) return;
			
			// Bright Cove ID
			if(is_int_value($file)) {
				// Cached?
				$object = cache_get('brightcove/files/'.$file);
				// No
				if(!$object) {
					// Get
					$object = $this->brightcove_client->find('find_video_by_id',$file);
					// Cache
					cache_save('brightcove/files/'.$file,$object);
				}
			}
			else {
				$object = (object) array();	
			}
			
			// Return
			return $object;
		}

		/**
		 * These functions aren't doable with remotely hosted files.											
		 */
		/*statice*/ function permission($file,$permission = NULL) {
			return;
		}
		/*static*/ function orientation($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function length($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function getid3($file = NULL) {
			return;
		}
		function save($destination = NULL,$c = NULL) {
			return;
		}
		function copy($destination = NULL,$c = NULL) {
			return;
		}
		/*static*/ function directory_create($directory,$recursive = 1) {	
			return;
		}
	}
}
?>