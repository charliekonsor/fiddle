<?php
// Load
require_once SERVER."core/core/libraries/aws/aws.phar";
use Aws\S3\S3Client;
use Aws\Common\Enum\Region;

if(!class_exists('file_amazons3',false)) {
	/**
	 * An extension of the file class with functionality specific to storing files via Amazons Simple Storage Service (S3).
	 */
	class file_amazons3 extends file {
		/** Your Amazon AWS Access Key ID. */
		public $amazons3_key = NULL;
		/** Your Amazon AWS Secret Access Key. */
		public $amazons3_secret = NULL;
		/** The Amazon S3 'bucket' you store your files in. */
		public $amazons3_bucket = NULL;
		/** Holds the 'client' object of the SDK's S3Client class. */
		public $amazons3_client = NULL;
		
		/**
		 * Constructs the class.
		 *
		 * @param string|array $file A file (either the path to it or the $_FILES array of it) you will be interacting with with this instance of this class.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($file = NULL,$c = NULL) {
			self::file_amazons3($file,$c);
		}
		function file_amazons3($file = NULL,$c = NULL) {
			// Credentials
			if($c[key]) file::value('amazons3_key',$c[key]);
			if(!file::value('amazons3_key')) file::value('amazons3_key',g('config.uploads.storage.amazons3.login.key'));
			if($c[secret]) file::value('amazons3_secret',$c[secret]);
			if(!file::value('amazons3_secret')) file::value('amazons3_secret',g('config.uploads.storage.amazons3.login.secret'));
			if($c[bucket]) file::value('amazons3_bucket',$c[bucket]);
			if(!file::value('amazons3_bucket')) file::value('amazons3_bucket',g('config.uploads.storage.amazons3.bucket'));
			
			if(file::value('amazons3_key') and file::value('amazons3_secret') and file::value('amazons3_bucket')) {
				// Client
				$this->amazons3_client = S3Client::factory(array(
					'key' => file::value('amazons3_key'),
					'secret' => file::value('amazons3_secret'),
				));
			}
			
			// Parent
			parent::__construct($file,$c);
		}
		
		/**
		 * Anaylzes current file for basic information.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function analyze($c = NULL) {
			// Error
			if(!file::value('file')) return;
			
			// Exists // Skipping this to save time
			//if(file::call('exists')) {
				file::value('name',file::call('name'));
				file::value('path',str_replace(file::value('name'),'',file::value('file')));
				file::value('extension',file::call('extension'));
				file::value('extension_standardized',file::call('extension_standardized'));
				file::value('type',file::call('type'));
				file::value('file_changed',0);
				file::value('exists',1);
			/*}
			// Doesn't exist
			else {
				file::value('exists',0);	
			}*/
			
			// Debug
			debug("name: ".file::value('name'),file::value('c.debug'));
			debug("extension: ".file::value('extension'),file::value('c.debug'));
			debug("extension_standardized: ".file::value('extension_standardized'),file::value('c.debug'));
			debug("type: ".file::value('type'),file::value('c.debug'));
			debug("exists: ".file::value('exists'),file::value('c.debug'));
		}
		
		/**
		 * Localizes a file, making sure it has a full path and doesn't contain the domain.
		 *
		 * @param string $file The path to the file you want to localize. Defaults to the global $this->file.
		 * @return string The localized file path.
		 */
		/*static*/ function localize($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!file::value('amazons3_client')) return;
			
			// Remove domain / bucket
			$file = str_replace("https://s3.amazonaws.com/".file::value('amazons3_bucket')."/","",$file);
			
			// Return
			return $file;
		}
		
		/**
		 * Gets the full URL of a file (opposite of 'localize').
		 *
		 * @param string $file The path to the file you want to get the URL of. Defaults to the global $this->file.
		 * @return string The URL of the file.
		 */
		/*static*/ function url($file = NULL) {
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return;
			
			// URL
			$url = "https://s3.amazonaws.com/".file::value('amazons3_bucket')."/".$file;
			
			// Return
			return $url;
		}
		
		/**
		 * Checks if a file (local or external) exists.					
		 * 
		 * @param string $file The file you want to check exists. Defaults to the global $this->file.
		 * @param boolean $external Not used in this child class
		 * @return boolean Whether or not the file exists.
		 */
		/*static*/ function exists($file = NULL,$external = 0) {
			// Default
			if(!$file) $file = file::value('file');
			
			// Error
			if(!file::value('amazons3_client') or !$file) return;
			
			// Debug
			/*$array = debug_backtrace();
			foreach($array as $k => $v) {
				if($v['object']) unset($array[$k]['object']);	
				if($v['args']) unset($array[$k]['args']);
			}
			print_array($array);*/
			
			// Command
			//$result = file::call('amazons3_command','DoesObjectExist',array(file::value('amazons3_bucket'),$file)); // Returns 'Command was not found' error
			$result = $this->amazons3_client->doesObjectExist(file::value('amazons3_bucket'),$file);
			//debug("doesObjectExist result: ".$result,file::value('c.debug'));
			
			// Return
			return $result;
		}
		
		/**
		 * Returns the permission of the given file/directory (example: 0777) or, if the $permission param is passed, it sets the permission of the file/directory.		
		 * 
		 * @param string $file The file/directory we want to get (and set if 2nd param) the permission of. Defaults to the global $this->file.
		 * @param int|string $permission The permission you wan to apply to the file/directory. Default = NULL
		 * @return int The resulting permission for the file/directory.
		 */
		/*statice*/ function permission($file,$permission = NULL) {
			// Params
			if(is_int_value($file)) { // $file->permission($permission);
				$permission = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!file::value('amazons3_client') or !$file) return;
				
			// Config
			$c[Bucket] = file::value('amazons3_bucket');
			$c[Key] = $file;
			
			// Set permission
			if($permission) {
				// ACL
				$c[ACL] = file::call('amazons3_acl',$permission);
				
				// Command
				$result = file::call('amazons3_command','PutObjectAcl',$c);
				
				// Return - should probably actually check the permission got set properly, but need to save time
				return $permission;
			}
			
			// Get permission
			$result = file::call('amazons3_command','GetObjectAcl',$c);
			
			// Convert permission
			$permission = file::call('amazons3_permission',$result[Grants]);
			
			// Return
			return $permission;
		}

		/**
		 * These functions aren't doable with remotely hosted files.											
		 */
		/*static*/ function orientation($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function dimensions($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function length($file = NULL,$c = NULL) {
			return;
		}
		/*static*/ function getid3($file = NULL) {
			return;
		}
		function save($destination = NULL,$c = NULL) {
			return;
		}
		
		/**
		 * Copies a file to another new destination (works with remote files as well).	
		 *
		 * Notes:
		 * - Both files must be stored on Amazon S3.						
		 * 
		 * @param string $destination The path where you want to save the copied file, including the file's name, though we'll default to the original file's name if the path contains none.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the copied file, if we successfully copied it (nothing is returned if there was an error).
		 */
		function copy($destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Prepare
			$array = file::call('prepare',$destination);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Config
				$c[Bucket] = file::value('amazons3_bucket');
				$c[Key] = $array[destination]->file;
				$c[ACL] = file::call('amazons3_acl',file::value('c.permission')); // Moved permission setting from save() to copy() so we could do it via the PutObject/CopyObject call and wouldn't have to do separate call for upading ACL
				$c[CopySource] = $array[source]->file;
				
				// Debug
				debug("Copying ".$array[source]->file." to ".$array[destination]->file."",file::value('c.debug'));
				
				// Needs copying?
				if($array[source]->file != $array[destination]->file) {
					// Timeout
					ini_set('max_execution_time',1500); // 30 Minutes
					if(ini_get('safe_mode') == 0) set_time_limit(1500);
					ini_set('memory_limit','512M'); // 512 MB
					debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
					$start = microtime_float();
					
					// Copy
					$result = file::call('amazons3_command','CopyObject',$c);
					
					// Debug
					debug("file ".$array[source]->file." was copied to ".$array[destination]->file,file::value('c.debug'));
					debug("took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
				}
				
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Copied file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}
		
		/**
		 * Deletes a file.
		 *
		 * @param string $file The path to the file you want to delete. Defaults to the global $this->file.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		/*static*/ function delete($file = NULL,$c = NULL) {
			// Params
			if(is_array($file)) { // $file->delete($c);
				$c = $file;
				$file = NULL;
			}
			// Default
			if(!$file) $file = file::value('file');
			// Error
			if(!$file) return; 
			
			// Localize
			$file = file::call('localize',$file);
			
			// Exists? // Not necessary (no error if you delete a non-existant file), removed for speed
			//if(file::call('exists',$file)) {
				// Config
				$c[Bucket] = file::value('amazons3_bucket');
				$c[Key] = $file;
				
				// Delete
				$result = file::call('amazons3_command','DeleteObject',$c);
				
				// Return
				if($result[RequestId]) return true;
			//}
			
			// Return
			return false;
		}

		/**
		 * Pushes the given $source to the given $destination on the external storage service.	
		 *
		 * Notes:
		 * - If push is called via $form->process(), it'll pass an array of $c[values] which contain
		 *	 db defined values such as 'name', 'description', etc.			
		 * 
		 * @param string $source The full path to the local source file.
		 * @param string $destination The path on the external storage where you want to push this file. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the external file if we successfully pushed (nothing is returned if there was an error).
		 */
		function push($source,$destination = NULL,$c = NULL) {
			// Return
			$return = 0;
			
			// Source
			$array[source] = file::load($source);
			// Destination
			$array[destination] = file::call('prepare_destination',$source,$destination,$c);
			
			// Have what we need?
			if($array[source]->exists and $array[destination]->file) {
				// Debug
				debug("Pushing ".$array[source]->file." (local) to ".$array[destination]->storage,file::value('c.debug'));
				
				// Config
				$c[Bucket] = file::value('amazons3_bucket');
				$c[Key] = $array[destination]->file;
				$c[ACL] = file::call('amazons3_acl',file::value('c.permission')); // Moved permission setting from save() to copy() so we could do it via the PutObject/CopyObject call and wouldn't have to do separate call for upading ACL
				
				// Timeout
				ini_set('max_execution_time',1500); // 30 Minutes
				if(ini_get('safe_mode') == 0) set_time_limit(1500);
				ini_set('memory_limit','512M'); // 512 MB
				debug("time limit: ".ini_get('max_execution_time').", memory limit: ".ini_get('memory_limit'),file::value('c.debug'));
				$start = microtime_float();
				
				// Remote file
				if(substr($array[source]->file,0,4) == "http") $c[Body] = $array[source]->get();
				// Local file
				else $c[Body] = fopen($array[source]->file,'r');
				
				// Push
				$result = file::call('amazons3_command','PutObject',$c);
				
				// Debug
				debug("File ".$array[source]->file." was uploaded to ".$array[destination]->file,file::value('c.debug'));
				debug("Took ".round(microtime_float() - $start,3)." seconds",file::value('c.debug'));
					
				// Exists?
				$array[destination]->analyze();
				if($array[destination]->exists) {
					// Return
					$return = 1;
					
					// Debug
					debug("Pushed file and it does exist",file::value('c.debug'));
				}
			}
			
			// Return
			if($return) return $array[destination];
		}

		/**
		 * Pulls the current file from external storage servicc to the local $destination.				
		 * 
		 * @param string $destination The local path where you want to save the pulled file.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The object for the local file if we successfully pulled it (nothing is returned if there was an error).
		 */
		function pull($destination,$c = NULL) {
			// Error
			if(!$destination) return;
			
			#build this
		}
		
		/**
		 * Determines if the given directory exists.
		 *
		 * Since Amazon creates a directory if it doesn't exist, there's no reason we need to actually check if it does exist.
		 * Plus, checking if the directory exists in prepare_destination() means extra calls to Amazon.
		 *
		 * @param string $directory The path to the directory you want to check for.
		 * @return boolean Whether or not the directory exists.
		 */
		/*static*/ function directory_exists($directory) {
			// Return
			return true;
			
			// Trim prepended slashes
			/*$directory = ltrim($directory,'/'); 
			// Add slash at end
			if(substr($directory,-1) != "/") $directory = "/";
			
			// Root - not in a folder
			if(!$directory) return true;
			
			// Exists?
			$exists = file::call('exists',$directory);
			
			// Return
			return $exists;*/
		}
		
		/**
		 * Creates a directory and all missing parent directories, applying 0777 permissions
		 *
		 * Note, if you don't pass a parent folder (ex: /uploads/ instead of /uploads/images/) we'll
		 * create a new root level folder, or 'bucket' instead of folder within the /uploads/ bucket.
		 *
		 * @param string $directory The path of the directory you want to create.
		 * @param boolean $recursive Do you want to 'recursively' create this directory (meaning we'll create parent directories if they don't exist). Default = 1
		 */
		/*static*/ function directory_create($directory,$recursive = 1) {	
			// Trim prepended slashes
			$directory = ltrim($directory,'/'); 
			// Add slash at end
			if(substr($directory,-1) != "/") $directory = "/";
			
			// Error
			if(!file::value('amazons3_client') or !$directory) return;
			
			// Config
			if(!x($c[permission])) $c[permission] = "0777";
			
			// Config
			$c[Bucket] = file::value('amazons3_bucket');
			$c[Key] = $directory;
			$c[ACL] = file::call('amazons3_acl',$c[permission]);
			
			// Create
			$result = file::call('amazons3_command','PutObject',$c);
		}
		
		/**
		 * Returns an array of items (files and directories) within the given directory.
		 *
		 * To do
		 * - Add ability to turn off 'recursive' (don't include if in a folder, is there a way to 'filter' when getting objects)
		 *
		 * @param string $directory The directory you want to look for items in. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of files in the directory.
		 */
		/*static*/ function directory_items($directory = NULL,$c = NULL) {
			// Error
			if(!file::value('amazons3_bucket')) return;
			
			// Config
			if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
			if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
			if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
			if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
			if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
			if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
				
			// Variables
			$array = array();
			if($directory and substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
			
			// Config
			$c[Bucket] = file::value('amazons3_bucket');
			if($directory) $c[Prefix] = $directory;
				
			// Objects
			$results = file::call('amazons3_command','ListObjects',$c);
			//print_array($results);
			
			// Loop through
			if($results[Contents]) {
				foreach($results[Contents] as $k => $v) {
					if($directory and $c[paths] == "relative") {
						$v[Key] = preg_replace("/".str_replace('/','\/',preg_quote($directory))."/",'',$v[Key],1);
						if(!x($v[Key])) continue;
					}
					
					// Directory
					if(substr($v[Key],-1) == "/" and $v[Size] == 0) {
						if($c[directories]) {
							$key = rtrim($v[Key],'/');
							if($c[multilevel] == 1) {
								$array = array_eval($array,str_replace('/','.',$key),array());
							}
							else {
								$array[] = $c[prefix].$key;		
							}
						}
					}
					// File
					else {
						if($c[files]) {
							if($c[multilevel] == 1) {
								$folders = explode('/',$v[Key]);
								$folders_count = count($folders);
								if($folders_count == 1) $array[] = $v[Key];
								else {
									$file = $folders[$folders_count - 1];
									unset($folders[$folders_count - 1]);
									$array = array_eval($array,implode('.',$folders).".",$file);
								}
							}
							else {
								$array[] = $c[prefix].$v[Key];
							}
						}
					}
				}
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Runs the given command, passing the given parameters.
		 *
		 * @param string $command The command to run.
		 * @param array $params An array of parameters to pass. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of results.
		 */
		function amazons3_command($command,$params = NULL,$c = NULL) {
			// Error
			if(!file::value('amazons3_client') or !$command) return;
		
			// Debug
			debug("command: ".$command.", params: ".return_array($params),file::value('c.debug'));
		
			// Method 1 - would have to determine $method from the $command (make first letter lower case) for ths to work.
			//$result = $this->amazons3_client->$method();
			
			// Method 2
			//$command = $this->amazons3_client->getCommand($command,$params);
			//$result = $command->getResult();
			
			// Method 3
			$result = $this->amazons3_client->getCommand($command,$params)->getResult();
			//debug("result:".return_array($result),file::value('c.debug'));
			
			// Result array
			$result_array = file::call('amazons3_result_array',$result);
			debug("result array:".return_array($result_array),file::value('c.debug'));
			
			// Return
			return $result_array;
		}
		
		/**
		 * Process the results returned from Amazon and returns an array of thos results.
		 *
		 * @param object $result The command result object.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of results.
		 */
		function amazons3_result_array($result,$c = NULL) {
			// Error
			if(!$result) return;
		
			// Method 1 - takes a single 'key' and returns that value
			//$result->get('Buckets');
			
			// Method 2 - takes a single 'key' and returns that value
			//$result[Buckets];
			
			// Method 3 - gets all results, don't have to define the key
			$result_array = NULL;
			$keys = $result->getKeys();
			foreach($keys as $key) {
				$result_array[$key] = $result->get($key);
			}
			
			// Return
			return $result_array;
		}
		
		/**
		 * Converts the given PHP permissions mode (ex: 0777) to the corresponding Amazons ACL (example: public-read-write).
		 *
		 * Note, currently only handles a few differnet permissions.
		 *
		 * @param string $permission The permission you want to convert to an ACL role.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The Amazon ACL role that corresponds to the given PHP permission mode.
		 */
		function amazons3_acl($permission,$c = NULL) {
			// Array
			$array = array(
				"0777" => "public-read-write",
				"0755" => "public-read",
				"0700" => "private",
			);
			
			// ACL
			$acl = $array[$permission];
			
			// Return
			return $acl;
		}
		
		/**
		 * Converts the given array of grants (returned by GetObjectAcl) to PHP permission mode (ex: 0777).
		 *
		 * Note, currently only handles a few differnet permissions.
		 *
		 * @param array $grants An array of grants.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The PHP permission mode that corresponds to the defined grants.
		 */
		function amazons3_permission($grants,$c = NULL) {
			$permission = array(0,0,0,0);
			foreach($grants as $grant) {
				// Is a user, assume this is the 'owner' user
				if($grant[Grantee][ID]) $x = 1;
				else $x = 2;
				
				if($grant[Permission] == "FULL_CONTROL") {
					if($permission[$x] < 7) $permission[$x] = 7;
				}
				else if($grant[Permission] == "WRITE") {
					if($permission[$x] < 7) $permission[$x] = 7;
				}
				else if($grant[Permission] == "READ") {
					if($permission[$x] < 5) $permission[$x] = 5;
				}
			}
			$permission[3] = $permission[2];
			
			// Return
			return implode($permission);
		}
		
		/**
		 * Gets info for the given object.
		 *
		 * Don't need for anything at the moment, comment out until I find a use.
		 *
		 * @param string $file The name of the object you wan to get
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of info about this object.
		 */
		/*function amazons3_object($file,$c = NULL) {
			// Error
			if(!file::value('amazons3_client') or !file::value('amazons3_bucket') or !$file) return;
			
			// Config
			$c[Bucket] = file::value('amazons3_bucket');
			$c[Key] = $file;
		
			// Get
			$amazons3_object = file::call('amazons3_command',"GetObject",$c);
		
			// Return
			return $amazons3_object;
		}*/
	}
}
?>