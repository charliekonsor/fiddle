/**
 * Calls up quicksearch() when page is loaded.
 *
 * Should really find a way to attach this to the refresh() (or is it refreshIt()) stuff so it gets called when modal loads, etc.
 */
$(document).ready(function() {
	quicksearch();
});

/**
 * Adds quicksearch functionality to inputs (and select boxes) with class='quicksearch'.												
 */
function quicksearch() {
	if($().liveUpdate) {
		// Deafults
		var d = {
			children: '', // Element type of the child elements we want to filter (usually 'option' or 'li'), will auto set if none passed
			placeholder: 'Search..', // Placeholder text (don't search if this is the q value)
			html: 0, // Include HTML when filtering text (as opposed to using just the plain text)
			quicksilver: 0, // Use quicksilver to score/filter (as opposed to basic indexOf filtering)
			nested: 0 // Allow for nested elements (ex: <ul><li><ul><li>), if 0 we hide nested elements when searching (only supported if children:'option')
		}
		
		// Class
		$('.quicksearch:not(.quicksearched)').each(function() {
			// Get ID
			var id = element_id($(this),'quicksearch_');
			
			// Config
			var c = $.extend(d,$(this).metadata());
			c.parent = '#'+id;
			
			// Children
			if(!c.children) {
				if($(this).is('select')) c.children = 'option';
				else if($(this).is('ul')) c.children = 'li';
			}
			
			// Add Search Input
			$(this).before("<input type='text' id='"+id+"_q' class='quicksearch_q'"+(c.placeholder ? " placeholder='"+c.placeholder+"'" : "")+" /><br />");
			
			// Preserver width/height - select only
			if(c.children == 'option') $(this).css({width:$(this).width(),height:$(this).height()});
			
			// Apply Quicksearch
			$("#"+id+"_q").liveUpdate(c);	
		}).addClass('quicksearched');
		
		// ID
		$('#quicksearch_q').liveUpdate('#quicksearch');
	}
}