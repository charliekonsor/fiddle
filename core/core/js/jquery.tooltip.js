/** 
 * Super simple tooltip plugin.
 *
 * Based on http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 */
$.fn.tooltip = function(options) {
	// Applied already?
	var data = this.data("tooltip-data");
	// No, apply now
	if(!data) {
		// Default options
		var options_default = {
			className: "tooltip-tip", // Class to apply to the tooltip (used for styling it)
			animation: "fade", // What animation do you want to run when we first show the tooltip: fade [default], none
			animationSpeed: "fast", // Speed of the animation we run when we first show the tooltip: slow, medium, fast, [milliseconds]
			track: 1, // Do you want the tooltip to track the mouse?
			xOffset: -5, // Offset from mouse pointer on the x-axis
			yOffset: 20 // Offset from mouse pointer on the y-axis
		}
		
		// Data
		data = $.extend({}, options_default, options);
		this.data("tooltip-data", data);
						
		// Hover
		this.hover(
			function(e) {
				// Title
				this.t = this.title;
				this.title = "";
				
				// Add to DOM
				$("body").append("<div id='tooltip' class='"+data.className+"' style='display:none;'>"+this.t+"</div>");	
				
				// Tooltip
				var $tooltip = $("#tooltip");
				var width = $tooltip.width() + 1;
				
				// Position		
				var position = $(this).tooltipPosition(e,data);			 
				$tooltip.css({
					top:position.top+"px",
					left:position.left+"px",
					width:width,
					display:(data.animation != "fade" ? "block" : "none")
				});
					
				// Animation
				if(data.animation == "fade") $tooltip.fadeIn(data.animationSpeed);
			},
			function(){
				this.title = this.t; // Restore title
				$("#tooltip").remove(); // Remove tooltip
			}
		);	
		
		// Mousemove
		if(data.track) {
			this.mousemove(function(e){
				// Position
				var position = $(this).tooltipPosition(e,data);
				// Move
				$("#tooltip")
					.css("top", position.top+ "px")
					.css("left", position.left+ "px");
			});
		}
	}
};
	
$.fn.tooltipPosition = function(e,data) {
	// Tooltip
	var $tooltip = $("#tooltip");
	
	// Position
	var top = (e.pageY + data.xOffset);
	var left = (e.pageX + data.yOffset);
	var width = $tooltip.outerWidth(true);
	
	// Off page?
	if((left + width) > $(window).width()) left = e.pageX - data.yOffset - width;
	
	// Return
	return {top:top,left:left};
};