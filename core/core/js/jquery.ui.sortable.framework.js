/**
 * Add sorting functionality to various elements								
 */
function sortable() {
	/*** Implementaion Example ***/
	/*
	<ul class='sort {vars:\"module=photos\"'>
		<li class='sort-sortable'><input type='hidden' class='sort-sorted' value='$qry[photo_id]' /></li>
	</ul>
	*/
	/*** Nested Implementaion Example ***/
	/*
	<ul class='sort {vars:\"module=products\"'>
		<li class='sort-sortable'>
			<ul class='sort {vars:\"module=products-options\",handle:\"option-handle\",items:\"option-sortable\",items_input:\"option-sorted\"}'>
				<li class='option-sortable'>
					<span class='option-handle i i-sort'></pan>
					<input type='hidden' class='option-sorted' value='$qry[variation_id]' />
				</li>
			</ul>
			<input type='hidden' class='sort-sorted' value='$qry[product_id]' />
		</li>
	</ul>
	*/
	
	// UI Sortable
	if($().sortable) {
		$('div.sort, ul.sort').each(function() {
			// Defaults
			var d = {
				items: 'div.sort-sortable, li.sort-sortable',
				//placeholder: 'sort-helper',
				handle: '',
				opacity: 0.5,
				items_input: 'input.sort-sorted'
			};
		
			// ID
			var id = element_id($(this),'sort-');
			
			// Config
			var c = $.extend(d,$(this).metadata());
			
			// Handle
			if(!c['handle'] && $(".sort-handle:not(.sort-sortable)",this).length > 0) c['handle'] = ".sort-handle";
			
			// Stop
			c['stop'] = function (event,ui) {
				var order = '';
				var items_input = 'input.sort-sorted';
				var c = $(this).metadata();
				if(c['items_input']) items_input = c['items_input'];
				$(items_input,this).each(function() {
					order += '|' + this.value;
				});
				//debug('ajax_action=sortIt&order='+order+(c['vars'] ? '&'+c['vars'] : "")+'<br />');
				if(order) $.ajax({type: 'POST',url: DOMAIN, data: 'ajax_action=sortIt&order='+order+(c['vars'] ? '&'+c['vars'] : ""), success: function(html) {debug(html);}});
				rows();
			};
			
			$(this).sortable(c);
		});
	}
	
	// Table Sorting
	if($().tableDnD) {
		$('table.sort:not(.sort-applied)').each(function() {
			$(this).children('tr:not(.sort-sortable)').addClass('nodrag'); // Make non .sort-sortable rows undraggable
			if($(".sort-handle:not(.sort-sortable)",this).length > 0) var handle = ".sort-handle";
			
			var settings = {
				onDragClass: "sort-sorting",
				onDrop: function(table, row) {
					var order = '';
					var c = $(table).metadata();
					$('input.sort-sorted',table).each(function() {
						order += '|' + this.value;
					});
					if(order) $.ajax({type: 'POST',url: DOMAIN, data: 'ajax_action=sortIt&order='+order+(c['vars'] ? '&'+c['vars'] : ""), success: function(html) {debug(html);}});
					rows();
				}
			};
			if(handle) settings['dragHandle'] = handle;
			
			$(this)
				.addClass('sort-applied')
				.tableDnD(settings);
		});
	}
}