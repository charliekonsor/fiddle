<?php
/**
 * @package kraken
 */

/************************************************************************************/
/************************************** Globals *************************************/
/************************************************************************************/
if(!function_exists('g')) {
	/**
	 * Returns value from the global scope (the $__GLOBAL array).
	 *
	 * The $key you pass will be the key as stored in the $__GLOBAL array. If it's not on the first level of the array, you'll separate the next level with a period. For example: "config.db.mysql.username".
	 *
	 * You can also pass a 2nd parameter, $value.  If you do so, it'll update the value of the global variable to that value.  If you want to empty the value, pass the string 'NULL' (in quotes) in the $value parameter.
	 *
	 * See array_eval() for more information.
	 * 
	 * @param string $key The key of the global value you want to get. Separate levels of the global array with a period (.). Escape periods with a preceding back slash.
	 * @param mixed $value A value you want to set the global variable to. If none passed, it'll simply return the current value.
	 * @return mixed The corresponding global value.
	 */
	function g($key,$value = NULL) {
		$value = array_eval("\$__GLOBAL",$key,$value);
		
		return $value;
	}
}

/************************************************************************************/
/************************************** Include *************************************/
/************************************************************************************/
if(!function_exists('include_class')) {
	/**
	 * Includes a class, the full path to the class file being defined in the global configuraiton with the given $key.
	 *
	 * @param string $key The assigned key of the class file path you want to include.
	 */
	function include_class($key) {
		$key = string_escape($key);
		if($files = g('config.include.class.'.$key)) {
			if(!is_array($files)) $files = array($files);
			foreach($files as $file) {
				include_once SERVER.$file;
			}
		}
	}
}

if(!function_exists('include_function')) {
	/**
	 * Includes a function(s), the full path to the function(s) file being defined in the global configuraiton with the given $key.
	 *
	 * @param string $key The assigned key of the function(s) file path you want to include.
	 */
	function include_function($key) {
		$key = string_escape($key);
		if($files = g('config.include.function.'.$key)) {
			if(!is_array($files)) $files = array($files);
			foreach($files as $file) {
				include_once SERVER.$file;
			}
		}
	}
}

if(!function_exists('include_functions')) {
	/**
	 * Catches in proper calls to include_functions() and redirects to proper function name, include_function().
	 *
	 * @param string $key The assigned key of the function(s) file path you want to include.
	 */
	function include_functions($key) {
		return include_function($key);
	}
}

if(!function_exists('include_javascript')) {
	/**
	 * Returns HTML for loading a javascript file(s), the full path to the javascript file(s) being
	 * defined in the global configuraiton with the given $key.
	 *
	 * Note, the $page->html_organize() method will handle all versioning / minification / duplication
	 * so we don't need to check that here anymore.
	 *
	 * @param string $key The assigned key of the javascript file(s) you want to include.
	 * @return string The HTML for including the desired javascript file(s).
	 */
	function include_javascript($key,$once = 1,$c = NULL) {
		// Files
		if($v = g('config.include.javascript.'.string_escape($key))) {
			if(!$v[files]) $v = array('files' => $v);
			if(!is_array($v[files])) $v[files] = array($v[files]);
			if(!$v[core]) {
				foreach($v[files] as $file) {
					//if(x($_GET['minify']) and !$_GET['minify']) $file .= "?".filemtime(SERVER.$file);  
					$html .= "<script type='text/javascript' language='javascript' src='".D.$file."'></script>";
				}
			}
		}
		
		// Return
		return $html;
	}
}

if(!function_exists('include_css')) {
	/**
	 * Returns HTML for loading a CSS file(s), the full path to the CSS file(s) being defined in the
	 * global configuraiton with the given $key.
	 *
	 * Note, the $page->html_organize() method will handle all versioning / minification / duplication
	 * so we don't need to check that here anymore.
	 *
	 * @param string $key The assigned key of the CSS file(s) you want to include.
	 * @return string The HTML for including the desired CSS file(s).
	 */
	function include_css($key,$once = 1,$c = NULL) {
		// Files
		if($v = g('config.include.css.'.string_escape($key))) {
			if(!$v[files]) $v = array('files' => $v);
			if(!is_array($v[files])) $v[files] = array($v[files]);
			if(!$v[core]) {
				foreach($v[files] as $file) {
					//if(x($_GET['minify']) and !$_GET['minify']) $file .= "?".filemtime(SERVER.$file);
					$html .= "<link rel='stylesheet' type='text/css' href='".D.$file."' />";
				}
			}
		}
		
		// Return
		return $html;
	}
}

/************************************************************************************/
/************************************** Browser *************************************/
/************************************************************************************/
if(!function_exists('browser')) {
	/**
	 * Determines current user's browser.														
	 * 
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string|array Either the name of the browsers (lowercase) or (if $c['return'] == "array") an arryay of the 'browser' and 'version'.
	 */
	function browser($c = NULL) {
		// Config
		if(!x($c[agent])) $c[agent] = $_SERVER['HTTP_USER_AGENT'];
		if(!x($c['return'])) $c['return'] = 'browser'; // What to return: browser [default], array (an array of the 'browser' and 'version')
		
		// Get Browser Function - Rarely Installed
		/*$array = get_browser($c[agent], true);
		if($array[browser]) {
			print "test 1<br />";
			$array[browser] = strtolower($array[browser]);
			if($c['return'] == "array") $browser = $array;
			else $browser = $array[browser];
		}
		// Custom Function
		else {*/
			$known = array(
				'msie',
				'firefox',
				'chrome',
				'safari',
				'webkit',
				'opera',
				'netscape',
				'konqueror', 
				'gecko'
			);
			if(preg_match_all('#(?<browser>'.join('|', $known).')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#', strtolower($c[agent]), $matches)) {
				$key = count($matches['browser']) - 1; // Use last browser listed incase more than one listed (Firefox + Gecko, Opera + MSIE, etc.)
				foreach($matches[browser] as $k => $v) {
					if($v == "chrome") $key = $k; // ..unless Chrome appears (Chrome appears before Safari in user agent string)	
				}
				
				$b = $matches['browser'][$key];
				$v = $matches['version'][$key];
				
				// Opera Version (version >= 10 put version number at end)
				if($b == "opera" and $v >= 9) {
					preg_match('/ version\/([0-9]{0,2}\.?[0-9]{0,2})/i',$c[agent],$results);
					if($results[1] and $results[1] >= 10) $v = $results[1];
				}
				
				if($c['return'] == "array") $browser = array('browser' => $b,'version' => $v); // Returns Array
				else $browser = $b; // Return browser name
			}
		//}
		
		// Return
		return $browser;
	}
}

if(!function_exists('browser_mobile')) {
	/**
	 * Determines if the current browser is a 'mobile' browser.
	 *
	 * Uses http://detectmobilebrowsers.com/ method.
	 * Note: added "|android|ipad|playbook|silk" to end of first regex to also catch tablets (http://detectmobilebrowsers.com/about).
	 *
	 * Old notes:
	 * - iPad, iPhone, etc. get caught because their user agent contains 'mobile'.
	 *
	 * @param array $c An array of configuration values. Default = NULL
	 * @return boolean Whether of not the user is using a mobile browser.
	 */
	function browser_mobile($c = NULL) {
		// Config
		if(!x($c[tablet])) $c[tablet] = $c[tablet] = 1; // Include 'tablets' in mobile lookup.
		
		// Mobile
		$mobile = 0;
	
		// Agent
		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		
		// Mobile? - new method - noted, added "|android|ipad|playbook|silk" to end of first regex to also catch tablets
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$agent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($agent,0,4))) {
			$mobile = 1;
		}
		// Mobile? - old method
		/*$array = array("sony","symbian","nokia","samsung","mobile","windows ce","epoc","opera mini","nitro","j2me","midp-","cldc-","netfront","mot","up.browser","up.link","audiovox","blackberry","ericsson,","panasonic","philips","sanyo","sharp","sie-","portalmmm","blazer","avantgo","danger","palm","series60","palmsource","pocketpc","smartphone","rover","ipaq","au-mic,","alcatel","ericy","up.link","vodafone/","wap1.","wap2","kindle","silk");
		foreach($array as $find) {
			if(strpos($agent,$find) !== false) {
				$mobile = 1;
				break;
			}
		}*/
		
		// Tablet?
		if($mobile and !$c[tablet]) {
			if(browser_tablet()) $mobile = 0;
		}
		
		// Return
		return $mobile;
	}
}

if(!function_exists('browser_tablet')) {
	/**
	 * Determines if the current browser is a 'tablet' browser.
	 *
	 * http://stackoverflow.com/questions/5341637/how-do-detect-android-tablets-in-general-useragent
	 *
	 * @return boolean Whether of not the user is using a tablet browser.
	 */
	function browser_tablet() {
		// Agent
		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		
		// Tablet
		$tablet = 0;
		
		// Tablet in user agent
		if(strstr($agent,'tablet')) $tablet = 1;
		// iPad
		else if(strstr($agent,'mobile') and strstr($agent,'ipad')) $tablet = 1;
		// Android
		else if(strstr($agent,'android') and !strstr($agent,'mobile')) $tablet = 1;
		// Other - android tablets, but old versions of user agent contained 'mobile'
		else if(
			strstr($agent,'kindle') or strstr($agent,'silk') // Kindle (Fire)
			or strstr($agent,'xoom') // Xoom
			or strstr($agent,'sch-i800') // Samsung Galaxy Tab
		) $tablet = 1;
		
		// Return
		return $tablet;
	}
}

/************************************************************************************/
/*************************************** Pages **************************************/
/************************************************************************************/
/*if(!function_exists('p')) {
	/**
	 * Returns the global page array value for the given key.
	 *
	 * @param string $key The key you want to get the value for. Levels of the page array are separated by periods (.) for example "meta.title".
	 * @return mixed The setting value, usually a string, boolean, or int, but sometimes an array.
	 */
	/*function p($key) {
		return array_eval("\$__GLOBAL['page']",$key);
	}
}*/

if(!function_exists('page_notice')) {
	/**
	 * Saves a 'notice' to the $_SESSION so it can be displayed on the next page.
	 *
	 * @param string $key The notice 'key'. It can be any string you want (most common is 'message' or 'error'). It will be added to the notice element class. Ex: key = 'error', the class 'notice-error' would be added to the element.
	 * @param string $notice The text of the notice.
	 * @param boolean $sticky Do you want this notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
	 */
	function page_notice($key,$notice,$sticky = 0) {
		if($key and $notice) {
			$_SESSION['notices'][$key.($sticky ? "_sticky" : "")] = $notice;
		}
	}
}

if(!function_exists('page_message')) {
	/**
	 * Saves a message to the $_SESSION 'notices' so it can be displayed on the next page.
	 *
	 * @param string $text The text of the message notice.
	 * @param boolean $sticky Do you want this error notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
	 */
	function page_message($text,$sticky = 0) {
		page_notice('message',$text,$sticky);
	}
}

if(!function_exists('page_error')) {
	/**
	 * Saves an error to the $_SESSION 'notices' so it can be displayed on the next page.
	 *
	 * @param string $text The text of the error notice.
	 * @param boolean $sticky Do you want this error notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
	 */
	function page_error($text,$sticky = 0) {
		page_notice('error',$text,$sticky);
	}
}

if(!function_exists('page_info')) {
	/**
	 * Saves an info message to the $_SESSION 'notices' so it can be displayed on the next page.
	 *
	 * @param string $text The text of the info notice.
	 * @param boolean $sticky Do you want this info notice to be 'sticky'? If true, the notice will stay on the page rather than slide away after a few moments. 			
	 */
	function page_info($text,$sticky = 0) {
		page_notice('info',$text,$sticky);
	}
}

/************************************************************************************/
/*************************************** URLs ***************************************/
/************************************************************************************/
if(!function_exists('url')) {
	/**
	 * Returns the URL of the current page
	 * 
	 * @param boolean $https Do you want to include https if the URL is on https?
	 * @return string The current page's URL
	 */
	function url($https = 1) {
		$url = "http";
		if($_SERVER["HTTPS"] == "on" and $https == 1) $url .= "s";
		$url .= "://";
		//$url .= $_SERVER["SERVER_NAME"]; // Sometimes doesn't have www when it should
		$url .= $_SERVER["HTTP_HOST"];
		$url .= $_SERVER["REQUEST_URI"];
		
		return $url;
	}
}

if(!function_exists('url_domain')) {
	/**
	 * Returns domain of given url (defaults to current url)
	 * 
	 * Configuration values (key, type, default - description):
	 * - https, boolean, 1 - Use 'https' if the $url contains it
	 * 
	 * @param string $url The URL we want to get the domain from (defaults to current url)
	 * @param array $c An array of configuration values
	 * @return string The domain of the given URL (including trailing backslash)
	 */
	function url_domain($url = NULL,$c = NULL) {
		// Error
		if(!$url) return;
		
		// Config
		if(!x($c[https])) $c[https] = 1; // Use 'https' if the $url contains it
		
		// Parse
		$array = parse_url($url);

		$domain = NULL;
		// Sheme
		if($array[scheme]) {
			if(strstr($array[scheme],"s") && $c[https]) $domain .= "https://";
			else $domain .= "http://";
		}
		// Host
		if($array[host]) $domain .= $array[host];
		
		// Dev path
		if(substr($array[path],0,2) == "/~") {
			$ex = explode('/',$array[path]);
			$domain .=  "/".$ex[1];
		}
		
		// Trailing slash
		if($domain) $domain .= "/";
		
		// Return
		return $domain;
	}
}

if(!function_exists('url_query')) {
	/**
	 * Get the query string from a URL.						
	 * 
	 * @param string $url The URL you want to get the query string from. If none passed, we'll use the current URL. Default = NULL
	 * @return string The URL's query string.
	 */
	function url_query($url = NULL) {
		if(!$url) $url = $_SERVER['REQUEST_URI'];
		list($base,$query) = explode('?',$url,2);
		return $query;
	}
}

if(!function_exists('url_query_array')) {
	/**
	 * Get the query string from a URL and returns an array of the key/value pairs (similar to $_GET)									
	 * 
	 * @param string $url The URL you want to get the query string from. If none passed, we'll use the current URL. Default = NULL
	 * @return array The array of key/value pairs in the URL's query string.
	 */
	function url_query_array($url = NULL) {
		$query = url_query($url);
		if($query) parse_str($query,$array);
		return $array;
	}
}

if(!function_exists('url_noquery')) {
	/**
	 * Strips the query string from the given URL and returns the base of the URL.					
	 * 
	 * @param string $url The URL you want to strip the query string from. If none passed, we'll use the current URL. Default = NULL
	 * @return string The base of the URL.
	 */
	function url_noquery($url = NULL) {
		if(!$url) $url = URL;
		list($base,$query) = explode('?',$url,2);
		return $base;
	}
}

if(!function_exists('url_query_append')) {
	/**
	 * Appends given string to the given URL's query string.			
	 * 
	 * @param string $url The URL you to append a string to.
	 * @param string $string The string you want to append to the URL.
	 * @return string The URL with the given string appended to it.
	 */
	function url_query_append($url,$string) {
		if($url and $string) {
			// Hash
			list($url,$hash) = explode('#',$url,2);
			
			// Query string pairs
			$pairs = explode('&',$string);
			foreach($pairs as $pair) {
				// Remove from URL if it was in there previously
				list($key,$value) = explode('=',$pair,2);
				$url = url_query_remove($url,$key);
				//$url = preg_replace('/(\?|&)'.$key.'\=(.*?)(&|$)/','$1$3',$url);
			}
			
			// Add new string to end of query string
			$url .= (strstr($url,"?") ? "&" : "?").$string;
			
			// Restore hash
			if(x($hash)) $url .= "#".$hash;
		}
		
		// Return
		return $url;
	}
}

if(!function_exists('url_query_remove')) {
	/**
	 * Removes given $key and corresponding value from the given URL's query string.	
	 * 
	 * @param string $url The URL you to remove the key (and value) from.
	 * @param string $key The key you want to remove from the URL.
	 * @return string The URL with the given key (and corresponding value) removed.
	 */
	function url_query_remove($url,$key) {
		if($url and $key) {
			// Query string keys/values
			$array = url_query_array($url);
			// Remove unwanted key (and value)
			unset($array[$key]);
			
			// Recreate query string
			$query = http_build_query($array);
			
			// Separater URL and old query string
			list($url,$query_old) = explode('?',$url,2);
			
			// Recreate URL with new query string
			if($query) $url .= "?".$query;
		}
		return $url;
	}
}

if(!function_exists('url_escape')) {
	/**
	 * Escapes quotes inside a URL string.	
	 * 
	 * @param string $url The URL you want to escape.
	 * @param boolean $js Is this string going to be used in javascript? &#039; get's reconverted before JS loads and screws up calls to js, ex: fw_confirm_redirect('Delete &#039;this&#039;?'); Default = 0
	 * @return string The URL with quotes escaped
	 */
	function url_escape($url,$js = 0) {
		// Escape
		$url = str_replace(array('"',"'"),array('&quot;','&#039;'),$url);
		
		// Double-escape
		if($js == 1) $url = str_replace("&#039;","&rsquo;",$url);
		
		// Return
		return $url;
	}
}

if(!function_exists('url_unescape')) {
	/**
	 * Unescapes quotes inside a URL string.	
	 * 
	 * @param string $url The URL you want to unescape.
	 * @return string The URL with quotes unescaped
	 */
	function url_unescape($url) {
		// Unescape
		return str_replace(array('&quot;','&#039;','&rsquo;',"’","!@%^","<>:{}"),array('"',"'","'","'",'"',"'"),$url);
	}
}

if(!function_exists('domain_page')) {
	/**
	 * Returns domain for use in creating page URL's.
	 *
	 * Basically determines if we have turned on 'query string' URLs instead of using URL rewriting via .htaccess.
	 * 
	 * Configuration values (key, type, default - description):
	 * - https, boolean, 1 - Return a HTTPS domain.
	 * 
	 * @param array $c An array of configuration values
	 * @return string The domain to use when creating page URL's
	 */
	function domain_page($c = NULL) {
		// Config
		if(!x($c[https])) $c[https] = 0;
		
		// Domain
		$domain = ($c[https] ? SDOMAIN : DOMAIN);
		
		// Query - if 'query string' URLs are turned on
		if(g('config.urls.query')) $domain .= "?sef=";
		
		// Return
		return $domain;
	}
}

if(!function_exists('redirect')) {
	/**
	 * Calls up a javascript redirect.													
	 * 
	 * @param string $url The URL you want to redirect the user.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function redirect($url,$c = NULL) {
		if(!$c[action]) $c[action] = 'replace'; // Redirect action: replace [default], assign, new (opens in new window)
		if(!x($c['exit'])) $c['exit'] = 1; // Exit script after printing redirect (to prevent any other scripts running)
			
		// Force rewriting of the session. Sometimes it dosen't update the session file before a redirect takes place. This forces it to. http://stackoverflow.com/a/13418518/502311
		session_regenerate_id(true); 
		session_write_close(); // If called on AJAX pages, this might actually 'reset' the session, as opposed to saving it. Not 100% sure, but keep in mind if future problems: http://www.php.net/manual/en/function.session-write-close.php#107945
		
		// URL
		$url = htmlspecialchars($url,ENT_QUOTES); // Convert <, >, ", ', & to special characters to avoid XSS attacks
		$url = str_replace('&amp;','&',$url); // Still need ampersands to separate query strings
		
		// Redirect
		print "<script type='text/javascript'>";
		if($c[action] == "new") print "window.open('".$url."','new-window-".random()."');";
		else print "location.".$c[action]."('".$url."');";
		print "</script>";
		
		// Exit
		if($c['exit'] == 1) exit;
	}
}

/************************************************************************************/
/**************************************** IP ****************************************/
/************************************************************************************/
if(!function_exists('ip')) {
	/**
	 * Returns the current user's IP address
	 * 
	 * @return string The current user's IP address
	 */
	function ip() {
		// Check ip from share internet
		if(!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
		// Check ip is pass from proxy
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
   		// Default
		else $ip = $_SERVER['REMOTE_ADDR'];
		
		// Only 1 (sometimnes has 2, comma seperated IPs for some reason)
		if($ip) list($ip,$scrap) = explode(',',$ip);
		
		// Return
   		return trim($ip);
	}
}

/************************************************************************************/
/************************************** Strings *************************************/
/************************************************************************************/
if(!function_exists('x')) {
	/**
	 * Determines if the passed variable has a value associated with it.
	 * 
	 * Returns true if value exists or false if it's empty:
	 * - 0 = true
	 * - "0" = true
	 * - "false" = true
	 * - false = false
	 * - "" = false
	 * - NULL = false
	 * - "NULL" = true
	 * - true = true
	 * - "true" = true
	 * 
	 * Also works for arrays.
	 * 
	 * @param mixed $value The value we want to check against
	 * @return boolean Whether or not the variable has a value associated with it
	 */
	function x($value) {
		if(strlen($value) > 0) return true;
		else if(is_array($value)) return true;
		else if(is_object($value)) return true;
		else if(is_resource($value)) return true;
		
		return false;
	}
}

if(!function_exists('string_pluralize')) {
	/**
	 * Pluralizes a word.
	 *
	 * Based onhttp://code.google.com/p/sehl/
	 * $count is the number of results (i.e. if only 1 result, won't pluralize)			
	 * 
	 * @param string $word The word we (may) want to pluralize.
	 * @param mixed $count The count, if anything but 1 we'll pluralize the string. If your count is a variable you'll pass it here. Default = 2
	 */
	function string_pluralize($word,$count = 2) {
		if($count != 1){
			$plural_rules = array(
				'/(x|ch|ss|sh)$/'         => '\1es',       # search, switch, fix, box, process, address
				'/series$/'               => '\1series',
				'/([^aeiouy]|qu)ies$/'    => '\1y',
				'/([^aeiouy]|qu)y$/'      => '\1ies',      # query, ability, agency
				'/(?:([^f])fe|([lr])f)$/' => '\1\2ves',    # half, safe, wife
				'/sis$/'                  => 'ses',        # basis, diagnosis
				'/([ti])um$/'             => '\1a',        # datum, medium
				'/person$/'               => 'people',     # person, salesperson
				'/man$/'                  => 'men',        # man, woman, spokesman
				'/child$/'                => 'children',   # child
				'/s$/'                    => 's',          # no change (compatibility)
				'/$/'                     => 's'
			);
			foreach($plural_rules as $rule => $replacement) {
				if(preg_match($rule, $word)) {
					return preg_replace($rule, $replacement, $word);
				}
			}
		} 
		else {
			return $word;
		}
	}
}

if(!function_exists('string_ownership')) {
	/**
	 * Adds 's or ' to end of string to indicate ownership (ex: john's or james'	).	
	 * 
	 * @param string $string The string you want to apply 'ownership' to.
	 & @return string The original string with ownership applied.
	 */
	function string_ownership($string) {
		// Error
		if(!$string) return;
		
		// Ownership
		if(substr($string,-1) == "s") $string .= "'";
		else $string .= "'s";
		
		// Return
		return $string;		
	}
}

if(!function_exists('string_an')) {
	/**
	 * Prepends the string with either 'a' or 'an' depending on if the string starts with a vowel or not.
	 * 
	 * @param string $string The string you want to prepend with 'a' or 'an'.
	 * @param array $c An array of configuration values. Default = NULL
	 & @return string The original string with 'a' or 'an' prepended.
	 */
	function string_an($string,$c = NULL) {
		// Error
		if(!$string) return;
		
		// Config
		if(!x($c[capitalize])) $c[capitalize] = 0; // Do we want to capitalize the a or an?
		
		// A/a
		$string_new = ($c[capitalize] ? "A" : "a");
		// n
		if(in_array(strtolower(substr($string,0,1)),array('a','e','i','o','u')) == "s") $string_new .= "n";
		// Space
		$string_new .= " ";
		// String
		$string_new .= $string;
		
		// Return
		return $string_new;		
	}
}

if(!function_exists('string_limit')) {
	/**
	 * Limits string to given length, adding $c[end] if it exceeds the given limit.								
	 * 
	 * @param string $string The string you want to limit.
	 * @param int $limit The limit, in characters, of the resulting string.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function string_limit($string,$limit,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Config
		if(!$c[end]) $c[end] = ".."; // The text you want to append to the end of the shortened string
		if(!$c['break']) $c['break'] = NULL; // Do you want to stop at a specific break point? (w = word sensitive, p = paragraph sensitive)
		if(!x($c[tags])) $c[tags] = 1; // Do you want to allow for HTML tags in the string?
		if(!x($c[greedy])) $c[greedy] = 0; // Do you want to show full text if it would fit in $limit + strlen($c[end])?
		if(!x($c[restrict])) $c[restrict] = 1; // Do you want to restrict the returned text to given length (meaning the strlen($c[end]) will also fit)
		
		// Strip tags
		if($c[tags] == 0) {
			$string = str_replace(array('<br />','<br>','<BR>','<BR />'),' ',$string); // Change breaks to spaces.
			$string = strip_tags($string); // Strip tags
		}
		
		// Change entities to characters so we don't cut one half way through
		$string = html_entity_decode_utf8(str_replace("&nbsp;"," ",$string));
		
		// String lenght
		$string_length = strlen($string);
		if($c[greedy] == 1) $string_length -= strlen($c[end]);
		if($c[restrict] == 1) $limit -= strlen($c[end]);
		// Too long
		if($string_length > $limit) {
 			// Paragraph Sensetive
			if($c['break'] == 'p') {
				// Break Points
				$array = array('</p>','<br />','<br>','<br/>','</P>','<BR />','<BR>','<BR/>');
				// Get First Breaking Point
				foreach($array as $tag) {
					$ex = explode($tag,$string);
					$tags[$tag] = strlen($ex[0]);
				}
				asort($tags);
				// Get Correct Length
				$x = 0;
				foreach($tags as $tag => $l) {
					if($x == 0) {
						$ex = explode($tag,$string);
						$string = NULL;
						foreach($ex as $y => $e) {
							if(strlen($string) < $limit or !$string) $string .= ($y > 0 ? $tag : '').$e;
						}
						break;
					}
				}
			}
			// Word Sensetive
			else if($c['break'] == 'w') {
				// Method 1
				//$string = (preg_match('/^(.*)\W.*$/', substr($string,0,$limit + 1), $matches) ? $matches[1] : substr($string,0,$limit));
				
				// Method 2
				/*for($i = 0;$string[$limit + $i] != " ";$i++) {
					if(!$string[$limit + $i]) {
						// Speed
						function_speed(__FUNCTION__,$f_r);
						
						// Return
						return $string;
					}
				}
				$string = substr($string,0,$limit + $i);*/
				
				// Method 3
				$i = 0;
				$break = 0;
				while($break == 0) {
					// Reached end of string, don't break at all (just return original)
					if(!x($string[$limit + $i])) {
						// Return
						return $string;
					}
					// Reched a space (or <br tag), break
					if($string[$limit + $i] == " " or strtolower($string[$limit + $i].$string[$limit + $i + 1].$string[$limit + $i + 2]) == "<br") {
						$break = 1;
					}
					else $i++;
				}
				$string = substr($string,0,$limit + $i);
			}
			// No Sensetivity
			else $string = substr($string,0,$limit);
			
			// Close any open HTML tags (also strips any partial tags)
			$string = string_close_tags($string);
			
			// Add 'end'
			$string .= $c[end];
		}
		
		// Change & back to &amp;
		$string = str_replace("&","&amp;",$string);
		$string = str_replace("&amp;amp;","&amp;",$string);
		
		// Replace Line Breaks (if .. is on new line pHAML treats it as <div class=".">)
		for($x = 0;$x <= 20;$x++) $string = str_replace(array("\r\n".$c[end],"\n\r".$c[end],"\r".$c[end],"\n".$c[end]),$c[end],$string);
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_strip_partial_tags')) {
	/**
	 * Strips partial tags (ex: <sp or </spa) off end of text (useful after substr()).
	 * 
	 * @param string $string The string you want to strip any partial tags from.
	 * @return string The cleaned up string with partial tags now removed.
	 */
	function string_strip_partial_tags($string) {
		$string = preg_replace('#<[a-z]+[^>]*?$#i', '', $string); // Strip partial tags (ex: <spa)
		$string = preg_replace('#<\/([a-z]*)$#i', '', $string); // Strip partical close tags (ex: </spa)
		return $string;
	}
}
		
if(!function_exists('string_close_tags')) {
	/**
	 * Closes any open HTML tags (useful after a substr() call).						
	 * 
	 * @param string $string The string you want to effect.
	 * @return string The string with all open HTML tags closed.
	 */
	function string_close_tags($string) {
		// Strip Partial Tags (ex: <sp or </spa)
		$string = string_strip_partial_tags($string);
		
		preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $string, $result);
		$openedtags = $result[1];
		preg_match_all('#</([a-z]+)>#iU', $string, $result);
		$closedtags = $result[1];
		$len_opened = count($openedtags);
		if(count($closedtags) == $len_opened) {
			return $string;
		}
		$openedtags = array_reverse($openedtags);
		for($i = 0; $i < $len_opened; $i++) {
			if(!in_array($openedtags[$i], $closedtags)) $string .= '</'.$openedtags[$i].'>'; 
			else unset($closedtags[array_search($openedtags[$i], $closedtags)]);
		}
		return $string;
	} 
}
		
if(!function_exists('string_breaks')) {
	/**
	 * Converts all new lines (\r, \n, or \r\n) to <br /> tags.
	 *
	 * Note, it only applies line breaks if the string doesn't yet contain a <br /> or <p> tag unless $force == 1;		
	 * 
	 * @param string $string The string you want to add line breaks to.
	 * @param boolean $force Do you want to force coversion of line breaks to <br /> tags, even if there are already <br /> or <p> tags in the string? Default = 0
	 * @return string The string with line breaks replaced by <br /> tags.
	 */
	function string_breaks($string,$force = 0) {
		// Breaks
		if(!strstr(strtolower($string),'<br') and !strstr(strtolower($string),'</p>') or $force) $string = nl2br($string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_strip_breaks')) {
	/**
	 * Strips line breaks and (optionally) tabs from a string.							
	 * 
	 * @param string $string The string you want to strip the line breaks from.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The string with line breaks stripped out.
	 */
	function string_strip_breaks($string,$c = NULL) {
		// Config
		if(!x($c[tabs])) $c[tabs] = 1; // Strip tabs
		if(!x($c[replace])) $c[replace] = NULL; // What to replace them with
		
		// Replace
		$string = preg_replace("/(\r\n|\r|\n|".($c[tabs] ? "\t" : "").")/",$c[replace],$string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_strip_tables')) {
	/**
	 * Removes all tables from the string while retaining the table content.
	 *
	 * @param string $string The string you want to remove tables from.
	 * @return string The string with all table elements removed.
	 */
	function string_strip_tables($string) {
		// Table
		$string = preg_replace('/<table(.*?)>/','',$string);
		$string = preg_replace('/<\/table>/','',$string);
		
		// Rows
		$string = preg_replace('/<tr(.*?)>/','',$string);
		$string = preg_replace('/<\/tr>/','',$string);
		
		// Head
		$string = preg_replace('/<thead(.*?)>/','',$string);
		$string = preg_replace('/<\/thead>/','',$string);
		$string = preg_replace('/<th(.*?)>/','',$string);
		$string = preg_replace('/<\/th>/','',$string);
		
		// Body
		$string = preg_replace('/<tbody(.*?)>/','',$string);
		$string = preg_replace('/<\/tbody>/','',$string);
		$string = preg_replace('/<td(.*?)>/','',$string);
		$string = preg_replace('/<\/td>/','',$string);
		
		// Foot
		$string = preg_replace('/<tfoot(.*?)>/','',$string);
		$string = preg_replace('/<\/tfoot>/','',$string);
		$string = preg_replace('/<tf(.*?)>/','',$string);
		$string = preg_replace('/<\/tf>/','',$string);
		
		// Return
		return $string;
	}
}
		
if(!function_exists('string_price')) {
	/**
	 * Converts number to a price format, including the dollar sign at the beginning (ex: 1.4 => $1.40).	
	 * 
	 * @param double $number The number you want to turn into a price string.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The number in price format.
	 */
	function string_price($number,$c = NULL) {
		// Config
		if(!$c[symbol]) $c[symbol] = "$";
		
		// String
		$string = $c[symbol].number_format(abs($number),2);
		if($number < 0) $string = "-".$string;
		
		// Return
		return $string;
	}
}

if(!function_exists('string_random')){
	/**
	 * Creates and returns a random string of the given $length using the $characters provided (default is alphanumeric characters).
	 *
	 * @param int $length The length of the random string you want to create. Default = 32
	 * @param string $characters The characters that can be used in the string. Default = alphanumeric characters (abcdefghijklmnopqrstuvwqyz0123456789)
	 * @return string The randomly generated string.
	 */
	function string_random($length = 32,$characters = 'abcdefghijklmnopqrstuvwqyz0123456789'){
		// String
		for($x = 1;$x <= $length;$x++) {
			$string .= substr($characters,mt_rand(0,strlen($characters)),1);
		}
		
		// Return
		return $string;
	}
}

/************************************************************************************/
/************************************** Numbers *************************************/
/************************************************************************************/
if(!function_exists('is_int_value')) {
	/**
	 * Checks whether value is an integer (number without decimals).
	 *
	 * Like is_int() but variable doesn't have to be an integer type Ex: $string = "123"; is_int_value($string) = true; is_int($string) = false;		
	 * 
	 * @param string $string The string we're checking against.
	 * @returnstring_convert_special_characters boolean Whether or not the $string is an integger.
	 */
	function is_int_value($string) {
		if(strlen($string) and preg_match('/^\-?[0-9]+$/',$string)) return true;
		else return false;
	}
}

if(!function_exists('random')) {
	/**
	 * Returns a random number which is unique within the current session.
	 * 
	 * @param int $min The minimum value for the random number. Default = 0
	 * @param int $max The maximum value for the random number. Default = 999999999
	 * @return int The unique random number.
	 */
	function random($min = 0,$max = 999999999) {
		$r = mt_rand($min,$max);
		if(!$_SESSION['random'][$r]) {
			$_SESSION['random'][$r] = 1;
			$return = $r;
		}
		else $return = random($min,$max);
		
		return $return;
	}
}

/************************************************************************************/
/************************************** Arrays **************************************/
/************************************************************************************/
if(!function_exists('print_array')) {
	/**
	 * Prints an array in a more easily readable format
	 * 
	 * @param array $array The array we want to print.
	 * @param boolean $return Return the array rather than print it. Legacy, we'd use return_array() now. Default = 0
	 */
	function print_array($array,$return = 0) {
		if($return) return return_array($array);
		else {
			print "<xmp>";
			print_r($array);
			print "</xmp>";
		}
	}
}

if(!function_exists('return_array')) {
	/**
	 * Returns an array as a string in a more easily readable format.
	 * 
	 * @param array $array The array we want to print.
	 * @return string A string representation of the array.
	 */
	function return_array($array) {
		ob_start();
		
		print "<xmp>";
		print_r($array);
		print "</xmp>";
		
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}
}

if(!function_exists('array_merge_associative')) {
	/**
	 * Merges 2, multi-level, associative arrays.
	 * 
	 * @param array $array_1 The first array we want to merge.
	 * @param array $array_2 The second array we want to merge. Its values take precedence.
	 * @param boolean $exists Do you only want to use the $array_2 value if the value exists (see x() function). Default = 0
	 * @return array The merged array.
	 */
	function array_merge_associative($array_1,$array_2,$exists = 0) {
		if(is_array($array_2)) {
			foreach($array_2 as $k => $v) {
				if(array_key_exists($k,$array_1) && is_array($v)) $array_1[$k] = array_merge_associative($array_1[$k],$array_2[$k],$exists);
				else {
					if(!$exists or x($v)) $array_1[$k] = $v;
				}
			}
		}
		return $array_1;
	}
}

if(!function_exists('array_eval')) {
	/**
	 * Evaluates given array to get value.
	 *
	 * Checks each level of the array to avoid "Cannot use string offset as an array" error.
	 * Note, you can also update the value by passing a 3rd parameter ($value). If you want to empty the value (set to NULL) pass "NULL" (in quotes).
	 * If a key within one of the levels of your key string contains a period you can escape it with a preceding backslash (\.).
	 * 
	 * @param string $variable The variable of the array we're evaluating.
	 * @param string $key The period separated key string representing the keys in the different levels of the array. Example: "users.db.id". Default = NULL
	 * @param mixed $value If we pass a $value, we'll update the value of the given array's key string rather than just returning it. Default = NULL
	 * @return mixed The value retrieved from the array.
	 */
	function array_eval($variable,$key = NULL,$value = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Passed an actual array (or nothing), perform the operation locally // Passing actual array. Never use currently so remove for now so as to optimize a bit.
		if(is_array($variable) or !$variable) {
			$variable_array = $variable;
			$variable = "\$variable_array";
			$variable_local = 1;
		}
		// Passed $__GLOBAL (most often used)
		else if(strstr($variable,'$__GLOBAL')) global $__GLOBAL;
		// Passed some other global variable (other than $_SESSION, $_GET, and $_POST which are already global).
		else if($variable != '$_SESSION' and $variable != '$_GET' and $variable != '$_POST') $variable = "\$GLOBALS['".substr($variable,1)."']";
		
		// Escaped periods
		$escaped = 0;
		if(strstr($key,'\.')) {
			$escaped = 1;
			// Temporarily escape
			$key = str_replace('\.','%$^&*',$key);
		}
		
		// Keys
		if(strlen($key)) $keys = explode('.',$key);
		$count = count($keys) - 1;
		
		// Set Value
		if(x($value)) {
			$variable_value = 1; // Passing actual array
			if($value == "NULL") $value = NULL; // Explicitly told to empty value
			foreach($keys as $var) {
				if(x($var)) $string .= "['".$var."']";
				else $string .= "[]";
			}
			if($escaped) $string = str_replace('%$^&*','.',$string); // Unescape escaped periods
			$eval = $variable.$string.' = $value;';
		}
		
		// Get Value
		else {
			if($keys) {
				foreach($keys as $x => $var) {
					$string .= "['".$var."']";
					if($x < $count) {
						$if .= 'if(is_array('.$variable.$string.')) {';
						$endif .= "}";
					}
				}
				if($escaped) $string = str_replace('%$^&*','.',$string); // Unescape escaped periods
			}
			
			// Build eval
			$eval = $if.'$value = '.$variable.$string.';'.$endif;
		}
		
		// Debug
		//debug($eval);
		
		// Evaluate
		eval($eval);
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
		
		// Return
		if($variable_value and $variable_local) return $variable_array; // Passing actual array
		else return $value;
	}
}

if(!function_exists('array_first')) {
	/**
	 * Returns first value of an array													
	 * 
	 * @param array $array The array you want to get the first value of.
	 */
	function array_first($array) {
		return reset($array);
	}
}

if(!function_exists('array_first_key')) {
	/**
	 * Returns first key of an array													
	 * 
	 * @param array $array The array you want to get the first key of.
	 */
	function array_first_key($array) {
		$keys = array_keys($array);
		return $keys[0];
	}
}

if(!function_exists('array_last')) {
	/**
	 * Returns last value of an array													
	 * 
	 * @param array $array The array you want to get the last value of.
	 */
	function array_last($array) {
		return end($array);
	}
}

if(!function_exists('array_last_key')) {
	/**
	 * Returns last key of an array														
	 * 
	 * @param array $array The array you want to get the last key of.
	 */
	function array_last_key($array) {
		if(is_array($array)) {
			$keys = array_keys($array);
			return end($keys);
		}
	}
}

if(!function_exists('array_compare_length')) {
	/**
	 * Compares the 2 array values, showing shortest first. 
	 *
	 * For use with usort(). Example:
	 * usort($array,'array_compare_length');
	 * 
	 * @param mixed $a The first value to compare.
	 * @param mixed $b The second value to compare.
	 * @return int Whether or not $a is longer (> 1), shorter (< 1), or the same length (0) as $b.
	 */
	function array_compare_length($a,$b){
		return strlen($a) - strlen($b);
	}
}

/************************************************************************************/
/************************************* Time *****************************************/
/************************************************************************************/
if(!function_exists('time_format')) {
	/**
	 * Returns given time in desired format.										
	 * 
	 * @param mixed $time The time we want to reformat. Can be a unix timestamp, date, datetime, or any other format strtotime() can read. Default = current unix timestamp
	 * @param string $format The format we want to return the time in: datetime, date, unix, atom. Default = datetime
	 */
	function time_format($time,$format = "datetime") {
		// Format
		$format = strtolower($format);
		
		// Time
		if($time != 0) {
			if(!is_int_value($time)) $time = adodb_strtotime($time); // Try and turn to UNIX if not already
		}
		else $time = 0;
		
		// Max out at 9999-12-31 23:59:59
		if($time > 253402329599 and $format != "unix") $time = 253402329599;
		
		// Format
		if($time != 0) {
			if($format == "datetime") $return = adodb_date('Y-m-d H:i:s',$time); // datetime
			if($format == "date") $return = adodb_date('Y-m-d',$time); // date
			if($format == "unix") $return = $time; // unix
			if($format == "atom") $return = adodb_date(DATE_ATOM,$time); // atom
			
			// Return
			return $return;
		}
	}
}

if(!function_exists('time_span')) {
	/**
	 * Returns string of time span for given start and end times				
	 * 
	 * @param string $start The start time (can be UNIX, date, or timestamp)
	 * @param string $end The end time (can be UNIX, date, or timestamp). Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The time span string.
	 */
	function time_span($start,$end = NULL,$c = NULL) {
		// Config
		if(!x($c[time])) $c[time] = 1; // Include time (as opposed to just the date)
		if(!x($c[time_multiple])) $c[time_multiple] = ($c[multiple_time] ? $c[multiple_time] : "span"); // How to handle time value on multi-day events: span [default], daily // Legacy, used to be multiple_time
		if(!x($c[time_midnight])) $c[time_midnight] = 0; // Show time if it's midnight (usually means they just selected a date, not a time, so we don't want to show)
		if(!is_numeric($start)) $start = adodb_strtotime($start);
		if(!is_numeric($end)) $end = adodb_strtotime($end);
		$start_time = adodb_date('g:i A',$start);
		$end_time = adodb_date('g:i A',$end);
		if($start_time == "12:00 AM" and $end_time == "12:00 AM" and !$c[time_midnight]) $c[time] = 0;
		
		// String
		if($start > 0) {
			// Date/Time
			if($c[time] == 1) {
				if(adodb_date('m/d/Y',$start) != adodb_date('m/d/Y',$end) and $end) {
					if($c[time_multiple] == "span") $date = $start_time.", ".adodb_date('M j, Y',$start).($end ? " - ".$end_time.", ".adodb_date('M j, Y',$end) : "");
					if($c[time_multiple] == "daily") $date = adodb_date('M j, Y',$start)." - ".adodb_date('M j, Y',$end).", ".$start_time." - ".$end_time;
				}
				else $date = adodb_date('F j, Y',$start).", ".$start_time.($end ? "-".$end_time : "");
			}
			// Date
			if($c[time] == 0) $date = adodb_date('F j, Y',$start).($end && adodb_date('m/d/Y',$start) != adodb_date('m/d/Y',$end) ? " - ".adodb_date('F j, Y',$end) : "");
		}
		
		// Return
		return $date;
	}
}

if(!function_exists('seconds2minutes')) {
	/**
	 * Turns number of seconds (90) into more readable format of minutes (1:30).		
	 * 
	 * @param int $seconds The number of seconds you want to convert into minutes format.
	 * @return string The seconds now in minutes format (ex: 92 seconds becomes 1:32).
	 */
	function seconds2minutes($seconds) {
		$seconds = round($seconds);
		$m = floor($seconds / 60);
		$s = round($seconds - (60 * $m));
		return $m.":".str_pad($s,2,0,STR_PAD_LEFT);
	}
}

if(!function_exists('minutes2hours')) {
	/**
	 * Turns number of minutes (90) into more readable format or hours (1:30).		
	 * 
	 * @param int $minutes The number of minutes you want to convert into hours format.
	 * @return string The minutes now in hours format (ex: 92 minutes becomes 1:32).
	 */
	function minutes2hours($minutes) {
		return seconds2minutes($minutes);
	}
}

if(!function_exists('seconds2hours')) {
	/**
	 * Turns number of seconds (4900) into more readable format of hours (1:21:40).	
	 * 
	 * @param int $seconds The number of seconds you want to convert into hours format.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The seconds now in hours format (ex: 4900 seconds becomes 1:21:40).
	 */
	function seconds2hours($seconds,$c = NULL) {
		if(!x($c[pad])) $c[pad] = 0; // 'Pad' numbers so we'd have 01:03:45 instead of 1:03:45
		$seconds = round($seconds);
		
		// Times
		$m = floor($seconds / 60);
		$h = floor($m / 60);
		$s = $seconds - (60 * $m);
		$m = $m - (60 * $h);
		
		// Pad
		if($c[pad]) $h = str_pad($h,2,0,STR_PAD_LEFT);
		
		return $h.":".str_pad($m,2,0,STR_PAD_LEFT).":".str_pad($s,2,0,STR_PAD_LEFT);
	}
}

if(!function_exists('minutes2seconds')) {
	/**
	 * Turns formated minutes (1:30) into seconds (90).								
	 * 
	 * @param string $minutes A minutes string you want to convert to the number of seconds
	 * @return int The number of seconds in the minutes string (ex: 1:32 becomes 92).
	 */
	function minutes2seconds($minutes) {
		list($m,$s) = explode(':',$minutes);
		return ($m * 60) + $s;
	}
}

if(!function_exists('hours2minutes')) {
	/**
	 * Turns formated hours (1:30) into minutes (90).								
	 * 
	 * @param string $hours An hours string you want to convert to the number of minutes.
	 * @return int The number of minutes in the hours string (ex: 1:32 becomes 92).
	 */
	function hours2minutes($hours) {
		return minutes2seconds($hours);
	}
}

if(!function_exists('hours2seconds')) {
	/**
	 * Turns formated hours (1:21:40) into seconds (4900).							
	 * 
	 * @param string $hours An hours string you want to convert to the number of seconds.
	 * @return int The number of seconds in the hours string (ex: 1:21:40 becomes 4900).
	 */
	function hours2seconds($hours) {
		$ex = explode(':',$hours);
		$h = $ex[0];
		$m = $ex[1];
		$s = $ex[2];
		return ($h * 3600) + ($m * 60) + $s;
	}
}

if(!function_exists('time_24_12')) {
	/**
	 * Converts a 24 hour time format (ex: 18:45) to a 12 hour time (ex: 6:45 PM)					
	 * 
	 * @param string $time The time in 24 hour format.
	 * @return string The time in 12 hour format.
	 */
	function time_24_12($time) {
		list($hour,$minute) = explode(':',$time);
		if(x($hour) and x($minute)) {
			$meridiem = "AM";
			if($hour >= 12) $meridiem = "PM";
			if($hour > 12) $hour -= 12;
			if($hour == 0) $hour = 12;
			
			return $hour.":".$minute." ".$meridiem;
		}
	}
}

/************************************************************************************/
/************************************* Cache ****************************************/
/************************************************************************************/
if(!function_exists('cache_save')) {
	/**
	 * Stores given data in cache using given name
	 * 
	 * Configuration values (key, type, default - description):
	 * - path, string, CACHE_PATH - The path where we want to store the cache file
	 * 
	 * @param string $name The name we want to use when storing the cached data. We'll use this when retrieving the cached data later.
	 * @param string|array $data The data (string or array) we want to cache
	 * @param array $c An array of configuration values
	 */
	function cache_save($name,$data,$c = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Disabled
		if((!CACHE_ACTIVE or !DEBUG_CACHE_ACTIVE) and !$c[force]) return;
		
		// Have data to save
		if(x($data)) {
			
			// Database
			if(CACHE_METHOD == "db") {
				// Save
				$db = db::load();
				$db->q("REPLACE INTO cache SET cache_id = '".$name."', cache_data = '".a(serialize($data))."', cache_date = '".date('Y-m-d')."', cache_created = '".time()."'");
			}
			// File
			else {
				// Config
				if(!$c[path]) $c[path] = CACHE_PATH;
				
				// Folder
				$folder = cache_folder($name);
				if(!file_exists($c[path].$folder)) mkdir_recursive($c[path].$folder);
				
				// File
				$file = $c[path].$name.".txt";
				
				// Save
				file_put_contents($file,serialize($data));
			}
		}

		// Speed
		//function_speed(__FUNCTION__,$f_r);
	}
}

if(!function_exists('cache_get')) {
	/**
	 * Retrieves cached data stored under given name (if it exists)
	 * 
	 * Configuration values (key, type, default - description):
	 * - life, int, 3600 - Liftime of cache (in seconds) before it expires. Pass 0 for no expiration date (fresh forever).
	 * - path, string, CACHE_PATH - The path where we want to store the cache file
	 * 
	 * @param string $name The name we want used when storing the data in the cache
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string|array The original data (if it hasn't expired)
	 */
	function cache_get($name,$c = NULL) {
		// Disabled
		if((!CACHE_ACTIVE or !DEBUG_CACHE_ACTIVE) and !$c[force]) return;
		
		// Config
		if(!$c[life]) $c[life] = 7200;
			
		// Database
		if(CACHE_METHOD == "db") {
			$db = db::load();
			$row = $db->f("SELECT cache_data,cache_created FROM cache WHERE cache_id = '".$name."'");
			if($row[cache_created]) {
				// Expired
				if($c[life] and (time() - $row[cache_created]) > $c[life]) {
					return false;
				}
				// Fresh
				else {
					return unserialize($row[cache_data]);
				}
			}
		}
		// File
		else {
			// Config
			if(!$c[path]) $c[path] = CACHE_PATH;
			
			// File
			$file = $c[path].$name.".txt";
			if(file_exists($file)) {
				// Date of cache
				if($c[life]) $created = filemtime($file);
				//debug("life: ".$c[life].", age: ".(time() - $created).", created: ".$created.", time: ".time(),$c[debug]);
					
				// Expired
				if($c[life] and ($created == 0 or (time() - $created) > $c[life])) {
					//debug("expired",$c[debug]);
					// Delete
					//unset($file); // Will almost always get overwritten momentarily so this isn't very useful
					// Return
					return false;
				}
				// Fresh
				else {
					//debug("fresh",$c[debug]);
					// Return
					return unserialize(file_get_contents($file));
				}
			}
		}
	}
}

/*if(!function_exists('cache_query')) {
	/**
	 * Returns cached results for the given query if they exist.  If they don't, it runs the query and caches the results.
	 *
	 * NOT YET WORKING, NOR IMPLEMENTED ANYWHERE
	 * 
	 * Configuration values (key, type, default - description):
	 * - folder, string, queries - The folder within the cache we want to store the queries in
	 * - life, int, 3600 - Liftime of cache (in seconds) before it expires
	 * 
	 * @param string $query The query we want results for
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of the query's results
	 */
	/*function cache_query($query,$c = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Disabled
		if(!CACHE_ACTIVE or !DEBUG_CACHE_ACTIVE) return;
		
		// Config
		if(!$c[folder]) $c[folder] = "queries"; // Folder we want to store the queries in
		if(!$c[life]) $c[life] = 3600; // Liftime of cache (in seconds)
		
		// Name
		$name = md5($query);
		
		// Cached?
		if($results = cache_get($c[folder].'/'.$name,array('life' => $c[life]))) {
			// Speed
			function_speed(__FUNCTION__,$f_r,1);
			// Return
			return $results;
		}
		// No, get results
		else {
			// Create results array
			$sql = q($query);
			while($qry = f($sql)) {
				$results[] = $qry;
			}
			
			// Save results
			cache_save($c[folder].'/'.$name,$results);
		}
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $results;
	}
}*/

if(!function_exists('cache_delete')) {
	/**
	 * Deletes given cached file/folder ($name)
	 * 
	 * Configuration values (key, type, default - description):
	 * - path, string, CACHE_PATH - The path where cached files are stored
	 * 
	 * @param string $name The name we want used when storing the data in the cache
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string|array The original data (if it hasn't expired)
	 */
	function cache_delete($name = NULL,$c = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Database
		if(CACHE_METHOD == "db") {
			$db = db::load();
			if(!$name) $db->q("TRUNCATE TABLE cache");
			else $db->q("DELETE FROM cache WHERE cache LIKE '".$name."%'");
		}
		// File
		else {
			// Config
			if(!$c[path]) $c[path] = CACHE_PATH;
			
			// Delete All Files
			if(!$name) directory_delete($c[path],0);
			else {
				// Delete Folder
				if(is_dir($c[path].$name)) directory_delete($c[path].$name,0);
				// Delete File
				if(is_file($c[path].$name.".txt")) unlink($c[path].$name.".txt");
			}
		}
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
	}
}

if(!function_exists('cache_delete_expired')) {
	/**
	 * Deletes expired files in given cache folder (default is all of cache)
	 * 
	 * Configuration values (key, type, default - description):
	 * - life, int, 259200 (2 days) - The number of seconds after which a cached file is considered 'expired'
	 * - path, string, CACHE_PATH - The path where cached files are stored
	 * 
	 * @param string $name The name we want used when storing the data in the cache
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string|array The original data (if it hasn't expired)
	 */
	function cache_delete_expired($folder = NULL,$c = NULL) {
		// Config
		if(!$c[life]) $c[life] = 3600 * 12; // The number of seconds after which a cached file is considered 'expired'. Default = 12 hours.
		
		// Database
		if(CACHE_METHOD == "db") {
			$db = db::load();
			$db->q("DELETE FROM cache WHERE cache_created < '".(time() - $c[life])."'");
		}
		// File
		else {
			// Config
			if(!$c[path]) $c[path] = CACHE_PATH;
		
			// Limits
			ini_set('max_execution_time',1800); // 30 Minutes
			if(ini_get('safe_mode') == 0) set_time_limit(1800);
			ini_set('memory_limit', 1024 * 1024 * 1000); // 1000 MB
			
			// Delete
			directory_delete($c[path].$folder,0,array('life' => $c[life],'skip' => array('css','javascript')));
		}
	}
}

if(!function_exists('cache_config')) {
	/**
	 * Turns an array of configuration values into a string which can then be used for the cache name, ensuring that the cached data is specific to all it's configuration values.
	 * 
	 * @param array $array The array of configuration values we want to encode
	 * @return string An encoded string created from the array of congiguration values
	 */
	function cache_config($array = NULL) {
		// Don't want to store these values
		unset($array[cache],$array[cache_life],$array[debug]);

		// Serialize
		$string = serialize($array);
		// Encode
		$string = md5($string);
		
		return $string;
	}
}	

if(!function_exists('cache_folder')) {
	/**
	 * Returns folder for given cache name string. Example, "items/users/1/photo" would return "items/users/1/".
	 * 
	 * @param string $string The cache name string
	 * @return string A string of the folders in the cache name string (the given $string minus the actual cache name)
	 */
	function cache_folder($string) {	
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Folder
		$folder = dirname($string);
		if($folder == ".") $folder = NULL;
		
		if($folder) {
			if(substr($folder,-1) != "/") $folder .= "/";
			if(substr($folder,0,1) == "/") $folder = substr($folder,1);
		}
			
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// REturn
		return $folder;
	}
}

/************************************************************************************/
/************************************ Escaping **************************************/
/************************************************************************************/
if(!function_exists('s')) {
	/**
	 * Strips escaping slashes from a string.				
	 * 
	 * You can also pass an array and the function will strip slashes from all the values in the array.
	 *
	 * @param string|array $string The string (or array) you want to stripslashes from.
	 * @return string|array The string (or array) with escaping slashes stripped.
	 */
	function s($string = NULL) {
		// Array
		if(is_array($string)) {
			foreach($string as $k => $v) $result[$k] = s($v);
		}
		// String
		else {
			$result = stripslashes($string);
		}
		
		// Return
		return $result;
	}
}

if(!function_exists('a')) {
	/**
	 * Adds escaping slashes to special characters in a string.
	 *
	 * You can also pass an array and the function will add escaping slashes to special characters contained in the values of the array.															
	 * 
	 * @param string|array $string The string (or array) you want to add escaping slashes to.
	 * @param boolean $force Do you want to force the adding of slashes. By default we only addslashes if they haven't alredy been added. Default = 0
	 * @return string|array The string (or array) with special characters escaped by slashes.
	 */
	function a($string,$force = 0) {
		// Array
		if(is_array($string)) {
			foreach($string as $k => $v) $result[$k] = a($v);
		}
		// String
		else {
			// Only Addslashes if they haven't yet been added (or if $force == 1) // Want to addslahes if there are ANY unescaped quotes
			/*$addslashes = 0;
			if($force == 1) $addslashes = 1;
			else if(!strstr($string,"\'") and !strstr($string,'\"') and !strstr($string,'\r\n')) $addslashes = 1;
			if($addslashes) {*/
				// Stripslashes
				//$string = stripslashes($string);
				
				// Need a Connection // have too many problems with \r\n, just use addslashes
				/*global $CONNECT;
				if(!$CONNECT) $CONNECT = my_c();
				// Escape Quotes, etc.
				if($CONNECT) {
					if(function_exists("mysql_real_escape_string")) $string = mysql_real_escape_string($string);
					else $string = mysql_escape_string($string);
				}
				else*/ $string = addslashes($string);
				// Change $ Signs
				//$string = str_replace('$','&#36;',$string);
				// Trim
				//$string = trim($string);
				
				// AJAX submitted variables only get a \n for some reason so change to \r\n
				if($_GET['ajax_action'] or $_POST['ajax_action']) $string = str_replace('\r\r\n','\r\n',str_replace('\n','\r\n',$string));
			//}
			
			// Result
			$result = $string;
		}
		
		// Return
		return $result;
	}
}

if(!function_exists('string_encode')) {
	/**
	 * Encodes a string for use in a form or javascript
	 * 
	 * @param string $string The string you want to encode.
	 * @param boolean $js Is this string going to be used in javascript? &#039; get's reconverted before JS loads and screws up calls to js, ex: fw_confirm_redirect('Delete &#039;this&#039;?');Default = 0
	 * @return string The encoded string
	 */
	function string_encode($string,$js = 0) {
		// Encode
		$string = htmlentities_utf8(html_entity_decode_utf8($string));
		
		// Double-encode
		if($js == 1) $string = str_replace('&#039;','&rsquo;',$string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_decode')) {
	/**
	 * Decodes a string that was encoded via string_encode().
	 * 
	 * @param string $string The string you want to decode.
	 * @return string The decoded string
	 */
	function string_decode($string) {
		// Decode
		$string = html_entity_decode_utf8($string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_escape')) {
	/**
	 * Escapes special characters in a string. Namely, the period (.) we use when separating array levels.
	 * 
	 * @param string $string The string you want to escape.
	 * @return string The escaped string.
	 */
	function string_escape($string) {
		// Periods
		$string = str_replace(array('.','\\.'),array('\.','\.'),$string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_unescape')) {
	/**
	 * Unescapes special characters in a string. Namely, the period (.) we use when separating array levels.
	 * 
	 * @param string $string The string you want to unescape.
	 * @return string The unescaped string.
	 */
	function string_unescape($string) {
		// Periods
		$string = str_replace('\.','.',$string);
		
		// Return
		return $string;
	}
}

if(!function_exists('string_array')) {
	/**
	 * Changes a period separated string (ex: settings.sitemap.views) to an array string (ex: settings[sitemap][views]).
	 * 
	 * @param string $string The period separated string you want to convert to an array string.
	 * @return string The array string.
	 */
	function string_array($string) {
		$ex = explode('.',$string);
		foreach($ex as $k => $v) {
			if($k > 1) $array .= "]";
			if($k > 0) $array .= "[";
			$array .= $v;
		}
		if(count($ex) > 1) $array .= "]";
		
		// Return
		return $array;
	}
}

if(!function_exists('array_string')) {
	/**
	 * Changes an array string (ex: settings[sitemap][views]) to a period separated string (ex: settings.sitemap.views).
	 * 
	 * @param string $array The array string you want to convert to a period separated string.
	 * @return string The period separated string
	 */
	function array_string($array) {
		// String
		$string = str_replace(array('[',']'),array('.',''),$array);
		
		// Return
		return $string;
	}
}

if(!function_exists('htmlentities_utf8')) {
	/**
	 * UTF-8 entity encoding (works in PHP 4 too)										
	 * 
	 * @param string $string The string we want to convert to HTML entities.
	 * @param int $quote_style Default = ENT_QUOTES
	 * @param string $charset Default = 'UTF-8'
	 * @return encoded The encoded string.
	 */
	function htmlentities_utf8($string,$quote_style = ENT_QUOTES,$charset = 'UTF-8') {
		if(PHP_MAJOR_VERSION <= 4) {
			//$string = utf8_encode($string);// Doesn't encode single quotes
			$string = htmlentities($string,$quote_style); // Doesn't accept multi-byte character sets (UTF-8) in 3rd param
		}
		else $string = htmlentities($string,$quote_style,$charset);
		return $string;
	}
}

if(!function_exists('html_entity_decode_utf8')) {
	/**
	 * UTF-8 entity decoding (works in PHP 4 too)										
	 * 
	 * @param string $string The string we want to decode.
	 * @param int $quote_style Default = ENT_QUOTES
	 * @param string $charset Default = 'UTF-8'
	 * @return string The decoded string.
	 */
	function html_entity_decode_utf8($string,$quote_style = ENT_QUOTES,$charset = 'UTF-8') {
		if(PHP_MAJOR_VERSION <= 4) {
			//$string = utf8_decode($string); // utf8_encode() doesn't encode single quotes
			$string = html_entity_decode($string,$quote_style); // Doesn't accept multi-byte character sets (UTF-8) in 3rd param
		}
		else $string = html_entity_decode($string,$quote_style,$charset);
		return $string;
	}

}

if(!function_exists('string_url_encode')){
	/**
	 * Encodes given string for use in a URL.
	 *
	 * @param string $string The string you want to URL encode. 
	 * @param boolean $dash Do you want to convert spaces to dashes? Default = 1
	 * @return string The URL encoded string. 										    
	 */
	function string_url_encode($string,$dash = 1){
		// URL Decode
		$string = rawurldecode($string);
		// Decode HTML entities
		$string = html_entity_decode_utf8($string);
		// Convert ampersands
		$string = str_replace('&','and',$string);
		// Convert special characters
		$string = string_convert_special_characters(s($string));
		// Windows Server
		if(PHP_OS == "WINNT") $string = preg_replace('/[^a-z0-9_\-]/i','',$string);
		// Trim
		$string = trim($string);
		// Convert spaces to dashes (and convert some other characters)
		if($dash == 1) {
			$search = array(' ','/','"',"'",'#','?','+');
			$replace = array('-','-','','','','','');
			$string = str_replace($search,$replace,$string);
			
			// Convert 2+ dashes to single dash
			$string = preg_replace('/\-+/','-',$string);
		}
		// URL Encode
		$string = rawurlencode($string);
		
		return $string;
	}
}

if(!function_exists('string_url_decode')) {
	/**
	 * Decode's a previously URL encoded string (sort of).							    
	 * 
	 * @param string $string The string you want to decode.
	 * @return string The URL decoded string.
	 */
	function string_url_decode($string) {
		$string = rawurldecode(s($string));
		return $string;
	}
}

if(!function_exists('string_convert_special_characters')){	
	/**
	 * Converts special characters into more browser friendly versions					
	 */
	function string_convert_special_characters($string,$entities = 0) {
		// Replace
		if($entities == 1) {
			$replace = array(
				'&lsquo;', // Left single quote
				'&rsquo;', // Right single quote
				'&ldquo;', // Left double quote
				'&rdquo;', // Right double quote
				'&ndash;', // Narrow dash
				'&mdash;', // Medium dash
				'&hellip;' // Ellipses
			);
		}
		else {
			$replace = array(
				"'", // Left single quote
				"'", // Right single quote
				'"', // Left double quote
				'"', // Right double quote
				'-', // Narrow dash
				'-', // Medium dash
				'...' // Ellipses
			); 
		}
		
		// UTF-8
		$search = array(
			chr(0xe2).chr(0x80).chr(0x98),
			chr(0xe2).chr(0x80).chr(0x99),
			chr(0xe2).chr(0x80).chr(0x9c),
			chr(0xe2).chr(0x80).chr(0x9d),
			chr(0xe2).chr(0x80).chr(0x93),
			chr(0xe2).chr(0x80).chr(0x94),
			chr(0xe2).chr(0x80).chr(0xa6)
		);
		$string = str_replace($search, $replace, $string);
		
		// ASCII
		$search = array(
			chr(145),
			chr(146),
			chr(147),
			chr(148),
			chr(150),
			chr(151),
			chr(133)
		); 
		$string = str_replace($search,$replace,$string);
	 
	 	// Return
		return $string;
	} 
}

if(!function_exists('purify')){	
	/**
	 * 'Purifies' submitted text, removing malicious code		
	 * 
	 * @param mixed $value The value you want to purify.
	 * @param array $c An array of congiruation values. Default = NULL
	 * @return mixed The purified value.
	 */
	function purify($value,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Value to purify
		if(x($value)) {
			// Config
			if(!x($c[html])) $c[html] = 0; // Strip HTML
			if(!x($c[html_keep])) $c[html_keep] = NULL; // A list of HTML tags to keep when stripping html (the 2nd param in strip_tags()). Example: "<p><b><i>".
			if(!x($c[flash])) $c[flash] = 0; // Strip flash
			if(!x($c[css])) $c[css] = 1; // Strip css styles
			if(!x($c[css_inline])) $c[css_inline] = 0; // Strip inline css styles. Example: style='font-weight:bold'
			if(!x($c[js])) $c[js] = 1; // Strip <script> tags and the javascript within
			if(!x($c[js_inline])) $c[js_inline] = $c[js]; // Strip inline javascript. Example: onclick="location.assign('http://otherwebsite.com');"
			if(!x($c[comments])) $c[comments] = 1; // Strip HTML comments
			if(!x($c[meta])) $c[meta] = 1; // Strip meta tags and CSS links
			if(!x($c[xml])) $c[xml] = 1; // Strip XML and the tags within
			if(!x($c[word])) $c[word] = 1; // Strip Microsoft Word specific tags, ex: <w:View>Normal</w:View>
			if(!x($c[characters])) $c[characters] = 1; // Clean up unusual characters such as smart quotes, ellipses, and dashes
			if(!x($c[xss])) $c[xss] = 1; // Prevent XSS attacks by calling htmlspecialchars()
			if(!x($c[trim])) $c[trim] = 0; // Trim whitespace from start/end of text
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// HTML Purifier // Too many issues
			/*require_once SERVER."core/htmlpurifier/HTMLPurifier.standalone.php";
			$config = HTMLPurifier_Config::createDefault();
			$config->set('Cache.SerializerPath', SERVER."core/cache/"); // Cache path
			$config->set('Attr.EnableID', true); // Allow 'id' attribute
			if($c[flash]) { // Allow flash
				$config->set('HTML.SafeObject', true);
				$config->set('HTML.SafeEmbed', true);
				$config->set('HTML.FlashAllowFullScreen', true);
			}
			if($c[js]) $config->set('HTML.Trusted', true); // Allow Javascript (by making this a 'trusted' user)
			//$config->set('Filter.Custom',  array( new HTMLPurifier_Filter_SafeIframe() )); // Allow Iframes // Doesn't ork
			//$config->set('HTML.Allowed', 'p,b,a[href],i'); // Allowed HTML
			$purifier = new HTMLPurifier($config);*/
			
			// Array
			if(is_array($value)) {
				foreach($value as $k => $v) {
					if($k === "sef") $result[$k] = $v; // Don't want to purify sef
					else $result[$k] = purify($v);
				}
			}
			// Value
			else {
				$result = $value;
				debug("value (start): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				
				// HTML purifier // Too many issues
				//$result = $purifier->purify($result);
				//debug("value (after html purify): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				
				// HTML
				if($c[html]) {
					$result = strip_tags($result,$c[html_keep]);
				}
		
				// CSS Styles
				if($c[css]) {
					$result = preg_replace('/<style(.*?)>(.*?)<\/style>/si','',$result); // Strip <style></style> tags and CSS withing
					debug("value (after style tags removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				// CSS styles - inline
				if($c[css_inline]) {
					$result = preg_replace('/style=[\'|"](.*?)[\'|"]/si','',$result);	
					debug("value (after intline styles removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Javascript // HTMLPurifier might already be doing this, but need to look into it more
				if($c[js]) {
					$result = preg_replace('/<script(.*?)>(.*?)<\/script>/si','',$result); // Strip <script> tags and JS within
					debug("value (after scripts removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				// Javascript - inline
				if($c[js_inline]) {
					$attributes = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavaible', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragdrop', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterupdate', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmoveout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
					$text = preg_replace("/(".implode('|', $attributes).")=('|\")(.*?)('|\") /si","",$text);	
					debug("value (after inline js removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Comments
				if($c[comments]) {
					$result = preg_replace('/<!--([^>]*?)>/si','',$result); // Strip HTML comments, ex: <!-- Here's a comment -->
					$result = preg_replace('/<!\[([^>]*?)-->/si','',$result); // Strip out 'endif' tags, <![endif]-->
					debug("value (after comments removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Meta Tags
				if($c[meta]) {
					$result = preg_replace('/<(meta|link)(.*?)>/si','',$result); // Strip header tags, ex: <meta content="text/html; charset=utf-8">
					debug("value (after meta/link removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// XML
				if($c[xml]) {
					$result = preg_replace('/<xml>(.*?)<\/xml>/si','',$result); // Strip XML tags and the content within
					debug("value (after xml removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Microsoft Word - also gets removed by the XML stripper above, don't need unless we're not stripping xml
				if($c[word] and !$c[xml]) {
					$result = preg_replace('/<(w|m):(.*?)<\/(.*?)>/','',$result); // Strip open/close tags, ex: <w:View>Normal</w:View>
					$result = preg_replace('/<(w|m):(.*?)\/>/','',$result); // Strip self closing tags, ex: <w:BreakWrappedTables/>
					$result = preg_replace('/<(w|m):(.*?)>/','',$result); // Strip remaining open tags (tags w/children), ex: <w:WordDocument>
					$result = preg_replace('/<\/(w|m):(.*?)>/','',$result); // Strip remaining open tags (tags w/children), ex: </w:LatentStyles>
					$result = preg_replace('/<(w|m):(.*?)>/s','',$result); // Strip multi-line tags
					debug("value (after word removed): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Clean characters
				if($c[characters]) {
					// Original list Justin came up with...maybe add these to the 
					/*$chars = array(
						130 => ',',		// baseline single quote
						131 => 'NLG',	// florin
						132 => '"', 	// baseline double quote
						133 => '...',	// ellipsis
						134 => '**',	// dagger (a second footnote)
						135 => '***',	// double dagger (a third footnote)
						136 => '^', 	// circumflex accent
						137 => 'o/oo',	// permile
						138 => 'Sh',	// S Hacek
						139 => '<',		// left single guillemet
						140 => 'OE',	// OE ligature
						145 => '\'',	// left single quote
						146 => '\'',	// right single quote
						147 => '"',		// left double quote
						148 => '"',		// right double quote
						149 => '-',		// bullet
						150 => '-',		// endash
						151 => '--',	// emdash
						152 => '~',		// tilde accent
						153 => '(TM)',	// trademark ligature
						154 => 'sh',	// s Hacek
						155 => '>',		// right single guillemet
						156 => 'oe',	// oe ligature
						159 => 'Y',		// Y Dieresis
						169 => '(C)',	// Copyright
						174 => '(R)'	// Registered Trademark
					);
					foreach($chars as $chr => $replace) {
						$result = str_replace(chr($chr), $replace, $result);
					}*/
					
					// My 'special characters' conversion
					//$result = string_convert_special_characters($result,1);
					
					// PHP's htmlentities conversion
					if(function_exists('mb_detect_encoding')) {
						$encoding = mb_detect_encoding($result);
						$result = html_entity_decode($result,ENT_QUOTES,$encoding);
						$result = htmlentities($result,ENT_QUOTES,$encoding);
					}
					// Encoding class conversion
					else if(class_exists('Encoding')) {
						$result = Encoding::toUTF8($result);
					}
					debug("value (after characters encoded): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// XSS
				if($c[xss]) {
					if(!$c[characters]) {
						$encoding = mb_detect_encoding($result);
						$result = htmlspecialchars_decode($result,ENT_QUOTES,$encoding);
						$result = htmlspecialchars($result,ENT_QUOTES,$encoding);
					}
					
					$search = array(
						"%3C", // Remove %3C (<) and replace with &lt;
						"%3E", // Remove %3E (>) and replace with &gt;
						"%27", // Remove %27 (') and replace with &#039;
						"%22", // Remove %22 (") and replace with &#x22;
						// htmlspecialchars() wil handle these
						/*"<", // Remove < and replace with &lt;
						">", // Remove > and replace with &gt;
						"'", // Remove ' and replace with &#039;
						'"', // Remove " and replace with &#x22;
						")", // Remove ) and replace with &#x29;
						"(" // Remove ( and replace with &#x28;*/
					);
					$replace = array(
						"&lt;", // Remove %3C (<) and replace with &lt;
						"&gt;", // Remove %3E (>) and replace with &gt;
						"&#039;", // Remove %27 (') and replace with &#039;
						"&#x22;", // Remove %22 (") and replace with &#x22;
						// htmlspecialchars() wil handle these
						/*"&lt;", // Remove < and replace with &lt;
						"&gt;", // Remove > and replace with &gt;
						"&#039;", // Remove ' and replace with &#039;
						"&#x22;", // Remove " and replace with &#x22;
						"&#x29;", // Remove ) and replace with &#x29;
						"&#x28;" // Remove ( and replace with &#x28;*/
					);
					$result = str_replace($search,$replace,$result);
					debug("value (after xss): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Trim
				if($c[trim]) {
					$result = trim($result);
					debug("value (after trim): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
				}
				
				// Debug
				debug("value (final): <xmp style='display:inline;'>".$result."</xmp>",$c[debug]);
			}
		}
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $result;
	}
}

if(!function_exists('unpurify')){	
	/**
	 * Basic 'unpurification' of a given value (string or array).
	 *
	 * Basically decodes encoded special characters.
	 * 
	 * @param mixed $value The value you want to unpurify.
	 * @return mixed The unpurified value.
	 */
	function unpurify($value) {
		if(x($value)) {
			// Array
			if(is_array($value)) {
				foreach($value as $k => $v) {
					$result[$k] = unpurify($v);
				}
			}
			// Value
			else {
				$result = html_entity_decode_utf8($value);
			}
			
			return $result;
		}
	}
}

/************************************************************************************/
/********************************* Serialization ************************************/
/************************************************************************************/
if(!function_exists('data_serialize')) {
	/**
	 * Converts array into a serialized string											
	 * 
	 * @param array $array The array you want to convert to a serialized string.
	 * @param array $c An array of congiruation values. Default = NULL
	 * @return string The resulting serialized string.
	 */
	function data_serialize($array,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		if(count($array) > 0) {
			// Escape quotes - Could be an alternative to single quote escaping below, think it might be slower/heavier to run though
			//$array = a($array); 
			
			// JSON encode
			$string = json_encode($array);
			
			// Escape single quotes so we can store in db
			$string = preg_replace("/([^\\\])'/","$1\'",$string); // Escape single quotes so we can store in db
			
			// PHP5 version to force object ({"key":"value"} instead of [key:value]) and encode (not escape) single quotes
			//$string = json_encode($array,JSON_HEX_APOS | JSON_FORCE_OBJECT); // PHP 5.3+
			
			// Add slashes to string
			$string = addslashes($string);
		
			// Speed
			function_speed(__FUNCTION__,$f_r);
			
			// Return
			return $string;
		}
	}
}

if(!function_exists('data_unserialize')) {
	/**
	 * Converts serialized string to an array.		
	 * 
	 * @param string $string The previously serialized string we want to unserialize.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The original array of data
	 */
	function data_unserialize($string,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Config
		if(!$c[merge]) $c[merge] = NULL; // Array we want to merge results with (need for the explode_value() cal;)
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Debug
		debug("unserialize string 1: <xmp>".$string."</xmp>",$c[debug]);
	
		if(x($string)) {
			//debug("unserialize string 1.1: <xmp>".$string."</xmp>",$c[debug]);
			// Stripslashes
			$stripslashes = 0;
			if(substr($string,0,3) == '{\"') $stripslashes = 1;
			else if(substr($string,0,3) == '[\"') $stripslashes = 1;
			else if(substr($string,0,4) == '[{\"') $stripslashes = 1;
			if($stripslashes == 1) $string = stripslashes($string); 
			//debug("unserialize string 1.2: <xmp>".$string."</xmp>",$c[debug]);
			
			// Unescape escaped single quotes (json will error out on them for some reason)
			$string = preg_replace("/(\\\)?(\\\)?\\\'/","'",$string); // Up to 3 levels of escape (ex: john\\\'s)
			//debug("unserialize string 1.3: <xmp>".$string."</xmp>",$c[debug]);
			
			// JSON Decode
			if(!$results = json_decode($string,true)) {
				//debug("json error: ".json_last_error(),$c[debug]); // Need PHP 5.3+ to do
			}
				
			// Debug
			if(debug_active() and $c[debug] == 1) {
				if(is_array($results)) debug("unserialize string 2: ".return_array($results),$c[debug]);
				else debug("unserialize string 2: ".$results,$c[debug]);
			}
			
			if($results) {
				// Stripslashes from array // Should be unescaping on our own end, but do here just in case
				$results = s($results);
				
				// Debug
				if(debug_active() and $c[debug] == 1) {
					if(is_array($results)) debug("unserialize string 3:",$results,$c[debug]);
					else debug("unserialize string 3: ".$results,$c[debug]);
				}
				
				// Merge
				if($c[merge]) $results = array_merge($c[merge],$results);
				
				// Debug
				if(debug_active() and $c[debug] == 1) {
					if(is_array($results)) debug("unserialize string 4: ".return_array($results),$c[debug]);
					else debug("unserialize string 4: ".$results,$c[debug]);
				}
			}
		}
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $results;
	}
}

if(!function_exists('json_encode')) {
	/**
	 * PHP 4 version of the json_encode function.
	 *
	 * Used the JSON class http://pear.php.net/pepr/pepr-proposal-show.php?id=198		
	 * 
	 * @param mixed $value The value you want to turn into a JSON encoded string
	 * @return string The JSON encoded string
	 */
	function json_encode($value) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Class
		require_once SERVER.'core/core/libraries/json.php';
		$json = new Services_JSON;
		
		// Result
		$result = $json->encode($value);
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $result;
	}
}

if(!function_exists('json_encode_javascript')) {
	/**
	 * Converts an array to a JSON string AND keeps javascript functions as functions (no quotes around them).
	 * 
	 * @param mixed $value The value you want to turn into a JSON encoded string.
	 * @param array $functions An array of javascript functions this function has already discovered in the original 'value' (only really used inside recursive loop).
	 * @param int $level The level we're at in the original 'value' (aka array).
	 * @return string The JSON encoded string.
	 */
	function json_encode_javascript($value,$functions,$level = 0) {
		foreach($value as $k => $v) {
			if(is_array($v)) {
				$ret = json_encode_javascript($v,$functions,1);
				$value[$k] = $ret[0];
				$functions = $ret[1];
			}
			else {
				if(substr($v,0,9) == 'function(') {
					$func_key = "#".uniqid()."#";
					$functions[$func_key] = $v;
					$value[$k] = $func_key;
				}
			}
		}
		if($level == 1) {
			return array($value,$functions);
		}
		else {
			$value_json = json_encode($value);
			foreach($functions as $k => $v) {
				$value_json = str_replace('"'.$k.'"',$v,$value_json);
			}
			return $value_json;
		}
	}
}

if(!function_exists('json_decode')) {
	/**
	 * PHP 4 version of the json_decode function.
	 *
	 * Used the JSON class http://pear.php.net/pepr/pepr-proposal-show.php?id=198			
	 * 
	 * @param string $json The JSON encoded string you want to decode.
	 * @return mixed The decoded value						
	 */
	function json_decode($json,$assoc = false) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Class
		require_once SERVER.'core/core/libraries/json.php';
		if($assoc) $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		else $json = new Services_JSON;
		
		// Result
		$result = $json->decode($json);
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $result;
	}
}

/************************************************************************************/
/************************************* Files ****************************************/
/************************************************************************************/
if(!function_exists('file_put_contents')) {
	/**
	 * Basic PHP 4 version of file_put_contents	(though doesn't return # of bytes)		
	 * 
	 * @param string $file The path of the file you want to save to.
	 * @param string $content The content you want to save to the file.
	 */
	function file_put_contents($file,$content) {
		$f = fopen($file, 'w');
		fwrite($f,$content);
		fclose($f);
	}
}

if(!function_exists('files_array')) {
	/**
	 * Converts normal $_FILES array format to more standard array format (images=>3=>tmp_name instead of images=>tmp_name=>3).
	 * 
	 * http://www.php.net/manual/en/reserved.variables.files.php#106608
	 *
	 * @param arran $files The files array you want to convert.
	 * @return array The array of file data in the mor standard format.
	 */
	function files_array($files, $top = true) {
		$files_new = array();
		foreach($files as $name => $file) {
			if($top) $sub_name = $file['name'];
			else $sub_name = $name;
		   
			if(is_array($sub_name)) {
				foreach(array_keys($sub_name) as $key) {
					$files_new[$name][$key] = array(
						'name' => $file['name'][$key],
						'type' => $file['type'][$key],
						'tmp_name' => $file['tmp_name'][$key],
						'error' => $file['error'][$key],
						'size' => $file['size'][$key],
					);
					$files_new[$name] = files_array($files_new[$name], fakse);
				}
			}
			else {
				$files_new[$name] = $file;
			}
		}
		return $files_new;
	}
}

if(!function_exists('files_exists')) {
	/**
	 * Runs file_exists() on an array of files.		
	 * 
	 * @param arran $files An array of files.
	 * @return array The array of files with all non-existant files removed.
	 */
	function files_exists($files) {
		$files_exist = NULL;
		foreach($files as $file) {
			if(file_exists($file)) $files_exist[] = $file;	
		}
		return $files_exist;
	}
}

/*if(!function_exists('file_permission')) {
	/**
	 * Returns the permission of the given file/directory (example: 0777) or, if the $permission param is passed, it sets the permission of the file/directory.	
	 *
	 * !!! Moved to file class. !!!
	 *
	 * A bit slow (.15 sec) when using chmod_ftp. // Not currently implemented			
	 * 
	 * @param string $file The file/directory we want to get (and set if 2nd param) the permission of.
	 * @param int|string $permission The permission you wan to apply to the file/directory. Default = NULL
	 * @return int The resulting permission for the file/directory.
	 */
	/*function file_permission($file,$permission = NULL) {
		// Save permission
		if($permission) {
			$permission_current = substr(sprintf('%o', fileperms($file)), -4);
			if(substr($permission,0,1) != 0) $permission = "0".$permission; // Make sure it's 4 digits (0777 instead of 777)
			
			// Try chmod()
			if($permission_current != $permission) {
				chmod($file,octdec($permission));
				$permission_current = file_permission($file);
			}
			// Try shell_exec("chmod")
			if($permission_current != $permission) {
				shell_exec("chmod ".substr($permission,1,3)." '".$file."'");
				$permission_current = file_permission($file);
			}
			// Change owner, chmod // Don't know if 'apache' is right name, rarely works
			/*if($permission_current != $permission) {
				shell_exec("chown apache ".$file);
				shell_exec("chmod ".substr($permission,1,3)." ".$file);
				//$writeable = is_writable($file);
				$permission_current = file_permission($file);
			}*/
			// Try chmod_ftp() // Not currently implemented
			/*if($permission_current != $permission) {
				chmod_ftp($file,$permission);
				$permission_current = file_permission($file);
			}*/
		/*}
		
		// Return permission
		$permission = substr(sprintf('%o', fileperms($file)), -4);
		return $permission;
	}
}

if(!function_exists('file_writeable')) {
	/**
	 * Determines if (and tries to make) given file/directory writeable.
	 *
	 * !!! Moved to file class. !!!
	 * 
	 * @param string $file Path the the file/directry you to determine if (and try to make) writeable.
	 * @return boolean Whether or not the file/directory is writeable.
	 */
	/*function file_writeable($file) {
		// Get/set permission
		$permission = file_permission($file,"0777");
		
		// Writeable // $permission gets cached and doesn't always show correct permission (even if we did actually make it 0777) so we'll check is_writable too
		$writeable = is_writable($file);
		
		// Return
		if($writeable or $permission == "0777") return true; 
	}
}

if(!function_exists('directory_items')) {
	/**
	 * Returns an array of items (files and directories) within the given directory.
	 *
	 * !!! Moved to file class. !!!
	 *
	 * @param string $directory The directory you want to look for items in.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of files in the directory.
	 */
	/*function directory_items($directory,$c = NULL) {	
		// Config
		if(!x($c[files])) $c[files] = 1; // Get files in the root directory.
		if(!x($c[directories])) $c[directories] = 1; // Get directories in the root directory.
		if(!x($c[recursive])) $c[recursive] = 0; // Get items from sub-directories as well as the items in the root directory.
		if(!x($c[multilevel])) $c[multilevel] = 1; // Return a 'multi-level' array when we're searching recursively. If true, child folders will be the key and their contents the value, ex: array('folder' => array('child' => array('file.txt'))). If no, it'll be a single level array such as array('folder','folder/child','folder/child/file.txt').
		if(!x($c[prefix])) $c[prefix] = NULL; // A prefix to append to each item path (used when we're looking through sub-directories in 'recursive' mode when multilevel is turned off).
		if(!$c[paths]) $c[paths] = 'relative'; // Do you want the file names returned to be 'relative' to the directory or 'full' (meaning, include the parent directory's full path)?
			
		// Variables
		$array = array();
		if($directory and substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
		if($c[paths] == "full") $c[prefix] = $directory.$c[prefix];
			
		// Open directory
		if($handle = opendir($directory)) {
			// Get entries
			while(false !== ($entry = readdir($handle))) {
				// Skip navigation entries (up one level, etc.)
				if(!in_array($entry,array(".",".."))) {
					// File
					if($c[files]) {
						// Entry is a file (and not a system file), add to array
						if(is_file($directory.$entry) and substr($entry,0,1) != ".") {
							$array[] = $c[prefix].$entry;
						}
					}
					// Directory
					if($c[directories] or $c[recursive]) {
						// Entry is a directory, add to array
						if(is_dir($directory.$entry)) {
							// Look for items inside (recursive)
							$sub_array = NULL;
							if($c[recursive] == 1) {
								$_c = $c;
								if(!$c[multilevel]) $_c[prefix] .= $entry."/";
								$sub_array = directory_items($directory.$entry."/",$_c);
							}
							
							// Add to array
							if($c[recursive] and $c[multilevel]) $array[$c[prefix].$entry] = ($sub_array ? $sub_array : array());
							else {
								if($c[directories]) $array[] = $c[prefix].$entry;
								if($sub_array) $array = array_merge($array,$sub_array);
							}
						}
					}
				}
			}
		}
		
		// Return
		return $array;
	}
}

if(!function_exists('directory_files')) {
	/**
	 * Returns an array of files within the given directory.
	 *
	 * !!! Moved to file class. !!!
	 *
	 * @param string $directory The directory you want to look for files in.
	 * @param array $c An array of configuration values (see directory_items() for configuration options). Default = NULL
	 * @return array An array of files in the directory.
	 */
	/*function directory_files($directory,$c = NULL) {
		$c[files] = 1;
		$c[directories] = 0;
		return directory_items($directory,$c);
	}
}

if(!function_exists('directory_folders')) {
	/**
	 * Returns an array of folders within the given directory.
	 *
	 * !!! Moved to file class. !!!
	 *
	 * @param string $directory The directory you want to look for folders in.
	 * @param array $c An array of configuration values (see directory_items() for configuration options). Default = NULL
	 * @return array An array of folders in the directory.
	 */
	/*function directory_folders($directory,$c = NULL) {
		$c[files] = 0;
		$c[directories] = 1;
		return directory_items($directory,$c);
	}
}

if(!function_exists('directory_writeable')) {
	/**
	 * Checks to see if a directory is writeable (and attempts to make it so if it isn't already).	
	 *
	 * !!! Moved to file class. !!!		
	 * 
	 * @param string $directory Path to the diretory you want to check the writeability of.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	/*function directory_writeable($directory,$c = NULL) {
		// Config
		if(!x($c[recursive])) $c[recursive] = 0; // Check (and apply) writeable permission recursively on all sub-directories
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Variables
		if(substr($directory,-1) != "/") $directory .= "/"; // Make sure we have a trailing slash
		
		// Writeable
		$return = file_writeable($directory);
		
		// Sub-directories
		if($c[recursive] == 1 and $return) {
			if($folders = directory_folders($directory)) {
				foreach($folders as $folder) {
					$writeable = directory_writeable($directory.$folder."/",$c);
					if(!$writeable) $return = 0;
				}
			}	
		}
		
		// Return
		return $return;
	}
}*/

if(!function_exists('directory_delete')) {
	/**
	 * Deletes a directory's content and (optionally) the directory itself.
	 *
	 * If you can, use the file class method of this (ex: file::directory_delete($path)).
	 * Just keeping this for use with 'cache' functions so we can keep them fast. 
	 * 
	 * http://www.roscripts.com/snippets/show/170
	 * 
	 * @param string $directory The directory whose contents (and possibly self) we want to delete
	 * @param boolean $delete_directory Whether or not we want to delete the directory too (if not, we'll just delete the contents within it)
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function directory_delete($directory,$delete_directory = 1,$c = NULL) {
		// Config
		//if(!$c[life]) $c[life] = NULL; // Optional life (in seconds) a file is considered 'fresh'. Only delete if file is older than this life (aka, is 'expired'). // Commented out for speed
		if(!$c[skip]) $c[skip] = array(); // An array of directories or files to 'skip' when deleting.

		// Directory
		if(substr($directory,-1) != "/") $directory .= "/";
		
		if(!$dh = @opendir($directory)) return;
		while(false !== ($obj = readdir($dh))) {
			// Not a real file, continue
			if($obj == '.' or $obj == '..') continue;
			//debug("file: ".$obj);
			
			// Skip?
			if($c[skip] and in_array($obj,$c[skip])) {
				//debug("skipping ".$directory.$obj);
				continue;
			}
			
			// Delete file, if can't it's probably a directory, delete it // Simple, quicker, but invalid and throws lots of PHP errors
			//if(!@unlink($directory.$obj)) directory_delete($directory.$obj,1);
			
			// Is a directory, delete contents, then delete it
			if(is_dir($directory.$obj)) directory_delete($directory.$obj,1,$c);
			// Is a file, delete
			else {
				// Want to check to see if it's expired first
				if($c[life]) {
					// Date file was created
					$created = filemtime($directory.$obj);
					//debug("Created: ".date('Y-m-d H:i:s',$created));
					
					// File isn't expired yet, don't delete
					if((time() - $created) < $c[life]) {
						//debug("Still fresh, not deleting",$created);
						continue;
					}
					
				}
				
				// Delete
				unlink($directory.$obj);
			}
		}
		
		closedir($dh);
		if($delete_directory) @rmdir($directory);
	}
}

/*if(!function_exists('copy_directory')) {
	/**
	 * Copies an entire directory.
	 *
	 * !!! Moved to file class. !!!
	 * 
	 * http://codestips.com/php-copy-directory-from-source-to-destination/
	 * 
	 * @param string $source The directory you want to copy.
	 * @param string $destination The path you want to copy the directory to.
	 */
	/*function copy_directory( $source, $destination ) {
		if(is_dir($source)) {
			@mkdir($destination);
			$directory = dir($source);
			while(FALSE !== ($readdirectory = $directory->read())) {
				if($readdirectory == '.' || $readdirectory == '..' ) {
					continue;
				}
				$PathDir = $source.'/'.$readdirectory; 
				if(is_dir($PathDir)) {
					copy_directory($PathDir,$destination.'/'.$readdirectory);
					continue;
				}
				copy($PathDir,$destination.'/'.$readdirectory);
			}
			$directory->close();
		}
		else {
			copy($source,$destination);
		}
	}
}*/

if(!function_exists('mkdir_recursive')) {
	/**
	 * Creates a directory and all missing parent directories, applying 0777 permissions
	 *
	 * If you can, use the file class method of this (ex: file::directory_create($path,1)).
	 * Just keeping this for use with 'cache' functions so we can keep them fast. 
	 *
	 * @param string $directory The directory you want to create
	 */
	function mkdir_recursive($directory) {	
		if(PHP_MAJOR_VERSION >= 5) mkdir($directory,0777,true);
		else { // No 3rd param (recursive), need to do ourself
			$folder_string = NULL;
			$folders = explode('/',$directory);
			foreach($folders as $folder) {
				$folder_string .= $folder."/";
				if(!file_exists($folder_string)) {
					mkdir($folder_string,0777);
				}
			}
		}
	}
}

if(!function_exists('file_process')) {
	/**
	 * Process file, converting, thumbing, etc. and returns an array of data about the file.
	 *
	 * @param string $file The full path to the file (including the file name).
	 * @param string $type The file type this file should be: file, image, video, audio. Default = file
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of data about this file, 
	 */
	function file_process($file,$type = "file",$c = NULL) {
		// Error
		if(!$file) return;
		
		// Config
		if(!x($c[extensions])) $c[extensions] = g('config.uploads.types.'.$type.'.extensions'); // Array of extensions to handle. Set to 0 if we don't want to handle extensions.
		if(!x($c[extensions_action])) $c[extensions_action] = "create"; // How to handle extensions: get (just get them if they exist), create (create them if they don't exist), recreate (create them, even if they do exist)
		if(!x($c[thumbs])) $c[thumbs] = g('config.uploads.types.'.$type.'.thumbs'); // Array of thumbs to handle. Set to 0 if we don't want to handle thumbs.
		if(!x($c[thumbs_action])) $c[thumbs_action] = "create"; // How to handle thumbs: get (just get them if they exist), create (create them if they don't exist), recreate (create them, even if they do exist)
		if(!x($c[storage])) $c[storage] = g('config.uploads.types.'.$type.'.storage'); // Where to store the file.
		
		// File
		$file_object = file::load($file);
		if($file_object->exists) {
			// Modified
			$file_modified = filemtime($file_object->file);
			
			// Values - basic
			$array = array(
				'name' => $file_object->name,
				'type' => $file_object->type,
				'path' => str_replace(array(SERVER,$file_object->name),'',$file_object->file),
				'extension' => $file_object->extension,
				'width' => $file_object->width(),
				'height' => $file_object->height(),
				'length' => $file_object->length(),
				'size' => $file_object->size(),
			);
						
			// Quickstart
			if($file_object->type == "video" and $file_object->extension_standardized == "mp4") {
				$file_object->video_faststart(array('debug' => $c[debug]));
			}
			
			// Values - extensions
			if($c[extensions]) {
				// Unstore, we'll recreate in after conversion
				$array[extensions] = NULL;
				
				// Debug
				debug("Converting file to other extensions:".return_array($c[extensions]),$c[debug]);
				
				// File
				$f = new file($c[path].$value,array('debug' => $c[debug]));
				
				// Extensions - video
				if($file_object->type == "video") {
					// Extensions
					foreach($c[extensions] as $k => $v) {
						// Thumb
						$extension_object = new file($v[path].$file_object->name);
						
						// Create - not created, parent file recently created (so if extension exists, it's probably old), or recreate
						if((!$extension_object->exists or $file_modified > (time() - 60)) and $c[extensions_action] == "create" or $c[extensions_action] == "recreate") {
							$extension_object = new file($file);
							$extension_object->video_convert($v[extension],array('path' => $v[path],'mobile' => $v[mobile],'debug' => $c[debug]));
						}
						
						// Values
						if($extension_object->exists) {
							$v[name] = $extension_object->name;
							$v[extension] = $extension_object->extension;
							$v[width] = $extension_object->width();
							$v[height] = $extension_object->height();
							$v[length] = $extension_object->length();
							$v[size] = $extension_object->size();
							$array[extensions][$k] = $v;
						}
					}
				}
			}
			
			// Values - thumbs
			if($c[thumbs]) {
				// Image
				if($type == "image") {
					foreach($c[thumbs] as $thumb => $v) {
						// Thumb
						$thumb_object = new file($v[path].$file_object->name);
						
						// Create - not created, parent file recently created (so if thumb exists, it's probably old), or recreate
						if((!$thumb_object->exists or $file_modified > (time() - 60)) and $c[thumbs_action] == "create" or $c[thumbs_action] == "recreate") {
							//$v[debug] = $c[debug];
							$thumb_object = new file($file);
							$thumb_object->image_thumb($v[width],$v[height],$v);
							if($thumb_object_saved = $thumb_object->save($v[path])) {
								$thumb_object = $thumb_object_saved;
							}
						}
						
						// Values
						if($thumb_object->exists) {
							$v[name] = $thumb_object->name;
							$v[extension] = $thumb_object->extension;
							$v[size] = $thumb_object->size();
							if(!$v[crop] or $v[enlarge]) { // If cropping and not enarging, we already know the size
								$v[width] = $thumb_object->width();
								$v[height] = $thumb_object->height();
							}
							$array[thumbs][$thumb] = $v;
						}
						
					}
				}
				
				// Video
				if($type == "video") {
					// Count/extension of screenshots (these should be the same across all thumb sizes, but check just to be sure)
					$count = 4;
					$extension = "jpg";
					foreach($thumbs as $thumb => $v) {
						if($v[count] and $v[count] > $count) $count = $v[count];
						if($v[extension]) $extension = $v[extension]; // All using one for now
					}
					
					// Get video screenshot(s) (save to temp folder for now)
					$temp = $file_object->temp();
					$thumbs_files = $file_object->video_thumb($temp,array('number' => $count,'extension' => $extension,'debug' => $c[debug]));
					debug("video thumbs: ".return_array($thumbs_files),$c[debug]);
					
					// Resize screenshots
					if($thumbs_files) {
						foreach($c[thumbs] as $thumb => $v) {
							foreach($thumbs_files as $x => $thumb_file) {
								if($x <= $v[count]) {
									// Thumb
									$thumb_object = new file($v[path].$thumb_file);
						
									// Create - not created, parent file recently created (so if thumb exists, it's probably old), or recreate
									if((!$thumb_object->exists or $file_modified > (time() - 60)) and $c[thumbs_action] == "create" or $c[thumbs_action] == "recreate") {
										//$v[debug] = $c[debug];
										$thumb_object = new file($temp.$thumb_file);
										$thumb_object->image_thumb($v[width],$v[height],$v);
										if($thumb_object_saved = $thumb_object->save($v[path])) {
											$thumb_object = $thumb_object_saved;
										}
									}
						
									// Values
									if($thumb_object->exists) {
										$v[name] = $thumb_object->name;
										$v[extension] = $thumb_object->extension;
										$v[size] = $thumb_object->size();
										//$v[width] = $thumb_object->width(); // Already set
										//$v[height] = $thumb_object->height(); // Already set
										$array[thumbs][$thumb][$x] = $v;
									}
								}
							}
						}
						
						// Delete temp video screenshot(s)
						foreach($thumbs_files as $x => $thumb_file) {
							debug("Deleting temp thumb: ".$temp.$thumb_file,$c[debug]);
							$thumb_object = new file($temp.$thumb_file);
							$thumb_object->delete();
						}
					}
				}
			}
				
			// Storage - file
			$storage = $c[storage];
			debug("file, storage: ".$storage,$c[debug]);
			if($storage and $storage != "local") {
				$v = $array;
			
				// Push
				debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$c[debug]);
				$file = file::load(NULL,array('storage' => $storage,'debug' => $c[debug]));
				if($file_pushed = $file->push($v[path].$v[name],$v[path],array('values' => $values))) {
					// Update values
					$array[name] = $value = $file_pushed->name;
					$array[path] = $file_pushed->path;
					$array[extension] = $file_pushed->extension;
					$array[storage] = $file_pushed->storage;
				
					// Delete original
					debug("deleting original file: ".$v[path].$v[name],$c[debug]);
					$file = file::load($v[path].$v[name]);
					$file->delete(array('debug' => $c[debug]));
				}
				else {
					$array[storage] = "local";
				}
			}
			
			// Storage - extensions
			if($array[extensions]) {
				foreach($array[extensions] as $k => $v) {
					// Storage
					$storage = $c[extensions][$k][storage];
					if(!$storage) $storage = $c[storage];
					debug("extension: ".$k.", storage: ".$storage,$c[debug]);
					if($storage and $storage != "local") {
						// Push
						debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$c[debug]);
						$extension_object = file::load(NULL,array('storage' => $storage,'debug' => $c[debug]));
						if($extension_object_pushed = $extension_object->push($v[path].$v[name],$v[path],array('values' => $values))) {
							// Update values
							$array[extensions][$k][name] = $extension_object_pushed->name;
							$array[extensions][$k][path] = $extension_object_pushed->path;
							$array[extensions][$k][storage] = $extension_object_pushed->storage;
						
							// Delete original
							debug("Deleting original file: ".$v[path].$v[name],$c[debug]);
							$extension_object = file::load($v[path].$v[name]);
							$extension_object->delete(array('debug' => $c[debug]));
						}
						else {
							$array[extensions][$k][storage] = "local";
						}
					}
				}
			}
			
			// Storage - thumbs
			if($array[thumbs]) {
				foreach($array[thumbs] as $k => $v) {
					// Storage
					$storage = $c[thumbs][$k][storage];
					if(!$storage) $storage = $c[storage];
					debug("thumb: ".$k.", storage: ".$storage,$c[debug]);
					if($storage and $storage != "local") {
						// Video - mulitple thumbs
						if($type == "video") {
							foreach($v as $k0 => $v0) {
								// Push
								debug("Pushing ".$v0[path].$v0[name]." (local) to ".$v[path]." (".$storage.")",$c[debug]);
								$thumb_object = file::load(NULL,array('storage' => $storage,'debug' => $c[debug]));
								if($thumb_object_pushed = $thumb_object->push($v0[path].$v0[name],$v0[path],array('values' => $values))) {
									// Update values
									$array[thumbs][$k][$k0][name] = $thumb_object_pushed->name;
									$array[thumbs][$k][$k0][path] = $thumb_object_pushed->path;
									$array[thumbs][$k][$k0][storage] = $thumb_object_pushed->storage;
								
									// Delete original
									debug("deleting original file: ".$v0[path].$v0[name],$c[debug]);
									$thumb_object = file::load($v0[path].$v0[name]);
									$thumb_object->delete(array('debug' => $c[debug]));
								}
								else {
									$array[thumbs][$k][$k0][storage] = "local";
								}
							}
						}
						// Default - single thumb
						else {
							// Push
							debug("Pushing ".$v[path].$v[name]." (local) to ".$v[path]." (".$storage.")",$c[debug]);
							$thumb_object = file::load(NULL,array('storage' => $storage,'debug' => $c[debug]));
							if($thumb_object_pushed = $thumb_object->push($v[path].$v[name],$v[path],array('values' => $values))) {
								// Update values
								$array[thumbs][$k][name] = $thumb_object_pushed->name;
								$array[thumbs][$k][path] = $thumb_object_pushed->path;
								$array[thumbs][$k][storage] = $thumb_object_pushed->storage;
								
								// Delete original
								debug("deleting original file: ".$v[path].$v[name],$c[debug]);
								$thumb_object = file::load($v[path].$v[name]);
								$thumb_object->delete(array('debug' => $c[debug]));
							}
							else {
								$array[thumbs][$k][storage] = "local";
							}
						}
					}
				}
			}
		}
		
		// Return
		return $array;
	}	
}

/************************************************************************************/
/********************************** Javascript **************************************/
/************************************************************************************/
if(!function_exists('minify_javascript')) {
	/**
	 * Minifies/combines array of js files and returns new file name					
	 * 
	 * @param array|string $files Either a single file or an array of files we want to minify.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The URL of the minified file.
	 */
	function minify_javascript($files,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Config
		if(!$c[path]) $c[path] = CACHE_PATH."javascript/"; // Path to save new file to
		//if(!$c['class']) $c['class'] = (PHP_MAJOR_VERSION <= 4 ? "jsmin" : "php-closure"); // Class to use: php-closure [default], jsmin
		if(!$c['class']) $c['class'] = "jsmin"; // Class to use: php-closure, jsmin // PHP Closure takes too long and has rate limits which we always seem to hit. Also can't handle remote URLs (might be able to make it by using other methods or something, haven't tried though) and struggles with new validation js.
		if(!x($c[expires])) $c[expires] = 0; // Number of days before minified file 'expires' (0 means it never expires).
		if(!x($c[clean])) $c[clean] = 1; // Clean up folder by removing old minified files (must have a 'prefix' we can match against).
		if(!x($c[debug])) $c[debug] = 0; // Debug
		if(!is_array($files)) $files = array($files);
		
		// Debug
		debug("<b>minify_js()</b>",$c[debug]);
		debug("files:".return_array($files),$c[debug]);
		debug("c:".return_array($c),$c[debug]);
			
		// Files exist / name
		$name = NULL;
		$name_time = NULL;
		$files_exist = NULL;
		foreach($files as $x => $file) {
			if($file) {
				if(file::exists($file)) {
					$time = filemtime($file);
					$name .= "|".$file;
					$name_time .= "|".$file.":".$time;
					$files_exist[] = $file;
					debug("file: ".$file.", time: ".$time.", name: ".$name,$c[debug]);
				}
			}
		}
		if($files_exist) {
			// Name / path
			$name_md5 = md5($name);
			$name_time_md5 = md5($name_time);
			$name = $name_md5."-".$name_time_md5.".js";
			$file_minified = $c[path].$name;
			$file_minified_time = filemtime($file_minified);
			$url_minified = str_replace(SERVER,D,$file_minified);
			
			// Debug
			debug("file_minified: ".$file_minified.", time: ".date('Y-m-d H:i:s',$file_minified_time)." (".$file_minified_time.")",$c[debug]);
			
			// Exists (and isn't expired)
			if($file_minified_time and (!$c[expires] or $file_minified_time > (time() - (86400 * $c[expires])))) {
				// Yes, don't need to re-minify
				debug("already minified, returning that file",$c[debug]);
				return $url_minified;
			}
			
			// Class
			if($c['class'] == "php-closure" and !fsockopen("closure-compiler.appspot.com", 80)) $c['class'] = "jsmin";
			require_once SERVER."core/core/libraries/".$c['class'].($c['class'] == "jsmin" && PHP_MAJOR_VERSION <= 4 ? ".4" : "").".php";
			if($c['class'] == "php-closure") $phpClosure = new PhpClosure();
			
			// Directory
			if(!file_exists($c[path])) file::directory_create($c[path]);
			
			// Loop through files
			foreach($files_exist as $x => $file) {
				// Debug
				debug("file = ".$file,$c[debug]);
				
				// Content
				if($c['class'] == "php-closure") $phpClosure->add($file);
				else $minify .= file_get_contents($file);
			}
			
			// Minify
			if($c['class'] == "php-closure") {
				$phpClosure->simpleMode(); // Simple minification
				//$phpClosure->advancedMode(); // Advanced minification (doesn't work with jQuery)
				//$phpClosure->quiet(); // Only basic errors
				if(!$c[debug] or !debug_active()) $phpClosure->hideDebugInfo(); // Hide debug (unless we're actually debugging)
				$minified = $phpClosure->_compile(); // Compile
				
				// Failed, likely because we've done too many recently. Use 'jsmin' instead.
				if(!$minified or !strstr($minified.'function')) {
					debug("php-closure failed, using jsmin instead",$c[debug]);
					$c['class'] = "jsmin";
					require_once SERVER."core/core/libraries/".$c['class'].($c['class'] == "jsmin" && PHP_MAJOR_VERSION <= 4 ? ".4" : "").".php";
					foreach($files_exist as $x => $file) {
						$minify .= file_get_contents($file);
					}
				}
			}
			if($c['class'] == "jsmin" and $minify) {
				$minified = JSMin::minify($minify);
			}
			debug("minified (first 100 characters): ".substr($minified,0,100),$c[debug]);
			
			// Delete old minified files
			if($c[clean] and $name_md5) {
				if($handle = opendir($c[path])) {
					while(false !== ($f = readdir($handle))) {
						if(is_file($c[path].$f) and substr($f,0,strlen($name_md5)) == $name_md5 and substr($f,-2) == "js" and $f != $name) {
							debug("deleting ".$c[path].$f,$c[debug]);
							unlink($c[path].$f);
						}
					}
				}
				closedir($handle);
			}
			
			// Save minified file
			if($minified) {
				file_put_contents($file_minified,$minified);
			}
		}
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $url_minified;
			
	}
}

/************************************************************************************/
/************************************* CSS ******************************************/
/************************************************************************************/
if(!function_exists('minify_css')) {
	/**
	 * Minifies/combines array of css files and returns new file name					
	 * 
	 * @param array|string $files Either a single file or an array of files we want to minify.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The URL of the minified file.
	 */
	function minify_css($files,$c = NULL) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Config
		if(!x($c[minify])) $c[minify] = g('config.minify.css.minify'); // Do we actually want to 'minify' code or just collect it (if you're using SASS and compile with minification, it'll be a lot faster to not minify here (as it's already minified)
		if(!$c[path]) $c[path] = CACHE_PATH."css/"; // Path to save new file to
		if(!$c['class']) $c['class'] = (PHP_MAJOR_VERSION <= 4 ? "cssmin.4" : "cssmin.3.0.1"); // Class to use: cssmin, cssmin.3.0.1
		if(!x($c[expires])) $c[expires] = 0; // Number of days before minified file 'expires' (0 means it never expires).
		if(!x($c[clean])) $c[clean] = 1; // Clean up folder by removing old minified files (must have a 'prefix' we can match against).
		if(!x($c[debug])) $c[debug] = 0; // Debug
		if(!is_array($files)) $files = array($files);
		
		// Debug
		debug("<b>minify_css()</b>",$c[debug]);
		debug("files:".return_array($files),$c[debug]);
		debug("c:".return_array($c),$c[debug]);
			
		// Files exist / name
		$name = NULL;
		$name_time = NULL;
		$files_exist = NULL;
		foreach($files as $x => $file) {
			if($file) {
				if(file::exists($file)) {
					$time = filemtime($file);
					$name .= "|".$file;
					$name_time .= "|".$file.":".$time;
					$files_exist[] = $file;
					//debug("file: ".$file.", time: ".$time.", name: ".$name,$c[debug]);
				}
			}
		}
		if($files_exist) {
			// Name / path
			$name_md5 = md5($name);
			$name_time_md5 = md5($name_time);
			$name = $name_md5."-".$name_time_md5.".css";
			$file_minified = $c[path].$name;
			$file_minified_time = filemtime($file_minified);
			$url_minified = str_replace(SERVER,D,$file_minified);
			
			// Debug
			debug("file_minified: ".$file_minified.", time: ".date('Y-m-d H:i:s',$file_minified_time)." (".$file_minified_time.")",$c[debug]);
			
			// Exists (and isn't expired)
			if($file_minified_time and (!$c[expires] or $file_minified_time > (time() - (86400 * $c[expires])))) {
				// Yes, don't need to re-minify
				return $url_minified;
			}
			
			// Class
			if($c[minify]) require_once SERVER."core/core/libraries/".$c['class'].".php";
			
			// Directory
			if(!file_exists($c[path])) file::directory_create($c[path]);
			
			// Depth
			$path_local = str_replace(SERVER,'',$c[path]);
			$path_folders = explode('/',$path_local);
			$path_depth = NULL;
			foreach($path_folders as $path_folder) {
				if(strlen($path_folder)) $path_depth .= "../";
			}
			
			// Loop through files
			foreach($files_exist as $x => $file) {
				// Debug
				debug("file = ".$file,$c[debug]);
					
				// Content
				$counter = 0;
				for($y = 0;$y < 1;) {
					$content = file_get_contents($file);
					if(!strstr($content,"/*##end##*/")) {
						if($counter < 10) {
							if($counter == 0) debug("<div class='notice notice-error notice-sticky'>Hey there! Looks like your ".str_replace(SERVER,'',$file)." file is missing the /*##end##*/ at the very end of it.</div>");
							usleep(300000); // Pause for .3 seconds
						}
						else $y = 1; // Only 10 tries so we don't crash the server
					}
					else $y = 1;
					$counter++;
				}
				if($content) {
					// Update image paths
					$depth = NULL;
					$folders = explode('/',str_replace(SERVER,'',$file));
					$count = count($folders) - 1;
					for($y = 0;$y < $count;$y++) $depth .= $folders[$y]."/";
					$content = preg_replace('/url\((\'|")?(?!http)/',"url($1".$path_depth.$depth."$2",$content);
					
					// Minify
					//$minified .= cssmin::minify($content); // Do now
					$minify .= $content; // Do later
				}
			}
			
			// Minify
			if($minify) {
				if($c[minify]) {
					if($c['class'] == "cssmin.3.0.1") $minified = CssMin::minify($minify);
					else $minified = cssmin::minify($minify);
				}
				else {
					$minified = $minify;
				}
				
				// Imports - must be at beginning
				preg_match_all('/@import (.*?);/',$minified,$matches);
				if(count($matches[1])) {
					$import = NULL;
					$minified = preg_replace('/@import (.*?);/','',$minified);
					foreach($matches[1] as $match) {
						if(substr($match,0,3) != "url") $import .= "@import url(".$match.");";
						else $import .= "@import ".$match.";";
					}
					$minified = $import.$minified;
				}
			}
			
			// Delete old minified files
			if($c[clean] and $name_md5) {
				if($handle = opendir($c[path])) {
					while(false !== ($f = readdir($handle))) {
						if(is_file($c[path].$f) and substr($f,0,strlen($name_md5)) == $name_md5 and substr($f,-3) == "css" and $f != $name) {
							unlink($c[path].$f);
						}
					}
				}
				closedir($handle);
			}
			
			// Save minified file
			file_put_contents($file_minified,$minified);
		}
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $url_minified;
	}
}

/************************************************************************************/
/************************************* HTML *****************************************/
/************************************************************************************/
if(!function_exists('minify_html')) {
	/**
	 * Minifies given html																
	 * 
	 * @param mixed $html
	 */
	function minify_html($html) {
		// Speed
		/*$f_r = function_speed(__FUNCTION__);
		// Having trouble with minification when the canvas() functions javascript is too long (more than about 60 shapes) 
		if(!x($_GET['minify']) or $_GET['minify'] == 1) {
			require_once SERVER."core/libraries/htmlmin.php";
			$html = Minify_HTML::minify($html);
			//$html = str_replace(array("\r","\n","\t"),' ',$html);
		}
		//$html = preg_replace('/<minifyhtml(.*?)>(.*?)<\/minifyhtml(.*?)>/si','',$html); // Not sure why this occurs, but get rid of it
		
		// Speed
		function_speed(__FUNCTION__,$f_r);*/
		
		// Return
		return $html;
	}
}

if(!function_exists('html_get')) {
	/**
	 * Returns HTML content of given file as it would be displayed to the end user (meaning, PHP gets processed).													
	 * 
	 * @param string $file The file you want to get the displayed content of.
	 * @param string The HTML of the displayed file.
	 */
	function html_get($file) {
		// Speed
		$f_r = function_speed(__FUNCTION__);
		
		// Get file content
		ob_start();
		if(strstr($file,"?")) print file_get_contents(str_replace(SERVER,DOMAIN,$file));
		else if(file_exists($file)) include $file;
		$html = ob_get_contents();
		ob_end_clean();
		
		// Speed
		function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $html;
	}
}

/************************************************************************************/
/********************************** Pagination **************************************/
/************************************************************************************/
if(!function_exists('pagination_limit')) {
	/**
	 * Returns start and end limit for given rows and perpage values.									
	 * 
	 * @param int $rows The number of total rows.
	 * @param int $perpage The number of rows to show per page. Default = 20
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The start and end limit for given values in a start,$perpage format.
	 */
	function pagination_limit($rows,$perpage = 20,$c = NULL) {
		// Config
		if(!$c[page]) $c[page] = ($_GET['p'] ? $_GET['p'] : 1); // The page we're currently on. Default = $_GET['p'] || 1
		
		// Limit
		if($rows and $perpage) {
			$start = ($c[page] - 1) * $perpage;
			$return = $start.",".$perpage;
		}
		else $return = 0;
		
		// Return
		return $return;
	}
}

if(!function_exists('pagination_count_first')) {
	/**
	 * Determines count of first item in current page's search results based given rows, perpage, and $c[page] values.
	 *
	 * Usually used to show what results we're listing. For example: 'Showing {start} - {end} of {total} results' where {start} would call up this function.
	 * 
	 * @param int $rows The number of total rows.
	 * @param int $perpage The number of rows to show per page. Default = 20
	 * @param array $c An array of configuration values. Default = NULL
	 * @return int The count of the first item in this page's results.
	 */
	function pagination_count_first($rows,$perpage = 20,$c = NULL) {
		// Config
		if(!$c[page]) $c[page] = ($_GET['p'] ? $_GET['p'] : 1); // The page we're currently on. Default = $_GET['p'] || 1
		
		// Limit
		if($rows and $perpage) {
			$return = (($c[page] - 1) * $perpage) + 1;
			if($return > $rows) $return = NULL;
		}
		else $return = NULL;
		
		// Return
		return $return;
	}
}

if(!function_exists('pagination_count_last')) {
	/**
	 * Determines count of last item in current page's search results based given rows, perpage, and $c[page] values.
	 *
	 * Usually used to show what results we're listing. For example: 'Showing {start} - {end} of {total} results' where {start} would call up this function.
	 * 
	 * @param int $rows The number of total rows.
	 * @param int $perpage The number of rows to show per page. Default = 20
	 * @param array $c An array of configuration values. Default = NULL
	 * @return int The count of the last item in this page's results.
	 */
	function pagination_count_last($rows,$perpage = 20,$c = NULL) {
		// Config
		if(!$c[page]) $c[page] = ($_GET['p'] ? $_GET['p'] : 1); // The page we're currently on. Default = $_GET['p'] || 1
		
		// Limit
		if($rows and $perpage) {
			// Start
			$return = ($c[page] - 1) * $perpage;
			// Start preceeds total rows, we're showing nothing
			if($return > $rows) $return = NULL;
			// End
			else {
				$return += $perpage;
				if($return > $rows) $return = $rows;
			}
		}
		else $return = NULL;
		
		// Return
		return $return;
	}
}

if(!function_exists('pagination_html')) {
	/**
	 * Returns HTML for displaying page navigation.	    
	 * 
	 * @param int $rows The number of total rows.
	 * @param int $perpage The number of rows to show per page. Default = 20
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The HTML used for navigating through pages.
	 */
	function pagination_html($rows,$perpage = 20,$c = NULL) {
		// Config
		if(!$c[page]) $c[page] = (is_int_value($_GET['p']) ? $_GET['p'] : 1); // The page we're currently on. Default = $_GET['p'] || 1
		if(!x($c[next_count])) $c[next_count] = 3; // The number of next pages to show after the current page. Default = 3
		if(!x($c[prev_count])) $c[prev_count] = 3; // The number of previous pages to show before the current page. Default = 3
		if(!x($c[buttons_text])) $c[buttons_text] = 1; // Do you want to show the 'next', 'prev', etc. text next to those buttons? Default = 1
		if(!x($c[url])) $c[url] = preg_replace('/(\?|\&)p=[0-9]{1,6}/','',URL); // Root URL we want to append our p={p} to
		$c[url] .= (strstr($c[url],'?') ? "&" : "?");
		
		// More than 1 page
		if($rows > $perpage) {
			// First page
			$page_first = 1;
			// Current page
			$page_current = $c[page];
			// Last page
			$page_last = ceil($rows / $perpage);
		
			// Next/prev text
			if($c[buttons_text]) {
				$text_first = " first";
				$text_previous = " prev";
				$text_next = "next ";
				$text_last = "last ";
			}
			$text = "
<div class='pagination'>";
	
			// Previous
			$text .= "
	<div class='pagination-page pagination-page-first".($page_current == $page_first ? " core-fade'>&lt;&lt;".$text_first : "'><a href='".$c[url]."p=1'>&lt;&lt;".$text_first."</a>")."</div>
	<div class='pagination-page pagination-page-prev".($page_current == $page_first ? " core-fade'>&lt;".$text_previous : "'><a href='".$c[url]."p=".($page_current - 1)."'>&lt;".$text_previous."</a>")."</div>";
				
			// Page Numbers
			if($page_current > $c[prev_count]) $page_start = $page_current - $c[prev_count];
			else $page_start = $page_first;
			if($page_last > $page_current + $c[next_count]) $page_end = $page_current + $c[next_count];
			else $page_end = $page_last;
									
			for($page_count = $page_start;$page_count <= $page_end;$page_count++) $text .= "
	<div class='pagination-page pagination-page-number".($page_count == $page_current ? " pagination-page-selected'>".$page_count : "'><a href='".$c[url]."p=".$page_count."'>".$page_count."</a>")."</div>";
						
			// Next
			$text .= "
	<div class='pagination-page pagination-page-next".($page_current == $page_last ? " core-fade'>".$text_next."&gt;" : "'><a href='".$c[url]."p=".($page_current + 1)."'>".$text_next."&gt;</a>")."</div>
	<div class='pagination-page pagination-page-last".($page_current == $page_last ? " core-fade'>".$text_last."&gt;&gt;" : "'><a href='".$c[url]."p=".$page_last."'>".$text_last."&gt;&gt;</a>")."</div>
</div>";
		}
	
		return $text;
	}
}

/************************************************************************************/
/************************************* Help *****************************************/
/************************************************************************************/
if(!function_exists('help')) {
	/**
	 * Returns HTML of a help image which shows a tooltip with given text on hover.			
	 * 
	 * @param string $text The text to show when a user hovers over the help icon.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function help($text,$c = NULL) {
		if(!x($c[sprite])) $c[sprite] = 0; // Use a sprite (floats left) as opposed to just an img element
		if(!$c[attributes]) $c[attributes] = NULL; // An array of attributes to add to the element. Example: array('class' => 'myclass','id' => 'my_id').
		
		// Line breaks
		$text = string_breaks($text);
		
		// Attributes
		$c[attributes]['class'] .= ($c[attributes]['class'] ? " " : "")."i i-help tooltip";
		$attributes = attributes_string($c[attributes]);
		
		// Icon
		$icon = "<span class='help-icon'><img src='".D."core/core/images/blank.gif' alt='[?]' width='16' height='16' title='".string_encode($text)."' ".$attributes." /></span>";
		
		return $icon;
	}
}

/************************************************************************************/
/********************************** Attributes **************************************/
/************************************************************************************/
if(!function_exists('attributes_string')) {
	/**
	 * Converts an array of attributes to a string.
	 * 
	 * @param array $array The array of attributes you want to turn into a string.
	 * @return string The string of attributes.
	 */
	function attributes_string($array) {
		foreach($array as $k => $v) {
			$quote = (preg_match('/[^\\\]"/',$v) ? "'" : '"');
			$string .= ($string ? " " : "").$k."=".$quote.$v.$quote;
		}
		return $string;
	}
}
?>