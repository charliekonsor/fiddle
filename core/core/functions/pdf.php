<?php
/**
 * @package kraken\pdf
 */
 
if(!function_exists('html2pdf')) {
	/**
	 * Converts the given HTML to a PDF file.
	 *
	 * Note: dompdf isn't included in framework by default. Must upload it to /core/core/libraries/dompdf/.
	 *
	 * @param string $html The HTML you want to convert to a PDF file.
	 * @param string $path The path where you want to save the PDF.
	 * @param array $c An array of configuration values. Default = NULL
	*/
	function html2pdf($html,$path,$c = NULL) {
		// Error
		if(!$html or !$path) return;
		
		// Config
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// PDF
		require_once SERVER."core/core/libraries/dompdf/dompdf_config.inc.php";
		spl_autoload_register('__autoload');
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->render();
		$pdf = $dompdf->output();
	
		// Save
		file_put_contents($path,$pdf);
	}
}
?>