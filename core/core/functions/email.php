<?php
/**
 * @package kraken\email
 */
// Mailgun autoloader
include SEVER.g('config.composer.path')."vendor/autoload.php";
use Mailgun\Mailgun;
 
if(!function_exists('email')) {
	/**
	 * Send out an e-mail given a to, subject, and body.
	 * 
	 * Dependencies
	 * - functions
	 *   - x()
	 *   - g()
	 *   - debug()
	 *   - return_array() 
	 * - classes
	 *   - file
	 *   - Whatever mailing $c['class'] you're using
	 * 
	 * @param string|array $to The e-mail address(es) we're sending this e-mail to. Can be a single address, a comma separated list of addreses, or an array of addresses.  The array format is either array('email@domain.com') or array('email@domain.com' => 'Persons Name').
	 * @param string $subject The subject line of the e-mail we're sending.
	 * @param mixed $body The body of the e-mail we're sending.
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function email($to,$subject,$body,$c = NULL) {
		// Debug speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Domain
		$domain = rtrim(str_replace(array('http://','https://','www.'),'',DOMAIN),'/');
		
		// Config
		if(!$c[from_email]) $c[from_email] = m('settings.settings.email.from.address'); // E-mail address to send this e-mail 'from'
		if(!$c[from_name]) $c[from_name] = m('settings.settings.email.from.name'); // The 'name' we'll send this e-mail from
		if(!$c[cc]) $c[cc] = NULL; // E-mail address(es) to cc this e-mail too
		if(!$c[bcc]) $c[bcc] = NULL; // E-mail address(es) to bcc this e-mail too
		if(!x($c[html])) $c[html] = 1; // Send an HTML version of the e-mail
		if(!x($c[text])) $c[text] = 1; // Send a text version of the e-mail
		if(!$c[headers]) $c[headers] = NULL; // Custom headers
		if(!$c[attachments]) $c[attachments] = NULL; // An array of files (either path to local file or url to external file) we want to attach
		if(!$c[attachment]) $c[attachment] = NULL; // More user friendly way to add a single attachment (same file types as the $c[attachments] array)
		if(!$c['class']) $c['class'] = g('config.mail.class'); // Class to send mail with: mail (php's mail() function), phpmailer [default], zend, swiftmailer, pear (requires PEAR mail to be installed), mailgun (requires mail gun account and composer)
		if(!x($c['wordwrap'])) $c['wordwrap'] = 0; // Wordwrap at 70 characters
		if(!x($c[smtp])) $c[smtp] = g('config.mail.smtp.active'); // Send via SMTP
		if(!$c[smtp_port]) $c[smtp_port] = g('config.mail.smtp.port'); // Port to send SMTP messages from
		if(!$c[smtp_secure]) $c[smtp_secure] = g('config.mail.smtp.secure'); // Security to use when sending via SMTP (optional): tls, ssl
		if(!$c[smtp_host]) $c[smtp_host] = (g('config.mail.smtp.host') ? g('config.mail.smtp.host') : "mail.".$_SERVER['HTTP_HOST']); // SMTP Host (usually mail.mydomain.com)
		if(!$c[smtp_username]) $c[smtp_username] = g('config.mail.smtp.username'); // SMTP Username
		if(!$c[smtp_password]) $c[smtp_password] = g('config.mail.smtp.password'); // SMTP Password
		if(!x($c[dkim])) $c[dkim] = g('config.mail.dkim.active'); // Use DKIM when sending mail
		if(!$c[dkim_domain]) $c[dkim_domain] = (g('config.mail.dkim.domain') ? g('config.mail.dkim.domain') : $domain); // Domain we're using in DKIM signature
		if(!$c[dkim_private]) $c[dkim_private] = (g('config.mail.dkim.private') ? g('config.mail.dkim.private') : SERVER.'.htkeyprivate'); // Private DKIM key location.
		if(!$c[dkim_selector]) $c[dkim_selector] = (g('config.mail.dkim.selector') ? g('config.mail.dkim.selector') : "mail"); // Selector we're using in DKIM signature
		if(!$c[dkim_password]) $c[dkim_password] = g('config.mail.dkim.password'); // Password we're using in DKIM signature
		if(!$c[mailgun_domain]) { // MailGun Domain
			$c[mailgun_domain] = g('config.mail.mailgun.domain');
			if(!$c[mailgun_domain]) $c[mailgun_domain] = $domain;
		}
		if(!$c[mailgun_api_key]) $c[mailgun_api_key] = g('config.mail.mailgun.api.key'); // MailGun API Key
		if(!$c[mandrill_api_key]) $c[mandrill_api_key] = g('config.mail.mandrill.api.key'); // Mandrill API Key
		if(!x($c[pop3])) $c[pop3] = 0; // Authorize via POP3 instead of SMTP (not sure what the difference is, only for phpmailer class)
		if(!x($c[debug])) $c[debug] = 0; // Debug
		if($c[smtp] and !in_array($c['class'],array('phpmailer'))) $c['class'] = "phpmailer"; // SMTP is only currently set up for phpmailer
		$ex = explode(',',$c[from_email]); // Make sure we only have one 'from e-mail'
		foreach($ex as $e) {
			if(x(trim($e))) {
				$c[from_email] = trim($e);
				break;
			}
		}
		$boundary = "Boundary_".md5(date('r'));
		$break = "\r\n";
		
		// Debug
		debug("<b>email($to,$subject,$body);</b>",$c[debug]);
		debug("c:".return_array($c),$c[debug]);
		
		// Cleanup From Name
		$c[from_name] = string_decode(str_replace(',','',$c[from_name]));
		
		// Standardize e-mails
		$emails = array(
			'to' => $to,
			'cc' => $c[cc],
			'bcc' => $c[bcc]
		);
		$emails_strings = NULL;
		foreach($emails as $type => $addresses) {
			$array = NULL;
			$emails_strings[$type] = 0;
			if(!is_array($addresses)) {
				$ex = explode(',',$addresses);
				foreach($ex as $email) {
					$email = trim($email);
					if(x($email)) $array[$email] = $email;
				}
				$emails_strings[$type] = 1;
			}
			else {
				foreach($addresses as $email => $name) {
					if(is_numeric($email) and strstr($name,"@")) $email = $name;
					$array[$email] = $name;
				}
			}
			$emails[$type] = $array;
		}
		debug("emails:".return_array($emails),$c[debug]);
		
		// No e-mail to send to
		if(!count($emails[to])) {
			debug("No e-mail to send to",$c[debug]);
			return;	
		}
		
		// Standardize attachments
		if($c[attachment] and !$c[attachments]) $c[attachments][] = $c[attachment]; // Single attachment, add to attachments array
		if($c[attachments]) {
			// Attachments
			$attachments = NULL;
			
			// Standarize
			foreach($c[attachments] as $file) {
				$f = file::load($file);
				$file = $f->file;
				if(substr($file,0,4) == "http") { // External URL, save to temporary folder
					if(ini_get('allow_url_fopen') != "On" or $c['class'] != "swiftmailer") { // If allow_url_fopen is on swiftmailer can handle external urls.
						$file = file::save_temp($file);
					}
				}
				if($file) $attachments[] = $file;
			}
			
			// Restore
			$c[attachments] = $attachments;
		}
			
		// Format Text Version
		$body_text = str_replace("
	
","",strip_tags(preg_replace('/<a(.*?)href=("|\')(.*?)("|\')(.*?)<\/a>/si','$3',$body)));

		// Save
		if(!$c[email_id]) {
			$values = array(
				'email_to' => str_replace(array("<xmp>","</xmp>"),"",return_array($emails)),
				'email_subject' => $subject,
				'email_message' => $body,
				'email_message_text' => $body_text,
				'email_class' => $c['class'],
				'email_config' => str_replace(array("<xmp>","</xmp>"),"",return_array($c)),
				'email_url' => URL,
				'email_created' => time(),
			);
			$db = db::load();
			$c[email_id] = $db->q("INSERT INTO emails ".$db->values($values));
		}
		
		// mail()
		if($c['class'] == "mail") {
			// Word Wrap
			if($c['wordwrap'] == 1) {
				$body = wordwrap($body,70);
				$body_text = wordwrap($body_text,70);
			}
		
			// Send both HTML and Text Versions
			$headers = "MIME-Version: 1.0".$break;
			if($c[html] == 1 and $c[text] == 1) $headers .= "Content-Type: multipart/alternative; boundary = \"".$boundary."\"".$break;
			else if($c[text] == 1) $headers .= "Content-type: text/plain; charset=\"UTF-8\"".$break;
			else $headers .= "Content-type: text/html; charset=\"UTF-8\"".$break;
			$headers .= "From: ".$c[from_name]." <".$c[from_email].">".$break;
			if($emails[cc]) $headers .= "Cc: ".implode(', ',$emails[cc]).$break;
			if($emails[bcc]) $headers .= "Bcc: ".implode(', ',$emails[bcc]).$break;
			$headers .= "Reply-To: ".$c[from_name]." <".$c[from_email].">".$break;
			$headers .= "Date: ".date("r").$break;
			$headers .= "Return-Path: ".$c[from_name]." <".$c[from_email].">".$break;
			$headers .= "Delivered-to: ".$c[from_name]." <".$c[from_email].">".$break;
			$headers .= "Message-ID: <".date("YmdHis")."-".random()."Mail@".$_SERVER['SERVER_NAME'].">";
			if($c[headers]) $headers .= $break.$c[headers];
				
			// Send Both Text and HTML
			if($c[html] == 1 and $c[text] == 1) {
				// Text
				$message = "--".$boundary."\n"; // beginning \n added to separate previous content
				$message .= "Content-type: text/plain; charset=\"UTF-8\"".$break;
				$message .= "Content-Transfer-Encoding: 7bit".$break.$break;
				$message .= $body_text;
				// HTML
				$message .= "\n--".$boundary."\n";
				$message .= "Content-type: text/html; charset=\"UTF-8\"".$break;
				$message .= "Content-Transfer-Encoding: 7bit".$break.$break;
				$message .= $body;
			}
			// Send Text Only
			else if($c[text] == 1) $message = $body_text;
			// Send HTML Only
			else $message = $body;
			
			/*$from_domain = substr($c[from_email],strpos($c[from_email],'@'),strlen($c[from_email]));
			$site_domain = substr(EMAIL_FROM_ADDRESS,strpos(EMAIL_FROM_ADDRESS,'@'),strlen(EMAIL_FROM_ADDRESS));
			
			if($from_domain == $site_domain) ini_set(sendmail_from,$c[from_email]);*/
			
			// String - example: $to = "test@test.com, john@gmail.com";
			if($emails_strings[to] == 1) {
				$email = implode(', ',array_keys($emails[to]));
				debug("<xmp>mail($email, $subject, $message, $headers);</xmp>",$c[debug]);
				mail($email, $subject, $message, $headers);
			}
			// Array - example: $to = array('test@test.com','john@gmail.com' => 'John Doe');
			else {
				foreach($emails[to] as $email => $name) {
					debug("<xmp>mail($email, $subject, $message, $headers);</xmp>",$c[debug]);
					mail($email, $subject, $message, $headers);	
				}
			}
		}
		
		// PHPMailer
		if($c['class'] == "phpmailer") {
			// Class
			$file = SERVER."core/core/libraries/email/phpmailer/PHPMailerAutoload.php";
			//$file = SERVER."core/core/libraries/email/phpmailer/class.phpmailer.php";
			if(file_exists($file)) {
				require_once $file;
				
				// POP3 // I think this just offers a different way to authorize via SMTP/POP3 (instead of using $mail->SMTPAuth below)
				if($c[pop3]) {
					require_once SERVER."core/phpmailer/class.pop3.php";
					$pop = new POP3();
					$pop->Authorise($c[smtp_host], 110, 30, $c[smtp_username], $c[smtp_password], 1);
				}
				
				// Instance
				$mail = new PHPMailer();
				if($c[smtp] and $c[smtp_host]) {
					$mail->IsSMTP();
					$mail->Host = $c[smtp_host];
					if($c[smtp_port]) $mail->Port = $c[smtp_port];
					if($c[smtp_secure]) $mail->SMTPSecure = $c[smtp_secure];
					if($c[smtp_username] and $c[smtp_password] and !$c[pop3]) {
						$mail->SMTPAuth = true;
						$mail->Username = $c[smtp_username];
						$mail->Password = $c[smtp_password];
					}
					if(debug_active() and $c[debug] == 1) $mail->SMTPDebug = 2;
				}
				else $mail->IsMAIL();
				
				// DKIM
				if($c[dkim] and $c[dkim_domain] and $c[dkim_private] and $c[dkim_selector] and $c[dkim_password]) {
					$mail->DKIM_domain = $c[dkim_domain];
					$mail->DKIM_private = $c[dkim_private];
					$mail->DKIM_selector = $c[dkim_selector];
					$mail->DKIM_passphrase = $c[dkim_password];
				}
				
				// Subject
				$mail->Subject = $subject;
				// Body
				$mail->Body = $body;
				// Headers
				$mail->From = $c[from_email];
				$mail->FromName = $c[from_name];
				$mail->ContentType = ($c[html] == 1 ? "text/html" : "text/plain"); // If both, the class will automatically set to 'multipart/alternative'
				$mail->CharSet = "UTF-8";
				if($c[text] == 1 and $c[html] == 1) $mail->AltBody = $body_text;
				if($c['wordwrap']) $mail->WordWrap = 70;
				if($c[headers]) $mail->AddCustomHeader($c[headers]); // Untested
				
				// To
				foreach($emails[to] as $email => $name) {
					debug("Sending mail to ".$email.($email != $name ? " (".$name.")" : ""),$c[debug]);
					$mail->AddAddress($email,($email != $name ? $name : NULL));
				}
				// CC
				if($emails[cc]) {
					foreach($emails[cc] as $email => $name) $mail->AddCC($email,($email != $name ? $name : NULL));
				}
				// BCC
				if($emails[bcc]) {
					foreach($emails[bcc] as $email => $name) $mail->AddBCC($email,($email != $name ? $name : NULL));
				}
				
				// Attachments
				if($c[attachments]) {
					foreach($c[attachments] as $file) $mail->AddAttachment($file);
				}
				
				// Send
				if(!$mail->Send()) {
					$error = "PHPMailer Error: ".nl2br($mail->ErrorInfo);
				}
				else {
					debug("Mail sent",$c[debug]);
				}
			}
			else {
				$error = SERVER."core/core/libraries/email/phpmailer/class.phpmailer.php doesn't exist";
			}
		}
		
		// Zend
		if($c['class'] == "zend") {
			/* Notes
			- Maybe add setReplyTo
			- Maybe add setReturnPath
			*/
			
			// Class
			if(file_exists(SERVER."core/core/Zend/Mail.php")) {
				set_include_path(get_include_path().PATH_SEPARATOR.SERVER."core/core/");
				require_once SERVER."core/core/Zend/Mail.php";
				$mail = new Zend_Mail("UTF-8"); // The UTF-8 parameter is untested, but should work: http://framework.zend.com/manual/en/zend.mail.character-sets.html
	
				// Word Wrap
				if($c['wordwrap'] == 1) {
					$body = wordwrap($body,70);
					$body_text = wordwrap($body_text,70);
				}
				
				// SMTP // Doesn't work, thorws error I can't see because developed on a shitty server that doesn't show errors
				/*if($c[smtp] and $c[smtp_host]) {
					$config = array('ssl' => 'tls', 'port' => $c[smtp_port], 'auth' => 'login', 'username' => $c[smtp_username], 'password' => $c[smtp_password]);
					$transport = new Zend_Mail_Transport_Smtp($c[smtp_host], $config);
				}*/
				
				// Subject
				$mail->setSubject($subject);
				// Body
				if($c[text]) $mail->setBodyText($body_text);
				if($c[html]) $mail->setBodyHtml($body);
				// Headers
				$mail->setFrom($c[from_email], $c[from_name]);
				if($c[headers]) $mail->addHeader($c[headers]); // Untested
				
				// To
				foreach($emails[to] as $email => $name) {
					debug("Sending mail to ".$email.($email != $name ? " (".$name.")" : ""),$c[debug]);
					$mail->addTo($email,($email != $name ? $name : NULL));
				}
				// CC
				if($emails[cc]) {
					foreach($emails[cc] as $email => $name) $mail->addCc($email,($email != $name ? $name : NULL));
				}
				// BCC
				if($emails[bcc]) {
					foreach($emails[bcc] as $email => $name) $mail->addBcc($email);
				}
				
				// Attachments
				if($c[attachments]) {
					foreach($c[attachments] as $file) {
						if(substr($file,0,4) == "http") $file_contents = curl($file); // External URL, cURL contents
						else $file_contents = file_get_contents($file); // Local URL, file_get_contents
						
						$mail_attachment = $mail->createAttachment($file_contents); // Content
						$mail_attachment->type = file_contenttype($file); // Contenttype
						$mail_attachment->filename = basename($file); // File name
					}
				}
				
				// Send
				$mail->send();
			}
			else {
				$error = SERVER."core/core/Zend/Mail.php doesn't exist";
			}
		}
		
		// Swift Mailer
		if($c['class'] == "swiftmailer") {
			/* Notes
			- Use their different 'transport types' http://swiftmailer.org/docs/transport-types
			- Too lazy to add custom headers
			- SwiftMailer automatically does word wrapping (maybe see if there's a setting to switch off)
			- Maybe add setReplyTo
			- Maybe add setReturnPath
			- Should take advantage of sendBatch option (sends individual e-mails to each 'to' insteade of adding all 'tos' to same e-mail)
			*/
			
			// Class
			if(file_exists(SERVER."core/core/libraries/email/swiftmailer/lib/swift_required.php")) {
				require_once SERVER."core/core/libraries/email/swiftmailer/lib/swift_required.php";
				
				// Instance
				$transport = Swift_MailTransport::newInstance();
				/*if($c[smtp] and $c[smtp_host]) { // Doesn't work, thorws error I can't see because developed on a shitty server that doesn't show errors
					$transport->setHost($c[smtp_host]);
					$transport->setPort($c[smtp_port]);
					if($c[smtp_username] and $c[smtp_password]) {
						$transport->setUsername($c[smtp_username]);
						$transport->setPassword($c[smtp_password]);
					}
				}*/
				$mailer = Swift_Mailer::newInstance($transport);
				$mail = Swift_Message::newInstance();
				
				// Subject
				$mail->setSubject($subject);
				// Body
				if($c[html] == 1) $mail->setBody($body,'text/html','UTF-8');
				else if($c[text] == 1) $mail->setBody($body,'text/plain','UTF-8');
				if($c[html] == 1 and $c[text] == 1) $mail->addPart($body_text,'text/plain','UTF-8');
				$mail->setCharset('UTF-8'); // UTF-8 on this line and above is untested, but should work: http://swiftmailer.org/docs/messages.html#setting-the-character-set
				
				// Headers
				$mail->setFrom(array($c[from_email] => $c[from_name]));
				
				// To
				foreach($emails[to] as $email => $name) {
					debug("Sending mail to ".$email.($email != $name ? " (".$name.")" : ""),$c[debug]);
					$mail->setTo($emails[to]);
				}
				// CC
				if($emails[cc]) $mail->setCc($emails[cc]);
				// BCC
				if($emails[bcc]) $mail->setBcc($emails[bcc]);
				
				// Attachments
				if($c[attachments]) {
					foreach($c[attachments] as $file) $mail->attach(Swift_Attachment::fromPath($file));
				}
				
				// Send
				$result = $mailer->send($mail);
				debug("send result: ".$result,$c[debug]);
			}
			else {
				$error = SERVER."core/core/libraries/email/swiftmailer/lib/swift_required.php doesn't exist";
			}
		}

		// PEAR - requires PEAR mail extension to be installed
		if($c['class'] == "pear") {
			if(file_exists("Mail.php")) {
				require_once "Mail.php";
				require_once 'Mail/mime.php' ;
				
				$to = implode(', ',$emails[to]);
				
				$mime = new Mail_mime(array('eol' => $crlf));
				
				// Content
				$mime->setTXTBody($body_text);
				$mime->setHTMLBody($body);
				
				// Attachments
				if($c[attachments]) {
					foreach($c[attachments] as $file) $mime->addAttachment($file, 'text/plain');
				}
				
				// Headers
				$headers = array(
					'From' => $c[from_name]." <".$c[from_email].">",
					'To' => $to,
					'Subject' => $subject);
				
				$message = $mime->get();
				$headers = $mime->headers($headers);
				
				// SMTP
				if($c[smtp]) {
					$mail = Mail::factory('smtp',
						array (
							'host' => $c[smtp_host],
							'port' => $c[smtp_port],
							'auth' => true,
							'username' => $c[smtp_username],
							'password' => $c[smtp_password]
						)
					);
				}
				// Regular mail
				else {
					$mail = Mail::factory('mail');
				}
	
				// Send
				$send = $mail->send($to, $headers, $message);
				debug("<xmp>\$mail->send($to, $headers, $message);</xmp>",$c[debug]);
				if(PEAR::isError($send)){
					$error = "PEAR Error: ".$send->getMessage();
				}
			}
			else {
				$error = "The PEAR extension Mail.php doesn't exist.";
			}
		}
		
		/**
		 * Mailgun
		 *
		 * Documentation: http://documentation.mailgun.com/quickstart.html
		 * PHP Class: https://github.com/mailgun/mailgun-php
		 *
		 * Requires composer and mailgun to be installed (see PHP Class link above).
		 * I have a copy of this (composer & mailgun) in /kraken-extras/core/core/libraries/composer/.
		 * Place /composer/ folder in /core/core/libraries/ and all should work fine.
		 *
		 * If you're getting errors with the requiring of vendor/autoload.php, update the composer path in the local config.php file.
		 */
		if($c['class'] == "mailgun") {
			if($c[mailgun_api_key]) {
				// Instantiate the client. 
				//if(class_exists('Mailgun')) { // Never seems to catch even if it exists
					$mail = new Mailgun($c[mailgun_api_key]);
					
					// To
					$to = NULL;
					foreach($emails[to] as $email => $name) {
						$to .= ($to ? ", " : "").(!strstr($name,'@') ? $name." <".$email.">" : $email);
					}
					// CC
					$cc = NULL;
					foreach($emails[cc] as $email => $name) {
						$cc .= ($cc ? ", " : "").(!strstr($name,'@') ? $name." <".$email.">" : $email);
					}
					// BCC
					$bcc = NULL;
					foreach($emails[bcc] as $email => $name) {
						$bcc .= ($bcc ? ", " : "").(!strstr($name,'@') ? $name." <".$email.">" : $email);
					}
					
					// Config
					$mail_c = array(
						'from' => $c[from_name]." <".$c[from_email].">",
						'to' => $to,
						'subject' => $subject,
						'text' => $contents_text,
						'html' => $contents,
					);
					if($cc) $mail_c[cc] = $cc; // Don't pass unless we have a value
					if($bcc) $mail_c[bcc] = $bcc; // Don't pass unless we have a value
					
					// Attachments
					$mail_attachments = NULL;
					if($c[attachments]) {
						foreach($c[attachments] as $file) {
							$file = file_localize($file); // Try to 'localize' file name
							if(file_exists($file)) $mail_attachments[attachment][] = "@".$file;
						}
					}
					
					// Send
					try {
						debug("sending. array: ".return_array($mail_c),$c[debug]);
						if($mail_attachments) $result = $mail->sendMessage($c[mailgun_domain],$mail_c,$mail_attachments); // Can't pass variable (even if NULL) unless there's data in it
						else $result = $mail->sendMessage($c[mailgun_domain],$mail_c);
						debug("result: ".return_array($result),$c[debug]);
					}
					catch(Exception $e) {
						$error = $e->getMessage();
					}
				/*}
				else {
					$error = "Composer isn't present or couldn't autoload the Mailgun class.";
				}*/
			}
			else {
				$error = "The MailGun API Key isn't defined.";
			}
		}
		
		// Mandrill
		if($c['class'] == "mandrill") {
			if($c[mandrill_api_key]) {
				include_once SERVER."core/core/libraries/mailchimp/src/Mandrill.php";
				// Instantiate the client. 
				if(class_exists('Mandrill')) {
					try {
						$to = NULL;
						foreach($emails[to] as $email => $name) {
							$to[] = array(
								'email' => $email,
								'name' => (!strstr($name,'@') ? $name : ''),
								'type' => 'to',
							);
						}
						foreach($emails[cc] as $email => $name) {
							$to[] = array(
								'email' => $email,
								'name' => (!strstr($name,'@') ? $name : ''),
								'type' => 'cc',
							);
						}   
						foreach($emails[bcc] as $email => $name) {
							$to[] = array(
								'email' => $email,
								'name' => (!strstr($name,'@') ? $name : ''),
								'type' => 'bcc',
							);
						}
						
						$mandrill = new Mandrill($c[mandrill_api_key]);	
						$message = array(
							'html' => $body,
							'text' => $body_text,
							'subject' => $subject,
							'from_email' => $c[from_email],
							'from_name' => $c[from_name],
							'to' => $to,
							'headers' => array('Reply-To' => $c[from_email]),
							'auto_text' => true,
						);
						$async = false;
						$result = $mandrill->messages->send($message, $async);
						debug("Mandrill result: ".return_array($result),$c[debug]);
					} catch(Mandrill_Error $e) {
						$error = $e->getMessage();
					}
				}
				else {
					$error = "The Mandrill class is missing.";
				}
			}
			else {
				$error = "The Mandrill API Key isn't defined.";
			}
		}
		
		// Error
		if($error) {
			// Debug
			debug($error,$c[debug]);
			
			// SMTP - try sending without
			if($c[smtp]) {
				$c[smtp] = 0;
				email($to,$subject,$body,$c);
			}
			// Save error
			else {
				$db->q("UPDATE emails SET email_error = '".a($error,1)."' WHERE email_id = '".$c[email_id]."'");
			}
		}
	}
}
?>