<?php
/**
 * @package kraken\table
 */
 
if(!function_exists('table2array')) {
	/**
	 * Converts a table in the given HTML string to an array of data contained within the table.
	 *
	 * Note, currently only one dimensional (e.g. can't parse a table within a table).
	 * 
	 * @param string $html The HTML string containing the table we want to parse the data from.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The resulting array of data.
	 */
	function table2array($html) {
		// Error
		if(!$html) return;
		
		// Dom
		$DOM = new DOMDocument;
		$DOM->loadHTML($html);
	
		// Rows
		$rows = $DOM->getElementsByTagName('tr');
		foreach($rows as $row) {
			// Row
			$array_row = NULL;
			foreach($row->childNodes as $column) {
				$array_row[] = $column->nodeValue;	
			}
			$array[] = $array_row;
		}
		
		// Return
		return $array;
	}
}
?>