<?php
/**
 * @package kraken\zip
 */
 
if(!function_exists('zip_files')) {
	/**
	 * Creates a zip archive containing all the files in the $files array and saves it to the given $path.
	 *
	 * @param array $files An array of files you want to zip (in $path => $name format).
	 * @param string $path The path (including file name) where you want to save the resulting zip archive file.
	 * @param array $c An array of configuration values. Default = NULL
	*/
	function zip_files($files,$path,$c = NULL) {
		// Error
		if(!$files or !$path) return;
		
		// Config
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// Class
		if(class_exists('ZipArchive')) {
			$zip = new ZipArchive;
			// Create target archive file
			if($zip->open($path,ZIPARCHIVE::OVERWRITE) === true) {
				// Files
				foreach($files as $file => $name) {
					if(is_int($file)) {
						$file = $name;
						$name = file::name($file);	
					}
					debug("adding file ".$file." => ".$name,$c[debug]);	
					$zip->addFile($file,$name);
				}
				// Close
				$zip->close();
			}
		}
		else {
			debug("ZipArchive class doesn't exist",$c[debug]);	
		}
	}
}

if(!function_exists('zip_directory')) {
	/**
	 * Grabs all items (files and folders) in a directory and creates a zip archive (which get saved to the $path param).
	 *
	 * @param string $directory The path to the local directory you want to zip the contents of.
	 * @param string $path The path (including file name) where you want to save the zip archive.
	 * @param array $c An array of configuration values. Default = NULL
	*/
	function zip_directory($directory,$path,$c = NULL) {
		// Error
		if(!$directory or !$path) return;
		
		// Config
		if(!$c[skip]) $c[skip] = array(); // An array of relative (to the $directory) paths of files you want to skip.
		if(!$c[prefix]) $c[prefix] = NULL; // A string to prefix to all file names in the archive. Usually used to put everything in a folder. Ex: "content/".
		
		// Files
		$files = file::directory_files($directory,array('recursive' => 1,'multilevel' => 0));
		
		// Array
		$files_array = array();
		foreach($files as $x => $file) {
			if(!in_array($file,$c[skip])) $files_array[$directory.$file] = $c[prefix].$file;
		}
		
		// Zip
		zip_files($files_array,$path,$c);
	}
}

if(!function_exists('zip_extract')) {
	/**
	 * Extracts the zipped file's contents to the given $path.
	 *
	 * To use the ZipArchive class, the zip PHP extension must be installed.
	 *
	 * If it isn't, we fall back to a simple PHP function based off of:
	 * http://www.php.net/manual/en/ref.zip.php#105989
	 * Note, that this requires the zip functions: http://php.net/manual/en/ref.zip.php
	 *
	 * @param string $file The zip file you want to extract.
	 * @param string $path The path where you want to unzip the zipped files to.
	 * @param array $c An array of configuration values. Default = NULL
	*/
	function zip_extract($file,$path,$c = NULL) {
		// Error
		if(!$file or !$path) return;
		
		// Config
		if(!x($c[debug])) $c[debug] = 0; // Debug
		
		// ZipArchive
		if(class_exists('ZipArchive')) {
			$zip = new ZipArchive;
			// Open
			if($zip->open($file) === true) {
				// Path exists
				if(!file_exists($path) or !is_dir($path)) mkdir($path,0777);
				
				// Extract
				$zip->extractTo($path);
				// Close
				$zip->close();
				
				// Return
				$return = true;
			}
				
			// Return
			debug("zip file (".$file.") couldn't be opened",$c[debug]);	
			$return = false;
		}
		
		// PclZip
		if(!x($return)) {
			include_once SERVER."core/core/libraries/pclzip.lib.php";
			if(class_exists('PclZip')) {
				debug("Using PclZip class",$c[debug]);
				
				$archive = new PclZip($file);
				if($archive->extract(PCLZIP_OPT_PATH,$path) == 0) {
					debug("Error extracting ".$file." to ".$path,$c[debug]);
					$return = false;
				}
				else {
					$return = true;
				}
			}
		}
		
		// Function // Untested
		if(!x($return) and function_exists('zip_open')) {
			// Open zipped file
			$zip = zip_open($file);
			if(is_resource($zip)){
				// Loop through entries
				$tree = "";
				while(($zip_entry = zip_read($zip)) !== false){
					debug("Unpacking ".zip_entry_name($zip_entry),$c[debug]);
					// Is a diretory
					if(strpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR) !== false){
						$last = strrpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR);
						$dir = substr(zip_entry_name($zip_entry), 0, $last);
						$file = substr(zip_entry_name($zip_entry), strrpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR)+1);
						if(!is_dir($dir)){
							@mkdir($dir, 0777, true) or die("Unable to create $dir\n");
						}
						if(strlen(trim($file)) > 0){
							$result = @file_put_contents($dir."/".$file, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
							if($result === false){
								die("Unable to write file $dir/$file");
							}
						}
					}
					// Is a file
					else{
						file_put_contents($file, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
					}
				}
				
				// Return
				$return = true;
			}
			else{
				// Return
				debug("Unable to open zip file",$c[debug]);
				$return = false;
			}	
		}
		
		/*if($return == true) {
			// Mac? - mac doesn't just zip folder, but creates a parent directory with folder and __MACOSX folders // Wrong, it doesn't create the parent, that was what I zipped, I created it (though it does creat the __MACOSX folder
			if(is_dir($path."__MACOSX/")) {
				// Path parent folder
				$ex = explode('/',rtrim($path,'/'));
				unset($ex[count($ex) - 1]);
				$path_parent = implode('/',$ex);
				
				// Unzipped folder name - not always equal to the zip's name or path where we're saving, might have renamed zip file when uploading (if already existed)
				$name = NULL;
				$path_folders = file::directory_folders($path);
				foreach($path_folders as $path_folder) {
					if($path_folder != "__MACOSX") {
						$name = $path_folder;
						break;
					}
				}
				
				// Temporarily rename enire unzipped folder
				$path_temp = $path_parent."/__temp_".$name."_".microtime_float()."/";
				debug("Renaming ".$path." to ".$path_temp,$c[debug]);
				rename($path,$path_temp);
				
				// Move actualy unzipped content from temp to path we want
				debug("Renaming ".$path_temp.$name."/ to ".$path,$c[debug]);
				rename($path_temp.$name."/",$path);
				
				// Delete temp folder an its remaining content
				debug("Removing temp dir ".$path_temp,$c[debug]);
				file::directory_delete($path_temp);
			}
		}*/
		
		// Return
		return $return;
	}
}
?>