<?php
/**
 * @package kraken\csv
 */
 
if(!function_exists('csv2array')) {
	/**
	 * Converts given csv file or string to an array of data.
	 * 
	 * @param string $file The path to the file or a csv string you want to get the data from.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array The resulting array of data.
	 */
	function csv2array($file,$c = NULL) {
		// Config
		if(!x($c[delimiter])) $c[delimiter] = ","; // Column deliminiter
		if(!x($c[enclosure])) $c[enclosure] = '"'; // Enclosure character which goes around text fields
		if(!x($c[enclosure_escape])) $c[enclosure_escape] = "\\"; // What character escapes the enclosure. Deafaults to backslash (\) (escaped because it's next to a quote)
		if(!x($c[row])) $c[row] = ($c[rows] ? $c[rows] : "\n"); // Row deliminiter (used to be $c[rows])
		
		// Data
		$extension = strtolower(substr($file,-4));
		if(in_array($extension,array(".csv",".txt"))) $data = utf8_encode(file_get_contents($file));
		else $data = $file;
		
		// PHP method - best, but >= 5.3 // Untested
		if(function_exists('str_getcsv')) {
			$rows = str_getcsv($data,$c[row],$c[enclosure],$c[enclosure_escape]); // Parse rows
			if(count($rows) == 1) {
				$_rows = str_getcsv($data,"\r\n",$c[enclosure],$c[enclosure_escape]); // Parse rows
				if(count($_rows) > 1) $rows = $_rows;
			}
			if(is_array($rows)) {
				foreach($rows as $x => $row) $results[] = str_getcsv($row,$c[delimiter],$c[enclosure],$c[enclosure_escape]); // Parse columns in row
			}
		}
		// Silas Palmer's method - http://www.php.net/manual/en/function.str-getcsv.php#106888
		else {
			$escaped_enclosure = $c[enclosure_escape].$c[enclosure];
			$escaped_enclosure_replace = "**!!^!!**"; // A string which likely won't appear within the actual data
		   
			// Clean up file
			$data = trim($data);
			$data = str_replace("\r\n",$c[row],$data);
		   
			$data = str_replace($escaped_enclosure,$escaped_enclosure_replace,$data); // Temporarily replace escaped enclosures with random string
			$data = str_replace(','.$escaped_enclosure_replace.',',',,',$data); // Handle ,"", empty cells correctly
		 
			$data .= $c[delimiter]; // Put a comma on the end, so we parse last cell 
		
			$row = 0;
			$start_point = 0;
			$inquotes = false;
			for($i = 0;$i < strlen($data);$i++) {
				$char = $data[$i];
				if($char == $c[enclosure]) {
					if($inquotes) {
						$inquotes = false;
					}
					else {
						$inquotes = true;
					}
				}
				if(($char == $c[delimiter] or $char == $c[row]) and !$inquotes) {
					$cell = substr($data,$start_point,$i-$start_point);
					$cell = str_replace($c[enclosure],'',$cell); // Remove enclosure characters
					$cell = str_replace($escaped_enclosure_replace,$c[enclosure],$cell); // Un-escape escaped enclosures
					$results[$row][] = $cell;
					$start_point = $i + 1;
					if($char == $c[row]) {
						$row ++;
					}
				}
			}
		}
		
		// Return
		return $results;
	}
}

if(!function_exists('array2csv')) {
	/**
	 * Converts given array into an csv string which can then be saved as an csv file.					
	 * 
	 * @param array $array The array of data you want to turn into an csv string.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The resulting csv string.
	 */
	function array2csv($array,$c = NULL) {
		// Error
		if(!$array) return;
		
		// Config
		if(!x($c[delimiter])) $c[delimiter] = ",";
		if(!x($c[enclosure])) $c[enclosure] = '"';
		if(!x($c[row])) $c[row] = "\n";
		
		// String
		$string = NULL;
		
		// Build
		foreach($array as $x => $row) {
			$string_row = NULL;
			foreach($row as $k => $v) {
				$string_row .= ($string_row ? $c[delimiter] : "").$c[enclosure].csv_escape($v).$c[enclosure];
			}
			$string .= ($string ? $c[row] : "").$string_row;
		}
		
		// Return
		return $string;
	}
}

if(!function_exists('csv_escape')) {
	/**
	 * Escapes special system characters present in a string.
	 *
	 * @param string $string The string you want to escape.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The string with all special characters escaped.							
	 */	
	function csv_escape($string,$c = NULL) {
		// Config
		if(!x($c[strip])) $c[strip] = 1; // Strip tags
		
		// Strip tags
		if($c[strip] == 1) $string = strip_tags($string);
		// Remove line breaks and tabs
		$string = str_replace(array("\t","\n","\r")," ",$string); 
		// Escape double quotes (with another double quote)
		$string = str_replace('"','""',$string); 
		// Encode
		$string = mb_convert_encoding($string,'UTF-16LE','UTF-8');
		
		// Return
		return $string;
	}
}
if(!function_exists('csv_download')) {	
	/**
	 * Forces download of the given CSV string as a CSV file.
	 *
	 * @param string $string The CSV string you want to include in the download.
	 * @param string $name The name of the CSV file the user will download. Default = csv.csv
	 */
	function csv_download($string,$name = "csv.csv") {
		// Extension
		if(!file::extension($name)) $name .= ".csv";
		
		// Header
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".$name."\";" );
		header("Content-Transfer-Encoding: binary");
			
		// String
		print $string;
		
		// Exit
		exit;
	}
}
?>