<?php
/**
 * @package kraken\debug
 */
 
if(!function_exists('debug')) {
	/**
	 * Displays a debugging message produced by one of the site's scripts.
	 * 
	 * Displays the given $text message if $debug == 1, config.debug.active == true, and the user's IP address is in the config.debug.ips array.
	 * 
	 * @param string $text The message that should be displayed
	 * @param boolean $debug Whether or not we want to display the message. Might use if debugging is a configuration option, example: debug('An error occured',$c[debug]);
	 */
	function debug($text,$debug = 1) {
		if($debug) {
			if(debug_active()) {
				print $text;
				if(substr($text,-6) != "<br />") print "<br />";
			}
		}
	}
}

if(!function_exists('debug_active')) {
	/**
	 * Checks whether or not debugging is 'active' for the current user.
	 * 
	 * Checks whether config.debug.active == true and if the user's IP address is in the config.debug.ips array.
	 * 
	 * @return boolean Whether or not debugging is active for the current user.
	 */
	function debug_active() {
		global $__GLOBAL;
		if(isset($__GLOBAL['config']['debug']['active']) and isset($__GLOBAL['config']['debug']['admin'])) {
			if($__GLOBAL['config']['debug']['active'] == true and $__GLOBAL['config']['debug']['admin'] == true) return true;
			else return false;
		}
		else return false;
	}
}

if(!function_exists('function_speed')) {
	/**
	 * Called at start end end of function, records how long it takes to run
	 * 
	 * @param string $function The name of the function that's running (use __FUNCTION__ to get the name of the function you're in)
	 * @param int $r A random integer. Used so we can separate different calls of the same function.
	 * @param boolean $cached Were the results of the function cached. Used in debugging to separate cached speed/count from non-cached speed/count)
	 */
	function function_speed($function,$r = NULL,$cached = NULL) {
		if(debug_active()) {
			global $__GLOBAL;
			// Start
			if(!$r) {
				$r = ($__GLOBAL['debug']['speed']['functions_count'] ? $__GLOBAL['debug']['speed']['functions_count'] : 1);
				$__GLOBAL['debug']['speed']['functions'][$function]['calls'] += 1;
				$__GLOBAL['debug']['speed']['functions_timer'][$function][$r]['start'] = microtime_float();
				$__GLOBAL['debug']['speed']['functions_count'] += 1;
				return $r;
			}
			// End
			else {
				$time = microtime_float() - $__GLOBAL['debug']['speed']['functions_timer'][$function][$r]['start'];
				$__GLOBAL['debug']['speed']['functions'][$function]['time']['total'] += $time;
				$__GLOBAL['debug']['speed']['functions'][$function]['time']['children'] += $__GLOBAL['debug']['speed']['functions_timer'][$function]['children'];
				if($cached == 1) {
					$__GLOBAL['debug']['speed']['functions'][$function]['time']['cache'] += $time;
					$__GLOBAL['debug']['speed']['functions'][$function]['cache'] += 1;
				}
				unset($__GLOBAL['debug']['speed']['functions_timer'][$function][$r]);
				$__GLOBAL['debug']['speed']['functions_timer'][$function]['children'] = 0;
				
				foreach($__GLOBAL['debug']['speed']['functions_timer'] as $f => $calls) {
					if($function != $f) {
						foreach($calls as $x => $v) {
							// Only add 'child' time to one of the function calls
							$__GLOBAL['debug']['speed']['functions_timer'][$f]['children'] += $time;
							break;
						}
					}
				}
			}
		}
	}
}

if(!function_exists('debug_speed')) {
	/**
	 * Starts (and stops previous) speed tests											
	 * 
	 * @param string $name The name used to identify this speed check.  We'll use this both in the reporting and in clusing the speed check (optional). Default = NULL
	 * @param string $parent If this speed check is occuring within a parent speed check, pass that parent's name here. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 */
	function debug_speed($name = NULL,$parent = NULL,$c = NULL) {
		if(debug_active()) {
			// Globals
			global $__GLOBAL;
			
			// Config
			if(strlen($c[debug]) == 0) $c[debug] = 0; // Debug
			
			// Debug
			debug("name = <em>".$name."</em>",$c[debug]);
			
			// Child
			if($parent) {
				debug("function <em>".$name."</em> is a child of <em>".$parent."</em>",$c[debug]);
				// Current Speed
				if($parent == 1) $parent = end(array_keys($__GLOBAL['debug']['speed']['loading']));
				// End Current
				if($__GLOBAL['debug']['speed']['loading'][$parent]['children'][$name]) {
					debug("exists..",$c[debug]);
					if(!$__GLOBAL['debug']['speed']['loading'][$parent]['children'][$name]['end']) {
						$__GLOBAL['debug']['speed']['loading'][$parent]['children'][$name]['end'] = microtime_float();
						debug("..stopping",$c[debug]);
					}
				}
				// Stop Previous / Start Next
				else {	
					// Stop Previous
					if($__GLOBAL['debug']['speed']['loading'][$parent]['children']) {
						$previous = end(array_keys($__GLOBAL['debug']['speed']['loading'][$parent]['children']));
						debug("previous = <em>".$previous."</em>",$c[debug]);
						if($previous) {
							if(!$__GLOBAL['debug']['speed']['loading'][$parent]['children'][$previous]['end']) {
								debug("stopping <em>".$previous."</em> child",$c[debug]);
								$__GLOBAL['debug']['speed']['loading'][$parent]['children'][$previous]['end'] = microtime_float();
							}
						}
					}
					// Start Next
					if($name) {
						debug("starting <em>".$name."</em> child",$c[debug]);
						$__GLOBAL['debug']['speed']['loading'][$parent]['children'][$name] = array(
							'name' => $name,
							'start' => microtime_float()
						);
					}
				}
			}
			// Default
			else {
				// End Current
				if($__GLOBAL['debug']['speed']['loading'][$name]) {
					debug("exists, stopping",$c[debug]);
					$__GLOBAL['debug']['speed']['loading'][$name]['end'] = microtime_float();
					// Children
					if($__GLOBAL['debug']['speed']['loading'][$name]['children']) {
						foreach($__GLOBAL['debug']['speed']['loading'][$name]['children'] as $k => $v) {
							if(!$v['end']) $__GLOBAL['debug']['speed']['loading'][$name]['children'][$k]['end'] = microtime_float();
						}
					}
				}
				// Stop Previous / Start Next
				else {		
					debug("doesn't exist..",$c[debug]);
					// Stop Previous
					if($__GLOBAL['debug']['speed']['loading']) {
						$previous = end(array_keys($__GLOBAL['debug']['speed']['loading']));
						if($previous) {
							if(!$__GLOBAL['debug']['speed']['loading'][$previous]['end']) {
								debug("..stopping previous, ".$previous."..",$c[debug]);
								$__GLOBAL['debug']['speed']['loading'][$previous]['end'] = microtime_float();
								
								// Children
								if($__GLOBAL['debug']['speed']['loading'][$previous]['children']) {
									foreach($__GLOBAL['debug']['speed']['loading'][$previous]['children'] as $k => $v) {
										if(!$v['end']) $__GLOBAL['debug']['speed']['loading'][$previous]['children'][$k]['end'] = microtime_float();
									}
								}
							}
						}
					}
					// Start Next
					if($name) {
						debug("..starting ".$name."",$c[debug]);
						$__GLOBAL['debug']['speed']['loading'][$name] = array(
							'name' => $name,
							'start' => microtime_float()
						);
					}	
				}
			}
		}
	}
}

if(!function_exists('debug_loop')) {
	/**
	 * Loops through array of debugging info and retuns string for displaying it.								
	 * 
	 * @param array $array The array you want to loop through.
	 * @param string The prefix we append to each line. Mostly used when we loop within a loop.
	 * @return string A string representation of the debugging info array.
	 */	
	function debug_loop($array,$prefix = NULL) {
		foreach($array as $k => $v) {
			$p = ($prefix ? $prefix." => " : "").$k;
			if(is_array($v)) $text .= debug_loop($v,$p);
			else $text .= "
	".$p." : ".$v."<br />";
		}
		return $text;
	}
}

if(!function_exists('debug_html')) {
	/**
	 * Generates and returns the HTML for displaying the debugging info.								
	 * 
	 * @return string The HTML for displaying the debugging info.
	 */
	function debug_html() {
		// Debugging inactive
		if(!debug_active()) return;
		
		// Global
		global $__GLOBAL;

		// End speed
		debug_speed();
		
		// Speed
		if($__GLOBAL['debug']['speed']) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_speed_div',{animation:'',heading:'debug_speed_arrow'});\" class='toggle-closed' id='debug_speed_arrow'><b>Speed</b> - Page took ".round(microtime_float() - $__GLOBAL['debug']['speed']['total']['start'],5)." seconds to load</a>
	<div class='debug_div' id='debug_speed_div'>";
		
			// Function Speed
			$text .= "
		<em>Function Speed</em><br />
		<div style='padding:1px 8px;'>";
			// Add orphaned (incomplete calls) child time to function's total
			if($__GLOBAL['debug']['speed']['functions_timer']) {
				foreach($__GLOBAL['debug']['speed']['functions_timer'] as $f => $calls) {
					foreach($calls as $x => $v) {
						if($v[children] > 0) $__GLOBAL['debug']['speed']['functions'][$f]['children'] += $v[children];
					}
				}
			}
			if($__GLOBAL['debug']['speed']['functions']) {
				asort($__GLOBAL['debug']['speed']['functions']);
				foreach($__GLOBAL['debug']['speed']['functions'] as $function => $v) {
					$adjusted = $v[time][total] - $v[time][children];
					$average = $v[time][total] / $v[calls];
					$text .= "
			<span".($average > .015 ? " style='color:red;'" : "").">
				".$function.", calls: ".$v[calls].", time: ".round($v[time][total],3)." sec (average: ".round($average,3)." sec";
				//if($v[time][children] != 0) $text .= ", adjusted time: ".round($adjusted,3)." sec, children time: ".round($v[time][children],3)." sec, children percent: ".round($v[time][children] / $v[time][total],2)."%";
					if($v[cache] > 0) $text .= ", cached calls: ".$v[cache].", cached time: ".round($v[time][cache],3)." sec, cached average: ".round($v[time][cache] / $v[cache],3)." sec, non-cached average: ".($v[time][cache] && $v[calls] > $v[cache] ? round(($v[time][total] - $v[time][cache]) / ($v[calls] - $v[cache]),3) : 0)." sec";
					$text .= ")</span><br />";
				}
				//$text .=_array($__GLOBAL['debug']['speed']['functions']);
			}
			$text .= "
		</div>";
		
			if($__GLOBAL['debug']['speed']['loading']) {
				$text .= "
		<em>Loading Speed</em><br />
		<div style='padding:1px 8px;'>";
				foreach($__GLOBAL['debug']['speed']['loading'] as $name => $v) {
					if($v[time]) $t = $v[time];
					else {
						if(!$v[end]) $v[end] = microtime_float();
						$t = $v[end] - $v[start];
					}
					$time = round($t,5);
					if(strstr($time,'E-')) $time = round(0,5);
					if($v[queries]) {
						$queries = round($v[queries],5);
						if(strstr($queries,'E-')) $queries = round(0,5);
						$percent = round($v[queries] / ($t),5) * 100;
					}
					$text .= "
			<span".($time > .01 ? " style='color:red;'" : "").">".$name.": ".$time." seconds".($v[queries] ? ", queries: ".$queries." seconds (".$percent."%)" : "")."</span><br />";
					// Children
					if($v[children]) {
						$text .= "
			<div style='padding:0px 8px;'>";
						foreach($v[children] as $_name => $_v) {
							if($_v[time]) $t = $_v[time];
							else {
								if(!$_v[end]) $_v[end] = microtime_float();
								$t = $_v[end] - $_v[start];
							}
							$time = round($t,5);
							if(strstr($time,'E-')) $time = round(0,5);
							$text .= "
				<span".($time > .005 ? " style='color:red;'" : "").">".$_name.": ".$time." seconds</span><br />";
						}
						$text .= "
			</div>";
					}
				}
				$text .= "
		</div>";
			}
			$text .= "
	</div>";
		}
		unset($__GLOBAL['debug']['speed']);
		
		// Database
		if($__GLOBAL['debug']['db']['mysql']) {
			$errors = count($__GLOBAL['debug']['db']['mysql']['errors']);
			// Info
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_db_div',{animation:'',heading:'debug_db_arrow'});\" class='toggle-closed' id='debug_db_arrow'>
		<b>Database</b> -
		".$__GLOBAL['debug']['db']['mysql']['count']." ".string_pluralize("query",$__GLOBAL['debug']['db']['mysql']['count']);
			if($__GLOBAL['debug']['db']['mysql']['cached'] > 0) $text .= ", <span style='color:green;'>".$__GLOBAL['debug']['db']['mysql']['cached']." cached ".string_pluralize("query",$__GLOBAL['debug']['db']['mysql']['cached'])."</span>";
			if($__GLOBAL['debug']['db']['mysql']['slow'] > 0) $text .= ", <span style='color:red;'>".$__GLOBAL['debug']['db']['mysql']['slow']." slow ".string_pluralize("query",$__GLOBAL['debug']['db']['mysql']['slow'])."</span>";
			if($__GLOBAL['debug']['db']['mysql']['errors']) $text .= ", <span style='color:red;'>".$errors." ".string_pluralize("error",$errors)."</span>";
			$text .= " - ".round($__GLOBAL['debug']['db']['mysql']['time'],6)." sec
	</a>
	<div class='debug_div' id='debug_db_div'>";
			// Queries
			if($__GLOBAL['debug']['db']['mysql']['queries']) {
				$text .= "
		<em>Queries - ".$__GLOBAL['debug']['db']['mysql']['count']."</em>
		<div style='padding:0px 8px;'>
			".implode('<br />',$__GLOBAL['debug']['db']['mysql']['queries'])."
		</div>";
			}
			// Errors
			if($__GLOBAL['debug']['db']['mysql']['errors']) {
				$text .= "
		<em>Errors - ".$errors."</em>
		<div style='padding:0px 8px;'>
			".implode('<br />',$__GLOBAL['debug']['db']['mysql']['errors'])."
		</div>";
			}
			$text .= "
	</div>";
		}
		unset($__GLOBAL['debug']['db']);
		
		// Page
		if($__GLOBAL['page']) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_page_div',{animation:'',heading:'debug_page_arrow'});\" class='toggle-closed' id='debug_page_arrow'>
		<b>Page</b>
	</a>
	<div class='debug_div' id='debug_page_div'>
		".return_array($__GLOBAL['page'])."
	</div>";
		}
		$page = $__GLOBAL['page']; // Save since we may use later (namely in the cart debuggin)
		unset($__GLOBAL['page']);
		
		// User
		if($_SESSION['u']) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_user_div',{animation:'',heading:'debug_user_arrow'});\" class='toggle-closed' id='debug_user_arrow'>
		<b>User</b>
	</a>
	<div class='debug_div' id='debug_user_div'>
		".return_array($_SESSION['u'])."
	</div>";
		}
		
		// Cart
		if(g('config.cart.active') and $page->module) {
			if(m($page->module.'.settings.cart.active')) {
				$cart = cart::load($page->module);
				if($cart->items) {
					$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_cart_div',{animation:'',heading:'debug_cart_arrow'});\" class='toggle-closed' id='debug_cart_arrow'>
		<b>Cart</b>
	</a>
	<div class='debug_div' id='debug_cart_div'>
		".return_array($cart)."
	</div>";
				}
			}
		}
		
		// URLs
		if($_SESSION['urls']) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_urls_div',{animation:'',heading:'debug_urls_arrow'});\" class='toggle-closed' id='debug_urls_arrow'>
		<b>URLs</b>
	</a>
	<div class='debug_div' id='debug_urls_div'>
		".return_array($_SESSION['urls'])."
	</div>";
		}
		
		// Default
		if($__GLOBAL['debug']) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_default_div',{animation:'',heading:'debug_default_arrow'});\" class='toggle-closed' id='debug_default_arrow'>
		<b>Other</b>
	</a>
	<div class='debug_div' id='debug_default_div'>
		".debug_loop($__GLOBAL['debug'])."
	</div>";
		}
		unset($__GLOBAL['debug']);
		
		// Global
		/*if($__GLOBAL) {
			$text .= "
	<a href='javascript:void(0);' onclick=\"toggle('#debug_global_div',{animation:'',heading:'debug_global_arrow'});\" class='toggle-closed' id='debug_global_arrow'>
		<b>Global</b>
	</a>
	<div class='debug_div' id='debug_global_div'>
		".return_array($__GLOBAL)."
	</div>";
		}*/
		
		if($text) print "
<link rel='stylesheet' type='text/css' href='".D."core/core/css/debug.css?".filemtime(SERVER."core/core/css/debug.css")."' />
<div id='debug'>
	<b>Debugging</b>
	<div id='debug-javascript'></div>
	".$text."
</div>";
	}
}
?>