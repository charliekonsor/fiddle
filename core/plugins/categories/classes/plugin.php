<?php
if(!class_exists('plugin_categories',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the categories plugin.
	 *
	 * @package kraken\plugins\categories
	 */
	class plugin_categories extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_categories($module,$id,$plugin,$c);
		}
		function plugin_categories($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Label
			$label = plugin($this->plugin.'.single');
			if(count($inputs) > 1) $label = plugin($this->plugin.'.plural');
			// Open
			$open = 0;
				
			// Categories
			$categories = module_categories($this->module,$this->type);
			foreach($categories as $id => $row) {
				// Exists?
				$exists = 0;
				foreach($form->inputs as $input) {
					// New method
					if(is_object($input)) {
						if(($input->options == "categories" or is_array($input->options)) and $input->c[options_category] and ($input->c[options_category] == $row[m('categories.db.id')] or $input->c[options_category] == $row[m('categories.db.code')])) {
							$exists = 1;
							break;
						}
					}
					// Old method
					else {
						if($input[options] == "categories" and $input[options_category] and ($input[options_category] == $row[m('categories.db.id')] or $input[options_category] == $row[m('categories.db.code')])) {
							$exists = 1;
							break;
						}
					}
				}
				if(!$exists) {
					// Options
					$options = categories_options($id);
					if($options) {
						// Value
						$value = $this->categories($id);
						
						// Input
						$options = array_merge(array(array('label' => '')),$options); // Want blank option on the top
						$input_c = array(
							'multiple' => ($row[category_multiple] ? $row[category_multiple] : NULL),
							'validate' => array(
								'required' => $row[category_required]
							),
						);
						$inputs[] = form_input_select::load($row[m('categories.db.name')],$id,$options,$value,$input_c);
						
						// Label
						if($row[category_multiple]) $label = plugin($this->plugin.'.plural');
						// Open
						if($row[category_required]) $open = 1;
					}
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => $label,
					'inputs' => $inputs,
					'open' => $open,
				);
				return $return;
			}
		}
		
		/**
		 * Saves an item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_save($values,$c = NULL) {
			// Error
			if(!$c['isset']) return;
		
			// Delete old values
			parent::item_delete($c);
			
			// Debug
			debug(__CLASS__."->".__FUNCTION__." values: ".return_array($values),$c[debug]);
			
			// New values
			if($values) {
				// Categories
				foreach($values as $id => $categories) {
					if($categories) {
						// Category items
						$_categories = data_unserialize($categories); // Was serialized array
						if($_categories) $categories = $_categories;
						if(!is_array($categories)) $categories = array($categories); // In case it wasn't a 'multiple' input
						foreach($categories as $category) {
							// Category item
							//$category_item_id = array_last(explode('.',$category)); // Not using for now, just using the actual category item id
							$category_item_id = $category;
							
							// Values
							$row_values = array(
								'category_id' => $id,
								'category_item_id' => $category_item_id,
								//'category_item_string' => $category, // Not using for now
							);
							
							// Save
							$plugin_item = $this->plugin_item();
							$plugin_item->save($row_values,$c);
						}
					}
				}
			}
		}
		
		/**
		 * Returns array of categories the item belongs to.
		 *
		 * Can (optionally) pass a $id, which will be the category id we want to get the category items of.
		 * If not passed, we'll return all categories in the array with all the individual cateogry items as children of their parent category.
		 *
		 * @param int $id The id of a specific category we want to get the selected category items of. Default = NULL
		 * @return array An array of categories the item belongs to either within the given $id parent category (if $id) or within all categories in the item's module.
		 */
		function categories($id = NULL) {
			// Rows
			$rows = $this->rows(array('where' => ($id ? m('categories_items.db.parent_id')." = '".$id."'" : "")));
			while($row = $this->db->f($rows)) {
				if($id) $array[] = $row[category_item_id];	
				else $array[$row[category_id]][] = $row[category_item_id];	
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Tweaks the $c configuration array before querying rows of a module.
		 *
		 * @param array $c The configuration array we want to tweak in this module. Defaut = NULL
		 * @return array The tweaked configuration array.
		 */
		function module_rows($c = NULL) {
			// Config
			if(!$c[categories]) $c[categories] = NULL; // An array of categories to match for items in the result in one of several formats (see below).
			if($c[category] and !$c[categories]) $c[categories] = $c[category]; // In case we messed up and passed the wrong variable.
				
			// Have categories
			if(x($c[categories])) {
				// Debug
				debug("categories (before standardization):".return_array($c[categories]),$c[debug]);
				
				// Standardize. Want in a array(category1 => array(category1_item1, category1_item2)) format.
				$categories = NULL;
				if(!is_array($c[categories]) and $c[categories]) { // A single category item. Example: $c[categories] = 123; 
					$item = item::load('categories_items',$c[categories]);
					if($item->parent_id) {
						$categories = array($item->parent_id => array($c[categories]));
					}
				}
				else {
					foreach($c[categories] as $category => $ids) {
						if(!is_array($ids) and $ids) { // Multiple category items (from any parent category). Example: $c[categories] = array(12, 87, 209);
							$item = item::load('categories_items',$ids);
							if($item->parent_id) {
								$categories[$item->parent_id][] = $ids;
							}
						}
						else if($ids) $categories[$category] = $ids; // Already in the correct format: Example: $c[categories] = array(1 => array(12, 87, 209),6 => array(102));
					}	
				}
				$c[categories] = $categories;
				
				// Debug
				debug("categories (after standardization):".return_array($c[categories]),$c[debug]);
				
				// Join/Where
				if($categories) {
					$x = 0;
					foreach($categories as $category => $ids) {
						$ids = array_filter($ids);
						if(is_array($ids) and count($ids) > 0) {
							// Join
							$c[join] .= "
				JOIN
					`items_categories` c".$x."
						ON i.".m($this->module.'.db.id')." = c".$x.".item_id";
						
							// Where
							$c[where] .= "
					".($c[where] ? "AND " : "")."c".$x.".module_code = '".$this->module."'
					AND c".$x.".category_id = '".$category."'
					AND c".$x.".category_item_id IN (".implode(',',$ids).")";
						
							$x++;
						}
					}
					
					// Debug
					debug("c (after categories):".return_array($c),$c[debug]);
				}
			}
			
			// Return
			return $c;
		}
	}
}
?>