<?php
if(!class_exists('plugin_meta',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the meta plugin.
	 *
	 * @package kraken\plugins\meta
	 */
	class plugin_meta extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_meta($module,$id,$plugin,$c);
		}
		function plugin_meta($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Placeholders
			$placeholders = array('meta_title' => 'name','meta_description' => 'description','meta_keywords' => 'tags');
			
			// Database variables
			foreach(plugin($this->plugin.'.db_variables') as $key => $v) {
				// Name
				if($name = m($this->module.'.db.'.$key,$this->type)) {
					// Already exists?
					if(!$form->input_exists($name)) {
						// Placholder
						$placeholder = $placeholders[$key];
						
						// Input
						$inputs[] = array(
							'label' => $v[label],
							'type' => 'text',
							'name' => $name,
							'placeholder' => $this->item->db($placeholder),
							'validate' => array(
								'maxlength' => g('config.meta.maxlength'),
							),
							'id' => "__plugins_".$key."_".$r,
							'value' => $this->item->v($name)
						);
						
						// Javascript
						$placeholder = m($this->module.'.db.'.$placeholder,$this->type);
						if($placeholder) {
							$javascript .= "
<script type='text/javascript'>
$(document).ready(function() {
	pluginsMeta".$key.$r."();

	$(':input[name=".$placeholder."]').blur(function() {
		pluginsMeta".$key.$r."();
	});
});
function pluginsMeta".$key.$r."() {
	// Default value
	var value = $(':input[name=".$placeholder."]').val();
	
	// Remove tags;
	value = value.replace(/<.[^<>]*?>/g,' ');
	// Remove multiple spaces
	value = value.replace(/\s\s+/g,' ');
	// Trim
	value = $.trim(value);
	
	// Update placeholder
	$('#__plugins_".$key."_".$r."').attr('placeholder',value);
	placeholders();
}
</script>";
							$inputs[] = array(
								'type' => 'html',
								'value' => $javascript
							);
						}
					}
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Processes meta data, basically just auto-populates meta keywords if none exist.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_process($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Meta Keywords
			if(m($this->module.'.db.meta_keywords') and !$this->item->meta_keywords and g('config.meta.keywords.auto.active') == 1) {
				$array = NULL;
				// Get indexed words
				if(m($this->module.'.settings.search.index')) {
					$query = "SELECT index_word, SUM(index_score) score FROM `items_index` WHERE module_code = '".$this->module."' AND item_id = '".$this->id."' GROUP BY index_word ORDER BY score DESC LIMIT ".g('config.meta.keywords.auto.count');
					debug($query,$c[debug]);
					$rows = $this->db->q($query);
					while($row = $this->db->f($rows)) {
						$array[] = $row[index_word];	
					}
				}
				// Get manually
				else {
					# build this
				}
				
				// Save
				if($array) {
					$values = array(
						'db.meta_keywords' => implode(', ',$array)
					);
					$this->item->save($values);	
				}
			}
		}
		
		/**
		 * Returns the meta title for the item.
		 *
		 * @return string The item's meta title.
		 */
		function meta_title() {
			// Saved // Does so little, caching will just slow it down
			//if($value = $this->item->cache[meta][title]) return $value;
			
			// Get
			$value = $this->item->db('meta_title');
			if(!$value) $value = $this->item->db('name');
			//if($value) $value = string_limit($value,120); // Gets 'limited' in $page_framework->page() method.
			
			// Save // Does so little, caching will just slow it down
			//$this->item->cache[meta][title] = $value;
			
			// Return
			return $value;
		}
		
		/**
		 * Returns the meta description for the item.
		 *
		 * @return string The item's meta description.
		 */
		function meta_description() {
			// Saved // Does so little, caching will just slow it down
			//if($value = $this->item->cache[meta][description]) return $value;
			
			// Get
			$value = $this->item->db('meta_description');
			if(!$value) $value = $this->item->db('description');
			//if(!$value) $value = m('settings.settings.site.meta.description'); // Gets this default in the $page_framework->page() method.
			//if($value) $value = string_limit($value,120); // Gets 'limited' in $page_framework->page() method.
			
			// Save // Does so little, caching will just slow it down
			//$this->item->cache[meta][description] = $value;
			
			// Return
			return $value;
		}
		
		/**
		 * Returns the meta keywords for the item.
		 *
		 * @return string The item's meta keywords.
		 */
		function meta_keywords() {
			// Saved // Does so little, caching will just slow it down
			//if($value = $this->item->cache[meta][keywords]) return $value;
			
			// Get
			$value = $this->item->db('meta_keywords');
			//if(!$value) $value = $this->item->db('tags'); // This doesn't exist
			//if(!$value) $value = m('settings.settings.site.meta.keywords'); // Gets this default in the $page_framework->page() method.
			//if($value) $value = string_limit($value,120); // Gets 'limited' in $page_framework->page() method.
			
			// Save // Does so little, caching will just slow it down
			//$this->item->cache[meta][keywords] = $value;
			
			// Return
			return $value;
		}
		
		/**
		 * Returns the meta image for the item.
		 *
		 * @return string The item's meta image URL.
		 */
		function meta_image() {
			// Saved
			if($value = $this->item->cache[meta][image]) return $value;
			
			// Get
			$file = $this->item->file('db.meta_image');
			if($file) $value = $file->url();
			if(!$value) {
				$file = $this->item->file('db.image');
				if($file) $value = $file->url();
			}
			
			// Save
			$this->item->cache[meta][image] = $value;
			
			// Return
			return $value;
		}
	}
}
?>