<?php
print "
The first things you need to know about meta data is that it's optional. If you don't do anything in this section, your website will automatically determine as much meta data as it can.  This section just provides you a way to customize exactly what the meta data for this ".strtolower(m($this->module.'.single'))." is. So what exactly is meta data? Well, I'm glad you asked.<br /><br />

Meta data is a way for the machines of the world to quickly understand what the content of a page is.  For example, a search engine will use the meta data of a page (as well as the actual content) to index search terms, will use your meta title as the text of the link to a page in their search results, and may even use the meta description as the description that appears in the search result for this page. As such, meta data is fairly important to Search Engine Optimization (SEO).<br /><br />

The meta data may also be used by social sites (Facebook, etc.) when you 'share' a link to this this ".strtolower(m($this->module.'.single'))." on those sites.  The meta title is also what's displayed in the header of your internet browser.<br /><br />

Now that we've discussed a little about its importance, let's figure out how we add this meta data.<br /><br />

<em>Meta sidebar section</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar.png' alt='' /><br /><br />

<h5>Meta Title</h5>
The meta title is the most important of the meta information.  It's what the browser displays in the heaader (as the 'Title' of the page) when you visit this ".strtolower(m($this->module.'.single')).". It's also used by search engines, social sites, and lots of other machines of the world as the 'title' of this page.<br /><br />

As we said earlier, all the meta data is optional. If you leave the Meta Title field blank, we'll automatically create a Meta Title for this ".strtolower(m($this->module.'.single')).". Usually this is the 'Name' of your ".strtolower(m($this->module.'.single')).".  For example, if you were to type 'Hello World' as the 'Name' of this ".strtolower(m($this->module.'.single')).", you'd see that value used as the default meta title:<br /><br />

<em>Default meta title</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar-title-default.png' alt='' /><br /><br />

But, if you want to change the meta title of this ".strtolower(m($this->module.'.single')).", you'd simply enter your custom meta title here.<br /><br />

Note, your meta title must be ".g('config.meta.maxlength')." characters or less. We do this because search engines don't like longer meta titles. They'll think you're trying to trick them by filling your title with lots of keywords. And they don't like that. <br /><br />

<h5>Meta Description</h5>
Your meta description <em>may</em> be used by other websies (search engines, social sites, etc.) as the description of this ".strtolower(m($this->module.'.single')).".  However, there's no guarantee. Google, for example, now largely comes up with its own description to use when displaying this page in its search results. Having a meta descrpition, though, can only help them (and all the other machines of the inter webs) figure out exactly what this page is about.<br /><br />

As with the Meta Title (and Meta Keywords), this field is optional.  If you leave it blank, the website will do its best to automtically generate a meta description for this ".strtolower(m($this->module.'.single')).".  This field in the sidebar, though, gives you the opportunity to specifically define what the meta description for this ".strtolower(m($this->module.'.single'))." is.<br /><br />

Once again, it's restriced to ".g('config.meta.maxlength')." characters or less because search engines don't like anything longer than that.<br /><br />

<h5>Meta Keywords</h5>
Once again this field is optional (as are all the other meta fields).  In fact, this is probably the least important of them.  Search engines are using the keywords meta data less and less and are instead using their fancy algorithms to spider the content of a page to figure out appropriate keywords. That being said, having keywords definitley will never hurt and can often help.<br /><br />

Keywords are a collection of comma separated words or phrases.  You should have a maximum of 15 keywords and, once again, the total length of your keywords should be ".g('config.meta.maxlength')." characters or less.";

?>