<?php
if(!class_exists('plugin_code',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the code plugin.
	 *
	 * @package kraken\plugins\code
	 */
	class plugin_code extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_code($module,$id,$plugin,$c);
		}
		function plugin_code($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			if(u('type') == "supers" and $name = m($this->module.'.db.code')) {
				if(!$form->input_exists($name)) {
					// Input
					$inputs[] = array(
						'label' => 'Code',
						'type' => 'text',
						'name' => $name,
					);
					
					// Return
					$return = array(
						'label' => plugin($this->plugin.'.name'),
						'inputs' => $inputs
					);
					return $return;
				}
			}
		}
	}
}
?>