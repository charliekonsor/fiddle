<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'URL',
	'single' => 'URL',
	'plural' => 'URLs',
	'icon' => 'link',
	'db' => array(
		'table' => 'items_urls',
		'id' => 'id',
		'parent_module' => 'item_module',
		'parent_id' => 'item_id',
		'visible' => 'url_visible',
		'created' => 'url_created',
		'updated' => 'url_updated'
	),
	/*'forms' => array(
		'sidebar' => array(
			'inputs' => array(
				array(
					'label' => 'Link',
					'type' => 'text',
					'name' => 'url_name',
					'validate' => array(
						'required' => 1,
					),
				)
			)
		)
	)*/
);
?>