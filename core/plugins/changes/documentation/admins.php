<?php
print "
".$this->plugin." plugindocumentation<br /><br />

Note, since a form will never contain this unless it has an $id, and the documentation for plugins checks a module level form (aka no $id) this will never appear in the docs.";

?>