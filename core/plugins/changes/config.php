<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'name' => 'Changelog',
	'single' => 'Change',
	'plural' => 'Changes',
	'icon' => 'change',
	'db' => array(
		'table' => 'items_changes',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'created' => 'change_created',
		'updated' => 'change_updated'
	)
);
?>