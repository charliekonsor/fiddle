<?php
// Error
if(!$this->module or !$this->id or !$this->plugin) return;

// Config
if(!$page) $page = ($_GET['p'] ? $_GET['p'] : 1); // Page number
if(!$perpage) $perpage = 10; // How many comments to show per page
if(!x($form)) $form = 1; // Include the form for posting a new comment
if(!$container) $container = "comments-container-".$this->module."-".$this->id."-".mt_rand();

// Form
if($form) {
	print "
<div class='comments-form-container'>";
	if($this->permission('add')) {
		$form_class = $this->plugin_class->form(array('container' => "#".$container));
		print $form_class->render();
	}
	else print "
	<div class='core-none comments-form-login'>
		".(u('id') ? "You don't have permission to add ".strtolower($this->plugin_class->v('plural'))."." : "You must be logged in to do this.")."
		".$this->login_form()."
	</div>";
	print "
</div>";
}

// Results
$row_count = 0;
print "
<div class='comments-container' id='".$container."'>";

// Have comments? Defining the db.comments_count in a module will speed up comment display.
if(!$this->module_class->v('db.comments_count') or $this->item_class->db('comments_count')) {
	// Pagination
	$query_c = array(
		'db.visible' => 1,
	);
	$rows_count = $this->plugin_class->rows_count($query_c);
	$query_c[order] = plugin($this->plugin.'.db.created')." DESC";
	$query_c[limit] = pagination_limit($rows_count,$perpage,array('page' => $page));

	// Results
	$rows = $this->plugin_class->rows($query_c);
	if($this->db->n($rows)) {
		while($row = $this->db->f($rows)) {
			$plugin_item = $this->plugin_class->plugin_item($row);
			/*print "
		".$this->load->view(array('module' => $this->module,'id' => $this->id,'plugin' => $this->plugin,'plugin_id' => $plugin_item->db('id'),'view' => 'comment'));*/
			print "
		".$plugin_item->view('item');
		}
	}
	else print "
	<div class='core-none'>No ".$this->plugin_class->v('plural')." Yet</div>";
}

print "
</div>";
			
// Pages
if($rows_count > $perpage) print "
<div class='comments-pages'>
	".pagination_html($rows_count,$perpage)."
</div>";