<?php
if(!class_exists('plugin_item_comments',false)) {
	/**
	 * An extension of the plugin_item class with functionality specific to the 'comments' plugin.
	 *
	 * @package kraken\plugins\comments
	 */
	class plugin_item_comments extends plugin_item {
		/**
		 * Constructs the class.
		 *
		 * @param string $plugin The plugin we're using.
		 * @param int $plugin_id The id of the item in the plugin we're acting upon.
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id,$plugin,$plugin_id,$c = NULL) {
			$this->plugin_item_comments($module,$id,$plugin,$plugin_id,$c);
		}
		function plugin_item_comments($module,$id,$plugin,$plugin_id,$c = NULL) {
			// Parent
			parent::__construct($module,$id,$plugin,$plugin_id,$c);
		}
		
		/**
		 * Saves the given values for the current plugin item.
		 *
		 * @param array $values An array of values you want to save.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the saved plugin item.
		 */
		function save($values,$c = NULL) {
			// Parent
			$return = parent::save($values,$c);
			
			if($return) {
				// Plugin class - since we extend plugin_item, which extends plugin (not plugin_comments where the count() method resides), must create plugin_comments class
				$plugin_class = plugin::load($this->module,$this->id,$this->plugin);
				// Count
				$plugin_class->count();	
			}
			
			// Return
			return $return;
		}
		
		/**
		 * Deletes plugin row for the current plugin item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function delete($c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin or !$this->plugin_id) return;
			
			// Parent
			parent::delete($c);
			
			// Plugin class - since we extend plugin_item, which extends plugin (not plugin_comments where the count() method resides), must create plugin_comments class
			$plugin_class = plugin::load($this->module,$this->id,$this->plugin);
			// Count
			$plugin_class->count();
		}
		
		/**
		 * Returns the form class object for editing this plugin item.
		 *
		 * This extends plugin_item which extends plugin (not plugin_comments) so must take control of it here.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The form class object with fields/saved values pre-added in.
		 */
		function form($c = NULL) {
			// Plugin
			$plugin = plugin::load($this->module,$this->id,$this->plugin);
			
			// Config
			if(!$c[id]) $c[id] = "comments-form-".mt_rand();
			if(!$c[action]) {
				$on_success = "
function(result,form,c) {
	$('".$c[container]."').replaceWith(result);
	debug('result: <xmp>'+result+'</xmp>');
}";
				$c[action] = "javascript:save('".$c[id]."',{on_success:".string_strip_breaks($on_success)."});";
			}
			
			// Parent
			$form = $plugin->form($c);
			
			// Saved values
			if($form->inputs) {
				// Row
				if(!$this->row) $this->row();
				
				// Values we want to overwrite with saved values
				$overwrite = array(
					plugin($this->plugin.'.db.parent_module'),
					plugin($this->plugin.'.db.parent_id'),
					plugin($this->plugin.'.db.user'),
				);
				
				// Saved values
				foreach($form->inputs as $k => $v) {
					// New method
					if(is_object($v)) {
						if($v->name and (!x($v->value) or in_array($v->name,$overwrite))) {
							$form->inputs[$k]->value = $this->row[$v->name];	
						}
					}
					// Old method
					else {
						if($v[name] and (!x($v[value]) or in_array($v[name],$overwrite))) {
							$form->inputs[$k][value] = $this->row[$v[name]];	
						}
					}
				}
			}
				
			// Saved id
			$form->hidden('id',$this->plugin_id,array('process' => 0));
			
			// Return
			return $form;
		}
	}
}
?>