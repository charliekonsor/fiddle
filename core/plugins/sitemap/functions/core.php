<?php
/**
 * @package kraken\modules\sitemap
 */

if(!function_exists('sitemap_array')) {
	/**
	 * Returns array of items included in the sitemap									
	 * 
	 * @param string $module The module you want to get the sitemap array of. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return array An array of all the sitemap items.
	 */
	function sitemap_array($module = NULL,$c = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Globals
		global $sitemap_mapped;
		
		// Config
		if(!x($c[nest])) $c[nest] = 1; // Nesting places item links as children of item's module, if no nesting all links on one level in array (good for xml)
		if(!x($c[cached])) $c[cached] = 1; // Use cached data (if available)
		if(!x($c[cache])) $c[cache] = 1; // Save data to cache
		if(!x($c[cache_life])) $c[cache_life] = 86400; // Length (in seconds) of cache (86400 = 24 hours)
		if(!x($c[cache_name])) $c[cache_name] = "sitemap/array".($module ? "/".$module : ""); // Name to use when caching
		if(!x($c[debug])) $c[debug] = 0; // Debug
	
		// Debug
		debug("<b>sitemap_array($module);</b>",$c[debug]);
		debug("c:".return_array($c),$c[debug]);
	
		// Cached
		if($c[cached] == 1) {
			$c[cache_name] .= "/".cache_config($c); // Append config string to cache name
			if($array = cache_get($c[cache_name],array('life' => $c[cache_life]))) {
				// Speed
				//function_speed(__FUNCTION__,$f_r,1);
				
				// Return
				return $array;
			}
		}
		
		// Variables
		$sitemap_mapped = NULL;
		$array = array();
		
		// Home
		if(!$module) {
			$array[] = array(
				'url' => DOMAIN,
				'name' => 'Home',
				'change' => 'daily',
				'priority' => '0.8'
			);
			$sitemap_mapped[DOMAIN] = 1;
		}
		
		// Modules
		foreach(m() as $code => $v) {
			if(!$module or $module == $code) {
				if($v[plugins][sitemap]) {
					$views = $v[settings][sitemap][views];
					debug("<em>".$code."</em>",$c[debug]);
					if($views) {
						debug("this module wants data in the sitemap. views:".return_array($views),$c[debug]);
						$m = module::load($code);
						$children = NULL;
						
						// Module views
						foreach($views as $view) {
							if($view != "item" and $view != "home") {
								debug("adding ".$view." view to sitemap array",$c[debug]);
								$children[] = array(
									'url' => DOMAIN.$code."/".$view,
									'name' => m($code.'.public.views.'.$view),
									'module' => $code
								);
							}
						}
						// Module Items
						if(in_array('item',$views)) {
							// Make sure less than 10000 rows (incase we forgot to turn off sitemap for large module) // Maybe make the 10000 value a global setting
							$count = $m->rows_count(array('db.visible' => 1));
							debug("count: ".$count,$c[debug]);
							if($count <= 10000) {
								debug("adding items to sitemap array",$c[debug]);
								$rows = $m->rows(array('db.visible' => 1,'order' => (m($code.'.db.name') ? m($code.'.db.name')." ASC, " : "").m($code.'.db.id')." DESC"));
								while($row = $m->db->f($rows)) {
									$item = item::load($code,$row);
									$url = $item->url();
									if($url and !$sitemap_mapped[$url]) {
										$children[] = array(
											'url' => $item->url(),
											'name' => ($item->name ? $item->name : m($code.'.single')." #".$item->id),
											'module' => $code,
											'id' => $item->id
										);
										$sitemap_mapped[$url] = 1;
									}
								}
							}
						}
						
						// Module Home (do last because we first need to get 'children')
						$home = 0;
						if(in_array('home',$views)) {
							debug("want to add home page, checking url..",$c[debug]);
							$url = $m->url();
							if($url and !$sitemap_mapped[$url]) {
								debug("adding home page to sitemap array",$c[debug]);
								$array[] = array(
									'url' => $url,
									'name' => $v[plural],
									'module' => $code,
									'children' => ($c[nest] == 1 ? $children : "")
								);
								$home = 1;
								$sitemap_mapped[$url] = 1;
							}
						}
						
						// No home page or no nesting (want all items on one level)
						if(($home == 0 or $c[nest] == 0) and $children) {
							// No nesting (or 'content' module, aka no 'home' do be child of)
							if($code == "content" or $c[nest] == 0) $array = array_merge($array,$children);
							// Child items
							else $array[] = array(
								'name' => $v[plural],
								'module' => $code,
								'children' => $children
							);
						}
					}
				}	
			}
		}
		
		// Cache
		if($c[cache] == 1) cache_save($c[cache_name],$array);
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $array;
	}
}

if(!function_exists('sitemap_array_categories')) {
	/**
	 * Returns array of categories for inclusion in the sitemap array					
	 * 
	 * @param string $module The module you're getting the categories in. Default = NULL
	 * @param int $parent The parent category you want to get the children of. Default = 0
	 * @return array An array of the categories within the module.
	 */
	function sitemap_array_categories($module = NULL,$parent = 0) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Globals
		global $sitemap_mapped;
		
		$sql = q("SELECT * FROM categories WHERE ".($parent ? "parent_id = '".$parent."'" : "module_id LIKE '%|".m($module.'.id')."|%'")." AND category_active = 1 ORDER BY category_order ASC",array('cache' => 0));
		if(n($sql)) {
			while($qry = f($sql)) {
				$url = DOMAIN.$module."/category/".$qry[category_id].SITE_EXT;
				if($url and !$sitemap_mapped[$url]) {
					if($qry[parent_id] and $qry[category_heading] == 0) {
						$array[] = array(
							'url' => $url,
							'name' => s($qry[category_name]),
							'module' => $module,
							'children' => sitemap_array_categories($module,$qry[category_id])
						);
					}
					$sitemap_mapped[$url] = 1;
				}
			}
		}
	
		// Speed
		//function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $array;
	}
}

if(!function_exists('sitemap_xml')) {
	/**
	 * Returns XML for displaying sitemap based on given array of sitemap items			
	 * 
	 * @param string $module. The module you want to get the sitemap XML of. Default = NULL
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The XML of the sitemap.
	 */
	function sitemap_xml($module = NULL,$c = NULL) {
		// Speed
		//$f_r = function_speed(__FUNCTION__);
		
		// Config
		if(!x($c['array'])) $c['array'] = NULL; // Can pass sitemap array (if none passed, call sitemap_array())
		if(!x($c[index])) $c[index] = 1; // Index sitemaps (if more than $c[index_perpage] entries)
		if(!x($c[index_perpage])) $c[index_perpage] = 10000; // Number of entries per index
		if(!x($c[image])) $c[image] = 0; // Include image elements
		if(!x($c[image_location])) $c[image_location] = 0; // Include image location in image element (resource heavy_
		if(!x($c[video])) $c[video] = 0; // Include video elements
		if(!x($c[geo])) $c[geo] = 0; // Include geo elements (not yet supported)
		if(!x($c[cache])) $c[cache] = 1; // Use/store data in cache
		if(!x($c[cache_life])) $c[cache_life] = 86400; // Length (in seconds) of cache (86400 = 24 hours)
		if(!x($c[cache_name])) $c[cache_name] = "sitemap/xml".($module ? "/".$module : ""); // Name to use when caching
		$c[cache_name] .= "/".cache_config($c); // Append config string to cache name
		
		// Counters
		if(!$sitemap_counter) $sitemap_counter = 0; // Entry counter
		if(!$sitemap_page) $sitemap_page = 1; // Page/index counter
		if(!$sitemap_index) $sitemap_index = NULL; // Variable to hold indexed sitemap
		
		// Cached
		if($c[cache] == 1) {
			if($xml = cache_get($c[cache_name],array('life' => $c[cache_life]))) {
				//function_speed(__FUNCTION__,$f_r,1);
				return $xml;
			}
		}
		
		// Array
		if($c['array']) $array = $c['array'];
		else $array = sitemap_array($module,array('nest' => 0));
		
		// Entries
		if(count($array) > 0) {
			foreach($array as $k => $v) {
				if($v[url]) {
					$item = NULL;
					$xml .= "
	<url>";
					// URL
					$xml .= "
		<loc>".$v[url]."</loc>";
					// Image
					if($c[image] == 1) {
						if($v[module] and $v[id] and m($v[module],'db.photo')) {
							$module = $v[module];
							if(!$item) $item = item::load($module,$v[id],array('cache' => 0));
							$file = $item->file('db.photo',array('default' => 0));
							$file_url = $file->url();
							if($file_url) {
								//if($c[image_location]) $location = $file->location(); // Doesn't exist yet
								$xml .= "
		<image:image>
			<image:loc>".escape_xml($file_url)."</image:loc>";
								if($item->name) $xml .= "
			<image:title>".escape_xml($item->name)."</image:title>";
								if($item->description) $xml .= "
			<image:caption>".escape_xml($item->description)."</image:caption>";
								if($c[image_location] and $location) $xml .= "
			<image:geo_location>".escape_xml($location)."</image:geo_location>";
								$xml .= "
		</image:image>";
							}
						}
					}
					// Video
					if($c[video] == 1) {
						if($v[module] and $v[id] and m($v[module],'db.video')) {
							$module = $v[module];
							if(!$item) $item = item::load($module,$v[id],array('cache' => 0));
							$file = $item->file('db.video',array('default' => 0));
							$file_url = $file->url();
							if($file_url) {
								$user = item::load('users',$item->user,array('cache' => 0));
								//$thumb = $file->thumb('m'); // Not built yet
								//$video_swf = $item->video('db.video',array('return' => 'swf')); // Not built yet
								$xml .= '
		<video:video>';
								if($thumb) $xml .= '
			<video:thumbnail_loc>'.escape_xml($thumb).'</video:thumbnail_loc>';
								$xml .= '
			<video:title>'.string_limit(escape_xml($item->name),100).'</video:title>
			<video:description>'.string_limit(escape_xml(($item->description ? $item->description : $item->name)),1024).'</video:description>
			<video:content_loc>'.escape_xml($file_url).'</video:content_loc>';
								if($video_swf) $xml .= '
			<video:player_loc allow_embed="yes" autoplay="ap=1">'.escape_xml(($video_swf ? $video_swf : $file_url)).'</video:player_loc>';
								/*if($value = $file->length()) $xml .= '
			<video:duration>'.escape_xml($value).'</video:duration>'; // Net yet built
								if($item->published_end > 0) $xml .= '
			<video:expiration_date>'.time_format($item->published_end,'atom').'</video:expiration_date>'; // No longer a core db variable
								if($item->rating) { // Not yet a core db variable
									$rating = round(($item->rating * 5) / m($module.'.ratings.stars'),1); // Must be out of 5, only 1 decimal
									$xml .= '
			<video:rating>'.escape_xml($rating).'</video:rating>';
								}
								if($item->views) $xml .= '
			<video:view_count>'.escape_xml($item->views).'</video:view_count>'; // Not yet a core db variable
								if($item->published_start > 0) $xml .= '
			<video:publication_date>'.t($item->published_start,'atom').'</video:publication_date>';*/ // No longer a core db variable
								/*$sql9 = q("SELECT * FROM items_tags WHERE item_id = '".$item->id."' AND module_id = '".m($module.'.id')."' LIMIT 32"); // Not yet built
								while($qry9 = f($sql9)) $xml .= '
			<video:tag>'.escape_xml($qry9[tag]).'</video:tag>';
								if($item->category) {
									$cat = string_array_first_item($item->category,1);
									if($cat) {
										$category_item = item::load('categories',$cat,array('cache' => 0));
										$xml .= '
			<video:category>'.string_limit(escape_xml($category_item->name),256).'</video:category>';
									}
								}*/
								$xml .= '
			<video:family_friendly>yes</video:family_friendly>';
								if($user->name) $xml .= '
			<video:uploader'.($user->url() ? ' info="'.$user->url().'"' : '').'>'.escape_xml($user->name).'</video:uploader>';
								$xml .= '
		</video:video>';
							}
						}
					}
					// Geo
					if($c[geo] == 1) {
						#not built yet
					}
					// Change frequency
					$xml .= "
		<changefreq>".($v[change] ? $v[change] : "weekly")."</changefreq>";
					// Priority
					if($v[priority]) $xml .= "
		<priority>".$v[priority]."</priority>";
					$xml .= "
	</url>";
					$sitemap_counter++;
					
					// Index
					if($sitemap_counter > $c[index_perpage] and $c[index] == 1) {
						// Save index
						$xml = sitemap_xml_container($xml,$c);
						$index_url = DOMAIN."core/cache/".$c[cache_name]."-".$sitemap_page.".xml";
						file_put_contents(str_replace(DOMAIN,SERVER,$index_url),$xml);
						
						// Start index
						if(!$sitemap_index) $sitemap_index = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
						// Index entry
						$sitemap_index .= "
	<sitemap>
		<loc>".$index_url."</loc>
	</sitemap>";
						
						// Counters
						$sitemap_counter = 0;
						$sitemap_page++;
						$xml = NULL;
					}
				}
			}
		}
		
		// Indexed
		if($sitemap_index) {
			// End last index
			if($xml) {
				$xml = sitemap_xml_container($xml,$c);
				$index_url = DOMAIN."core/cache/".$c[cache_name]."-".$sitemap_page.".xml";
				file_put_contents(str_replace(DOMAIN,SERVER,$index_url),$xml);
				// Index entry
				$sitemap_index .= "
	<sitemap>
		<loc>".$index_url."</loc>
	</sitemap>";
			}
			$sitemap_index .= "
</sitemapindex>";
			
			// Return
			$return = $sitemap_index;
		}
		// No Index
		else $return = sitemap_xml_container($xml,$c);
		
		// Cache
		if($c[cache] == 1) cache_save($c[cache_name],$return);
		
		// Speed
		//function_speed(__FUNCTION__,$f_r);
		
		// Return
		return $return;
	}
}

if(!function_exists('sitemap_xml_container')) {
	/**
	 * Wraps sitemap xml in appropriate tags											
	 * 
	 * @param string $xml The XML string you want to wrap in XML tags.
	 * @param array $c An array of configuration values. Default = NULL
	 * @return string The XML wrapped up properly.
	 */
	function sitemap_xml_container($xml) {
		$text = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'.($c[image] ? ' xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"' : '').($c[video] ? ' xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"' : '').' xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'.$xml.'
</urlset>';
		return $text;
	}
}
?>