<?php
$module_class = module::load($this->module);
$single = strtolower($module_class->v('single'));

print "
The 'Permissions' section in the sidebar lets us define precisely who has access to view this ".$single.".<br /><br />

<em>Permissions sidebar</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar.png' alt='' /><br /><br />

<h5>Users</h5>
The <b>Users</b> field let us define what user 'types' can view this ".$single.".  If we don't check any boxes (which is how is will be by default) then all user types can view the ".$single.".  However, if we were to check the 'Admins' box, but leave 'Guests' blank, then only Admin users would be able to view this ".$single." and all other users would see an error saying they don't have permission to view this.<br /><br />

<h5>Password</h5>
We can also password protect this ".$single." by simply entering the needed password into the <b>Password</b> field. The first time a user visits this ".$single." then, they'd be asked to enter the password before they could view it.";
?>