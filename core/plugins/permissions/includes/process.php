<?php
/************************************************************************************/
/********************************** page_password ***********************************/
/************************************************************************************/
if($_POST['process_action'] == "page_password") {
	if($_POST['module'] and $_POST['id']) {
		// Plugin
		$plugin = plugin::load($_POST['module'],$_POST['id'],'permissions');
		// Row
		$row = $plugin->db->f($plugin->rows());
		// Plugin item
		$plugin_item = $plugin->plugin_item($row);
		
		// Check password
		if(!$plugin_item->v('permission_password') or trim($_POST['password']) == $plugin_item->v('permission_password')) {
			// Correct, save to session
			$_SESSION['passwords'][$_POST['module']][$_POST['id']] = 1; // Having trouble with sessions on occassion, using cache instead
			cache_save('plugins/passwords/'.md5(IP.$_SERVER['HTTP_USER_AGENT']).'/'.$_POST['module'].'/'.$_POST['id'],1);
		}
		// Incorrect, error
		else {
			page_error("I'm sorry, but the password you entered was incorrect.");	
		}
	}
	
	// Redirect
	$redirect = ($_POST['redirect'] ? $_POST['redirect'] : DOMAIN);
	redirect($redirect);
}
?>