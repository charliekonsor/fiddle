<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Permissions',
	'single' => 'Permission',
	'plural' => 'Permissions',
	'icon' => 'lock',
	'db' => array(
		'table' => 'items_permissions',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
	)
);
?>