<?php
if(!class_exists('plugin_permissions',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the permissions plugin.
	 *
	 * @package kraken\plugins\permissions
	 */
	class plugin_permissions extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_permissions($module,$id,$plugin,$c);
		}
		function plugin_permissions($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Saved
			if(plugin($this->plugin.'.db.table') and plugin($this->plugin.'.db.parent_module') and plugin($this->plugin.'.db.parent_id')) {
				$row = $this->db->f("SELECT * FROM ".plugin($this->plugin.'.db.table')." WHERE ".plugin($this->plugin.'.db.parent_module')." = '".$this->module."' AND ".plugin($this->plugin.'.db.parent_id')." = '".$this->id."'"); 
			}
			
			// Users
			if(m('users.types')) {
				$options = NULL;
				foreach(m('users.types') as $k => $v) {
					if($k != "supers") $options[$k] = $v[name];
				}
				if($options) {
					$inputs[] = array(
						'label' => 'Users',
						'type' => 'checkbox',
						'name' => 'permission_users',
						'options' => $options,
						'help' => "Do you want to restrict access to this ".strtolower(m($this->module.'.single'))." to specific user types? If so, check the boxes next to the user types you want to be able to see this.<br /><br />If none of the boxes are checked all user types will be able to see this.",
						'value' => data_unserialize($row[permission_users])
					);
				}
			}
			// Password
			$inputs[] = array( // Because Chrome autofills password, even afer you clear it, we need to add this fake, hidden field before the actual input so it will autofill it instead: http://benjaminjshore.info/2014/05/chrome-auto-fill-honey-pot-hack.html
				'type' => 'password',
				'name' => 'permission_password_hidden',
				'row' => array(
					'style' => 'display:none;',
				),
				'autocomplete' => 'off',
			);
			$inputs[] = array(
				'label' => 'Password',
				'type' => 'password',
				'name' => 'permission_password',
				'help' => "Do you want to password protect this so that users have to enter a password before they can view it?  If so, enter the required password here.",
				'value' => $row[permission_password],
				'autocomplete' => 'off',
			);
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Saves an item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_save($values,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Config
			if(!x($c['isset'])) $c['isset'] = 0; // This plugin was set in the values array, even if empty. Meaning the option to add values was present and, if none are present, we may want to delete existing ones.
			if(!x($c[debug])) $c[debug] = 1; // Debug
			
			// No values - empty $values array so we will delete the row (if it existed)
			if(!array_filter($values)) $values = NULL;
			
			// Values?
			$values = $this->save_values($values);
			if($values) {
				// Exists?
				if(self::v('db.table') and self::v('db.parent_module') and self::v('db.parent_id')) {
					$row = $this->db->f("SELECT * FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'"); 
				}
				
				// Save
				$plugin_item = $this->plugin_item($row);
				$id = $plugin_item->save($values,$c);
			}
			// No values, delete
			else if($c['isset']) {
				$query = "DELETE FROM ".self::v('db.table')." WHERE ".self::v('db.parent_module')." = '".$this->module."' AND ".self::v('db.parent_id')." = '".$this->id."'";
				debug($query,$c[debug]);
				$this->db->q($query); 
			}
		}
		
		/**
		 * Determines whether or not the current user has permission to perform the given action.
		 * 
		 * @param string $action The action they would be performing.
		 * @param boolean|string $message Whether or not you want to display an error message if they don't have permission. 0 = no, 1 = yes, use default message, string = yes, use the $message string.
		 * @return boolean Whether or not they have perimssion to perform the action.
		 */
		function item_permission($action,$message = 0) {
			// Defaults to true
			$permission = 1;
			
			// Row
			$row = $this->db->f($this->rows());
			// Item
			$plugin_item = $this->plugin_item($row);
			if($plugin_item->id) {
				// User type
				if($plugin_item->v('permission_users') and u('type') != "supers") {
					$types = data_unserialize($plugin_item->v('permission_users'));
					if($types and !in_array(u('type'),$types)) $permission = 0;
				}
				// Password protected
				if($permission and $plugin_item->v('permission_password')) {
					// Already inputted password?
					//if(!$_SESSION['passwords'][$this->module][$this->id]) { // Having trouble with sessions on occassion, using cache instead
					if(!cache_get('plugins/passwords/'.md5(IP.$_SERVER['HTTP_USER_AGENT']).'/'.$this->module.'/'.$this->id)) {
						if($message) {
							$form = new form();
							$form->password('Password','password');
							$form->submit('Submit');
							$form->hidden('process_plugin',$this->plugin);
							$form->hidden('process_action','page_password');
							$form->hidden('module',$this->module);
							$form->hidden('id',$this->id);
							$form->hidden('redirect',URL);
							$message = "
	<div class='page-password'>
	This page is password protected.  Please fill in the password to view the content.<br /><br />
	
	".$form->render()."
	</div>";
						}
						
						$permission = array(
							'permission' => 0,
							'message' => $message
						);
					}
				}
			}
			
			// Return
			return $permission;
		}
	}
}
?>