<?php
/**
 * An extenion of the plugin class which handles functionality specific to the changes plugin.
 */
if(!class_exists('plugin_trash',false)) {
	class plugin_trash extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_trash($module,$id,$plugin,$c);
		}
		function plugin_trash($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}

		/**
		 * Adds an item to the global trash.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array containing the 'id' of the trash row. Example: array('id' => '123').
		 */
		function item_delete($c = NULL) {
			if($this->module and $this->id > 0) {
				// Config
				if(!x($c[trash])) $c[trash] = NULL; // Can use this to force (1) or prevent (0) trashing. Otherwise we'll use the module/plugin setting (trash => auto).
				if(!$c[reason]) $c[reason] = 'delete'; // Reason trashed
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Want to trash?
				$trash = $c[trash];
				if(!x($trash)) $trash = m($this->module.'.settings.trash.auto',$this->item->type);
				if($trash) {
					// Data
					$data = $this->item->row();
					
					// Query
					$query = "INSERT INTO items_trash SET module_code = '".$this->module."', item_id = '".$this->id."', data = '".data_serialize($data)."', trash_created = '".time()."', trash_updated = '".time()."', user_id = '".u('id')."', user_ip = '".IP."', trash_reason = '".a($c[reason])."'";
					debug($query,$c[debug]);
					
					// Trash
					$id = $this->db->q($query);
				
					// Cascade - a setting only in the 'users' module which will delete all content created by the user
					if($this->module == "users" and m($this->module.'.settings.trash.cascade',$this->type)) {
						// Modules
						foreach(m() as $module => $v) {
							if($v[plugins][trash] and $v[db][user] and $module != $this->module) {
								$rows = $this->db->q("SELECT * FROM ".$v[db][table]." WHERE ".$v[db][user]." = '".$this->id."'");
								while($row = $this->db->f($rows)) {
									$item = item::load($module,$row);
									$item->delete(array('trash' => 1,'reason' => $this->module.'->'.$c[reason]));
								}
							}
						}
						
						// Plugins - note, these are plugin items that the user created, not plugin items attached to items they created
						foreach(plugins() as $plugin => $v) {
							if($v[plugins][trash] and $v[db][user]) {
								$rows = $this->db->q("SELECT * FROM ".$v[db][table]." WHERE ".$v[db][user]." = '".$this->id."'");
								while($row = $this->db->f($rows)) {
									$plugin_item = plugin_item::load($module,$row[$v[db][parent_module]],$row[$v[db][parent_id]],$row);
									$plugin_item->delete(array('trash' => 1,'reason' => $this->module.'->'.$c[reason]));
								}
							}
						}
					}
					
					// Return
					return array('id' => $id);
				}
			}
		}

		/**
		 * Adds a plugin item to the global trash.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array containing the 'id' of the trash row. Example: array('id' => '123').
		 */
		function plugin_item_delete($c = NULL) {
			if($this->module and $this->id > 0 and $this->c[parent_plugin] and $this->c[parent_plugin_id]) {
				// Config
				if(!x($c[trash])) $c[trash] = NULL; // Can use this to force (1) or prevent (0) trashing. Otherwise we'll use the module/plugin setting (trash => auto).
				if(!$c[reason]) $c[reason] = 'delete'; // Reason trashed
				if(!x($c[debug])) $c[debug] = 0; // Debug
				
				// Want to trash?
				$trash = $c[trash];
				if(!x($trash)) {
					# should really check for module specific plugin setting, but can't think of how that would be done right now
					$trash = plugin($this->c[parent_plugin].'.settings.trash.auto');
				}
				if($trash) {
					// Data
					$plugin_item = plugin_item::load($this->module,$this->id,$this->c[parent_plugin],$this->c[parent_plugin_id]);
					$data = $plugin_item->row();
					
					// Query
					$query = "INSERT INTO items_trash SET module_code = '".$this->module."', item_id = '".$this->id."', plugin_code = '".$this->c[parent_plugin]."', plugin_id = '".$this->c[parent_plugin_id]."', data = '".data_serialize($data)."', trash_created = '".time()."', trash_updated = '".time()."', user_id = '".u('id')."', user_ip = '".IP."', trash_reason = '".a($c[reason])."'";
					debug($query,$c[debug]);
					
					// Trash
					$id = $this->db->q($query);
					
					// Return
					return array('id' => $id);
				}
			}
		}
	}
}
?>