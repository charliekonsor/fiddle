<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'Files',
	'single' => 'File',
	'plural' => 'Files',
	'icon' => 'file',
	'db' => array(
		'table' => 'items_files',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id',
		'parent_plugin' => 'plugin_code',
		'parent_plugin_id' => 'plugin_id',
		'created' => 'file_created',
		'updated' => 'file_updated'
	),
	/*'settings' => array( // Can add settings => plugins => files => keep => 1 to a module to force site to 'keep' file on server, even if item it's associated with is deleted
		'keep' => 0,
	),*/
);
?>