<?php
if(!class_exists('plugin_files',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the files plugin.
	 *
	 * @package kraken\plugins\files
	 */
	class plugin_files extends plugin {
		// Variables
		var $file;
		
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_files($module,$id,$plugin,$c);
		}
		function plugin_files($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Saves an item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_save($values,$c = NULL) {
			// Error
			if(!$this->module or (!$this->id and !$this->c[parents][0][plugin_id]) or !$this->plugin or !$c['isset']) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Columns
			foreach($values as $column => $v) {
				// Unserialize data
				if($v and !is_array($v)) $v = data_unserialize($v);
				
				// Plugin item row
				$plugin_item_row = $this->db->f($this->rows(array('where' => "file_column = '".$column."'")));
				// Plugin item
				$plugin_item = $this->plugin_item($plugin_item_row);
				if($plugin_item->v('file_name')) {
					// Delete old row (and files) - if the file has changed (which would also be the name if there were no $v[file_name] value)
					debug("old name: ".$plugin_item->v('file_name').", new name: ".$v[file_name],$c[debug]);
				 	if($v[file_name] != $plugin_item->v('file_name')) {
						debug("deleting via \$".__CLASS__."-".__FUNCTION__,$c[debug]);
						$plugin_item->delete($c);
						$plugin_item = $this->plugin_item(); // I'm pretty sure delete() will refresh the item so calling save() later would create a new item...but just to be sure
					}
				}
				
				// Values
				$v = $this->save_values($v);
				if($v) {
					// Strip server - file
					if($v[file_path]) $v[file_path] = str_replace(SERVER,'',$v[file_path]);
					
					// Strip server - extensions
					if($v[file_extensions]) {
						foreach($v[file_extensions] as $extension_k => $extension_v) {
							$v[file_extensions][$extension_k][path] = str_replace(SERVER,'',$extension_v[path]);
						}
						$v[file_extensions] = data_serialize($v[file_extensions]);
					}
					
					// Strip server - thumbs
					if($v[file_thumbs]) {
						foreach($v[file_thumbs] as $thumb_k => $thumb_v) {
							if(!$thumb_v[path] and is_array(array_first($thumb_v))) {
								foreach($thumb_v as $thumb_k1 => $thumb_v1) {
									// Path
									$v[file_thumbs][$thumb_k][$thumb_k1][path] = str_replace(SERVER,'',$thumb_v1[path]);
									unset($v[file_thumbs][$thumb_k][$thumb_k1][debug],$v[file_thumbs][$thumb_k][$thumb_k1][count]);
								}
							}
							else {
								// Path
								$v[file_thumbs][$thumb_k][path] = str_replace(SERVER,'',$thumb_v[path]);
								unset($v[file_thumbs][$thumb_k][debug]);
							}
						}
						$v[file_thumbs] = data_serialize($v[file_thumbs]);
					}
					
					// Save
					$plugin_item->save($v,$c);
				}
			}
		}
		
		/**
		 * Saves a plugin item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function plugin_item_save($values,$c = NULL) {
			// Error
			if(!$this->module /*or !$this->id */or !$this->plugin or !$this->c[parents][0][plugin] or !$this->c[parents][0][plugin_id]) return;
			
			// New values
			if($values and $c['isset']) {
				// Columns
				foreach($values as $column => $v) {
					// Unserialize data
					if($v and !is_array($v)) $v = data_unserialize($v);
					
					// Parent values
					$v[plugin_code] = $this->c[parents][0][plugin];
					$v[plugin_id] = $this->c[parents][0][plugin_id];
					
					// Update
					$values[$column] = data_serialize($v);
				}
			
				// Debug
				debug("new values (after adding parent plugins): ".return_array($values),$c[debug]);
				
				// Save
				$this->item_save($values,$c);
			}
			// Old values - that were missing an id
			if($this->id) {
				$query = "UPDATE items_files SET item_id = '".$this->id."' WHERE module_code = '".$this->module."' AND item_id = 0 AND plugin_code = '".$this->c[parents][0][plugin]."' AND plugin_id = '".$this->c[parents][0][plugin_id]."'";
				debug($query,$c[debug]);
				$this->db->q($query);
			}
		}
		
		/**
		 * Copy's the plugin row(s) for the item to the given new item.
		 *
		 * Need to make sure we copy the actual file and give it a unique name.
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			// Parent
			parent::item_copy($new_id,$c);
			
			// New item
			$item = item::load($this->module,$new_id);
			$item_plugin = $item->plugin($this->plugin);
			
			// Files
			$rows = $item_plugin->rows();
			while($row = $this->db->f($rows)) {
				// File
				$f = file::load($row[file_path].$row[file_name],array('storage' => $row[file_storage]));
				$name_old_root = $f->name(0);
				
				// Copy - copy to same path, but overwrite = 0 will give it a unique name
				$f->copy($row[file_path],array('overwrite' => 0));
				$name = $f->name;
				$name_root = str_replace('.'.$f->extension,'',$name);
				debug("Copied ".$f->file." to ".$row[file_path].$name,$c[debug]);
				
				// Copy - extensions
				if($row[file_extensions]) {
					$extensions = data_unserialize($row[file_extensions]);
					foreach($extensions as $k => $v) {
						$extensions[$k][name] = str_replace('.'.$row[file_extension],'.'.$k,$name);
						$f = file::load($v[path].$v[name],array('storage' => $v[storage]));
						$f->copy($v[path].$extensions[$k][name]);
						debug("Copied ".$f->file." to ".$v[path].$extensions[$k][name],$c[debug]);
					}
					$extensions = data_serialize($extensions);
				}
				// Copy - thumbs
				if($row[file_thumbs]) {
					$thumbs = data_unserialize($row[file_thumbs]);
					foreach($thumbs as $k => $v) {
						// Copy - thumbs - video
						if(!$v[name] and is_array($v)) {
							foreach($v as $k1 => $v1) {
								$thumbs[$k][$k1][name] = str_replace($name_old_root,$name_root,$v1[name]);
								$f = file::load($v1[path].$v1[name],array('storage' => $v[storage]));
								$f->copy($v1[path].$thumbs[$k][$k1][name]);	
								debug("Copied ".$f->file." to ".$v1[path].$thumbs[$k][$k1][name],$c[debug]);
							}
						}
						// Copy - thumbs - image
						else {
							$thumbs[$k][name] = str_replace($name_old_root,$name_root,$v[name]);
							$f = file::load($v[path].$v[name],array('storage' => $v[storage]));
							$f->copy($v[path].$thumbs[$k][name]);
							debug("Copied ".$f->file." to ".$v[path].$thumbs[$k][name],$c[debug]);
						}
					}
					$thumbs = data_serialize($thumbs);
				}
				// Copy - thumb
				$thumb = str_replace($name_old_root,$name_root,$row[file_thumb]);
				
				// Save - plugin row
				$values = array(
					'file_name' => $name,
					'file_extensions' => $extensions,
					'file_thumbs' => $thumbs,
					'file_thumb' => $thumb,
				);
				$values_string = $this->db->values($values);
				$query = "UPDATE ".plugin($this->plugin.'.db.table')." ".$values_string." WHERE ".plugin($this->plugin.'.db.id')." = '".$row[plugin($this->plugin.'.db.id')]."'";
				debug($query,$c[debug]);
				$this->db->q($query);
				
				// Save - item row
				if($row[file_column]) {
					$values = array(
						$row[file_column] => $name
					);
					$item->save($values,array('debug' => $c[debug]));
				}
			}
		}
		
		/**
		 * Analyzes file and returns array of data about it which can be saved in the plugin's database.
		 *
		 * @param string $name The name of the file you want to analyze (not including the 'path').
		 * @param string $type The file type this file should be: file, image, video, audio. Default = file
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of data about this file.
		 */
		function values($name,$type = "file",$c = NULL) {
			// Error
			if(!$name) return;
			
			// Config
			if(!x($c[extensions])) $c[extensions] = m($this->module.'.uploads.types.'.$type.'.extensions'); // Array of extensions to handle. Set to 0 if we don't want to handle extensions.
			if(!x($c[thumbs])) $c[thumbs] = m($this->module.'.uploads.types.'.$type.'.thumbs'); // Array of thumbs to handle. Set to 0 if we don't want to handle thumbs.
			if(!x($c[storage])) $c[storage] = m($this->module.'.uploads.types.'.$type.'.storage'); // Where to store the file.
			
			// File
			$file = m($this->module.'.uploads.types.'.$type.'.path').$name;
			
			// Process
			$values = file_process($file,$type,$c);
			
			// Re-key
			$values_new = NULL;
			foreach($values as $k => $v) {
				$values_new['file_'.$k] = $v;
			}
				
			// Extensions
			if($array[file_extensions]) {
				$array[file_extensions] = data_serialize($array[file_extensions]);	
			}
				
			// Thumbs
			if($array[file_thumbs]) {
				$array[file_thumb] = 1;
				$array[file_thumbs] = data_serialize($array[file_thumbs]);
			}
			
			
			// Return
			return $values_new;
		}
	}
}
?>