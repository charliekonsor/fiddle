<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'Menu',
	'single' => 'Menu',
	'plural' => 'Menu',
	'icon' => 'menu',
	'db' => array(
		'table' => 'items_menus' // Won't use, but need this so $form->plugins() knows we want the values passed in plugin[menu][{key}] format.
	)
);
?>