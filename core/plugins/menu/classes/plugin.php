<?php
if(!class_exists('plugin_menu',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the menu plugin.
	 *
	 * @package kraken\plugins\menu
	 */
	class plugin_menu extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_menu($module,$id,$plugin,$c);
		}
		function plugin_menu($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			// Can appear in menu and we're editing it via the 'Menu' section (has __plugins[menu][id])
			if(m($this->module.'.public.views.item.urls.0') and $input = $form->input_exists('__plugins[menu][id]')) {
				$menu_item = item::load('menus_items',$input->value);
				$r = random();
				$placeholder = m($this->module.'.db.name',$this->type);
				$javascript .= "
<script type='text/javascript'>
$(document).ready(function() {
	pluginsMenu".$r."();
	
	$(':input[name=".$placeholder."]').blur(function() {
		pluginsMenu".$r."();
	});
});
function pluginsMenu".$r."() {
	var value = $(':input[name=".$placeholder."]').val();
	$('#__plugins_menu_".$r."').attr('placeholder',value);
	placeholders();
}
</script>";
				$inputs = array(
					array(
						'label' => 'Name',
						'type' => 'text',
						'name' => 'item_name',
						'value' => $menu_item->name,
						'placeholder' => $this->item->name,
						'id' => '__plugins_menu_'.$r
					),
					array(
						'label' => 'Image',
						//'type' => 'swfupload', // Need to figure out bug with SWFUpload when initally hidden to use this
						'type' => 'file',
						'name' => 'item_image',
						'value' => $menu_item->v('item_image'),
						'file' => array(
							'type' => 'image'
						)
					),
					array(
						'label' => 'Rollover Image',
						//'type' => 'swfupload', // Need to figure out bug with SWFUpload when initally hidden to use this
						'type' => 'file',
						'name' => 'item_image_rollover',
						'value' => $menu_item->v('item_image_rollover'),
						'file' => array(
							'type' => 'image'
						)
					),
					array(
						'type' => 'html',
						'value' => $javascript
					)
				);
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Saves the given values as an item in this plugin.
		 *
		 * @param array $values An array of values you want to save.
		 * @param int $id The id of the plugin item you want to edit (if none passed, we assume you're adding a new plugin item).
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the saved plugin item.
		 */
		function save($values,$id = NULL,$c = NULL) {
			// Error
			if(!$this->module or !$this->id or !$this->plugin) return;
			
			// Menu passed
			if(is_array($values) and $values[menu_id]) {
				// Values
				$values[item_type] = 'module';
				$values[item_module] = $this->module;
				$values[item_id] = $this->id;
				
				// Save
				$item = item::load('menus_items',$values[id]);
				$item->save($values,array('debug' => $c[debug]));
			}
		}
		
		/**
		 * Saves an item's plugin values, either inserting it or (if row already exists) updating the existing row.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_save($values,$c = NULL) {
			$this->save($values,NULL,$c);
		}
		
		/**
		 * Copy's the plugin row(s) for the item to the given new item.
		 *
		 * Does nothing as we won't want to create a new menu item for the copied item automatically.
		 *
		 * @param int $new_id The id of the new, copied item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_copy($new_id,$c = NULL) {
			return;
		}
	
		/**
		 * Delete's the menu items associated with a newly deleted item.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function item_delete($c = NULL) {
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug 
			
			if($this->module != "menus_items") {
				// Query
				$query = "DELETE FROM menus_items WHERE item_type = 'module' AND module_area = 'public' AND module_code = '".$this->module."' AND module_view = 'item' AND item_id = '".$this->id."'";
				debug($query,$c[debug]);
				
				// Delete
				$this->db->q($query);
			}
		}
		
		/**
		 * Returns array (in array(array('url' => $url,'label' => $label),array(..)) format) of menu item options available for this module.
		 *
		 * @return array An array available menu item options for this module.
		 */
		function options() {
			// Error
			if(!$this->module) return;
			
			// Options
			$options = array();
			
			// Areas
			$areas = array(
				'public' => '',
				'account' => 'Account',
			);
			foreach($areas as $area => $label) {
				$options_area = $this->options_area($area);
				if($options_area) {
					if($label) $options[$label] = $options_area;
					else $options = array_merge($options,$options_area);
				}
			}
			
			// Return
			return $options;
		}
		
		/**
		 * Returns array (in array(array('url' => $url,'label' => $label),array(..)) format) of menu item options available for this module in the given area.
		 *
		 * @param string $area The area you want to get available menu item options for.
		 * @return array An array available menu item options for this module.
		 */
		function options_area($area) {
			// Error
			if(!$this->module or !$area) return;
			
			// Module
			$module_class = module::load($this->module);
			
			// Options
			$options = NULL;
			
			// Views
			if($views = $module_class->v($area.'.views')) {
				foreach($views as $k => $v) {
					if($v[urls]) {
						foreach($v[urls] as $x => $url_v) {
							if($x == 0 or $url_v[label]) {
								// Label
								$label = ($url_v[label] ? $url_v[label] : $v[label]);
								
								// Public item
								if($v[item] and $area == "public" and !strstr($url_v[format],"/new")) {
									if(permission($v[item_module],'edit',$v[item_id])) {
										$options[] = array(
											'url' => "item_type=module&module_area=".$area."&module_code=".$this->module."&module_view=".$k."&item_module=".$v[item_module]."&item_id=".$v[item_id],
											'label' => (!$v[item_id] && $v[item_module] == $this->module ? "Existing " : "").$label,
										);
									}
									if(permission($v[item_module],'add') and !$v[item_id] and $v[item_module] == $this->module) {
										$options[] = array(
											'url' => "item_type=module&module_area=".$area."&module_code=".$this->module."&module_view=".$k."&item_module=".$v[item_module]."&item_id=new",
											'label' => "New ".$label,
										);
									}
								}
								// Other
								else {
									$options[] = array(
										'url' => "item_type=module&module_area=".$area."&module_code=".$this->module."&module_view=".$k,
										'label' => $label,
									);
								}
							}
						}
					}
				}
			}
			
			// Return
			return array_filter($options);
		}
	}
}
?>