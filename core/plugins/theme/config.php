<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'default' => 1,
	'required' => 1,
	'name' => 'Theme',
	'single' => 'Theme',
	'plural' => 'Themes',
	'icon' => 'theme',
	'db_variables' => array(
		'theme' => array(
			'label' => 'Theme'
		),
		'template' => array(
			'label' => 'Template'
		)
	),
	/*'settings' => array(
		'inputs' => array(
			'label' => 'Theme',
			'type' => 'select',
			'name' => 'theme[theme]',
			'options' => 'theme',
			'id' => 'theme-theme',
		),
		 array(
			'label' => 'Template',
			'type' => 'select',
			'name' => 'theme[tempalte]',
			'options' => 'templates',
			'id' => 'theme-templates'
		),
		array(
			'type' => 'html',
			'value' => "
<script type='text/javascript'>
var template_options;
$(document).ready(function() {
	// Store all template options
	template_options = $('#theme-templates').html();
	
	toggleTheme();
	
	$('#theme-theme').change(function() {
		toggleTheme();
	});
});
function toggleTheme() {
	var value = $('#theme-theme').val();
	if(!value) value = '".m('settings.settings.theme')."';
	
	// Re-store all options
	$('#theme-templates').html(template_options);
	
	// Remove unwanted options
	$('#theme-templates option.template:not(.template-'+value+')').remove();
}
</script>"
		),
	),*/
);
?>