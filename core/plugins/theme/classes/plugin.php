<?php
if(!class_exists('plugin_theme',false)) {
	/**
	 * An extenion of the plugin class which handles functionality specific to the theme plugin.
	 *
	 * @package kraken\plugins\theme
	 */
	class plugin_theme extends plugin {
		/**
		 * Constructs the class.
		 *
		 * @param string $module The module of the item the plugin is acting upon.
		 * @param int $id The id of the item the plugin is acting upon. Default = NULL
		 * @param string $plugin The plugin we're using.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module,$id = NULL,$plugin,$c = NULL) {
			$this->plugin_theme($module,$id,$plugin,$c);
		}
		function plugin_theme($module,$id = NULL,$plugin,$c = NULL) {
			parent::__construct($module,$id,$plugin,$c);
		}
		
		/**
		 * Returns array of inputs for use in the sidebar box for this plugin.
		 *
		 * @param object $form The form object we'll be adding these inputs to.
		 * @return array An array of inputs to add to the form.
		 */
		function sidebar($form) {
			if($name = m($this->module.'.db.theme')) {
				// Themes
				if($themes = themes()) {
					// Random
					$r = random();
					
					// Options
					$options = array('' => '');
					foreach($themes as $k => $v) {
						// Option
						$options[$v[code]] = $v[name];
						// Templates
						$theme_templates[$v[code]] = $v[templates];
					}
					
					// Theme
					$inputs[] = array(
						'label' => 'Theme',
						'type' => 'select',
						'name' => $name,
						'options' => $options,
						'value' => $this->item->theme,
						'id' => 'theme-'.$r,
					);
					
					// Templates
					if($name = m($this->module.'.db.template')) {
						// Options
						$options = array('' => '');
						foreach($theme_templates as $theme => $templates) {
							if($templates) {
								foreach($templates as $k => $v) {
									$options[$k][value] = $k;
									$options[$k][label] = $v;
									$options[$k]['class'] .= ($options[$k]['class'] ? "" : "template")." template-".$theme;
								}
							}
							else {
								$options[''][value] = '';
								$options[''][label] = 'No templates found';
								$options['']['class'] .= ($options['']['class'] ? "" : "template")." template-".$theme;
							}
						}
						$options = array_values($options);
						
						// Input
						$inputs[] = array(
							'label' => 'Template',
							'type' => 'select',
							'name' => $name,
							'options' => $options,
							'value' => $this->item->template,
							'id' => 'theme-'.$r.'-templates'
						);
						
						// Javascript
						$javascript = "
<script type='text/javascript'>
var template_options;
$(document).ready(function() {
	// Store all template options
	template_options = $('#theme-".$r."-templates').html();
	
	toggleTheme".$r."(1);
	
	$('#theme-".$r."').change(function() {
		toggleTheme".$r."();
	});
});
function toggleTheme".$r."(keep) {
	var value = $('#theme-".$r."').val();
	if(!value) value = '".(m($this->module.'.settings.public.views.item.theme') ? m($this->module.'.settings.public.views.item.theme') : m('settings.settings.theme'))."';
	
	// Re-store all options
	$('#theme-".$r."-templates').html(template_options);
	// Reset value (unless we want to 'keep' it)
	if(!keep) $('#theme-".$r."-templates').val('');
	
	// Remove unwanted options
	$('#theme-".$r."-templates option.template:not(.template-'+value+')').remove();
}
</script>";
						$inputs[] = array(
							'type' => 'html',
							'value' => $javascript
						);
					}
				}
			}
			
			// Return
			if($inputs) {
				$return = array(
					'label' => plugin($this->plugin.'.name'),
					'inputs' => $inputs
				);
				return $return;
			}
		}
		
		/**
		 * Returns array of settings for this plugin which can be set on a per module basis.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of inputs that constitute the plugin settings that can be set on a per module basis.
		 */
		function module_settings($c = NULL) {
			// Label
			$label = array(
				array(
					'label' => $this->v('name'),
					'type' => 'collapsed'
				)
			);
			
			// Settings
			$settings = NULL;
			$views = NULL;
			$module_class = module::load($this->module);
			if(count($module_class->v('public.views')) > 0) {
				$views['public'] = $module_class->v('public.views');
			}
			if(count($module_class->v('account.views')) > 0) {
				$views['account'] = $module_class->v('account.views');
			}
			if($views) {
				// Themes
				if($themes = themes()) {
					// Themes - options
					$options_themes = array('' => '');
					foreach($themes as $k => $v) {
						// Option
						$options_themes[$v[code]] = $v[name];
						// Templates
						$theme_templates[$v[code]] = $v[templates];
					}
					
					// Templates - options
					$options_templates = array('' => '');
					foreach($theme_templates as $theme => $templates) {
						if($templates) {
							foreach($templates as $k => $v) {
								$options_templates[] = array(
									'value' => $k,
									'label' => $v,
									'class' => 'template template-'.$theme
								);
							}
						}
						else $options_templates[] = array(
							'value' => NULL,
							'label' => "No templates found",
							'class' => 'template template-'.$theme
						);
					}
					
					foreach($views as $area => $area_views) {
						foreach($area_views as $k => $v) {
							if($v[urls]) {
								$r = random();
								// Input - view
								$settings[] = array(
									'type' => 'html',
									'value' => '<b>'.$v[label].'</b>'
								);
								// Input - theme
								$settings[] = array(
									'label' => 'Theme',
									'type' => 'select',
									'name' => $area.'[views]['.$k.'][theme]',
									'options' => $options_themes,
									'id' => 'theme-'.$r,
								);
								// Input - template
								$settings[] = array(
									'label' => 'Template',
									'type' => 'select',
									'name' => $area.'[views]['.$k.'][template]',
									'options' => $options_templates,
									'id' => 'theme-'.$r.'-templates',
								);
								// Input - javascript
								$settings[] = array(
									'type' => 'html',
									'value' => "
<script type='text/javascript'>
var template_options_".$r.";
$(document).ready(function() {
	// Store all template options
	template_options_".$r." = $('#theme-".$r."-templates').html();
	
	toggleTheme".$r."();
	
	$('#theme-".$r."').change(function() {
		toggleTheme".$r."();
	});
});
function toggleTheme".$r."() {
	var value = $('#theme-".$r."').val();
	if(!value) value = '".m('settings.settings.theme')."';
	
	// Store selected template
	var template = $('#theme-".$r."-templates').val();
	
	// Restore all options
	$('#theme-".$r."-templates').html(template_options_".$r.");
	
	// Remove unwanted options
	$('#theme-".$r."-templates option.template:not(.template-'+value+')').remove();
	
	// Restore selected template
	$('#theme-".$r."-templates').val(template);
}
</script>",
								);
							}
						}
					}
				}
			}
			
			if($settings) {
				// Add label
				$settings = array_merge($label,$settings);
				$settings[] = array('type' => 'collapsed_end');
					
				// Return
				return $settings;
			}
		}
	}
}
?>