<?php
print "
The <b>Theme</b> sidebar seection lets you choose exactly what theme and template this specific ".strtolower(m($this->module.'.single'))." uses.<br /><br />

Of course, as with most everything else in the sidebar, this is optional. If you don't do anything in this section, we'll use the default theme and template defined for this module and your ".strtolower(m($this->module.'.single'))." will show up just fine.  This just gives you the option to choose another theme or (more likely) template if this page is a little special and needs to look differently.<br /><br />

<em>Theme sidebar section</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar.png' alt='' /><br /><br />

<h5>Theme</h5>
A theme is, very simply, the general look and design of a website.  Generally, the entire website will use only one theme (which theme, exactly, is defined in the <a href='".DOMAIN."admin/settings'>global settings</a>).  As such, you'll almost never need to use this field.<br /><br />

If, however, this ".strtolower(m($this->module.'.single'))." needs a different theme than the rest of the site, you'd simply select what that theme is from the drop down.<br /><br />

<h5>Template</h5>
A template is part of a theme and is used to define a general layout.  A theme usually has a few different templates. For example, you may have a 'home' template which  is only used on the home page which includes a hearder, a menu bar, a carousel with sliding images, a flashy video, and a footer. You may also have a 'default' template which simply has a header, menu bar, content area, and footer.<br /><br />

The 'Template' drop down in this section lets you choose what specific template this ".strtolower(m($this->module.'.single'))." uses. Which templates are available depends upon the selected theme for this ".strtolower(m($this->module.'.single')).". If no theme is selected, that means you're using the globally defined theme and, as such, the templates that appear in the drop down are part of the globally defined theme.<br /><br />

If you leave this blank (as you almost always will) this ".strtolower(m($this->module.'.single'))." will use the defalt template as defined for this module. This is almost always the template named 'default'.";

?>