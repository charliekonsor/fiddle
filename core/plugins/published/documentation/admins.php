<?php
$module_class = module::load($this->module);
$single = strtolower($module_class->v('single'));

print "
Using the 'Published' sidebar section, as you may have guessed, lets us define if this ".$single." is publishe and (if so) some specific dates during which it's published.<br /><br />

First of all, what do we mean by 'published'? When ".string_an($single)." is 'published' it simply means it's to those who have permission to view it.  When it's unpublished, no one (except you, an admin) can see it.  Of course, even if ".string_an($single)." is published, the user's 'type' still must have permission to 'View' items in this module.  Which is to say, if 'Guest' users are set up so that they do not have permission to 'View' items in this module, then they never will be able to see it, published or not.<br /><br />

So, how do we go about defining something as published or not?  Well, first let's take a look at what this sidebar section looks like, then we'll talk about what each input in it does.<br /><br />

<em>Published sidebar</em><br />
<img src='".DOMAIN.$this->v('path')."documentation/images/sidebar.png' alt='' /><br /><br />

<h5>Published</h5>
First of all, the <b>Published</b> checkbox gives us a way to simply say if the ".$single." is published or not. As you can see in the screenshot above, it's checked by default, which is to say if you don't do anything, it'll be considered 'Published'.  If you were to uncheck this box and submit the form, the ".$single." would then be considered 'Unpublished' and, as such, wouldn't be visible to non-admins.<br /><br />

Note, if we uncheck this box, the ".$single." is summarily considered 'Unpublished'.  However, if the box is checked, it is considred published, but we can also define a specific date range that it's published.  Exactly how we do that will is now the two hours traffic of our stage.<br /><br />

<h5>Start Publication</h5>
First of all, the <b>Start Publication</b> (and the End Publication) fields are optional. If we check the 'Published' box, but leave these blank, the ".$single." will be considered published, not matter what the date is.<br /><br />

Similarily, if we leave this blank, but input a 'End Publication' date, it will be considred published from the beginning of time until that 'End Publication' date.<br /><br />

If there's a specific date / time at which we want this ".$single." to be 'Published' (which is to say visible to the public) then we simply select the date and input the time here. Simple enough.<br /><br />

<h5>End Publication</h5>
Just like with the 'Start Publication' field, the <b>End Publication</b> field is optional. If we check the 'Published' box, but leave these blank, the ".$single." will be considered published, not matter what the date is.<br /><br />

If we input a 'Start Publication' date, but leave this field blank, the ".$single." will be considred 'Published' from that Start Publication date until the end of time.<br /><br />

If, however, there's a specific date after which this ".$single." should not longer be 'Published' (visible to the public), you'd simply input that date / time here.";
?>