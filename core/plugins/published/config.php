<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
// New method - store in module's table
$config = array(
	'name' => 'Published',
	'single' => 'Published',
	'plural' => 'Published',
	'icon' => 'time',
	'db_variables' => array(
		'published' => array(
			'label' => 'Published',
		),
		'published_start' => array(
			'label' => 'Start Publication',
		),
		'published_end' => array(
			'label' => 'End Publication',
		),
	)
);

// Old method - store in separate table
/*$config = array(
	'name' => 'Published',
	'single' => 'Published',
	'plural' => 'Published',
	'icon' => 'time',
	'db' => array(
		'table' => 'items_published',
		'id' => 'id',
		'parent_module' => 'module_code',
		'parent_id' => 'item_id'
	)
);*/
?>