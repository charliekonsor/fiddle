<?php
print "<h1>Published</h1>";

// New method - store in module's table
foreach(g('modules') as $module => $v) {
	if(
		$v[plugins][published]
		and $v[db][visible]
		and (
			$v[db][published_start]	// Cron will only effect 'published' state if we have a start and/or end time
			or $v[db][published_end]	
		)
	) {
		// Invisible, but is now visible
		$query = "
		SELECT
			*
		FROM
			".$v[db][table]."
		WHERE
			".$v[db][visible]." = 0
			".($v[db][published] ? " AND ".$v[db][published]." = 1" : "");
		if($v[db][published_start]) $query .= "
			AND ".$v[db][published_start]." <= '".time_format(time())."'";
		if($v[db][published_end]) $query .= "
			AND (".$v[db][published_end]." = 0 OR ".$v[db][published_end]." > '".time_format(time())."')";
		print "<br />".$query."<br />";
		$rows = $core->db->q($query);
		while($row = $core->db->f($rows)) {
			print m($module.'.single')." #".$row[$v[db][id]]." is now visible (time: ".time_format(time()).", start: ".$row[$v[db][published_start]].", end: ".$row[$v[db][published_end]].")<br />";
			$item = item::load($module,$row);
			$item->process_visible();
		}
		
		// Visible, but is now invisible
		if($v[db][published_end]) { // Can only become invisible on 'end' date as visibility was check when 'start' was set and we're never going to go backwards in time (aka, won't reach a point before start time).
			$query = "
			SELECT
				*
			FROM
				".$v[db][table]."
			WHERE
				".$v[db][visible]." = 1
				".($v[db][published] ? " AND ".$v[db][published]." = 1" : "")."
				AND ".$v[db][published_end]." > 0
				AND ".$v[db][published_end]." <= '".time_format(time())."'";
			print "<br />".$query."<br />";
			$rows = $core->db->q($query);
			while($row = $core->db->f($rows)) {
				print m($module.'.single')." #".$row[$v[db][id]]." is now invisible (time: ".time_format(time()).", start: ".$row[$v[db][published_start]].", end: ".$row[$v[db][published_end]].")<br />";
				$item = item::load($module,$row);
				$item->process_visible();
			}
		}
	}
}
	
// Old method - store in separate table
// Invisible, but is now visible
/*$query = "SELECT * FROM items_published WHERE published = 1 AND published_value = 0 AND published_start <= '".time_format(time())."' AND (published_end = 0 OR published_end > '".time_format(time())."')";
print "<br />".$query."<br />";
$rows = $core->db->q($query);
while($row = $core->db->f($rows)) {
	print m($row[module_code].'.single')." #".$row[item_id]." is now visible (time: ".time_format(time()).", start: ".$row[published_start].", end: ".$row[published_end].")<br />";
	$item = item::load($row[module_code],$row[item_id]);
	$item->process_visible();
}

// Visible, but is now invisible
$query = "SELECT * FROM items_published WHERE published = 1 AND published_value = 1 AND published_end AND published_end <= '".time_format(time())."'";
print "<br />".$query."<br />";
$rows = $core->db->q($query);
while($row = $core->db->f($rows)) {
	print m($row[module_code].'.single')." #".$row[item_id]." is now invisible (time: ".time_format(time()).", start: ".$row[published_start].", end: ".$row[published_end].")<br />";
	$item = item::load($row[module_code],$row[item_id]);
	$item->process_visible();
}*/
?>