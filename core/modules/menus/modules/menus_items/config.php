<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Menu Links',
	'single' => 'Link',
	'plural' => 'Links',
	'icon' => 'menus_items',
	'db' => array(
		'table' => 'menus_items',
		'id' => 'id',
		'parent_id' => 'menu_id',
		'name' => 'item_name',
		'order' => 'item_order',
		'order_parent' => 'item_order_parent',
		'order_level' => 'item_order_level',
		//'order_string' => 'item_order_string', // Deprecated
		'visible' => 'item_visible',
		'created' => 'item_created',
		'updated' => 'item_updated'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'edit' => array(
			'label' => 'Edit',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'delete' => array(
			'label' => 'Delete',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'copy' => array(
			'label' => 'Copy',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'header' => 0,
		'sidebar' => 0,
		'section' => '',
		'views' => array(
			'home' => 'All Links',
			'item' => 'Edit Link'
		)
	),
	'public' => array(
		'active' => 0
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'item_name',
					'validate' => array(
						'required' => 1,
					),
				),
				/*array( // We'll manually add this if needed in the admin.item.php view 
					'label' => 'Link',
					'type' => 'text',
					'name' => 'item_url',
					'validate' => array(
						'required' => 1,
					),
				),*/
			),
			'plugins' => array(
				array(
					'label' => 'Menu',
					'inputs' => array(
						array(
							'label' => 'Image',
							//'type' => 'swfupload', // Need to figure out bug with SWFUpload when initally hidden to use this
							'type' => 'file',
							'name' => 'item_image',
							'file' => array(
								'type' => 'image'
							)
						),
						array(
							'label' => 'Rollover Image',
							//'type' => 'swfupload', // Need to figure out bug with SWFUpload when initally hidden to use this
							'type' => 'file',
							'name' => 'item_image_rollover',
							'file' => array(
								'type' => 'image'
							)
						),
					)
				)
			)	
		)
	)
);
?>