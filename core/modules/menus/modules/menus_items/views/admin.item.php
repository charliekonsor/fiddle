<?php
// Child module, but missing parent id, redirect to parent home page
if($this->module_class->v('parent') and !$this->page->parents[last][id]) {
	$parent_module = module::load($this->module_class->v('parent'));
	redirect($parent_module->url('home',$this->area));
}

// Permission
if($this->permission('edit',1)) {
	// Heading
	$heading = NULL;
	$headings = $this->heading_array();
	foreach($headings as $k => $v) {
		if($v != $this->module_class->v('name')) {
			$heading .= ($heading ? ": " : "")."<a href='".$k."'>".$v."</a>";
		}
	}
	$heading = "<h1 class='page-heading'>".$heading."</h1>";
	print $heading;
	
	// Value
	if($this->item_class->id) {
		$values = $this->item_class->a();
		$parent_id = $values[menu_id];
	}
	else {
		$values = $_GET;
		$parent_id = $this->page->parents[last][id];
	}
	
	// Content type
	if(!$values[module_area] and !$values[item_type]) {
		// Areas
		$areas = array(
			'public' => '',
			'account' => 'Account',
		);
		
		// Modules - get first so we can see if we have any 'account' places, if so we can add an Account home page option
		$options_modules = NULL;
		foreach(m() as $k => $v) {
			if($k != "home") {
				$plugin_class = plugin::load($k,NULL,'menu');
				$options_module = $plugin_class->options();
				if($options_module) $options_modules[$v[name]] = $options_module;
			}
		}
		// Home
		$plugin_class = plugin::load('home',NULL,'menu');
		//$options = $plugin_class->options(); // This isn't working for some reason, just using below for now.
		$options = array(
			array(
				'url' => 'item_type=module&module_area=public&module_code=home&item_module=home&module_view=item&item_id=1',
				'label' => 'Home Page',
			)
		);
		// Home - Account
		foreach($options_modules as $k => $v) {
			if($v[Account]) {
				$options[] = array(
					'url' => 'module_area=account',
					'label' => 'Account Home Page',
				);
				break;
			}
		}
		// Home - Admin
		$options[] = array(
			'url' => 'module_area=admin',
			'label' => 'Admin Home Page',
		);
		// Modules
		$options = array_merge($options,$options_modules);
		
		// External link
		$options[] = array(
			'url' => 'item_type=external',
			'label' => 'External Link',
		);
		// No link
		$options[] = array(
			'url' => 'item_type=nolink',
			'label' => 'No Link',
		);
		
		print "
<h2>Content Type</h2>
<ul>";
		foreach($options as $k => $v) {
			print "
	<li>";
			if($v[url]) print "
		<a href='".url_noquery()."?".$v[url]."'>".$v[label]."</a>";
			else {
				print "
					".$k."
	<ul>";
				foreach($v as $k0 => $v0) {
					print "
		<li>";
					if($v0[url]) print "
			<a href='".url_noquery()."?".$v0[url]."'>".$v0[label]."</a>";
					else {
						print "
							".$k0."
			<ul>";
						foreach($v0 as $k1 => $v1) {
							print "
				<li>";
							if($v1[url]) print "
					<a href='".url_noquery()."?".$v1[url]."'>".$v1[label]."</a>";
							print "
				</li>";
						}
						print "
			</ul>";
					}
					print "
		</li>";
				}
				print "
	</ul>";
			}
			print "
		</li>";
		}
		print "
</ul>";
	}
	// Content
	else {
		// Permission
		if($values[item_type] == "module") {
			if($values[module_view] == "item") {
				if($values[item_id] > 0) $permission = permission($values[module_code],'edit',$values[item_id]);
				else $permission = permission($values[module_code],'add');
			}
			else $permission = 1;
		}
		else $permission = 1;
	
		print "
<h2>Content</h2>
<div id='menus_items-content'>";
		
		// Permission
		if($permission) {
			
			// Redirect
			$redirect = $this->page->parents[last][item]->url($this->area)."/".$this->module;
			
			// Form - will overwrite if we don't want to use these
			$form = $this->item_class->form();
			// Inputs - will overwrite if we don't want to use these
			$inputs = array(
				array(
					'type' => 'hidden',
					'name' => 'menu_id',
					'value' => $parent_id
				),
				array(
					'type' => 'hidden',
					'name' => 'item_type',
					'value' => $values[item_type]
				),
				array(
					'type' => 'hidden',
					'name' => 'module_area',
					'value' => $values[module_area]
				),
				array(
					'type' => 'hidden',
					'name' => 'module_code',
					'value' => $values[module_code]
				),
				array(
					'type' => 'hidden',
					'name' => 'module_view',
					'value' => $values[module_view]
				),
				array(
					'type' => 'hidden',
					'name' => 'item_module',
					'value' => $values[item_module]
				),
			);
			
			// External
			if($values[item_type] == "external") {
				$inputs[] = array(
					'label' => 'Link',
					'type' => 'text',
					'name' => 'item_url',
					'validate' => array(
						'required' => 1,
					),
				);
			}
			// Module
			else if($values[item_type] == "module") {
				// View
				$view = m($values[module_code].'.'.$values[module_area].'.views.'.$values[module_view]);
				
				// Item
				if($view[item]) {
					// Category
					if($view[item_module] == "categories_items") {
						$options = NULL;
						$categories = module_categories($values[module_code]);
						if($categories) {
							foreach($categories as $id => $row) {
								$items = categories_options($id);
								if($items) {
									$options[] = array('label' => $row[m('categories.db.name')],'options' => $items);
								}
							}
							if($options) {
								$options = array_merge(array(array('label' => '')),$options); // Blank
								$inputs[] = array(
									'label' => 'Category',
									'name' => 'item_id',
									'type' => 'select',
									'options' => $options,
									'validate' => array(
										'required' => 1,
									),
								);
							}
						}
					}
					// Type
					else if($view[item_module] == "types") {
						$options = NULL;
						$types = m($values[module_code].'.types');
						if($types) {
							foreach($types as $type => $v) {
								if(m($values[module_code].'.'.$values[module_area].'.views.type',$type)) {
									$options[] = array(
										'label' => $v[name],
										'value' => $v[id]
									);
								}
							}
							if($options) {
								$options = array_merge(array(array('label' => '')),$options); // Blank
								$inputs[] = array(
									'label' => 'Type',
									'name' => 'item_id',
									'type' => 'select',
									'options' => $options,
									'validate' => array(
										'required' => 1,
									),
								);
							}
						}
					}
					// Default
					else {
						// Existing - new
						if(!$values[item_id]) {
							// Module
							$module_class = module::load($values[item_module]);
							// Items
							$options = NULL;
							$rows = $module_class->rows(array('order' => $module_class->v('db.name').' ASC'));
							if($this->db->n($rows)) {
								$options[] = "";
								while($row = $this->db->f($rows)) {
									$options[$row[$module_class->v('db.id')]] = string_format($module_class->v('db.name'),$row);
								}
							}
							$list_string = NULL;
							if($options) {
								// Autocomplete - javascript
								$options_string = NULL;
								foreach($options as $k => $v) {
									$options_string .= ($options_string ? "," : "").'{label:"'.string_encode($v).'",value:"'.$k.'"}';
									if($v) $list_string .= "<li><a href='javascript:void(0);' onclick=\"loadMenuItem('".$k."');\">".$v."</a></li>";
								}
								print "
".include_javascript('jquery.ui.autocomplete')."
".include_css('jquery.ui.autocomplete')."
<script type='text/javascript'>
$(function() {
	loadMenuItem('".$values[item_id]."');
	
	var tags = [".$options_string."];
	$('#suggest-items').autocomplete({
		source: tags,
		select: function(event, ui) {
			//$('#suggest-items').val(ui.item.label);
			$('#suggest-items').val('').blur();
			loadMenuItem(ui.item.value);

			return false;
		}
	});
});
</script>";
								// Autocomplete - text
								$input = array(
									'label' => array(
										'value' => $module_class->v('single')
									),
									'type' => 'text',
									'id' => 'suggest-items',
									'placeholder' => 'Start typing the name of the '.strtolower($module_class->v('single')).'..'
								);
							
								// Select
								/*$input = array(
									'label' => array(
										'value' => $module_class->v('single')
									),
									'type' => 'select',
									'name' => 'item_id',
									'value' => $values[item_id],
									'validate' => array(
										'required' => 1,
									),
									'id' => 'item_id',
									'onchange' => 'loadMenuItem();',
									'options' => $options
								);*/
							}
							else {
								$input = array(
									'type' => 'html',
									'value' => "<em>No ".strtolower($module_class->v('plural'))." found</em><input type='hidden' name='item_id' class='required' />"
								);
							}
							print "
	".$form->input_render($input)."
</div>
<script type='text/javascript'>
function loadMenuItem(id) {
	loader('#menus_items-item');
	
	//var id = $('#item_id').val();
	if(x(id)) {
		$.ajax({
			type: 'POST',
			url: DOMAIN,
			data: 'ajax_action=loadMenuItem&ajax_module=".$this->module."&module=".$values[module_code]."&id='+id+'&__plugins[menu][menu_id]=".$parent_id."&__plugins[menu][module_area]=".$values[module_area]."&__plugins[menu][module_code]=".$values[module_code]."&__plugins[menu][module_view]=".$values[module_view]."&__plugins[menu][id]=".$this->id."&redirect=".$redirect."',
			success: function(html) {
				$('#menus_items-item').html(html);
				js_refresh();
			}
		});
	}
	else {
		$('#menus_items-item').html('<ul>".a($list_string)."</ul>');
	}
}
</script>
<div id='menus_items-item'>";
							$form = NULL;
						}
						// New (or previously defined existing item)
						else {
							$form = form::load($values[item_module],$values[item_id],array('redirect' => $redirect));
							$form->hidden('__plugins[menu][menu_id]',$parent_id);
							$form->hidden('__plugins[menu][module_area]',$values[module_area]);
							$form->hidden('__plugins[menu][module_code]',$values[module_code]);
							$form->hidden('__plugins[menu][module_view]',$values[module_view]);
							$form->hidden('__plugins[menu][id]',$this->id);
							$inputs = NULL;
						}
					}
				}
				
				
				// Item
				/*if($values[module_view] == "item") {
					// Existing - new
					if(!$values[item_id]) {
						// Module
						$sub_module_class = module::load($sub_module);
						// Items
						$options = NULL;
						$rows = $sub_module_class->rows(array('order' => m($sub_module.'.db.name').' ASC'));
						if($this->db->n($rows)) {
							$options[] = "";
							while($row = $this->db->f($rows)) {
								$options[$row[m($sub_module.'.db.id')]] = string_format(m($sub_module.'.db.name'),$row);
							}
						}
						$list_string = NULL;
						if($options) {
							// Autocomplete - javascript
							$options_string = NULL;
							foreach($options as $k => $v) {
								$options_string .= ($options_string ? "," : "").'{label:"'.string_encode($v).'",value:"'.$k.'"}';
								if($v) $list_string .= "<li><a href='javascript:void(0);' onclick=\"loadMenuItem('".$k."');\">".$v."</a></li>";
							}
							print "
".include_javascript('jquery.ui')."
".include_javascript('jquery.ui.autocomplete')."
".include_css('jquery.ui')."
".include_css('jquery.ui.autocomplete')."
<script type='text/javascript'>
$(function() {
	loadMenuItem('".$values[item_id]."');
	
	var tags = [".$options_string."];
	$('#suggest-items').autocomplete({
		source: tags,
		select: function(event, ui) {
			//$('#suggest-items').val(ui.item.label);
			$('#suggest-items').val('').blur();
			loadMenuItem(ui.item.value);

			return false;
		}
	});
});
</script>";
							// Autocomplete - text
							$input = array(
								'label' => array(
									'value' => m($sub_module.'.single')
								),
								'type' => 'text',
								'id' => 'suggest-items',
								'placeholder' => 'Start typing the name of the '.strtolower(m($sub_module.'.single')).'..'
							);
							
							// Select
							/*$input = array(
								'label' => array(
									'value' => m($sub_module.'.single')
								),
								'type' => 'select',
								'name' => 'item_id',
								'value' => $values[item_id],
								'validate' => array(
									'required' => 1,
								),
								'id' => 'item_id',
								'onchange' => 'loadMenuItem();',
								'options' => $options
							);*/
						/*}
						else {
							$input = array(
								'type' => 'html',
								'value' => "<em>No ".strtolower(m($sub_module.'.plural'))." found</em><input type='hidden' name='item_id' class='required' />"
							);
						}
						print "
	".$form->input_render($input)."
</div>
<script type='text/javascript'>
function loadMenuItem(id) {
	loader('#menus_items-item');
	
	//var id = $('#item_id').val();
	if(x(id)) {
		$.ajax({
			type: 'POST',
			url: DOMAIN,
			data: 'ajax_action=loadMenuItem&ajax_module=".$this->module."&module=".$values[module_code]."&id='+id+'&__plugins[menu][menu_id]=".$parent_id."&__plugins[menu][id]=".$this->id."&redirect=".$redirect."',
			success: function(html) {
				$('#menus_items-item').html(html);
				js_refresh();
			}
		});
	}
	else {
		$('#menus_items-item').html('<ul>".a($list_string)."</ul>');
	}
}
</script>
<div id='menus_items-item'>";
						$form = NULL;
					}
					// New/Existing - defined
					else {
						$form = form::load($sub_module,$values[item_id],array('redirect' => $redirect));
						$form->hidden('__plugins[menu][menu_id]',$parent_id);
						$form->hidden('__plugins[menu][id]',$this->id);
						$inputs = NULL;
					}
				}
				// Category
				if($values[module_view] == "category") {
					// Categories
					$options = NULL;
					$categories = module_categories($values[module_code]);
					if($categories) {
						foreach($categories as $id => $row) {
							$items = categories_options($id);
							if($items) {
								$options[] = array('label' => $row[m('categories.db.name')],'options' => $items);
							}
						}
						if($options) {
							$options = array_merge(array(array('label' => '')),$options); // Blank
							$inputs[] = array(
								'label' => 'Category',
								'name' => 'item_id',
								'type' => 'select',
								'options' => $options,
								'validate' => array(
									'required' => 1,
								),
							);
						}
					}
				}
				
				// Type
				if($values[module_view] == "type") {
					$options = NULL;
					$types = m($values[module_code].'.types');
					if($types) {
						foreach($types as $type => $v) {
							if(m($values[module_code].'.'.$values[module_area].'.views.type',$type)) {
								$options[] = array(
									'label' => $v[name],
									'value' => $v[id]
								);
							}
						}
						if($options) {
							$options = array_merge(array(array('label' => '')),$options); // Blank
							$inputs[] = array(
								'label' => 'Type',
								'name' => 'item_id',
								'type' => 'select',
								'options' => $options,
								'validate' => array(
									'required' => 1,
								),
							);
						}
					}
				}*/
				
				// Home - don't need to do anything, it's all been done.
			}
			
			// Form
			if($form) {
				$form->inputs($inputs);
				print $form->render();
			}
			print "
</div>";
		}
		// No permission
		else {
			print "
<div class='core-red core-none'>I'm sorry, but you don't have permission to do this.</div>";	
		}
	}
}
?>