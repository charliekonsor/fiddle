<?php
/************************************************************************************/
/*********************************** loadMenuItem ***********************************/
/************************************************************************************/
if($_POST['ajax_action'] == "loadMenuItem") {
	if($_POST['module'] and $_POST['id']) {
		$item = item::load($_POST['module'],$_POST['id']);
		$form = $item->form(array('redirect' => $_POST['redirect'],'plugins' => 1));  // Need to set plugins == 1 since $this->area != admin
		$form->hidden('__plugins[menu][menu_id]',$_POST['__plugins']['menu']['menu_id']);
		$form->hidden('__plugins[menu][module_area]',$_POST['__plugins']['menu']['module_area']);
		$form->hidden('__plugins[menu][module_code]',$_POST['__plugins']['menu']['module_code']);
		$form->hidden('__plugins[menu][module_view]',$_POST['__plugins']['menu']['module_view']);
		$form->hidden('__plugins[menu][id]',$_POST['__plugins']['menu']['id']);
		print $form->render();	
	}
	exit;	
}
?>