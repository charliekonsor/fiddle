<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Menus',
	'single' => 'Menu',
	'plural' => 'Menus',
	'icon' => 'menus',
	'children' => array(
		'menus_items'
	),
	'db' => array(
		'table' => 'menus',
		'id' => 'menu_id',
		'code' => 'menu_code',
		'name' => 'menu_name',
		'visible' => 'menu_visible',
		'created' => 'menu_created',
		'updated' => 'menu_updated'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'edit' => array(
			'label' => 'Edit',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'delete' => array(
			'label' => 'Delete',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		/*'manage' => array( // Need at least one 'manage' function otherwise they won't have permission to manage anything in the admin.
			'label' => 'Manage',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),*/
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'section' => 'menus',
		'views' => array(
			'home' => 'All Menus',
			'item' => 'Edit Menu'
		)
	),
	'public' => array(
		'active' => 0,
		'views' => array(
			'item' => 'Menu'
		)
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'menu_name',
					'validate' => array(
						'required' => 1,
					),
				)
			)
		)
	)
);
?>