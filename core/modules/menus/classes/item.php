<?php
if(!class_exists('item_menus',false)) {
	/**
	 * An extension of the item class with functionality specific to the menus module.
	 *
	 * @package kraken\modules\menus
	 */
	class item_menus extends item {
		/**
		 * Constructs the class
		 *
		 * @param string $module The module this item is in.
		 * @param int $id The id of the item.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function __construct($module = NULL,$id = NULL,$c = NULL) {
			return $this->item_menus($module,$id,$c);
		}
		function item_menus($module = NULL,$id = NULL,$c = NULL) {
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * Returns an array of items in this menu.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the items in this menu.
		 */
		function menu_array($c = NULL) {
			// Error
			if(!$this->id) return;
			
			// Config
			if(!x($c['parent'])) $c['parent'] = 0; // The parent item we're getting child items from. Used when we recursively call this function from within this function.
			if(!x($c[cache])) $c[cache] = 1; // Cache results?
			if(!x($c[cached])) $c[cached] = 1; // Attempt to get cached results?
			if(!x($c[debug])) $c[debug] = 0; // Debug
			$module = 'menus_items';
			$array = NULL;
			
			// Languge - needed when building cache string
			if(m('languages') and plugin('international') and !$c[language] and strpos(URL,'/admin') === false) { // Language to display the data in
				$c[language] = $_SESSION['__modules']['languages']['selected']; 
				if(!$c[language]) $c[language] = m('languages.settings.languages.default');
			}
			if(!$c[language]) $c[language] = "eng";
			
			// Cached
			if($c[cached] and !$c['parent']) {
				if($array = cache_get('menus/'.$this->id.'/'.u('type')."/".$c[language])) {
					function_speed(__FUNCTION__,$f_r,1);
					return $array;
				}
			}
			
			// Module
			$module_class = module::load($module);
			// Items
			$rows = $module_class->rows(array('db.parent_id' => $this->id,'db.order_parent' => $c['parent'],'db.visible' => 1,'order' => m($module.'.db.order')." ASC",'debug' => $c[debug]));
			if($this->db->n($rows)) {
				while($row = $this->db->f($rows)) {
					// Item
					$item = $module_class->item($row);
					$item_array = $item->a();
					$item_array[url] = $item->url();
					$item_array[name] = $item->name();
					$item_array[row] = $item->row();
					
					// Children
					$_c = $c;
					$_c['parent'] = $item->id;
					$item_array[children] = $this->menu_array($_c);
					
					// Store
					$array[] = $item_array;
				}
			}
			
			// Cache
			if($c[cache] and !$c['parent']) {
				cache_save('menus/'.$this->id.'/'.u('type')."/".$c[language],$array);
			}
			
			// Return
			return $array;
		}
		
		/**
		 * Returns the HTML for displaying this menu.
		 *
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The HTML for displaying the menu.
		 */
		function menu_html($c = NULL) {
			// Error
			if(!$this->id and !$c['array']) return;
			
			// Config
			if(!$c['array']) $c['array'] = NULL; // Defined array of items. Used when we recursively call this function from within this function. Also can be used to pass a custom or altered array of menu items.  If none passed, it'll get the array from the menu_array() method.
			if(!x($c[separator])) $c[separator] = 0; // Include separator <li> tags between links
			if(!x($c[debug])) $c[debug] = 0; // Debug
			$html = NULL;
			
			// Array
			if($c['array']) $array = $c['array'];
			else {
				$array = $this->menu_array();
				debug("menu array: ".return_array($array),$c[debug]);
			}
			
			// Items
			if($array) {
				$html .= "<ul class='menu-ul'>";
				foreach($array as $k => $v) {
					// Separator
					if($k > 0 and $c[separator]) $html .= "<li class='menu-separator'>".($c[separator] !== 1 ? $c[separator] : "")."</li>";
					
					// Selected
					$selected = 0;
					if(URL == $v[url] || url_noquery(URL) == $v[url]) $selected = 1;
					
					// Children
					$children = NULL;
					if($v[children]) {
						$_c = $c;
						$_c['array'] = $v[children];
						$children = $this->menu_html($_c);
					}
					
					// LI - class
					$li_class = "menu-li";
					if($selected == 1) $li_class .= " menu-selected";
					if(strstr($children,' menu-selected')) $li_class .= " menu-child-selected";
					if($v[li][attributes]['class']) {
						$li_class .= " ".$v[li][attributes]['class'];
						unset($v[li][attributes]['class']);
					}
					$li_class .= " menu-li-id-".$v[id];
					// LI - attributes
					if($v[li][attributes]) $li_attributes = attributes_string($v[li][attributes]);
					
					// A - class
					$a_class = ($v[url] ? "menu-link" : "menu-nolink");
					if($v[a][attributes]['class']) {
						$a_class .= " ".$v[a][attributes]['class'];
						unset($v[a][attributes]['class']);
					}
					$a_class .= " menu-item-id-".$v[id];
					// A - attributes
					if($v[a][attributes]) $a_attributes = attributes_string($v[a][attributes]);
					
					// HTML
					$html .= "<li class='".$li_class."'".($li_attributes ? " ".$li_attributes : "").">";
					if($v[url]) $html .= "<a href='".$v[url]."'";
					else $html .= "<span";
					$html .= " class='".$a_class."'".($a_attributes ? " ".$a_attributes : "").">";
					if($v[item_image]) {
						//print_array($v);
						$item = item::load('menus_items',$v[row]);
						//print_array($item);
						if($selected and $v[item_image_rollover]) {
							$file = $item->file('item_image_rollover');
							$html .= "<img src='".$file->url()."' alt='".string_encode($v[name])."' width='".$file->width()."' height='".$file->height()."' />";
						}
						else {
							$file = $item->file('item_image');
							$html .= "<img src='".$file->url()."' alt='".string_encode($v[name])."' width='".$file->width()."' height='".$file->height()."'";
							if($v[item_image_rollover]) $html .= " onmouseover=\"this.src='".DOMAIN.g('config.uploads.types.image.path').$v[item_image_rollover]."';\" onmouseout=\"this.src='".DOMAIN.g('config.uploads.types.image.path').$v[item_image]."';\"";
							$html .=  " />";
						}
					}
					else $html .= $v[name];
					if($v[url]) $html .= "</a>";
					else $html .= "</span>";
					$html .= $children."</li>";
				}
				$html .= "</ul>";
			}
			
			return $html;
		}
	}
}
?>