<?php
if(!class_exists('item_user',false)) {
	/**
	 * Extension of the item class with functionality specific to the 'categories_items' module.
	 *
	 * @package kraken\modules\categories_items
	 */
	class item_categories_items extends item {
		/**
		 * construct
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			$this->item_categories_items($module,$id,$c);
		}
		function item_categories_items($module,$id = NULL,$c = NULL) {
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * Determines the URL for viewing an item in the given area.
		 *
		 * @param string $area The area of the site where this URL will link to: public [default], admin, account
		 * @param array $c An array of configuration values. Default = NULL
		 * @return string The URL of the item in the given area (if it exists).
		 */
		function url($area = NULL,$c = NULL) {
			// Parent module / view
			if(!$c[module] and !$c[view] and $area == "public") {
				$c[module] = $this->v('module_code');
				$c[view] = "category";	
			}
				
			// Return
			return parent::url($area,$c);
		}
	}
}
?>