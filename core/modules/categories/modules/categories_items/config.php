<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Category Items',
	'single' => 'Item',
	'plural' => 'Items',
	'icon' => 'categories',
	'db' => array(
		'table' => 'categories_items',
		'id' => 'item_id',
		'parent_id' => 'category_id',
		'name' => 'item_name',
		'order' => 'item_order',
		'order_parent' => 'item_order_parent',
		'order_level' => 'item_order_level',
		//'order_string' => 'item_order_string', // Deprecated
		'visible' => 'item_visible',
		'created' => 'item_created',
		'updated' => 'item_updated'
	),
	'permissions' => array(
		'add' => array(
			'label' => 'Add',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'edit' => array(
			'label' => 'Edit',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'delete' => array(
			'label' => 'Delete',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'copy' => array(
			'label' => 'Copy',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'header' => 0,
		'sidebar' => 0,
		'section' => '',
		'views' => array(
			'home' => 'All Items',
			'item' => 'Edit Item'
		),
		'urls' => array(
			'home' => array(
				'format' => 'admin/{parent_parent_module}/{parent_module}/{parent_id}/{module}'
			),
			'item' => array(
				'format' => 'admin/{parent_parent_module}/{parent_module}/{parent_id}/{module}/{id}'
			)
		),
	),
	'public' => array(
		// We have a custom url() method in the categories_items item class which will get parent module, then get the 'category' view from there. We'd only use this if we were linking to a category item independent of it's parent module
		'active' => 0,
	),
	'forms' => array(
		'add' => array(
			'inputs' => array(
				array(
					'label' => 'Name',
					'type' => 'text',
					'name' => 'item_name',
					'validate' => array(
						'required' => 1,
					),
				)
			)
		)
	)
);
?>