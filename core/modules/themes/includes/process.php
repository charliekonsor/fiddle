<?php
/************************************************************************************/
/********************************** themes_change ***********************************/
/************************************************************************************/
if($_GET['process_action'] == "themes_change") {
	// Module
	$module = "themes";
	
	// Permission
	if(!permission($module,'change')) page_error("I'm sorry, but you don't have permission to do this.");
	// Theme
	else if(!$_GET['theme']) page_error("Please select the theme you'd like to use.");
	// Save
	else {
		// Save
		$module_class = module::load('settings');
		$module_class->setting('theme',$_GET['theme']);
	}
	
	// Redirect
	redirect(DOMAIN."admin/".$module);
}