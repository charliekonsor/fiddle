<?php
// Permission
if($this->permission('edit',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	
	// Theme
	$theme = theme($this->item_class->code);
	if($settings = $theme[settings][inputs]) {
		// Values
		foreach($settings as $k => $v) {
			if($v[name]) {
				// Value
				$name = array_string($v[name]);
				$settings[$k][value] = array_eval($theme[settings],$name);
					
				// Name
				$name = string_array("theme_settings.".$name);
				$settings[$k][name] = $name;
			}
		}
		
		// Form
		$form = $this->item_class->form(array('redirect' => URL,'plugins' => 0));
		$form->inputs($settings);
		print $form->render();
	}
	else print "
<div class='core-none'>No Settings Found</div>";
}

?>