<?php
/************************************************************************************/
/*************************************** save ***************************************/
/************************************************************************************/
if($_POST['process_action'] == "save") {
	// Module
	$module = $_POST['process_module'];
	//print "module: ".$module."<br />";
	//print "post: ".return_array($_
	//$m = module::load($module,array('type' => $_POST['type_code'])); // Should probably come up with a better way to retrieve 'type'
	$m = module::load($module); // Should probably come up with a better way to retrieve 'type'
	
	// Form
	$form = $m->form();
	
	// Process
	$results = $form->process(g('unpure.post'),g('unpure.files'),array('debug' => 0));
	
	// Errors
	if($results[errors]) {
		if($results[url] and (!$results[c][debug] or !debug_active())) {
			page_error("There was an error saving this ".$m->v('single').". Please correct the ".string_pluralize("error",count($results[errors][inline]))." shown below.",1);
			redirect($results[url]);
		}
		else if(debug_active()) print "
<div class='core-red'>
	<b>Errors:</b>
	".return_array($results[errors])."
</div>";
	}
	// No errors
	else {
		$error = NULL;
		
		// Double check username
		if($form->input_exists('user_name')) {
			if(user_name_exists_framework($results[values][user_name],$results[id])) {
				$error = "This username is already taken.";
			}
		}
		
		// Double check e-mail
		if(g('config.register.email.unique')) {
			if($form->input_exists('user_email')) {
				if(user_email_exists_framework($results[values][user_email],$results[id])) {
					$error = "This e-mail address is already in use.";
				}
			}
		}
		
		// Type - make sure they didn't change it to an admin user type (unless they have permission to)
		if(m('users.types.'.$results[values][type_code].'.settings.admin') and (u('type') == "guests" or !permission('users','add'))) {
			$error = "You don't have permission to become this user type.";
		}
		
		// Error
		if($error) {
			if($results[url] and (!$results[c][debug] or !debug_active())) {
				page_error($error);
				redirect($results[url]);
			}
			else if(debug_active()) {
				print "
<div class='core-red'>
	".$error."
</div>";	
				exit;
			}
		}
		
		// Item
		$item = $m->item($results[id]);
		
		// Save
		$id = $item->save($results[values],array('debug' => $results[c][debug]));
		
		// Success
		if($id > 0) {
			// Login - new user or edited own account, relogin so we have most recent info stored in $_SESSION
			if((!$results[id] or $results[id] == "new") and !u('id') or $id == u('id')) {
				$core->login($id,array('source' => (!$results[id] || $results[id] == "new" ? "register" : "update")));
			}
			
			// Message
			if($results[id] > 0) $message = 'Changes saved.';
			else if($id == u('id')) $message = "You've successfully registered!";
			else $message = 'This '.strtolower($m->v('single',$results[values][$m->v('db.type')])).' has been successfully added.';
			page_message($message);
	
			// Redirect
			if($results[redirect] and (!$results[c][debug] or !debug_active())) redirect($results[redirect]);
		}
		// Error
		else {
			$error = "There was an error saving this ".strtolower($m->v('single')).".";
			if($results[url] and (!$results[c][debug] or !debug_active())) {
				page_error($error);
				redirect($results[url]);
			}
			else if(debug_active()) print "
<div class='core-red'>
	".$error."
</div>";	
		}
	}
	
	// Exit
	exit;
}
?>