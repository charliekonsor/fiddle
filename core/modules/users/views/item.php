<?php
// Visible / permission
if($this->visible(1) and $this->permission('view',1)) {
	print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>";
	// Heading
	//print $this->heading();
	print "
<h1 class='page-heading'>
	<a href='".$this->module_class->url()."'>".$this->module_class->v('name')."</a>:
	<a href='".$this->item_class->url()."'>".$this->item_class->name."</a>
</h1>";
	// Buttons
	print $this->heading_buttons();
		
	// Form
	$form = $this->item_class->form(array('redirect' => $this->item_class->url()));
	print $form->render();

	print "
</div>";
}
?>