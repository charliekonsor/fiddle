<?php
// Item
$item = item::load('users',u('id'));

// Permission
if($item->permission('edit',1)) {
	print "
<div class='page page-module-".$this->module." page-view-".$this->view."'>";
	// Heading
	print "
<h1 class='page-heading'>
	<a href='".$this->module_class->url($this->view,$this->area)."'>Profile</a>
</h1>";
	// Buttons
	print $this->heading_buttons();
		
	// Form
	$form = $item->form(array('redirect' => $this->module_class->url($this->view,$this->area)));
	print $form->render();

	print "
</div>";
}
?>