<?php
// Heading
print $this->heading();
// Buttons
$buttons = $this->heading_buttons_array();
if($this->item_module_class->permission('permissions')) $buttons[DOMAIN.$this->area."/".$this->module."/".$this->type."/permissions"] = "Permissions";
print $this->heading_buttons_html($buttons);

// Items
if(!$this->page->variables[0]) {
	// Permission
	if($this->module_class->permission('manage',1)) {
		// Filters
		print $this->type_class->manage_filters();
		// Items
		print $this->type_class->manage_items();
	}
}

// Permissions
if($this->page->variables[0] == "permissions") {
	// Permission
	if($this->item_module_class->permission($this->page->variables[0],1)) {
		$module = "types";
		$id = $this->type_class->v('id');
		// Item
		$item = item::load($module,$id);
		// Form
		$form = $item->form(array('plugins' => 0));
		
		// Permissions
		$permissions = data_unserialize($item->v('type_permissions'));
		
		// Modules
		$found = 0;
		foreach(m() as $module => $options) {
			// Module permissions
			if($options[permissions]) {
				// Collapsed
				if($found) $form->html("</div>");
				$form->html("<a href='javascript:void(0);' onclick=\"toggle('#permissions-".$module."-content',{heading:'permissions-".$module."-heading'});\" id='permissions-".$module."-heading' class='toggle-closed'>".$options[name]."</a>");
				$form->html("<div id='permissions-".$module."-content' style='display:none;padding:2px 18px;'>");
				$form->checkbox(NULL,NULL,array(1 => '<em>Check All</em>'),NULL,array('onclick' => "checkAll(this,'checkbox_".$module."');"));
				$found = 1;
				
				// Inputs
				foreach($options[permissions] as $k => $v) {
					$checked = ($permissions[$module][$k] ? 1 : 0);
					$form->checkbox(NULL,"type_permissions[".$module."][".$k."]",array(1 => $v[label]),$checked,array('class' => 'checkbox_'.$module));
				}
				
				// Plugin permissions
				$indented = 0;
				foreach($options[plugins] as $plugin => $active) {
					if($active) {
						if($plugin_permissions = plugin($plugin.'.permissions')) {
							if($indented) $form->html("</div>");
							
							$indented = 1;
							$form->html("<em>".plugin($plugin.'.name')."</em><div style='padding-left:5px;'>");
							
							foreach($plugin_permissions as $k => $v) {
								$k = $plugin.'.'.$k;
								$checked = ($permissions[$module][$k] ? 1 : 0);
								$form->checkbox(NULL,"type_permissions[".$module."][".$k."]",array(1 => $v[label]),$checked,array('class' => 'checkbox_'.$module));
							}
						}
					}
				}
				if($indented) $form->html("</div>");
			}
		}
		if($found) {
			$form->html('</div><br />');
			print $form->render();
		}
		else print "
<div class='core-none'>No Permissions Found</div>";
	}
}
?>