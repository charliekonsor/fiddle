<?php
if(!class_exists('item_users',false)) {
	/**
	 * Extension of the item class with functionality specific to the 'users' module.
	 *
	 * @package kraken\modules\users
	 */
	class item_users extends item {
		/**
		 * construct
		 */
		function __construct($module,$id = NULL,$c = NULL) {
			$this->item_users($module,$id,$c);
		}
		function item_users($module,$id = NULL,$c = NULL) {
			parent::__construct($module,$id,$c);
		}
		
		/**
		 * Saves an item, either inserting it or (if $this->id) updating the existing row, then processes its data.
		 *
		 * @param array $values An array of values to save (column => value). Note you can pass a global databse definition as the column, example "db.name" will save it to the defined name column.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return int The id of the new item.
		 */
		function save($values = NULL,$c = NULL) {
			// Password
			if($values[user_password]) {
				// Hash
				$hash = $this->v('user_hash');
				if(!$hash) {
					$hash = $values[user_hash] = string_random();
					debug("Created hash for user: ".$hash,$c[debug]);
				}
				
				// Encrypt
				$values[user_password] = crypt(trim(strtolower($values[user_password])),$hash);
				debug("Encrypted password",$c[debug]);
			}
			
			// Parent
			return parent::save($values,$c);
		}
		
		/**
		 * Returns either an array of 'external' login info for this user or (if $provider passed)
		 * an array of info on their external login for that specific provider.
		 *
		 * @param string $provider The specific provider you want to get this user's external login info for. Default = NULL
		 * @param string $provider_id The specific provider id you want to get this user's external login info for. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return array An array of the user's external login info (specific to the given $provider if one's passed).
		 */
		function external_get($provider = NULL,$provider_id = NULL,$c = NULL) {
			// Error - need either a user to look up or a provider and provider id
			if(!$this->id and (!$provider or !$provider_id)) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// HybridAuth
			$hybridauth = $this->external_authenticate_object();
			if(!$hybridauth) return;
			//$hybridauth_session = array();
		
			// Query
			$query = "external_deleted = 0";
			if($this->id) $query .= ($query ? " AND " : "")."user_id = '".$this->id."'";
			if($provider) $query .= ($query ? " AND " : "")."external_provider = '".a($provider)."'";
			if($provider_id) $query .= ($query ? " AND " : "")."external_id = '".a($provider_id)."'";
			$query = "SELECT * FROM users_external WHERE ".$query;
		$this->db->q("INSERT INTO test SET test = '".a("external login. log external_get() 1. provider: ".$provider.", provider_id: ".$provider_id.", user: ".return_array($this).", query: ".$query.", ip: ".IP.", urls: ".return_array($_SESSION['urls']),1)."'");
			debug($query,$c[debug]);
			
			// External logins
			$rows = $this->db->q($query);
			while($row = $this->db->f($rows)) {
				$array[$row[external_provider]] = $row;
				if($row[external_session]) {
					//$hybridauth_session = array_merge($hybridauth_session,unserialize($row[external_session]));
					$data = unserialize($row[external_session]);
					foreach($data as $k => $v) {
						$_SESSION["HA::STORE"][$k] = $v;
					}
				}
			}
		$this->db->q("INSERT INTO test SET test = '".a("external login. log external_get() 2. provider: ".$provider.", provider_id: ".$provider_id.", array: ".return_array($array).", ip: ".IP.", urls: ".return_array($_SESSION['urls']),1)."'");
			
			// Session
			/*if($hybridauth_session) {
				$hybridauth->restoreSessionData(serialize($hybridauth_session));
			}*/
			
			// Return
			if($provider) return $array[$provider];
			else return $array;
		}
		
		/**
		 * Saves the external info for this user and the given provider.
		 *
		 * @param string $provider The specific provider you want to save this user's external login info for.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function external_save($provider,$c = NULL) {
			// Error
			if(!$this->id or !$provider) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Info
			$user = $this->external_connected($provider,array('save' => 0,'debug' => $c[debug]));
			if($user) {
				$user_profile = $this->external_profile($user,array('debug' => $c[debug]));
				if($user_profile->identifier) {
					$hybridauth = $this->external_authenticate_object();
					if(!$hybridauth) return;
					
					$data_array = unserialize($hybridauth->getSessionData());
					foreach($data_array as $k => $v) {
						if(!strstr($k,$provider)) {	
							unset($data_array[$k]);
						}
					}
					$data = serialize($data_array);
					debug("session data: ".$data,$c[debug]);
					debug("session: ".return_array($_SESSION["HA::STORE"]),$c[debug]);
				
					// Query
					$query = "users_external SET user_id = '".$this->id."', external_provider = '".a($provider)."', external_session = '".a($data)."', external_id = '".$user_profile->identifier."', external_updated = '".time()."'";
					if($row = $this->external_get($provider,$user_profile->identifier,array('debug' => $c[debug]))) {
						$query = "UPDATE ".$query." WHERE id = '".$row[id]."'";
					}
					else {
						$query = "INSERT INTO ".$query.", external_created = '".time()."'";	
					}
					
					// Save
					debug($query,$c[debug]);
					$this->db->q($query);
				}
			}
		}
		
		/**
		 * Removes record of connection (if connection is now 'failed').
		 *
		 * @param string $provider The provider you want to remove the connection for with this user.
		 * @param object $connection The Hybrid_Provider_Adapter object for this connection.
		 * @param array $c An array of configuration values. Default = NULL
		 */
		function external_remove($provider,$connection,$c = NULL) {
			// Error
			if(!$this->id or !$provider) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Database
			$query = "UPDATE users_external SET external_deleted = 1 WHERE user_id = '".$this->id."' AND external_provider = '".$provider."'";
			debug($query,$c[debug]);
			$this->db->q($query);
			
			// Connection
			if($connection) {
				debug("logging out of connection",$c[debug]);
				try {
					$connection->logout();
				}
				catch (Exception $e) {
					debug($e->getMessage(),$c[debug]);
				}
			}
		}
		
		/**
		 * Returns external user profile object, using the given external user object.
		 *
		 * We use this so we can 'cache' it and limit calls to the external APIs.
		 *
		 * @param object $user The external user object you want to use to get the profile info.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The HybridAuth getUserProfile object for this user on the given provider's website.
		 */
		function external_profile($user,$c = NULL) {
			// Error
			if(!$user) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0;
			
			// Cached
			if($profile = $this->cache_local[external][profile][$user->id]) {
				debug("returning cached profile: ".return_array($profile),$c[debug]);
				return $profile;
			}
			
			// Profile
			try {
				$profile = $user->getUserProfile();
			}
			catch (Exception $e) {
				debug($e->getMessage(),$c[debug]);
			}
			
			// Cache
			if($profile) {
				$this->cache_local[external][profile][$user->id] = $profile;
				$this->cache_save();
			}
			
			// Debug
			debug("profile: ".return_array($profile),$c[debug]);
			
			// Return
			return $profile;
		}
		
		/**
		 * Checks whether or not this user is connected externally with the given provider or (if no $provider is passed) 
		 * it checks to see all providers this user is connected with.
		 *
		 * Basically tries its best to find a saved session for this user and provider (via $this->external_get()).
		 * If it does, that saved session is lodded into the $_SESSION and the HybridAuth object should return truen when 
		 * isConnectedWith() is called. 
		 *
		 * @param string $provider The provider you want to check to see if this user is connected with. Default = NULL
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object|array Either the HybridAuth->authenticate() object for the given provider (if $provider was passed) or an array of those objects for all providers this user is connected with. If they're not connected, nothing is returned.
		 */
		function external_connected($provider = NULL,$c = NULL) {
			// Config
			if(!x($c[save])) $c[save] = 1; // Save external row (if connected). Don't want to do if calling from $this->external_save() as we'd be stuck in an in finite loop.
			if(!x($c[debug])) $c[debug] = 0; // Debug
			debug("external_connected(".$provider.")",$c[debug]);
			
			// HybridAuth
			$hybridauth = $this->external_authenticate_object();
			if(!$hybridauth) return;
				
			// Connections - session
			$connections = $hybridauth->getConnectedProviders();
			if($connections) {
				debug("connections (session): ".return_array($connections),$c[debug]);
				foreach($connections as $k => $v) {
					$key = hybridauth_provider_code($v);
					if(!$provider or $provider == $key) {
						$remove = 0;
						// Connected?
						try {
							$array[$key] = $hybridauth->authenticate($v);
							$profile = $this->external_profile($array[$key],array('debug' => $c[debug]));
							if(!$profile->identifier and $provider != "twitter") { // Ignore twitter because this can fail because of rate limiting
								$remove = 1;
							}
							//else debug("got connection (from session) for ".$key.": ".return_array($array[$key]));
							
							// Check if used already - should figure out how to actually remove this from the session when we logout/login as a nother user id, but for now...
							$row = $this->db->f("SELECT * FROM users_external WHERE external_provider = '".$provider."' AND external_id = '".$profile->identifier."' AND user_id != '".$this->id."'");
							if($row[id]) unset($array[$key]);
						}
						catch (Exception $e) {
							$remove = 1;
							debug($e->getMessage(),$c[debug]);
						}
						
						// Remove
						if($remove) {
							$this->external_remove($key,$array[$key]);
							unset($array[$key]);
						}
					}
				}
			}
				
			// Connectsions - database
			if((!$provider or !$array[$provider]) and $this->id) {
				$connections = $this->external_get($provider,NULL,array('debug' => $c[debug]));
				if($connections) {
					debug("connections (database): ".return_array($connections),$c[debug]);
					foreach($connections as $k => $v) {		
						$key = ($provider ? $provider : $k); // If we pass a $provider to external_get(), it only returns that providers connection object, not an array of connections, in which case we can't use $k.
						if(!$array[$key]) {	
							$remove = 0;
							// Connected?
							try {
								$provider_hybridauth = hybridauth_provider($key);
								if($hybridauth->isConnectedWith($provider_hybridauth)) {
									// Yes, authenticate
									$array[$key] = $hybridauth->authenticate($provider_hybridauth);
									$profile = $this->external_profile($array[$key],array('debug' => $c[debug]));
									if(!$profile->identifier) $remove = 1;
									//else debug("got connection (from db) for ".$key.": ".return_array($array[$key]));
								}
							}
							catch (Exception $e) {
								$remove = 1;
								debug($e->getMessage(),$c[debug]);
							}
						
							// Remove
							if($remove) {
								$this->external_remove($key,$array[$key]);
								unset($array[$key]);
							}
						}
					}
				}
			}
							
			// Save
			if($c[save] and $array) {
				foreach($array as $k => $v) {
					$this->external_save($k,array('debug' => $c[debug]));
				}
			}
			
			// Return
			if($provider) return $array[$provider];
			else return $array;
		}
		
		/**
		 * Authenticates external user profile at given provider's website.
		 *
		 * If already 'connected', returns info.
		 * If not, it redirects them to allow connection.
		 *
		 * @param string $provider The provider you want to authenticate user's with.
		 * @param array $c An array of configuration values. Default = NULL
		 * @return object The HybridAuth getUserProfile object for this user on the given provider's website.
		 */
		function external_authenticate($provider,$c = NULL) {
			// Error
			if(!$provider) return;
			
			// Config
			if(!x($c[debug])) $c[debug] = 0; // Debug
			
			// Cached
			if($user = $this->cache_local[external][authenticate][$provider]) { 
				debug("returning cached authentication",$c[debug]);
				return $user;
			}

			// Authenticate
			try {
				$hybridauth = $this->external_authenticate_object();
				if(!$hybridauth) return;
				$provider_hybridauth = hybridauth_provider($provider);
				debug("authenticating ".$provider_hybridauth." (".$provider.")",$c[debug]);
				if($provider_hybridauth) {
					// Try to authenticate with the selected provider
					$user = $hybridauth->authenticate($provider_hybridauth);
					debug("authenticated user: ".return_array($user),$c[debug]);
				}
			}
			catch (Exception $e) {
				debug($e->getMessage(),$c[debug]);
			}
			
			// Cache
			if($user) {
				$this->cache_local[external][authenticate][$provider] = $user;
				$this->cache_save(); // Not sure I want to save this for an hour
			}
			
			// Return
			return $user;
		}
		
		/**
		 * Sets up and returns a Hybrid_Auth class object.
		 *
		 * @return object The HybridAuth object.
		 */
		function external_authenticate_object() {
			// Cached
			/*if($hybridauth = $this->cache_local[external][hybridauth]) {
				return $hybridauth;
			}*/
			
			// Object
			try {
				// Class
				$config = require SERVER."core/core/libraries/hybridauth/config.php";
				$hybridauth = new Hybrid_Auth($config);
			}
			catch (Exception $e) {
				debug($e->getMessage(),$c[debug]);
			}
			
			// Cache
			//$this->cache_local[external][hybridauth] = $hybridauth;
			
			// Return
			return $hybridauth;
		}
	}
}
?>