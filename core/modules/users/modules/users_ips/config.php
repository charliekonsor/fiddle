<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'User IPs',
	'single' => 'User IP',
	'plural' => 'User IPs',
	'icon' => 'users_ips',
	'db' => array(
		'table' => 'users_ips',
		'parent_id' => 'user_id',
		'id' => 'ip_id',
		'name' => 'ip',
		'created' => 'ip_created',
		'updated' => 'ip_updated',
	),
	'permissions' => array(
		'view' => array(
			'label' => 'View',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		),
		'settings' => array(
			'label' => 'Settings',
			'default' => array(
				'admins',
			)
		),
	),
	'admin' => array(
		'active' => 0, // Comment this out to see IPs in admin // Note, not yet built (need icon and admin view files)
		'sidebar' => 0,
		'views' => array(
			'home' => 'All User IPs',
			'item' => 'Single User IP'
		)
	),
	'public' => array(
		'active' => 0,
	),
	'settings' => array(),
);
?>
