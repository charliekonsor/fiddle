<?php
// Permission
if($this->permission('manage',1)) {
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	// Filters
	print $this->module_class->manage_filters();
	
	// Module
	$module = $this->page->parents[last][module];
	
	// Sortable
	$sort = $this->module_class->manage_items_sort();
	
	// Items
	$array = $this->module_class->manage_items(array('return' => 'array','perpage' => 9999,'query_c' => array('where' => ($module ? "module_code = '".$module."'" : ""),'order' => 'module_code ASC, '.($this->module_class->v('db.order') ? $this->module_class->v('db.order')." ASC" : $this->module_class->v('db.name')." ASC"))));
	if($array[items]) {
		if($sort) print "
".include_javascript('jquery.ui.sortable');
		if($module) print "
<table class='core-table core-rows".($sort ? " sort {vars:\"module=".$this->module."\"}" : "")."'>";
		$saved = NULL;
		foreach($array[items] as $item) {
			if($item->v('module_code') != "users" or $item->code != "supers") {
				// Module heading
				if($item->v('module_code') != $saved and !$module) {
					if($saved) print "
</table><br />";
					print "
<h2>".m($item->v('module_code').'.name')."</h2>
<table class='core-table core-rows".($sort ? " sort {vars:\"module=".$this->module."\"}" : "")."'>";
				}
				
				// Item - module
				$item_module_class = module::load($item->v('module_code'));
				$url_module = DOMAIN.$this->area."/".$item->v('module_code')."/".$item->code;
				
				// Icons
				$icons = NULL;
				// Icon - sort
				if($sort) $icons[sort] = "<span class='i i-sort i-inline sort-handle tooltip' title='Sort'><input type='hidden' class='sort-sorted' value='".$item->id."' /></span>";
				// Icon - edit
				//$icons[edit] = $item->icon('edit');
				// Icon - settings
				if(m($item->v('module_code').'.settings.inputs',$item->code) and permission($item->v('module_code'),'settings',NULL,array('type' => $item->code))) $icons[settings] = $item_module_class->icon('settings',array('url' => $url_module."/settings"));
				// Icon - permissions
				if($item->permission('permissions')) {
					$icons[permissions] = $item->icon('permissions',array('url' => $url_module."/permissions"));
				}
				// Icon - copy
				//$icons[copy] = $item->icon('copy');
				// Icon - disable
				//$icons[disable] = $item->icon('disable');
				// Icon - delete
				//$icons[delete] = $item->icon('delete');
				
				// Icons - width
				$icons = array_filter($icons);
				$icons_width = (count($icons) * 18);
				
				// Row
				print "
	<tr id='".$this->module."-".$item->id."'".($sort ? " class='sort-sortable'" : "").">";
				if($icons) print "
		<td width='".$icons_width."'>".implode($icons)."</td>";
				print "
		<td>".$item->name."</td>
	</tr>";
		
				$saved = $item->v('module_code');
			}
		}
		print "
</table>";
	}
	else print "
<div class='core-none'>No ".$this->module_class->v('plural')." Found</div>";
}

?>