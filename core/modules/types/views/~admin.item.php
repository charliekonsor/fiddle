<?php
// Variables
$module = $this->item_class->v('module_code');
$type = $this->item_class->code;

// Heading
print $this->heading();
// Buttons
print $this->heading_buttons();

// Settings
/*if(!$this->page->variables[0]) {
	// Permission
	if($this->permission('edit',1)) {
		// Settings
		$settings = m($module.'.settings.inputs',$type);
		if($settings) {
			// Values
			foreach($settings as $k => $v) {
				if($v[name]) {
					// Value
					$name = array_string($v[name]);
					$settings[$k][value] = m($module.'.settings.'.$name,$type);
					
					// Name
					$name = string_array("type_settings.".$name);
					$settings[$k][name] = $name;
				}
			}
			
			// Redirect
			$redirect = DOMAIN.$this->area.($this->page->parents[last][module] ? "/".$this->page->parents[last][module] : "")."/".$this->module;
			
			// Form
			$form = $this->item_class->form(array('redirect' => $redirect));
			$form->inputs($settings);
			print $form->render();
		}
		else print "
<div class='core-none'>No Settings Found</div>";
	}
}

// Permissions
if($this->page->variables[0] == "permissions") {*/
	// Permission
	if($this->permission($this->page->variables[0],1)) {
		// Form
		$form = $this->item_class->form();
		
		// Permissions
		$permissions = data_unserialize($this->item_class->v('type_permissions'));
		
		// Fields
		$found = 0;
		foreach(m() as $module => $options) {
			if($options[permissions]) {
				if($found) $form->html("</div>");
				$form->html("<a href='javascript:void(0);' onclick=\"toggle('#permissions-".$module."-content',{heading:'permissions-".$module."-heading'});\" id='permissions-".$module."-heading' class='toggle-closed'>".$options[name]."</a>");
				$form->html("<div id='permissions-".$module."-content' style='display:none;padding:2px 18px;'>");
				$found = 1;
				foreach($options[permissions] as $k	=> $v) {
					$checked = ($permissions[$module][$k] ? 1 : 0);
					$form->checkbox(NULL,"type_permissions[".$module."][".$k."]",array(1 => $v[label]),$checked);
				}
			}
		}
		if($found) {
			$form->html('</div><br />');
			print $form->render();
		}
		else print "
<div class='core-none'>No Permissions Found</div>";
	}
//}
?>