<?php
/**
 * Delete's a module and its files.
 */
if($_POST['ajax_action'] == "delete") {
	if($_POST['module'] and $_POST['id'] > 0) {
		// Item
		$item = item::load($_POST['module'],$_POST['id']);
		// Permission
		if($item->permission('delete')) {
			// Delete files
			if($item->code and !m($item->code.'.core')) file::directory_delete(SERVER."local/modules/".$item->code);
			
			// Delete types
			if($types = m($item->code.'.types')) {
				foreach($types as $type => $v) {
					$type_item = item::load('types',$v[id]);
					$type_item->delete(array('debug' => 1));	
				}
			}
			
			// Delete item
			# will happen in the normal delete script in core/framework/includes/ajax.php
		}
	}
	
	// Exit
	//exit; // Don't want to exit, want to do normal delete in addition the file deleting we just did.
}