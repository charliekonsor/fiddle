<?php
// Permission
if($this->permission('add',1)) {
	// Make sure we're adding a new one
	$this->id = NULL;
	
	// Heading
	print $this->heading();
	// Buttons
	print $this->heading_buttons();
	
	// Form
	$form = $this->module_class->form(array('inline' => 1,'submit' => 0,'plugins' => 0));
	$form->file(NULL,'file',NULL,array('file' => array('path' => SERVER.g('config.uploads.types.file.path'),'overwrite' => 1,'browse' => 0)));
	$form->submit('Install',array('style' => 'margin-top:3px;'));
	$form->hidden('process_action','modules_install');
	print $form->render();
}
?>