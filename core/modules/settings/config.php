<?php
/************************************************************************************/
/************************************** Config **************************************/
/************************************************************************************/
$config = array(
	'name' => 'Settings',
	'single' => 'Setting',
	'plural' => 'Settings',
	'icon' => 'settings',
	'permissions' => array(
		'settings' => array(
			'label' => 'Manage',
			'manage' => 1,
			'default' => array(
				'admins',
			)
		)
	),
	'admin' => array(
		'section' => 'system',
		'views' => array(
			'home' => array(
				'label' => 'Settings',
				'urls' => array(
					array(
						'format' => 'admin/settings',
					),
				)
			),
		)
	),
	'public' => array(
		'active' => 0
	),
	'settings' => array(
		'inputs' => array(
			array(
				'type' => 'html',
				'value' => '<b>Site</b>'
			),
			array(
				'label' => 'Name',
				'type' => 'text',
				'name' => 'site[name]',
				'validate' => array(
					'required' => 1,
				),
			),
			array(
				'type' => 'html',
				'value' => '<b>Meta</b>'
			),
			array(
				'label' => 'Title',
				'type' => 'text',
				'name' => 'site[meta][title]',
				'validate' => array(
					'maxlength' => g('config.meta.maxlength'),
				),
			),
			array(
				'label' => 'Description',
				'type' => 'text',
				'name' => 'site[meta][description]',
				'validate' => array(
					'maxlength' => g('config.meta.maxlength'),
				),
			),
			array(
				'label' => 'Keywords',
				'type' => 'text',
				'name' => 'site[meta][keywords]',
				'validate' => array(
					'maxlength' => g('config.meta.maxlength'),
				),
			),
			array(
				'label' => 'Site Verification',
				'type' => 'textarea',
				'name' => 'site[meta][verification]',
				'purify' => array(
					'meta' => 0, // Don't strip meta tags when purifying
					'xss' => 0, // Don't encode ", <, >, etc. when purifying
					'characters' => 0, // Don't encoded characters into UTF-8
					'comments' => 0, // Don't strip comments
				),
			),
			array(
				'label' => 'Analytics',
				'type' => 'textarea',
				'name' => 'site[meta][analytics]',
				'purify' => array(
					'js' => 0, // Don't strip javascript when purifying
					'xss' => 0, // Don't encode ", <, >, etc. when purifying
					'characters' => 0, // Don't encoded characters into UTF-8
					'comments' => 0, // Don't strip comments
				)
			),
			array(
				'type' => 'html',
				'value' => '<b>E-mail</b>'
			),
			array(
				'label' => 'From Address',
				'type' => 'email',
				'name' => 'email[from][address]',
				'validate' => array(
					'email' => 1
				),
			),
			array(
				'label' => 'From Name',
				'type' => 'text',
				'name' => 'email[from][name]'
			),
			
			// Hidden settings (we'll set them manually somewhere in the admin)
			array(
				'type' => 'hidden',
				'name' => 'theme'
			)
		)
	)
);
?>